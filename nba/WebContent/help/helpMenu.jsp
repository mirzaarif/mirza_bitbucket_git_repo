<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA349		    NB-1401	  nbA IP Compliance -->
<!-- SPR3824		NB-1401   Documentation Clean Up -->
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<head>
			<base href="<%=	basePath%>">
			<title>Help Menu</title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<link rel="stylesheet" type="text/css" href="css/accelerator.css">
			<script language="JavaScript" src="include/common.js" ></script>
			<script language="JavaScript" src="javascript/global/desktopComponent.js" ></script>
			<script type="text/javascript">
				var helpBasePath = '<%=basePath%>';
				
				function openAbout() {
				   	var path = "../init/aboutForm.jsp";//NBA349
				    window.open(path,'','status=no,resizable=yes,scrollbars=yes,width=540,height=530');//NBA349
				    top.document.all.helpMenuFrame.style.visibility = 'hidden';
				}
				
				function openFAQ() {
					var path = helpBasePath + "docs/nbafaq.htm";
				    window.open(path,'FAQ','status=no,resizable=yes,scrollbars=yes,width=600,height=400');
				   	top.document.all.helpMenuFrame.style.visibility = 'hidden';
				}
				function openContents()	{
					var path = helpBasePath + 'docs/nbahelp.htm';
				   	window.open(path ,'Help','status=no,resizable=yes,scrollbars=yes,width=600,height=400');
				   	top.document.all.helpMenuFrame.style.visibility = 'hidden';
				}
				
				function openProcessGuide() {
					var path = helpBasePath + 'docs/nbA Business Process Guide.htm';  //SPR3824
				   	window.open(path ,'Guide','status=no,resizable=yes,scrollbars=yes,width=600,height=400');
				   	top.document.all.helpMenuFrame.style.visibility = 'hidden';
				}
				
				function highlight(cell, textElement) {
					var item = document.getElementById(textElement);
					item.className = "menuItemSelected";
					cell.className = "menuItemSelected";
				}			
				
				function unHighlight(cell, textElement) {
					var item = document.getElementById(textElement);
					item.className = "menuItem";
					cell.className = "menuItem";
				}	
				
				function closeMenu(){
					setTimeout("top.document.all.helpMenuFrame.style.visibility = 'hidden';", 300);
				}		
			</script>
		</head>
		<body class="helpMenuBody" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
			<h:form id="helpMenuForm">
				<f:loadBundle basename="properties.nbaApplicationData" var="property" />
				<div id="helpMenu" class="menuHelp">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td class="menuItem" onmouseover="highlight(this,'helpMenuForm:aboutText');" onmouseout="unHighlight(this,'helpMenuForm:aboutText');">
								<a id="aboutAnchor" onclick="openAbout();" title="About">
									<h:outputText id="aboutText" styleClass="menuItem" value="#{property.about}"/>
								</a>
							</td>
						</tr>
						<tr>
							<td class="menuItem" onmouseover="highlight(this,'helpMenuForm:faqText');" onmouseout="unHighlight(this,'helpMenuForm:faqText');">
								<a id="faqAnchor" onclick="openFAQ();" title="FAQ">
									<h:outputText id="faqText" styleClass="menuItem" value="#{property.faq}"/>
								</a>
							</td>
						</tr>
						<tr>
							<td class="menuItem" onmouseover="highlight(this,'helpMenuForm:bpgText');" onmouseout="unHighlight(this,'helpMenuForm:bpgText');">
								<a id="bpgAnchor" onclick="openProcessGuide();" title="BPG">
									<h:outputText id="bpgText" styleClass="menuItem" value="#{property.bpg}"/>
								</a>
							</td>
						</tr>
						<tr>
							<td class="menuItem" onmouseover="highlight(this,'helpMenuForm:contentsText');" onmouseout="unHighlight(this,'helpMenuForm:contentsText');">
								<a id="contentsAnchor" onclick="openContents();" title="Contents">
									<h:outputText id="contentsText" styleClass="menuItem" value="#{property.contents}"/>
								</a>
							</td>
						</tr>
					</table>
				</div>
			</h:form>
		</body>
	</f:view>
</html>
