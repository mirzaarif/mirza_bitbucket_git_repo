<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3055           6      Decoupling of Version 5 JSF views from Accelerator Framework -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- NBA660		NB-1301	  AWD Image Viewer does not close open images when a user clicks Close -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA350			NB-1401   Applet Implementation of AWD Image Viewer -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>New Business Coordinator Desktop</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	<!--
	var dmWorkPath = "javascript/menu/";
	function initCoord(){
		try{
			top.loadMenuBar('<%=path%>','/faces/desktops/menus/coordinator.jsp');
			top.setContentAreaSize(); // NBA213
			//NBA350 code deleted
			//top.loadContextBar('<%=path%>','/faces/desktops/menus/nbaStatusBar.jsp');
		} catch(err){
			alert("err");
		}
	}
	//-->
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<script type="text/javascript" src="javascript/menu/dmenu.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="javascript/menu/menuattributes.js"></script>

</head>
<body class="desktopPanelBody" onload="initCoord();">  <!-- SPR3055 -->
	<f:view>
	<div style="display:none;">
					<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" /> 	
					<f:loadBundle var="property" basename="properties.nbaApplicationData" />  <!-- NBA186 -->
	</div>
	<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
	<tbody>
	<tr class="menubar" id="menuBar" height="18px">
		<td colspan="2" align="left">
			<iframe height="22px" width="100%" id="desktopMenu" name="desktopMenu" src="initializingMenu.html" frameborder="0" scrolling="no"></iframe>
		</td>
	</tr>
	<tr class="contextBar" id="contextBar">
		<td align="left" id="contextMenuContainer" height="25px">
			<h:graphicImage value="#{bundle.DesktopImage_3}" style="vertical-align: middle;"></h:graphicImage>
			<h:outputText value="#{bundle.DesktopType_3}"></h:outputText>
		</td>
		<td id="dateMenuContainter" align="right">
			<div id="CurrentDate"></div>
		</td>
	</tr>
	
	<tr class="desktopPane">
			<td valign="top" colspan="2">
				<h:form id="underwriterWorkbenchForm">
					<div id="contentArea">
					<table id="contentAreaTable" height="100%" width="100%">
						<tbody>
							<tr>
								<td id="leftPane"><iframe name="contentLeftFrame" height="100%"
									width="100%" scrolling="NO" frameborder="0"
									src="faces/welcome.jsp" marginheight="0"
									marginwidth="0"></iframe></td>
							</tr>
						</tbody>
					</table>
					</div>
				</h:form>
			</td>
	</tr>
</table>
	<!-- begin NBA186 -->
	<h:form id="coordmenuForm">
		<h:outputText value="#{pc_Coordinator.reset}" style="display:none;" ></h:outputText>
		<div id="utilMenu" class="menuUtilities" onmouseout="timer(0);">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="menuItem"
						onmouseover="onSubMenuOver(this,document.all['coordmenuForm:managerReportsMenuItem']);"
						onmouseout="onSubMenuOut(this,document.all['coordmenuForm:managerReportsMenuItem']);">
						<h:commandLink value="" action="#{pc_Coordinator.managerReports}" styleClass="menuItem"
									onmouseover="this.isClicked=false;" id="managerReportsMenuItem">
							<h:outputText value="#{property.utilities_menu_managerReports}"></h:outputText>
						</h:commandLink>
					</td>
				</tr>
			</tbody>
		</table>
		</div>
	</h:form>
	<!-- end NBA186 -->
	</f:view>
</body>
</html>
