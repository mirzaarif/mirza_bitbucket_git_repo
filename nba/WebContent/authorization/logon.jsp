<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>Logon</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
	<!--
		var authConfirmed = false;
		var changePassword = false;
		function completeLogon(){
			try{
				// store elements for CTI registration
				top.notificationClientFrame.username = logonForm.item("logonForm:usernamefld").value;
				top.notificationClientFrame.password = logonForm.item("logonForm:passwordfld").value;
			} catch(err){
				top.reportException(err, "logon - completeLogon");
			}
			document.all["logonForm:submit"].isClicked = "false";
			document.all["logonForm:submit"].click();
		}
		
		function doAuth(){
			if(!authConfirmed && !changePassword){
				top.controlFrame.location.href="<%=path%>/authorization/checkAuth.jsp";
				return false;
			} else {
				return true;
			}
		}
		
		function isChangePassword(){
			changePassword = true;
		}
		
		function checkTimeOut(){
		    try{
				var query = location.search.substring(1);
				if(query != null && query.indexOf("sessionTimedOut=true") > -1){
				    document.getElementById("Messages").innerHTML = "Session Timed Out";
				    top.showWindow('<%=path%>','faces/error.jsp', this);
				}
		    } catch(err){
		    }
		}
		
		function openAbout() {
					var path = "../init/aboutForm.jsp";
				    window.open(path,'','status=no,resizable=yes,scrollbars=yes,width=540,height=530');
				}
				

	//-->
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>

</head>
  
<body class="darkBlueBody" onload="logonForm.item('logonForm:usernamefld').focus(); top.setContentAreaSize(); checkTimeOut();">
	<f:view>
		<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
			<tbody>
			<tr class="logonPane" id="contentArea">
				<TD colspan="3">
					<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
						<f:loadBundle var="details"	basename="com.csc.fs.accel.ui.config.nba.ApplicationDetails" />
					<h:form id="logonForm" onsubmit="return doAuth();">
						<div style="position: absolute; top: 1; left:1">
							<IMG src="images/CustLogo.jpg">
						</div>
						<table border="0" cellpadding="0" cellspacing="0" style="position: absolute; right: 15; top: 10">
							<tr style="text-align: right; position: absolute;">
								<td>
									<h:outputText rendered="true" escape="false" style="font-family:Arial;font-size:30px;" styleClass="productTitle" value="#{details.Application_Name}"></h:outputText>
								</td>
							</tr>
						</table>
						<table id="logonFormTable" align="center" border="0" cellpadding="0" cellspacing="0" width="370" height="250" style="position: relative; top: 220;">
							<tbody>
							</TR>
							<tr style="text-align:left;" >
								<TD class="textLabel" colspan="4" style="text-align:center;">
									<div style="vertical-align: baseline;font-size: 13;">Please enter your User Name and Password to Logon.
									</div>
								</TD>
							</tr>
							<TR style="height:35px">
								<TD valign="bottom" >
									<h:outputText styleClass="textRequired" style="width: 140px;" rendered="true" value="#{bundle.logon_username}"></h:outputText>
								</TD>
								<TD valign="bottom">
									<h:inputText styleClass="textInput" id="usernamefld" style="width: 140px" size="30" value="#{pc_Logon.username}">
									<f:validateLength minimum="1" maximum="30"></f:validateLength>
									</h:inputText>
								</TD>
								
							</TR>
							<TR>
								<TD>
									<h:outputText styleClass="textRequired" style="width: 140px;" rendered="true" value="#{bundle.logon_password}"></h:outputText>
								</TD>
								<TD>
									<h:inputSecret styleClass="textInput" id="passwordfld" style="width: 140px" value="#{pc_Logon.password}" maxlength="8"></h:inputSecret>
								</TD>
							</TR>
							<TR>
								<TD align="right">
									<h:commandButton id="submit" styleClass="button"  value="#{bundle.logon_submit}" action="#{pc_Logon.submit}" />
									<td align="left"><h:commandButton id="changepassword" styleClass="button"  style="width: 140px;" value="#{bundle.logon_change_password}" action="#{pc_Logon.changepassword}" onclick="isChangePassword()" />
									</td>
								</TD>
							</TR>
							<TR >
							<td>
							</td>
								<TD align="left" >
									<h:commandButton styleClass="button" style="width: 140px;" value="#{bundle.logon_cancel}" onclick="return parent.closeApplication();" />
								</TD>
							</TR>
							<TR>
							<td>
							</td>
								<TD align="left" valign="top;" >
									&nbsp;&nbsp;&nbsp;<a id="aboutAnchor" onclick="openAbout();" title="Software Information" style="color:blue;text-decoration:underline;text-align:right;">Software Information</a>
								</TD>
							</TR>
						</TBODY></TABLE>
					</h:form>
				<table align="left" border="0" cellpadding="0" cellspacing="0"
					style="position: absolute; bottom: 30; left: 0;">
					<tbody>
						<TR>
							<td colspan="4" align="center">
								<div style="text-align: center; vertical-align: bottom;">
									<p><h:outputText escape="false" rendered="true" value="#{details.Footnote}" styleClass="copyrightFootnote"></h:outputText></p>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				</TD>
			</TR>
		</TBODY></TABLE>		
	</f:view>
	<div id="Messages">	
	</div>
</body>
</html>
