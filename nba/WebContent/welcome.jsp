<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA025            2      Business Function Security --> 
<!-- NBA031            3      Rewrite logon/menu/status bar in HTML -->
<!-- SPR1360           3      Scroll bar is not displayed if nbA views do not fit into window -->
<!-- NBA097            4      Work Routing Reason Displayed -->
<!-- NBA118            5      Work Item Identification Project-->
<!-- NBA122            5      Underwriter Workbench Rewrite (also removed audit history of NBA044) -->
<!-- NBA127            5      Performance tuning -->
<!-- SPR3228           6      Remove "News and Tips" from Welcome Page with Foreground Color Same As Background Color of White -->
<!-- NBA213            7      Unified UI -->
<!-- NBA349		    NB-1401	  nbA IP Compliance -->

<!-- Begin NBA031 -->
<%@ page language="java" errorPage="errorpage.jsp" %>
<HTML>
<title>nbA - Welcome</title><!-- NBA097 -->
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<!-- NBA118 code deleted -->
<SCRIPT language="JavaScript" src="include/common.js" ></SCRIPT>
<!-- NBA127 - Duplicate Code Deleted -->

<!-- NBA213 - included base accel JS for key capture-->
<SCRIPT language="JavaScript" src="javascript/global/desktopComponent.js" ></SCRIPT>

<BODY class="desktopTable"  onload="top.setContentAreaSize();"> <!-- SPR1360 -->
	<table width="75%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tbody>
			<tr>
				<td class="bar">
				<div align="center" class="textTableTitle"><p></div>  <!-- SPR3228 -->
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<p class="textNewsTitle">News Flash - Message Board</p>
				<div class="textNewsStandard">Welcome to nbAccelerator<sup>�</sup></div><!-- NBA349 -->
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<hr />
				<p class="textNewsTitle">Navigation Tips</p>
				<div class="textNewsStandard">To begin:
				<ol>
					<li>Select "Get Work" or one of the other options for opening a work item.</li>
				</ol>
				When work is open:
				<ol>
					<li>Review the reason the work was received on the Status Bar.</li>
					<li>Perform the appropriate business function. Refer to the Business Process Guide for details.</li>
				</ol>
				To end:
				<ol>
					<li>In most cases after completing the work simply select "Work Completed". Refer to the Business Process Guide for details.</li>
					<li>When finished working in nbA select "Logoff".</li>
				</ol>
				</div>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- End NBA122 -->
</BODY>
<!-- End NBA031 -->
</HTML>