<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-658     NB-1301   Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench-->
<!-- SPR3103        NB-1401   Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View  -->
<!-- NBA349		    NB-1401	  nbA IP Compliance -->
<!-- NBA353		    NB-1501	  Save Draft Comments when navigating from case to case -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=	basePath%>" target="controlFrame">
<title>Call Center</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<script>
//<!--

	function toggleDebug(){
		if(top.debug == true){
			top.document.all.controlFrame.style.width=0;
			top.document.all.controlFrame.style.height=0;
			top.document.all.controlFrame.style.left=0;
			top.document.all.controlFrame.style.top=0;
			top.document.all.debugButton.style.visibility = "hidden";
			top.debug = false;
		} else {
			top.document.all.controlFrame.style.width=200;
			top.document.all.controlFrame.style.height=200;
			top.document.all.controlFrame.style.left=400;
			top.document.all.controlFrame.style.top=100;
			top.document.all.debugButton.style.visibility = "visible";
			top.debug = true;
		}
	}
		
		//SPRNBA-658 new Method
	function onShowLeft() {	
		if (top.mainContentFrame && top.mainContentFrame.contentRightFrame && top.mainContentFrame.contentRightFrame.nbaContextMenu) {
			//NBA353 code deleted	
			draftChanges = top.mainContentFrame.contentRightFrame.draftChanges;		//SPR3103		
			//Display draft items popup			
			if (draftChanges == "true") {	//SPR3103 //NBA353
				viewId="Title";
				top.showWindow(top.basePath,'faces/confirmDraftMessage.jsp?viewName='+viewId, this);
				return;
			}
		}
		top.showLeft();
	}
	
//-->
</script>
</head>
<body class="desktopPanelBody" bgcolor="black" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<f:view>
	<h:form id="callCenterForm">
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
		<f:loadBundle var="details" basename="com.csc.fs.accel.ui.config.nba.ApplicationDetails" /> <!-- FNB016 -->
		<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%">
			<tbody>
				<tr>
					<td align="left" style="height: 33px" >&nbsp;<!-- NBA349 -->
						<h:outputText rendered="true" escape="false" styleClass="productTitle" value="#{details.Application_Name}"></h:outputText><!-- NBA349 -->
						<h:outputText rendered="true" style="font-size: 8;font-variant: small-caps; color: white;vertical-align:super;"
									value="#{details.Application_Version}"></h:outputText><!-- NBA349 -->
						<h:outputText style="font-size: 8;font-variant: small-caps; color: white;vertical-align:super;" rendered="true"
									value="#{details.Application_Region}"></h:outputText><!-- NBA349 -->
						<IMG border="0" id="debug" src="javascript/menu/images/blank.gif" ondblclick="toggleDebug();" style="width: 10px;" />  <!-- NBA349 --> 
					</td>
					<td align="right">
						<div id="windowButtonBar">
							<a id="helpMenu" onclick="toggleHelpMenu('windowButtonBar');" title="Help"><IMG border="0" src="images/help_white.gif"/></a>
							<a onclick="top.restoreWindow();" title="Restore"><IMG border="0" src="images/restore.gif"/></a>
							<a onclick="onShowLeft();" title="Show Left"><IMG border="0" src="images/arrow_left.gif"/></a><!-- SPRNBA-658 -->
							<a onclick="top.showRight();" title="Show Right"><IMG border="0" src="images/arrow_right.gif"/></a>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
</f:view>
</body>
</html>
