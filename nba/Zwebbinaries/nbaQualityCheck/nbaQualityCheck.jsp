<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%@ taglib uri="http://fs.csc.com/rqa-interview" prefix="rqa" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Quality Check</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="theme/nbaStyle.css">
<script type="text/javascript">
	<!--
		var width=580;
		var height=100;

		function setTargetFrame() {
			document.forms['mainForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['mainForm'].target='';
			return false;
		}
//-->
</script>
</head>
<body>
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_QUALITY_CHECK" value="#{pc_nbaQualityCheck}" /> <!-- NBA151 -->
	<h:form id="mainForm">
		<TABLE border="0">
			<TBODY>
				<TR>
				<TR>
				<TD valign="top" width="296">
	 			 	<rqa:rqaInterview id="rqaInterview" 
				 			viewTemplate="#{pc_nbaQualityCheck.viewTemplateFile}"  
				 			value="#{pc_nbaQualityCheck.vpmsArgs}" 
				 			rqaListener="#{pc_nbaQualityCheck.performRQAAction}" 
				 			productName="QUALITYCHECK" 
				 			form="mainForm" />				 									
				</TD>
				</TR> 
				<TR>
				<TD>			
					<h:commandButton
						styleClass="button" value="OK"
						action="#{pc_nbaQualityCheck.executeOK}" immediate="true"
						disabled="#{pc_nbaQualityCheck.OKDisabled}"
						onclick="performOK();" />				
					<h:commandButton 
						styleClass="button" value="Cancel"
						action="#{pc_nbaQualityCheck.cancel}" immediate="true"
						onclick="performClose();" /> <!-- NBA151 -->
				</TD>
				</TR>	 
			</TBODY>
		</TABLE>
		<h:inputHidden id="appProposedSignatureOK" value="#{pc_nbaQualityCheck.appProposedSignatureOK}"/>
		<h:inputHidden id="replacementFormSignatureOK" value="#{pc_nbaQualityCheck.replacementFormSignatureOK}"/>
		<h:inputHidden id="checkSignedOK" value="#{pc_nbaQualityCheck.checkSignedOK}"/>
		<h:inputHidden id="pacValidationOK" value="#{pc_nbaQualityCheck.pacValidationOK}"/>
		<h:inputHidden id="modelResults" value="#{pc_nbaQualityCheck.modelResults}"/>
		</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>
</f:view>
</body>
</html>
