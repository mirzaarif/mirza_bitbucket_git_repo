<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%@ taglib uri="http://fs.csc.com/rqa-interview" prefix="rqa" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Quality Review</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="theme/nbaStyle.css">
<script type="text/javascript">
	<!--
		var width=580;
		var height=100;

		function setTargetFrame() {
			document.forms['mainForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['mainForm'].target='';
			return false;
		}
//-->
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="mainBody" onload="popupInit();">
<f:view>
	<br>
	<h:form id="mainForm">
		<h:dataTable border="2" value="#{pc_nbaQualityReview.questions}" var="question"
			styleClass="rqa" headerClass="header">
			<h:column>
				<f:facet name="header">
					<B><h:outputText value="#{pc_nbaQualityReview.colLabels[0]}"/></B>
				</f:facet>
				<h:outputText value="#{question.text}"/>
			</h:column>
			<h:column>
				<f:facet name="header">
					<B><h:outputText value="#{pc_nbaQualityReview.colLabels[1]}"/></B>
				</f:facet>
				<h:outputText value="#{question.response}"/>
			</h:column>
		</h:dataTable>
		<br>
		<h:commandButton
			styleClass="button" value="OK"
			action="#{pc_nbaqualityReview.executeOK}" immediate="true"
			onclick="javascript:window.close();" />
	</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>
</f:view>
</body>
</html>
