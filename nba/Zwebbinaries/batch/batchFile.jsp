
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
String queryString = request.getQueryString();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>" target="control">
<title>Batch File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
	var context = '<%=path%>';
	function getArgs() {
		try {
			var args = new Object();
			var query = location.search.substring(1);
			var pairs = query.split("&");
			for (var i = 0; i < pairs.length; i++) {
				var pos = pairs[i].indexOf('=');
				if (pos == -1) {
					continue;
				}
				var argname = pairs[i].substring(0, pos);
				var value = pairs[i].substring(pos + 1);
				args[argname] = value;
			}
			return args;
		}catch(er){
			reportException(er, "common - getArgs");
		}
	}
	function initialize(){
		var args = getArgs();
		if(args['embedded'] != 'true'){
			document.body.className = "desktopBody";
			element = document.getElementById("TitleBar");
			if(element != null){
				element.style.display = "inline";
				element.style.visibility = "visible";
			}
			element = document.getElementById("mainTitle");
			if(element != null){
				element.className = "textMainTitleBar";
			}
			element = document.getElementById("mainTitleHelp");
			if(element != null){
				element.className = "textMainTitleBar";
			}
		} else {
			document.body.className = "updatePanel";
		}
		selectBatchView('batch/file/processes.faces', document.getElementById("batchOption"));
	}	
	
	var lastSelection = null;
	
	function selectBatchView(page, item){
		try{
			if(lastSelection != null){
				lastSelection.style.fontWeight = "normal";
			}
			if(item!= null){
				item.style.fontWeight = "bold";
				lastSelection = item;
			}
		} catch(err){
		}
		loadPage('<%=basePath%>' + page);
		return false;
	}

	//-->
	</script>
<script type="text/javascript" src="javascript/loader.js"></script>	
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="initialize();">
<f:view>
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody>
			<tr class="whiteBody" >
				<td>
				<table width="100%" height="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="textTitleBar" id="mainTitle" height="27px">
							<h:outputText value="Batch - "></h:outputText>
							<a class="textTitleDetail" href="#" onclick="return selectWorkView('batch/file/processes.faces', this);" id="batchOption"><h:outputText value="Manual"></h:outputText></a>
							<span class="textTitleDetail"> | </span>
						</td>
						<TD align="right" class="textTitleBar" id="mainTitleHelp"><span class="textTitleDetail"></span></TD>
					</tr>
					<tr>
						<td colspan="2"><iframe name="file" src="" class="batchFile" height="100%" width="100%" frameborder="0" scrolling="no"></iframe></td>
					</tr>
				</table>
			</td>
			</tr>
		</tbody>
	</table>
</f:view>
</body>
</html>
