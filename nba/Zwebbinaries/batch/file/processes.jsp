<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<head>
<base href="<%=basePath%>">
<title>Processes</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript">
	<!--
	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['batchProcessesForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['batchProcessesForm'].target='';
		return false;
	}
	function launchPolicy(){		
		if(parent != null && parent.policySelection != null){
			var currPolicySel = parent.policySelection.location.href;
			parent.policySelection.location.href = currPolicySel; 
		}
	}

	function highlightRow(){
		top.scrollHighlight(document.getElementById('batchProcessesDiv'), 
					document.getElementById('batchProcessesForm:selectedRow').innerHTML);
	}	
	//-->
</script>

</head>
<body class="whiteBody"
	onload="filePageInit();highlightRow()">
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="batchProcessesForm">
		<h:outputText value="#{pc_BatchDesktop.refresh}" style="display:none;"></h:outputText>
		<table width="100%" align="center" cellpadding="0" cellspacing="0"
			border="0">
			<tbody>
				<TR>
					<td colspan="2">
						<TABLE width="100%" border="1" class="textMainTitleBar">
							<tr style="border: none">
								<td style="border: none">
									<h:outputText value="#{bundle.batch_processes}"></h:outputText>
								</td>
							</tr>
						</TABLE>
					</td>
				</TR>			
				<tr valign="bottom">
					<td width="100%" class="headerRow"><h:dataTable styleClass="header" cellpadding="0"
						width="100%" cellspacing="0" border="0" headerClass="headerTable">
						<h:column>
							<f:facet name="header">
								<h:commandLink id="processCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_BatchDesktop.sortColumn}">
									<h:outputText style="width:305px;" value="#{bundle.batch_processName}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="runningCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();">
									<h:outputText value="#{bundle.batch_processStatus}" />
								</h:commandLink>
							</f:facet>
						</h:column>
					</h:dataTable></td>
				</tr>
				<tr>
					<td>
					<div class="divTable" style="height: 287px" id="batchProcessesDiv"><h:dataTable
						id="dTableInbox" var="currentRow" columnClasses="column"
						rowClasses="#{pc_BatchDesktop.rowStyles}" headerClass="header"
						styleClass="complexTable" cellpadding="2" cellspacing="0"
						width="100%" border="1" binding="#{pc_BatchDesktop.table}"
						value="#{pc_BatchDesktop.data}">
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" action="#{pc_BatchDesktop.selectRow}">
								<h:outputText style="width:305px;"
									value="#{currentRow.processDescription}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" action="#{pc_BatchDesktop.selectRow}">
								<h:graphicImage url="images/run.gif"
									style="border:0px;" rendered="#{currentRow.running}"/>
								<h:graphicImage url="images/pause.gif"
									style="border:0px;" rendered="#{!currentRow.running}"/>
							</h:commandLink>
						</h:column>
					</h:dataTable></div>

					</td>
				</tr>
				<tr>
					<td align="right">
						<h:commandButton type="submit" disabled="#{pc_BatchDesktop.selectedProcess == null || pc_BatchDesktop.selectedProcess.running}"
						value="#{bundle.batch_run}" styleClass="button" action="#{pc_BatchDesktop.startProcess}" onclick="setTargetFrame();"/>
					</td>
				</tr>
			</tbody>
		</table>
		<h:outputText style="display:none;" id="selectedRow" value="#{pc_BatchDesktop.selectedRowIndex}"></h:outputText>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
