<%-- CHANGE LOG
Audit Number   Version   Change Description
NBA411         NB-1601   nbA Mass Case Manager Reassignment
--%> 

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:panelGroup id="searchHeader" styleClass="ovDivTableHeader" style="width: 1235px">
	<h:panelGrid columns="10" styleClass="ovTableHeader" style="width: 1215px" cellspacing="0"
		columnClasses="ovColHdrIcon,ovColHdrText100,ovColHdrText105,ovColHdrText155,ovColHdrText185,ovColHdrText155,ovColHdrText105,ovColHdrText155,ovColHdrText120,ovColHdrText125">
		<h:commandLink id="searchHdrCol1" value="" style="width: 100%; height: 100%" styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol1}"
			actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol2" value="#{property.massReassignmentCreateDate}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol2}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol3" value="#{property.massReassignmentPriority}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol3}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol4" value="#{property.massReassignmentContract}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol4}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol5" value="#{property.massReassignmentName}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol5}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol6" value="#{property.massReassignmentWorkType}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol6}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol7" value="#{property.massReassignmentAgency}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol7}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol8" value="#{property.massReassignmentNewMgr}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol8}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol9" value="#{property.massReassignmentNewQueue}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol9}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol10" value="#{property.massReassignmentSuspended}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_massReassignmentResultTable.sortedByCol10}" actionListener="#{pc_massReassignmentResultTable.sortColumn}" immediate="true" />
	</h:panelGrid>
</h:panelGroup>
<h:panelGroup id="searchData" styleClass="ovDivTableData" style="width: 1235px; background-color: white; height: 425px">
	<h:dataTable id="searchTable" styleClass="ovTableData" style="width: 1215px" cellspacing="0" binding="#{pc_massReassignmentResultTable.dataTable}"
		value="#{pc_massReassignmentResultTable.results}" var="result" rowClasses="#{pc_massReassignmentResultTable.rowStyles}"
		columnClasses="ovColIconTop,ovColText100,ovColText105,ovColText155,ovColText185,ovColText155,ovColText105,ovColText155,ovColText120,ovColText125">
		<h:column>
			<h:commandLink id="searchCol1" styleClass="ovFullCellSelect" style="width: 100%; height: 100%;" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true">
				<h:graphicImage url="images/comments/lock-closed-forlist.gif" title="#{result.lockedBy}" styleClass="ovViewIcon#{result.locked}"
					style="border-width: 0px; margin-top: -3px;" />
			</h:commandLink>
		</h:column>
		<h:column>
			<h:commandLink id="searchCol2" value="#{result.createdDate}" styleClass="ovFullCellSelect" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol3" value="#{result.priority}" styleClass="ovFullCellSelect" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol4" value="#{result.contractNumber}" styleClass="ovFullCellSelect" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol5" value="#{result.name}" styleClass="ovFullCellSelect" style="word-wrap: break-word;width:100%;" action="#{pc_massReassignmentResultTable.selectRow}" immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol6" value="#{result.workTypeTranslation}" styleClass="ovFullCellSelect" style="word-wrap: break-word;width:100%;" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol7" value="#{result.agency}" styleClass="ovFullCellSelect" action="#{pc_massReassignmentResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol8" value="#{result.newMgr}" styleClass="ovFullCellSelect" style="word-wrap: break-word;width:100%;"	action="#{pc_massReassignmentResultTable.selectRow}" immediate="true"/>
		</h:column>
		<h:column>
			<h:commandLink id="searchCol9" value="#{result.newQueue}" styleClass="ovFullCellSelect" style="word-wrap: break-word;width:100%;" action="#{pc_massReassignmentResultTable.selectRow}" immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol10" value="#{result.suspended}" styleClass="ovFullCellSelect" title="#{result.suspendOriginator}" style="width: 100%; text-align: right;"
				action="#{pc_massReassignmentResultTable.selectRow}" immediate="true"/>
		</h:column>

	</h:dataTable>
</h:panelGroup>
<h:panelGroup styleClass="ovButtonBar" style="width: 1235px;">
	<h:commandButton id="btnSelectAll" value="#{property.buttonSelectAll}" styleClass="tabButtonRight" style="left: 10px"
				disabled="#{!pc_massReassignmentResultTable.selectAllButtonDisabled}" action="#{pc_massReassignmentResultTable.selectAll}" />
	<h:commandButton id="btnReassign" value="#{property.buttonReassign}" styleClass="tabButtonRight" style="left: 1035px"
				disabled="#{pc_massReassignmentResultTable.reassignButtonDisabled}" action="#{pc_massReassignmentResultTable.reassign}" />
	<h:commandButton id="btnReallocate" value="#{property.buttonReallocate}" styleClass="tabButtonRight" style="left: 1135px"
				disabled="#{pc_massReassignmentResultTable.reallocateButtonDisabled}" action="#{pc_massReassignmentResultTable.reallocate}" />
</h:panelGroup>
<h:panelGroup styleClass="ovStatusBar" style="width: 1235px;">
	<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_massReassignmentResultTable.showPrevious}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.previousPageAbsolute}" immediate="true" />
	<h:commandLink value="#{property.previousPage}" rendered="#{pc_massReassignmentResultTable.showPrevious}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.previousPage}" immediate="true" />
	<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_massReassignmentResultTable.showPreviousSet}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.previousPageSet}" immediate="true" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page1Number}" rendered="#{pc_massReassignmentResultTable.showPage1}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page1}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page1Number}" rendered="#{pc_massReassignmentResultTable.currentPage1}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page2Number}" rendered="#{pc_massReassignmentResultTable.showPage2}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page2}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page2Number}" rendered="#{pc_massReassignmentResultTable.currentPage2}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page3Number}" rendered="#{pc_massReassignmentResultTable.showPage3}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page3}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page3Number}" rendered="#{pc_massReassignmentResultTable.currentPage3}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page4Number}" rendered="#{pc_massReassignmentResultTable.showPage4}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page4}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page4Number}" rendered="#{pc_massReassignmentResultTable.currentPage4}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page5Number}" rendered="#{pc_massReassignmentResultTable.showPage5}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page5}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page5Number}" rendered="#{pc_massReassignmentResultTable.currentPage5}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page6Number}" rendered="#{pc_massReassignmentResultTable.showPage6}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page6}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page6Number}" rendered="#{pc_massReassignmentResultTable.currentPage6}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page7Number}" rendered="#{pc_massReassignmentResultTable.showPage7}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page7}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page7Number}" rendered="#{pc_massReassignmentResultTable.currentPage7}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_massReassignmentResultTable.page8Number}" rendered="#{pc_massReassignmentResultTable.showPage8}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.page8}" immediate="true" />
	<h:outputText value="#{pc_massReassignmentResultTable.page8Number}" rendered="#{pc_massReassignmentResultTable.currentPage8}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_massReassignmentResultTable.showNextSet}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.nextPageSet}" immediate="true" />
	<h:commandLink value="#{property.nextPage}" rendered="#{pc_massReassignmentResultTable.showNext}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.nextPage}" immediate="true" />
	<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_massReassignmentResultTable.showNext}" styleClass="ovStatusBarText"
		action="#{pc_massReassignmentResultTable.nextPageAbsolute}" immediate="true" />
</h:panelGroup>
