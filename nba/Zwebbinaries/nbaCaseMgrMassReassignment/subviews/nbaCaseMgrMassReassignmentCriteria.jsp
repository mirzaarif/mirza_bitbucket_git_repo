<%-- CHANGE LOG
Audit Number   Version   Change Description
NBA411         NB-1601   nbA Mass Case Manager Reassignment
--%> 

<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:panelGroup id="massReassignmentFormat" styleClass="inputFormMat" style="width:1235px">
	<h:panelGroup id="massReassignmentForm" styleClass="inputForm" style="height: 160px">

		<h:panelGroup styleClass="formTitleBar">
			<h:outputLabel value="#{property.massReassignmentSection}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="businessAreaPanelGroup" styleClass="formDataEntryTopLine">
			<h:outputLabel value="#{property.massReassignmentBussArea}:" styleClass="formLabel" />
			<h:selectOneMenu id="nbamassReassignmentCriteria_businessarea" immediate="true" value="#{pc_massReassignmentCriteria.businessArea}" styleClass="formEntryText"
				style="width: 425px" valueChangeListener="#{pc_massReassignmentCriteria.changeBusinessArea}" onchange="submit()">
				<f:selectItems id="nbamassReassignmentCriteria_businessarea_selectItems" value="#{pc_massReassignmentCriteria.businessAreas}" />
			</h:selectOneMenu>
			<h:outputLabel value="#{property.massReassignmentQueue}:" styleClass="formLabel" style="margin-left: 25px"/>
			<h:selectOneMenu id="nbamassReassignmentCriteria_queue" immediate="true" valueChangeListener="#{pc_massReassignmentCriteria.changeQueue}"
				value="#{pc_massReassignmentCriteria.queue}" styleClass="formEntryText" style="width: 425px">
				<f:selectItems id="nbamassReassignmentCriteria_queue_selectItems" value="#{pc_massReassignmentCriteria.queues}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="workTypePanelGroup" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.massReassignmentWorkType}:" styleClass="formLabel" />
			<h:selectOneMenu id="nbamassReassignmentCriteria_worktype" immediate="true" value="#{pc_massReassignmentCriteria.workType}" styleClass="formEntryText"
				style="width: 425px" valueChangeListener="#{pc_massReassignmentCriteria.changeWorkType}" onchange="resetTargetFrame();submit()">
				<f:selectItems id="nbamassReassignmentCriteria_worktype_selectItems" value="#{pc_massReassignmentCriteria.workTypes}" />
			</h:selectOneMenu>
			<h:outputLabel value="#{property.massReassignmentMgr}:" styleClass="formLabel" style="margin-left: 25px"/>
			<h:selectOneMenu id="nbamassReassignmentCriteria_mgr" styleClass="formEntryText" value="#{pc_massReassignmentCriteria.mgrQueue}"
				style="width: 425px; margin-top: 4px" valueChangeListener="#{pc_massReassignmentCriteria.changeMgr}" onchange="resetTargetFrame();submit()">
				<f:selectItems id="nbamassReassignmentCriteria_mgr_selectItems" value="#{pc_massReassignmentCriteria.mgrQueues}" />
			</h:selectOneMenu>			
		</h:panelGroup>
		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>
		<h:panelGrid id="datesPanelGrid" columns="4" cellspacing="0" cellpadding="0" styleClass="formDataEntryLine" >
			<h:column >
				<h:outputLabel value="#{property.massReassignmentFromDate}:" styleClass="formLabel" />
				<h:inputText id="From_Date" value="#{pc_massReassignmentCriteria.fromDate}" styleClass="formEntryDate" style="width: 100px"
					immediate="true">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
			</h:column>
			<h:column>
				<h:outputLabel value="#{property.massReassignmentToDate}:" styleClass="formLabel" style="margin-right:3px;" />
				<h:inputText id="To_Date" value="#{pc_massReassignmentCriteria.toDate}" styleClass="formEntryDate" style="width: 100px;margin-right:26px;" immediate="true">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
			</h:column>
		</h:panelGrid>
		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="searchButtonsPanelGroup" styleClass="formButtonBar">
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft" action="#{pc_massReassignmentCriteria.clear}"
				onclick="resetTargetFrame();" />
			<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight-1" style="left: 1125px"
				action="#{pc_massReassignmentCriteria.search}" onclick="resetTargetFrame();" disabled="#{pc_massReassignmentCriteria.searchDisabled}" />
		</h:panelGroup>
	</h:panelGroup>
</h:panelGroup>
