<%-- CHANGE LOG
Audit Number   Version   Change Description
NBA411         NB-1601   nbA Mass Case Manager Reassignment
--%> 


<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reallocate</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">

		function setTargetFrame() {
			document.forms['form_massReassignment'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_massReassignment'].target='';
			return false;
		}

		function initialize() {
			top.defaultWindowOrientation = top.FULL_SCREEN;
			top.showFullScreen();
			if (window.screen.availWidth >= 1280) {
				this.document.body.style.overflowX = "hidden";
				if (window.screen.availHeight >= 980) {
					this.document.body.style.overflowY = "hidden";
				}
			}
		}
		
	</script>
</head>
<body onload="filePageInit();initialize();" style="overflow-x: scroll; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="form_massReassignment">
		<f:subview id="nbaMassReassignmentSearchCriteria">
			<c:import url="/nbaCaseMgrMassReassignment/subviews/nbaCaseMgrMassReassignmentCriteria.jsp" />
		</f:subview>
		<h:panelGroup id="massReassign" styleClass="sectionSubheader" style="width: 1235px; height: 35px; text-indent: 0px;">
			<h:outputText value="#{property.massReassignmentMgr}:" styleClass="formLabel" />
			<h:selectOneMenu id="mgrQueue" styleClass="formEntryText" value="#{pc_massReassignmentResultTable.mgrQueue}"
				valueChangeListener="#{pc_massReassignmentResultTable.changeReassignmentMgrQueue}" style="width: 275px; margin-top: 4px">
				<f:selectItems value="#{pc_massReassignmentResultTable.mgrQueues}" />
			</h:selectOneMenu>


			<h:outputText value="#{property.massReassignmentQueue}:" styleClass="formLabel" />
			<h:selectOneMenu id="reassignToQueue" styleClass="formEntryText" value="#{pc_massReassignmentResultTable.queue}"
				valueChangeListener="#{pc_massReassignmentResultTable.changeReassignmentQueue}" style="width: 275px; margin-top: 4px">
				<f:selectItems value="#{pc_massReassignmentResultTable.queues}" />
			</h:selectOneMenu>
	
	 		<h:outputText value="#{property.massReassignmentStatus}:" styleClass="formLabel" style="margin-top: -5px;" />
	 		<h:selectOneMenu id="reassignToStatus" styleClass="formEntryText" value="#{pc_massReassignmentResultTable.status}"
				style="width: 300px; margin-top: 4px">
				<f:selectItems value="#{pc_massReassignmentResultTable.statuses}" />
			</h:selectOneMenu>


		</h:panelGroup>
		<f:subview id="nbaMassReassignmentSearchResults">
			<c:import url="/nbaCaseMgrMassReassignment/subviews/nbaCaseMgrMassReassignmentTable.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar" style="height: 35px">
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" style="left: 1135px"
				disabled="#{!pc_massReassignmentResultTable.reassignmentProcessed && !pc_massReassignmentResultTable.reallocateProcessed}" action="#{pc_massReassignmentResultTable.commit}" />
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
