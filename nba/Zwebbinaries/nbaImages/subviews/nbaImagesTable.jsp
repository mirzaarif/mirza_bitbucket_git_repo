<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA227            8      Selection List of Images to Display -->
<!-- SPR3925        NB-1101   Images List Pop-up Not Sorted Correctly on Date Column -->
<!-- NBA389			NB-1601   Contract Print Manual Form Selection Enhancement -->
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="imagesHeader" styleClass="ovDivTableHeader" style="width: 100%;">
		<h:panelGrid columns="6" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrIcon,ovColHdrText165,ovColHdrText165,ovColHdrText150,ovColHdrText50,ovColHdrText85"> <!-- NBA389 -->
			<h:commandLink id="imageIconCol" value="#{property.imageHeaderCol1}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol1}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="imageTypeCol" value="#{property.imageHeaderCol2}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol2}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="descriptionCol" value="#{property.imageHeaderCol3}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol3}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="personCol" value="#{property.imageHeaderCol4}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol4}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="packetCol" value="#{property.imageHeaderCol6}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol6}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />	<!-- NBA389 -->
			<h:commandLink id="dateCol" value="#{property.imageHeaderCol5}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_imagesTable.sortedByCol5}" actionListener="#{pc_imagesTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="imagesPanel" styleClass="ovDivTableData" style="width: 100%; height: 300px; border: 1px solid; background-color: white">
		<h:dataTable id="imagesData" styleClass="ovTableData" cellspacing="0"
				binding="#{pc_imagesTable.dataTable}" value="#{pc_imagesTable.imagesList}" var="image"
				rowClasses="#{pc_imagesTable.rowStyles}" 
				columnClasses="ovColIconTop,ovColText165,ovColText165,ovColText150,ovColText50,ovColText85" > <!-- NBA389 -->
			<h:column>
				<h:commandLink id="imageIconCol" styleClass="ovViewIcon" action="#{pc_imagesTable.actionSelectImageIcon}" onmousedown="setTargetFrame();"
								style="border-width: 0px; margin-top: 0px;" title="#{image.message}" immediate="true">
					<h:graphicImage url="images/link_icons/documents.gif" style="border:none;"></h:graphicImage>
				</h:commandLink>
			</h:column>
			<h:column>
					<h:commandLink id="imageTypeCol" onmouseover="resetTargetFrame()" title="#{image.message}" action="#{pc_imagesTable.actionSelectImageRow}" immediate="true">
						<h:inputTextarea readonly="true" value="#{image.imageType}" styleClass="ovMultiLine" style="width: 165px" cols="35" />
				</h:commandLink>
			</h:column>
			<h:column>
					<h:commandLink id="descriptionCol" onmouseover="resetTargetFrame()" title="#{image.message}" action="#{pc_imagesTable.actionSelectImageRow}" immediate="true">
						<h:inputTextarea readonly="true" value="#{image.description}" styleClass="ovMultiLine" style="width: 210px" />
				</h:commandLink>
			</h:column>
			<h:column>
					<h:commandLink id="personCol" onmouseover="resetTargetFrame()" title="#{image.message}" action="#{pc_imagesTable.actionSelectImageRow}" immediate="true">
						<h:inputTextarea readonly="true" value="#{image.person}" styleClass="ovMultiLine" style="width: 165px" />
				</h:commandLink>
			</h:column>
			<h:column> <!-- NBA389 -->
					<h:selectBooleanCheckbox id="packetInd" value="#{image.packet}" styleClass="ovFullCellSelectCheckBox" style="width: 40px" disabled="#{image.enabled}" valueChangeListener="#{image.packetChanged}" onclick="submit();" immediate="true"/> <!-- NBA389 -->
			</h:column> <!-- NBA389 -->
			<h:column>
				<h:commandLink id="dateCol" onmouseover="resetTargetFrame()" title="#{image.message}" action="#{pc_imagesTable.actionSelectImageRow}" immediate="true">
						<!-- Begin SPR3925 -->
						<h:inputTextarea readonly="true" value="#{image.date}" styleClass="ovMultiLine" style="width: 85px" > 
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputTextarea> 
						<!-- End SPR3925 -->
				</h:commandLink>
			</h:column>
		</h:dataTable>
	</h:panelGroup>	
	<h:inputHidden id="imagesTableVScroll" value="#{pc_imagesTable.VScrollPosition}" />
	
</jsp:root>