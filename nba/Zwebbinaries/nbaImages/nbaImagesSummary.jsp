<%-- CHANGE LOG
	Audit Number	Version		Change Description
	NBA227            8			Selection List of Images to Display
	SPRNBA-798		NB-1401		Change JSTL Specification Level
	NBA350			NB-1401		Applet Implementation of AWD Image Viewer
	NBA389			NB-1601		Contract Print Manual Form Selection Enhancement --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
		<base href="<%=basePath%>">
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<title>Images</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link  href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
		<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="javascript/global/file.js"></script>	
		<script type="text/javascript">
		<!--	
			function setTargetFrame() {
				document.forms['formImages'].target='controlFrame';
				return false;
			}
			function resetTargetFrame() {
				document.forms['formImages'].target='';
				return false;
			}	
		//-->
		</script>
</head>
	
<body onload="filePageInit();top.hideParentWait();">	<%-- NBA350 --%>	<%-- NBA389 --%>
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_IMAGES_INFO" value="#{pc_imagesTable}" />			
		<h:form id="formImages">
			<f:subview id="imagesTable">
				<c:import url="/nbaImages/subviews/nbaImagesTable.jsp" />
			</f:subview>
			<h:panelGroup styleClass="ovButtonBar" style="width: 100%;">
				<h:commandButton id="btnCloseAll" value="#{property.buttonCancel}" styleClass="ovButtonLeft" style="margin-left: 20px;" action="#{pc_imagesTable.clearImagesSelection}"/>
				<h:commandButton id="btnCommitImages" value="#{property.buttonCommit}" styleClass="ovButtonRight-1" action="#{pc_imagesTable.commit}" disabled="#{!pc_imagesTable.checkboxChanged}" onclick="resetTargetFrame();top.showParentWait();"/> <%-- NBA389 --%>						
				<h:commandButton id="btnOpenImages" value="#{property.buttonOpen}" styleClass="ovButtonRight" action="#{pc_imagesTable.displaySelectedImages}" disabled="#{pc_imagesTable.noRowSelected}" onclick="setTargetFrame();"/>						
			</h:panelGroup>			
		</h:form>	
		<div id="Messages" style="display:none">	<%-- NBA389 --%>
			<h:messages />	<%-- NBA389 --%>
		</div>	<%-- NBA389 --%>
</f:view>
</body>
</html>			
