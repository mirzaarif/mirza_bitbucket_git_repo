<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA227            8      Selection List of Images to Display -->
<!-- SPR3715		NB-1301	  Modify Logic to Open Image Viewer in NbaBeanBase to Minimum Elemental Data -->
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String query = request.getQueryString();
boolean closeWindow = (query != null && query.indexOf("close=true") >= 0);
boolean closeAllAndOpen = (query != null && query.indexOf("closeAllAndOpen=true") >= 0); //NBA212
boolean activate = (query != null && query.indexOf("activate=true") >= 0); //NBA212
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		function refreshInbox(){
			if(top.mainContentFrame.desktopMenu != null){
				top.mainContentFrame.desktopMenu.location.href = top.mainContentFrame.desktopMenu.location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
		}
		function refreshImageSummary() {
			if (top.mainContentFrame.contentRightFrame !=null) {  // SPR3715
				if (top.mainContentFrame.contentRightFrame.nbaContextMenu !=null) {  // SPR3715
					if (top.mainContentFrame.contentRightFrame.nbaContextMenu.imagesSummary !=null) {
						top.mainContentFrame.contentRightFrame.nbaContextMenu.refreshImageSummary();
					}
				}  // SPR3715
			}  // SPR3715		
		}		
		function process(){
			if(top.lastButtonClicked != null){
				try{
					top.lastButtonClicked.isClicked="false";
				}catch(err){
				}
			}
			var data = document.all.ImageDefinition.innerHTML;
			data = data.replace(/(&lt;)/g,'<');
			data = data.replace(/(&gt;)/g,'>');
			if ('<%=closeWindow%>' == 'true'){
				top.ImgViewer.closeImages(data);
			} else if ('<%=closeAllAndOpen%>' == 'true'){
				top.ImgViewer.reopenImages(data);
			}else if ('<%=activate%>' == 'true'){
				top.ImgViewer.activateImages(data); 
			}else {
				top.ImgViewer.showImages(data);
			}			
			refreshInbox(); 			
		}
	</script>
</head>
<body onload="refreshImageSummary();process();"> 
<f:view>
	<h:outputText id="ImageDefinition" 
		value="#{pc_Desktop.ids['IMAGEATTRIB']}" style="display:none;"></h:outputText>
</f:view>
</body>
</html>
