
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA172           	7        	Credit Card Payment Rewrite -->
<!-- NBA213 		  		7		   	Unified User Interface -->
<!-- SPR1613			   	8	  		Some Business Functions should be disabled on an Issued Contract  --> 
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Credit Card Payment</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/jquery.js"></script><!-- NBA317 -->
<script type="text/javascript" src="include/creditCard.js"></script><!-- NBA317 -->
<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011		
	
		 //NBA317 code deleted
        
		//begin NBA317
		function formatCreditCardNumber(subviewID,field,hiddenField) {
			return formatCardNumber('form_creditCardPayment',subviewID,field,hiddenField);
		}		
			
		function processCreditCardInformation(subviewID,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID){
			var formID = 'form_creditCardPayment';
			var errorMessageDiv = 'Messages';
			var skipCreditCardValidation = top.ccTokenizationFrame.isSkipCCValidation();
			var tokenizationServiceURL = top.ccTokenizationFrame.getTokenizationURL();
			var tokenizationServiceRequest = top.ccTokenizationFrame.getTokenizationRequest();
			processCreditCardTokenization(formID,subviewID,skipCreditCardValidation,tokenizationServiceURL,tokenizationServiceRequest,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv);
		}
		
		function authorizeCommit(formID,subviewID,enable){
			document.getElementById('form_creditCardPayment:btnCommit').disabled = !enable;
		}
		//end NBA317 
		
		//begin NBA213
		function resetTargetFrame() {
			document.forms['form_creditCardPayment'].target='';
			return false;
		}

		function setTargetFrame() {
			document.forms['form_creditCardPayment'].target='controlFrame';
			return false;
		}	
		//end NBA213	
	</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CREDIT_CARD_PAYMENT" value="#{pc_creditCardPayment}" />
	<h:form id="form_creditCardPayment">
		<DIV class="inputFormMat" style="height: 350px">
		<DIV class="inputForm" style="height: auto"><f:subview id="ccPaymentInfo">
			<c:import url="/nbaCreditCardPayment/subviews/nbaCreditCardPaymentInfo.jsp" />
		</f:subview></DIV>
		</DIV>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:panelGroup styleClass="tabButtonBar">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="formButtonLeft" disabled="#{pc_creditCardPayment.disableRefresh}"
					action="#{pc_creditCardPayment.refresh}" />
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_creditCardPayment.commit}"
					disabled="#{pc_creditCardPayment.auth.enablement['Commit'] ||
							    pc_creditCardPayment.notLocked ||
								pc_creditCardPayment.disableCommit ||
								pc_creditCardPayment.issued}" 
					onclick="setTargetFrame()"								
					styleClass="formButtonRight" />	<!-- NBA213 SPR1613 -->
			</h:panelGroup>
		</h:panelGroup>
		<!-- begin NBA317  -->
		<h:panelGroup>			
			<h:inputHidden id="guid" value="#{pc_creditCardPayment.guid}"></h:inputHidden>
			<h:inputHidden id="txnDate" value="#{pc_creditCardPayment.txnDate}"></h:inputHidden>
			<h:inputHidden id="txnTime" value="#{pc_creditCardPayment.txnTime}"></h:inputHidden>
		</h:panelGroup>
		<!-- end NBA317  -->
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>

</html>
