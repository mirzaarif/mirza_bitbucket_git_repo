<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA172           7       Credit Card Payment Rewrite -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formTitleBar">
		<h:outputLabel id="createCCPTitle" value="#{property.createCCPTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="paymentTypePGroup" styleClass="formDataEntryTopLine">
		<h:outputText id="pType" value="#{property.ccpPaymentType}" styleClass="formLabel" />
		<h:selectOneMenu id="pTypeDD" value="#{pc_creditCardPayment.paymentType}" styleClass="formEntryText" style="width: 465px;" rendered="#{!pc_creditCardPayment.inforcePayment}">
			<f:selectItems id="pTypeList" value="#{pc_creditCardPayment.paymentTypeList}" />
		</h:selectOneMenu>
		<h:selectOneMenu id="iPTypeDD" value="#{pc_creditCardPayment.paymentType}" styleClass="formEntryText" style="width: 465px;" rendered="#{pc_creditCardPayment.inforcePayment}">
			<f:selectItems id="ipTypeList" value="#{pc_creditCardPayment.inforcePaymentTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="ccpAmount" styleClass="formDataEntryLine">
		<h:outputText id="chargeAmt" value="#{property.ccpAmount}" styleClass="formLabel" />
		<h:inputText id="chargeAmtEF" value="#{pc_creditCardPayment.amount}" disabled="#{pc_creditCardPayment.disableAmount}" styleClass="formEntryText"
			style="width: 170px;">
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
		</h:inputText>
		<h:outputText id="effDate" value="#{property.ccpDate}" styleClass="formLabel" style="width: 70px;" />
		<h:inputText id="effDateEF" value="#{pc_creditCardPayment.effectiveDate}" styleClass="formEntryText" style="width: 90px; margin-left: 10px;">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>

	</h:panelGroup>
	<h:panelGroup id="ccTypePGroup" styleClass="formDataEntryLine">
		<h:outputText id="ccType" value="#{property.ccpCardType}" styleClass="formLabel" />
		<h:selectOneMenu id="ccTypeDD"
			value="#{pc_creditCardPayment.creditCardType}"
			styleClass="formEntryText" style="width: 170px;"
			onchange="processCreditCardInformation('ccPaymentInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');">
			<!-- NBA317 -->
			<f:selectItems id="ccTypeList"
				value="#{pc_creditCardPayment.creditCardTypeList}" />
		</h:selectOneMenu>
		<h:inputHidden id="ccLabel" value="#{pc_creditCardPayment.selectedCardLabel}"></h:inputHidden><!-- NBA317 -->
	</h:panelGroup>
	<h:panelGroup id="cardNumberPGroup" styleClass="formDataEntryLine">
		<h:outputText id="cardNumber" value="#{property.ccpCardNo}" styleClass="formLabel" />
		<h:inputText id="cardNumberEF"
			value="#{pc_creditCardPayment.cardNumberFormatted}"
			binding="#{pc_creditCardPayment.uiCardNumber}"
			styleClass="formEntryText" style="width: 170px;"
			onchange="formatCreditCardNumber('ccPaymentInfo','cardNumberEF','cardNumberEFHidden');processCreditCardInformation('ccPaymentInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');" />
		<!-- NBA317 -->
		<h:inputHidden id="cardNumberEFHidden" value="#{pc_creditCardPayment.creditCardNo}"></h:inputHidden><!-- NBA317 -->	
		<h:outputText id="cardNumberV" value="#{property.ccpVerifyCardNo}" styleClass="formLabel" style="width: 125px;" />
		<h:inputText id="cardNumberVEF"
			value="#{pc_creditCardPayment.verifyCardNumberFormatted}"
			binding="#{pc_creditCardPayment.uiVerifyCardNumber}"
			styleClass="formEntryText" style="width: 170px;"
			onchange="formatCreditCardNumber('ccPaymentInfo','cardNumberVEF','cardNumberVEFHidden');processCreditCardInformation('ccPaymentInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');" />
		<!-- NBA317 -->
		<h:inputHidden id="cardNumberVEFHidden" value="#{pc_creditCardPayment.verifyCreditCardNo}"></h:inputHidden><!-- NBA317 -->
	</h:panelGroup>
	<h:panelGroup id="expirationPGroup" styleClass="formDataEntryLine">
		<h:outputText id="exp" value="#{property.ccpCardExpiration}" styleClass="formLabel" />
		<h:selectOneMenu id="expMonthDD" value="#{pc_creditCardPayment.expirationMonth}" styleClass="formEntryTextFull" style="width: 170px">
			<f:selectItems id="expMonthList" value="#{pc_creditCardPayment.expirationMonthList}" />
		</h:selectOneMenu>
		<h:outputText id="expMonth" value="#{property.ccpExpirationMonth}" styleClass="formLabel" style="width: 70px;" />
		<h:selectOneMenu id="expYearDD" value="#{pc_creditCardPayment.expirationYear}" styleClass="formEntryTextFull"
			style="width: 75px; margin-left: 10px;">
			<f:selectItems id="expYearList" value="#{pc_creditCardPayment.expirationYearList}" />
		</h:selectOneMenu>
		<h:outputText id="expYear" value="#{property.ccpExpirationYear}" styleClass="formLabel" style="width: 65px; margin-left: -5px;" />
	</h:panelGroup>
	<h:panelGroup id="namePGroup" styleClass="formDataEntryLine">
		<h:outputText id="nameCard" value="#{property.ccpNameOnCard}" styleClass="formLabel" />
		<h:inputText id="nameCardEF" value="#{pc_creditCardPayment.nameOnCard}" styleClass="formEntryText" style="width: 465px" maxlength="39" />
	</h:panelGroup>
	<h:panelGroup id="billingAddressPGroup" styleClass="formDataEntryLine">
		<h:outputText id="billingAdd" value="#{property.ccpBillingAddress}" styleClass="formLabel" />
		<h:inputText id="billingAddEF" value="#{pc_creditCardPayment.billingAddress}" styleClass="formEntryText" style="width: 465px" maxlength="40" />
	</h:panelGroup>
	<h:panelGroup id="ccCityPGroup" styleClass="formDataEntryLine">
		<h:outputText id="city1" value="#{property.ccpCity}" styleClass="formLabel" />
		<h:inputText id="city1EF" value="#{pc_creditCardPayment.city}" styleClass="formEntryText" style="width: 130px;" maxlength="30" />
		<h:outputText id="state1" value="#{property.ccpStateProvince}" styleClass="formLabel" style="width: 120px;" /> 
		<h:selectOneMenu id="state1DD" value="#{pc_creditCardPayment.stateProvince}" styleClass="formEntryTextFull" style="width: 215px;">
			<f:selectItems id="ccState1List" value="#{pc_creditCardPayment.stateProvinceList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid id="ccCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort" columns="3" cellpadding="0" cellspacing="0"
		style="padding-top: 6px;">
		<h:column>
			<h:outputText id="ccZipCode" value="#{property.ccpCode}" styleClass="formLabel" />
		</h:column>
		<h:column>
			<h:selectOneRadio id="pinsZipRB" value="#{pc_creditCardPayment.zipPostalType}" layout="pageDirection" styleClass="radioLabel"
				style="width: 70px;margin-left: -5px;">
				<f:selectItems id="ccZipTypeCodes" value="#{pc_creditCardPayment.zipPostalTypeList}" />
			</h:selectOneRadio>
		</h:column>
		<h:column>
			<h:inputText id="zipEF" value="#{pc_creditCardPayment.zipCode}" styleClass="formEntryText" style="width: 100px;">
				<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}" /> <!-- SPRNBA-493 -->
			</h:inputText>

			<h:inputText id="postalEF" value="#{pc_creditCardPayment.postalCode}" disabled="#{pc_creditCardPayment.zipCodeCheck}" styleClass="formEntryText"
				style="width: 100px;" />
		</h:column>
	</h:panelGrid>

</jsp:root>
