<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>


<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Confirm</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var titleName = null;
		var width=360;
		var height=140;
	//-->
	</script>
<script type="text/javascript">
	<!--
		function initTitle(){
			try{
				titleName = document.getElementById("confirmationTitle").innerHTML;
			}catch(err){
			}
		}
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="initTitle(); popupInit();this.ok.focus();">
<f:view>
	<div style="display:none">
		<h:outputText id="confirmationTitle" value="#{pc_Confirm.title}" styleClass="textLabel"> </h:outputText>
	</div>
	<h:form id="confirmationForm">
	<table width="350" cellpadding="0" cellspacing="0">
		<tbody>
			<tr >
				<td colspan="2" class="text">
					<div style="height:86;overflow: auto;">
						<h:outputText value="#{pc_Confirm.message}" styleClass="textLabel">
						</h:outputText>
					</div>
				</td>
			</tr>
			<tr valign="bottom">
				<td align="left">
					<h:commandButton styleClass="button" value="#{pc_Confirm.noLabel}"
						action="#{pc_Confirm.no}" style="width:120"/>
				</td>
				<td align="right">
					<h:commandButton styleClass="button" value="#{pc_Confirm.yesLabel}"
						action="#{pc_Confirm.yes}" style="width:120"/>
				</td>
			</tr>
		</tbody>
	</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>	
</f:view>
</body>
</html>
