<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA177           7         Change Scan Type Rewrite -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Launch Desktop</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		function refreshDesktop(){
			top.mainContentFrame.location.href = top.mainContentFrame.location.href;
		}
	</script>
</head>
<body onload="refreshDesktop();">
	<f:view>
	</f:view>
</body>
</html>
