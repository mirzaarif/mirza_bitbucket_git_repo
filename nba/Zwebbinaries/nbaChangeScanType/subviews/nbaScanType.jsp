<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA177           7       Change Scan Type Rewrite -->
<!-- NBA213           7       Unified User Interface -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDataEntryTopLine" style="height: 50px;">
		<h:outputText value="#{property.chngeScanType}" styleClass="formLabel" />
		<h:selectOneMenu styleClass="formEntryText" value="#{pc_changeScanType.workType}" valueChangeListener="#{pc_changeScanType.changeWorkType}"
			onchange="resetTargetFrame();submit()" rendered="#{!pc_changeScanType.sourceSelected}"  style="width: 475px;"><!-- NBA213 -->
			<f:selectItems value="#{pc_changeScanType.scanTypes}" />
		</h:selectOneMenu>
		<h:selectOneMenu styleClass="formEntryText" value="#{pc_changeScanType.workType}" rendered="#{pc_changeScanType.sourceSelected}"  style="width: 475px;"><!-- NBA213 -->
			<f:selectItems value="#{pc_changeScanType.scanTypes}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="height: 50px;">
		<h:outputText value="#{property.chngeScanStatus}" styleClass="formLabel" />
		<h:selectOneMenu value="#{pc_changeScanType.status}" styleClass="formEntryText" style="width: 475px;"><!-- NBA213 -->
			<f:selectItems value="#{pc_changeScanType.statuses}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="scanType" styleClass="buttonBar"><!-- NBA213 -->
		<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" action="#{pc_changeScanType.update}" styleClass="buttonRight" /><!-- NBA213 -->
	</h:panelGroup>

</jsp:root>
