
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA177           7       Change Scan Type Rewrite -->
<!-- NBA212           7       Content Services -->
<!-- NBA213           7       Unified User Interface -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="chngeScanTypeHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="3" styleClass="ovTableHeader" cellspacing="0" columnClasses="ovColHdrIcon,ovColHdrText210,ovColHdrText210"><!-- NBA213 -->
			<h:commandLink id="chngeScanTypeHdrCol1" value="" styleClass="ovColSortedFalse" />
			<h:commandLink id="chngeScanTypeHdrCol2" value="#{property.chngeScanTypeCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="chngeScanTypeHdrCol3" value="#{property.chngeScanTypeCol2}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="chngeScanTypeData" styleClass="ovDivTableData" style="height:150px; background-color: white;"><!-- NBA213 -->
		<h:dataTable id="chngeScanTypeTable" styleClass="ovTableData" cellspacing="0" rows="0" border="0"
			value="#{pc_changeScanType.changeScanTypeList}" var="scanType" binding="#{pc_changeScanType.dataTable}"
			rowClasses="#{pc_changeScanType.rowStyles}" columnClasses="ovColIconTop,ovColText210,ovColText210"
			style="min-height:19px;"><!-- NBA213 -->
			<h:column>
				<h:commandButton id="icon" image="#{scanType.imageUrl}" onclick="setTargetFrame()"  
						action="#{scanType.showSelectedImage}" disabled="#{scanType.medicalRequirement}"/> <!-- NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="current" value="#{scanType.currentTypeTranslation}" action="#{pc_changeScanType.selectRow}"  styleClass="ovFullCellSelect#{scanType.failedText}"/><!-- NBA213 -->
			</h:column>
			<h:column>
				<h:commandLink id="changeTo" title="#{scanType.commitError}" value="#{scanType.changedTypeTranslation}" action="#{pc_changeScanType.selectRow}"  styleClass="ovFullCellSelect#{scanType.failedText}" style="width:100%" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_changeScanType.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.previousPageAbsolute}" immediate="true" />
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_changeScanType.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.previousPage}" immediate="true" />
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_changeScanType.showPreviousSet}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.previousPageSet}" immediate="true" />
		<h:commandLink value="#{pc_changeScanType.page1Number}" rendered="#{pc_changeScanType.showPage1}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page1}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page1Number}" rendered="#{pc_changeScanType.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page2Number}" rendered="#{pc_changeScanType.showPage2}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page2}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page2Number}" rendered="#{pc_changeScanType.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page3Number}" rendered="#{pc_changeScanType.showPage3}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page3}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page3Number}" rendered="#{pc_changeScanType.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page4Number}" rendered="#{pc_changeScanType.showPage4}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page4}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page4Number}" rendered="#{pc_changeScanType.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page5Number}" rendered="#{pc_changeScanType.showPage5}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page5}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page5Number}" rendered="#{pc_changeScanType.currentPage5}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page6Number}" rendered="#{pc_changeScanType.showPage6}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page6}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page6Number}" rendered="#{pc_changeScanType.currentPage6}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page7Number}" rendered="#{pc_changeScanType.showPage7}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page7}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page7Number}" rendered="#{pc_changeScanType.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_changeScanType.page8Number}" rendered="#{pc_changeScanType.showPage8}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.page8}" immediate="true" />
		<h:outputText value="#{pc_changeScanType.page8Number}" rendered="#{pc_changeScanType.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_changeScanType.showNextSet}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.nextPageSet}" immediate="true" />
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_changeScanType.showNext}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.nextPage}" immediate="true" />
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_changeScanType.showNext}" styleClass="ovStatusBarText"
			action="#{pc_changeScanType.nextPageAbsolute}" immediate="true" />
	</h:panelGroup>
</jsp:root>
