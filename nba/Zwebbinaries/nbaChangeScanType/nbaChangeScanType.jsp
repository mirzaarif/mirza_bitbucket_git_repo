<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA177           7         Change Scan Type Rewrite -->
<!-- NBA212           7         Content Services -->
<!-- NBA213           7         Unified User Interface -->
<!-- SPRNBA-798     NB-1401   	Change JSTL Specification Level -->


<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<head>
			<base href="<%=basePath%>">
			<f:loadBundle basename="properties.nbaApplicationData" var="property" /><!-- NBA213 -->
			<title><h:outputText value="#{property.scanTypes}" /></title><!-- NBA213 -->
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" /><!-- NBA213 -->
			<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<!-- NBA213 code deleted -->
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script><!-- NBA213 -->
			<script language="JavaScript" type="text/javascript">
					var width=625;  //NBA213
					var height=455;  //NBA213
					function setTargetFrame() {
						document.forms['form_changescantype'].target='controlFrame';
						return false;
					}
					function resetTargetFrame() {
						document.forms['form_changescantype'].target='';
						return false;
					}
					
					//NBA212 code deleted
					//NBA213 code deleted
			</script>
		</head>
		<body onload="popupInit();"><!-- NBA213 -->
			<PopulateBean:Load serviceName="RETRIEVE_CHANGESCANTYPE" value="#{pc_changeScanType}" />
			<h:form id="form_changescantype">
				<f:subview id="currentChangeToTbl">
					<c:import url="/nbaChangeScanType/subviews/nbaChangeScanTypeTable.jsp" />
				</f:subview>
				<div class="inputForm" style="height: 180px">
					<h:panelGroup styleClass="formTitleBar">
					<h:outputLabel value="#{property.chngeScanTypeTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
					 <f:subview id="scantype">
						<c:import url="/nbaChangeScanType/subviews/nbaScanType.jsp" />
					</f:subview>
				</div>
		
				<!-- begin NBA213 -->
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" action="#{pc_changeScanType.cancel}" onclick="setTargetFrame()"  
						styleClass="buttonLeft"/>
					<h:commandButton id="btnApply" value="#{property.buttonApply}" action="#{pc_changeScanType.apply}" onclick="resetTargetFrame();" 
						disabled="#{pc_changeScanType.auth.enablement['Commit'] || 
									pc_changeScanType.notLocked}"
						styleClass="buttonRight-1"/>
					<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_changeScanType.commit}" onclick="setTargetFrame();" 
						disabled="#{pc_changeScanType.auth.enablement['Commit'] || 
									pc_changeScanType.notLocked}"
						styleClass="buttonRight"/>
				</h:panelGroup>
				<!-- end NBA213 -->
				<h:inputHidden id="result" value="#{pc_changeScanType.commitCompleted}" />
			</h:form>
			<!-- NBA213 code deleted -->
			<div id="Messages" style="display:none"><h:messages /></div>
		</body><!-- NBA213 -->
	</f:view><!-- NBA213 -->
</html>
