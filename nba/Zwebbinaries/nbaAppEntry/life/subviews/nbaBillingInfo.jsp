<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- SPR3377           8      Credit Card Payment Information Check Box Should Only Be Enabled For Payment Method of Credit Card (9) -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-645     NB-1301   DI Application Entry Use Credit Card Payment Check Box Does Not Copy to Billing Section -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="billingTitle" value="#{property.appEntBillingTitle}" styleClass="formSectionBar"/>
	<h:panelGroup id="paymentPGroup" styleClass="formDataEntryTopLine">
		<h:outputText id="paymentfrequency" value="#{property.appEntPaymentFrequency}" styleClass="formLabel" style="width: 140px;"/>
		<h:selectOneMenu id="paymentfrequencyDD" value="#{pc_billingInfo.frequency}" styleClass="formEntryText" style="width: 135px;" 
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" valueChangeListener="#{pc_billingInfo.firstSkipMonthDis}" onchange="handleSubmit()"
			> <!-- NBA211 --><!-- NBA175 --><!-- NBA317 -->
			<f:selectItems id="paymentFrequencyList" value="#{pc_billingInfo.frequencyList}" />
		</h:selectOneMenu>
		<h:outputText id="paymentMethod" value="#{property.appEntPmntMethod}" styleClass="formLabel" style="width: 120px;"/>
		<h:selectOneMenu id="paymentMethodDD" value="#{pc_billingInfo.paymentMethod}" styleClass="formEntryText" style="width: 190px;"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled }"  valueChangeListener="#{pc_billingInfo.checkPaymentMethodType}" onchange="handleSubmit()" immediate="true"><!-- NBA211 --><!-- SPR3377 --><!-- NBA317 -->
			<f:selectItems id="paymentMethodList" value="#{pc_billingInfo.paymentMethods}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="firstSkpMth" styleClass="formDataEntryTopLine" ><!-- begin  NBA175 -->
		<h:outputText id="firstSkpMonth" value="#{property.firstSkpMonth}" styleClass="formLabel" style="width: 140px;"/>				
		<h:inputText id="firstSkipMonth" value="#{pc_billingInfo.firstSkipMonth}" 
					 styleClass="formEntryText" style="width: 135px;" 
					 disabled="#{pc_billingInfo.firstSkipMonthDisabled || pc_nbaAppEntryNavigation.vantageBESSystem}" 
					 binding="#{pc_billingInfo.frstSkipMnth}"/> <!-- NBA247 -->
	</h:panelGroup><!-- end NBA175  -->
	<h:panelGroup id="useCCPaymentInfoPGroup" styleClass="formDataEntryTopLine" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA317 -->
		<h:selectBooleanCheckbox id="useCCPaymentInfoCB" value="#{pc_billingInfo.useCreditCardPaymentInfo}" styleClass="ovFullCellSelectCheckBox" 
			style="width: 20px;margin-left: 5px;" valueChangeListener="#{pc_billingInfo.populateCCInfo}"  onclick="resetTargetFrame();handleSubmit();"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"/><!-- NBA211 --><!-- SPR3377 --><!-- NBA317 --> <!-- SPRNBA-645 -->
		<h:outputText id="useCCPaymentInfo" value="#{property.appEntCCPaymentinfo}" styleClass="formLabel" style="width: 230px;text-align: left;"/>				
	</h:panelGroup>
	<h:panelGroup id="nameOnCardPGroup" styleClass="formDataEntryLine" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA317 -->
		<h:outputText id="nameCard" value="#{property.appEntNameOnCC}" styleClass="formLabel" style="width: 140px;"/>
		<h:inputText id="nameCardEF" value="#{pc_billingInfo.fullName}" styleClass="formEntryText" style="width: 330px;" binding="#{pc_billingInfo.fullNameComp}"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"/><!-- NBA211 --><!-- SPR3377 -->
	</h:panelGroup>
	<h:panelGroup id="ccTypePGroup" styleClass="formDataEntryLine" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA317 -->
		<h:outputText id="ccType" value="#{property.appEntCCType}" styleClass="formLabel" style="width: 140px;"/>
		<h:selectOneMenu id="ccTypeDD" value="#{pc_billingInfo.cardType}" styleClass="formEntryText" style="width: 130px;" binding="#{pc_billingInfo.cardTypeComp}"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"
			onchange="processCreditCardInformation('billingInfo',false,'ccTypeDD','ccLabel','cardNumberEFHidden','','cardNumberEF','');">
			<!-- NBA211 --><!-- SPR3377 --><!-- NBA317 -->
			<f:selectItems id="ccTypeList" value="#{pc_billingInfo.cardTypeList}" />
		</h:selectOneMenu>
		<h:inputHidden id="ccLabel" value="#{pc_billingInfo.selectedCardLabel}"></h:inputHidden><!-- NBA317 -->
		<h:outputText id="cardNumber" value="#{property.appEntCCNumber}" styleClass="formLabel" style="width: 140px;"/>
		<h:inputText id="cardNumberEF" value="#{pc_billingInfo.cardNumberFormatted}" styleClass="formEntryText" 
			style="width: 160px;" onchange="formatCreditCardNumber('billingInfo','cardNumberEF','cardNumberEFHidden');processCreditCardInformation('billingInfo',false,'ccTypeDD','ccLabel','cardNumberEFHidden','','cardNumberEF','');" binding="#{pc_billingInfo.cardNumberComp}"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"/><!-- NBA211 -->	<!-- SPR3377 --><!-- NBA317 -->	
		<h:inputHidden id="cardNumberEFHidden" value="#{pc_billingInfo.cardNumber}"></h:inputHidden>
		<h:inputHidden id="cardNumberTokenized" value="#{pc_billingInfo.ccNumberTokenized}"></h:inputHidden><!-- NBA317 -->
	</h:panelGroup>
	<h:panelGroup id="expirationPGroup" styleClass="formDataEntryLine" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA317 -->
		<h:outputText id="exp" value="#{property.appEntExp}" styleClass="formLabel" style="width: 140px;"/>
		<h:selectOneMenu id="expMonthDD" value="#{pc_billingInfo.expirationMonth}" styleClass="formEntryTextFull" style="width: 130px;" binding="#{pc_billingInfo.expirationMonthComp}"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"> <!-- NBA211 --><!-- SPR3377 -->
			<f:selectItems id="expMonthList" value="#{pc_billingInfo.expirationMonths}" />
		</h:selectOneMenu>
		<h:outputText id="expMonth" value="#{property.appEntExpMonth}" styleClass="formLabel" style="width: 100px;text-align: left;padding-left: 10px;"/>
		<h:selectOneMenu id="expYearDD" value="#{pc_billingInfo.expirationYear}" styleClass="formEntryTextFull" style="width: 95px;" binding="#{pc_billingInfo.expirationYearComp}"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_billingInfo.creditCardInfoDisabled}"> <!-- NBA211 --><!-- SPR3377 -->
			<f:selectItems id="expYearList" value="#{pc_billingInfo.expirationYears}" />
		</h:selectOneMenu>
		<h:outputText id="expYear" value="#{property.appEntExpYear}" styleClass="formLabel" style="width: 110px;text-align: left;padding-left: 10px;"/>
	</h:panelGroup>
	<h:panelGroup id="premIncPGroup" styleClass="formDataEntryLine">
		<h:selectBooleanCheckbox id="premIncCB" value="#{pc_billingInfo.premiumIncludedWithAppl}" styleClass="ovFullCellSelectCheckBox" style="width: 20px;margin-left: 5px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
		<h:outputText id="premInc" value="#{property.appEntIncludedPrem}" styleClass="formLabel" style="width: 210px;text-align: left"/>
		<h:outputText id="plannedPrem" value="#{property.appEntPlannedPrem}" styleClass="formLabel" style="width: 180px;"/>
		<h:inputText id="plannedPremEF" value="#{pc_billingInfo.plannedPremium}" styleClass="formEntryText" style="width: 150px;" disabled="#{pc_nbaAppEntryNavigation.traditional || pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA175,NBA211 -->
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="automaticPremLoanPGroup" styleClass="formDataEntryLine">
		<h:selectBooleanCheckbox id="automaticPremLoanCB" value="#{pc_billingInfo.automaticPremiumLoan}" 
								 styleClass="ovFullCellSelectCheckBox" 
								 style="width: 20px;margin-left: 5px;" 
								 disabled="#{pc_billingInfo.automaticPremiumLoanDisabled || pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.vantageBESSystem}"/><!-- NBA211, NBA247 -->
		<h:outputText id="automaticPremLoan" value="#{property.appEntAutomaticPremLoan}" styleClass="formLabel" style="width: 210px;text-align: left"/>
		<h:outputText id="plannedInitialPrem" value="#{property.appEntPlannedInitialPrem}" styleClass="formLabel" style="width: 180px;"/> 
		<h:inputText id="plannedInitialPremEF" value="#{pc_billingInfo.plannedInitialPremium}" styleClass="formEntryText" style="width: 150px;"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.traditional}"><!-- NBA211 --><!-- NBA175 -->
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
		</h:inputText>
	</h:panelGroup>
</jsp:root>