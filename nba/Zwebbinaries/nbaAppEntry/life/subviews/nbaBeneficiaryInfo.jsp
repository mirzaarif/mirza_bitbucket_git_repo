<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-396		NB-1101	  Application Entry does not allow the user to input the beneficiary distribution type -->
<!-- SPRNBA-404		NB-1101	  Need to Disable Trustee Fields for Person -->
<!-- SPRNBA-364		NB-1101	  Standard Phone Number and Social Security Number Format Should be Used and Tab Order Corrected from First to Middle Name -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- SPRNBA-554		NB-1301   Error Pop Up "Policy Type selection required please correct" Not Received -->
<!-- SPR3469		NB-1401   Party Type Radio Buttons Enabled After Save and After Submit for Beneficiary -->
<!-- NBA340         NB-1501   Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="pinsInfo" value="#{property.appEntBeneficiaryInformation}" styleClass="formSectionBar"/>
		<h:panelGrid id="beneficiaryPGrid" columns="2" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0">
			<h:column>
				<h:dataTable id="beneficiary" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_beneficiaryInfo.beneficiaryList}" 
					var="beneficiary" style="width: 520px;padding-left: 2px;" binding="#{pc_beneficiaryInfo.beneficiaryTable}">
					<h:column>
					<h:panelGroup id="beneficiaryBar" styleClass="formDataEntryLine" rendered="#{beneficiary.displayBar}">
						<f:verbatim>
								<hr id="separator1" class="formSeparator" />
						</f:verbatim>
					</h:panelGroup>
					<!-- begin NBA247 -->
					<h:panelGroup id="beneficiaryPlanGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
						<h:outputLabel id="beneficiaryPlan" value="#{property.appEntBeneficiaryPlan}" styleClass="formLabel" style="width: 115px;"/> 
						<h:selectOneMenu id="beneficiaryPlanEF" value="#{beneficiary.beneficiaryPlan}" styleClass="formEntryText" style="width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
							<f:selectItems id="beneficiaryPlanList" value="#{beneficiary.beneficiaryPlanList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<!-- end NBA247 -->
					<h:panelGroup id="beneficiaryPGroup" styleClass="formDataEntryLine">
						<h:outputLabel id="beneficiaryType" value="#{property.appEntBeneficiaryType}" styleClass="formLabel" style="width: 115px;"/> 
						<h:selectOneMenu id="beneficiaryTypeEF" value="#{beneficiary.beneficiaryType}" styleClass="formEntryText" style="width: 100px;"
							onchange="resetTargetFrame();submit();" valueChangeListener="#{beneficiary.beneficiaryTypeChanged}" immediate="true"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211, SPRNBA-396 -->
							<f:selectItems id="beneficiaryTypeList" value="#{beneficiary.beneficiaryTypeList}" />
						</h:selectOneMenu>
						<h:outputLabel id="beneficiaryBlank" value="" styleClass="formLabel" style="width: 20px;" />
						<h:selectBooleanCheckbox id="irrevocableCheck" value="#{beneficiary.irrevocable}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;" 
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 --> 
						</h:selectBooleanCheckbox>
						<h:outputLabel id="irrevocableCheckLabel" value="#{property.appEntIrrevocable}" styleClass="formLabel" style="width: 80px;text-align: left;" />
						<h:outputLabel id="delBeneficiaryBlank" value="" styleClass="formLabel" style="width: 40px;" /><!-- NBA175 -->
						<h:selectBooleanCheckbox id="deleteBeneficiary" value="#{beneficiary.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 50px; width: 25px;" disabled="#{beneficiary.disabled}"
						valueChangeListener="#{beneficiary.delPartyAddrDis}" onclick="resetTargetFrame();submit()" /><!-- NBA175 -->
						<h:outputText id="delBeneficiary" value="#{property.appEntDelBenificiary}" styleClass="formLabel" style="position: relative;left: 50px; width: 120px;"/><!-- NBA175 -->
					</h:panelGroup>
					<h:panelGroup>
						<h:outputText value="" styleClass="formLabel" style="width: 115px;"></h:outputText>
						<h:selectOneRadio id="selectRadioGroup" value="#{beneficiary.partyType}" layout="lineDirection" styleClass="radioLabel" style="width:180px;margin-top: -5px;" onclick="resetTargetFrame();submit();"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled  || beneficiary.personOrOrg}"> <!-- NBA211 --><!-- NBA175 --><!-- SPR3469 -->
								<f:selectItem id="individual" itemValue="1" itemLabel="#{property.appEntPerson}"/>
								<f:selectItem id="group" itemValue="2" itemLabel="#{property.appEntCorporation}"/>
						</h:selectOneRadio>							
					</h:panelGroup>		
					<h:panelGroup id="pinsNamePGroup" styleClass="formDataEntryLine" rendered="#{beneficiary.person}">
						<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="firstNameEF" value="#{beneficiary.demographics.firstName}" styleClass="formEntryText" style="width: 90px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
						<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
						<h:inputText id="middelNameEF" value="#{beneficiary.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
						<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;"/>
						<h:inputText id="lastNameEF" value="#{beneficiary.demographics.lastName}" styleClass="formEntryText" style="width: 120px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
					</h:panelGroup>
					<h:panelGroup id="ownerFullNamePGroup" styleClass="formDataEntryLine" rendered="#{beneficiary.corporation}">
						<h:outputText id="fullName" value="#{property.appEntFullName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="fullNameEF" value="#{beneficiary.demographics.fullName}" styleClass="formEntryText" style="width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
					</h:panelGroup>
					
					<h:panelGroup id="beneficiaryLine1PGroup" styleClass="formDataEntryLine">
						<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="addressLine1" value="#{beneficiary.address.addressLine1}" styleClass="formEntryText" style="width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
						<h:selectBooleanCheckbox id="deleteAddress" value="#{beneficiary.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="position: absolute;left: 440px; width: 25px;" disabled="#{beneficiary.addressDisabled}"
						valueChangeListener="#{beneficiary.delPartyDis}" onclick="resetTargetFrame();submit()" /><!-- NBA175 -->
						<h:outputText id="delAddress" value="#{property.appEntDelBenAdd}" styleClass="formLabel" style="position: absolute;left: 465px;width: 100px;"/><!-- NBA175 -->
					</h:panelGroup>
					<h:panelGroup id="beneficiaryLine2PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine2" value="#{beneficiary.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
					</h:panelGroup>
					<h:panelGroup id="beneficiaryLine3PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine3" value="#{beneficiary.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
					</h:panelGroup>
					<!-- begin NBA247 -->
					<h:panelGroup id="beneficiaryLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
						<h:inputText id="addressLine4" value="#{beneficiary.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<!-- end NBA247 -->
					<h:panelGroup id="beneficiaryCityPGroup" styleClass="formDataEntryLine">
						<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="city1EF" value="#{beneficiary.address.city}" styleClass="formEntryText" style="width: 100px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
						<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 120px;"/>
						<h:selectOneMenu id="state1DD" value ="#{beneficiary.address.state}" styleClass="formEntryText" style="width: 200px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!--  NBA211 -->
							<f:selectItems id="beneficiaryState1List" value="#{beneficiary.address.stateList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGrid id="beneficiaryCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" styleClass="formDataEntryLine"><!-- NBA316 -->
						<h:column>
							<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 95px;"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="beneficiaryZipRB" value="#{beneficiary.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
								<f:selectItems id="beneficiaryZipTypes" value="#{beneficiary.address.zipTypeCodes}" />
							</h:selectOneRadio>				
						</h:column>
						<h:column>
							<!-- begin SPRNBA-493 -->
							<h:inputText id="zipEF" value="#{beneficiary.address.zip}" styleClass="formEntryText" style="width: 85px;" 
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" ><!-- NBA211 -->
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> 
							</h:inputText> 
							<h:inputText id="postalEF" value="#{beneficiary.address.postal}" styleClass="formEntryText" style="width: 85px;" 
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
							<!-- end SPRNBA-493 -->
						</h:column>
						<h:column>
							<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 60px;"/> <!-- SPRNBA-493 -->
							<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 60px;margin-top: 10px;" />	<!-- NBA316 -->
						</h:column>
						<h:column>
							<h:selectOneMenu id="countryDD" value ="#{beneficiary.address.country}" styleClass="formEntryText" style="width: 200px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
								<f:selectItems id="beneficiaryCountryList" value="#{beneficiary.address.countryList}" />
							</h:selectOneMenu>	
							<h:inputText id="countyEF"
								value="#{beneficiary.address.county}" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 195px;margin-top: 5px;" />	<!-- NBA316 -->				
						</h:column>
					</h:panelGrid>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText id="trustee" value="#{property.appEntTrustee}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="trusteeEF" value="#{beneficiary.trustee}" styleClass="formEntryText" style="width: 150px;" 
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || beneficiary.person}"/> <!-- NBA211, SPRNBA-404 -->				
						<h:outputText id="dateOfTrustAgreement" value="#{property.appEntDateOfTrustAgreement}" styleClass="formLabel" style="width: 170px;"/>
						<h:inputText id="dateOfTrustAgreementEF" value="#{beneficiary.dateOfTrustAgreement}" styleClass="formEntryText" style="width: 100px;" disabled="#{beneficiary.person}"> <!-- SPRNBA-404 -->  				
								<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
					</h:panelGroup>
					<h:panelGrid columns="3" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0">
						<h:column>
							<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 95px;" rendered="#{beneficiary.person}"/>
							<h:inputText id="dobEF" value="#{beneficiary.demographics.birthDate}" styleClass="formEntryDate" style="width: 100px;" rendered="#{beneficiary.person}"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>				
							<h:outputText id="dobBlank" value="" styleClass="formLabel" style="width: 195px;" rendered="#{beneficiary.corporation}"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="govtIdType" value="#{beneficiary.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 180px;" rendered="#{beneficiary.person}"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 NBA340 -->
								<f:selectItem id="ssn" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
								<f:selectItem id="tin" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sin" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>	
							<h:selectOneRadio id="govtIdCorpType" value="#{beneficiary.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 180px;" rendered="#{beneficiary.corporation}"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 NBA340-->
								<f:selectItem id="tincorp" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sincorp" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>			
								
						</h:column>
						<h:column>
						<!-- begin NBA340 -->
							<h:inputText id="govtIdInput" value="#{beneficiary.demographics.ssn}" styleClass="formEntryText" style="width: 135px;margin-top: 50px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211, SPRNBA-364 -->							
						    </h:inputText> <!-- SPRNBA-364 -->
						    <h:inputHidden id="govtIdKey" value="#{beneficiary.demographics.govtIdKey}"></h:inputHidden>
						<!-- end NBA340 -->
						</h:column>
					</h:panelGrid>
					
					<h:panelGroup id="beneRelationshipPGroup" styleClass="formDataEntryLine"> <!-- NBA247 -->
						<h:outputText id="relationShip" value="#{property.appEntRelToPrimaryIns}" styleClass="formLabel" style="width: 190px;"/> <!-- SPRNBA-396 -->
						<h:selectOneMenu id="relationShipDD" value="#{beneficiary.relationToPrimaryInsured}" 
										 styleClass="formEntryTextFull" 
										 style="width: 140px;">
							<f:selectItems id="beneficiaryRelationToList" value="#{beneficiary.relationToPINSList}" />
						</h:selectOneMenu>					
						 <!-- SPRNBA-396 code deleted  -->				
					</h:panelGroup>
					
					<!-- begin SPRNBA-396 -->
					<h:panelGroup id="beneficiaryDistributionTypePGroup" styleClass="formDataEntryLine">
						<h:outputLabel id="beneficiaryDistributionType" value="#{property.appEntBeneficiaryDistributionType}" styleClass="formLabel" style="width: 190px;"/> 
						<h:selectOneMenu id="beneficiaryDistributionTypeEF" value="#{beneficiary.beneficiaryDistributionType}" styleClass="formEntryText" style="width: 140px;"
							onchange="resetTargetFrame();submit();" valueChangeListener="#{beneficiary.beneficiaryDistributionTypeChanged}" immediate="true"
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || beneficiary.disableDistributionType}">
							<f:selectItems id="beneficiaryDistributionTypeList" value="#{beneficiary.beneficiaryDistributionTypeList}" />
						</h:selectOneMenu>
						<h:outputText id="percentage" value="#{property.appEntPercentage}" styleClass="formLabel" style="width: 105px;"/>
						<h:inputText id="percentageEF" value="#{beneficiary.percentage}" styleClass="formEntryText" style="width: 60px;" 
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || beneficiary.disablePercent}"/>
					</h:panelGroup>
					
					<h:panelGroup id="beneficiaryAmountPGroup" styleClass="formDataEntryLine">
						<h:outputText id="amount" value="#{property.appEntAmount}" styleClass="formLabel" style="position: relative;left: 330px; width: 105px;"/>
						<h:inputText id="amountEF" value="#{beneficiary.amount}" styleClass="formEntryText" style="position: relative;left: 330px; width: 60px;" 
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || beneficiary.disableAmount}"> <!-- SPRNBA-554 -->
							<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/> <!-- SPRNBA-554 -->
						</h:inputText>	<!-- SPRNBA-554 -->
					</h:panelGroup>
					<!-- end SPRNBA-396 -->
								
				</h:column>
			</h:dataTable>
			</h:column>
			<h:column>
				<h:commandLink value="#{property.buttonAddAnother}" action="#{pc_beneficiaryInfo.addAnotherBeneficiary}" styleClass="formButtonInterface" style="width: 65px;"
					onmousedown="getScrollXY('form_appEntryULPage3');"/>
			</h:column>
</h:panelGrid>
	

</jsp:root>
