<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- SPR3295           7      Application Entry Reusability -->
<!-- NBA211            7      Partial Application -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
	<h:outputText id="employerTitle" value="Employer" styleClass="formSectionBar"/>
	<h:panelGroup id="deleteEmployer" style="position: relative;left: 460px;"><!-- begin NBA175  -->
		<h:selectBooleanCheckbox id="deleteEmp" value="#{pc_primaryInsured.employerInfo.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"/>
		<h:outputText id="delEmployer" value="#{property.appEntDelEmp}" styleClass="formLabel" style="width: 200px;text-align: left;"/>
	</h:panelGroup>	<!-- end NBA175  -->
	<h:panelGroup styleClass="formDataEntryLine">
		<h:outputText id="empName" value="#{property.appEntEmployerName}" styleClass="formLabel" style="width: 110px;"/>
		<h:inputText id="empNameEF" value="#{pc_primaryInsured.employerInfo.employerName}" styleClass="formEntryText" style="width: 200px;" maxlength="100" 
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- SPR3295, NBA211 -->
		<h:commandButton id="empAddrPopup" image="images/link_icons/circle_i.gif" action="#{pc_primaryInsured.employerInfo.launchPopUp}"
							onclick="setTargetFrame();" style="position: relative; left: 5px; vertical-align: bottom;" 
							disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- SPR3295, NBA211 -->
		<h:outputText id="yrsWithEmp" value="#{property.appEntYrsWithEmployer}" styleClass="formLabel" style="width: 200px;"/>
		<h:inputText id="yrsWithEmpEF" value="#{pc_primaryInsured.employerInfo.yrsWithEmployer}" styleClass="formEntryText" style="width: 50px;" 
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- SPR3295, NBA211 -->
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine">
		<h:outputText id="annualIncome" value="#{property.appEntAnnualIncome}" styleClass="formLabel" style="width: 110px;"/>
		<h:inputText id="annualIncomeEF" value="#{pc_primaryInsured.employerInfo.annualIncome}" styleClass="formEntryText" style="width: 80px;"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}">  <!-- SPR3295, NBA211 -->
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
		</h:inputText>
		<h:outputText id="netWorth" value="#{property.appEntNetWorth}" styleClass="formLabel" style="width: 80px;"/>
		<h:inputText id="netWorthEF" value="#{pc_primaryInsured.employerInfo.netWorth}" styleClass="formEntryText" style="width: 85px;">  <!-- SPR3295 -->
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
		</h:inputText>
		<h:outputText id="occupation" value="#{property.appEntOccupation}" styleClass="formLabel" style="width: 85px;"/>
		<h:selectOneMenu id="occupationDD" value ="#{pc_primaryInsured.employerInfo.occupation}" styleClass="formEntryText" style="width: 140px"
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}">  <!-- SPR3295,NBA211 -->
			<f:selectItems id="occupationList" value="#{pc_nbaAppEntryNavigation.occupationList}"/>  <!-- SPR3295 -->
		</h:selectOneMenu>	
	</h:panelGroup>
</jsp:root>
