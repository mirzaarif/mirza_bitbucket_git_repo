<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->
<!-- SPR3545		   8      Delete Owner Relation for One Originally Created by User or by Markup as Same as Primary Insured -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-404		NB-1101	  Need to Disable Trustee Fields for Person -->
<!-- SPRNBA-407		NB-1101	  Enable Relation to Insured Drop Down in Owner Section When Same As Check Box Selected -->
<!-- SPRNBA-364		NB-1101	  Standard Phone Number and Social Security Number Format Should be Used and Tab Order Corrected from First to Middle Name -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-327		NB-1601	  Verification Drop Down Should Be Moved to Right of Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="ownerInfo" value="#{property.appEntOwnerInfo}" styleClass="formSectionBar"/>
		<h:panelGroup id="ownerTypePGroup" styleClass="formDataEntryLine">
			<h:selectBooleanCheckbox id="sameAsInsCB" value="#{pc_ownerInfo.sameAsPrimaryInsured}" styleClass="radioMenu" style="width: 25px;" 
				onclick="resetTargetFrame();submit();" valueChangeListener="#{pc_ownerInfo.sameAsPrimaryInsuredChecked}" immediate="true"
				binding="#{pc_ownerInfo.sameAsPrimaryInsCB}" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_ownerInfo.disableSameAsCheckBox}"> <!-- NBA211, SPR3545 -->
			</h:selectBooleanCheckbox>
			<h:outputText id="sameAsIns" value="#{property.appEntSameAsPrimaryIns}" styleClass="formLabel" style="width: 185px;text-align: left;"/>
		</h:panelGroup>
		<h:panelGrid id="ownerPGrid" columns="2" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0">
			<h:column>

				<h:dataTable id="owners" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_ownerInfo.ownersList}" 
					var="owner" style="width: 525px;padding-left: 2px;">
					<h:column>

						<h:panelGroup styleClass="formDataEntryLine" rendered="#{owner.displayBar}">
							<f:verbatim>
									<hr class="formSeparator" />
							</f:verbatim>
						</h:panelGroup>
						<h:panelGroup id="deleteOwner" style="position: relative;left: 425px;"><!-- NBA175  begin-->
							<h:selectBooleanCheckbox id="deleteOwn" value="#{owner.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" disabled="#{owner.disabled}" valueChangeListener="#{owner.delPartyAddrDis}" onclick="resetTargetFrame();submit()"/>
							<h:outputText id="delOwner" value="#{property.appEntDelOwner}" styleClass="formLabel" style="width: 200px;text-align: left;" />
						</h:panelGroup>	<!-- NBA175  end-->
						<h:panelGrid id="personCorpPGrid" columns="2" cellpadding="0" cellspacing="0">
							<h:column>
								<h:outputText value="" styleClass="formLabel" style="width: 80px;"></h:outputText>
							 	<h:selectOneRadio id="selectRadioGroup" value="#{owner.partyType}" disabled="#{owner.sameAsPrimaryIns || owner.personOrOrg || pc_nbaAppEntryNavigation.partialUpdateDisabled}" onclick="resetTargetFrame();submit();"
							 					layout="lineDirection" styleClass="radioLabel" style="width:180px;margin-top: -5px;"><!-- NBA175, NBA211 -->
										<f:selectItem id="individual" itemValue="1" itemLabel="#{property.appEntPerson}"/>
										<f:selectItem id="group" itemValue="2" itemLabel="#{property.appEntCorporation}"/>
								</h:selectOneRadio>	 						
							</h:column>
							<h:column rendered="#{owner.corporation}">
								<h:outputText id="ownerType" value="#{property.appEntOwnerType}" styleClass="formLabel" style="width: 95px;"/>
							 	<h:selectOneMenu id="ownerTypeDD" value="#{owner.ownerType}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
							 					styleClass="formEntryText" style="width: 170px;"> <!-- NBA211 -->
									<f:selectItems id="ownerTypeList" value="#{owner.ownerTypeList}"/> 
								</h:selectOneMenu>		 				
							</h:column>						
						</h:panelGrid>
						<h:panelGroup id="trusteeAggrementPGroup" styleClass="formDataEntryLine">
							<h:outputText id="trustee" value="#{property.appEntTrustee}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="trusteeEF" value="#{owner.trustee}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled || owner.person}"
													styleClass="formEntryText" style="width: 170px;"/> <!-- NBA211, SPRNBA-404 -->
							<h:outputText id="trusteeAggrementDate" value="#{property.appEntTrustAgreementDate}" styleClass="formLabel" style="width: 170px;"/>
							<h:inputText id="trusteeAggrementDateEF" value="#{owner.trustAgreementDate}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled || owner.person}"
													styleClass="formEntryDate" style="width: 85px;"> <!-- NBA211, SPRNBA-404 -->
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>
						</h:panelGroup>	
						<h:panelGroup id="ownerFullNamePGroup" styleClass="formDataEntryLine" rendered="#{owner.corporation}">
							<h:outputText id="fullName" value="#{property.appEntFullName}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="fullNameEF" value="#{owner.demographics.fullName}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 300px;"/> <!-- NBA211 -->
						</h:panelGroup>
					
						<h:panelGroup id="ownerNamePGroup" styleClass="formDataEntryLine" rendered="#{owner.person}">
							<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="firstNameEF" value="#{owner.demographics.firstName}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 90px;"/> <!-- NBA211 -->
							<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
							<h:inputText id="middelNameEF" value="#{owner.demographics.midName}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 30px;"/> <!-- NBA211 -->
							<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;"/>
							<h:inputText id="lastNameEF" value="#{owner.demographics.lastName}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 120px;"/> <!-- NBA211 -->
						</h:panelGroup>
						<h:panelGroup id="ownerLine1PGroup" styleClass="formDataEntryLine">
							<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="addressLine1" value="#{owner.address.addressLine1}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 300px;"/> <!-- NBA211 -->
							<h:panelGroup id="deleteAdd" ><!-- NBA175  begin-->
								<h:selectBooleanCheckbox id="deleteAddress" value="#{owner.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox"  style="position: relative;left: 27px; width:25px;" 
								disabled="#{owner.addressDisabled}" valueChangeListener="#{owner.delPartyDis}" onclick="resetTargetFrame();submit()"/>
								<h:outputText id="delAddress" value="#{property.appEntDelOwnerAdd}" styleClass="formLabel" style="position: relative;left: 25px; width:100px;"/>
							</h:panelGroup>	<!-- NBA175  end-->							
						</h:panelGroup>
						<h:panelGroup id="ownerLine2PGroup" styleClass="formDataEntryLine">
							<h:inputText id="addressLine2" value="#{owner.address.addressLine2}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/> <!-- NBA211 -->
						</h:panelGroup>
						<h:panelGroup id="ownerLine3PGroup" styleClass="formDataEntryLine">
							<h:inputText id="addressLine3" value="#{owner.address.addressLine3}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/> <!-- NBA211 -->
						</h:panelGroup>
						<!-- being NBA247 -->
						<h:panelGroup id="ownerLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
							<h:inputText id="addressLine4" value="#{owner.address.addressLine4}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/> <!-- NBA211 -->
						</h:panelGroup>
						<!-- end NBA247 -->
						<h:panelGroup id="ownerCityPGroup" styleClass="formDataEntryLine">
							<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="city1EF" value="#{owner.address.city}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 100px;"/> <!-- NBA211 -->
							<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 125px;"/> <!-- SPRNBA-493 -->
							<h:selectOneMenu id="state1DD" value ="#{owner.address.state}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
							 						styleClass="formEntryText" style="width: 200px;"> <!-- NBA211 -->
								<f:selectItems id="state1List" value="#{owner.address.stateList}" />
							</h:selectOneMenu> 
						</h:panelGroup>
						<h:panelGrid id="ownerCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" styleClass="formDataEntryLine"><!-- NBA316 -->
							<h:column>
								<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 95px;"/>
							</h:column>
							<h:column>
								<h:selectOneRadio id="pinsZipRB" value="#{owner.address.zipTC}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													layout="pageDirection" styleClass="radioLabel" style="width: 70px;"> <!-- NBA211 -->
									<f:selectItems value="#{owner.address.zipTypeCodes}" />
								</h:selectOneRadio>				
							</h:column>
							<h:column>
								<!-- begin SPRNBA-493 -->
								<h:inputText id="zipEF" value="#{owner.address.zip}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 85px;" > <!-- NBA211 -->
									<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>
								</h:inputText> 
								<h:inputText id="postalEF" value="#{owner.address.postal}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
													styleClass="formEntryText" style="width: 85px;" /> <!-- NBA211 -->
								<!-- end SPRNBA-493 -->
							</h:column>
							<h:column>
								<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 60px;"/> <!-- SPRNBA-493 -->
								<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 60px;margin-top: 10px;" />	<!-- NBA316 -->
							</h:column>
							<h:column>
								 <h:selectOneMenu id="countryDD" value="#{owner.address.country}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
								 					styleClass="formEntryText" style="width: 200px;"> <!-- NBA211 -->
									<f:selectItems id="countryList" value="#{owner.address.countryList}" />
								</h:selectOneMenu>	
								<h:inputText id="countyEF"
								value="#{owner.address.county}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 200px;margin-top: 5px;" />	<!-- NBA316 -->			 
							</h:column>
						</h:panelGrid>
						<!-- begin NBA247 -->
						<h:panelGroup id="dobPGroup" styleClass="formDataEntryLine">
								<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 95px;" rendered="#{owner.person}"/>
								<h:inputText id="dobEF" value="#{owner.demographics.birthDate}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.person}"
													styleClass="formEntryDate" style="width: 100px;" > <!-- NBA211 -->
									<f:convertDateTime pattern="#{property.datePattern}"/>
								</h:inputText>				
								<h:outputText id="dobBlank" value="" styleClass="formLabel" style="width: 195px;" rendered="#{owner.corporation}"/>
						</h:panelGroup>
						<h:panelGrid columns="3" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0">
							<h:column>
								<h:selectOneRadio id="govtIdType" value="#{owner.demographics.ssnTC}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.person}"
													layout="pageDirection" styleClass="radioLabel" style="width: 140px;"> <!-- NBA211 NBA340 -->
									<f:selectItem id="ssn" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
									<f:selectItem id="ti" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
									<f:selectItem id="sin" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
								</h:selectOneRadio>	
								<h:selectOneRadio id="govtIdCorpType" value="#{owner.demographics.ssnTC}" disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.corporation}"
													layout="pageDirection" styleClass="radioLabel" style="width: 140px;"> <!-- NBA211 NBA340-->
									<f:selectItem id="tincorp" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
									<f:selectItem id="sincorp" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
								</h:selectOneRadio>			
							</h:column>
							<h:column>
							<!-- begin NBA340 -->
								<h:inputText id="govtIdInput" value="#{owner.demographics.ssn}" 
											 disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
											 styleClass="formEntryText"
										     style="width: 90px;margin-top: 50px;margin-bottom:0px"> <!-- NBA211, SPRNBA-364, SPRNBA-327 -->
								</h:inputText> <!-- SPRNBA-364 -->
								<h:inputHidden id="govtIdKey" value="#{owner.demographics.govtIdKey}"></h:inputHidden>
							<!-- end NBA340 -->
							</h:column>
							<h:column>	
								<h:outputText id="taxVerification" value="Verification" styleClass="formLabel" 
										style="width: 90px;margin-top:50px;"/> <!-- SPRNBA-327 -->
								<h:selectOneMenu id="taxVerificationDD" value ="#{owner.demographics.taxIDVerification}" 
										styleClass="formEntryTextFull" 
										style="width: 200px;margin-top:50px;" 
										disabled="#{owner.sameAsPrimaryIns || pc_nbaAppEntryNavigation.partialUpdateDisabled}">
									<f:selectItems id="taxVerificationList" value="#{owner.demographics.taxIDVerificationList}" />
								</h:selectOneMenu>							 		
							</h:column>
						</h:panelGrid>
						<!-- end NBA247 -->
						<h:panelGroup id="ownerRelationshipPGroup" styleClass="formDataEntryLine">
							<h:outputText id="relationShip" value="#{property.appEntRelToPrimaryIns}" styleClass="formLabel" style="width: 180px;"/>						
						 	<h:selectOneMenu id="relationShipDD" value="#{owner.relToPrimaryInsured}" styleClass="formEntryTextFull" style="width: 335px;"> <!-- SPRNBA-407 --> 
								<f:selectItems id="relationShipList" value="#{owner.relToPrimaryInsuredList}"/>
							</h:selectOneMenu>  
						</h:panelGroup>
						  <h:panelGroup id="ownerCitizenshipPGroup" styleClass="formDataEntryLine" rendered="#{owner.person}">
							<h:selectBooleanCheckbox id="usCitizenCB" value="#{owner.demographics.usCitizen}" disabled="#{owner.sameAsPrimaryIns}"
													styleClass="ovFullCellSelectCheckBox" style="width: 15px;"/>
							<h:outputText id="usCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;"/>
							<h:outputText id="citizenship" value="#{property.appEntCitizenship}" styleClass="formLabel" style="width: 95px;"/>
							<h:selectOneMenu id="citizenshipDD" value="#{owner.demographics.usCitizenship}" 
											 disabled="#{owner.sameAsPrimaryIns}"
											 styleClass="formEntryText" style="width: 270px;"> 
								<f:selectItems id="citizenshipList" value="#{owner.demographics.usCitizenshipList}" />
							</h:selectOneMenu>							
						</h:panelGroup>	 															
					</h:column>
				</h:dataTable>
			</h:column>
			<h:column>
				<h:commandLink id="owneradd" value="#{property.buttonAddAnother}" action="#{pc_ownerInfo.addAnotherOwner}" styleClass="formButtonInterface"
					onmousedown="getScrollXY('form_appEntryULPage2');"  style="margin-bottom: 10px; width: 65px;"/>
			</h:column>
		</h:panelGrid>
</jsp:root>