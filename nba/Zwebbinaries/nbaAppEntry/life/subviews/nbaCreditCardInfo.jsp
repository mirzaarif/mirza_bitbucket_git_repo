<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
		<h:outputText id="creditCardPmnt" value="#{property.appEntCreditCardPmnt}" styleClass="formSectionBar"/>
		<h:panelGroup id="paymentTypePGroup" styleClass="formDataEntryTopLine">
			<h:outputText id="pType" value="#{property.appEntPaymntType}" styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="pTypeDD" value="#{pc_creditCardInfo.paymentType}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 270px;">
				<f:selectItems id="pTypeList" value="#{pc_creditCardInfo.paymentTypeList}" />
			</h:selectOneMenu>
			<h:outputText id="effDate" value="#{property.appEntEffDate}" styleClass="formLabel" style="width: 105px;"/>
			<h:inputText id="effDateEF" value="#{pc_creditCardInfo.effectiveDate}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 90px;">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>			
		</h:panelGroup>
		<h:panelGroup id="ccTypePGroup" styleClass="formDataEntryLine">
			<h:outputText id="ccType" value="#{property.appEntCCType}" styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="ccTypeDD" value="#{pc_creditCardInfo.creditCardType}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 190px;" onchange="processCreditCardInformation('creditCardInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');"><!-- NBA317 -->
				<f:selectItems id="ccTypeList" value="#{pc_creditCardInfo.creditCardTypeList}" />
			</h:selectOneMenu>
			<h:inputHidden id="ccLabel" value="#{pc_creditCardInfo.selectedCardLabel}"></h:inputHidden><!-- NBA317 -->
			<h:outputText id="chargeAmt" value="#{property.appEntChargeAmt}" styleClass="formLabel" style="width: 125px;"/>
			<h:inputText id="chargeAmtEF" value="#{pc_creditCardInfo.chargeAmount}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 150px;">
				<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="cardNumberPGroup" styleClass="formDataEntryLine">
			<h:outputText id="cardNumber" value="#{property.appEntCardNumber}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="cardNumberEF" value="#{pc_creditCardInfo.cardNumberFormatted}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 160px;" onchange="formatCreditCardNumber('creditCardInfo','cardNumberEF','cardNumberEFHidden');processCreditCardInformation('creditCardInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');"/><!-- NBA317 -->
			<h:inputHidden id="cardNumberEFHidden" value="#{pc_creditCardInfo.cardNumber}"></h:inputHidden>
			<h:outputText id="cardNumberV" value="#{property.appEntCardNumberV}" styleClass="formLabel" style="width: 140px;"/>
			<h:inputText id="cardNumberVEF" value="#{pc_creditCardInfo.verifyCardNumberFormatted}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 165px;" onchange="formatCreditCardNumber('creditCardInfo','cardNumberVEF','cardNumberVEFHidden');processCreditCardInformation('creditCardInfo',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');"/><!-- NBA317 -->
			<h:inputHidden id="cardNumberVEFHidden" value="#{pc_creditCardInfo.verifyCardNumber}"></h:inputHidden>
			<h:inputHidden id="cardNumberTokenized" value="#{pc_creditCardInfo.ccNumberTokenized}"></h:inputHidden><!-- NBA317 -->
		</h:panelGroup>
		<h:panelGroup id="expirationPGroup" styleClass="formDataEntryLine">
			<h:outputText id="exp" value="#{property.appEntExp}" styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="expMonthDD" value="#{pc_creditCardInfo.expirationMonth}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryTextFull" style="width: 140px">
				<f:selectItems id="expMonthList" value="#{pc_creditCardInfo.expirationMonths}" />
			</h:selectOneMenu>
			<h:outputText id="expMonth" value="#{property.appEntExpMonth}" styleClass="formLabel" style="width: 100px;text-align: left;padding-left: 10px;"/>
			<h:selectOneMenu id="expYearDD" value="#{pc_creditCardInfo.expirationYear}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryTextFull" style="width: 95px;">
				<f:selectItems id="expYearList" value="#{pc_creditCardInfo.expirationYears}" />
			</h:selectOneMenu>
			<h:outputText id="expYear" value="#{property.appEntExpYear}" styleClass="formLabel" style="width: 110px;text-align: left;padding-left: 10px;"/>
		</h:panelGroup>
		<h:panelGroup id="namePGroup" styleClass="formDataEntryLine">
			<h:outputText id="nameCard" value="#{property.appEntNameCard}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="nameCardEF" value="#{pc_creditCardInfo.demographics.fullName}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 330px"/>
		</h:panelGroup>
		<h:panelGroup id="billingAddressPGroup" styleClass="formDataEntryLine">
			<h:outputText id="billingAdd" value="#{property.appEntBillingAdd}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="billingAddEF" value="#{pc_creditCardInfo.address.addressLine1}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 330px"/>
		</h:panelGroup>
		<h:panelGroup id="ccCityPGroup" styleClass="formDataEntryLine">
			<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="city1EF" value="#{pc_creditCardInfo.address.city}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 130px;"/>
			<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="state1DD" value ="#{pc_creditCardInfo.address.state}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryTextFull" style="width: 225px;">
				<f:selectItems id="ccState1List" value="#{pc_creditCardInfo.address.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGrid id="ccCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort" columns="3"  cellpadding="0" cellspacing="0" style="padding-top: 6px;">
			<h:column>
				<h:outputText id="ccZipCode" value="#{property.appEntCode}" styleClass="formLabel" style="width: 120px;"/>
			</h:column>
			<h:column>
				<h:selectOneRadio id="pinsZipRB" value="#{pc_creditCardInfo.address.zipTC}" layout="pageDirection" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="radioLabel" style="width: 70px;">
					<f:selectItems id="ccZipTypeCodes" value="#{pc_creditCardInfo.address.zipTypeCodes}" />
				</h:selectOneRadio>				
			</h:column>
			<h:column>
				<!-- begin SPRNBA-493 -->
				<h:inputText id="zipEF" value="#{pc_creditCardInfo.address.zip}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 85px;" >
					<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> 
				</h:inputText>
				<h:inputText id="postalEF" value="#{pc_creditCardInfo.address.postal}" disabled="#{pc_nbaAppEntryNavigation.updateMode}" styleClass="formEntryText" style="width: 85px;" />
				<!-- end SPRNBA-493 -->
			</h:column>
		</h:panelGrid>
</jsp:root>