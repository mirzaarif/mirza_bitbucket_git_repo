<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- SPRNBA-365        NB-1101   Insured's Name Field Populated from Impairment Search Should Be Expanded -->



<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
		<h:outputText value="#{property.appEntPage6Q3}" styleClass="formLabel" style="width: 370px;text-align: left;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup>
		<h:outputText value="" style="width: 100px;"></h:outputText>
		<h:commandButton id="cb1" action="#{pc_nbaUlPage6.impairmentSearch}" value="#{property.appEntImpSearch}" styleClass="ovAppEntryButton"
			style="width: 140px;" onclick="setTargetFrame();" disabled="#{!pc_nbaUlPage6.servicePresent}"/>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="width: 420px; padding-left: 2px;text-align: left;"> <!-- SPRNBA-365 -->
		<h:outputText value="#{property.appEntInsured}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText id="insuredNameText" value="#{pc_nbaUlPage6.insuredImpairmentInformation.insuredName}" styleClass="formEntryText" style="width: 230px;"
			disabled="#{!pc_nbaUlPage6.servicePresent}" ></h:inputText> <!-- SPRNBA-365 -->
		<h:outputText value="" style="width: 10px;"></h:outputText>
		<h:selectBooleanCheckbox id="deletechkBoxInsured" value="#{pc_nbaUlPage6.insuredImpairmentInformation.deleteImpairment}" styleClass="ovFullCellSelectCheckBox"
			style="width: 15px;" disabled="#{pc_nbaAppEntryNavigation.updateMode || !pc_nbaUlPage6.servicePresent}"/>
		<h:outputText value="#{property.appEntDelete}" styleClass="formLabel" style="width: 70px;text-align: center;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntImpairment}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:outputText value="#{pc_nbaUlPage6.insuredImpairmentInformation.impairmentName}" styleClass="formEntryText" style="width: 250px;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntDate}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText value="#{pc_nbaUlPage6.insuredImpairmentInformation.impairmentDate}" styleClass="formEntryText" style="width: 140px;" disabled="#{!pc_nbaUlPage6.servicePresent}">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>

	<h:panelGroup id="jointOrOtherInsuredSection" rendered="#{pc_nbaUlPage6.jointOrOtherInsuredPresent || pc_jointOrOtherInsured.jointOrOtherInsuredPresent}">
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntInsured}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText value="#{pc_nbaUlPage6.otherOrJointInsuredImpairmentInformation.insuredName}" styleClass="formEntryText" style="width: 160px;"
			disabled="#{!pc_nbaUlPage6.servicePresent}"></h:inputText>
		<h:outputText value="" style="width: 10px;"></h:outputText>
		<h:selectBooleanCheckbox id="deletechkBoxOtherOrJointInsured" value="#{pc_nbaUlPage6.otherOrJointInsuredImpairmentInformation.deleteImpairment}"
			styleClass="ovFullCellSelectCheckBox" style="width: 15px;" disabled="#{pc_nbaAppEntryNavigation.updateMode || !pc_nbaUlPage6.servicePresent}"/>
		<h:outputText value="#{property.appEntDelete}" styleClass="formLabel" style="width: 70px;text-align: center;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntImpairment}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:outputText value="#{pc_nbaUlPage6.otherOrJointInsuredImpairmentInformation.impairmentName}" styleClass="formEntryText" style="width: 250px;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntDate}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText value="#{pc_nbaUlPage6.otherOrJointInsuredImpairmentInformation.impairmentDate}" styleClass="formEntryText" style="width: 140px;" disabled="#{!pc_nbaUlPage6.servicePresent}">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	</h:panelGroup>
</jsp:root>
