<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA211            7      Partial Application-->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- FNB002 		NB-1101	  App Entry  -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301	  Address Normalization Using Web Service -->
<!-- NBA297         NB-1401   Suitability Enhancement -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-327		NB-1601	  Verification Drop Down Should Be Moved to Right of Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="pinsInfo" value="#{property.appEntPrimaryInsInfo}" styleClass="formSectionBar"/>
		<h:panelGroup id="pinsNamePGroup" styleClass="formDataEntryLine">
			<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 100px" />
			<h:inputText id="firstNameEF" value="#{pc_primaryInsured.demographics.firstName}" styleClass="formEntryText" style="width: 100px;"		
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_primaryInsured.firstNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px"/>
			<h:inputText id="middelNameEF" value="#{pc_primaryInsured.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_primaryInsured.middleNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px"/>
			<h:inputText id="lastNameEF" value="#{pc_primaryInsured.demographics.lastName}" styleClass="formEntryText" style="width: 175px;"
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_primaryInsured.lastNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
		</h:panelGroup>
		
		<!-- Begin FNB002  -->
		<h:panelGroup id="pinsnickNamePGroup" styleClass="formDataEntryLine">
			<h:outputText id="nickName" value="#{property.appEntNickName}" styleClass="formLabel" style="width: 100px"/>
			<h:inputText id="nickNameEF" value="#{pc_primaryInsured.demographics.nickName}" styleClass="formEntryText" style="width: 100px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" maxlength="20"/>
		</h:panelGroup>		
		<!-- end FNB002  -->
		
		<h:panelGroup id="pinsLine1PGroup" styleClass="formDataEntryLine">
			<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 100px"/>
			<h:inputText id="addressLine1" value="#{pc_primaryInsured.address.addressLine1}" styleClass="formEntryText" style="width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
			<h:panelGroup id="deleteAdd" style="position: relative;left: 100px;width: 170px;"><!-- begin NBA175  -->
				<h:selectBooleanCheckbox id="deleteAddress" value="#{pc_primaryInsured.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"/>
				<h:outputText id="delAddress" value="#{property.appEntDelPrimInsAdd}" styleClass="formLabel" style="width: 100px;text-align: left;"/>
			</h:panelGroup>	<!-- end NBA175  -->									
		</h:panelGroup>
		<h:panelGroup id="pinsLine2PGroup" styleClass="formDataEntryLine">
			<h:inputText id="addressLine2" value="#{pc_primaryInsured.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
			<h:outputText id="years" value="#{property.appEntYearAtCurrentAdd}" styleClass="formLabel" style="position: relative;left: 90px;width: 170px;"/><!-- NBA175 -->
			<h:inputText id="yearsEF" value="#{pc_primaryInsured.address.yearsAtCurrentAdd}" styleClass="formEntryText" style="position: relative;left: 100px;width: 40px;"/><!-- NBA175 -->
		</h:panelGroup>
		<h:panelGroup id="pinsLine3PGroup" styleClass="formDataEntryLine">
			<h:inputText id="addressLine3" value="#{pc_primaryInsured.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>	<!-- NBA211 -->
		</h:panelGroup>
		<!-- begin NBA247 -->
		<h:panelGroup id="pinsLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
			<h:inputText id="addressLine4" value="#{pc_primaryInsured.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>	
		</h:panelGroup>
		<!-- end NBA247 -->
		<h:panelGroup id="pinsCityPGroup" styleClass="formDataEntryLine">
			<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="city1EF" value="#{pc_primaryInsured.address.city}" styleClass="formEntryText" style="width: 95px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
			<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 130px;"/>
			<h:selectOneMenu id="state1DD" value ="#{pc_primaryInsured.address.state}" styleClass="formEntryTextFull" style="width: 260px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="state1List" value="#{pc_primaryInsured.address.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGrid id="pinsCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" style="padding-top: 6px;"><!-- NBA316 -->
			<h:column>
				<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 100px;"/>
			</h:column>
			<h:column>
				<h:selectOneRadio id="pinsZipRB" value="#{pc_primaryInsured.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					<f:selectItems id="pinsZipTypes" value="#{pc_primaryInsured.address.zipTypeCodes}" />
				</h:selectOneRadio>				
			</h:column>
			<h:column>
				<h:inputText id="zipEF" value="#{pc_primaryInsured.address.zip}" styleClass="formEntryText" style="width: 86px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					 <f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>  <!-- SPRNBA-493 -->
				</h:inputText>
				<h:inputText id="postalEF" value="#{pc_primaryInsured.address.postal}" styleClass="formEntryText" style="width: 86px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
			</h:column>
			<h:column>
				<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 68px;"/>
				<h:outputText id="county" value="#{property.appEntCounty}"
				styleClass="formLabel" style="width: 70px;margin-top: 10px;" /><!-- NBA316 -->
			</h:column>
			<h:column>
				<h:selectOneMenu id="countryDD" value ="#{pc_primaryInsured.address.country}" styleClass="formEntryTextFull" style="width: 260px;"
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					<f:selectItems id="countryList" value="#{pc_primaryInsured.address.countryList}" />
				</h:selectOneMenu>
				<h:inputText id="countyEF"
				value="#{pc_primaryInsured.address.county}" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
				styleClass="formEntryText" maxlength="100" style="width: 260px;margin-top: 5px;" />	<!-- NBA316 -->				
			</h:column>
		</h:panelGrid>
		<h:panelGroup id="pinsMailPGroup" styleClass="formDataEntryLine">
			<h:outputText id="mailAdd" value="#{property.appEntEmail}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="mailAddEF" value="#{pc_primaryInsured.email}" styleClass="formEntryText" style="width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" />	<!-- NBA211 -->			
		</h:panelGroup>					
		<f:verbatim>
			<hr id="primaryInsuredSeparator1" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="pinsDobPGroup" styleClass="formDataEntryLine">
			<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="dobEF" value="#{pc_primaryInsured.demographics.birthDate}" styleClass="formEntryText" style="width: 100px;" 
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputText id="birthState" value="#{property.appEntBirthState}" styleClass="formLabel" style="width: 100px;"/>
			<h:selectOneMenu id="birthStateDD" value="#{pc_primaryInsured.demographics.birthState}" styleClass="formEntryText" style="width: 225px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="birthStateList" value="#{pc_primaryInsured.address.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="pinsBirthCountryPGroup" styleClass="formDataEntryLine">
			<h:outputText id="birthCountry" value="#{property.appEntBirthCountry}" styleClass="formLabel" style="width: 158px;"/> 
			<h:selectOneMenu id="birthCountryDD" value ="#{pc_primaryInsured.demographics.birthCountry}" styleClass="formEntryText" style="width: 425px;">
				<f:selectItems id="birthCountryList" value="#{pc_primaryInsured.address.countryList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="pinsHeightPGroup" styleClass="formDataEntryLine">
			<h:outputText id="gender" value="#{property.appEntGender}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="genderDD" value ="#{pc_primaryInsured.demographics.gender}" styleClass="formEntryText" style="width: 90px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->			 			
				<f:selectItems id="genderList" value="#{pc_primaryInsured.demographics.genderList}" />
			</h:selectOneMenu>
			
			<h:outputText id="height" value="#{property.appEntHeight}" styleClass="formLabel" style="width: 90px;"/>
			<h:inputText id="heightEF1" value="#{pc_primaryInsured.demographics.heightInFeet}" styleClass="formEntryText" style="width: 50px;" />
			<h:inputText id="heightEF2"  value="#{pc_primaryInsured.demographics.heightInInch}" styleClass="formEntryText" style="width: 50px;" />
			<h:outputText id="weight" value="#{property.appEntWeight}" styleClass="formLabel" style="width: 95px;"/>
			<h:inputText id="weightEF" value="#{pc_primaryInsured.demographics.weight}" styleClass="formEntryText" style="width: 50px;" />
					
		</h:panelGroup>
		<h:panelGroup id="pinsMaidenPGroup" styleClass="formDataEntryLine">
			<h:outputText id="maidenLN" value="#{property.appEntMaidenLN}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="maidenLNEF" value="#{pc_primaryInsured.demographics.maidenLName}" styleClass="formEntryText" style="width: 160px;" />								
			<h:outputText id="maritalStatus" value="#{property.appEntMaritalStatus}" styleClass="formLabel" style="width: 140px;"/>
			<h:selectOneMenu id="maritalStatusDD" value="#{pc_primaryInsured.demographics.marStatus}" styleClass="formEntryText" style="width: 125px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="maritalStatusList" value="#{pc_primaryInsured.demographics.marStatusList}" />
			</h:selectOneMenu>					

		</h:panelGroup>		
		<!-- begin NBA247 -->
		<h:panelGroup id="pinsRateClassPGroup" styleClass="formDataEntryLine">
			<h:outputText id="rateClass" value="#{property.appEntRateAppliedFor}" styleClass="formLabel" style="width: 158px;" />
			<h:selectOneMenu id="rateClassDD" value="#{pc_primaryInsured.demographics.appliedRateClass}" styleClass="formEntryText" style="width: 125px;">					
				<f:selectItems id="rateClassList" value="#{pc_primaryInsured.demographics.appliedRateClassList}"/>
			</h:selectOneMenu>	
		</h:panelGroup>
		<!-- end NBA247 -->
		<h:panelGrid id="pinsGovtIdPGrid" columnClasses="formDataEntryLineAppEntry" columns="3">
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_primaryInsured.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 151px;" immediate="true" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 NBA340 -->
					<f:selectItem id="ssnRB" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
					<f:selectItem id="sinRB" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
				</h:selectOneRadio>
			</h:column>
			<h:column>
			<!-- begin NBA340 -->
				<h:inputText id="govtIdInput" value="#{pc_primaryInsured.demographics.ssn}" styleClass="formEntryText" style="width: 100px;margin-top: 25px;margin-bottom:0px" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" > <!-- NBA211, NBA247, SPRNBA-327 -->
				</h:inputText>				
			    <h:inputHidden id="govtIdKey" value="#{pc_primaryInsured.demographics.govtIdKey}"></h:inputHidden>	
			<!-- end NBA340 -->
			</h:column>
			<h:column>	
				<h:outputText id="taxVerification" value="Verification" styleClass="formLabel" style="width: 90px;margin-top: 25px;"/> <!-- SPRNBA-327 -->
				<h:selectOneMenu id="taxVerificationDD" value ="#{pc_primaryInsured.demographics.taxIDVerification}" styleClass="formEntryTextFull" style="width: 240px;margin-top: 25px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}">
					<f:selectItems id="taxVerificationList" value="#{pc_primaryInsured.demographics.taxIDVerificationList}" />
				</h:selectOneMenu>							 		
			</h:column>
		</h:panelGrid>
		<!-- end NBA247 -->
		<h:panelGroup id="pinsLicenseNumPGroup" styleClass="formDataEntryLine">
			<h:outputText id="licenseNumber" value="#{property.appEntLicenseNumber}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="licenseNumberEF" value="#{pc_primaryInsured.demographics.licenseNumber}" styleClass="formEntryText" style="width: 155px;" />
			<h:outputText id="pinsBlank" value="" style="width: 110px;"/>
			<h:selectBooleanCheckbox id="usCitizenCB" value="#{pc_primaryInsured.demographics.usCitizen}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"/>
			<h:outputText id="usCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;text-align: left;"/>
		</h:panelGroup>
		<h:panelGroup id="pinsCitizenPGroup" styleClass="formDataEntryLine">
			<h:outputText id="licenseState" value="#{property.appEntLicenseState}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="licenseStateDD" value ="#{pc_primaryInsured.demographics.licenseState}" styleClass="formEntryText" style="width: 190px;">
				<f:selectItems id="licenseStateList" value="#{pc_primaryInsured.address.stateList}" />
			</h:selectOneMenu>
			<h:outputText id="citizenship" value="#{property.appEntCitizenship}" styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="citizenshipDD" value ="#{pc_primaryInsured.demographics.usCitizenship}" 
							 styleClass="formEntryText" style="width: 125px;"
							 disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="citizenshipList" value="#{pc_primaryInsured.demographics.usCitizenshipList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="otherInsVisaPGroup" styleClass="formDataEntryLine">
			<h:outputText id="visaType" value="#{property.appEntVisaType}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="visaTypeDD" value ="#{pc_primaryInsured.demographics.visaType}" styleClass="formEntryText" style="width: 190px;">
				<f:selectItems id="visaTypeList" value="#{pc_primaryInsured.demographics.visaTypeList}" />
			</h:selectOneMenu>
			<h:outputText id="arrivalDate" value="#{property.appEntArrivalDate}" styleClass="formLabel" style="width: 110px;"/>
			<h:inputText id="arrivalDateEF" value="#{pc_primaryInsured.demographics.arrivalDate}" styleClass="formEntryText" style="width: 125px;">			
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>			
		</h:panelGroup>
		<!-- begin NBA297-->
		<f:verbatim>
			<hr id="primaryInsuredSeparator4" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="investmentTimeGroup" styleClass="formDataEntryLine">
			<h:outputText id="invTimeFreq" value="#{property.appEntInvTimeFrquency}" styleClass="formLabel" style="width: 235px;"/>
			<h:selectOneMenu id="invTimeFreqDD" value ="#{pc_primaryInsured.demographics.invHorizonPeriod}" styleClass="formEntryText" style="width: 250px;">
				<f:selectItems id="invTimeFreqList" value="#{pc_primaryInsured.demographics.invHorizonPeriodList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="investmentTimeGroup2" styleClass="formDataEntryLine">
			<h:outputText id="invFrom" value="#{property.appEntFrom}" styleClass="formLabel" style="width: 280px;"/>
			<h:inputText id="invFromEF" value="#{pc_primaryInsured.demographics.invHorizonFrom}" styleClass="formEntryText" style="width: 60px;"/>			
			<h:outputText id="invTo" value="#{property.appEntTo}" styleClass="formLabel" style="width: 85px;"/>
			<h:inputText id="invToEF" value="#{pc_primaryInsured.demographics.invHorizonTo}" styleClass="formEntryText" style="width: 60px;"/>	
		</h:panelGroup>
		<!-- end NBA297-->
		<f:verbatim>
			<hr id="primaryInsuredSeparator2" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="pinsHomePhonePGroup" styleClass="formDataEntryLine">
				<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="homePhoneEF" value="#{pc_primaryInsured.contactInfo.homePhone}" styleClass="formEntryText" style="width: 150px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					<f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
				</h:inputText>
				<h:outputText id="bestDayToCall1" styleClass="formLabel" value="#{property.appEntBestDayToCall}" style="width: 170px;"/>
				<h:selectOneMenu id="bestDayToCall1DD" value ="#{pc_primaryInsured.contactInfo.bestDayToCallHome}" styleClass="formEntryText" style="width: 150px;">
					<f:selectItems id="bestDayToCall1List" value="#{pc_primaryInsured.contactInfo.bestDayToCallList}" />
				</h:selectOneMenu>				
		</h:panelGroup>
		<h:panelGrid id="pinsHomePhonePGrid" columnClasses="formDataEntryLineAppEntry" columns="4" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputText id="from1" styleClass="formLabel" value="#{property.appEntFrom}" style="width: 100px;"/>
				<h:inputText id="from1EF" value="#{pc_primaryInsured.contactInfo.fromHome}" styleClass="formEntryText" style="width: 55px;">
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioFromHome" value="#{pc_primaryInsured.contactInfo.fromHomeTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioFromHomeTimePeriods" value="#{pc_primaryInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputText id="to1" styleClass="formLabel" value="#{property.appEntTo}" style="width: 170px;"/>
				<h:inputText id="to1EF" styleClass="formEntryText" style="width: 45px;" value="#{pc_primaryInsured.contactInfo.toHome}" >			
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>				
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioToHome" value="#{pc_primaryInsured.contactInfo.toHomeTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioToHomeTimePeriods" value="#{pc_primaryInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>	
		</h:panelGrid>
		<h:panelGroup id="pinsWorkPhonePGroup" styleClass="formDataEntryLine">
				<h:outputText id="workPhone" value="#{property.appEntWorkPhone}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="workPhoneEF" value="#{pc_primaryInsured.contactInfo.workPhone}" styleClass="formEntryText" style="width: 150px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					<f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
				</h:inputText>
				<h:outputText id="bestDayToCall2" styleClass="formLabel" value="#{property.appEntBestDayToCall}" style="width: 170px;"/>
				<h:selectOneMenu id="bestDayToCall2DD" value ="#{pc_primaryInsured.contactInfo.bestDayToCallWork}" styleClass="formEntryText" style="width: 150px;">
					<f:selectItems id="bestDayToCall2List" value="#{pc_primaryInsured.contactInfo.bestDayToCallList}" />
				</h:selectOneMenu>				
		</h:panelGroup>
		<h:panelGrid id="pinsworkPhonePGrid" columnClasses="formDataEntryLineAppEntry" columns="4" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputText id="from2" styleClass="formLabel" value="#{property.appEntFrom}" style="width: 100px;"/>
				<h:inputText id="from2EF" value="#{pc_primaryInsured.contactInfo.fromWork}" styleClass="formEntryText" style="width: 55px;">
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioFromWork" value="#{pc_primaryInsured.contactInfo.fromWorkTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioFromWorkTimePeriods" value="#{pc_primaryInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputText id="to2" styleClass="formLabel" value="#{property.appEntTo}" style="width: 170px;"/>
				<h:inputText id="to2EF" styleClass="formEntryText" style="width: 45px;" value="#{pc_primaryInsured.contactInfo.toWork}"> 			
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>				
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioToWork" value="#{pc_primaryInsured.contactInfo.toWorkTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioToWorkTimePeriods" value="#{pc_primaryInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>					
		</h:panelGrid>
		<!-- Begin FNB002  -->
		<h:panelGroup id="pinsCallAtPGroup" styleClass="formDataEntryLine">
					<h:outputText id="callAt" styleClass="formLabel" value="#{property.appEntCallAt}" style="width: 100px;"/>
					<h:selectOneMenu id="callAtDD" value ="#{pc_primaryInsured.demographics.callAtTypCode}" styleClass="formEntryText" style="width: 150px;">
						<f:selectItems id="callAtList" value="#{pc_primaryInsured.demographics.callAtTypList}" />
 					</h:selectOneMenu>
					<h:outputText id="contactInfo" value="#{property.appEntContactInfo}" styleClass="formLabel" style="width: 170px;"/>
					<h:inputText id="contactInfoEF" value="#{pc_primaryInsured.demographics.additionalcontactInfo}" styleClass="formEntryText" style="width: 150px;" 
						disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" />
		</h:panelGroup>
		<!-- End FNB002  -->				
		<f:verbatim>
				<hr id="primaryInsuredSeparator3" class="formSeparator" />
		</f:verbatim>
</jsp:root>
