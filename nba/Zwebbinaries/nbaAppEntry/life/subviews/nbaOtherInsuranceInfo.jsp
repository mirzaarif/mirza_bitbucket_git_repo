<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- SPRNBA-554		NB-1301   Error Pop Up "Policy Type selection required please correct" Not Received -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
		xmlns:DynaDiv="/WEB-INF/tld/DynamicDiv.tld">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
	<h:outputText id="otherInsuranceTitle" value="#{property.appEntOtherInsuranceInformation}" styleClass="formSectionBar" style="width: 575px;"/>
	<h:panelGrid columns="2" border="0" style="width: 575px" styleClass="formDataEntryTopLine">
		<h:column>			
			<h:outputText id="question1" value="#{property.appEntOtherInsuranceInfoQues1}" styleClass="formLabel" style="width: 450px;text-align: left;"></h:outputText>
		</h:column>
		<h:column>
			<h:selectOneRadio id="answer1" value="#{pc_otherInsuranceInfo.otherInsuranceQuesA}" layout="lineDirection" styleClass="radioLabel" 
					style="width: 95px;" valueChangeListener="#{pc_otherInsuranceInfo.collapseOtherInsuranceFields}" onclick="resetTargetFrame();submit();">
				<f:selectItems id="oiAnswer1Options" value="#{pc_otherInsuranceInfo.questionOptions}"/>			
			</h:selectOneRadio>				
		</h:column>
		<h:column>
			<h:outputText id="question2" value="#{property.appEntOtherInsuranceInfoQues2}" styleClass="formLabel" style="width: 450px;text-align: left;"></h:outputText>
		</h:column>
		<h:column>
			<h:selectOneRadio id="answer2" value="#{pc_otherInsuranceInfo.otherInsuranceQuesB}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
				<f:selectItems id="oiAnswer2Options" value="#{pc_otherInsuranceInfo.questionOptions}"/>
			</h:selectOneRadio>				
		</h:column>
	</h:panelGrid>
	<h:panelGroup id="collapseImage">
		<h:outputText id="blankImage" value="" styleClass="formLabel" style="width: 550px;"></h:outputText>
		<h:commandLink  id="collapseImg" onmousedown="getScrollXY('form_appEntryULPage3');" style="margin-right: 5px" action="#{pc_otherInsuranceInfo.imageClicked}">
			<h:graphicImage url="images/arrow_down.gif" style="border-width: 0px;" id="img"/>
		</h:commandLink>
	</h:panelGroup>
<DynaDiv:DynamicDiv id="otherInsSection" rendered="#{pc_otherInsuranceInfo.expandInd}">
<h:panelGrid id="otherInsPGroup" columns="2" styleClass="formDataEntryLine" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0" style="width: 595px;">
<h:column>
	<h:dataTable id="otherInsurance" styleClass="formTableData" cellspacing="0" cellpadding="0" border="0" var="otherInsurance" 
			style="width: 520;padding-left: 2px;padding-top: 10px" value="#{pc_otherInsuranceInfo.otherInsuranceList}">
		<h:column>
			<h:panelGroup id="otherInsuranceBar" styleClass="formDataEntryLine" rendered="#{otherInsurance.displayBar}">
				<f:verbatim>
						<hr id="separator1" class="formSeparator" />
				</f:verbatim>
			</h:panelGroup>
			<h:selectBooleanCheckbox id="deleteOtherInsurance" value="#{otherInsurance.deleteOtherIns}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 390px;width: 25px;" /><!-- NBA175 -->														
			<h:outputText id="delOtherInsurance" value="#{property.appEntDelOtherInsurance}" styleClass="formLabel" style="position: relative;left: 385px;width: 150px;" /><!-- NBA175 -->
			<h:panelGroup id="otherInsurancePGroup" styleClass="formDataEntryLine">
				<h:outputLabel id="insured" value="#{property.appEntOtherInsured}" styleClass="formLabel" style="width: 170px;" />
				<h:selectOneMenu id="insuredEF" value="#{otherInsurance.insured}" styleClass="formEntryText" style="width: 280px;">
					<f:selectItems id="oiInsuredList" value="#{otherInsurance.insuredList}"/>
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine">
				<h:outputLabel id="insuranceCompanyName" value="#{property.appEntOtherInsuranceCompanyName}" styleClass="formLabel" style="width: 170px;" />
				<h:inputText id="insuranceCompanyNameEF" value="#{otherInsurance.insuranceCompanyName}" styleClass="formEntryText" style="width: 280px;"/>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine">
				<h:outputLabel id="contractNumber" value="#{property.appEntContractNumber}" styleClass="formLabel" style="width: 170px;" />
				<h:inputText id="contractNumberEF" value="#{otherInsurance.contractNumber}" styleClass="formEntryText" style="width: 140px;"/>
				<h:outputLabel id="amount" value="#{property.appEntAmount}" styleClass="formLabel" style="width: 70px;" />
				<h:inputText id="amountEF" value="#{otherInsurance.amount}" styleClass="formEntryText" style="width: 130px;"> <!-- SPRNBA-554 -->
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/> <!-- SPRNBA-554 -->
				</h:inputText> <!-- SPRNBA-554 -->
			</h:panelGroup>
			<h:panelGroup id="otherInsurancePGroup1" >
			</h:panelGroup>
			<h:panelGroup id="otherInsurancePGroup2" styleClass="formDataEntryLine">
				<h:outputLabel id="contractType" value="#{property.appEntContractType}" styleClass="formLabel" style="width: 170px;" />
				<h:selectOneMenu id="contractTypeEF" value="#{otherInsurance.contractType}" styleClass="formEntryText" style="width: 140px;">
					<f:selectItems id="oiContractTypeList" value="#{otherInsurance.contractTypeList}"/>
				</h:selectOneMenu>
				<h:outputLabel id="issueDate" value="#{property.appEntIssueDate}" styleClass="formLabel" style="width: 85px;" />
				<h:inputText id="issueDateEF" value="#{otherInsurance.issueDate}" styleClass="formEntryText" style="width: 115px;">
					<f:convertDateTime pattern="#{property.datePattern}"/>
				</h:inputText>
			</h:panelGroup>
			<h:panelGrid id="otherInsurancePGroup3" columns="2" border="0" >
				<h:column>
					<h:outputLabel id="purpose" value="#{property.appEntPurpose}" styleClass="formLabel" style="width: 167px;" />
					<h:selectOneMenu id="purposeEF" value="#{otherInsurance.purpose}" styleClass="formEntryText" style="width: 140px;">
						<f:selectItems id="oiPurposeList" value="#{otherInsurance.purposeList}"/>
					</h:selectOneMenu>
				</h:column>
				<h:column>
					<h:outputText id="radioBlank" value="" styleClass="formEntryText" style="width: 40px;"></h:outputText>
					<h:selectOneRadio id="selectRadioGroup" value="#{otherInsurance.individualOrGroup}" layout="lineDirection" styleClass="radioLabel" style="width: 160px;margin-top: -10px;">
						<f:selectItem id="individual" itemValue="1" itemLabel="Individual"/>
						<f:selectItem id="group" itemValue="2" itemLabel="Group"/>
					</h:selectOneRadio>	
				</h:column>
			</h:panelGrid>
			<h:panelGroup id="otherInsurancePGroup4" styleClass="formDataEntryLine">
				<h:outputText id="checkBoxBlank" value="" styleClass="formLabel" style="width: 100px;"/>

				<h:selectBooleanCheckbox id="pendingCB" value="#{otherInsurance.pending}" 
						styleClass="ovFullCellSelectCheckBox" style="width: 25px"/>
				<h:outputText id="usCitizen" value="#{property.appEntPending}" styleClass="formLabel" style="width: 80px;text-align: left;"/>

				<h:selectBooleanCheckbox id="replacementCB" value="#{otherInsurance.replacement}" 
						styleClass="ovFullCellSelectCheckBox" style="width: 25px"/>
				<h:outputText id="replacement" value="#{property.appEntReplacement}" styleClass="formLabel" style="width: 100px;text-align: left;"/>

				<h:selectBooleanCheckbox id="exchangeCB" value="#{otherInsurance.exchange}"  
						styleClass="ovFullCellSelectCheckBox" style="width: 25px"/>
				<h:outputText id="exchange" value="#{property.appEnt1035Exchange}" styleClass="formLabel" style="width: 120px;text-align: left;"/>
			</h:panelGroup>
		</h:column>
	</h:dataTable>
</h:column>
<h:column>
	<h:commandLink value="#{property.buttonAddAnother}" action="#{pc_otherInsuranceInfo.addAnother}" styleClass="formButtonInterface" 
		onmousedown="getScrollXY('form_appEntryULPage3');"	style="margin-bottom: 10px;width: 75px;"/>
</h:column>
</h:panelGrid>
</DynaDiv:DynamicDiv>
	
</jsp:root>
