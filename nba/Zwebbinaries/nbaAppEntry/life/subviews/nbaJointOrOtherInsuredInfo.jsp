<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->
<!-- SPR3265           8      Standard Phone Number and Social  Security Number Format Should be Used and Tab Order Corrected from First to Middle Name -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- FNB002 		NB-1101	  App Entry  -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-327		NB-1601	  Verification Drop Down Should Be Moved to Right of Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="otherInsuredInfo" value="#{property.appEntOtherInsInfo}" styleClass="formSectionBar"/>
		<h:panelGroup id="jointInsPGroup" styleClass="formDataEntryLine">
			<h:selectBooleanCheckbox id="jointInsCheck" value="#{pc_jointOrOtherInsured.jointInsured}" styleClass="radioLabel" style="width: 25px;" 
					onchange="resetTargetFrame();submit();" disabled="true"> 
			</h:selectBooleanCheckbox>
			<h:outputText id="jointInsCB" value="#{property.appEntJointIns}" styleClass="formLabel" style="width: 90px"/>
			<h:selectBooleanCheckbox id="deleteOthIns" value="#{pc_jointOrOtherInsured.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 320px;width: 25px;" 
				disabled="#{pc_jointOrOtherInsured.disabled || pc_nbaAppEntryNavigation.partialUpdateDisabled}" valueChangeListener="#{pc_jointOrOtherInsured.delPartyAddrDis}" onclick="resetTargetFrame();submit()" />
			<h:outputText id="delOtherInsured" value="#{property.appEntDelOthIns}" styleClass="formLabel" style="position: relative;left: 305px;width: 150px;"/>
		</h:panelGroup>
		<h:panelGroup id="otherInsuredNamePGroup" styleClass="formDataEntryLine">
			<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="firstNameEF" value="#{pc_jointOrOtherInsured.demographics.firstName}" styleClass="formEntryText" style="width: 100px;"
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_jointOrOtherInsured.firstNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:inputHidden id="firstNameHD" value="#{pc_jointOrOtherInsured.firstNameHidden}"></h:inputHidden><!-- SPR3265 -->
			<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="middelNameEF" value="#{pc_jointOrOtherInsured.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_jointOrOtherInsured.middleNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:inputHidden id="middleNameHD" value="#{pc_jointOrOtherInsured.middleNameHidden}"></h:inputHidden><!-- SPR3265 -->
			<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;" />
			<h:inputText id="lastNameEF" value="#{pc_jointOrOtherInsured.demographics.lastName}" styleClass="formEntryText" style="width: 175px;"
				onchange="resetTargetFrame();submit();" valueChangeListener="#{pc_jointOrOtherInsured.lastNameChange}"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:inputHidden id="lastNameHD" value="#{pc_jointOrOtherInsured.lastNameHidden}"></h:inputHidden><!-- SPR3265 -->
		</h:panelGroup>
		
		<!-- Begin FNB002  -->
		<h:panelGroup id="pinsnickNamePGroup" styleClass="formDataEntryLine">
			<h:outputText id="nickName" value="#{property.appEntNickName}" styleClass="formLabel" style="width: 100px"/>
			<h:inputText id="nickNameEF" value="#{pc_jointOrOtherInsured.demographics.nickName}" styleClass="formEntryText" style="width: 100px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" maxlength="20"/>
		</h:panelGroup>		
		<!-- end FNB002  -->
		
		<h:panelGroup id="otherInsuredLine1PGroup" styleClass="formDataEntryLine">
			<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="addressLine1" value="#{pc_jointOrOtherInsured.address.addressLine1}" styleClass="formEntryText" style="width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:panelGroup id="deleteAdd" style="position: relative;left: 95px;width: 170px;"><!-- begin NBA175  -->
				<h:selectBooleanCheckbox id="deleteAddress" value="#{pc_jointOrOtherInsured.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" 
					disabled="#{pc_jointOrOtherInsured.addressDisabled}" valueChangeListener="#{pc_jointOrOtherInsured.delPartyDis}" onclick="resetTargetFrame();submit()" />
				<h:outputText id="delAddress" value="#{property.appEntDelOthInsAdd}" styleClass="formLabel" style="width: 100px;text-align: left;"/>
			</h:panelGroup>	<!-- end NBA175  -->
		</h:panelGroup>
		<h:panelGroup id="otherInsuredLine2PGroup" styleClass="formDataEntryLine">
			<h:inputText id="addressLine2" value="#{pc_jointOrOtherInsured.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 100px;width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:outputText id="years" value="#{property.appEntYearAtCurrentAdd}" styleClass="formLabel" style="position: relative;left: 100px;width: 170px;"/><!-- NBA175 -->
			<h:inputText id="yearsEF" value="#{pc_jointOrOtherInsured.address.yearsAtCurrentAdd}" styleClass="formEntryText" style="position: relative;left: 100px;width: 40px;"/><!-- NBA175 -->
		</h:panelGroup>
		<h:panelGroup id="otherInsuredLine3PGroup" styleClass="formDataEntryLine">
			<h:inputText id="addressLine3" value="#{pc_jointOrOtherInsured.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
		</h:panelGroup>
		<!-- begin NBA247 -->
		<h:panelGroup id="otherInsuredLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
			<h:inputText id="addressLine4" value="#{pc_jointOrOtherInsured.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
		</h:panelGroup>
		<!-- end NBA247 -->
		<h:panelGroup id="otherInsuredCityPGroup" styleClass="formDataEntryLine">
			<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="city1EF" value="#{pc_jointOrOtherInsured.address.city}" styleClass="formEntryText" style="width: 95px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
			<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 130px;"/>
			<h:selectOneMenu id="state1DD" value ="#{pc_jointOrOtherInsured.address.state}" styleClass="formEntryTextFull" style="width: 260px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
				<f:selectItems id="state1List" value="#{pc_jointOrOtherInsured.address.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGrid id="otherInsCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" style="padding-top: 6px;"><!-- NBA316 -->
			<h:column>
				<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 100px;"/>
			</h:column>
			<h:column>
				<h:selectOneRadio id="otherInsZipRB" value="#{pc_jointOrOtherInsured.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
					<f:selectItems id="otherInsZipTypes" value="#{pc_jointOrOtherInsured.address.zipTypeCodes}" />
				</h:selectOneRadio>				
			</h:column>
			<h:column>
				<!-- begin SPRNBA-493 -->
				<h:inputText id="zipEF" value="#{pc_jointOrOtherInsured.address.zip}" styleClass="formEntryText" style="width: 85px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" ><!-- NBA211 -->
					<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> 
				</h:inputText> 
				<h:inputText id="postalEF" value="#{pc_jointOrOtherInsured.address.postal}" styleClass="formEntryText" style="width: 85px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->
				<!-- end SPRNBA-493 -->
			</h:column>
			<h:column>
				<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 69px;"/> <!-- SPRNBA-493 -->
				<h:outputText id="county" value="#{property.appEntCounty}"
				styleClass="formLabel" style="width: 70px;margin-top: 10px;" /><!-- NBA316 -->
			</h:column>
			<h:column>
				<h:selectOneMenu id="countryDD" value ="#{pc_jointOrOtherInsured.address.country}" styleClass="formEntryTextFull" style="width: 260px;"
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
						<f:selectItems id="countryList" value="#{pc_jointOrOtherInsured.address.countryList}" />
				</h:selectOneMenu>	
				<h:inputText id="countyEF"
					value="#{pc_jointOrOtherInsured.address.county}" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
					styleClass="formEntryText" maxlength="100" style="width: 260px;margin-top: 5px;" />	<!-- NBA316 -->			
			</h:column>
		</h:panelGrid>			
		<h:panelGroup id="otherInsuredMailPGroup" styleClass="formDataEntryLine">
			<h:outputText id="mailAdd" value="#{property.appEntEmail}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="mailAddEF" value="#{pc_jointOrOtherInsured.email}" styleClass="formEntryText" style="width: 275px;" 
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/><!-- NBA211 -->			
		</h:panelGroup>					
		<f:verbatim>
			<hr id="jointInsuredSeparator1" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="otherInsuredDobPGroup" styleClass="formDataEntryLine">
			<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="dobEF" value="#{pc_jointOrOtherInsured.demographics.birthDate}" styleClass="formEntryText" style="width: 100px;" 	
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputText id="birthState" value="#{property.appEntBirthState}" styleClass="formLabel" style="width: 100px;"/>
			<h:selectOneMenu id="birthStateDD" value="#{pc_jointOrOtherInsured.demographics.birthState}" styleClass="formEntryText" style="width: 225px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
				<f:selectItems id="birthStateList" value="#{pc_jointOrOtherInsured.address.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="otherInsBirthCountryPGroup" styleClass="formDataEntryLine">
			<h:outputText id="birthCountry" value="#{property.appEntBirthCountry}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="birthCountryDD" value ="#{pc_jointOrOtherInsured.demographics.birthCountry}" styleClass="formEntryText" style="width: 425px;">
				<f:selectItems id="birthCountryList" value="#{pc_jointOrOtherInsured.address.countryList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="otherInsHeightPGroup" styleClass="formDataEntryLine">
			<h:outputText id="gender" value="#{property.appEntGender}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="genderDD" value ="#{pc_jointOrOtherInsured.demographics.gender}" styleClass="formEntryText" style="width: 90px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->						
				<f:selectItems id="genderList" value="#{pc_jointOrOtherInsured.demographics.genderList}" />
			</h:selectOneMenu>					
			
			<h:outputText id="height" value="#{property.appEntHeight}" styleClass="formLabel" style="width: 90px;"/>
			<h:inputText id="heightEF1" value="#{pc_jointOrOtherInsured.demographics.heightInFeet}" styleClass="formEntryText" style="width: 50px;" />
			<h:inputText id="heightEF2"  value="#{pc_jointOrOtherInsured.demographics.heightInInch}" styleClass="formEntryText" style="width: 50px;" />
			<h:outputText id="weight" value="#{property.appEntWeight}" styleClass="formLabel" style="width: 90px;"/>
			<h:inputText id="weightEF" value="#{pc_jointOrOtherInsured.demographics.weight}" styleClass="formEntryText" style="width: 50px;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsrMaidenPGroup" styleClass="formDataEntryLine">
			<h:outputText id="maidenLN" value="#{property.appEntMaidenLN}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="maidenLNEF" value="#{pc_jointOrOtherInsured.demographics.maidenLName}" styleClass="formEntryText" style="width: 160px;" />															
			<h:outputText id="maritalStatus" value="#{property.appEntMaritalStatus}" styleClass="formLabel" style="width: 140px;"/>
			<h:selectOneMenu id="maritalStatusDD" value="#{pc_jointOrOtherInsured.demographics.marStatus}" styleClass="formEntryText" style="width: 125px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 -->
				<f:selectItems id="maritalStatusList" value="#{pc_jointOrOtherInsured.demographics.marStatusList}" />
			</h:selectOneMenu>				
		</h:panelGroup>
		<!-- NBA247 -->
		<h:panelGroup id="otherRateClassPGroup" styleClass="formDataEntryLine">
		<h:outputText id="rateClass" value="#{property.appEntRateAppliedFor}" styleClass="formLabel" style="width: 158px;" />
				<h:selectOneMenu id="rateClassDD" value="#{pc_jointOrOtherInsured.demographics.appliedRateClass}" styleClass="formEntryText" style="width: 125px;">					
					<f:selectItems id="rateClassList" value="#{pc_jointOrOtherInsured.demographics.appliedRateClassList}"/>
				</h:selectOneMenu>		
		</h:panelGroup>
		<!-- NBA247 -->
		<h:panelGrid id="otherInsGovtIdPGrid" columnClasses="formDataEntryLineAppEntry" columns="3">
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_jointOrOtherInsured.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 175px;"
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 NBA340 -->
						<f:selectItem id="ssnRB" itemValue="1" itemLabel="Social Security Number"/>
						<f:selectItem id="sinRB" itemValue="3" itemLabel="Social Insurance Number"/> <!-- NBA340 -->
				</h:selectOneRadio>
			</h:column>
			<h:column>
			<!-- begin NBA340 -->
				<h:inputText id="govtIdInput" value="#{pc_jointOrOtherInsured.demographics.ssn}" 
					styleClass="formEntryText" 
					style="width: 90px;margin-top: 20px;margin-bottom: 0px" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211, SPR3265, NBA247, SPRNBA-327 -->
				</h:inputText><!-- SPR3265 -->
				<h:inputHidden id="govtIdKey" value="#{pc_jointOrOtherInsured.demographics.govtIdKey}"></h:inputHidden>
			<!-- end NBA340 -->
			</h:column>
			<h:column>	
				<!-- begin NBA247 -->
				<h:outputText id="taxVerification" value="Verification" styleClass="formLabel" style="width: 90px;margin-top: 20px;"/> <!-- SPRNBA-327 -->
				<h:selectOneMenu id="taxVerificationDD" value ="#{pc_jointOrOtherInsured.demographics.taxIDVerification}" styleClass="formEntryTextFull" style="width: 240px;margin-top: 20px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}">
					<f:selectItems id="taxVerificationList" value="#{pc_jointOrOtherInsured.demographics.taxIDVerificationList}" />
				</h:selectOneMenu>
				<!-- end NBA247 -->							 		
			</h:column>
		</h:panelGrid>
		<h:panelGroup id="otherInsLicenseNumPGroup" styleClass="formDataEntryLine">
			<h:outputText id="licenseNumber" value="#{property.appEntLicenseNumber}" styleClass="formLabel" style="width: 158px;"/>
			<h:inputText id="licenseNumberEF" value="#{pc_jointOrOtherInsured.demographics.licenseNumber}" styleClass="formEntryText" style="width: 155px;" />
			<h:outputText id="otherInsBlank" value="" style="width: 80px;"/>
			<h:selectBooleanCheckbox id="usCitizenCB" value="#{pc_jointOrOtherInsured.demographics.usCitizen}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"/>
			<h:outputText id="usCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;text-align: left;"/>
		</h:panelGroup>
		<h:panelGroup id="otherInsCitizenPGroup" styleClass="formDataEntryLine">
			<h:outputText id="licenseState" value="#{property.appEntLicenseState}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="licenseStateDD" value ="#{pc_jointOrOtherInsured.demographics.licenseState}" styleClass="formEntryText" style="width: 190px;">
				<f:selectItems id="stateList" value="#{pc_jointOrOtherInsured.address.stateList}" />
			</h:selectOneMenu>
		
			<h:outputText id="citizenship" value="#{property.appEntCitizenship}" styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="citizenshipDD" value ="#{pc_jointOrOtherInsured.demographics.usCitizenship}" 
							 styleClass="formEntryText" 
							 style="width: 125px;"> 
				<f:selectItems id="citizenshipList" value="#{pc_jointOrOtherInsured.demographics.usCitizenshipList}" />
			</h:selectOneMenu>
		</h:panelGroup>
	
		<h:panelGroup id="otherInsVisaPGroup" styleClass="formDataEntryLine">
			<h:outputText id="visaType" value="#{property.appEntVisaType}" styleClass="formLabel" style="width: 158px;"/>
			<h:selectOneMenu id="visaTypeDD" value ="#{pc_jointOrOtherInsured.demographics.visaType}" styleClass="formEntryText" style="width: 190px;">
				<f:selectItems id="visaTypeList" value="#{pc_jointOrOtherInsured.demographics.visaTypeList}" />
			</h:selectOneMenu>
			<h:outputText id="arrivalDate" value="#{property.appEntArrivalDate}" styleClass="formLabel" style="width: 110px;"/>
			<h:inputText id="arrivalDateEF" value="#{pc_jointOrOtherInsured.demographics.arrivalDate}" styleClass="formEntryText" style="width: 125px;">			
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>			
		</h:panelGroup>
		<f:verbatim>
			<hr id="jointInsuredSeparator2" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="otherInsHomePhonePGroup" styleClass="formDataEntryLine">
			<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 100px;"/>
			<h:inputText id="homePhoneEF" value="#{pc_jointOrOtherInsured.contactInfo.homePhone}" styleClass="formEntryText" style="width: 150px;" 
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 --><!-- SPR3265 -->
				<f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/><!-- SPR3265 -->
			</h:inputText><!-- SPR3265 -->
			<h:outputText id="bestDayToCall1" styleClass="formLabel" value="#{property.appEntBestDayToCall}" style="width: 170px;"/>
			<h:selectOneMenu id="bestDayToCall1DD" value ="#{pc_jointOrOtherInsured.contactInfo.bestDayToCallHome}" styleClass="formEntryText" style="width: 150px;">
					<f:selectItems id="bestDayToCall1List" value="#{pc_jointOrOtherInsured.contactInfo.bestDayToCallList}" />
			</h:selectOneMenu>				
		</h:panelGroup>
		<h:panelGrid id="otherInsHomePhonePGrid" columnClasses="formDataEntryLineAppEntry" columns="4" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputText id="from1" styleClass="formLabel" value="#{property.appEntFrom}" style="width: 100px;"/>
				<h:inputText id="from1EF" value="#{pc_jointOrOtherInsured.contactInfo.fromHome}" styleClass="formEntryText" style="width: 55px;">
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioFromHome" value="#{pc_jointOrOtherInsured.contactInfo.fromHomeTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioFromHomeTimePeriods" value="#{pc_jointOrOtherInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputText id="to1" styleClass="formLabel" value="#{property.appEntTo}" style="width: 170px;"/>
				<h:inputText id="to1EF" value="#{pc_jointOrOtherInsured.contactInfo.toHome}" styleClass="formEntryText" style="width: 45px;">			
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>				
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioToHome" value="#{pc_jointOrOtherInsured.contactInfo.toHomeTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioToHomeTimePeriods" value="#{pc_jointOrOtherInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>	
		</h:panelGrid>
		<h:panelGroup id="otherInsWorkPhonePGroup" styleClass="formDataEntryLine">
				<h:outputText id="workPhone" value="Work Phone:" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="workPhoneEF" value="#{pc_jointOrOtherInsured.contactInfo.workPhone}" styleClass="formEntryText" style="width: 150px;" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"><!-- NBA211 --><!-- SPR3265 -->
					<f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/><!-- SPR3265 -->
				</h:inputText><!-- SPR3265 -->
				<h:outputText id="bestDayToCall2" styleClass="formLabel" value="#{property.appEntBestDayToCall}" style="width: 170px;"/>
				<h:selectOneMenu id="bestDayToCall2DD" value ="#{pc_jointOrOtherInsured.contactInfo.bestDayToCallWork}" styleClass="formEntryText" style="width: 150px;">
					<f:selectItems id="bestDayToCall2List" value="#{pc_jointOrOtherInsured.contactInfo.bestDayToCallList}" />				
				</h:selectOneMenu>				
		</h:panelGroup>
		<h:panelGrid id="otherInsWorkPhonePGrid" columnClasses="formDataEntryLineAppEntry" columns="4" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputText id="from2" styleClass="formLabel" value="#{property.appEntFrom}" style="width: 100px;"/>
				<h:inputText id="from2EF" value="#{pc_jointOrOtherInsured.contactInfo.fromWork}" styleClass="formEntryText" style="width: 55px;">
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioFromWork" value="#{pc_jointOrOtherInsured.contactInfo.fromWorkTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioFromWorkTimePeriods" value="#{pc_jointOrOtherInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputText id="to2" styleClass="formLabel" value="#{property.appEntTo}" style="width: 170px;"/>
				<h:inputText id="to2EF" styleClass="formEntryText" style="width: 45px;" value="#{pc_jointOrOtherInsured.contactInfo.toWork}">			
					<f:convertDateTime pattern="#{property.shorttimePattern}" type="time"/>
				</h:inputText>				
			</h:column>
			<h:column>
				<h:selectOneRadio id="radioToWork" value="#{pc_jointOrOtherInsured.contactInfo.toWorkTimeType}" layout="lineDirection" styleClass="radioLabel" style="width: 95px;">
					<f:selectItems id="radioToWorkTimePeriods" value="#{pc_jointOrOtherInsured.contactInfo.timePeriods}" />
				</h:selectOneRadio>
			</h:column>					
		</h:panelGrid>
		
		<!-- Begin FNB002  -->
		<h:panelGroup id="pinsCallAtPGroup" styleClass="formDataEntryLine">
					<h:outputText id="callAt" styleClass="formLabel" value="#{property.appEntCallAt}" style="width: 100px;"/>
					<h:selectOneMenu id="callAtDD" value ="#{pc_jointOrOtherInsured.demographics.callAtTypCode}" styleClass="formEntryText" style="width: 150px;">
						<f:selectItems id="callAtList" value="#{pc_jointOrOtherInsured.demographics.callAtTypList}" />
 					</h:selectOneMenu>
					<h:outputText id="contactInfo" value="#{property.appEntContactInfo}" styleClass="formLabel" style="width: 170px;"/>
					<h:inputText id="contactInfoEF" value="#{pc_jointOrOtherInsured.demographics.additionalcontactInfo}" styleClass="formEntryText" style="width: 150px;" 
						disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}" />
		</h:panelGroup>	
		<!-- end FNB002  -->
		
</jsp:root>