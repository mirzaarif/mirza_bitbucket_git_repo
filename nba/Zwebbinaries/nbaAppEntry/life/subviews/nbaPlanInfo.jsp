<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA211            7      Partial Application -->
<!-- NBA175			   7	  Traditional Life Application Entry Rewrite-->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- NBA280 		NB-1301   Integration Upgrade to Product Accelerator 1101 -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="properties" />
		<h:outputText id="PlanInfoTitle" value="#{property.appEntPlanTitle}" styleClass="formSectionBar" style="width: 575px;"/>
		<h:panelGroup id="pinsPlanPGroup" styleClass="formDataEntryLine" >
			<h:outputText id="plan" value="#{property.appEntPlan}"  styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="planVal" value="#{pc_planInfo.plan}" style="width: 465px;" styleClass="formEntryText" 
						disabled="#{pc_planInfo.riderDisabled  || pc_planInfo.jointInsured ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
						valueChangeListener="#{pc_planInfo.planChange}" onchange="resetTargetFrame();submit();" immediate="true"
						binding="#{pc_planInfo.planInfoCC}"> <!-- NBA211 --><!-- NBA175 -->
				<f:selectItems id="planValList" value="#{pc_planInfo.planList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		<!-- begin NBA247 -->
		<h:panelGroup id="pinsQualPGroup" styleClass="formDataEntryLine" rendered="#{pc_planInfo.primaryInsured}" >
			<h:outputText id="qual" 
						 value="#{property.appEntQualType}"  
						 styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="qualVal" 
						value="#{pc_planInfo.qualificationType}" 
						style="width: 465px;" styleClass="formEntryText" 
						disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 --><!-- NBA175 -->
				<f:selectItems id="qualValList" value="#{pc_planInfo.qualificationTypeList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		<!-- end NBA247 -->
		<h:panelGrid columns="3" id="pinsPlanAmountGrid" cellpadding="0" cellspacing="0" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA247, NBA280 -->
			<h:outputText id="Amount" value="#{property.appEntAmount}"  styleClass="formLabel" style="width: 110px; margin-top: 5px;"/> <!-- NBA280-->
			<h:inputText id="AmountVal" value="#{pc_planInfo.amount}" styleClass="formEntryText" style="width: 120px;  margin-top: 5px;" 
				disabled="#{pc_planInfo.benefitOrLifeParticipantSelected  || pc_planInfo.jointInsured || pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211, NBA280 -->
				<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
			</h:inputText>
			<!-- begin NBA280 -->
			<h:panelGroup id="overrideDefaultLoanGroup" styleClass="formDataEntryLine" rendered="#{pc_planInfo.overrideDefaultLoanRendered}">
				<h:outputText id="overrideDefaultLoan" value="#{property.appEntOverrideDefaultLoan}"  styleClass="formLabel" style="width: 105px;"/>
				<h:selectOneMenu id="overrideDefaultLoanVal" value="#{pc_planInfo.overrideDefaultLoan}" style="width: 240px; margin-top: 2px;" styleClass="formEntryText"
					disabled="#{pc_planInfo.overrideDefaultLoanDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}">
					<f:selectItems id="overrideDefaultLoanValList" value="#{pc_planInfo.overrideDefaultLoanList}"/>
				</h:selectOneMenu>
			</h:panelGroup>
			<!-- end NBA280 -->
		</h:panelGrid>
		<!-- begin NBA247 -->
		<h:panelGroup id="pinsPlanUnitsGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}" >
			<h:outputText id="Units" value="#{property.appEntUnits}"  styleClass="formLabel" style="width: 110px;"/> 
			<h:inputText id="UnitsVal" value="#{pc_planInfo.units}" styleClass="formEntryText" style="width: 120px;" 
				disabled="#{pc_planInfo.benefitOrLifeParticipantSelected  || pc_planInfo.jointInsured || pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
			</h:inputText>			
		</h:panelGroup>
		<!-- enNBA247 -->
		<h:panelGroup id="pinsPlanDeathBenefitPGroup" styleClass="formDataEntryLine" rendered="#{pc_planInfo.primaryInsured}">
			<h:outputText id="deathBenefit" value="#{property.appEntDeathBenefit}"  styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="deathBenefitVal" value="#{pc_planInfo.deathBenefit}" style="width: 230px;" styleClass="formEntryText" 
				disabled="#{pc_planInfo.deathBenefitDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="deathBenefitValList" value="#{pc_planInfo.deathBenefitList}"/>
			</h:selectOneMenu>
			<h:outputText id="purpose" value="#{property.appEntPurpose}" styleClass="formLabel" style="width: 85px;"/> 
			<h:selectOneMenu id="purposeVal" value="#{pc_planInfo.purpose}" style="width: 150px;" styleClass="formEntryText" 
				disabled="#{!pc_planInfo.baseCoverageSelected}"> 
				<f:selectItems  id="purposeVALList" value="#{pc_planInfo.purposeList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="pinsPlanPurposePGroup" styleClass="formDataEntryLine" rendered="#{pc_planInfo.primaryInsured}">
			<h:outputText id="dividendOption" value="#{property.appEntDividendOption}" styleClass="formLabel" style="width: 110px"/>
			<h:selectOneMenu id="dividendOptionVal" value="#{pc_planInfo.dividendOption}" style="width: 230px;" styleClass="formEntryText" 
				disabled="#{pc_planInfo.dividendOptionDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="dividendOptionValList" value="#{pc_planInfo.dividendOptionList}"/>
			</h:selectOneMenu>
			<h:outputText id="business" value="#{property.appEntBusiness}" styleClass="formLabel" style="width: 85px;"/>
			<h:selectOneMenu id="businessVal" value="#{pc_planInfo.business}" style="width: 150px;" styleClass="formEntryText" 
				disabled="#{!pc_planInfo.baseCoverageSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="businessValList" value="#{pc_planInfo.businessList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="pinsPlanBenefitPGroup" styleClass="formDataEntryLine">
			<h:outputText id="benefit" value="#{property.appEntBenefit}" styleClass="formLabel" style="width: 110px;"/>
			<h:selectOneMenu id="benefitVal" value="#{pc_planInfo.benefit}" style="width: 315px;" styleClass="formEntryText" 
				disabled="#{pc_planInfo.benefitsDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
				<f:selectItems id="benefitValList" value="#{pc_planInfo.benefitList}"/>
			</h:selectOneMenu>
		<h:selectBooleanCheckbox id="jointBenefit" value="#{pc_planInfo.jointBenefit}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"
			rendered="#{!pc_planInfo.primaryInsured}" disabled="#{!pc_jointOrOtherInsured.jointInsured}" />
		<h:outputText id="usCitizen" value="Joint Benefit" styleClass="formLabel" style="width: 125px;text-align: left;" rendered="#{!pc_planInfo.primaryInsured}"/>
		</h:panelGroup>
		<h:panelGrid id="pinsBenefitPGroup" columnClasses="labelAppEntry,labelAppEntry,colMIBShort,colMIBLong" columns="4" cellpadding="0" cellspacing="0" style="padding-top: 12px;">
			<h:column>
				<h:outputText id="blankAmountUnits" style="width:110px;" value=""></h:outputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="pinsBenefitAmountUnitsRB" value="#{pc_planInfo.benefitUnitsRB}" layout="pageDirection" styleClass="radioLabel" style="width: 75px;" 
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
					<f:selectItem id="Item1" itemValue="1" itemLabel="Amount"/>
					<f:selectItem id="Item2" itemValue="2" itemLabel="Units"/>
				</h:selectOneRadio>				
			</h:column>
			<h:column>
				<h:inputText id="benefitAmountEF" value="#{pc_planInfo.benefitAmount}" styleClass="formEntryText" style="width: 120px;"	
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled  || pc_nbaAppEntryNavigation.vantageBESSystem}"> <!-- NBA211, NBA247 -->
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
				</h:inputText>
				<h:inputText id="benefitUnitsEF" value="#{pc_planInfo.benefitUnits}" styleClass="formEntryText" style="width: 120px;" 
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>
			</h:column>
			<h:column>
				<h:panelGroup styleClass="formDataEntryLine" >
					<h:outputText id="benefitPercent" value="Percent:" styleClass="formLabel" style="width: 120px;" rendered="#{pc_planInfo.percentRendered}" />
					<h:outputText id="benefitPercentEF" value="#{pc_planInfo.benefitPercent}" styleClass="formDisplayText" rendered="#{pc_planInfo.percentRendered}" >
						<f:convertNumber type="percent"/>
					</h:outputText>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
		<h:panelGroup styleClass="formDivTableHeaderRightButtons">
			<h:panelGrid columns="2" styleClass="formTableHeaderRightButtons" columnClasses="ovColHdrText300,ovColHdrText200" cellspacing="0">
				<h:outputLabel id="planInfoCol1Hdr" value="#{property.appEntPlanTableCol1}" styleClass="ovColSortedFalse" />
				<h:outputLabel id="planInfoCol2Hdr" value="#{property.appEntPlanTableCol2}" styleClass="ovColSortedFalse" />
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGrid id="pinsPlanInfoTablePGrid" columns="2" cellpadding="0" cellspacing="0" width="100%">
			<h:column>
				<h:panelGroup id="pGroup2" styleClass="formDivTableDataRightButtons5" style="height: 125px;">
					<h:dataTable id="primaryInsuredPlanTable" styleClass="formTableDataRightButtons" cellspacing="0"
								binding="#{pc_planInfo.planInfoTable}" value="#{pc_planInfo.rows}" style="min-height: 19px;"
								var="currRow" columnClasses="ovColText300,ovColText200" rowClasses="#{pc_planInfo.rowStyles}">
						<h:column>
							<h:panelGroup>
								<h:commandButton id="planInfoIcon1" image="images/hierarchies/#{currRow.icon1}" rendered="#{currRow.icon1Rendered}"
												action="#{pc_planInfo.selectRow}" 
												styleClass="ovViewIconTrue" style="margin-left: 5px; vertical-align: top" />
								<h:commandLink id="planInfoCol1" action="#{pc_planInfo.selectRow}">
									<h:inputTextarea id="planInfoCol1Text" readonly="true" value="#{currRow.col1}" styleClass="ovMultiLine#{currRow.draftText}"
												style="margin-top: 3px; margin-left: 2px; width: 270px" />
								</h:commandLink>
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:commandLink id="planInfoCol2" action="#{pc_planInfo.selectRow}">
								<h:inputTextarea id="planInfoCol2Text" value="#{currRow.col2}" styleClass="ovMultiLine#{currRow.draftText}" style="width: 200px" />
							</h:commandLink>
						</h:column>					
					</h:dataTable>
				</h:panelGroup>					
			</h:column>
			<h:column>
				<h:panelGrid columns="1">
					<h:commandButton id="pinsPlanInfoAdd" value="#{property.buttonAdd}" styleClass="formButtonInterface"  
										disabled="#{pc_planInfo.addDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.addRidersOrBenefits}" onclick="resetTargetFrame();"/> <!-- NBA211 -->
					<h:commandButton id="pinsPlanInfoUpdate" value="#{property.buttonUpdate}" styleClass="formButtonInterface"
										action="#{pc_planInfo.updateRidersOrBenefits}" onclick="resetTargetFrame()" 
										disabled="#{pc_nbaAppEntryNavigation.updateMode || pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!-- NBA211 -->
					<h:commandButton id="pinsPlanInfoClear" value="#{property.buttonClear}" styleClass="formButtonInterface" 
										disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.resetPlanInfoFields}" onclick="resetTargetFrame()"/> <!-- NBA211 -->
					<h:commandButton id="pinsPlanInfoDelete" value="#{property.buttonDelete}" styleClass="formButtonInterface" 
										disabled="#{pc_planInfo.disableDelete ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.deleteRidersOrBenefits}" onclick="setTargetFrame();"/> <!-- NBA211 -->		
				</h:panelGrid>
			</h:column>
		</h:panelGrid>
</jsp:root>