<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- SPR3545		   8      Delete Owner Relation for One Originally Created by User or by Markup as Same as Primary Insured -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-364		NB-1101	  Standard Phone Number and Social Security Number Format Should be Used and Tab Order Corrected from First to Middle Name -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="payorTitle" value="#{property.appEntPayorTitle}" styleClass="formSectionBar"/>
	<h:panelGrid id="payorPGrid" columns="2" columnClasses="colOwnerInfo" cellpadding="0" cellspacing="0">
		<h:column>
			<h:dataTable id="payor" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_payorInfo.payorList}" 
				var="payor" style="width: 610px;padding-left: 2px;">
				<h:column>
					<h:panelGroup id="sameAsInsPGroup" styleClass="formDataEntryLine">
						<h:selectBooleanCheckbox id="sameAsIns" value="#{payor.sameAsPrimaryInsured}" styleClass="radioMenu" style="width: 20px;" 
								  onclick="resetTargetFrame();handleSubmit();" valueChangeListener="#{pc_payorInfo.sameAsPrimaryInsuredChecked}" immediate="true"
								  binding="#{pc_payorInfo.sameAsPrimaryInsCB}" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_payorInfo.disableSameAsCheckBox}"><!-- NBA211, SPR3545 NBA317-->  
						</h:selectBooleanCheckbox>
						<h:outputText id="sameAsInsured" value="#{property.appEntSameAsPrimaryIns}" styleClass="formLabel" style="width: 275px;text-align: left;"/>
						<h:selectBooleanCheckbox id="sameAsOwner" value="#{payor.sameAsOwner}" styleClass="radioMenu" style="width: 20px;" 
								 onclick="resetTargetFrame();handleSubmit();" valueChangeListener="#{pc_payorInfo.sameAsOwnerChecked}" immediate="true"
								 binding="#{pc_payorInfo.sameAsOwnerCB}" 
								 disabled="#{pc_payorInfo.ownerDisabled || pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_payorInfo.disableSameAsCheckBox}">  <!-- NBA211, SPR3545 NBA317-->
						</h:selectBooleanCheckbox>
						<h:outputText id="sameOwner" value="#{property.appEntSameAsOwner}" styleClass="formLabel" style="width: 115px;text-align: left;"/><!-- NBA175 -->
						<h:selectBooleanCheckbox id="deletePayor" value="#{payor.deleteParty}" styleClass="ovFullCellSelectCheckBox"
								style="position: relative;left: 50px;width: 25px;" disabled="#{payor.disabled}"
								valueChangeListener="#{payor.delPartyAddrDis}" onclick="resetTargetFrame();handleSubmit()" />  <!-- NBA175 NBA317-->
						<h:outputText id="delPayor" value="#{property.deletePayor}"  styleClass="formLabel" style="position: relative;left: 10px;" /><!-- NBA175 -->
					</h:panelGroup>
					<h:panelGroup id="payorNamePGroup" styleClass="formDataEntryLine">
						<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 100px"/>
						<h:inputText id="firstNameEF" value="#{payor.demographics.firstName}" styleClass="formEntryText" style="width: 100px;" 
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
						<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px"/>
						<h:inputText id="middelNameEF" value="#{payor.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
						<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 90px"/>
						<h:inputText id="lastNameEF" value="#{payor.demographics.lastName}" styleClass="formEntryText" style="width: 165px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
					</h:panelGroup>
					<h:panelGroup id="payorLine1PGroup" styleClass="formDataEntryLine">
						<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 100px"/>
						<h:inputText id="addressLine1" value="#{payor.address.addressLine1}" styleClass="formEntryText" style="width: 275px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
						<h:selectBooleanCheckbox id="deleteAddress" value="#{payor.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox"
								style="position: relative;left: 105px;width: 25px;" disabled="#{payor.addressDisabled}"
								valueChangeListener="#{payor.delPartyDis}" onclick="resetTargetFrame();handleSubmit()" />  <!-- NBA175 NBA317-->
						<h:outputText id="delAddress" value="#{property.appEntDelPayorAdd}" styleClass="formLabel" style="position: relative;left: 103px;width: 100px;" /><!-- NBA175 -->
					</h:panelGroup>
					<h:panelGroup id="payorLine2PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine2" value="#{payor.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;" 
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
					</h:panelGroup>
					<h:panelGroup id="payorLine3PGroup" styleClass="formDataEntryLine">		
						<h:inputText id="addressLine3" value="#{payor.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
					</h:panelGroup>
					<!-- begin NBA247 -->
					<h:panelGroup id="payorLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">		
						<h:inputText id="addressLine4" value="#{payor.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
					</h:panelGroup>
					<!-- end NBA247 -->
					<h:panelGroup id="payorCityPGroup" styleClass="formDataEntryLine">
						<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 100px;"/>
						<h:inputText id="city1EF" value="#{payor.address.city}" styleClass="formEntryText" style="width: 145px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
						<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 110px;"/>
						<h:selectOneMenu id="state1DD" value ="#{payor.address.state}" styleClass="formEntryTextFull" style="width: 230px;"
							disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}">  <!-- NBA211 -->
							<f:selectItems id="payorState1List" value="#{payor.address.stateList}"/>
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGrid id="payorCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" style="padding-top: 6px;"><!-- NBA316 -->
						<h:column>
							<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 100px;"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="payorZipRB" value="#{payor.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}">  <!-- NBA211 -->
								<f:selectItems id="payorZipTypes" value="#{payor.address.zipTypeCodes}" />
							</h:selectOneRadio>				
						</h:column>
						<h:column>
							<h:inputText id="zipEF" value="#{payor.address.zip}" styleClass="formEntryText" style="width: 80px;" 
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}" >  <!-- NBA211, SPRNBA-493 -->
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> <!-- SPRNBA-493 -->
							</h:inputText> <!-- SPRNBA-493 -->
							<h:inputText id="postalEF" value="#{payor.address.postal}" styleClass="formEntryText" style="width: 80px;" 
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>  <!-- NBA211 -->
						</h:column>
						<h:column>
							<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 105px;"/>
							<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 105px;margin-top: 10px;" />	<!-- NBA316 -->
						</h:column>
						<h:column>
							<h:selectOneMenu id="countryDD" value ="#{payor.address.country}" styleClass="formEntryTextFull" style="width: 230px;"
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}">  <!-- NBA211 -->
								<f:selectItems id="payorCountryList" value="#{payor.address.countryList}" />
							</h:selectOneMenu>				
							<h:inputText id="countyEF"
								value="#{payor.address.county}" disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 230px;margin-top: 5px;" />	<!-- NBA316 -->
						</h:column>
					</h:panelGrid>
					<h:panelGrid columns="2" styleClass="formDataEntryLineAppEntry" cellpadding="0" cellspacing="0" style="margin-top:-10px;">
						<h:column>
							<h:selectOneRadio id="govtIdType" value="#{payor.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="position:relative;left:70px;width: 180px;"
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner}"> <!-- NBA340 -->
								<f:selectItem id="ssn" itemValue="1" itemLabel="Social Security Number"/>
								<f:selectItem id="sin" itemValue="3" itemLabel="Social Insurance Number"/><!-- NBA340 -->
								<f:selectItem id="ti" itemValue="2" itemLabel="Tax Identification"/><!--NBA340 -->
							</h:selectOneRadio>			
						</h:column>
						<h:column>
						<!-- begin NBA340 -->
							<h:inputText id="govtIdInput" value="#{payor.demographics.ssn}" styleClass="formEntryText" style="position:relative;left:70px;width: 135px;margin-top: 45px;"
								disabled="#{payor.sameAsPrimaryInsured || payor.sameAsOwner || pc_nbaAppEntryNavigation.partialUpdateDisabled}" >  <!-- NBA211, SPRNBA-364 -->								
		                    </h:inputText> <!-- SPRNBA-364 -->
		                    <h:inputHidden id="govtIdKey" value="#{payor.demographics.govtIdKey}"></h:inputHidden>
						<!-- end NBA340  -->
						</h:column>
					</h:panelGrid>
				</h:column>
			</h:dataTable>
		</h:column>	
	</h:panelGrid>
</jsp:root>