<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="genInfo" value="#{property.appEntGeneralInfo}"
		styleClass="formSectionBar" />
	<h:panelGrid id="GeneralInfoQuestions" columns="#{pc_generalInfo.noOfColumns}" cellpadding="1" border="1" cellspacing="0" styleClass="formDataEntryLine"
			style="margin-top: 10px;margin-bottom: 10px;">
		<h:outputText id="questionCol" value="#{property.appEntQuestion}" styleClass="formLabel" style="width: 320px;font-weight: bold;text-align: center;" />
		<h:outputText id="insCol" value="#{property.appEntPrimaryIns}" 	styleClass="formLabel" 	style="width: 125px;font-weight: bold;text-align: center;" />
		<h:outputText id="otherInsCol" value="#{property.appEntOthIns}" rendered="#{pc_generalInfo.othInsRenderer}"
					styleClass="formLabel" style="width: 125px;font-weight: bold;text-align: center;" />
		<h:outputText id="jointInsCol" value="#{property.appEntJntIns}" rendered="#{pc_generalInfo.jntInsRenderer}"
					styleClass="formLabel" style="width: 125px;font-weight: bold;text-align: center;" />		
					
			
		<h:panelGroup id="q1PG" styleClass="formDataEntryLine" style="padding-left: 2px;"> 
			<h:outputText id="q1" value="#{property.appEntPage5Q1}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ1YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q1CB1Yes" 	value="#{pc_generalInfo.q1PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q1CB1Yes');" />
			<h:outputText id="pinsQ1Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q1CB1No" value="#{pc_generalInfo.q1PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q1CB1No');" />
			<h:outputText id="pinsQ1No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ1YesNoPG" rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q1CB2Yes" value="#{pc_generalInfo.q1OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q1CB2Yes');" />
			<h:outputText id="otherInsQ1Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q1CB2No" value="#{pc_generalInfo.q1OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q1CB2No');" />
			<h:outputText id="otherInsQ1No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q2PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText value="#{property.appEntPage5Q2}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ2YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q2CB1Yes" value="#{pc_generalInfo.appPendIndPriYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q2CB1Yes');" />
			<h:outputText id="pinsQ2Yes"  value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q2CB1No" value="#{pc_generalInfo.appPendIndPriNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q2CB1No');" />
			<h:outputText id="pinsQ2No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ2YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q2CB2Yes" value="#{pc_generalInfo.appPendIndOthYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q2CB2Yes');" />
			<h:outputText id="otherInsQ2Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q2CB2No" value="#{pc_generalInfo.appPendIndOthNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q2CB2No');" />
			<h:outputText id="otherInsQ2No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q3PG">
			<h:outputText id="q3" value="#{property.appEntPage5Q3}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ3YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q3CB1Yes" value="#{pc_generalInfo.q2PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q3CB1Yes');" />
			<h:outputText id="pinsQ3Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q3CB1No" value="#{pc_generalInfo.q2PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q3CB1No');" />
			<h:outputText id="pinsQ3No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ3YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q3CB2Yes" value="#{pc_generalInfo.q2OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q3CB2Yes');" />
			<h:outputText id="otherInsQ3Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q3CB2No" value="#{pc_generalInfo.q2OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q3CB2No');" />
			<h:outputText id="otherInsQ3No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q4PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q4" value="#{property.appEntPage5Q4}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ4YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q4CB1Yes" value="#{pc_generalInfo.q3PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q4CB1Yes');" />
			<h:outputText id="pinsQ4Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q4CB1No" value="#{pc_generalInfo.q3PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q4CB1No');" />
			<h:outputText id="pinsQ4No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ4YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q4CB2Yes" value="#{pc_generalInfo.q3OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q4CB2Yes');" />
			<h:outputText id="otherInsQ4Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q4CB2No" value="#{pc_generalInfo.q3OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q4CB2No');" />
			<h:outputText id="otherInsQ4No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q5PG" style="padding-left: 2px;">
			<h:outputText id="q5" value="#{property.appEntPage5Q5}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ5YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q5CB1Yes" value="#{pc_generalInfo.q4PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q5CB1Yes');" />
			<h:outputText id="pinsQ5Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q5CB1No" value="#{pc_generalInfo.q4PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q5CB1No');" />
			<h:outputText id="pinsQ5No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ5YesNopG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q5CB2Yes" value="#{pc_generalInfo.q4OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q5CB2Yes');" />
			<h:outputText id="otherInsQ5Yes" value="#{property.appEntYes}" styleClass="formLabel" 	style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q5CB2No" value="#{pc_generalInfo.q4OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q5CB2No');" />
			<h:outputText id="otherInsQ5No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q6PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q6" value="#{property.appEntPage5Q6}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ6YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q6CB1Yes" value="#{pc_generalInfo.tobaccoIndPriYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;"
				onclick="toggleCBGroup('q6CB1Yes');" />
			<h:outputText id="pinsQ6Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q6CB1No" value="#{pc_generalInfo.tobaccoIndPriNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;"
				onclick="toggleCBGroup('q6CB1No');" />
			<h:outputText id="pinsQ6No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:commandButton id="tobaccoInfo" image="images/link_icons/circle_i.gif" action="#{pc_generalInfo.tobaccoInfoPriIns}" onclick="setTargetFrame();" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ6YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" 	style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q6CB2Yes" value="#{pc_generalInfo.tobaccoIndOthYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q6CB2Yes');" />
			<h:outputText id="otherInsQ6Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q6CB2No" value="#{pc_generalInfo.tobaccoIndOthNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q6CB2No');" />
			<h:outputText id="otherInsQ6No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:commandButton image="images/link_icons/circle_i.gif" action="#{pc_generalInfo.tobaccoInfoOthIns}" onclick="setTargetFrame();" />
		</h:panelGroup>
		<h:panelGroup id="q7PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q7" value="#{property.appEntPage5Q7}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ7YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q7CB1Yes" value="#{pc_generalInfo.q5PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q7CB1Yes');" />
			<h:outputText id="pinsQ7Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q7CB1No" value="#{pc_generalInfo.q5PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q7CB1No');" />
			<h:outputText id="pinsQ7No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup> 
		<h:panelGroup id="otherInsQ7YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q7CB2Yes" 	value="#{pc_generalInfo.q5OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q7CB2Yes');" />
			<h:outputText id="otherInsQ7Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q7CB2No" value="#{pc_generalInfo.q5OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q7CB2No');" />
			<h:outputText id="otherInsQ7No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="q8PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q8" value="#{property.appEntPage5Q8}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ8YesNoPG" style="padding-left: 2px;"> 
			<h:selectBooleanCheckbox id="q8CB1Yes" value="#{pc_generalInfo.criminalIndPriYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q8CB1Yes');" />
			<h:outputText id="pinsQ8Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q8CB1No" value="#{pc_generalInfo.criminalIndPriNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q8CB1No');" />
			<h:outputText id="pinsQ8No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ8YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q8CB2Yes" value="#{pc_generalInfo.criminalIndOthYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q8CB2Yes');" />
			<h:outputText id="otherInsQ8Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q8CB2No" value="#{pc_generalInfo.criminalIndOthNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q8CB2No');" />
			<h:outputText id="otherInsQ8No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>

		<h:panelGroup id="q9PG" styleClass="formDataEntryLine" style="padding-left: 2px;"> 
			<h:outputText id="q9" value="#{property.appEntPage5Q9}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ9YesNoPG" style="padding-left: 2px;"> 
			<h:selectBooleanCheckbox id="q9CB1Yes" value="#{pc_generalInfo.q6PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q9CB1Yes');" />
			<h:outputText id="pinsQ9Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q9CB1No" value="#{pc_generalInfo.q6PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q9CB1No');" />
			<h:outputText id="pinsQ9No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ9YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" 	style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q9CB2Yes" value="#{pc_generalInfo.q6OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q9CB2Yes');" />
			<h:outputText id="otherInsQ9Yes" value="#{property.appEntYes}" styleClass="formLabel" 	style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q9CB2No" value="#{pc_generalInfo.q6OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q9CB2No');" />
			<h:outputText id="otherInsQ9No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>

		<h:panelGroup id="q10PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q10" value="#{property.appEntPage5Q10}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup> 
		<h:panelGroup id="pinsQ10YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q10CB1Yes" value="#{pc_generalInfo.q7PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q10CB1Yes');" />
			<h:outputText id="pinsQ10Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q10CB1No" value="#{pc_generalInfo.q7PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q10CB1No');" />
			<h:outputText id="pinsQ10No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ10YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}"	style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q10CB2Yes" value="#{pc_generalInfo.q7OthInsYes}" 	styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q10CB2Yes');" />
			<h:outputText id="otherInsQ10Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q10CB2No" 	value="#{pc_generalInfo.q7OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q10CB2No');" />
			<h:outputText id="otherInsQ10No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>

		<h:panelGroup id="q11PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q11" value="#{property.appEntPage5Q11}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup>
		<h:panelGroup id="pinsQ11YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q11CB1Yes" value="#{pc_generalInfo.q8PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q11CB1Yes');" />
			<h:outputText id="pinsQ11Yes"  value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q11CB1No" value="#{pc_generalInfo.q8PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q11CB1No');" />
			<h:outputText id="pinsQ11No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ11YesNoPG"  rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q11CB2Yes" value="#{pc_generalInfo.q8OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q11CB2Yes');" />
			<h:outputText id="otherInsQ11Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q11CB2No" value="#{pc_generalInfo.q8OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q11CB2No');" />
			<h:outputText id="otherInsQ11No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>

		<h:panelGroup id="q12PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
			<h:outputText id="q12" value="#{property.appEntPage5Q12}" styleClass="formLabel" style="width: 320px;text-align: left;" />
		</h:panelGroup> 
		<h:panelGroup id="pinsQ12YesNoPG" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q12CB1Yes" value="#{pc_generalInfo.q9PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q12CB1Yes');" />
			<h:outputText id="pinsQ12Yes"  value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q12CB1No" value="#{pc_generalInfo.q9PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q12CB1No');" />
			<h:outputText id="pinsQ12No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
		<h:panelGroup id="otherInsQ12YesNoPG" rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" style="padding-left: 2px;">
			<h:selectBooleanCheckbox id="q12CB2Yes" value="#{pc_generalInfo.q9OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q12CB2Yes');" />
			<h:outputText id="otherInsQ12Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
			<h:selectBooleanCheckbox id="q12CB2No" value="#{pc_generalInfo.q9OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
				onclick="toggleCBGroup('q12CB2No');" /> 
			<h:outputText id="otherInsQ12No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 40px;text-align: center;" />
		</h:panelGroup>
	</h:panelGrid>
</jsp:root>
