<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
    <script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
	<script language="JavaScript" type="text/javascript">
		//NBA213 code deleted
		var contextpath = '<%=path%>'; //NBA340		
		function setTargetFrame() {
			getScrollXY('form_appEntryULPage3')
			document.forms['form_appEntryULPage3'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryULPage3')
			document.forms['form_appEntryULPage3'].target='';
			return false;
		}
		function submitContents()
		{
			setTargetFrame();
			document.forms['form_appEntryULPage3'].submit();
			return true;
		}		
		function saveForm(action) {
			if($(this).checkErrorMessage()){ //NBA340
				setTargetFrame();
				document.forms['form_appEntryULPage3']['form_appEntryULPage3:action'].value= action;
				document.forms['form_appEntryULPage3']['form_appEntryULPage3:hiddenSaveButton'].click();
				return true;
			}
			return false; //NBA340
		}
		
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();
			$("input[name$='govtIdCorpType']").govtidType();
	        $("input[name$='govtIdInput']").govtidValue();
		})	
	</script>
	
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryULPage3')" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE3" value="#{pc_nbaUlPage3}" />
		<h:form id="form_appEntryULPage3" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryULPage3')">
			<h:inputHidden id="scrollx" value="#{pc_nbaUlPage3.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaUlPage3.scrollYC}"></h:inputHidden>	
			<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left:10px;">
				<f:subview id="beneficiaryInformation">
					<c:import url="/nbaAppEntry/life/subviews/nbaBeneficiaryInfo.jsp" /><!-- NBA176  -->
				</f:subview>
				<f:subview id="otherInsuranceInformation">
					<c:import url="/nbaAppEntry/life/subviews/nbaOtherInsuranceInfo.jsp" /><!-- NBA176  -->
				</f:subview>		
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" /><!-- NBA175  -->
					<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>
		</h:form>
		<div id="Messages" style="display: none">
			<h:messages />
		</div>
	</f:view>
		
</body>
</html>
