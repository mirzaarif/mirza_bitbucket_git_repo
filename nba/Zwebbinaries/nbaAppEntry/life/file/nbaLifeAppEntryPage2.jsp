<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPR3265           8      Standard Phone Number and Social  Security Number Format Should be Used and Tab Order Corrected from First to Middle Name -->
<!-- FNB002          NB-1101  App Entry -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
<script language="JavaScript" type="text/javascript">
        var contextpath = '<%=path%>'; //NBA340
		//NBA213 code deleted
		function setTargetFrame() {
			getScrollXY('form_appEntryULPage2');
			document.forms['form_appEntryULPage2'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryULPage2');
			document.forms['form_appEntryULPage2'].target='';
			return false;
		}		
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryULPage2'].submit();
			return true;
		}
		
		function saveForm(action) {
			if($(this).checkErrorMessage()){ //NBA340
				setTargetFrame();
				document.forms['form_appEntryULPage2']['form_appEntryULPage2:action'].value= action;
				document.forms['form_appEntryULPage2']['form_appEntryULPage2:hiddenSaveButton'].click();
				return true;
			}
			return false; //NBA340
		}
		//Begin SPR3265
		function showfocus() {
			if (document.getElementById('form_appEntryULPage2:otherInsuredInfo:firstNameHD').value=='true') {
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:middelNameEF').focus();
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:firstNameHD').value = 'false';
			} else if (document.getElementById('form_appEntryULPage2:otherInsuredInfo:middleNameHD').value=='true') {
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:lastNameEF').focus();
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:middleNameHD').value = 'false';
			} else if (document.getElementById('form_appEntryULPage2:otherInsuredInfo:lastNameHD').value=='true') {
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:addressLine1').focus();
				document.getElementById('form_appEntryULPage2:otherInsuredInfo:lastNameHD').value = 'false';
			}			
		}
		//End SPR3265		
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();
			$("input[name$='govtIdCorpType']").govtidType();
	        $("input[name$='govtIdInput']").govtidValue();
		})	
	
	</script>	
</head>
<body onload="showfocus();filePageInit();scrollToCoordinates('form_appEntryULPage2')" style="overflow-x: hidden; overflow-y: scroll"><!-- SPR3265 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE2" value="#{pc_nbaUlPage2}" />
	<PopulateBean:Load serviceName="RETRIEVE_JOINT_OR_OTHER_INSURED_BEAN" value="#{pc_planInfo}" />  <!-- FNB002  -->
	<h:form id="form_appEntryULPage2" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryULPage2')">
		<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left: 10px;">
			<h:inputHidden id="scrollx" value="#{pc_nbaUlPage2.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaUlPage2.scrollYC}"></h:inputHidden>	
			<h:panelGroup>
				<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" /><!-- NBA175  -->
				<h:inputHidden id="partyRoleCodeForCurrentTab" value="#{pc_nbaUlPage2.partyRoleForCurrentTab}"></h:inputHidden>
				<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
			</h:panelGroup>

			<f:subview id="otherInsuredInfo">
				<c:import url="/nbaAppEntry/life/subviews/nbaJointOrOtherInsuredInfo.jsp" /><!-- NBA176  -->
			</f:subview>
			<f:subview id="otherInsuredEmployer">
				<c:import url="/nbaAppEntry/life/subviews/nbaJointOrOtherInsEmployer.jsp" /><!-- NBA176  -->
			</f:subview>
			
			<f:subview id="planInformation">
				<c:import url="/nbaAppEntry/life/subviews/nbaPlanInfo.jsp" /><!-- NBA176  -->
			</f:subview>
			<f:subview id="ownerInfo">
				<c:import url="/nbaAppEntry/life/subviews/nbaOwnerInfo.jsp" /><!-- NBA176  -->
			</f:subview>
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
