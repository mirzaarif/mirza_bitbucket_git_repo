<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- FNB002         NB-1101   App Entry -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
	<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
	<script language="JavaScript" type="text/javascript">
		//NBA213 code deleted
		var contextpath = '<%=path%>'; //NBA340		
		function setTargetFrame() {
			getScrollXY('form_appEntryULPage1');
			document.forms['form_appEntryULPage1'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryULPage1');
			document.forms['form_appEntryULPage1'].target='';
			return false;
		}
		function submitContents(){
			setTargetFrame();
			document.forms['form_appEntryULPage1'].submit();
			return true;
		}		
		function saveForm(action) {
			if($(this).checkErrorMessage()){ //NBA340
			    setTargetFrame();
				document.forms['form_appEntryULPage1']['form_appEntryULPage1:action'].value= action;
				document.forms['form_appEntryULPage1']['form_appEntryULPage1:hiddenSaveButton'].click();
				return true;
				}
			return false; //NBA340
		}

		function toggleCBGroup(selectedCB) {
			if (document.forms['form_appEntryULPage1']['form_appEntryULPage1:formalPurpose:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('AppTypeChk') != -1) {
					temp = selectedCB.substring(selectedCB.length - 1,selectedCB.length);
					for(i=1;i<5;i++) {
						if(i != temp) {
							document.forms['form_appEntryULPage1']['form_appEntryULPage1:formalPurpose:AppTypeChk' + i].value=false;
							document.forms['form_appEntryULPage1']['form_appEntryULPage1:formalPurpose:AppTypeChk' + i].checked=false;
						}
					}
				} 
			}
		}
		
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();			
	        $("input[name$='govtIdInput']").govtidValue();
		})
	</script>	
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryULPage1')" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>		
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE1" value="#{pc_nbaUlPage1}" />
		<PopulateBean:Load serviceName="RETRIEVE_PRIMARYINSURED_BEAN" value="#{pc_planInfo}" />  <!-- FNB002  -->
		<h:form id="form_appEntryULPage1" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryULPage1')">
			<h:inputHidden id="scrollx" value="#{pc_nbaUlPage1.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaUlPage1.scrollYC}"></h:inputHidden>	
			<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left: 10px;">
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" /><!-- NBA175  -->
					<h:inputHidden id="hiddenPartyRole" value="#{pc_nbaUlPage1.partyRoleForCurrentTab}"></h:inputHidden>
					<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
				<f:subview id="formalPurpose">
					<c:import url="/nbaAppEntry/life/subviews/nbaAppEntryHeader.jsp" /><!-- NBA176  -->
				</f:subview>				
				<f:subview id="producerInformation">
					<c:import url="/nbaAppEntry/subviews/nbaProducerInfo.jsp" />
				</f:subview>
				<f:subview id="planInformation">
					<c:import url="/nbaAppEntry/life/subviews/nbaPlanInfo.jsp" /><!-- NBA176  -->
				</f:subview>
				<f:subview id="primaryInsuredInfo">
					<c:import url="/nbaAppEntry/life/subviews/nbaPrimaryInsuredInfo.jsp" /><!-- NBA176  -->
				</f:subview>
				<f:subview id="employerInfo">
					<c:import url="/nbaAppEntry/life/subviews/nbaEmployer.jsp" /><!-- NBA176  -->
				</f:subview>
			</h:panelGroup>
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
