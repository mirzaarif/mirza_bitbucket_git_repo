<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- SPR3295           7      Application Entry Reusability -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Application Entry Page 4</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<!-- NBA213 code deleted -->
	<script language="JavaScript" type="text/javascript">
		//NBA213 code deleted
		var contextpath = '<%=path%>'; //NBA340
		function setTargetFrame() {
			document.forms['form_appEntryULPage4'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_appEntryULPage4'].target='';
			return false;
		}
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryULPage4'].submit();
			return true;
		}
		
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryULPage4']['form_appEntryULPage4:action'].value= action;
			document.forms['form_appEntryULPage4']['form_appEntryULPage4:hiddenSaveButton'].click();
			return true;
		}		
		
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE4" value="#{pc_nbaUlPage4}" />  <!-- SPR3295 -->
		<h:form id="form_appEntryULPage4" styleClass="inputFormMat" >
			<h:panelGroup id ="PGroup1" styleClass="inputForm" >
				<!-- begin SPR3295 -->
				<f:subview id="formalPurpose">
					<c:import url="/nbaAppEntry/subviews/nbaFunds.jsp" />
				</f:subview>				
				<!-- end SPR3295 -->
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" /><!-- NBA175  -->
					<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>					
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>