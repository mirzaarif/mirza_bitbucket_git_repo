<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA211            7      Partial Application -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- FNB002         NB-1101   Additional Fields on Application Entry -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Application Entry Page 7</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<!-- NBA213 code deleted -->
	<script language="JavaScript" type="text/javascript">
		//NBA213 code deleted
		function setTargetFrame() {
			document.forms['form_appEntryULPage7'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_appEntryULPage7'].target='';
			return false;
		}
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryULPage7'].submit();
			return true;
		}
		
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryULPage7']['form_appEntryULPage7:action'].value= action;
			document.forms['form_appEntryULPage7']['form_appEntryULPage7:hiddenSaveButton'].click();
			return true;
		}		
		function toggleCBGroup(selectedCB) {
			if (document.forms['form_appEntryULPage7']['form_appEntryULPage7:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('Yes') != -1) {
					temp = selectedCB.substring(0, selectedCB.indexOf('Yes'));
					document.forms['form_appEntryULPage7']['form_appEntryULPage7:' + temp + 'No'].value=false;
					document.forms['form_appEntryULPage7']['form_appEntryULPage7:' + temp + 'No'].checked=false;
				} else {
					temp = selectedCB.substring(0, selectedCB.indexOf('No'));
					document.forms['form_appEntryULPage7']['form_appEntryULPage7:' + temp + 'Yes'].value=false;
					document.forms['form_appEntryULPage7']['form_appEntryULPage7:' + temp + 'Yes'].checked=false;
				}
			}
		}
		
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:form id="form_appEntryULPage7" styleClass="inputFormMat">
			<h:panelGroup id ="PGroup1" styleClass="inputForm" style="padding-left:10px;">
				<!-- begin NBA176  -->
				<f:subview id="signatureInformation">
					<c:import url="/nbaAppEntry/subviews/nbaSignatureInfo.jsp" />
				</f:subview>				
				<f:subview id="producerStmtInformation">
					<c:import url="/nbaAppEntry/subviews/nbaProducerStatement.jsp" />
				</f:subview>
				<!-- end NBA176  -->
				<h:outputText id="tranlatorInfo" value="#{property.appEntTranslatorInfo}" styleClass="formSectionBar"/>
				<h:panelGroup id="transAppCBPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
					<h:selectBooleanCheckbox id="transAppCB" value="#{pc_applicationInfo.appTranslated}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;"/>
					<h:outputText id="transApp" value="#{property.appEntAppTranslated}" styleClass="formLabel" style="width: 145px;" />
				</h:panelGroup>
				<h:panelGroup id="transNamePGroup" styleClass="formDataEntryLine">
					<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 85px;"/>
					<h:inputText id="firstNameEF" value="#{pc_applicationInfo.transFirstName}" styleClass="formEntryText" style="width: 110px;"/>
					<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px"/>
					<h:inputText id="middelNameEF" value="#{pc_applicationInfo.transMidName}" styleClass="formEntryText" style="width: 30px;"/>
					<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 90px"/>
					<h:inputText id="lastNameEF" value="#{pc_applicationInfo.transLastName}" styleClass="formEntryText" style="width: 185px;"/>
				</h:panelGroup>
				<!-- begin FNB002  -->
				<f:subview id="additionalInformation">
					<c:import url="/nbaAppEntry/subviews/nbaAdditionalInfo.jsp" />
				</f:subview>
				<!-- end FNB002  -->
				<h:panelGroup >
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" /><!-- NBA175  -->
					<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>					
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>