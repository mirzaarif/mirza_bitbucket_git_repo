<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA212            7      Content Services -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPR1613		   8	  Some Business Functions should be disabled on an Issued Contract  --> 
<!-- SPR3576		   8	  Unable to Delete Owner in Application Update -->
<!-- SPR3542		   8	  Commit Should Be Disabled on Application Entry Views for Reinstatement and Increase Contract Changes -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA330			NB-1401   Product Versioning -->
<!-- SPRNBA-947 	NB-1601   Null Pointer Exception May Occur in Inbox if User Selects Different Work -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>  <!-- NBA212 -->

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Application Entry</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="include/rqaJSF.js"></script> 
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = -22;
			//NBA213 code deleted
			var defaultindex = 1;
			var draftChanges = false;
			var commentBarRefresh = false;
			function mainTabSize() {
				winHeight = window.screen.availHeight;
				// 175 = 37(browser title) + 33(title) + 23(menu) + 50(status) + 32(tabs)
				tabHeight = winHeight - 335; //NBA213
				document.getElementById('appEntry').height = tabHeight;
			}			
			
			function resetTargetFrame() {
				document.forms['form_appEntryOverview'].target='';
				return false;
			}
	
			function setTargetFrame() {
				document.forms['form_appEntryOverview'].target='controlFrame';
				return false;
			}

			//SPR3576 code deleted		
			function refreshCommentBar() {
				commentBarRefresh = false;
				if (top.mainContentFrame.contentRightFrame.file.commentBar != null) {  //NBA213
					top.mainContentFrame.contentRightFrame.file.commentBar.location.href = top.mainContentFrame.contentRightFrame.file.commentBar.location.href;  //NBA213
				}
			}
			
			//begin NBA317
			// enables/disables the state of the save/commit/submit buttons based on the tokenization status of the credit card
			function authorizeSaveSubmit(enable){
				btnSave = document.getElementById('form_appEntryOverview:btnSave');
				if(btnSave){
					btnSave.disabled = !enable;
				}
				
				btnSubmit = document.getElementById('form_appEntryOverview:btnSubmit');
				if(btnSubmit){
					btnSubmit.disabled = !enable;
				}
				
				btnValidate	= document.getElementById('form_appEntryOverview:btnValidate');
				if(btnValidate){
					btnValidate.disabled = !enable;
				}
				
				updateBtnCommit = document.getElementById('form_appEntryOverview:updateBtnCommit');
				if(updateBtnCommit){
					updateBtnCommit.disabled = !enable;
				}
			}
			//end NBA317
	
			// this method calls the saveForm method for the current open tab which will submit the current tab
			function submitForm(action) {
				if (top.mainContentFrame.contentRightFrame.file.file != null &&top.mainContentFrame.contentRightFrame.file.file.saveForm != null) {  //NBA213
					//if submitting check to see if quality check needs to be done first
					if( action == 'Submit'&& qualCheckComplete() == false) {
						performSubmit = doQualityAssurance();
						if( performSubmit == false) {
							return false;
						}
					} else if( action == 'doSubmit') {
						action = 'Submit';
					} else if( action == 'noSubmit') {
						top.hideWait();
						return;
					}
					top.mainContentFrame.contentRightFrame.file.file.saveForm(action);  //NBA213
					return false;
				} 
			}

			function viewImages() {
				//begin SPR3576 
				var hasImageViewerLaunched = document.forms['form_appEntryOverview']['form_appEntryOverview:hasImageViewerLaunched'].value;
				if (hasImageViewerLaunched == 'false') {
					top.controlFrame.location.href = '../../launchImage.faces';
				}
				//end SPR3576
			}
		//-->
	</script>
</head>
<body class="desktopBody" style="overflow-x: hidden; overflow-y: hidden" >  <!-- SPRNBA-947 remove top.hideWait -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="DETERMINE_APPENT_NAVIGATION" value="#{pc_nbaAppEntryNavigation}" />
		<!-- NBA330 code deleted -->
		<PopulateBean:Load serviceName="RETRIEVE_ALL_IMAGES" value="#{pc_imageViewerMgmt}" /> <!-- SPR3576 -->
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:31px">
					<td><FileLoader:Files location="nbaAppEntry/life/file/" numTabsPerRow="7" defaultIndex="#{pc_nbaAppEntryNavigation.defaultIndex}"/></td> <!-- NBA213 --><!-- NBA176  -->
				</tr>
				<tr style="height:*; vertical-align: top;">
					<td><iframe id="appEntry" name="file" src="" width="100%" frameborder="0" onload="mainTabSize();"></iframe>
					</td>
				</tr>
				<tr style="height:60px; vertical-align: bottom; padding-top: 2px;">
					<td>
						<h:form id="form_appEntryOverview">		
							<iframe id="commentFrame" name="commentBar" style="width: 100%; height: 21px;" src="./nbaAppEntry/nbaAECommentBar.faces" scrolling="no" frameborder="0"></iframe><!-- NBA176  -->
							<h:panelGroup styleClass="tabButtonBar" rendered="#{!pc_nbaAppEntryNavigation.updateMode}">
								<h:commandButton id="btnSave" styleClass="tabButtonLeft" value="#{property.buttonSave}" 
									disabled="#{pc_nbaAppEntryNavigation.auth.enablement['Commit'] || 
												pc_nbaAppEntryNavigation.notLocked || 
												pc_nbaAppEntryNavigation.issued}" 
									onclick="resetTargetFrame();submitForm('Save');return false;"/> <!-- NBA213 SPR1613 NBA317-->
								<h:commandButton  id="btnSubmit" styleClass="tabButtonRight" value="#{property.buttonSubmit}" 
									disabled="#{pc_nbaAppEntryNavigation.auth.enablement['Commit'] || 
												pc_nbaAppEntryNavigation.notLocked || 
												pc_nbaAppEntryNavigation.issued}"  
									onclick="resetTargetFrame();submitForm('Submit');return false;" /> <!-- NBA213 SPR1613 NBA317-->
								<h:commandButton  id="btnValidate" styleClass="tabButtonRight-1" value="#{property.buttonValidate}" onclick="resetTargetFrame();submitForm('Validate');return false;"/><!-- NBA317 -->
								<h:commandButton  styleClass="tabButtonRight-2" value="#{property.buttonQualityReview}" onclick="resetTargetFrame();qualityCheckReview();top.hideWait();return false;"
									style="left: 334px; width: 97px;" disabled="#{pc_nbaAppEntryNavigation.qualityReviewDisabled}"/>
							</h:panelGroup>
							<h:panelGroup styleClass="tabButtonBar" rendered="#{pc_nbaAppEntryNavigation.updateMode}">
								<h:commandButton id="updateBtnSave" styleClass="tabButtonLeft" value="#{property.buttonSave}" onclick="resetTargetFrame();submitForm('Save');return false;" 
									disabled="#{pc_nbaAppEntryNavigation.auth.enablement['Commit'] ||
												pc_nbaAppEntryNavigation.notLocked ||
												pc_nbaAppEntryNavigation.updateMode || 
												pc_nbaAppEntryNavigation.issued}"/>	<!-- NBA213 SPR1613 NBA317-->
								<h:commandButton id="updateBtnCommit" styleClass="tabButtonRight" value="#{property.buttonCommit}" 
									disabled="#{pc_nbaAppEntryNavigation.auth.enablement['Commit'] ||
												pc_nbaAppEntryNavigation.notLocked || 
												pc_nbaAppEntryNavigation.issued ||
												pc_nbaAppEntryNavigation.reinstOrInc}" 
									onclick="resetTargetFrame();submitForm('Save');return false;"/> <!-- NBA213 SPR1613 SPR3542 NBA317-->									
								<h:commandButton id="updateBtnSubmit" styleClass="tabButtonRight-1" value="#{property.buttonSubmit}" onclick="resetTargetFrame();submitForm('Submit');return false;" 
									disabled="#{pc_nbaAppEntryNavigation.auth.enablement['Commit'] ||
												pc_nbaAppEntryNavigation.notLocked ||
												pc_nbaAppEntryNavigation.updateMode || 
												pc_nbaAppEntryNavigation.issued}"/>	<!-- NBA213 SPR1613 NBA317-->
								<h:commandButton id="updateBtnValidate" styleClass="tabButtonRight-2" value="#{property.buttonValidate}" onclick="resetTargetFrame();submitForm('Validate');return false;" 
									disabled="#{pc_nbaAppEntryNavigation.updateMode}"/><!-- NBA317 -->
								<h:commandButton  styleClass="tabButtonRight" value="#{property.buttonQualityReview}" onclick="resetTargetFrame();qualityCheckReview();top.hideWait();return false;"
									style="left: 240px; width: 97px;" disabled="#{pc_nbaAppEntryNavigation.qualityReviewDisabled}"/>
							</h:panelGroup>	
							<!-- NBA212 -->						
							<h:inputHidden id="QualityCheckModelResults" value="#{pc_nbaAppEntryNavigation.qualityModelResults}" />
							<h:inputHidden id="hasImageViewerLaunched" value="#{pc_imageViewerMgmt.imageViewerLaunched}" /> <!-- SPR3576 -->
						</h:form>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- SPR3576 code deleted -->
	</f:view>
	
</body>
</html>