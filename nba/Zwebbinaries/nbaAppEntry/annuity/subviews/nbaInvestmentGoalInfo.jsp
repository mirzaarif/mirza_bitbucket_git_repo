<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPRNBA-515		 NB-1301  Dollar ($) Denomination Missing from Investment Goals Amount -->
<!-- NBA297          NB-1401  Suitability Enhancement -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="investmentGoalInfo" value="#{property.annuityAppInvGoalInfo}" styleClass="formSectionBar"/>
		<h:panelGroup id="investmentObjective" styleClass="formDataEntryLine">
			<h:outputText id="investmentObj" value="#{property.annuityAppInvObj}" styleClass="formLabel" style="width: 200px;"/>
			<h:selectOneMenu id="investmentObjDD" value ="#{pc_investmentGoalInfo.investmentObj}" styleClass="formEntryText" style="width: 200px;">						
				<f:selectItems id="investmentObjList" value="#{pc_investmentGoalInfo.investmentObjList}" />
			</h:selectOneMenu>			
		</h:panelGroup>
		<h:panelGroup id="riskTolerancePGroup" styleClass="formDataEntryLine">
			<h:outputText id="riskTolerance" value="#{property.annuityAppRiskTol}" styleClass="formLabel" style="width: 200px;"/>
			<h:selectOneMenu id="riskToleranceDD" value ="#{pc_investmentGoalInfo.riskTolerance}" styleClass="formEntryText" style="width: 200px;">						
				<f:selectItems id="riskToleranceList" value="#{pc_investmentGoalInfo.riskToleranceList}" />
			</h:selectOneMenu>		
		</h:panelGroup>
		<h:panelGroup id="investmentExperience" styleClass="formDataEntryLine">
			<h:outputText id="investmentExp" value="#{property.annuityAppInvYearsExp}" styleClass="formLabel" style="width: 200px;"/>
			<h:inputText id="investmentExperienceEF" value="#{pc_investmentGoalInfo.investmentExperience}" styleClass="formEntryText" style="width: 100px;" >
			</h:inputText>
			<h:outputText id="estimatedAnnualIncome" value="#{property.annuityAppEstimatedAnnualCost}" styleClass="formLabel" style="width: 185px;"/>
			<h:inputText id="estimatedAnnualIncomeEF" value="#{pc_investmentGoalInfo.estimatedAnnualIncome}" styleClass="formEntryText" style="width: 100px;" >
				<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/> <!-- SPRNBA-515 -->
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="federalTax" styleClass="formDataEntryLine">
			<h:outputText id="federalTaxBracket" value="#{property.annuityAppFederalTaxBracket}" styleClass="formLabel" style="width: 200px;"/>
			<h:selectOneMenu id="federalTaxBracketDD" value ="#{pc_investmentGoalInfo.federalTaxBracket}" styleClass="formEntryText" style="width: 100px;">						
				<f:selectItems id="federalTaxBracketList" value="#{pc_investmentGoalInfo.federalTaxBracketList}" />
			</h:selectOneMenu>
			<h:outputText id="netWorth" value="#{property.appEntNetWorth}" styleClass="formLabel" style="width: 185px;"/>
			<h:inputText id="netWorthEF" value="#{pc_investmentGoalInfo.netWorth}" styleClass="formEntryText" style="width: 100px;" >
				<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/> <!-- SPRNBA-515 -->
			</h:inputText>
		</h:panelGroup>
		<!-- begin NBA297 -->
		<h:panelGroup id="investmentTimeGroup" styleClass="formDataEntryLine">
			<h:outputText id="invTimeFreq" value="#{property.appEntInvTimeFrquency}" styleClass="formLabel" style="width: 240px;"/>
			<h:selectOneMenu id="invTimeFreqDD" value ="#{pc_investmentGoalInfo.demographics.invHorizonPeriod}" styleClass="formEntryText" style="width: 250px;">
				<f:selectItems id="invTimeFreqList" value="#{pc_investmentGoalInfo.demographics.invHorizonPeriodList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="investmentTimeGroup2" styleClass="formDataEntryLine">
			<h:outputText id="invFrom" value="#{property.appEntFrom}" styleClass="formLabel" style="width: 285px;"/>
			<h:inputText id="invFromEF" value="#{pc_investmentGoalInfo.demographics.invHorizonFrom}" styleClass="formEntryText" style="width: 60px;"/>			
			<h:outputText id="invTo" value="#{property.appEntTo}" styleClass="formLabel" style="width: 85px;"/>
			<h:inputText id="invToEF" value="#{pc_investmentGoalInfo.demographics.invHorizonTo}" styleClass="formEntryText" style="width: 60px;"/>	
		</h:panelGroup>
		<!--end NBA297 -->
</jsp:root>