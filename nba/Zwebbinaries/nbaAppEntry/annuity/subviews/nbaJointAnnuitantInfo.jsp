<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!--SPR3551 		   8 	  Receive Exception Error on Save when Beneficiary and Annuitant Same As Owner -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- SPRNBA-515		NB-1301   Dollar ($) Denomination Missing from Investment Goals Amount -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-982		NB-1601	  Joint Annuitant Government ID Click Event Error and Not Masked -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:panelGroup id="jointAnnuitantInfoAvailable" styleClass="formDataEntryLine">
			<h:selectBooleanCheckbox id="jointAnnuitantInfoAvailableCheck" value="#{pc_jointAnnuitantInfo.jointAnnuitantAvailable}" styleClass="radioLabel" style="width: 25px;" 
				valueChangeListener="#{pc_jointAnnuitantInfo.jointAnnuitantInfoValueChange}" onclick="resetTargetFrame();submit();" disabled="false"> 
			</h:selectBooleanCheckbox>
			<h:outputText id="jointAnnuitantInfoAvailableCB" value="#{property.annuityAppJointAnnuitantInfoAvailable}" styleClass="formLabel" style="width: 315px;text-align: left;"/>
		</h:panelGroup>				
		<h:panelGroup id="jointAnnuitantInfoPGroup" rendered="#{pc_jointAnnuitantInfo.jointAnnuitantAvailable}">
			<h:outputText id="jointAnnuitantInfo" value="#{property.annuityAppEntJointAnnuitantInfo}" styleClass="formSectionBar"/>
			<h:panelGroup id="ownerChkPGroup" styleClass="formDataEntryLine">
				<h:selectBooleanCheckbox id="sameAsOwnerCheckCB" value="#{pc_jointAnnuitantInfo.sameAsOwner}" styleClass="radioLabel" style="width: 25px;" 
					valueChangeListener="#{pc_jointAnnuitantInfo.sameAsOwnerValueChange}" onclick="resetTargetFrame();submit();" 
					disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_jointAnnuitantInfo.sameAsOwnerDisabled || pc_nbaAnnuityAppEntryNavigation.sameAsOwnerBeneChecked}">  <!--  SPR3551 --> 
				</h:selectBooleanCheckbox>
				<h:outputText id="sameAsOwnerCheck" value="#{property.annuityAppEntSameAsOwner}" styleClass="formLabel" style="width: 125px;text-align: left;"/>
				<h:panelGroup id="deletejointAnnuitant" style="position: relative;left: 250px;">
					<h:selectBooleanCheckbox id="deletejointAnnuitantCB" value="#{pc_jointAnnuitantInfo.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" 
						disabled="#{pc_nbaAnnuityAppEntryNavigation.updateMode || pc_jointAnnuitantInfo.deletePartyDisabled}" 
						valueChangeListener="#{pc_jointAnnuitantInfo.deleteJointAnnuitant}" onclick="resetTargetFrame();submit()" />
					<h:outputText id="deletejointAnnuitantText" value="#{property.annuityAppEntDelJointAnuuitant}" styleClass="formLabel" style="width: 200px;text-align: left;"/>
				</h:panelGroup>
			</h:panelGroup>			
			<h:panelGroup id="jointAnnuitantNamePGroup" styleClass="formDataEntryLine">
				<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="firstNameEF" value="#{pc_jointAnnuitantInfo.demographics.firstName}" styleClass="formEntryText" style="width: 100px;"
					 disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
				<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="middelNameEF" value="#{pc_jointAnnuitantInfo.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
					 disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"  />
				<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;" />
				<h:inputText id="lastNameEF" value="#{pc_jointAnnuitantInfo.demographics.lastName}" styleClass="formEntryText" style="width: 175px;"
					 disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"  />
			</h:panelGroup>
			<h:panelGroup id="jointAnnuitantLine1PGroup" styleClass="formDataEntryLine">
				<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="addressLine1" value="#{pc_jointAnnuitantInfo.address.addressLine1}" styleClass="formEntryText" style="width: 275px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
				<h:panelGroup id="deleteAdd" style="position: relative;left: 25px;">
					<h:selectBooleanCheckbox id="deleteAddress" value="#{pc_jointAnnuitantInfo.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" 
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || pc_jointAnnuitantInfo.deleteAddressDisabled}"  
						valueChangeListener="#{pc_jointAnnuitantInfo.deleteJointAnnuitantAddress}" onclick="resetTargetFrame();submit()" />
					<h:outputText id="delAddress" value="#{property.appEntDelOthInsAdd}" styleClass="formLabel" style="width: 100px;text-align: left;"/>
				</h:panelGroup>
			</h:panelGroup>
			<h:panelGroup id="jointAnnuitantLine2PGroup" styleClass="formDataEntryLine">
				<h:inputText id="addressLine2" value="#{pc_jointAnnuitantInfo.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 100px;width: 275px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
			</h:panelGroup>
			<h:panelGroup id="jointAnnuitantLine3PGroup" styleClass="formDataEntryLine">
				<h:inputText id="addressLine3" value="#{pc_jointAnnuitantInfo.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
			</h:panelGroup>
			<!-- begin NBA247 -->
			<h:panelGroup id="jointAnnuitantLine4PGroup" styleClass="formDataEntryLine"  rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}">
				<h:inputText id="addressLine4" value="#{pc_jointAnnuitantInfo.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
			</h:panelGroup>
			<!--  end NBA247 -->
			<h:panelGroup id="jointAnnuitanttCityPGroup" styleClass="formDataEntryLine">
				<h:outputText id="jointAnnuitantcity" value="#{property.appEntCity}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="jointAnnuitantcityEF" value="#{pc_jointAnnuitantInfo.address.city}" styleClass="formEntryText" style="width: 95px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
				<h:outputText id="jointAnnuitantState" value="#{property.appEntState}" styleClass="formLabel" style="width: 130px;"/>
				<h:selectOneMenu id="jointAnnuitantStateDD" value ="#{pc_jointAnnuitantInfo.address.state}" styleClass="formEntryTextFull" style="width: 260px;"
						disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
					<f:selectItems id="jointAnnuitantStateList" value="#{pc_jointAnnuitantInfo.address.stateList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGrid id="jointAnnuitantCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" 
						cellspacing="0" style="padding-top: 6px;"><!-- NBA316 -->
				<h:column>
					<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 100px;"/>
				</h:column>
				<h:column>
					<h:selectOneRadio id="jointAnnuitantZipRB" value="#{pc_jointAnnuitantInfo.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
							disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
						<f:selectItems id="jointAnnuitantZipTypes" value="#{pc_jointAnnuitantInfo.address.zipTypeCodes}" />
					</h:selectOneRadio>				
				</h:column>
				<h:column>
					<!-- begin SPRNBA-493 -->
					<h:inputText id="zipEF" value="#{pc_jointAnnuitantInfo.address.zip}" styleClass="formEntryText" style="width: 85px;" 
							disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
						<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>
					</h:inputText> 
					<h:inputText id="postalEF" value="#{pc_jointAnnuitantInfo.address.postal}" styleClass="formEntryText" style="width: 85px;" 
							disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
					<!-- end SPRNBA-493 -->
				</h:column>
				<h:column>
					<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 69px;"/> <!-- SPRNBA-493 -->
					<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 70px;margin-top: 10px;" />	<!-- NBA316 -->
				</h:column>
				<h:column>
					<h:selectOneMenu id="countryDD" value ="#{pc_jointAnnuitantInfo.address.country}" styleClass="formEntryTextFull" style="width: 260px;"
							disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
							<f:selectItems id="countryList" value="#{pc_jointAnnuitantInfo.address.countryList}" />
					</h:selectOneMenu>	
					<h:inputText id="countyEF" value="#{pc_jointAnnuitantInfo.address.county}"
								disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 260px;margin-top: 5px;" /> <!-- NBA316 -->			
				</h:column>
			</h:panelGrid>			
			<h:panelGroup id="jointAnnuitantTaxPGroup" styleClass="formDataEntryLine">
				<h:panelGrid id="jointAnnuitantTaxPGrid" columns="2" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0" style="width: 250px;">
					<h:column>
						<h:selectOneRadio id="govtIdType" value="#{pc_jointAnnuitantInfo.demographics.ssnTC}" 
								disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" 
								layout="pageDirection" styleClass="radioLabel" style="width: 180px;"><!-- NBA340 -->
							<f:selectItem id="ssn2" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
							<f:selectItem id="sin2" itemValue="3" itemLabel="#{property.appEntSiNumber}"/> <!-- NBA340 -->
						</h:selectOneRadio>	
					</h:column>
					<h:column>	
					<!-- begin NBA340 -->
						<h:inputText id="govtIdInput" value="#{pc_jointAnnuitantInfo.demographics.ssn}" styleClass="formEntryText" style="width: 135px;margin-top: 30px;"
							disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
							<!-- SPRNBA-982 code deleted -->													
						</h:inputText>	
						<h:inputHidden id="govtIdKey" value="#{pc_jointAnnuitantInfo.demographics.govtIdKey}"></h:inputHidden>
						<!-- end NBA340 -->											 
					</h:column>
				</h:panelGrid>	
			</h:panelGroup>	
			<h:panelGroup id="jointAnnuitantHomePhonePGroup" styleClass="formDataEntryLine">
				<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="homePhoneEF" value="#{pc_jointAnnuitantInfo.contactInfo.homePhone}" styleClass="formEntryText" style="width: 125px;" 
					disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
				    <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
				</h:inputText>
				<h:outputText id="workPhone" value="#{property.appEntWorkPhone}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="workPhoneEF" value="#{pc_jointAnnuitantInfo.contactInfo.workPhone}" styleClass="formEntryText" style="width: 125px;" 
					disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
				    <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
				</h:inputText>
				<h:outputText id="ext" value="#{property.appEntExtension}" styleClass="formLabel" style="width: 50px;"/>
				<h:inputText id="extEF" value="#{pc_jointAnnuitantInfo.contactInfo.extension}" styleClass="formEntryText" style="width: 50px;" 
					disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
			</h:panelGroup>	
			<h:panelGroup id="jointAnnuitantGenderDobPGroup" styleClass="formDataEntryLine">
				<h:outputText id="gender" value="#{property.appEntGender}" styleClass="formLabel" style="width: 100px;"/>
				<h:selectOneMenu id="genderDD" value ="#{pc_jointAnnuitantInfo.demographics.gender}" styleClass="formEntryText" style="width: 125px;"
					disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >						
					<f:selectItems id="genderList" value="#{pc_jointAnnuitantInfo.demographics.genderList}" />
				</h:selectOneMenu>
				<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 100px;"/>
				<h:inputText id="dobEF" value="#{pc_jointAnnuitantInfo.demographics.birthDate}" styleClass="formEntryText" style="width: 125px;" 
					disabled="#{pc_jointAnnuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
					<f:convertDateTime pattern="#{property.datePattern}"/>
				</h:inputText>
			</h:panelGroup>
		</h:panelGroup>
		<f:verbatim>
			<hr id="jointAnnuitantSaperator" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="plannedAndReplacementPGroup" styleClass="formDataEntryLine">
			<h:outputText id="plannedInitialPayment" value="#{property.annuityAppEntPlannedInitPayment}" styleClass="formLabel" style="width: 160px;"/>
			<h:inputText id="plannedInitialPaymentEF" value="#{pc_jointAnnuitantInfo.plannedInitialPayment}" styleClass="formEntryText" style="width: 70px;" 
					disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/> <!-- SPRNBA-515 -->
			</h:inputText>
			<h:outputText id="replacement" value="#{property.annuityAppEntReplacement}" styleClass="formLabel" style="width: 95px;"/>
			<h:selectOneMenu id="replacementDD" value ="#{pc_jointAnnuitantInfo.replacement}" styleClass="formEntryText" style="width: 262px;"
					disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >	
				<f:selectItems id="replacementList" value="#{pc_jointAnnuitantInfo.replacementList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
</jsp:root>