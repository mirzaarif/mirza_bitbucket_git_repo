<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-404		NB-1101	  Need to Disable Trustee Fields for Person -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-327		NB-1601	  Verification Drop Down Should Be Moved to Right of Government ID -->
<!-- SPRNBA-326     NB-1601   Need to Disable Citizenship for wmA Annuities -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="ownerInfo" value="#{property.appEntOwnerInfo}" styleClass="formSectionBar"/>
		<h:panelGrid columns="2" style="padding-top: 5px;" columnClasses="colOwnerInfo,colMoreCodes" cellspacing="0" cellpadding="0">
		  <h:column>		
			<h:dataTable id="owners" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_annuityOwnerInformation.ownersList}" 
				var="owner" style="padding-left: 2px;">
				<h:column>
					<h:panelGroup styleClass="formDataEntryLine" rendered="#{owner.displayBar}">
						<f:verbatim>
								<hr class="formSeparator" />
						</f:verbatim>
					</h:panelGroup>
					<h:panelGroup id="introline" styleClass="formDataEntryLine">
						<h:selectOneRadio id="selectRadioGroup" value="#{owner.partyType}" 
							disabled="#{owner.personOrOrg || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" 
							onclick="resetTargetFrame();submit();"
							layout="lineDirection" styleClass="radioLabel" style="width:180px;margin-top: -5px;">
							<f:selectItem id="individual" itemValue="1" itemLabel="#{property.appEntPerson}"/>
							<f:selectItem id="group" itemValue="2" itemLabel="#{property.appEntCorporation}"/>
						</h:selectOneRadio>
						<h:panelGroup id="OwnerTypePGroup" >
							<h:outputText id="ownerType" value="#{property.appEntOwnerType}" styleClass="formLabel" style="width:95px;"/>
							<h:selectOneMenu id="ownerTypeDD" value="#{owner.ownerType}" disabled="#{owner.person || owner.personOrOrg}"
								styleClass="formEntryText" style="width:160px;">
								<f:selectItems id="ownerTypeList" value="#{owner.ownerTypeList}"/> 
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="deleteOwnerPGroup" >		 				
							<h:selectBooleanCheckbox id="deleteOwn" value="#{owner.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 167px; width:25px;" disabled="#{owner.annuityPartyDis}" valueChangeListener="#{owner.delPartyAddrDis}" onclick="resetTargetFrame();submit()"/>
							<h:outputText id="delOwner" value="#{property.appEntDelOwner}" styleClass="formLabel" style="position: relative;left: 165px; width:100px;" />						
						</h:panelGroup>
					</h:panelGroup>
					<h:panelGroup id="trusteeAggrementPGroup" styleClass="formDataEntryLine">
						<h:outputText id="trustee" value="#{property.appEntTrustee}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="trusteeEF" value="#{owner.trustee}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || owner.person}"
												styleClass="formEntryText" style="width: 170px;"/> <!-- SPRNBA-404 -->
						<h:outputText id="trusteeAggrementDate" value="#{property.appEntTrustAgreementDate}" styleClass="formLabel" style="width: 170px;"/>
						<h:inputText id="trusteeAggrementDateEF" value="#{owner.trustAgreementDate}" disabled="#{owner.person}"
												styleClass="formEntryDate" style="width: 85px;"> <!-- SPRNBA-404 -->
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
					</h:panelGroup>	
					<h:panelGroup id="ownerFullNamePGroup" styleClass="formDataEntryLine" rendered="#{owner.corporation}">
						<h:outputText id="fullName" value="#{property.appEntFullName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="fullNameEF" value="#{owner.demographics.fullName}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 300px;"/>
					</h:panelGroup>
				
					<h:panelGroup id="ownerNamePGroup" styleClass="formDataEntryLine" rendered="#{owner.person}">
						<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="firstNameEF" value="#{owner.demographics.firstName}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												onchange="resetTargetFrame();submit();" valueChangeListener="#{owner.firstNameChange}"
												styleClass="formEntryText" style="width: 90px;" immediate="true"/>
						<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
						<h:inputText id="middelNameEF" value="#{owner.demographics.midName}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 30px;"/>
						<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;"/>
						<h:inputText id="lastNameEF" value="#{owner.demographics.lastName}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												onchange="resetTargetFrame();submit();" valueChangeListener="#{owner.lastNameChange}"
												styleClass="formEntryText" style="width: 120px;" immediate="true"/>
					</h:panelGroup>
					<h:panelGroup id="ownerLine1PGroup" styleClass="formDataEntryLine">
						<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="addressLine1" value="#{owner.address.addressLine1}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 300px;"/>
						<h:panelGroup id="deleteAdd" >
							<h:selectBooleanCheckbox id="deleteAddress" value="#{owner.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox"  style="position: relative;left: 27px; width:25px;" 
							disabled="#{owner.annuityPartyAddressDis}" valueChangeListener="#{owner.delPartyDis}" onclick="resetTargetFrame();submit()"/>
							<h:outputText id="delAddress" value="#{property.appEntDelOwnerAdd}" styleClass="formLabel" style="position: relative;left: 25px; width:100px;"/>
						</h:panelGroup>
					</h:panelGroup>
					<h:panelGroup id="ownerLine2PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine2" value="#{owner.address.addressLine2}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/>
					</h:panelGroup>
					<h:panelGroup id="ownerLine3PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine3" value="#{owner.address.addressLine3}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/>
					</h:panelGroup>
					<!-- begin NBA247 -->
					<h:panelGroup id="ownerLine4PGroup" styleClass="formDataEntryLine"  rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}">
						<h:inputText id="addressLine4" value="#{owner.address.addressLine4}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"/>
					</h:panelGroup>
					<!--  end NBA247 -->
					<h:panelGroup id="ownerCityPGroup" styleClass="formDataEntryLine">
						<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="city1EF" value="#{owner.address.city}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 100px;"/>
						<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 145px;"/>
						<h:selectOneMenu id="state1DD" value ="#{owner.address.state}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
						 						styleClass="formEntryText" style="width: 200px;">
							<f:selectItems id="state1List" value="#{owner.address.stateList}" />
						</h:selectOneMenu> 
					</h:panelGroup>
					<h:panelGrid id="ownerCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort,labelAppEntry" columns="6" cellpadding="0" cellspacing="0" styleClass="formDataEntryLine"><!-- NBA316 -->
						<h:column>
							<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 95px;"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="pinsZipRB" value="#{owner.address.zipTC}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												layout="pageDirection" styleClass="radioLabel" style="width: 70px;">
								<f:selectItems value="#{owner.address.zipTypeCodes}" />
							</h:selectOneRadio>				
						</h:column>
						<h:column>
							<!-- begin SPRNBA-493 -->
							<h:inputText id="zipEF" value="#{owner.address.zip}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
									styleClass="formEntryText" style="width: 85px;" >
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>
							</h:inputText>
							<h:inputText id="postalEF" value="#{owner.address.postal}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 85px;" />
							<!-- end SPRNBA-493 --> 
						</h:column>
						<h:column>
							<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 90px;"/> <!-- SPRNBA-493 -->
							<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 90px;margin-top: 10px;" />	<!-- NBA316 -->
						</h:column>
						<h:column >
							 <h:selectOneMenu id="countryDD" value="#{owner.address.country}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
							 					styleClass="formEntryText" style="width: 200px;text-align:left">
								<f:selectItems id="countryList" value="#{owner.address.countryList}" />
							</h:selectOneMenu>
							<h:inputText id="countyEF" value="#{owner.address.county}"
								disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 200px;margin-top: 5px;" /> 	<!-- NBA316 -->				 
						</h:column>
						<h:column>
							<h:outputText id="country121" value="" styleClass="formLabel"/>
						</h:column>
					</h:panelGrid>
					<!-- begin NBA247 -->
					<h:panelGroup id="dobPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
						<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 92px;" rendered="#{owner.person}"/>
							<h:inputText id="dobEF" value="#{owner.demographics.birthDate}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.person}"
												styleClass="formEntryDate" style="width: 100px;" > 
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>				
							<h:outputText id="dobBlank" value="" styleClass="formLabel" style="width: 195px;" rendered="#{owner.corporation}"/>
					</h:panelGroup>
					<h:panelGrid columns="3" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0" style="width: 550px;">
						<h:column>
							<h:selectOneRadio id="govtIdType" value="#{owner.demographics.ssnTC}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.person}"
												layout="pageDirection" styleClass="radioLabel" style="width:130px;"> <!--NBA340-->
								<f:selectItem id="ssn" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
								<f:selectItem id="ti" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sin" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>	
							<h:selectOneRadio id="govtIdCorpType" value="#{owner.demographics.ssnTC}" disabled="#{ pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" rendered="#{owner.corporation}"
												layout="pageDirection" styleClass="radioLabel" style="width: 130px;"> <!--NBA340-->
								<f:selectItem id="tincorp" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sincorp" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>			
						</h:column>
						<h:column>
						<!-- begin NBA340 -->
							<h:inputText id="govtIdInput" value="#{owner.demographics.ssn}" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
												styleClass="formEntryText" style="width: 90px;margin-top: 50px;margin-bottom:0px" >	<!-- SPRNBA-327 -->						
							</h:inputText>	
							<h:inputHidden id="govtIdKey" value="#{owner.demographics.govtIdKey}"></h:inputHidden>											 
						</h:column>
						<!-- end NBA340 -->
						<h:column>	
							<h:outputText id="taxVerification" value="Verification" styleClass="formLabel" style="width: 90px;margin-top: 50px;"/> <!-- SPRNBA-327 -->
							<h:selectOneMenu id="taxVerificationDD" value ="#{owner.demographics.taxIDVerification}" styleClass="formEntryTextFull" style="width: 240px;margin-top: 50px;" 
								disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
								<f:selectItems id="taxVerificationList" value="#{owner.demographics.taxIDVerificationList}" />
							</h:selectOneMenu>							 		
						</h:column>
					</h:panelGrid>
					<!--  end NBA247 -->
					<h:panelGroup id="annuitantHomePhonePGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
							<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 90px;text-align:left;"/>
							<h:inputText id="homePhoneEF" value="#{owner.contactInfo.homePhone}" styleClass="formEntryText" style="width: 125px;" 
								disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
							   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
							</h:inputText>
							<h:outputText id="workPhone" value="#{property.appEntWorkPhone}" styleClass="formLabel" style="width: 100px;"/>
							<h:inputText id="workPhoneEF" value="#{owner.contactInfo.workPhone}" styleClass="formEntryText" style="width: 125px;" 
								disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
							   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
							</h:inputText>
							<h:outputText id="ext" value="#{property.appEntExtension}" styleClass="formLabel" style="width: 50px;"/>
							<h:inputText id="extEF" value="#{owner.contactInfo.extension}" styleClass="formEntryText" style="width: 50px;" 
								disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
					</h:panelGroup>
					<h:panelGroup id="ownerRelationshipPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
						<h:outputText id="relationShip" value="#{property.annuityAppRelationToAnnuitant}" styleClass="formLabel" style="width: 150px;text-align:left;"/>						
					 	<h:selectOneMenu id="relationShipDD" value="#{owner.relToAnnuitant}" 
					 							styleClass="formEntryTextFull" style="width: 290px;"> 
							<f:selectItems id="relationShipList" value="#{owner.relToAnnuitantList}"/>
						</h:selectOneMenu>  
					</h:panelGroup>
					<h:panelGroup id="ownerCitizenshipPGroup" styleClass="formDataEntryLine" rendered="#{owner.person}" style="padding-left:5px;">
						<h:selectBooleanCheckbox id="usCitizenCB" value="#{owner.demographics.usCitizen}" 
								styleClass="ovFullCellSelectCheckBox" style="width: 12px;text-align:left;"/> 
						<h:outputText id="usCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;"/>
						<h:outputText id="citizenship" value="#{property.annuityAppCountryOfCitizenship}" styleClass="formLabel" style="width: 165px;"/>
						<h:selectOneMenu id="citizenshipDD" value="#{owner.demographics.usCitizenship}" 
							disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"	
							styleClass="formEntryText" style="width: 200px;">	<!-- SPRNBA-326 -->
							<f:selectItems id="citizenshipList" value="#{owner.demographics.usCitizenshipList}" />
						</h:selectOneMenu>							
					</h:panelGroup>	 															
					<h:panelGroup id="annuitantVisaPGroup" styleClass="formDataEntryLine" style="padding-left:5px;" rendered="#{owner.person}">
							<h:outputText id="cardNumber" value="#{property.annuityAppPermanentResidentCardNum}" styleClass="formLabel" style="width: 225px;text-align:left;"/>
							<h:inputText id="cardNumberIF" value="#{owner.demographics.permanentResCardNum}" styleClass="formEntryText" style="width: 194px;text-align:left;" />
					</h:panelGroup>
					<h:panelGroup id="annuiantvisanumber" styleClass="formDataEntryLine" style="padding-left:5px;" rendered="#{owner.person}">	
                            <h:outputText id="VisaNumber" value="#{property.annuityAppVisaNum}" styleClass="formLabel" style="width:95px;text-align:left;"/>		
							<h:inputText id="VisaNumberIF" value="#{owner.demographics.visaNumber}" styleClass="formEntryText" style="width: 160px;" />
					        <h:outputText id="visaType1" value="#{property.appEntVisaType}" styleClass="formLabel" style="width: 120px;"/>
							<h:selectOneMenu id="visaTypeDD" value ="#{owner.demographics.visaType}" styleClass="formEntryText" style="width: 160px;">
								<f:selectItems id="visaTypeList" value="#{owner.demographics.visaTypeList}" />
							</h:selectOneMenu>
				    </h:panelGroup>			
			    </h:column>
			</h:dataTable>
		   </h:column>
		   <h:column>
			 <h:commandLink value="#{property.buttonAddAnother}" 
			  				 action="#{pc_annuityOwnerInformation.addAnotherOwner}" 
			  				 styleClass="formButtonInterface" 
			  				 style="position: relative;right: 25px;width: 20px"
			  				 rendered="#{!pc_annuityOwnerInformation.addAnotherDisabled}"/> <!-- NBA247 -->
			  	<h:outputText id="addanother" 
			  				  value="#{property.buttonAddAnother}" 
			  				  styleClass="formButtonInterface" 
			  				  style="width: 65px;margin-left: 5px;" 
			  				  rendered="#{pc_annuityOwnerInformation.addAnotherDisabled}"/> <!--  NBA247 -->
		  </h:column>
		</h:panelGrid>  
</jsp:root>