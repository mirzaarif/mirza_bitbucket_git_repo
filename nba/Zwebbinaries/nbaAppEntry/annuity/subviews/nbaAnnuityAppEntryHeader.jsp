<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- NBA231 		   		NB-1101	Replacement Processing  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:panelGroup id="planTransactionPGroup" styleClass="formDataEntryLine">
			<h:outputText id="plan" value="#{property.appEntPlan}"  styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="planVal" value="#{pc_planInfo.plan}" style="width: 460px;" styleClass="formEntryText" 
				disabled="true">
				<f:selectItems id="planValList" value="#{pc_planInfo.planList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="TransactionPGroup" styleClass="formDataEntryLine">
			<h:outputText id="transactionNumber" value="#{property.annuityAppTransType}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="transactionNumberEF" value="#{pc_annuityApplicationInformation.transactionNumber}" styleClass="formEntryText" style="width: 100px;" >
			</h:inputText>					
		</h:panelGroup>
		<h:panelGroup id="appDateComDatePGroup" styleClass="formDataEntryLine">
			<h:outputText id="applicationDate" value="#{property.annuityApplicationDate}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="applicationDateEF" value="#{pc_annuityApplicationInformation.applicationDate}" styleClass="formEntryText" style="width: 100px;" 
				disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputText id="payOutComDate" value="#{property.annuityAppPayOutComDate}" styleClass="formLabel" style="width: 260px;"/>
			<h:inputText id="payOutComDateEF" value="#{pc_annuityApplicationInformation.payOutComDate}" styleClass="formEntryText" style="width: 100px;" 
				disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="appTypeContriYearPGroup" styleClass="formDataEntryLine">
			<h:outputText id="appType" value="#{property.annuityAppType}" styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="appTypeDD" value ="#{pc_applicationInfo.applicationType}" styleClass="formEntryText" style="width: 460px;"
				disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">						
				<f:selectItems id="appTypeList" value="#{pc_applicationInfo.applicationTypeList}" />	<!-- NBA231 -->
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="ContriYearPGroup" styleClass="formDataEntryLine">
			<h:outputText id="contriYear" value="#{property.annuityAppContriPlan}" styleClass="formLabel" style="width: 120px;"/>
			<h:inputText id="contriYearEF" value="#{pc_annuityApplicationInformation.contriYear}" styleClass="formEntryText" style="width: 100px;" 
				disabled="#{pc_annuityApplicationInformation.contrYearDisabled || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" 
				binding="#{pc_annuityApplicationInformation.contributionYear}" >
			</h:inputText>
			<!-- begin NBA247 -->
			<h:outputText id="maturityDate" value="#{property.annuityAppMaturityDate}" styleClass="formLabel" style="width: 260px;"
				rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"/>
			<h:inputText id="maturityDateEF" value="#{pc_annuityApplicationInformation.maturityDate}" styleClass="formEntryText" style="width: 100px;" 
				rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<!-- end NBA247 -->
		</h:panelGroup>
		<h:panelGroup id="qualificationTypePGroup" styleClass="formDataEntryLine">
			<h:outputText id="qualificationType" value="#{property.annuityAppQualificationType}" styleClass="formLabel" style="width: 120px;"/>
			<h:selectOneMenu id="qualificationTypeDD" value ="#{pc_annuityApplicationInformation.qualificationType}" styleClass="formEntryText" style="width: 460px;" 
				valueChangeListener="#{pc_annuityApplicationInformation.contriYearDis}" onchange="submit()"
				disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">						
				<f:selectItems id="aqualificationTypeList" value="#{pc_annuityApplicationInformation.qualificationTypeList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
		<!--  begin NBA247  -->
	<h:panelGroup id="statutoryCodeGroup" styleClass="formDataEntryLine"  rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}">
		<h:outputText id="statutoryCode" value="#{property.appEntStatutory}" styleClass="formLabel" style="width: 125px;" />
		<h:selectOneMenu id="statutoryCodeSelect" value="#{pc_annuityApplicationInformation.statutoryCode}" styleClass="formEntryTexthalf"
				disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
				onchange="resetTargetFrame();submit();"> <!--  NBA211 -->
			<f:selectItems id="statutoryCodeList" value="#{pc_annuityApplicationInformation.statutoryCodeList}" />
		</h:selectOneMenu>
	</h:panelGroup>	
	<!-- end NBA247 -->	
</jsp:root>