<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG --> 
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!--SPR3551 		   8 	  Receive Exception Error on Save when Beneficiary and Annuitant Same As Owner -->
<!--SPR3678 		   8 	  Beneficiary Percentage field is disabled if the owner is same as Beneficiary -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-326     NB-1601   Need to Disable Citizenship for wmA Annuities -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="pinsInfo" value="#{property.appEntBeneficiaryInformation}" styleClass="formSectionBar"/>
		
		<h:panelGrid columns="2" style="padding-top: 5px;" columnClasses="colOwnerInfo,colMoreCodes" cellspacing="0" cellpadding="0">
			<h:column>
			  <h:dataTable id="beneficiary" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_annuityBeneficiaryInformation.beneficiaryList}" 
				var="beneficiary" style="padding-left: 2px;" binding="#{pc_annuityBeneficiaryInformation.beneficiaryTable}">
				<h:column>
					<h:panelGroup id="beneficiaryBar" styleClass="formDataEntryLine" rendered="#{beneficiary.displayBar}">
						<f:verbatim>
								<hr id="separator1" class="formSeparator" />
						</f:verbatim>
					</h:panelGroup>
					<h:panelGroup id="ownerChkPGroup" styleClass="formDataEntryLine">
						<h:selectBooleanCheckbox id="sameAsOwnerCheckCB" value="#{beneficiary.sameAsOwner}" styleClass="radioLabel" style="width: 25px;"
							disabled="#{beneficiary.disableSameAsOwnerCB ||pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || pc_nbaAnnuityAppEntryNavigation.sameAsOwnerAnnuitantChecked}" 
							binding="#{pc_annuityBeneficiaryInformation.sameAsOwnerCB}" 
							valueChangeListener="#{pc_annuityBeneficiaryInformation.sameAsOwnerChecked}" onclick="resetTargetFrame();submit();">  <!--  SPR3551 -->
						</h:selectBooleanCheckbox>
						<h:outputText id="sameAsOwnerCheckB" value="#{property.annuityAppEntSameAsOwner}" styleClass="formLabel" style="width: 125px;text-align: left;"/>
					</h:panelGroup>
					<h:panelGroup id="beneficiaryPGroup" styleClass="formDataEntryLine">
						<h:outputLabel id="beneficiaryType" value="#{property.appEntBeneficiaryType}" styleClass="formLabel" style="width: 115px;"/> 
						<h:selectOneMenu id="beneficiaryTypeEF" value="#{beneficiary.beneficiaryType}" styleClass="formEntryText" style="width: 100px;"
							disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || beneficiary.personOrOrg}"> 
							<f:selectItems id="beneficiaryTypeList" value="#{beneficiary.beneficiaryTypeList}" />
						</h:selectOneMenu>
						<h:outputLabel id="beneficiaryBlank" value="" styleClass="formLabel" style="width: 20px;" />
						<h:selectBooleanCheckbox id="irrevocableCheck" value="#{beneficiary.irrevocable}" styleClass="ovFullCellSelectCheckBox" style="position:relative;left:30px;width: 15px;" 
							disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">  
						</h:selectBooleanCheckbox>
						<h:outputLabel id="irrevocableCheckLabel" value="#{property.appEntIrrevocable}" styleClass="formLabel" style="position:relative;left:30px;width: 75px;"  />
						<h:outputLabel id="delBeneficiaryBlank" value="" styleClass="formLabel" style="width: 40px;" />
						<h:selectBooleanCheckbox id="deleteBeneficiary" value="#{beneficiary.deleteParty}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 50px; width:25px;"  disabled="#{beneficiary.annuityPartyDis}"
						valueChangeListener="#{beneficiary.delPartyAddrDis}" onclick="resetTargetFrame();submit()" />
						<h:outputText id="delBeneficiary" value="#{property.appEntDelBenificiary}" styleClass="formLabel" style="position: relative;left: 50px; width:130px;"/>
					</h:panelGroup>
					<h:panelGroup>
						<h:outputText value="" styleClass="formLabel" style="width: 115px;"></h:outputText>
						<h:selectOneRadio id="selectRadioGroup" value="#{beneficiary.partyType}" layout="lineDirection" styleClass="radioLabel" style="width:180px;margin-top: -5px;" onclick="resetTargetFrame();submit();"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled  || beneficiary.personOrOrg}"> 
								<f:selectItem id="individual" itemValue="1" itemLabel="#{property.appEntPerson}"/>
								<f:selectItem id="group" itemValue="2" itemLabel="#{property.appEntCorporation}"/>
						</h:selectOneRadio>							
					</h:panelGroup>		
					<h:panelGroup id="pinsNamePGroup" styleClass="formDataEntryLine" rendered="#{beneficiary.person}">
						<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="firstNameEF" value="#{beneficiary.demographics.firstName}" styleClass="formEntryText" style="width: 90px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
						<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
						<h:inputText id="middelNameEF" value="#{beneficiary.demographics.midName}" styleClass="formEntryText" style="width: 30px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
						<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;"/>
						<h:inputText id="lastNameEF" value="#{beneficiary.demographics.lastName}" styleClass="formEntryText" style="width: 120px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<h:panelGroup id="beneficiaryFullNamePGroup" styleClass="formDataEntryLine" rendered="#{beneficiary.corporation}">
						<h:outputText id="fullName" value="#{property.appEntFullName}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="fullNameEF" value="#{beneficiary.demographics.fullName}" styleClass="formEntryText" style="width: 300px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<h:panelGroup id="beneficiaryLine1PGroup" styleClass="formDataEntryLine">
						<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="addressLine1" value="#{beneficiary.address.addressLine1}" styleClass="formEntryText" style="width: 300px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
						<h:panelGroup id="deleteAdd">
							<h:selectBooleanCheckbox id="deleteAddress" value="#{beneficiary.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 27px; width:25px;" disabled="#{beneficiary.annuityPartyAddressDis}"
								valueChangeListener="#{beneficiary.delPartyDis}" onclick="resetTargetFrame();submit()" />
							<h:outputText id="delAddress" value="#{property.appEntDelBenAdd}" styleClass="formLabel" style="position: relative;left: 25px; width:100px;"/>
						</h:panelGroup>	
					</h:panelGroup>
					<h:panelGroup id="beneficiaryLine2PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine2" value="#{beneficiary.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<h:panelGroup id="beneficiaryLine3PGroup" styleClass="formDataEntryLine">
						<h:inputText id="addressLine3" value="#{beneficiary.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<!-- begin NBA247 -->
					<h:panelGroup id="beneficiaryLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"> 
						<h:inputText id="addressLine4" value="#{beneficiary.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 95px; width: 300px;"
							disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
					</h:panelGroup>
					<!-- end NBA247 -->
					<h:panelGroup id="beneficiaryCityPGroup" styleClass="formDataEntryLine">
						<h:outputText id="city1" value="#{property.appEntCity}" styleClass="formLabel" style="width: 95px;"/>
						<h:inputText id="city1EF" value="#{beneficiary.address.city}" styleClass="formEntryText" style="width: 100px;" disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
						<h:outputText id="state1" value="#{property.appEntState}" styleClass="formLabel" style="width: 145px;"/>
						<h:selectOneMenu id="state1DD" value ="#{beneficiary.address.state}" styleClass="formEntryText" style="width: 200px;" disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"> <!--  NBA211 -->
							<f:selectItems id="beneficiaryState1List" value="#{beneficiary.address.stateList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGrid id="beneficiaryCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort,labelAppEntry" columns="6" cellpadding="0" cellspacing="0" styleClass="formDataEntryLine"><!-- NBA316 -->
						<h:column>
							<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 95px;"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="beneficiaryZipRB" value="#{beneficiary.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"> 
								<f:selectItems id="beneficiaryZipTypes" value="#{beneficiary.address.zipTypeCodes}" />
							</h:selectOneRadio>				
						</h:column>
						<h:column>
							<!-- begin SPRNBA-493 -->
							<h:inputText id="zipEF" value="#{beneficiary.address.zip}" styleClass="formEntryText" style="width: 85px;" 
									disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>
							</h:inputText> 
							<h:inputText id="postalEF" value="#{beneficiary.address.postal}" styleClass="formEntryText" style="width: 85px;" 
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 
							<!-- end SPRNBA-493 -->
						</h:column>
						<h:column>
							<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 90px;"/> <!-- SPRNBA-493 -->
							<h:outputText id="county" value="#{property.appEntCounty}"
								styleClass="formLabel" style="width: 90px;margin-top: 10px;" />	<!-- NBA316 -->
						</h:column>
						<h:column>
							<h:selectOneMenu id="countryDD" value ="#{beneficiary.address.country}" styleClass="formEntryText" style="width: 200px;"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
								<f:selectItems id="beneficiaryCountryList" value="#{beneficiary.address.countryList}" />
							</h:selectOneMenu>	
							<h:inputText id="countyEF" value="#{beneficiary.address.county}"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
								styleClass="formEntryText" maxlength="100" style="width: 200px;margin-top: 5px;" /> 	<!-- NBA316 -->			
						</h:column>
						<h:column>
							<h:outputText id="country121" value="" styleClass="formLabel"/>
						</h:column>
					</h:panelGrid>
					<h:panelGrid columns="3" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0" style="width: 564px;">
						<h:column>
							<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 95px;" rendered="#{beneficiary.person}"/>
							<h:inputText id="dobEF" value="#{beneficiary.demographics.birthDate}" styleClass="formEntryDate" style="width: 100px;" rendered="#{beneficiary.person}"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"> 
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>				
							<h:outputText id="dobBlank" value="" styleClass="formLabel" style="width: 195px;" rendered="#{beneficiary.corporation}"/>
						</h:column>
						<h:column>
							<h:selectOneRadio id="govtIdType" value="#{beneficiary.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 180px;" rendered="#{beneficiary.person}"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"> <!--NBA340-->
								<f:selectItem id="ssn" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
								<f:selectItem id="tin" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sin" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>	
							<h:selectOneRadio id="govtIdCorpType" value="#{beneficiary.demographics.ssnTC}" layout="pageDirection" styleClass="radioLabel" style="width: 180px;" rendered="#{beneficiary.corporation}"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"> <!--NBA340 -->
								<f:selectItem id="tincorp" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
								<f:selectItem id="sincorp" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
							</h:selectOneRadio>			
								
						</h:column>
						<h:column>
						<!-- begin NBA340 -->
							<h:inputText id="govtIdInput" value="#{beneficiary.demographics.ssn}" styleClass="formEntryText" style="width: 125px;margin-top: 50px;"
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >							
							</h:inputText>
							<h:inputHidden id="govtIdKey" value="#{beneficiary.demographics.govtIdKey}"></h:inputHidden>												 
						<!-- end NBA340 -->
						</h:column>
					</h:panelGrid>
					<h:panelGroup id="annuitantHomePhonePGroup" styleClass="formDataEntryLine">
							<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 95px;"/>
							<h:inputText id="homePhoneEF" value="#{beneficiary.contactInfo.homePhone}" styleClass="formEntryText" style="width: 125px;" disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
							   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
							</h:inputText>
							<h:outputText id="workPhone" value="#{property.appEntWorkPhone}" styleClass="formLabel" style="width: 100px;"/>
							<h:inputText id="workPhoneEF" value="#{beneficiary.contactInfo.workPhone}" styleClass="formEntryText" style="width: 125px;" disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
							   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
							</h:inputText>
							<h:outputText id="ext" value="#{property.appEntExtension}" styleClass="formLabel" style="width: 50px;"/>
							<h:inputText id="extEF" value="#{beneficiary.contactInfo.extension}" styleClass="formEntryText" style="width: 50px;" disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
					</h:panelGroup>
					<h:panelGroup id="beneCitizenshipPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
						<h:outputText id="relationShip" value="#{property.annuityAppRelationToAnnuitant}" styleClass="formLabel" style="width: 125px;text-align:left;"/>
						<h:selectOneMenu id="relationShipDD" value="#{beneficiary.relToAnnuitant}" styleClass="formEntryTextFull" style="width: 185px;" disabled="#{beneficiary.sameAsOwner}">
							<f:selectItems id="beneficiaryRelationToList" value="#{beneficiary.relToAnnuitantList}" />
						</h:selectOneMenu>					
						<h:outputText id="percentage" value="#{property.appEntPercentage}" styleClass="formLabel" style="width: 105px;"/>
						<h:inputText id="percentageEF" value="#{beneficiary.percentage}" styleClass="formEntryText" style="width: 125px;" 
							disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/> 				<!-- SPR3678 -->
					</h:panelGroup>
					<h:panelGroup id="beneficiaryCitizenshipPGroup" styleClass="formDataEntryLine" rendered="#{beneficiary.person}" style="padding-left:5px;">
						<h:selectBooleanCheckbox id="beneficiaryUSCitizenCB" value="#{beneficiary.demographics.usCitizen}" disabled="#{beneficiary.sameAsOwner}"
												styleClass="ovFullCellSelectCheckBox" style="width: 15px;text-align:left; "/> 
						<h:outputText id="beneficiaryUSCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;"/>
						<h:outputText id="beneficiarycitizenship" value="#{property.annuityAppCountryOfCitizenship}" styleClass="formLabel" style="width: 165px;"/>
						<h:selectOneMenu id="beneficiarycitizenshipDD" value="#{beneficiary.demographics.usCitizenship}" 
								disabled="#{beneficiary.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"
								styleClass="formEntryText" style="width: 200px;"> 		<!-- SPRNBA-326 -->
							<f:selectItems id="beneficiarycitizenshipList" value="#{beneficiary.demographics.usCitizenshipList}" />
						</h:selectOneMenu>							
					</h:panelGroup>	 															
					<h:panelGroup id="beneficiaryVisaPGroup" styleClass="formDataEntryLine" style="padding-left:5px;" rendered="#{beneficiary.person}">
							<h:outputText id="beneficiarycardNumber" value="#{property.annuityAppPermanentResidentCardNum}" styleClass="formLabel" style="width: 225px;text-align:left;"/>
							<h:inputText id="beneficiarycardNumberIF" value="#{beneficiary.demographics.permanentResCardNum}" styleClass="formEntryText" style="width: 194px;text-align:left;" disabled="#{beneficiary.sameAsOwner}"/>
					</h:panelGroup>
					<h:panelGroup id="beneficiaryVisaPGroup1" styleClass="formDataEntryLine" style="padding-left:5px;" rendered="#{beneficiary.person}">		
							<h:outputText id="beneficiaryVisaNumber" value="#{property.annuityAppVisaNum}" styleClass="formLabel" style="width:95px;text-align:left;"/>		
							<h:inputText id="beneficiaryVisaNumberIF" value="#{beneficiary.demographics.visaNumber}" styleClass="formEntryText" style="width: 160px;" disabled="#{beneficiary.sameAsOwner}"/>
							<h:outputText id="beneficiaryVisaType1" value="#{property.appEntVisaType}" styleClass="formLabel" style="width: 120px;"/>
							<h:selectOneMenu id="beneficiaryVisaTypeDD" value ="#{beneficiary.demographics.visaType}" styleClass="formEntryText" style="width: 160px;" disabled="#{beneficiary.sameAsOwner}">
								<f:selectItems id="beneficiaryVisaTypeList" value="#{beneficiary.demographics.visaTypeList}" />
							</h:selectOneMenu>
					</h:panelGroup>			
				</h:column>
			  </h:dataTable>
			</h:column>
			<h:column>
				<h:commandLink value="#{property.buttonAddAnother}" action="#{pc_annuityBeneficiaryInformation.addAnotherBeneficiary}" styleClass="formButtonInterface" style="position: relative;right: 25px;width: 20px;margin-top:-12px;"/>
			</h:column>
		</h:panelGrid>


</jsp:root>
