<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!--SPR3551 		   8 	  Receive Exception Error on Save when Beneficiary and Annuitant Same As Owner -->
<!--SPR3563 		   8 	  Gender Should Be Enabled on Annuity Tab 2 When Annuitant Same As Owner -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA316			NB-1301   Address Normalization Using Web Service -->
<!-- NBA340         NB-1501   Mask Government ID -->
<!-- SPRNBA-326     NB-1601   Need to Disable Citizenship for wmA Annuities -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
	<h:outputText id="annuitantInfo" value="#{property.annuityAppEntAnnuitantInfo}" styleClass="formSectionBar"/>
	<h:panelGroup id="ownerChkPGroup" styleClass="formDataEntryLine">
		<h:selectBooleanCheckbox id="sameAsOwnerCheckCB" value="#{pc_annuitantInfo.sameAsOwner}" styleClass="radioLabel" style="width: 25px;" 
				valueChangeListener="#{pc_annuitantInfo.sameAsOwnerValueChange}" onclick="resetTargetFrame();submit();" 
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_annuitantInfo.sameAsOwnerDisabled || pc_nbaAnnuityAppEntryNavigation.sameAsOwnerBeneChecked}">  <!--  SPR3551 -->
		</h:selectBooleanCheckbox>
		<h:outputText id="sameAsOwnerCheck" value="#{property.annuityAppEntSameAsOwner}" styleClass="formLabel" style="width: 125px;text-align: left;"/>
	</h:panelGroup>
	<h:panelGroup id="annuitantNamePGroup" styleClass="formDataEntryLine">
		<h:outputText id="firstName" value="#{property.appEntFirstName}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="firstNameEF" value="#{pc_annuitantInfo.demographics.firstName}" styleClass="formEntryText" style="width: 100px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"  />
		<h:outputText id="middelName" value="#{property.appEntMidName}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="middelNameEF" value="#{pc_annuitantInfo.demographics.midName}" styleClass="formEntryText" style="width: 30px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
		<h:outputText id="lastName" value="#{property.appEntLastName}" styleClass="formLabel" style="width: 80px;" />
		<h:inputText id="lastNameEF" value="#{pc_annuitantInfo.demographics.lastName}" styleClass="formEntryText" style="width: 175px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
	</h:panelGroup>
	<h:panelGroup id="annuitantLine1PGroup" styleClass="formDataEntryLine">
		<h:outputText id="address" value="#{property.appEntAdd}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="addressLine1" value="#{pc_annuitantInfo.address.addressLine1}" styleClass="formEntryText" style="width: 275px;"
		    disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
		<h:panelGroup id="deleteAdd" style="position: relative;left: 95px;width: 170px;">
			<h:selectBooleanCheckbox id="deleteAddress" value="#{pc_annuitantInfo.deletePartyAddress}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" 
				disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"  valueChangeListener="#{pc_annuitantInfo.deleteAnnuitantAddress}" onclick="resetTargetFrame();submit()" />
			<h:outputText id="delAddress" value="#{property.appEntDelOthInsAdd}" styleClass="formLabel" style="width: 100px;text-align: left;"/>
		</h:panelGroup>
	</h:panelGroup>
	<h:panelGroup id="annuitantLine2PGroup" styleClass="formDataEntryLine">
		<h:inputText id="addressLine2" value="#{pc_annuitantInfo.address.addressLine2}" styleClass="formEntryText" style="position: relative;left: 100px;width: 275px;"
		        disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"    />
	</h:panelGroup>
	<h:panelGroup id="annuitantLine3PGroup" styleClass="formDataEntryLine">
		<h:inputText id="addressLine3" value="#{pc_annuitantInfo.address.addressLine3}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
			disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
	</h:panelGroup>
	<!-- begin NBA247 -->
	<h:panelGroup id="annuitantLine4PGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"> 
		<h:inputText id="addressLine4" value="#{pc_annuitantInfo.address.addressLine4}" styleClass="formEntryText" style="position: relative;left: 100px; width: 275px;"
			disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
	</h:panelGroup>
	<!-- end NBA247 -->
	<h:panelGroup id="annuitantCityPGroup" styleClass="formDataEntryLine">
		<h:outputText id="annuitantcity" value="#{property.appEntCity}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="annuitantcityEF" value="#{pc_annuitantInfo.address.city}" styleClass="formEntryText" style="width: 95px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
		<h:outputText id="annuitantState" value="#{property.appEntState}" styleClass="formLabel" style="width: 130px;"/>
		<h:selectOneMenu id="annuitantStateDD" value ="#{pc_annuitantInfo.address.state}" styleClass="formEntryTextFull" style="width: 260px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
			<f:selectItems id="annuitantStateList" value="#{pc_annuitantInfo.address.stateList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid id="annuitantCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort,colMIBShort,colMIBShort" columns="5" cellpadding="0" cellspacing="0" 
					style="padding-top: 6px;"><!-- NBA316 -->
		<h:column>
			<h:outputText id="code" value="#{property.appEntCode}" styleClass="formLabel" style="width: 100px;"/>
		</h:column>
		<h:column>
			<h:selectOneRadio id="annuitantZipRB" value="#{pc_annuitantInfo.address.zipTC}" layout="pageDirection" styleClass="radioLabel" style="width: 70px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
				<f:selectItems id="annuitantZipTypes" value="#{pc_annuitantInfo.address.zipTypeCodes}" />
			</h:selectOneRadio>				
		</h:column>
		<h:column>
			<!-- begin SPRNBA-493 -->
			<h:inputText id="zipEF" value="#{pc_annuitantInfo.address.zip}" styleClass="formEntryText" style="width: 85px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
				<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/>
			</h:inputText>
			<h:inputText id="postalEF" value="#{pc_annuitantInfo.address.postal}" styleClass="formEntryText" style="width: 85px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
			<!-- end SPRNBA-493 -->
		</h:column>
		<h:column>
			<h:outputText id="country" value="#{property.appEntCountry}" styleClass="formLabel" style="width: 69px;"/> <!-- SPRNBA-493 -->
			<h:outputText id="county" value="#{property.appEntCounty}"
				styleClass="formLabel" style="width: 70px;margin-top: 10px;" />	<!-- NBA316 -->
		</h:column>
		<h:column>
			<h:selectOneMenu id="countryDD" value ="#{pc_annuitantInfo.address.country}" styleClass="formEntryTextFull" style="width: 260px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
					<f:selectItems id="countryList" value="#{pc_annuitantInfo.address.countryList}" />
			</h:selectOneMenu>
			<h:inputText id="countyEF"
				value="#{pc_annuitantInfo.address.county}" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"
				styleClass="formEntryText" maxlength="100" style="width: 260px;margin-top: 5px;" />	<!-- NBA316 -->					
		</h:column>
	</h:panelGrid>			
	<h:panelGroup id="annuitantTaxPGroup" styleClass="formDataEntryLine">
		<h:panelGrid id="annuitantTaxPGrid" columns="4" styleClass="formDataEntryLine" cellpadding="0" cellspacing="0" style="width: 250px;"> <!-- NBA247 -->
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_annuitantInfo.demographics.ssnTC}" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" 
					layout="pageDirection" styleClass="radioLabel" style="width: 150px;"> <!--NBA247 NBA340 -->
					<f:selectItem id="ssn2" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
					<f:selectItem id="sin2" itemValue="3" itemLabel="#{property.appEntSiNumber}"/> <!-- NBA340 -->
				</h:selectOneRadio>	
			</h:column>
			<h:column>	
			<!-- begin NBA340 -->
				<h:inputText id="govtIdInput" value="#{pc_annuitantInfo.demographics.ssn}" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" 
													styleClass="formEntryText" style="width: 135px;margin-top: 30px;">
				</h:inputText>	
				<h:inputHidden id="govtIdKey" value="#{pc_annuitantInfo.demographics.govtIdKey}"></h:inputHidden>											 		
			<!-- end NBA340 -->
			</h:column>
			<!-- begin NBA247 -->
			<h:column>	
				<h:outputText id="taxVerification" value="Verification" styleClass="formLabel" style="width: 90px;margin-top: 30px;"/>
			</h:column>
			<h:column>	
				<h:selectOneMenu id="taxVerificationDD" value ="#{pc_annuitantInfo.demographics.taxIDVerification}" styleClass="formEntryTextFull" style="width: 240px;margin-top: 30px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
					<f:selectItems id="taxVerificationList" value="#{pc_annuitantInfo.demographics.taxIDVerificationList}" />
			</h:selectOneMenu>							 		
			</h:column>
			<!-- end NBA247 -->					
		</h:panelGrid>	
	</h:panelGroup>		
	<h:panelGroup id="annuitantHomePhonePGroup" styleClass="formDataEntryLine">
		<h:outputText id="homePhone" value="#{property.appEntHomePhone}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="homePhoneEF" value="#{pc_annuitantInfo.contactInfo.homePhone}" styleClass="formEntryText" style="width: 125px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
			   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
		</h:inputText>
		<h:outputText id="workPhone" value="#{property.appEntWorkPhone}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="workPhoneEF" value="#{pc_annuitantInfo.contactInfo.workPhone}" styleClass="formEntryText" style="width: 125px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
			   <f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>
		</h:inputText>
		<h:outputText id="ext" value="#{property.appEntExtension}" styleClass="formLabel" style="width: 50px;"/>
		<h:inputText id="extEF" value="#{pc_annuitantInfo.contactInfo.extension}" styleClass="formEntryText" style="width: 50px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" />
	</h:panelGroup>
	<h:panelGroup id="annuitantGenderDobPGroup" styleClass="formDataEntryLine">
		<h:outputText id="gender" value="#{property.appEntGender}" styleClass="formLabel" style="width: 100px;"/>
		<h:selectOneMenu id="genderDD" value ="#{pc_annuitantInfo.demographics.gender}" styleClass="formEntryText" style="width: 125px;" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >		<!-- SPR3563 -->				
			<f:selectItems id="genderList" value="#{pc_annuitantInfo.demographics.genderList}" />
		</h:selectOneMenu>
		<h:outputText id="dob" value="#{property.appEntDob}" styleClass="formLabel" style="width: 100px;"/>
		<h:inputText id="dobEF" value="#{pc_annuitantInfo.demographics.birthDate}" styleClass="formEntryText" style="width: 125px;" disabled="#{pc_annuitantInfo.sameAsOwner || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" >
			<f:convertDateTime pattern="#{property.datePattern}"/>
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="annuitantCitizenPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
		<h:selectBooleanCheckbox id="usCitizenCB" value="#{pc_annuitantInfo.demographics.usCitizen}" styleClass="ovFullCellSelectCheckBox" 
				style="width: 25px;text-align:left;" disabled="#{pc_annuitantInfo.sameAsOwner}" /> 
		<h:outputText id="usCitizen" value="#{property.appEntUsCitizenCB}" styleClass="formLabel" style="width: 135px;text-align: left;"/>			
		<h:outputText id="citizenship" value="#{property.annuityAppCountryOfCitizenship}" styleClass="formLabel" style="width: 160px;"/> 
		<h:selectOneMenu id="citizenshipDD" value ="#{pc_annuitantInfo.demographics.usCitizenship}" styleClass="formEntryText" style="width: 260px;" 
			disabled="#{pc_annuitantInfo.sameAsOwner  || pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled || pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}" >	<!-- SPRNBA-326 -->
			<f:selectItems id="citizenshipList" value="#{pc_annuitantInfo.demographics.usCitizenshipList}" />
		</h:selectOneMenu>
	</h:panelGroup>		
	<h:panelGroup id="annuitantVisaPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
		<h:outputText id="cardNumber" value="#{property.annuityAppPermanentResidentCardNum}" styleClass="formLabel" style="width: 225px;text-align:left;"/>
		<h:inputText id="cardNumberIF" value="#{pc_annuitantInfo.demographics.permanentResCardNum}" styleClass="formEntryText" style="width: 194px;text-align:left;" disabled="#{pc_annuitantInfo.sameAsOwner}" />
	</h:panelGroup>
	<h:panelGroup  id="annuiantvisanumber" styleClass="formDataEntryLine" style="padding-left:5px;">	
		<h:outputText id="VisaNumber" value="#{property.annuityAppVisaNum}" styleClass="formLabel" style="width: 95px;text-align:left;"/>		
		<h:inputText id="VisaNumberIF" value="#{pc_annuitantInfo.demographics.visaNumber}" styleClass="formEntryText" style="width: 160px;" disabled="#{pc_annuitantInfo.sameAsOwner}" />
		<h:outputText id="visaType1" value="#{property.appEntVisaType}" styleClass="formLabel" style="width: 120px;"/>
		<h:selectOneMenu id="visaTypeDD" value ="#{pc_annuitantInfo.demographics.visaType}" styleClass="formEntryText" style="width: 205px;" disabled="#{pc_annuitantInfo.sameAsOwner}" >
			<f:selectItems id="visaTypeList" value="#{pc_annuitantInfo.demographics.visaTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
</jsp:root>