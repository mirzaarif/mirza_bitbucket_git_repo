<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA247 		   8 	  wmA Application Entry Project -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0"> 
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/dialogManagement.js"></script>	
	<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>	
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">

		function setTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage4')
			document.forms['form_appEntryAnnuityPage4'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage4')
			document.forms['form_appEntryAnnuityPage4'].target='';
			return false;
		}		
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage4'].submit();
			return true;
		}
		
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage4']['form_appEntryAnnuityPage4:action'].value= action;
			document.forms['form_appEntryAnnuityPage4']['form_appEntryAnnuityPage4:hiddenSaveButton'].click();
			return true;
		}
	</script>
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryAnnuityPage4')" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_ANNUITY_PAGE4" value="#{pc_nbaAnnuityPage4}" />
		<h:form id="form_appEntryAnnuityPage4" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryAnnuityPage4')">
			<h:inputHidden id="scrollx" value="#{pc_nbaAnnuityPage4.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaAnnuityPage4.scrollYC}"></h:inputHidden>	
			<h:panelGroup id="PGroup1" styleClass="inputForm"  style="padding-left:10px;">
				<h:outputText id="billingTitle" value="#{property.appEntBillingTitle}" styleClass="formSectionBar"/>
				<h:panelGroup id="paymentPGroup" styleClass="formDataEntryLine">
					<h:outputText id="plannedPrem" value="#{property.appEntPlannedPrem}" styleClass="formLabel" />
					<h:inputText id="plannedPremEF" value="#{pc_billingInfo.plannedPremium}" styleClass="formEntryText" style="width: 150px;" 
							disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>
					<h:outputText id="paymentfrequency" value="#{property.appEntFrequency}" styleClass="formLabel"/>
					<h:selectOneMenu id="paymentfrequencyDD" value="#{pc_billingInfo.frequency}" styleClass="formEntryText" style="width: 185px;" 
						disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}" valueChangeListener="#{pc_billingInfo.firstSkipMonthDis}" 
						onchange="submit()">
					<f:selectItems id="paymentFrequencyList" value="#{pc_billingInfo.frequencyList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				<f:subview id="mailCodeView" rendered="#{pc_nbaAnnuityAppEntryNavigation.vantageBESSystem}"> <!-- NBA247 -->
					<c:import url="/nbaAppEntry/subviews/nbaMailCodes.jsp" /> <!-- NBA247 -->
				</f:subview> <!-- NBA247 -->
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAnnuityAppEntryNavigation.processAppEntry}" />
					<h:inputHidden id="action" value="#{pc_nbaAnnuityAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>
		</h:form>	
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>