<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Application Entry Page 3</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/dialogManagement.js"></script>	
	<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>	
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
	<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_appEntryAnnuityPage3'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_appEntryAnnuityPage3'].target='';
			return false;
		}
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage3'].submit();
			return true;
		}
		
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage3']['form_appEntryAnnuityPage3:action'].value= action;
			document.forms['form_appEntryAnnuityPage3']['form_appEntryAnnuityPage3:hiddenSaveButton'].click();
			return true;
		}		
		
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_ANNUITY_PAGE3" value="#{pc_nbaAnnuityPage3}" />
		<h:form id="form_appEntryAnnuityPage3" styleClass="inputFormMat" >
			<h:panelGroup id ="PGroup1" styleClass="inputForm" >
				<f:subview id="formalPurpose">
					<c:import url="/nbaAppEntry/subviews/nbaFunds.jsp" />
				</f:subview>
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAnnuityAppEntryNavigation.processAppEntry}" />
					<h:inputHidden id="action" value="#{pc_nbaAnnuityAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>					
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>