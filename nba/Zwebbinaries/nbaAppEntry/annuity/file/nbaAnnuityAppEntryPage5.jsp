<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- FNB011 		NB-1101	  Work Tracking -->
<!-- FNB002         NB-1101   Additional Fields on Application Entry -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Application Entry Page 5</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/dialogManagement.js"></script>	
	<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>	
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
	<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_appEntryAnnuityPage5'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_appEntryAnnuityPage5'].target='';
			return false;
		}
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage5'].submit();
			return true;
		}
		
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:action'].value= action;
			document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:hiddenSaveButton'].click();
			return true;
		}		
		function toggleCBGroup(selectedCB) {
			if (document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('Yes') != -1) {
					temp = selectedCB.substring(0, selectedCB.indexOf('Yes'));
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'No'].value=false;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'No'].checked=false;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'Yes'].value=true;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'Yes'].checked=true;
				} else {
					temp = selectedCB.substring(0, selectedCB.indexOf('No'));
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'Yes'].value=false;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'Yes'].checked=false;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'No'].value=true;
					document.forms['form_appEntryAnnuityPage5']['form_appEntryAnnuityPage5:' + temp + 'No'].checked=true;
				}
			}
		}
		
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_ANNUITY_PAGE5" value="#{pc_nbaAnnuityPage5}" />
		<h:form id="form_appEntryAnnuityPage5" styleClass="inputFormMat">
			<h:panelGroup id ="PGroup1" styleClass="inputForm" style="padding-left:10px;">
				<f:subview id="signatureInformation">
					<c:import url="/nbaAppEntry/subviews/nbaSignatureInfo.jsp" />
				</f:subview>					
				<f:verbatim>
					<hr id="jointAnnuitantSaperator" class="formSeparator" />
				</f:verbatim>
				<h:panelGroup id="prospectusReceivedPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
					<h:selectBooleanCheckbox id="prospectusReceivedCB" styleClass="ovFullCellSelectCheckBox" style="width: 20px;" 
						value="#{pc_applicationInfo.disclosureInd}"/>
					<h:outputText id="prospectusReceivedLabel" value="#{property.prospectusReceived}" styleClass="formLabel" 
							style="width: 205px;text-align: left;"/>
				</h:panelGroup>			
				<h:panelGroup id="questionPGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
					<h:outputText id="q1" value="#{property.annuityAppEntPage5Q1}" styleClass="formLabel" style="width: 495px;text-align: left;"/>
					<h:selectBooleanCheckbox id="q3CBYes" value="#{pc_annuitantInfo.annuitantInsuranceQuesYes}" styleClass="ovFullCellSelectCheckBox" 
						style="width: 20px;" onclick="toggleCBGroup('q3CBYes');" disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
					<h:outputText id="y1" value="#{property.appEntYes}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
					<h:selectBooleanCheckbox id="q3CBNo" value="#{pc_annuitantInfo.annuitantInsuranceQuesNo}" styleClass="ovFullCellSelectCheckBox" 
						style="width: 20px;" onclick="toggleCBGroup('q3CBNo');"  disabled="#{pc_nbaAnnuityAppEntryNavigation.partialUpdateDisabled}"/>
					<h:outputText id="n1" value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
				</h:panelGroup>
				<f:subview id="producerStmtInformation">
					<c:import url="/nbaAppEntry/subviews/nbaProducerStatement.jsp" />
				</f:subview>
				<f:subview id="producerInformation">
					<c:import url="/nbaAppEntry/subviews/nbaProducerInfo.jsp" />
				</f:subview>
				<!-- begin FNB002  -->
				<f:subview id="additionalInformation">
					<c:import url="/nbaAppEntry/subviews/nbaAdditionalInfo.jsp" />
				</f:subview>
				<!-- end FNB002  -->
				<h:panelGroup >
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAnnuityAppEntryNavigation.processAppEntry}" />
					<h:inputHidden id="action" value="#{pc_nbaAnnuityAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
			</h:panelGroup>					
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>