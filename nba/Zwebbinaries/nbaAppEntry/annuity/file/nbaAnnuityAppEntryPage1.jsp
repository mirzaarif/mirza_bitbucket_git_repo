<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
} 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
	<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
    
	<script language="JavaScript" type="text/javascript">

	    var contextpath = '<%=path%>'; //NBA340	
		function setTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage1');
			document.forms['form_appEntryAnnuityPage1'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage1');
			document.forms['form_appEntryAnnuityPage1'].target='';
			return false;
		}
		function submitContents(){
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage1'].submit();
			return true;
		}		
		function saveForm(action) {
		 if($(this).checkErrorMessage()){  //NBA340
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage1']['form_appEntryAnnuityPage1:action'].value= action;
			document.forms['form_appEntryAnnuityPage1']['form_appEntryAnnuityPage1:hiddenSaveButton'].click();
			return true;
		 }
		 return false; //NBA340
		}

		function toggleCBGroup(selectedCB) {
			if (document.forms['form_appEntryAnnuityPage1']['form_appEntryAnnuityPage1:formalPurpose:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('AppTypeChk') != -1) {
					temp = selectedCB.substring(selectedCB.length - 1,selectedCB.length);
					for(i=1;i<5;i++) {
						if(i != temp) {
							document.forms['form_appEntryAnnuityPage1']['form_appEntryAnnuityPage1:formalPurpose:AppTypeChk' + i].value=false;
							document.forms['form_appEntryAnnuityPage1']['form_appEntryAnnuityPage1:formalPurpose:AppTypeChk' + i].checked=false;
						}
					}
				} 
			}
		}        
		
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();
			$("input[name$='govtIdCorpType']").govtidType();
	        $("input[name$='govtIdInput']").govtidValue();
		})		
		
	</script>
	
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryAnnuityPage1')" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>		
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_ANNUITY_PAGE1" value="#{pc_nbaAnnuityPage1}" />
		<h:form id="form_appEntryAnnuityPage1" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryAnnuityPage1')">
			<h:inputHidden id="scrollx" value="#{pc_nbaAnnuityPage1.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaAnnuityPage1.scrollYC}"></h:inputHidden>	
			<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left: 10px;">
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAnnuityAppEntryNavigation.processAppEntry}" />
					<h:inputHidden id="action" value="#{pc_nbaAnnuityAppEntryNavigation.action}"></h:inputHidden>
				</h:panelGroup>
				<f:subview id="ApplicationInformation">
					<c:import url="/nbaAppEntry/annuity/subviews/nbaAnnuityAppEntryHeader.jsp" />
				</f:subview>
				<f:subview id="ownerInformation">
					<c:import url="/nbaAppEntry/annuity/subviews/nbaAnnuityOwnerInfo.jsp" />
				</f:subview>
				<f:subview id="beneficiaryInformation">
					<c:import url="/nbaAppEntry/annuity/subviews/nbaAnnuityBeneficiaryInfo.jsp" />
				</f:subview>				
			</h:panelGroup>
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
