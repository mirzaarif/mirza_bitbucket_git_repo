<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbaimages.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
	<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
<script language="JavaScript" type="text/javascript">
        var contextpath = '<%=path%>'; //NBA340
		function setTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage2');
			document.forms['form_appEntryAnnuityPage2'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY('form_appEntryAnnuityPage2');
			document.forms['form_appEntryAnnuityPage2'].target='';
			return false;
		}		
		function submitContents() {
			setTargetFrame();
			document.forms['form_appEntryAnnuityPage2'].submit();
			return true;
		}
		
		function saveForm(action) {
			if($(this).checkErrorMessage()){ //NBA340
			   setTargetFrame();
				document.forms['form_appEntryAnnuityPage2']['form_appEntryAnnuityPage2:action'].value= action;
				document.forms['form_appEntryAnnuityPage2']['form_appEntryAnnuityPage2:hiddenSaveButton'].click();
				return true;
			}
			return false; //NBA340
		}           
		
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();	
	        $("input[name$='govtIdInput']").govtidValue();
		})		
	</script> 
	
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryAnnuityPage2')" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_APPENT_ANNUITY_PAGE2" value="#{pc_nbaAnnuityPage2}" />
	<h:form id="form_appEntryAnnuityPage2" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryAnnuityPage2')">
		<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left: 10px;">
			<h:inputHidden id="scrollx" value="#{pc_nbaAnnuityPage2.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaAnnuityPage2.scrollYC}"></h:inputHidden>	
			<h:panelGroup>
				<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAnnuityAppEntryNavigation.processAppEntry}" />
				<h:inputHidden id="action" value="#{pc_nbaAnnuityAppEntryNavigation.action}"></h:inputHidden>
			</h:panelGroup>
			<f:subview id="annuitantInformation">
				<c:import url="/nbaAppEntry/annuity/subviews/nbaAnnuitantInfo.jsp" />
			</f:subview>
			<f:subview id="investmentGoalInformation">
				<c:import url="/nbaAppEntry/annuity/subviews/nbaInvestmentGoalInfo.jsp" />
			</f:subview>
			<f:subview id="jointAnnuitantInformation">
				<c:import url="/nbaAppEntry/annuity/subviews/nbaJointAnnuitantInfo.jsp" />
			</f:subview>			
		</h:panelGroup>					
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
