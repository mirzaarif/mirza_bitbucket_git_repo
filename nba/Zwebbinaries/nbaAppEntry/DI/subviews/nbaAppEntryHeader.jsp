<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB013          NB1101    DI Support in nbA -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="formalAppPGroup" styleClass="formDataEntryLine" >
		<h:outputText id="applicationType" value="#{property.appEntApplicationType}" styleClass="formLabel"  />
		<h:selectOneMenu id="applicationTypeInquiry" value="#{pc_applicationInfo.applicationType}" styleClass="formEntryTextHalf" >
			<f:selectItems id="applicationTypeList" value="#{pc_applicationInfo.applicationTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>	
	<h:panelGroup id="appOriginPGroup" styleClass="formDataEntryLine" >
		<h:outputText id="applicationOrigin" value="#{property.appEntApplicationOrigin}" styleClass="formLabel" style="width: 125px;" />
		<h:selectOneMenu id="applicationOriginSelect" value="#{pc_applicationInfo.applicationOrigin}" styleClass="formEntryTexthalf"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
				valueChangeListener="#{pc_applicationInfo.appOriginSelection}" 
				onchange="resetTargetFrame();submit();"> 
			<f:selectItems id="applicationOriginList" value="#{pc_applicationInfo.applicationOriginList}" />
		</h:selectOneMenu>
		<h:selectBooleanCheckbox id="useInformalContractNo" value="#{pc_applicationInfo.useInformalContractNumber}" styleClass="formEntryCheckbox" style="margin-left:35px;" 
			disabled="#{!pc_applicationInfo.trialOrigin}"> 
		</h:selectBooleanCheckbox>
		<h:outputText id="useInformalContractNoLabel" value="#{property.appEntUseInformalContractNo}" styleClass="formLabelRight" />
	</h:panelGroup>	
	<h:panelGroup id="contractNumberPGroup" styleClass="formDataEntryLine" >
		<h:outputText id="contractNumber" value="#{property.appEntContractNumber}" styleClass="formLabel" />
		<h:outputText id="contractNumberVal" value="#{pc_applicationInfo.contractNumber}" styleClass="formDisplayText" style="width:  177px">
		</h:outputText>
		<h:outputText id="caseId" value="#{property.appEntCaseID}" styleClass="formLabel" style="width: 100px;"> 
		</h:outputText>
		<h:inputText id="caseIdVal" value="#{pc_applicationInfo.caseID}" styleClass="formEntryTextHalf" 
			disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> 
	</h:panelGroup>
	<h:panelGroup id="statutoryCodeGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
		<h:outputText id="statutoryCode" value="#{property.appEntStatutory}" styleClass="formLabel" style="width: 125px;" />
		<h:selectOneMenu id="statutoryCodeSelect" value="#{pc_applicationInfo.statutoryCode}" 
				styleClass="formEntryText"
				style="width: 300px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
				onchange="resetTargetFrame();submit();">
			<f:selectItems id="statutoryCodeList" value="#{pc_applicationInfo.statutoryCodeList}" />
		</h:selectOneMenu>
	</h:panelGroup>	
	<h:panelGroup id="grandfatheredGroup" styleClass="formDataEntryLine" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
		<h:outputText id="grandfatheredCode" value="#{property.appEntGrandFathered}" styleClass="formLabel" style="width: 125px;" />
		<h:selectOneMenu id="grandfatheredSelect" value="#{pc_applicationInfo.grandfatheredTaxType}" 
				styleClass="formEntryText"
				style="width: 300px;"
				disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"
				onchange="resetTargetFrame();submit();">
			<f:selectItems id="grandfatherCodeList" value="#{pc_applicationInfo.grandfatheredTaxTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>	
</jsp:root>