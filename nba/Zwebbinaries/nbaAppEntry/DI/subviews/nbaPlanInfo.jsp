<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB013		    NB-1101	   DI Support for nbA -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		
		<h:outputText id="PlanInfoTitle" value="#{property.appEntPlanTitle}" styleClass="formSectionBar"/>
		<h:panelGroup id="pinsPlanPGroup" styleClass="formDataEntryLine" >
		  <h:outputText id="plan" value="#{property.appEntPlan}"  styleClass="formLabel" style="width: 50px;"/>
			<h:selectOneMenu id="planVal" value="#{pc_planInfo.plan}" style="position: relative;left: 20px;width: 500px;" styleClass="formEntryText" 
						disabled="#{pc_planInfo.riderDisabled  || pc_planInfo.jointInsured ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
						valueChangeListener="#{pc_planInfo.planChange}" onchange="resetTargetFrame();submit();" immediate="true"
						binding="#{pc_planInfo.planInfoCC}"> 
				<f:selectItems id="planValList" value="#{pc_planInfo.planList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		
		
		<h:panelGroup id="pinsPlanAmountGroup" styleClass="formDataEntryLine" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}">
			<h:outputText id="Amount" value="#{property.appEntAmount}"  styleClass="formLabel" style="width: 70px;"/> 
			<h:inputText id="AmountVal" value="#{pc_planInfo.amount}" styleClass="formEntryText" style="width: 120px;" 
				disabled="#{pc_planInfo.benefitOrLifeParticipantSelected  || pc_planInfo.jointInsured || pc_nbaAppEntryNavigation.partialUpdateDisabled}">
				<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
			</h:inputText>
			<h:outputText id="purpose" value="#{property.appEntPurpose}" styleClass="formLabel" style="width: 190px;"/> 
			<h:selectOneMenu id="purposeVal" value="#{pc_planInfo.purpose}" style="width: 190px;" styleClass="formEntryText" 
				disabled="#{!pc_planInfo.baseCoverageSelected}"> 
				<f:selectItems  id="purposeVALList" value="#{pc_planInfo.purposeList}"/>
			</h:selectOneMenu>
		</h:panelGroup>
		
		<h:panelGroup id="pinsPlanOccupationClassGroup" styleClass="formDataEntryLine" rendered="#{pc_planInfo.primaryInsured}">
			<h:outputText id="occupationClass" value="#{property.appEntOccupationClass}" styleClass="formLabel" style="width: 122px;"/>
			<h:selectOneMenu id="occupationClassVal" value="#{pc_planInfo.occupationClass}" styleClass="formEntryText" style="position: relative;left: 35px;width: 412px;"
			    disabled="#{!pc_planInfo.baseCoverageSelected}" > 
				<f:selectItems id="occupationClassValList" value="#{pc_planInfo.occupationClassList}"/> 
			</h:selectOneMenu>
		</h:panelGroup>
		 <h:panelGroup id="benefitPeriodAccGroup" rendered="#{pc_planInfo.primaryInsured}" styleClass="formDataEntryLine">
			<h:outputText id="AccidentBenefitPeriod" value="#{property.appEntBenefitPeriodAcc}" styleClass="formLabel" style="width: 157px;"/>
				<h:selectOneMenu id="benefitPeriodAcc" value ="#{pc_planInfo.benefitPeriodAcc}" styleClass="formEntryText" 
					style="width: 412px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.updateMode}" >
				   <f:selectItems id="benefitPeriodAccList" value="#{pc_planInfo.benefitPeriodAccList}" />
     			</h:selectOneMenu>
		 </h:panelGroup>
		 <h:panelGroup id="waitingPeriodAccGroup" rendered="#{pc_planInfo.primaryInsured}" styleClass="formDataEntryLine">
			<h:outputText id="AccidentWaitingPeriod" value="#{property.appEntWaitingPeriodAcc}" styleClass="formLabel" style="width: 157px;"/>
				<h:selectOneMenu id="waitingPeriodAcc" value ="#{pc_planInfo.waitingPeriodAcc}" styleClass="formEntryText" 
					style="width: 412px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.updateMode}" >
				  <f:selectItems id="waitingPeriodAccList" value="#{pc_planInfo.waitingPeriodAccList}" />
				</h:selectOneMenu>
		  </h:panelGroup>
		  <h:panelGroup id="benefitPeriodSickGroup" rendered="#{pc_planInfo.primaryInsured}" styleClass="formDataEntryLine">
			 <h:outputText id="SicknessBenefitPeriod" value="#{property.appEntBenefitPeriodSick}" styleClass="formLabel" style="width: 157px;"/>
				 <h:selectOneMenu id="benefitPeriodSick" value ="#{pc_planInfo.benefitPeriodSick}" styleClass="formEntryText" 
					style="width: 412px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.updateMode}">
						<f:selectItems id="benefitPeriodSickList" value="#{pc_planInfo.benefitPeriodSickList}" />
				  </h:selectOneMenu>
		  </h:panelGroup>
		  <h:panelGroup id="waitingPeriodSickGroup" rendered="#{pc_planInfo.primaryInsured}" styleClass="formDataEntryLine">
				<h:outputText id="SicknessWaitingPeriod" value="#{property.appEntWaitingPeriodSick}" styleClass="formLabel" style="text-align: right;width: 158px;"/>
						<h:selectOneMenu id="waitingPeriodSick" value ="#{pc_planInfo.waitingPeriodSick}" styleClass="formEntryText" 
							style="width: 411px;" disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || pc_nbaAppEntryNavigation.updateMode}">
							 <f:selectItems id="waitingPeriodSickList" value="#{pc_planInfo.waitingPeriodSickList}" />
						</h:selectOneMenu>
		  </h:panelGroup>
		  	
			<h:panelGroup id="pinsPlanBenefitPGroup" styleClass="formDataEntryLine">
			<h:outputText id="benefit" value="#{property.appEntBenefit}" styleClass="formLabel" style="width: 63px;"/>
			<h:selectOneMenu id="benefitVal" value="#{pc_planInfo.benefit}" style="position: relative;left: 8px;width: 270px;" styleClass="formEntryText" 
				disabled="#{pc_planInfo.benefitsDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> 
				<f:selectItems id="benefitValList" value="#{pc_planInfo.benefitList}"/>
			</h:selectOneMenu>
			
		<h:selectBooleanCheckbox id="jointBenefit" value="#{pc_planInfo.jointBenefit}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;"
			rendered="#{!pc_planInfo.primaryInsured}" disabled="#{!pc_jointOrOtherInsured.jointInsured}" />
		<h:outputText id="usCitizen" value="Joint Benefit" styleClass="formLabel" style="width: 125px;text-align: left;" rendered="#{!pc_planInfo.primaryInsured}"/>
		</h:panelGroup>
		<h:panelGrid id="pinsBenefitPGroup" columnClasses="labelAppEntry,labelAppEntry,colMIBShort,colMIBLong" columns="4" cellpadding="0" cellspacing="0" style="padding-top: 12px;">
			<h:column>
				<h:outputText id="blankAmountUnits" style="width:110px;" value=""></h:outputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="pinsBenefitAmountUnitsRB" value="#{pc_planInfo.benefitUnitsRB}" layout="pageDirection" styleClass="radioLabel" style="width: 75px;" 
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"> 
					<f:selectItem id="Item1" itemValue="1" itemLabel="Amount"/>
					<f:selectItem id="Item2" itemValue="2" itemLabel="Units"/>
				</h:selectOneRadio>				
			</h:column>
			<h:column>
				<h:inputText id="benefitAmountEF" value="#{pc_planInfo.benefitAmount}" styleClass="formEntryText" style="width: 120px;"	
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled  || pc_nbaAppEntryNavigation.vantageBESSystem}"> 
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
				</h:inputText>
				<h:inputText id="benefitUnitsEF" value="#{pc_planInfo.benefitUnits}" styleClass="formEntryText" style="width: 120px;" 
					disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"/>
			</h:column>
			<h:column>
				<h:panelGroup styleClass="formDataEntryLine" >
					<h:outputText id="benefitPercent" value="Percent:" styleClass="formLabel" style="width: 120px;" rendered="#{pc_planInfo.percentRendered}" />
					<h:outputText id="benefitPercentEF" value="#{pc_planInfo.benefitPercent}" styleClass="formDisplayText" rendered="#{pc_planInfo.percentRendered}" >
						<f:convertNumber type="percent"/>
					</h:outputText>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
		<h:panelGroup styleClass="formDivTableHeaderRightButtons">
			<h:panelGrid columns="2" styleClass="formTableHeaderRightButtons" columnClasses="ovColHdrText300,ovColHdrText200" cellspacing="0">
				<h:outputLabel id="planInfoCol1Hdr" value="#{property.appEntPlanTableCol1}" styleClass="ovColSortedFalse" />
				<h:outputLabel id="planInfoCol2Hdr" value="#{property.appEntPlanTableCol2}" styleClass="ovColSortedFalse" />
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGrid id="pinsPlanInfoTablePGrid" columns="2" cellpadding="0" cellspacing="0" width="100%">
			<h:column>
				<h:panelGroup id="pGroup2" styleClass="formDivTableDataRightButtons5" style="height: 125px;">
					<h:dataTable id="primaryInsuredPlanTable" styleClass="formTableDataRightButtons" cellspacing="0"
								binding="#{pc_planInfo.planInfoTable}" value="#{pc_planInfo.rows}" style="min-height: 19px;"
								var="currRow" columnClasses="ovColText300,ovColText200" rowClasses="#{pc_planInfo.rowStyles}">
						<h:column>
							<h:panelGroup>
								<h:commandButton id="planInfoIcon1" image="images/hierarchies/#{currRow.icon1}" rendered="#{currRow.icon1Rendered}"
												action="#{pc_planInfo.selectRow}" 
												styleClass="ovViewIconTrue" style="margin-left: 5px; vertical-align: top" />
								<h:commandLink id="planInfoCol1" action="#{pc_planInfo.selectRow}">
									<h:inputTextarea id="planInfoCol1Text" readonly="true" value="#{currRow.col1}" styleClass="ovMultiLine#{currRow.draftText}"
												style="margin-top: 3px; margin-left: 2px; width: 270px" />
								</h:commandLink>
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:commandLink id="planInfoCol2" action="#{pc_planInfo.selectRow}">
								<h:inputTextarea id="planInfoCol2Text" value="#{currRow.col2}" styleClass="ovMultiLine#{currRow.draftText}" style="width: 200px" />
							</h:commandLink>
						</h:column>					
					</h:dataTable>
				</h:panelGroup>					
			</h:column>
			<h:column>
				<h:panelGrid columns="1">
					<h:commandButton id="pinsPlanInfoAdd" value="#{property.buttonAdd}" styleClass="formButtonInterface"  
										disabled="#{pc_planInfo.addDisabled ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.addRidersOrBenefits}" onclick="resetTargetFrame();"/>
					<h:commandButton id="pinsPlanInfoUpdate" value="#{property.buttonUpdate}" styleClass="formButtonInterface"
										action="#{pc_planInfo.updateRidersOrBenefits}" onclick="resetTargetFrame()" 
										disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> 
					<h:commandButton id="pinsPlanInfoClear" value="#{property.buttonClear}" styleClass="formButtonInterface" 
										disabled="#{pc_planInfo.lifeParticipantSelected ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.resetPlanInfoFields}" onclick="resetTargetFrame()"/>
					<h:commandButton id="pinsPlanInfoDelete" value="#{property.buttonDelete}" styleClass="formButtonInterface" 
										disabled="#{pc_planInfo.disableDelete ||  pc_nbaAppEntryNavigation.partialUpdateDisabled}"
										action="#{pc_planInfo.deleteRidersOrBenefits}" onclick="setTargetFrame();"/> 
				</h:panelGrid>
			</h:column>
		</h:panelGrid>
</jsp:root>