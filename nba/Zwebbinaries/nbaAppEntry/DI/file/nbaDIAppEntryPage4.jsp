<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB013          NB1101    DI Support in nbA -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Application Entry Page 6</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {		
			document.forms['form_appEntryULPage4'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_appEntryULPage4'].target='';
			return false;
		}

		function toggleCBGroup(selectedCB) {
			if (document.forms['form_appEntryULPage4']['form_appEntryULPage4:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('Yes') != -1) {
					temp = selectedCB.substring(0, selectedCB.indexOf('Yes'));
					document.forms['form_appEntryULPage4']['form_appEntryULPage4:' + temp + 'No'].value=false;
					document.forms['form_appEntryULPage4']['form_appEntryULPage4:' + temp + 'No'].checked=false;
				} else {
					temp = selectedCB.substring(0, selectedCB.indexOf('No'));
					document.forms['form_appEntryULPage4']['form_appEntryULPage4:' + temp + 'Yes'].value=false;
					document.forms['form_appEntryULPage4']['form_appEntryULPage4:' + temp + 'Yes'].checked=false;
				}
			}
		}

		function submitContents(){
			setTargetFrame();
			document.forms['form_appEntryULPage4'].submit();
			return true;
		}	
	
		function saveForm(action) {
			setTargetFrame();
			document.forms['form_appEntryULPage4']['form_appEntryULPage4:action'].value= action;
			document.forms['form_appEntryULPage4']['form_appEntryULPage4:hiddenSaveButton'].click();
			return true;
		}

</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE6" value="#{pc_nbaUlPage6}"/>
	<h:form id="form_appEntryULPage4" styleClass="inputFormMat">
		<h:panelGroup id="GeneralInfo" styleClass="inputForm" style="padding-left:6px;">
			<h:outputText value="#{property.appEntGeneralInfo}" styleClass="formSectionBar" style="margin-left: 1px;"/>
			<h:panelGrid id="GeneralInfoQuestions" columns="#{pc_generalInfo.noOfColumns}" cellpadding="1" cellspacing="0" border="1" styleClass="formDataEntryLine">
				<h:outputText value="#{property.appEntQuestion}" styleClass="formLabel" style="width: 300px;font-weight: bold;text-align: center;"></h:outputText>
				<h:outputText value="#{property.appEntPrimaryIns}" styleClass="formLabel" style="width: 110px;font-weight: bold;text-align: center;"></h:outputText>
				<h:outputText value="#{property.appEntOthIns}" rendered="#{pc_generalInfo.othInsRenderer}" styleClass="formLabel" style="width: 110px;font-weight: bold;text-align: center;"></h:outputText>
				<h:outputText value="#{property.appEntJntIns}" rendered="#{pc_generalInfo.jntInsRenderer}" styleClass="formLabel" style="width: 110px;font-weight: bold;text-align: center;"></h:outputText>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:outputText value="#{property.appEntPage6Q1}" styleClass="formLabel" style="width: 370px;text-align: left;padding-bottom:4px;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q1CB1Yes" value="#{pc_generalInfo.q10PriInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q1CB1Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q1CB1No" value="#{pc_generalInfo.q10PriInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q1CB1No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q1CB2Yes" value="#{pc_generalInfo.q10OthInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q1CB2Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q1CB2No" value="#{pc_generalInfo.q10OthInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q1CB2No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:outputText value="#{property.appEntPage6Q2}" styleClass="formLabel" style="width: 370px;text-align: left;padding-bottom:4px;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q2CB1Yes" value="#{pc_generalInfo.q11PriInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q2CB1Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q2CB1No" value="#{pc_generalInfo.q11PriInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q2CB1No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q2CB2Yes" value="#{pc_generalInfo.q11OthInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q2CB2Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q2CB2No" value="#{pc_generalInfo.q11OthInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q2CB2No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="width:370px;" rendered="#{!pc_nbaAppEntryNavigation.updateMode}">
					<f:subview id="impairmentSection">
						<c:import url="/nbaAppEntry/life/subviews/nbaImpairmentInfo.jsp" />
					</f:subview>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;" rendered="#{pc_nbaAppEntryNavigation.updateMode}">
					<h:outputText value="#{property.appEntPage6Q3}" styleClass="formLabel" style="width: 370px;text-align: left;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q3CB1Yes" value="#{pc_generalInfo.q12PriInsYes}"
						styleClass="ovFullCellSelectCheckBox"  style="width: 15px" onclick="toggleCBGroup('q3CB1Yes');resetTargetFrame();submit();"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q3CB1No" value="#{pc_generalInfo.q12PriInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q3CB1No');resetTargetFrame();submit();"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q3CB2Yes" value="#{pc_generalInfo.q12OthInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q3CB2Yes');resetTargetFrame();submit();"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q3CB2No" value="#{pc_generalInfo.q12OthInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q3CB2No');resetTargetFrame();submit();"/> 
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:outputText value="#{property.appEntPage6Q4}" styleClass="formLabel" style="width: 370px;text-align: left;padding-bottom:4px;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q4CB1Yes" value="#{pc_generalInfo.q13PriInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q4CB1Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q4CB1No" value="#{pc_generalInfo.q13PriInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q4CB1No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q4CB2Yes" value="#{pc_generalInfo.q13OthInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q4CB2Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q4CB2No" value="#{pc_generalInfo.q13OthInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q4CB2No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:outputText value="#{property.appEntPage6Q5}" styleClass="formLabel" style="width: 370px;text-align: left;padding-bottom:4px;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q5CB1Yes" value="#{pc_generalInfo.q14PriInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q5CB1Yes')"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q5CB1No" value="#{pc_generalInfo.q14PriInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q5CB1No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="padding-left: 2px;">
					<h:selectBooleanCheckbox id="q5CB2Yes" value="#{pc_generalInfo.q14OthInsYes}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q5CB2Yes');"/>
					<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
					<h:selectBooleanCheckbox id="q5CB2No" value="#{pc_generalInfo.q14OthInsNo}"
						styleClass="ovFullCellSelectCheckBox" style="width: 15px" onclick="toggleCBGroup('q5CB2No');"/>
					<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 35px;text-align: center;"></h:outputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:panelGrid columns="1">
						<h:outputText value="#{property.appEntHeartDisorder}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntHeartDisease}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntAngina}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntStroke}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntHypertension}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntHBP}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntDiabetes}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
						<h:outputText value="#{property.appEntCancer}" styleClass="formLabel" style="width:370px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="text-align: center;">
					<h:panelGrid columns="1">
						<h:selectBooleanCheckbox id="sb1" value="#{pc_generalInfo.heartDisOrderPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb2" value="#{pc_generalInfo.heartDiseaserPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb3" value="#{pc_generalInfo.anginaPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb4" value="#{pc_generalInfo.strokePriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb5" value="#{pc_generalInfo.hypertensionPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb6" value="#{pc_generalInfo.highBPPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb7" value="#{pc_generalInfo.diabetesPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb8" value="#{pc_generalInfo.cancerPriIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup rendered="#{pc_generalInfo.othInsRenderer || pc_generalInfo.jntInsRenderer}" styleClass="formDataEntryLine" style="text-align: center;">
					<h:panelGrid columns="1">
						<h:selectBooleanCheckbox id="sb9" value="#{pc_generalInfo.heartDisOrderOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb10" value="#{pc_generalInfo.heartDiseaserOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb11" value="#{pc_generalInfo.anginaOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb12" value="#{pc_generalInfo.strokeOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb13" value="#{pc_generalInfo.hypertensionOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb14" value="#{pc_generalInfo.highBPOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb15" value="#{pc_generalInfo.diabetesOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
						<h:selectBooleanCheckbox id="sb16" value="#{pc_generalInfo.cancerOthIns}"
							styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					</h:panelGrid>
				</h:panelGroup>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup>
			<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" />
			<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
		</h:panelGroup>

	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
