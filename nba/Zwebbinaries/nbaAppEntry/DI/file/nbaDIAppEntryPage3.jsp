<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB013          NB1101    DI Support in nbA -->
<!-- NBA317          NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0"> 
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!-- NBA317 -->
	<script type="text/javascript" src="include/creditCard.js"></script><!-- NBA317 -->
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
    <script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
	<script language="JavaScript" type="text/javascript">
		//var fileLocationHRef  = '<%=basePath%>' + 'nbaAppEntry/file/nbaDIAppEntryPage3.faces';
		//parent.fileLocationHRef = '<%=basePath%>' + 'nbaAppEntry/file/nbaDIAppEntryPage3.faces';
		//begin NBA317
		var contextpath = '<%=path%>'; //NBA340
		var formID = 'form_appEntryULPage3';
		var ccInfo = 'creditCardInfo';
		var billingInfo = 'billingInfo';
		var errorMessageDiv = 'Messages';
		var ccNumberTokenized = 'cardNumberTokenized';
		var cardNumberEFHidden = 'cardNumberEFHidden';
		var cardNumberVEFHidden = 'cardNumberVEFHidden';
		var actionMenuCardNumber1Hidden = 'actionMenuForm:cardNumber1Hidden';
		var actionMenuVerifyCardNumber1Hidden = 'actionMenuForm:verifyCard1NumberHidden';
		var actionMenuCardNumber2Hidden = 'actionMenuForm:cardNumber2Hidden';
		//end NBA317
		function setTargetFrame() {
			getScrollXY(formID)
			document.forms[formID].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			getScrollXY(formID)
			document.forms[formID].target='';
			return false;
		}		
		function submitContents() {
			setTargetFrame();
			//NBA317 code deleted
			handleSubmit();//NBA317 this method gets called on tab change event, so need to promote the hidden fields
			return true;
		}
		
		function saveForm(action) {
			if($(this).checkErrorMessage()){ //NBA340
				setTargetFrame();
				document.forms[formID]['form_appEntryULPage3:action'].value= action;
				document.forms[formID]['form_appEntryULPage3:hiddenSaveButton'].click();
				return true;
			}
			return false; //NBA340
		}			
		
		//begin NBA317
		function formatCreditCardNumber(subviewID,field,hiddenField) {
			return formatCardNumber(formID,subviewID,field,hiddenField);
		}
		
		function processCreditCardInformation(subviewID,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID){
			var skipCreditCardValidation = top.ccTokenizationFrame.isSkipCCValidation();
			var tokenizationServiceURL = top.ccTokenizationFrame.getTokenizationURL();
			var tokenizationServiceRequest = top.ccTokenizationFrame.getTokenizationRequest();
			processCreditCardTokenization(formID,subviewID,skipCreditCardValidation,tokenizationServiceURL,tokenizationServiceRequest,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv);
		}
		
		function authorizeCommit(formID,subviewID,tokenized){
		 	var ccInfoTokenized = true;
		 	var billingInfoTokenized = true;
		 	if(ccInfo == subviewID){
		 		ccInfoTokenized = tokenized;
		 		setValue(formID,subviewID,ccNumberTokenized,tokenized);
		 		billingInfoTokenized = getValue(formID,billingInfo,ccNumberTokenized);
		 	}else if(billingInfo == subviewID){
		 		billingInfoTokenized = tokenized;
		 		setValue(formID,subviewID,ccNumberTokenized,tokenized);
		 		ccInfoTokenized = getValue(formID,ccInfo,ccNumberTokenized);
		 	}
		 	parent.authorizeSaveSubmit(ccInfoTokenized && billingInfoTokenized);
		}
		
		//Removes the credit card numbers before submitting the page if the credit card tokenization has not taken place
		function handleSubmit(){
		 	var ccInfoTokenized = getValue(formID,ccInfo,ccNumberTokenized);
		 	var billingInfoTokenized = getValue(formID,billingInfo,ccNumberTokenized);
			if(!ccInfoTokenized){ //if credit card number in Credit Card Payment section is not tokenized
				var cardNumberEFHiddenValue = getValue(formID,ccInfo,cardNumberEFHidden);
				var cardNumberVEFHiddenValue = getValue(formID,ccInfo,cardNumberVEFHidden);
				//set the card numbers in the nbFile.jsp's hidden card number attributes
				top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber1Hidden,cardNumberEFHiddenValue);
				top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuVerifyCardNumber1Hidden,cardNumberVEFHiddenValue);
				//delete the card numbers from the hidden fields on the jsp
				setValue(formID,ccInfo,cardNumberEFHidden,'');
				setValue(formID,ccInfo,cardNumberVEFHidden,'');				
			}
			if(!billingInfoTokenized){//if credit card number in Billing Information section is not tokenized
				var cardNumberEFHiddenValue = getValue(formID,billingInfo,cardNumberEFHidden);
				//set the card numbers in the nbFile.jsp's hidden card number attributes
				top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber2Hidden,cardNumberEFHiddenValue);
				//delete the card numbers from the hidden fields on the jsp
				setValue(formID,billingInfo,cardNumberEFHidden,'');
			}
			//Submit the page
			document.forms[formID].submit();
		}
		
		//If nbFile.jsp contains values in hidden fields and credit card tokenization has not yet taken place, reload the values in the jsp
		function loadHiddenCardNumberFieldValues(){
			if(parent.authorizeSaveSubmit){//if the page load is due to a tab-in event on page3 execute this method
			 	var ccInfoTokenized = getValue(formID,ccInfo,ccNumberTokenized);
			 	var billingInfoTokenized = getValue(formID,billingInfo,ccNumberTokenized);
				if(!ccInfoTokenized){ //if credit card number in Credit Card Payment section is not tokenized
					var cardNumberEFHiddenValue = top.mainContentFrame.contentRightFrame.getHiddenFieldValue(actionMenuCardNumber1Hidden);
					var cardNumberVEFHiddenValue = top.mainContentFrame.contentRightFrame.getHiddenFieldValue(actionMenuVerifyCardNumber1Hidden);
					//Reload the values for the card number fields from nbFile.jsp
					setValue(formID,ccInfo,cardNumberEFHidden,cardNumberEFHiddenValue);
					setValue(formID,ccInfo,cardNumberVEFHidden,cardNumberVEFHiddenValue);
					//Delete the values from nbFile.jsp once they have been reloaded in the life app entry jsps
					top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber1Hidden,'');
					top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuVerifyCardNumber1Hidden,'');
				}
				if(!billingInfoTokenized){//if credit card number in Billing Information section is not tokenized
					var cardNumberEFHiddenValue = top.mainContentFrame.contentRightFrame.getHiddenFieldValue(actionMenuCardNumber2Hidden);
					//Reload the values for the card number field from nbFile.jsp
					setValue(formID,billingInfo,cardNumberEFHidden,cardNumberEFHiddenValue);
					//Delete the values from nbFile.jsp once they have been reloaded in the life app entry jsps
					top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber2Hidden,'');
				}
				parent.authorizeSaveSubmit(ccInfoTokenized && billingInfoTokenized);
			}
		}
		//end NBA317 
		
		//NBA317 code deleted

		function toggleCBGroup(selectedCB) {
			if (document.forms[formID]['form_appEntryULPage3:genInfo:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('Yes') != -1) {
					temp = selectedCB.substring(0, selectedCB.indexOf('Yes'));
					document.forms[formID]['form_appEntryULPage3:genInfo:' + temp + 'No'].value=false;
					document.forms[formID]['form_appEntryULPage3:genInfo:' + temp + 'No'].checked=false;
				} else {
					temp = selectedCB.substring(0, selectedCB.indexOf('No'));
					document.forms[formID]['form_appEntryULPage3:genInfo:' + temp + 'Yes'].value=false;
					document.forms[formID]['form_appEntryULPage3:genInfo:' + temp + 'Yes'].checked=false;
				}
			}
		}
		//NBA340 new Function
		$(document).ready(function() {
		    $("input[name$='govtIdType']").govtidType();			
	        $("input[name$='govtIdInput']").govtidValue();
		})		
	</script>
	
</head>
<body onload="filePageInit();scrollToCoordinates('form_appEntryULPage3');loadHiddenCardNumberFieldValues();" style="overflow-x: hidden; overflow-y: scroll"><!-- NBA317 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_APPENT_LIFE_PAGE5" value="#{pc_nbaUlPage5}" />
		<h:form id="form_appEntryULPage3" styleClass="inputFormMat" onsubmit="getScrollXY('form_appEntryULPage3')">
			<h:inputHidden id="scrollx" value="#{pc_nbaUlPage5.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_nbaUlPage5.scrollYC}"></h:inputHidden>	
			<h:panelGroup id="PGroup1" styleClass="inputForm" style="padding-left:10px;">
				<f:subview id="creditCardInfo" rendered="#{!pc_nbaAppEntryNavigation.vantageBESSystem}">
					<c:import url="/nbaAppEntry/life/subviews/nbaCreditCardInfo.jsp" />
				</f:subview>
				<f:subview id="billingInfo">
					<c:import url="/nbaAppEntry/life/subviews/nbaBillingInfo.jsp" />
				</f:subview>
				<f:subview id="mailCodeView" rendered="#{pc_nbaAppEntryNavigation.vantageBESSystem}">
					<c:import url="/nbaAppEntry/subviews/nbaMailCodes.jsp" />
				</f:subview>
				<f:subview id="payorInfo">
					<c:import url="/nbaAppEntry/life/subviews/nbaPayorInfo.jsp" />
				</f:subview>
				<f:subview id="genInfo">
					<c:import url="/nbaAppEntry/life/subviews/nbaGeneralInfo.jsp" />
				</f:subview>
				
				<h:panelGroup>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit" action="#{pc_nbaAppEntryNavigation.processAppEntry}" />
					<h:inputHidden id="action" value="#{pc_nbaAppEntryNavigation.action}"></h:inputHidden>
					<!-- begin NBA317 -->
					<h:inputHidden id="guid" value="#{pc_nbaAppEntryNavigation.guid}"></h:inputHidden>
					<h:inputHidden id="txnDate" value="#{pc_nbaAppEntryNavigation.txnDate}"></h:inputHidden>
					<h:inputHidden id="txnTime" value="#{pc_nbaAppEntryNavigation.txnTime}"></h:inputHidden>
					<!-- end NBA317 -->
				</h:panelGroup>
			</h:panelGroup>
		</h:form>	
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>