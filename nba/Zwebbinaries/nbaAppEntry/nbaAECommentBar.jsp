<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPRNBA-947 	NB-1601 Null Pointer Exception May Occur in Inbox if User Selects Different Work -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=	basePath%>" target="controlFrame">
<title>nbA</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<link rel="stylesheet" type="text/css" href="theme/accelerator.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script>
//<!--
		var contextpath = '<%=path%>';	//FNB011
		function resetTargetFrame() {
				document.forms['form_appEntryCommentBar'].target='';
				if (top.mainContentFrame.contentRightFrame.commentBarRefresh != null) {  //NBA213
					top.mainContentFrame.contentRightFrame.commentBarRefresh = true;  //NBA213
				}
				return false;
			}
	
			function setTargetFrame() {
				document.forms['form_appEntryCommentBar'].target='controlFrame';
				return false;
			}

			function refreshBar() {
				if (top.mainContentFrame.contentRightFrame.commentBarRefresh != null && top.mainContentFrame.contentRightFrame.commentBarRefresh == true) {  //NBA213
					top.mainContentFrame.contentRightFrame.refreshCommentBar();  //NBA213
				}
			}
			
			function commitComments() {
				document.forms['form_appEntryCommentBar']['form_appEntryCommentBar:refresh'].value = 'commit';
				return resetTargetFrame();
			}
//-->
</script>
</head>
<body onload="refreshBar();" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"> <!-- SPRNBA-947 remove filePageInit -->      
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="form_appEntryCommentBar">
		<f:subview id="commentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
	</h:form>
</f:view>
</body>
</html>
