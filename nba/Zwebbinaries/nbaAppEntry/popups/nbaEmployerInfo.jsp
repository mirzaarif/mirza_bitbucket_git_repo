<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- SPR3295           7      Application Entry Reusability -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.appEntEmployerTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
				var width=550;
				var height=270;  //SPR3295
				function setTargetFrame() {
					document.forms['form_UlEmployerInfo'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_UlEmployerInfo'].target='';
					return false;
				}
			</script>
		</head>
		<body onload="popupInit();">
		<h:form id="form_UlEmployerInfo" >
			<h:panelGroup styleClass="entryLinePadded" style="padding-top:10px;">  <!-- SPR3295 -->
				<h:outputText id="address" value="#{property.appEntAdd}" styleClass="entryLabel" />  <!-- SPR3295 -->
				<h:inputText id="addressLine1" value="#{pc_employer.address.addressLine1}" styleClass="entryFieldLong" />  <!-- SPR3295 -->
			</h:panelGroup>
			<h:panelGroup styleClass="entryLinePaddedNoLabel">  <!-- SPR3295 -->
				<h:inputText id="addressLine2" value="#{pc_employer.address.addressLine2}" styleClass="entryFieldLong" />  <!-- SPR3295 -->
			</h:panelGroup>
			<h:panelGroup styleClass="entryLinePaddedNoLabel">  <!-- SPR3295 -->
				<h:inputText id="addressLine3" value="#{pc_employer.address.addressLine3}" styleClass="entryFieldLong" />  <!-- SPR3295 -->
			</h:panelGroup>
			<h:panelGroup styleClass="entryLinePadded">
				<h:outputText id="city1" value="#{property.appEntCity}" styleClass="entryLabel" />  <!-- SPR3295 -->
				<h:inputText id="city1EF" value="#{pc_employer.address.city}" styleClass="entryFieldLong" />  <!-- SPR3295 -->
			</h:panelGroup>  <!-- SPR3295 -->
			<h:panelGroup styleClass="entryLinePadded">  <!-- SPR3295 -->
				<h:outputText id="state1" value="#{property.appEntState}" styleClass="entryLabel" />  <!-- SPR3295 -->
				<h:selectOneMenu id="state1DD" value="#{pc_employer.address.state}" styleClass="entryFieldLong">  <!-- SPR3295 -->
					<f:selectItems value="#{pc_employer.address.stateList}"/>
				</h:selectOneMenu>				
			</h:panelGroup>
			<h:panelGrid columns="3" styleClass="tableLinePadded" columnClasses="tableColumnLabel,tableColumn100,tableColumn150" cellspacing="0" cellpadding="0">
				<h:column>
					<h:outputLabel id="code" value="#{property.appEntCode}" styleClass="entryLabelTop" />
				</h:column>
				<h:column>
					<h:selectOneRadio id="zipCodeRB" value="#{pc_employer.address.zipTC}" layout="pageDirection" styleClass="radioMenu">
						<f:selectItems value="#{pc_employer.address.zipTypeCodes}"/>
					</h:selectOneRadio>
				</h:column>
				<h:column>
					<h:panelGroup>
						<h:inputText id="zipCode" value="#{pc_employer.address.zip}" styleClass="entryFieldShort" > <!-- SPRNBA-493 -->
							<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> <!-- SPRNBA-493 -->
						</h:inputText> <!-- SPRNBA-493 -->
						<h:inputText id="postalCode" value="#{pc_employer.address.postal}" styleClass="entryFieldShortRadioPage" />
					</h:panelGroup>
				</h:column>
			</h:panelGrid>			

			<h:panelGroup styleClass="buttonBar">
				<h:commandButton id="employerCancel" value="#{property.buttonCancel}" immediate="true" action="#{pc_employer.cancel}" styleClass="buttonLeft" onclick="setTargetFrame()"/> <!-- SPRNBA-493 -->
				<h:commandButton id="employerClear" value="#{property.buttonClear}" action="#{pc_employer.clear}" styleClass="buttonLeft-1" onclick="resetTargetFrame()"/>
				<h:commandButton id="employerAdd" value="#{property.buttonAdd}" action="#{pc_employer.add}" styleClass="buttonRight" onclick="setTargetFrame()"/>
			</h:panelGroup>
									
		</h:form>
	<!-- begin SPRNBA-493 -->	
	<div id="Messages" style="display:none"> 
		<h:messages /> 
	</div> 
	<!-- end SPRNBA-493 -->
	</body>
	</f:view>
</html>