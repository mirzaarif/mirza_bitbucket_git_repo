<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPRNBA-365        NB-1101   Insured's Name Field Populated from Impairment Search Should Be Expanded -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText id="titleBar" value="#{property.ImpairmentSearchTitleBar}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	<script language="JavaScript" type="text/javascript">
			<!--
				var width=620;
				var height=400; 
				function setTargetFrame() {
					document.forms['form_impairment'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_impairment'].target='';
					return false;
				}
			//-->
			</script>
	</head>
	<body onload="popupInit();">
	<h:form id="form_impairment">
		<h:panelGroup styleClass="formDataEntryTopLine">
			<h:outputLabel value="#{property.appEntImpSearchInsured}" styleClass="formLabel" />
			<h:selectOneMenu styleClass="formEntryText" value="#{pc_ImpairmentSearch.roleCode}" binding="#{pc_ImpairmentSearch.roleCodeUIComp}" style="width: 245px"> <!-- SPRNBA-365 -->
				<f:selectItems value="#{pc_nbaUlPage6.parties}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup styleClass="formDataEntryLineBottom">
			<h:outputLabel value="#{property.impSearchCriteria}" styleClass="formLabel" />
			<h:inputText value="#{pc_ImpairmentSearch.searchText}"  binding="#{pc_ImpairmentSearch.searchTextUIComp}" styleClass="formEntryText" style="width: 270px" />
			<h:selectOneMenu styleClass="formEntryText" value="#{pc_ImpairmentSearch.searchType}" binding="#{pc_ImpairmentSearch.searchTypeUIComp}">
				<f:selectItems value="#{pc_ImpairmentSearch.searchTypeList}" />
			</h:selectOneMenu>
			<h:commandButton value="#{property.buttonSearch}" styleClass="formButtonRight" action="#{pc_ImpairmentSearch.search}"
				disabled="#{pc_ImpairmentSearch.searchDisabled}" onclick="resetTargetFrame();" />
		</h:panelGroup>

		<h:panelGroup styleClass="formDivTableHeader">
			<h:panelGrid columns="2" styleClass="formTableHeader" columnClasses="ovColHdrText155,ovColHdrText155" cellspacing="0">
				<h:commandLink id="impSearchColHdr1" value="#{property.impSearchCol1}" 
					actionListener="#{pc_ImpairmentSearchTable.sortColumn}" immediate="true" styleClass="ovColSorted#{pc_ImpairmentSearchTable.sortedByCol1}" />
				<h:commandLink id="impSearchColHdr2" value="#{property.impSearchCol2}" 
					actionListener="#{pc_ImpairmentSearchTable.sortColumn}" immediate="true"styleClass="ovColSorted#{pc_ImpairmentSearchTable.sortedByCol2}" />
			</h:panelGrid>
		</h:panelGroup>

		<h:panelGroup styleClass="formDivTableData3" style="height :200px">
			<h:dataTable id="searchResultsTable" binding="#{pc_ImpairmentSearchTable.dataTable}" value="#{pc_ImpairmentSearchTable.impairmentsList}" var="rslt"
				rows="0" styleClass="formTableData" columnClasses="ovColText155,ovColText155" rowClasses="#{pc_ImpairmentSearchTable.rowStyles}" cellspacing="0">
				<h:column>
					<h:commandLink value="#{rslt.col1}" styleClass="ovFullCellSelect" action="#{pc_ImpairmentSearchTable.selectImpairmentRow}" immediate="true" />
				</h:column>
				<h:column>
					<h:commandLink value="#{rslt.col2}" styleClass="ovFullCellSelect" action="#{pc_ImpairmentSearchTable.selectImpairmentRow}" immediate="true" />
				</h:column>
			</h:dataTable>
		</h:panelGroup>

		<!-- Table Columns -->
		<h:panelGroup styleClass="ovStatusBar">
			<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_ImpairmentSearchTable.showPrevious}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.previousPageAbsolute}" immediate="true" />
			<h:commandLink value="#{property.previousPage}" rendered="#{pc_ImpairmentSearchTable.showPrevious}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.previousPage}" immediate="true" />
			<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_ImpairmentSearchTable.showPreviousSet}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.previousPageSet}" immediate="true" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page1Number}" rendered="#{pc_ImpairmentSearchTable.showPage1}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page1}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page1Number}" rendered="#{pc_ImpairmentSearchTable.currentPage1}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page2Number}" rendered="#{pc_ImpairmentSearchTable.showPage2}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page2}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page2Number}" rendered="#{pc_ImpairmentSearchTable.currentPage2}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page3Number}" rendered="#{pc_ImpairmentSearchTable.showPage3}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page3}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page3Number}" rendered="#{pc_ImpairmentSearchTable.currentPage3}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page4Number}" rendered="#{pc_ImpairmentSearchTable.showPage4}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page4}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page4Number}" rendered="#{pc_ImpairmentSearchTable.currentPage4}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page5Number}" rendered="#{pc_ImpairmentSearchTable.showPage5}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page5}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page5Number}" rendered="#{pc_ImpairmentSearchTable.currentPage5}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page6Number}" rendered="#{pc_ImpairmentSearchTable.showPage6}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page6}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page6Number}" rendered="#{pc_ImpairmentSearchTable.currentPage6}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page7Number}" rendered="#{pc_ImpairmentSearchTable.showPage7}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page7}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page7Number}" rendered="#{pc_ImpairmentSearchTable.currentPage7}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_ImpairmentSearchTable.page8Number}" rendered="#{pc_ImpairmentSearchTable.showPage8}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.page8}" immediate="true" />
			<h:outputText value="#{pc_ImpairmentSearchTable.page8Number}" rendered="#{pc_ImpairmentSearchTable.currentPage8}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_ImpairmentSearchTable.showNextSet}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.nextPageSet}" immediate="true" />
			<h:commandLink value="#{property.nextPage}" rendered="#{pc_ImpairmentSearchTable.showNext}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.nextPage}" immediate="true" />
			<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_ImpairmentSearchTable.showNext}" styleClass="ovStatusBarText"
				action="#{pc_ImpairmentSearchTable.nextPageAbsolute}" immediate="true" />
		</h:panelGroup>
		<h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="imprmntSrchCancel" value="#{property.buttonCancel}" immediate="true" action="#{pc_ImpairmentSearch.cancel}"
				onclick="setTargetFrame();" styleClass="formButtonLeft" />
			<h:commandButton id="imprmntSrchClear" value="#{property.buttonClear}" action="#{pc_ImpairmentSearch.clear}" onclick="resetTargetFrame();"
				styleClass="formButtonLeft-1" />
			<h:commandButton id="imprmntSrchAdd" value="#{property.buttonAdd}" disabled="#{pc_ImpairmentSearchTable.selectedImpairment==null}" action="#{pc_ImpairmentSearch.add}" onclick="setTargetFrame();"
				styleClass="formButtonRight" />
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
	</body>
</f:view>
</html>
