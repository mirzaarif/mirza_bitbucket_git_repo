<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB002			NB-1101	  Additional Fields on Application Entry -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:outputText id="relatedBusinessInfo" value="#{property.appEntAdditionalInfo}" styleClass="formSectionBar"/>
		
		<h:panelGroup id="casePG" styleClass="formDataEntryLine" style="position: relative;left: 33px;width: 550px;">
			<h:selectBooleanCheckbox id="attentionCB" value="#{pc_SpecialProcessingInfo.advancedCase}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" />
			<h:outputText id="attention" value="#{property.appEntAttentionInd}" styleClass="formLabel" style="width: 360px;text-align: left;"/>
			
			<h:selectBooleanCheckbox id="rushIndCB" value="#{pc_SpecialProcessingInfo.rushInd}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" />
			<h:outputText id="rushInd" value="#{property.appEntrushInd}" styleClass="formLabel" style="width: 130px;text-align: left;"/>
			
			<h:selectBooleanCheckbox id="eliteCaseCB" value="#{pc_SpecialProcessingInfo.eliteCaseInd}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" />
			<h:outputText id="eliteCaseInd" value="#{property.appEntEliteCaseInd}" styleClass="formLabel" style="width: 360px;text-align: left;"/>
			
			<h:selectBooleanCheckbox id="confidentialIndCB" value="#{pc_SpecialProcessingInfo.confidentialInd}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" />
			<h:outputText id="confidentialInd" value="#{property.appEntConfidentialInd}" styleClass="formLabel" style="width: 135px;text-align: left;"/>
		</h:panelGroup>
		
		<h:panelGroup id="case1PG" styleClass="formDataEntryLine" style="position: relative;left: 8px;width: 550px;">
			<h:outputText id="receiptDate" value="#{property.appEntHomeOfficeReceiptDate}" styleClass="formLabel" style="width: 195px;"/>
			<h:inputText id="receiptDateEF" value="#{pc_SpecialProcessingInfo.submissionDate}" styleClass="formEntryText" style="width: 105px;" > 
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>

			<h:selectBooleanCheckbox id="splitDollarIndCB" value="#{pc_SpecialProcessingInfo.splitDollarInd}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 110px;width: 25px;" />
			<h:outputText id="splitDollarInd" value="#{property.appEntSplitDollarInd}" styleClass="formLabel" style="width: 185px;"/>
		</h:panelGroup>

		<h:panelGroup id="case2PG" styleClass="formDataEntryLine" style="position: relative;left: 33px;width: 550px;">
			<h:selectBooleanCheckbox id="bypassReqOrderIndCB" value="#{pc_SpecialProcessingInfo.bypassReqOrderInd}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" />
			<h:outputText id="bypassReqOrderInd" value="#{property.appEntBypassReqOrderInd}" styleClass="formLabel" style="width: 360px;text-align: left;"/>
			
			<h:selectBooleanCheckbox id="bypassUnderwritingIndCB" value="#{pc_SpecialProcessingInfo.bypassUnderwritingInd}" styleClass="ovFullCellSelectCheckBox" style="width: 25px;" rendered="#{!pc_SpecialProcessingInfo.annuity}" />
			<h:outputText id="bypassUnderwritingInd" value="#{property.appEntbypassUnderwritingInd}" styleClass="formLabel" style="width: 135px;text-align: left;" rendered="#{!pc_SpecialProcessingInfo.annuity}"/>
		</h:panelGroup>
		
</jsp:root>