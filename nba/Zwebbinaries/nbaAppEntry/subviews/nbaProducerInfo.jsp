<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA247		   	   8 	  wmA Application Entry Project -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<f:loadBundle basename="properties.nbaApplicationData" var="properties" />
		<h:outputText id="producerTitle" value="#{property.appEntProducerTitle}" styleClass="formSectionBar"/>
		<h:panelGrid id="pinsProducerPGrid" columns="2" styleClass="formDataEntryLine" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0" style="width: 575px;">
			<h:column>
				<h:dataTable id="producers" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_producerInfo.producerList}" var="producer"
						style="width: 510px;margin-top: -4px;">
					<h:column id="prodCol1">
						<h:selectBooleanCheckbox id="deleteProducer" value="#{producer.deleteProducer}" styleClass="ovFullCellSelectCheckBox" style="position: relative;left: 450px;width: 25px;" disabled="#{producer.deleteProducerDisabled}" binding="#{pc_producerInfo.delProducer}"/><!-- NBA175 -->
						<h:outputText id="delProducer" value="#{property.appEntDelProd}" styleClass="formLabel" style="position: relative;left: 355px;width: 200px;" /><!-- NBA175 -->
						<!-- begin FNB002 -->
						<h:panelGroup id="ProducerInformationTitle" styleClass="formDataEntryLine">
							<h:outputText id="producerNameLbl" value="#{property.appEntName}:" styleClass="formLabel" style="width: 95px;"/>
							<h:outputText id="producerName" value="#{producer.name}" styleClass="formDisplayText" style="text-transform: capitalize" />
						</h:panelGroup>
						<!-- end FNB002 -->
						<h:panelGroup id="identificationPGroup1" styleClass="formDataEntryLine" style="padding-left:5px;"><!-- NBA176 -->
							<h:outputText id="identification" value="#{property.appEntIdentification}" styleClass="formLabel" style="width: 80px;text-align:left;"/><!-- NBA176 -->
							<h:inputText id="identificationEF" value="#{producer.producerID}" styleClass="formEntryText" style="width: 50px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"/> <!--  NBA211 -->
							<h:outputText id="commissionShare" value="#{property.appEntCommissionShare}" styleClass="formLabel" style="width: 135px; text-align:right;"/><!-- NBA176 -->
							<h:inputText id="commissionShareEF" value="#{producer.interestPercent}" styleClass="formEntryText" style="width: 80px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled}"> <!-- NBA211 -->
								<f:convertNumber type="percent"/>
							</h:inputText>
							<!-- begin NBA247 -->
							<h:outputText id="profile" value="#{property.appEntProfile}" styleClass="formLabel" style="width: 60px; text-align:right;"/>
							<h:inputText id="profileEF" value="#{producer.profileCode}" styleClass="formEntryText" style="width: 50px;"
								disabled="#{pc_nbaAppEntryNavigation.partialUpdateDisabled || !pc_producerInfo.agentSystemPERF}"> 
							</h:inputText>
							<!-- end NBA247 -->
						</h:panelGroup>						
					</h:column>
				</h:dataTable>
			</h:column>
			<h:column>
				<h:commandLink id="addAnotherProducer" value="#{property.buttonAddAnother}" styleClass="formButtonInterface" action="#{pc_producerInfo.addAnotherProducer}" style="margin-bottom: 2px; width: 65px;"/><!-- NBA176 -->
			</h:column>
		</h:panelGrid>
</jsp:root>