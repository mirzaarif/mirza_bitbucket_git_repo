<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3295           7      Application Entry Reusability -->
<!-- NBA211            7      Partial Application -->
<!-- NBA175            7      Traditional Life Application Entry Rewrite -->
<!-- NBA330         NB-1401   Product Versioning -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="fundInfo" value="#{property.appEntFundInfo}" styleClass="formSectionBar"/>
	<h:panelGrid columns="3" style="padding-top: 5px;" columnClasses="colOwnerInfo,colMoreCodes" cellspacing="0" cellpadding="0">
		<h:column>
			<h:dataTable id="fundDataTable" styleClass="formTableData" style="width: 535px;" cellspacing="0" cellpadding="0" rows="0" value="#{pc_fundInfo.totalFunds}" var="fundRow" >
				<h:column>
					<h:panelGroup styleClass="formDataDisplayLine">
						<h:outputText id="fund2" value="#{property.appEntFund}" styleClass="formLabel" style="width: 50px;"/>
						<h:selectOneMenu value="#{fundRow.productCode}" styleClass="formEntryTextFull" style="width:260px;" onchange="resetTargetFrame();submit()" disabled="#{pc_fundInfo.partialUpdateDisabled || pc_nbaAppEntryNavigation.traditional}"> <!-- NBA211 --><!-- NBA175 -->
							<f:selectItems id="fundList" value="#{pc_fundInfo.allowableFunds}" />  <!-- NBA330 -->
						</h:selectOneMenu>
						<h:outputText id="fundPercent2" value="#{property.appEntFundPercent}" styleClass="formLabel" style="width: 120px;"/>
						<h:inputText id="fundPercent2EF" value="#{fundRow.allocPercent}" styleClass="formEntryText" style="width: 90px;" disabled="#{pc_fundInfo.partialUpdateDisabled || pc_nbaAppEntryNavigation.traditional}"> <!-- NBA211 --><!-- NBA175 -->
							<f:convertNumber type="percent"/>
						</h:inputText>
					</h:panelGroup>
				</h:column>
			</h:dataTable>
		</h:column>
		<h:column>
			<h:commandLink value="#{property.buttonAddAnother}" action="#{pc_fundInfo.addAnotherFund}" styleClass="formButtonInterface" style="width: 65px;margin-left: 5px;" rendered="#{!pc_nbaAppEntryNavigation.traditional}"/> <!--TODO disable NBA211 --><!-- NBA175 -->
			<h:outputText id="addanother" value="#{property.buttonAddAnother}" styleClass="formButtonInterface" style="width: 65px;margin-left: 5px;" rendered="#{pc_nbaAppEntryNavigation.traditional}"/> <!-- NBA175 -->
		</h:column>
	</h:panelGrid>
</jsp:root>
