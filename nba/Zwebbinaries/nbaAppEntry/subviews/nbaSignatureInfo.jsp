<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- NBA211            7      Partial Application -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id ="PGroup1">
		<h:outputText id="sigInfo" value="#{property.appEntSignatureInfo}" styleClass="formSectionBar"/>
		<h:panelGroup id="signedStatePGroup" styleClass="formDataEntryLine">
			<h:outputText id="signDate" value="#{property.appEntAppSignedDate}" styleClass="formLabel" style="width: 165px;"/>
			<h:inputText id="signDateEF" value="#{pc_applicationInfo.signedDate}" styleClass="formEntryDate" style="width: 100px;"
				disabled="#{pc_applicationInfo.partialUpdateDisabled}"> <!--  NBA211 -->
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputText id="city" value="#{property.appEntAppSignedCity}" styleClass="formLabel" style="width: 185px;"/>
			<h:inputText id="cityEF" value="#{pc_applicationInfo.signedCity}" styleClass="formEntryText" style="width: 150px;"/>								
		</h:panelGroup>
		<h:panelGroup id="signedDatePGroup" styleClass="formDataEntryLine" >
			<h:outputText id="state" value="#{property.appEntAppSignedState}" styleClass="formLabel" style="width: 165px;"/>
			<h:selectOneMenu id="stateDD" value ="#{pc_applicationInfo.signedState}" styleClass="formEntryTextFull" style="width: 290px;"
				disabled="#{pc_applicationInfo.partialUpdateDisabled}"> <!--  NBA211 -->
				<f:selectItems value="#{pc_applicationInfo.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
