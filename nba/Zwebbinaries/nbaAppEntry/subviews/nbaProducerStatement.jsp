<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<f:loadBundle basename="properties.nbaApplicationData" var="properties" />
		<h:outputText id="prodStmnt" value="#{property.appEntProdStmnt}" styleClass="formSectionBar" />
		<h:panelGroup id="question1PGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
			<h:outputText id="q1" value="#{property.appEntPage7Q1}" styleClass="formLabel" style="width: 495px;text-align: left;"/>
			<h:selectBooleanCheckbox id="q1CBYes" value="#{pc_applicationInfo.prodStmnt1Yes}" styleClass="ovFullCellSelectCheckBox" style="width: 20px;" 
				onclick="toggleCBGroup('producerStmtInformation:q1CBYes');"/>
			<h:outputText id="y1" value="#{property.appEntYes}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
			<h:selectBooleanCheckbox id="q1CBNo" value="#{pc_applicationInfo.prodStmnt1No}" styleClass="ovFullCellSelectCheckBox" style="width: 20px;" 
				onclick="toggleCBGroup('producerStmtInformation:q1CBNo');"/>
			<h:outputText id="n1" value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
		</h:panelGroup>
		<h:panelGroup id="question2PGroup" styleClass="formDataEntryLine" style="padding-left:5px;">
			<h:outputText id="q2" value="#{property.appEntPage7Q2}" styleClass="formLabel" style="width: 495px;text-align: left;"/>
			<h:selectBooleanCheckbox id="q2CBYes" value="#{pc_applicationInfo.prodStmnt2Yes}" styleClass="ovFullCellSelectCheckBox" style="width: 20px" 
				onclick="toggleCBGroup('producerStmtInformation:q2CBYes');"/>
			<h:outputText id="y2" value="#{property.appEntYes}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
			<h:selectBooleanCheckbox id="q2CBNo" value="#{pc_applicationInfo.prodStmnt2No}" styleClass="ovFullCellSelectCheckBox" style="width: 20px" 
				onclick="toggleCBGroup('producerStmtInformation:q2CBNo');"/>
			<h:outputText id="n2" value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: left;"/>
		</h:panelGroup>
</jsp:root>