<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA247		   	   8 	  wmA Application Entry Project -->
<!-- NBA317          NB-1301  PCI Compliance For Credit Card Numbers Using Web Service  --> 
<!-- SPRNBA-23       NB-1601  Add Mail Codes to Contract Validation  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="mailCodeTitle" value="#{property.appEntMailCodesTitle}" styleClass="formSectionBar"/>
	<h:panelGrid columns="3" style="padding-top: 5px;" columnClasses="colOwnerInfo,colMoreCodes" cellspacing="0" cellpadding="0">
		<h:column>
			<h:dataTable id="mailCodeTable" styleClass="formTableData" style="width: 535px;" cellspacing="0" cellpadding="0" rows="0" value="#{pc_mailCodeInfo.mailCodes}" var="mailCodeRow" >
				<h:column>
					<h:panelGroup styleClass="formDataDisplayLine">
						<h:outputText id="mailCodeType" value="#{property.appEntMailCodeType}" styleClass="formLabel" style="width: 120px;"/>
						<h:selectOneMenu id="mailCodeTypeDD" value="#{mailCodeRow.type}" styleClass="formEntryTextFull"  style="width: 400px;"  onchange="resetTargetFrame();handleSubmit();"><!-- NBA317 -->
							<f:selectItems id="mailCodeTypeList" value="#{pc_mailCodeInfo.typesList}" /> <!-- SPRNBA-23 -->
						</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataDisplayLine" style="padding-bottom:19px">
						<h:outputText id="mailCodes" value="#{property.appEntMailCodes}" styleClass="formLabel" style="width: 120px;"/>
						<h:selectOneMenu id="mailCodeDD" value="#{mailCodeRow.code}" styleClass="formEntryTextFull"  style="width: 400px;" onchange="resetTargetFrame();handleSubmit();" ><!-- NBA317 --> 
							<f:selectItems id="mailCodesList" value="#{pc_mailCodeInfo.codesList}" /> <!-- SPRNBA-23 -->
						</h:selectOneMenu>
					</h:panelGroup>
				</h:column>
			</h:dataTable>
		</h:column>
		<h:column>
			<h:panelGroup styleClass="formDataDisplayLine" style="padding-bottom:19px">
				<h:commandLink id="addAnotherButton" value="#{property.buttonAddAnother}" action="#{pc_mailCodeInfo.addAnotherMailCode}" styleClass="formButtonInterface" style="width: 65px;margin-left: 5px;" />
			</h:panelGroup>
		</h:column>
	</h:panelGrid>
</jsp:root>
