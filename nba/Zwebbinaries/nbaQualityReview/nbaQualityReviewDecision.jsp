<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA318         NB-1301    Quality Review Process -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
     <head>
			<base href="<%=basePath%>">
			<title>Review Quality</title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<script language="JavaScript" type="text/javascript">
				var width=400;
				var height=120;  
				function setTargetFrame() {
					document.forms['form_qualityReviewDecision'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_qualityReviewDecision'].target='';
					return false;
				}
				function resizePopup(){
					if(document.forms['form_qualityReviewDecision']['form_qualityReviewDecision:reviewDecision'][1].checked){
						height = 330;
						width = 500;
					}else{
						height = 120;
						width = 400;
					}
				}
				function closePopup(){
					window.opener = parent;
					parent.closeWindow();
					parent.hideWait();
				}
			</script>
	</head>
	<body onload="resizePopup();popupInit();">
	<f:view>	    
		   <f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		   <PopulateBean:Load serviceName="RETRIEVE_ROUTE_INFO" value="#{pc_routeInfo}" />		
	   	   <h:form id="form_qualityReviewDecision">
		   	
			<h:panelGroup id="reviewDecision_PGroup" styleClass="formDataEntryLine">
				<h:selectOneRadio id="reviewDecision"  layout="pageDirection" styleClass="radioLabel" style="width :150px; margin-left: 12px;"			  	  	 
			 	  	onclick="resetTargetFrame();submit();" value="#{pc_qualityReviewDecision.qualityReviewDecision}" valueChangeListener="#{pc_qualityReviewDecision.valueChangeRadio}">
					<f:selectItem itemLabel="#{property.reviewAccepted}" itemValue="1" />
					<f:selectItem itemLabel="#{property.reviewRejected}" itemValue="2" />
				</h:selectOneRadio>
			</h:panelGroup>
			
			
    	    <f:subview id="nbaRouteInfo" rendered="#{pc_qualityReviewDecision.displayrouteInfo}">
				<c:import url="/nbaRoute/subviews/nbaRouteInfo.jsp" />
			</f:subview>

					
		    <h:panelGroup id="buttonGroup" styleClass="buttonBar">
			  <h:commandButton id="reviewCancel_CButton" value="#{property.buttonCancel}" action="#{pc_qualityReviewDecision.cancel}" onclick="setTargetFrame();" styleClass="buttonLeft" />
			  <h:commandButton id="reviewCommit_CButton" value="#{property.buttonCommit}" action="#{pc_qualityReviewDecision.commit}" onclick="setTargetFrame();closePopup();" styleClass="buttonRight" disabled="#{pc_qualityReviewDecision.disableCommit}" />
			</h:panelGroup>

		  </h:form>
		  <div id="Messages" style="display:none">
				<h:messages />
		  </div>
	</f:view>
	</body>
</html>
		