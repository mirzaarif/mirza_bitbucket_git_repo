<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA305         NB-1301   Standardized CSC Product UI Colors -->
<!-- NBA244         NB-1401   Combine Contract Status and Contract Summary View -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACTSUMMARY" value="#{pc_contractSummary}" />
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<html>
	<head>
		<base href="<%=basePath%>">
		<title><h:outputText value="#{property.summaryTitle}" /></title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">    
		<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
		<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />  <!-- NBA305 -->
		<!-- Begin NBA244 -->
		<script type="text/javascript"> 
		function resizeView()
		{
		  var viewHeight=window.document.body.scrollHeight; 
		  var winHeight = window.screen.availHeight;
		  if((winHeight-viewHeight)>50){
		  window.resizeTo(670,viewHeight+50);		  
		  }
		  else{
		  window.resizeTo(670,winHeight);
		  }
		}
		</script>
		<!-- End NBA244 -->
	
	</head>
	<body background="images/watermark/case-watermark.gif"
			style="background-position: center; background-repeat: no-repeat;" onload="resizeView()" >  <!-- NBA305,NBA244 -->	
		<h:form id="form_contractsummary" >
			<f:subview id="tabHeader" >
				<c:import url="/uw/subviews/nbaContractSummaryDetail.jsp"  />
			</f:subview>	
			<!-- Begin NBA244 -->		
			<h:panelGroup>	
	<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"  action="#{pc_contractSummary.refresh}"/>
		</h:panelGroup>
		<!-- End NBA244 -->
		</h:form>
		
	</body>
</html>
</f:view>
	