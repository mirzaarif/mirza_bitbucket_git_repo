<%-- CHANGE LOG
Audit Number   Version   Change Description
SPR3548           8      General Code Clean Up for Version 8
NBA186            8      nbA Underwriter Additional Approval and Referral Project
NBA350			NB-1401   Applet Implementation of AWD Image Viewer 
NBA411			NB-1601   nbA Mass Case Manager Reassignment 
--%>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>Logon Page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript" src="javascript/global/menu.js"></script>
	
	<script>
	
	function initialize(){
		try{
			if(top.notificationClientFrame.getExtension() == null || (top.notificationClientFrame.getExtension() != null && top.notificationClientFrame.getExtension() == "")){
				var currentExtension = top.notificationClientFrame.register();
				if( currentExtension != null && currentExtension != ""){
					document.all["TelephonyMenu"].style.display = "inline";
					if(top.notificationClientFrame.getAvailable() == false){
						document.all["UnavailableCalls"].style.display = "inline";
						document.all["AvailableCalls"].style.display = "none";
					} else {
						document.all["AvailableCalls"].style.display = "inline";
						document.all["UnavailableCalls"].style.display = "none";
					}
				} else {
					document.all["ManualTelephonyMenu"].style.display = "inline";
				}
			} else {
				document.all["TelephonyMenu"].style.display = "inline";
				if(top.notificationClientFrame.getAvailable() == false){
					document.all["UnavailableCalls"].style.display = "inline";
					document.all["AvailableCalls"].style.display = "none";
				} else {
					document.all["AvailableCalls"].style.display = "inline";
					document.all["UnavailableCalls"].style.display = "none";
				}
			}
		}catch(err){
			document.all["ManualTelephonyMenu"].style.display = "inline";
		}
		try{
			if(top.notificationClientFrame.chatEnabled != null && top.notificationClientFrame.chatEnabled == "true"){
				if(top.notificationClientFrame.notifyClient.getChatUser() == null || (top.notificationClientFrame.notifyClient.getChatUser() != null && top.notificationClientFrame.notifyClient.getChatUser() == "")){
					top.notificationClientFrame.registerChat();
				}
				document.all["ChatMenu"].style.display = "inline";
				if(top.notificationClientFrame.getAvailableChat() == false){
					document.all["UnavailableChats"].style.display = "inline";
					document.all["AvailableChats"].style.display = "none";
				} else {
					document.all["AvailableChats"].style.display = "inline";
					document.all["UnavailableChats"].style.display = "none";
				}
			}			
		}catch(err1){
		}
		//begin NBA350
		if (top.document.getElementById("ImgViewer").src == "imageViewer/imageViewerInitialization.jsp") {	//Will be true the first time the jsp loads
			try {
				var userid = document.forms['coordmenuForm']['coordmenuForm:userName'].value;
				var userpwd = document.forms['coordmenuForm']['coordmenuForm:userPwd'].value;
				var imageViewInitURL = document.forms['coordmenuForm']['coordmenuForm:imageViewInitURL'].value;				
				top.ImgViewer.loadImageViewer(userid, userpwd, imageViewInitURL);	//Load the Applet
			} catch(err1){
				alert ('loadImageViewer failed: ' + err1.description);
			}			
		}
		//end NBA350
	}
	
	function makeAvailableForCalls(){
		top.notificationClientFrame.switchAvailable();
		if(top.notificationClientFrame.getAvailable() == false){
			document.all["UnavailableCalls"].style.display = "inline";
			document.all["AvailableCalls"].style.display = "none";
		} else {
			document.all["AvailableCalls"].style.display = "inline";
			document.all["UnavailableCalls"].style.display = "none";
		}
	}
	
	function makeAvailableForChats(){
		top.notificationClientFrame.switchAvailableChat();
		if(top.notificationClientFrame.getAvailableChat() == false){
			document.all["UnavailableChats"].style.display = "inline";
			document.all["AvailableChats"].style.display = "none";
		} else {
			document.all["AvailableChats"].style.display = "inline";
			document.all["UnavailableChats"].style.display = "none";
		}
	}
	
	<%-- begin NBA186 --%>
	function setTargetFrame() {
		document.forms['coordmenuForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		document.forms['coordmenuForm'].target='';
		return false;
	}
	<%-- end NBA186 --%>
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
	
</head>
  
<body class="menubar" onload="menuBarInit();initialize();" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
	<f:view>
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" /> 	
		<f:loadBundle var="property" basename="properties.nbaApplicationData" />  <%-- SPR3548 --%>
		<h:form id="coordmenuForm">
		<h:outputText  id="refresh" value="#{pc_Coordinator.refresh}"></h:outputText>
		<table class="menubar" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
			<tbody>
			<tr class="menubar" id="menuBar" align="left">
				<td class="menu" align="left">
					<%-- add menu bar here --%>
					<span id="ManualTelephonyMenu" style="display:none; text-align: left;">
						<h:commandLink id="takeCallCmdLink" value="" action="#{pc_Coordinator.takeCall}"
									rendered="#{pc_Coordinator.auth.visibility['takeCall']}" onmousedown="setTargetFrame();" >  <%-- NBA186 --%>
							<h:outputText id="takeCallCmdText" styleClass="menu"  value="#{bundle.coordinator_menu_takecall}"></h:outputText>
						</h:commandLink>
					</span>

					<h:commandLink id="cashieringCmdLink" action="#{pc_Coordinator.cashiering}" value=""
								rendered="#{pc_Coordinator.auth.visibility['cashiering']}" onmousedown="setTargetFrame();" >  <%-- NBA186 --%>
						<h:outputText id="cashieringCmdText" styleClass="menu"  value="#{property.coordinator_menu_cashiering}"></h:outputText>  <%-- SPR3548 --%>
					</h:commandLink>

					<h:commandLink id="changeContractCmdLink" action="#{pc_Coordinator.changeContract}" value=""
								rendered="#{pc_Coordinator.auth.visibility['changeContract']}" onmousedown="setTargetFrame();" >  <%-- NBA186 --%>
						<h:outputText id="changeContractCmdText" styleClass="menu"  value="#{property.coordinator_menu_changeContract}"></h:outputText>  <%-- SPR3548 --%>
					</h:commandLink>

					<h:commandLink id="workloadCmdLink" action="#{pc_Coordinator.workloadManagement}" value=""
								rendered="#{pc_Coordinator.auth.visibility['workload']}" onmousedown="setTargetFrame();" >  <%-- NBA186, NBA411 --%>
						<h:outputText id="workoadCmdText" styleClass="menu"  value="#{property.coordinator_menu_workload}"></h:outputText>  <%-- SPR3548,NBA411 --%>
					</h:commandLink>

					<h:commandLink id="backOfficeCmdLink" action="#{pc_Coordinator.backOffice}" value=""
								rendered="#{pc_Coordinator.auth.visibility['processCenter']}" onmousedown="setTargetFrame();" >  <%-- NBA186 --%>
						<h:outputText id="backOfficeCmdText" styleClass="menu"  value="#{property.coordinator_menu_backoffice}"></h:outputText>  <%-- SPR3548 --%>
					</h:commandLink>

					<span id="TelephonyMenu" style="display:none; text-align: left;">
						<span class="menu">
							|
						</span>
						<a href="#" id="AvailableCalls" onclick="makeAvailableForCalls(); return false;">
							<h:outputText id="makeAvailCmdText" styleClass="phoneMenu"  
								value="#{bundle.telephony_make_unavailable}" >
							</h:outputText>
						</a>
						<a href="#" id="UnavailableCalls" onclick="makeAvailableForCalls(); return false;">
							<h:outputText id="makeUnavailCmdText" styleClass="phoneMenu"  
								value="#{bundle.telephony_make_available}" >
							</h:outputText>
						</a>
					</span>
					
					<span id="ChatMenu" style="display:none; text-align: left;">
						<span class="menu">
							|
						</span>
						<a href="#" id="AvailableChats" onclick="makeAvailableForChats(); return false;">
							<h:outputText id="makeAvailChatCmdText" styleClass="phoneMenu"  
								value="#{bundle.chat_make_unavailable}" >
							</h:outputText>
						</a>
						<a href="#" id="UnavailableChats" onclick="makeAvailableForChats(); return false;">
							<h:outputText id="makeUnavailChatCmdText" styleClass="phoneMenu"  
								value="#{bundle.chat_make_available}" >
							</h:outputText>
						</a>
					</span>
					
				</td>
				<td align="right">
					<span onclick="return top.mainContentFrame.toggleMenu(this);">
						<h:outputText id="utilitiesCmdText" styleClass="menu" 
							value="#{bundle.call_center_menu_utilities}" rendered="#{pc_Coordinator.auth.visibility['utilities']}"> <%-- NBA213 --%>
						</h:outputText>
					</span>
					<h:commandLink id="logoffCmdLink" action="#{pc_Coordinator.logoff}" value="" onmousedown="setTargetFrame();" >  <%-- NBA186 --%>
						<h:outputText id="logoffCmdText" styleClass="menu"  value="#{bundle.coordinator_menu_logoff}"></h:outputText>
					</h:commandLink>
				</td>
			</tr>
			<%-- begin NBA350 --%>
			<tr>
				<td>
					<h:inputHidden id="userName" value="#{pc_Desktop.userName}"></h:inputHidden>
					<h:inputHidden id="userPwd" value="#{pc_Coordinator.pwd}"></h:inputHidden>
					<h:inputHidden id="imageViewInitURL" value="#{pc_Coordinator.imageViewInitURL}"></h:inputHidden>
				</td>
			</tr>
			<%-- end NBA350 --%>
			</tbody>
		</table>
	</h:form>		
		<div id="Messages" style="display: none"><h:messages /></div>
	</f:view>
</body>

</html>
