<%-- CHANGE LOG
		Audit Number   Version   Change Description
		NBA122            5      Underwriter Workbench Rewrite
		SPR3058           6      Upgraded status bar support when a case open is not open
		NBA134            6      nbA Assignment and Reassignment of an Underwriter Queue Project
		NBA171            6      nbA Linux re-certification project
		SPR3175           6      Browser becomes unavailable with wait cursor
		NBA230            8      Reopen Date Enhancement Project
		NBA227            8      Selection List of Images to Display
		FNB011         NB-1101   Work Tracking
		FNB004         NB-1101   PHI
		NBA231         NB-1101   Replacement Processing
		NBA244         NB-1401   Combine Contract Status and Contract Summary View
		SPRNBA-798     NB-1401   Change JSTL Specification Level
		SPRNBA-829     NB-1401   View All Images Does Not Open If View Images Summary Folder Icon First Invoked
		NBA356         NB-1501   Comments Floating View
		SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View
		NBA389		   NB-1601	 Contract Print Manual Form Selection Enhancement --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><%-- NBA171 --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>nbA Status Bar</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css" />  <%-- NBA213 --%>
	<link rel="stylesheet" type="text/css" href="theme/accelerator.css" />
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
	<script type="text/javascript">
		<!--					
				var contextpath = '<%=path%>';	//FNB011		
		
				var contractSummary;
				var commentPopup; //SPR3073
				var phipopup;  //FNB004 - VEPL1224
				function launchSummary() {
					if (!contractSummary) {
						contractSummary =launchPopupWithScrollBar('ContractSummary', './nbaContractSummary.jsp', 660, 690);  //NBA134 NBA230 NBA231 NBA244
						contractSummary.focus();
					} else {
						if (!contractSummary.closed) {
							contractSummary.focus();
						} else {
							contractSummary = launchPopupWithScrollBar('ContractSummary', './nbaContractSummary.jsp', 660, 690);  //NBA134 NBA230 NBA231 NBA244
							contractSummary.focus();
						}
					}
				}
				//Begin NBA227				
				var imagesSummary;
				//Begin NBA389
				function launchImagesSummary() {
					if (!imagesSummary || imagesSummary.closed) {
						imagesSummary = launchPopup('ImagesSummary',
										'' + contextpath + '/common/popup/popupFrameset.html?popup='
											+ contextpath + '/nbaImages/nbaImagesSummary.faces' ,
										650, 350);
					}
					imagesSummary.focus();
				}
				//End NBA389
				//End NBA227
				//begin SPRNBA-306
				function launchComment(commentType) {
					if (!commentPopup || commentPopup.closed) {
						commentPopup = launchPopup('CommentPopUp',
										'' + contextpath + '/common/popup/popupFrameset.html?popup='
											+ contextpath + '/nbaComments/popups/nbaCommentsTabFile.faces?commentType=' + commentType,
										650, 350);
					}
					commentPopup.focus();
				}
				//end SPRNBA-306
				function closePopups(){
					if (contractSummary) {
						closePopup(contractSummary);
					}
 					if (phipopup) {	//FNB004 - VEPL1224
						closePopup(phipopup);	//FNB004 - VEPL1224
					}	//FNB004 - VEPL1224				
					//Begin SPR3073
					if (commentPopup){ 
						closePopup(commentPopup); 
					}
					//End SPR3073
					if (imagesSummary){  // NBA227
						closePopup(imagesSummary);  // NBA227
					}	// NBA227
					if (commentView) {	//NBA356 To close UW Comments Popup View 
						closePopup(commentView);  //NBA356	
					}  //NBA356
				}
				//Begin NBA227
				function refreshImageSummary() {
					if (imagesSummary != null && !imagesSummary.closed) { //SPRNBA-829
						imagesSummary.document.location.href = imagesSummary.document.location.href;
					}
				}
				//End NBA227
		//-->
	</script>
</head>
<%-- NBA213 - commonized the header area to me more like csa/cma item id area --%>
<body class="contractBar" onunload="closePopups();" style="margin-left: 0px; margin-right: 0px;" >  <%-- NBA213 --%>
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_CONTRACTSTATUS" value="#{pc_contractStatus}" />
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:outputFormat id="wiBar" value="{0} - {1}" styleClass="workStatusBar" title="#{pc_contractStatus.routeReason}">
			<f:param id="wiType" value="#{pc_contractStatus.workTypeText}" />
			<f:param id="wiStatus" value="#{pc_contractStatus.routeReason}" />
		</h:outputFormat>
		<h:panelGroup id="contractBar" styleClass="contractBar">
			<h:commandButton id="cbSummary" image="images/link_icons/case.gif" title="#{property.summaryToolTip}" disabled="#{!pc_contractStatus.summaryAvailable}" style="position: relative; left:10px; top:5px" onclick="launchSummary(); top.hideWait(); return false;" onmouseover="this.isClicked=false;"/>  <%-- SPR3058, SPR3175 --%>
			<h:outputText id="contractInsured" value="#{pc_contractStatus.insuredName}" style="position: relative; left:15px; top:3px; max-width: 100px;" styleClass="contractBarInsured" />
			<h:outputText id="contractNumber" value="#{pc_contractStatus.polNumber}" style="position: relative; left:30px; top:3px" styleClass="contractBarNumber" />
			<h:outputText id="contractStatus" value="#{pc_contractStatus.contractStatus}" style="position: relative; left:40px; top:3px; font-size: 10px" styleClass="contractBarStatus" />
			<h:outputText id="contractCarrier" value="#{pc_contractStatus.carrierCodeText}" style="position: absolute; top: auto; margin-top: 2px; right: 15px; width: 120px" styleClass="contractBarCompany" />
			<h:commandButton id="cbImagesSummary" image="images/link_icons/documents-selection.gif" title="#{property.imageSummaryToolTip}" style="position: absolute; top: auto; margin-top: 2px; right: 140px;" 
							onclick="launchImagesSummary(); top.hideWait(); return false;" onmouseover="this.isClicked=false;" rendered="#{pc_nbaReinsNav.auth.visibility['Images']}"/> <%-- NBA227 FNB011--%>
		</h:panelGroup>
	</f:view>
</body>
</html>
