

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Call Center Menu</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/menu.js"></script>
<script type="text/javascript">
	<!--

			function disableCTI(){
				document.all["callManagement"].style.display = "none";
			}
			
			function initCallCenter(){
				// setup telephony controls
				if(document.all["callCenterForm:CallEnabled"] != null){
					try{
						if(top.notificationClientFrame.getExtension() == null || (top.notificationClientFrame.getExtension() != null && top.notificationClientFrame.getExtension() == "")){
							document.all["callManagement"].style.display = "none";
							document.all["callManagementManual"].style.display ="inline";
						} else {
							document.all["callManagementManual"].style.display ="none";
							document.all["callManagement"].style.display = "inline";
						}
					}catch(err){
						document.all["callManagement"].style.display = "none";
						document.all["callManagementManual"].style.display ="inline";
					}
	
					try{
						if(top.notificationClientFrame.isCurrentCallHeld()){
							document.all["holdCall"].style.display = "none";
							document.all["resumeCall"].style.display ="inline";
						} else {
							document.all["holdCall"].style.display = "inline";
							document.all["resumeCall"].style.display ="none";
						}
					}catch(err){
							document.all["holdCall"].style.display = "inline";
							document.all["resumeCall"].style.display ="none";
					}
				}	
				// see if we have a productID from the select customer
				
				try{
					var productID = top.getCookieValue("policyProduct");
					if(productID != null && productID != ""){
						top.loadClaimOnRight("<%=basePath%>");
					}
				} catch(err1){
					top.reportException(err1, "loading policy");
				}
			}
			
			function hold(){
				try{
					top.notificationClientFrame.hold();
				} catch(err1){
				}
				
				try{
					if(top.notificationClientFrame.isCurrentCallHeld()){
						document.all["holdCall"].style.display = "none";
						document.all["resumeCall"].style.display ="inline";
					} else {
						document.all["holdCall"].style.display = "inline";
						document.all["resumeCall"].style.display ="none";
					}
				}catch(err){
						document.all["holdCall"].style.display = "inline";
						document.all["resumeCall"].style.display ="none";
				}
				return false;
			}
			
			function resume(){
				try{
					top.notificationClientFrame.resume();
				} catch(err1){
				}
				
				try{
					if(top.notificationClientFrame.isCurrentCallHeld()){
						document.all["holdCall"].style.display = "none";
						document.all["resumeCall"].style.display ="inline";
					} else {
						document.all["holdCall"].style.display = "inline";
						document.all["resumeCall"].style.display ="none";
					}
				}catch(err){
						document.all["holdCall"].style.display = "inline";
						document.all["resumeCall"].style.display ="none";
				}
				return false;
			}
			
			function consult(){
				top.controlFrame.document.write("<SCRIPT>top.showWindow('<%=path%>', 'faces/cti/consultCall.jsp',top);</SCRIPT>");
				return false;
			}
			
			function dial(){
				top.controlFrame.document.write("<SCRIPT>top.showWindow('<%=path%>', 'faces/cti/dialCall.jsp',top);</SCRIPT>");
				return false;
			}

	//-->
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>

</head>
<body class="contextBar" onload="menuBarInit();initCallCenter();" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<f:view>
	<h:form id="callCenterForm">
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
			<h:outputText id="RefreshText" rendered="true" value="#{pc_CallCenter.refresh}"></h:outputText>
			<table cellpadding="0" cellspacing="2" border="0" width="98%" height="100%">
				<tbody>
				<tr class="contextBar" id="menuBar" align="left">
					<td class="menu" align="left">
					<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
						<h:outputText id="CallRefresh" rendered="true" value="#{pc_CallCenter.refresh}" style="display:none"></h:outputText>
						<h:outputText id="CallEnabled" rendered="#{pc_Coordinator.callEnabled}" value="" style="display:none"></h:outputText>
						<span id="callManagement" style="display: none">
							<span id="holdCall" style="display: none">
								<a href="#" onclick="return hold();">
								<h:outputText id="HoldLink" styleClass="phoneMenu" rendered="true" value="#{bundle.telephony_hold_call}"></h:outputText></a>
							</span>
							<span id="resumeCall" style="display: none">
								<a href="#" onclick="return resume();">
								<h:outputText id="ResumeLink" styleClass="phoneMenu" rendered="true" value="#{bundle.telephony_resume_call}"></h:outputText></a>
							</span>
							<a href="#" onclick="return consult();">
							<h:outputText id="ConsultLink" styleClass="phoneMenu" rendered="true" value="#{bundle.telephony_consult_call}"></h:outputText></a>
							<h:outputText id="OutCallDialLink" rendered="#{pc_Coordinator.outboundCallEnabled}" value="">
								<a href="#" onclick="return dial();"><h:outputText styleClass="phoneMenu" rendered="true" value="#{bundle.telephony_dial_call}"></h:outputText></a>
							</h:outputText>
							<span > | </span>
						</span>
						
						<span id="callManagementManual" style="display: none">
							<a href="#" onclick="return consult();"><h:outputText styleClass="phoneMenuManual" value="#{bundle.telephony_consult_call}"></h:outputText></a>
							<h:outputText id="OutCallEnabled" rendered="#{pc_Coordinator.outboundCallEnabled}" value="">
								<a href="#" onclick="return dial();"><h:outputText styleClass="phoneMenuManual" rendered="true" value="#{bundle.telephony_dial_call}"></h:outputText></a>
							</h:outputText>
							<span > | </span>
						</span>
					<h:commandLink id="createWorkLink" action="#{pc_CallCenter.createWork}" value="" ><h:outputText id="createWorkText" styleClass="menu"	rendered="#{pc_CallCenter.auth.visibility['createWorkItem']}" value="#{bundle.create_work_link}"></h:outputText></h:commandLink>
					<h:commandLink id="wrapup" action="#{pc_CallCenter.wrapUp}" value=""><h:outputText styleClass="menu" rendered="true" value="#{bundle.call_center_menu_wrap_up}"></h:outputText></h:commandLink>
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>



