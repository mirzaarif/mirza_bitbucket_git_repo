<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3690          8      On invoking the Utilities menu in workload management, nbA logon view is displayed instead of displaying the utilities menu items -->
<!-- SPRNBA-947 	NB-1601  Null Pointer Exception May Occur in Inbox if User Selects Different Work -->
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Call Center Menu</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<%-- SPRNBA-947 code deleted--%> 
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>

</head>
<body class="menubar" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"> <%-- SPRNBA-947 remove filePageInit --%>
</body>
</html>
