

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Call Center Menu</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/menu.js"></script>
<script type="text/javascript">
	<!--
		function initChatCenter(){
			try{
				var productID = top.getCookieValue("policyProduct");
				if(productID != null && productID != ""){
					top.loadClaimOnRight("<%=basePath%>");
				}
			} catch(err1){
				top.reportException(err1, "loading policy");
			}
		}
	//-->
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>

</head>
<body class="contextBar" onload="menuBarInit();initChatCenter();" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<f:view>
	<h:form id="chatCenterForm">
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
			<h:outputText id="RefreshText" rendered="true" value="#{pc_CallCenter.refresh}"></h:outputText>
			<table cellpadding="0" cellspacing="2" border="0" width="98%" height="100%">
				<tbody>
				<tr class="contextBar" id="menuBar" align="left">
					<td class="menu" align="left">
					<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
						<h:outputText id="CallRefresh" rendered="true" value="#{pc_CallCenter.refresh}" style="display:none"></h:outputText>
						<h:outputText id="CallEnabled" rendered="#{pc_Coordinator.callEnabled}" value="" style="display:none"></h:outputText>
						<h:commandLink id="createWorkLink" action="#{pc_CallCenter.createWork}" value="" ><h:outputText id="createWorkText" styleClass="menu"	rendered="#{pc_CallCenter.auth.visibility['createWorkItem']}" value="#{bundle.create_work_link}"></h:outputText></h:commandLink>
						<h:commandLink id="wrapup" action="#{pc_CallCenter.wrapUpChat}" value=""><h:outputText styleClass="menu" rendered="true" value="#{bundle.call_center_menu_wrap_up}"></h:outputText></h:commandLink>
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>



