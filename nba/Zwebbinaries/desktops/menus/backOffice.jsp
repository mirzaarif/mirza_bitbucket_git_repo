<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA213            7      Unified User Interface -->
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                    + "/";
        }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Back Office</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/menu.js"></script>

<script type="text/javascript">
			<!--
        var titleName="";
		var width=690;
		var height=340;
			function setTargetFrame() {
				document.forms['backOfficeForm'].target='controlFrame';
				return false;
			}
			function resetTargetFrame() {
				document.forms['backOfficeForm'].target='';
				return false;
			}
			//-->
		</script>


<script type="text/javascript"
	src="javascript/global/desktopComponent.js"></script>
</head>

<body class="contextbar" onload="menuBarInit();" marginheight="0"
	marginwidth="0" leftmargin="0" topmargin="0">
<f:view>
	<h:form id="backOfficeForm">
		<f:loadBundle var="bundle"
			basename="com.csc.fs.accel.ui.config.ApplicationData" />
		<h:outputText rendered="true" value="#{pc_BackOffice.refresh}"></h:outputText>
		<table cellpadding="0" cellspacing="0" border="0" width="98%" height="100%">
				<tbody>
				<tr class="contextBar" id="menuBar" align="left">
					<td class="menu" align="left">
                        <h:commandLink id="createWorkLink" action="#{pc_BackOffice.createWorkForSelected}"
							value="" onmousedown="setTargetFrame();" immediate="true"><!-- NBA213 -->
							<h:outputText id="createWorkText" styleClass="menu" rendered="#{pc_BackOffice.auth.visibility['createWorkItem']}"
								value="#{bundle.create_work_link}">  <!-- NBA213 -->
							</h:outputText>
						</h:commandLink>								
                        <!-- begin NBA213 -->
                        <h:commandLink id="completeWorkLink" action="#{pc_BackOffice.completeWork}"
							value="" onmousedown="setTargetFrame();" immediate="true">
							<h:outputText id="completeWorkText" styleClass="menu" rendered="#{pc_BackOffice.auth.visibility['completeWorkItem']}"
								value="#{bundle.back_office_complete_work}">
							</h:outputText>
						</h:commandLink>								
                        <h:commandLink id="closeWorkLink" action="#{pc_BackOffice.closeWorkItem}"
							value="" onmousedown="setTargetFrame();" immediate="true">
							<h:outputText id="closeWorkText" styleClass="menu" rendered="#{pc_BackOffice.auth.visibility['closeWorkItem']}"
								value="#{bundle.back_office_close_work_item}">
							</h:outputText>
						</h:commandLink>								
                        <!-- end NBA213 -->
						 <h:commandLink id="closeDesktopLink" action="#{pc_BackOffice.endDesktopUnlockAll}"
							value="">	 <!-- NBA213 -->
							<h:outputText id="closeDesktopText" styleClass="menu" rendered="true"
								value="#{bundle.back_office_close}">
							</h:outputText>
						</h:commandLink>	
						</td>
					</tr>
				</tbody>
			</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
