<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182 		   7      Cashiering Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>Cashiering Desktop</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">

	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>	
	<script type="text/javascript">
		<!--
				var defaultWindowOrientation;

				function initialize(){
					top.loadMenuBar('<%= path%>','/faces/desktops/menus/globalMenu.jsp');
					top.loadContextBar('<%=	path%>','/faces/desktops/menus/cashieringMenu.jsp');
					defaultWindowOrientation = top.defaultWindowOrientation;
					top.defaultWindowOrientation = top.FULL_SCREEN;
					top.showFullScreen();
				}

				function resetDefaultOrientation() {
					top.defaultWindowOrientation = defaultWindowOrientation;
					top.restoreWindow();
				}
		//-->
	</script>
</head>
<body class="desktopPanelBody" onload="initialize();" onunload="resetDefaultOrientation();">
	<f:view>
		<h:form id="cashieringForm">
		<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr class="menubar" id="menuBar">
					<td colspan="3" align="right">
					<iframe style="height: 22px; width: 100%;" id="desktopMenu" name="desktopMenu" src="initializingMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
				</tr>
				<tr class="contextBar" id="contextBar">
					<td id="typeMenuContainer" align="left" width="200">
						<h:graphicImage value="#{pc_Coordinator.desktopImage}" style="vertical-align: middle;"></h:graphicImage> <h:outputText value="Cashiering"></h:outputText>
					</td>
					<td align="left" valign="bottom" id="contextMenuContainer">
						<iframe height="22px" width="100%" id="contextMenu" name="contextMenu" src="initializingContextMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
					<td id="dateMenuContainter" align="right" width="200">
						<div id="CurrentDate">
						</div>
					</td>
				</tr>
				<tr class="desktopBody">
					<td colspan="3">
					<div id="contentArea">
						<table id="contentAreaTable" height="100%" width="100%">
							<tr>
								<td id="leftPane">
									<iframe name="contentLeftFrame" height="100%" width="100%" scrolling="NO" frameborder="0"
											src="faces/nbaCashiering/cashieringFile.jsp" marginheight="0" marginwidth="0">
									</iframe>
								</td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
			</tbody>
		</table>
		</h:form>
	</f:view>
</body>
</html>
