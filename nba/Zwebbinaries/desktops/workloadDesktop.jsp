<%-- CHANGE LOG
Audit Number   Version   Change Description
NBA411			NB-1601   nbA Mass Case Manager Reassignment 
--%>
<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Workload</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--

		function initialize(){
			top.loadMenuBar('<%= path%>','/faces/desktops/menus/globalMenu.jsp');
			top.loadContextBar('<%=	path%>','/faces/desktops/menus/workloadMenu.jsp');
			top.restoreWindow();
			top.setContentAreaSize();
		}

		function reloadLeft(){
		    contentLeftFrame.location.href = '<%=path%>/faces/workflow/workflowFile.jsp';
		}

	//-->
</script>
</head>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<body class="desktopPanelBody" onload="initialize();" onunload="top.resetDefaultOrientation();">
<f:view>
	<h:form id="workloadDesktopForm"> <%-- NBA411 --%>
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
			<div style="display:none">
				<h:outputText value="#{pc_BackOffice.refresh}"></h:outputText>
			</div>
			
		<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr class="menubar" id="menuBar">
					<td colspan="3" align="right">
					<iframe style="height: 22px; width: 100%;" name="desktopMenu" name="desktopMenu" src="initializingMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
				</tr>
				<tr class="contextBar" id="contextBar">
					<td id="typeMenuContainer" align="left" width="200">
						<h:graphicImage value="#{pc_BackOffice.desktopImage}" style="vertical-align: middle;"></h:graphicImage><h:outputText value="Workload"></h:outputText> | <span id="CallTime"></span> |
					</td>
					<td align="left" valign="bottom" id="contextMenuContainer">
						<iframe height="22px" width="100%" id="contextMenu" name="contextMenu" src="initializingContextMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
					<td id="dateMenuContainter" align="right" width="200">
						<div id="CurrentDate">
						</div>
					</td>
				</tr>
				<tr class="desktopBody">
					<td colspan="3">
					<div id="contentArea">
					<table id="contentAreaTable" height="100%" width="100%">
						<tr>
							<td id="leftPane"><iframe name="contentLeftFrame" height="100%" width="100%"
								scrolling="NO" frameborder="0"
								src="faces/workload/workloadFile.jsp" marginheight="0"
								marginwidth="0"></iframe></td>
						</tr>
					</table>
					</div>
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
</f:view>
</body>
</html>
