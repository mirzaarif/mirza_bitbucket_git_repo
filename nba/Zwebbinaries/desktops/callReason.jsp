<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                    + "/";
        }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Call Reason</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=500;
		var height=240;

		function setTargetFrame() {
			document.forms['callReasonForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['callReasonForm'].target='';
			return false;
		}

	// function to set the focus on "ok" button, if enabled
	function setDefault(){
		try	{
				if(document.all['callReasonForm:focus'].innerHTML == '1'){
					document.forms[0].item('callReasonForm:searchCaller').focus();
				} else 	if(document.all['callReasonForm:focus'].innerHTML == '2'){
					document.forms[0].item('callReasonForm:searchInsured').focus();
				} else if(document.all['callReasonForm:focus'].innerHTML == '3') {
					document.forms[0].item('callReasonForm:ok').focus();
				}
			}
			catch(err){
				
			}

		}


	//-->
</script>

<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" onload="popupInit();setDefault();">
<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />

	<h:form id="callReasonForm">
		<h:outputText id="focus" style="display:none"
			value="#{pc_CallReason.focus}"></h:outputText>
		<h:outputText style="display:none;" value="#{pc_CallReason.refresh}"></h:outputText>
		<table width="100%" height="100%" border="0">
			<tbody>
				<tr>
					<td><h:outputText styleClass="textLabel"
						value="#{bundle.call_reason}">
					</h:outputText></td> <td align="right"><Help:Link
							contextId="Accel_Desktop_Menu" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><h:selectOneListbox
						id="quoteCategory" 
						style="width: 70%" size="6" 
						value="#{pc_CallReason.reason}">
						<f:selectItems value="#{pc_CallReason.reasons}" />
					</h:selectOneListbox></td>
				</tr>
				<tr>
					<td colspan="2"><h:outputText styleClass="textLabel"
						value="#{bundle.call_reason_caller}">
					</h:outputText> <h:commandButton id="searchCaller"
						styleClass="buttonTiny" value="#{bundle.search}"
						action="#{pc_CallReason.addCaller}" onclick="setTargetFrame();">
					</h:commandButton>&nbsp; <h:outputText styleClass="textLabel"
						value="#{pc_CallReason.callerName}">
					</h:outputText></td>
				</tr>
				<tr>
					<td colspan="2"><h:outputText styleClass="textLabel"
						value="#{bundle.call_reason_insured}">
					</h:outputText> <h:commandButton id="searchInsured"
						styleClass="buttonTiny" value="#{bundle.search}"
						action="#{pc_CallReason.addInsured}" onclick="setTargetFrame();">
					</h:commandButton>&nbsp; <h:outputText styleClass="textLabel"
						value="#{pc_CallReason.insuredName}">
					</h:outputText></td>
				</tr>
				<tr>
					<td align="right" colspan="2"><h:commandButton styleClass="button" value="OK"
						action="#{pc_CallReason.submit}" id="ok"
						onclick="setTargetFrame();" />
						<h:commandButton styleClass="buttonCancel"
						value="Cancel" id="cancel" action="#{pc_CallReason.cancelCallReason}"
						immediate="true" onclick="setTargetFrame();" />						
					</td>
				</tr>
			</tbody>
		</table>
		<div id="Messages" style="display: none"><h:messages></h:messages></div>
	</h:form>
</f:view>
</body>
</html>
