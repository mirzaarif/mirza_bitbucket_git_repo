
<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Claim Comments</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
	//-->
</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>

</head>
<body class="updatePanel">
<table cellspacing="0" cellpadding="0" width="100%">
	<tbody>
		<tr class="textTitleBar">
				<td>Comments</td>
		</tr>
		<tr>
			<td class="panelBody" height="113" ><textarea id="comments"
				class="textComments" cols="97" rows="10"></textarea></td>
		</tr>
	</tbody>
</table>
</body>
</html>
