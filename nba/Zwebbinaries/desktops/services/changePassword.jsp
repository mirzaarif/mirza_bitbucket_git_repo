<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Change Password</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=280;
		var height=140;


		var logonForm = top.mainContentFrame.window.document.forms[0];
		var user = logonForm.item("logonForm:usernamefld").value;
	
		function setUserName(){		
			if (user  != "" ) {
				document.forms[0].item("changePasswordForm:userID").value = user;
			}
		}
	//-->
	</script>
<script type="text/javascript">
	<!--
		function setTargetFrame() {
			document.forms['changePasswordForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['changePasswordForm'].target='';
			return false;
		}
	//-->
</script>
	
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="popupInit();setUserName();">
<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="changePasswordForm">
		<table>
			<tbody>
				<tr>
					<td class="textLabel"><h:outputText rendered="true"
						value="#{bundle.changepassword_existingPassword}"></h:outputText></td>
					<td class="textInput"><h:inputSecret id="existingPassword"
						value="#{pc_ChangePassword.existingPassword}" required="true" maxlength="8"
						styleClass="textInput"></h:inputSecret></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText rendered="true"
						value="#{bundle.changepassword_newpassword}"></h:outputText></td>
					<td class="textInput"><h:inputSecret styleClass="textInput"
						id="newPassword" value="#{pc_ChangePassword.password}" 
						required="true" maxlength="8"></h:inputSecret></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText rendered="true"
						value="#{bundle.changepassword_confirmpassword}"></h:outputText></td>
					<td class="textInput"><h:inputSecret styleClass="textInput"
						id="confirmedPassword" maxlength="8"
						value="#{pc_ChangePassword.confirmedPassword}" required="true"></h:inputSecret></td>
				</tr>
				<tr>
					<td align="right" colspan="2"><h:commandButton id="ok"
						styleClass="button" value="#{bundle.ok}"
						action="#{pc_ChangePassword.submit}" onclick="setTargetFrame();">
					</h:commandButton>
					<h:commandButton id="Cancel"
						styleClass="buttonCancel" value="#{bundle.cancel}" immediate="true"
						action="#{pc_ChangePassword.cancel}" onclick="setTargetFrame();"/></td>
				</tr>
				<tr>
					<td>
							<h:inputHidden id="userID" value="#{pc_ChangePassword.userID}"></h:inputHidden>
					</td>
				</tr>
			</tbody>
		</table>

	</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>
</f:view>
</body>
</html>
