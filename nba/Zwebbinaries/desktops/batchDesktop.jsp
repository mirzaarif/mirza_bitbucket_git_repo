
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv" %>

<%	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=	basePath%>" target="controlFrame">
<title>Call Center</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
			function initialize(){
				top.loadMenuBar('<%= path%>','/faces/desktops/menus/globalMenu.jsp');
				top.loadContextBar('<%=	path%>','/faces/desktops/menus/batchDesktop.jsp');
				top.setContentAreaSize();
			}
	
	//-->
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>

<body class="desktopPanelBody" onload="initialize();">
<f:view>
	<h:form id="callCenterForm">
		<f:loadBundle var="bundle"
			basename="com.csc.fs.accel.ui.config.ApplicationData" />
			<div style="display:none">
				<h:outputText value="#{pc_BatchDesktop.refresh}"></h:outputText>
			</div>
		<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr class="menubar" id="menuBar">
					<td colspan="3" align="right">
					<iframe style="height: 22px; width: 100%;" id="desktopMenu" name="desktopMenu" src="initializingMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
				</tr>
				<tr class="contextBar" id="contextBar">
					<td id="typeMenuContainer" align="left" width="200">
						<h:graphicImage value="#{pc_BatchDesktop.desktopImage}" style="vertical-align: middle;"></h:graphicImage> <h:outputText value="#{pc_BatchDesktop.displayDesktopType}"></h:outputText> |
					</td>
					<td align="left" valign="bottom" id="contextMenuContainer">
						<iframe height="22px" width="100%" id="contextMenu" name="contextMenu" src="initializingContextMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
					<td id="dateMenuContainter" align="right" width="200">
						<div id="CurrentDate">
						</div>
					</td>
				</tr>
				<tr class="desktopBody"  style="width:100%;height:100%;" >
					<td colspan="3">
					<div id="contentArea">
					<table id="contentAreaTable" height="100%" width="100%">
						<tbody>
							<tr>
								<td id="leftPane"><iframe  name="contentLeftFrame" height="100%" width="100%"
									scrolling="NO" frameborder="0"
									src="faces/batch/batchFile.jsp" marginheight="0"
									marginwidth="0"></iframe></td>
								<td width="8"></td>
								<td id="rightPane" width="1">
								</td>
							</tr>
						</tbody>
					</table>
					</div>
					</td>
				</tr>
			</tbody>
		</table>

		<div id="utilMenu" class="menuUtilities" onmouseout="timer(0);">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="menuItem"
						onmouseover="onSubMenuOver(this, document.all['callCenterForm:helpMenuItem']);"
						onmouseout="onSubMenuOut(this, document.all['callCenterForm:helpMenuItem']);"
						onclick="top.showAccelHelp(null); return false;"><a href="#"
						onclick="top.showAccelHelp(null); return false;" class="menuItem"
						id="callCenterForm:helpMenuItem"> <img src="images/help.gif"
						style='cursor: hand ! IMPORTANT; border: none' align="middle" />
					<h:outputText rendered="true" value="#{bundle.utilities_menu_help}">
					</h:outputText> </a></td>
				</tr>
			</tbody>
		</table>
		</div>
	</h:form>
</f:view>
</body>
</html>
