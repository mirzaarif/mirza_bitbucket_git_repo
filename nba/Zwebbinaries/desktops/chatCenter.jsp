
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Call Center</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		function initialize(){
			try{
				top.loadMenuBar('<%= path%>','/faces/desktops/menus/globalMenu.jsp');
				top.loadContextBar('<%=	path%>','/faces/desktops/menus/chatCenter.jsp');
				top.setContentAreaSize();
				top.showWindow('<%=path%>','faces/chat/chat.jsp', this);
			}catch(err){
			}
		}

		function reloadLeft(){
		    contentLeftFrame.location.href = '<%=path%>/faces/customer/customerFile.jsp';
		}
	
	//-->
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>

<body class="desktopPanelBody"
	onload="initialize();">
<f:view>
	<h:form id="callCenterForm">
		<f:loadBundle var="bundle"
			basename="com.csc.fs.accel.ui.config.ApplicationData" />
			<div style="display:none">
				<h:outputText value="#{pc_CallCenter.refresh}"></h:outputText>
			</div>
		<table class="desktopTable" cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr class="menubar" id="menuBar">
					<td colspan="3" align="right">
					<iframe style="height: 22px; width: 100%;" id="desktopMenu" name="desktopMenu" src="initializingMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
				</tr>
				<tr class="contextBar" id="contextBar">
					<td id="typeMenuContainer" align="left" width="250">
						<h:graphicImage value="#{pc_CallCenter.desktopImage}" style="vertical-align: middle;"></h:graphicImage><h:outputText value="#{pc_CallCenter.displayDesktopType}"></h:outputText> | <span id="CallTime"></span> |
					</td>
					<td align="left" valign="bottom" id="contextMenuContainer">
						<iframe height="22px" width="100%" id="contextMenu" name="contextMenu" src="initializingContextMenu.html" frameborder="0" scrolling="no"></iframe>
					</td>
					<td id="dateMenuContainter" align="right" width="200">
						<div id="CurrentDate">
						</div>
					</td>
				</tr>
				<tr class="desktopBody" >
					<td colspan="3">
					<div id="contentArea">
					<table id="contentAreaTable" height="100%" width="100%">
						<tbody>
							<tr>
								<td id="leftPane"><iframe  name="contentLeftFrame" height="100%" width="100%"
									scrolling="NO" frameborder="0"
									src="faces/customer/customerFile.jsp" marginheight="0"
									marginwidth="0"></iframe></td>
								<td width="8"></td>
								<td id="rightPane"><iframe name="contentRightFrame" height="100%" width="100%"
									scrolling="NO" frameborder="0" src="desktops/blank.html"
									marginheight="0" marginwidth="0"></iframe></td>
							</tr>
						</tbody>
					</table>
					</div>
					</td>
				</tr>
			</tbody>
		</table>

		<div id="utilMenu" class="menuUtilities" onmouseout="timer(0);">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="menuItem"
						onmouseover="onSubMenuOver(this,document.all['callCenterForm:formsMenuItem']);"
						onmouseout="onSubMenuOut(this,document.all['callCenterForm:formsMenuItem']);"><h:commandLink
						value="" action="#{pc_CallCenter.forms}" styleClass="menuItem" onmouseover="this.isClicked=false;"
						id="formsMenuItem">
						<h:outputText rendered="true"
							value="#{bundle.utilities_menu_forms}">
						</h:outputText>
					</h:commandLink></td>
				</tr>
				<tr>
					<td class="menuItem"
						onmouseover="onSubMenuOver(this, document.all['callCenterForm:productMenuItem']);"
						onmouseout="onSubMenuOut(this,document.all['callCenterForm:productMenuItem']);"
						onclick="top.showAccelInfoBrowser(null);return false;"><a href="#"
						onclick="top.showAccelInfoBrowser(null);return false;"
						class="menuItem" id="callCenterForm:productMenuItem"> <h:outputText
						rendered="true" value="#{bundle.utilities_menu_product}">
					</h:outputText> </a></td>
				</tr>
				<tr>
					<td class="menuItem"
						onmouseover="onSubMenuOver(this, document.all['callCenterForm:reportsMenuItem']);"
						onmouseout="onSubMenuOut(this, document.all['callCenterForm:reportsMenuItem']);"
						onclick="top.showAccelReports(); return false;"><a href="#"
						onclick="top.showAccelReports(); return false;" class="menuItem"
						id="callCenterForm:reportsMenuItem"> <h:outputText rendered="true"
						value="#{bundle.utilities_menu_reports}">
					</h:outputText> </a></td>
				</tr>
			</tbody>
		</table>
		</div>

		
	</h:form>
</f:view>
</body>
</html>
