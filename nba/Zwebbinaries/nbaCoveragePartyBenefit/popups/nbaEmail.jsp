<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="Email" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=425;
				var height=150; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_address'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_address'].target='';
					return false;
				}
				function refreshOnAddNew() {
					refresh =  document.forms['form_address']['form_address:refreshOnAddNew'].value;
					if (refresh == "true") {
						top.mainContentFrame.contentRightFrame.file.location.href = top.mainContentFrame.contentRightFrame.file.location.href;
					}
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();refreshOnAddNew()">
			<h:form id="form_address" >
				<h:panelGroup styleClass="entryLinePadded" style="padding-top:20px">
					<h:outputLabel id="typeLabel" value="#{property.covPartyEmailType}" styleClass="entryLabel" style="width: 100px;"/>
					<h:selectOneMenu id="typeDD" value="#{pc_covPartyEmail.typeCode}" styleClass="entryFieldLong" style="width: 275px;" >
						<f:selectItems value="#{pc_covPartyEmail.typeList}"/>
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="attnLine" value="#{property.covPartyEmailAddress}" styleClass="entryLabel" style="width: 100px;"/>
					<h:inputText id="homePhoneEF" value="#{pc_covPartyEmail.address}" styleClass="formEntryText" style="width: 275px;" />
				</h:panelGroup>
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="cancel" value="#{property.buttonCancel}"
									action="#{pc_covPartyEmail.actionCancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="clear" value="#{property.buttonClear}"
									action="#{pc_covPartyEmail.actionClear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
					<h:commandButton id="update" value="#{property.buttonUpdate}"
						action="#{pc_covPartyEmail.actionUpdate}" onclick="setTargetFrame();" styleClass="buttonRight" 
						rendered="#{pc_covPartyEmail.update}" />
					<h:commandButton id="add" value="#{property.buttonAdd}"
						action="#{pc_covPartyEmail.actionAdd}" onclick="setTargetFrame();" styleClass="buttonRight-1" 
						rendered="#{!pc_covPartyEmail.update}"/>
					<h:commandButton id="addNew" value="#{property.buttonAddNew}"
						action="#{pc_covPartyEmail.actionAddNew}" onclick="resetTargetFrame();" styleClass="buttonRight" 
						rendered="#{!pc_covPartyEmail.update}"/>
				</h:panelGroup>
				<h:inputHidden id="refreshOnAddNew" value="#{pc_covPartyEmail.refreshBackground}" /> 
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
		</body>
	</f:view>
</html>