<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_NEW_BENE_DIST" value="#{pc_covPartyBeneDist}" />
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="Beneficiary Distribution" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=250; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_pc_covPartyBeneDist'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_pc_covPartyBeneDist'].target='';
					return false;
				}
				function clearRadio() {
					zip = document.forms['form_pc_covPartyBeneDist']['form_pc_covPartyBeneDist:zipCode'];
					postal = document.forms['form_pc_covPartyBeneDist']['form_pc_covPartyBeneDist:postalCode'];
					zip.value = "";
					postal.value = "";
					return false;
				}
				function refreshOnAddNew() {
					refresh =  document.forms['form_pc_covPartyBeneDist']['form_pc_covPartyBeneDist:refreshOnAddNew'].value;
					if (refresh == "true") {
						top.mainContentFrame.contentRightFrame.file.location.href = top.mainContentFrame.contentRightFrame.file.location.href;
					}
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();refreshOnAddNew()">
			<h:form id="form_pc_covPartyBeneDist" >
				<h:panelGroup id="PGIrevokable" styleClass="entryLinePadded"  style="margin-bottom:-20px" rendered="#{pc_covPartyBeneDist.vantageBESSystem}">
					<h:selectBooleanCheckbox value="#{pc_covPartyBeneDist.irrevokableInd}" styleClass="formEntryCheckbox"  style="margin-left: 125px;" />
					<h:outputLabel id="clientIrrevokableLabel" value="Irrevokable" styleClass="formLabelRight" />
				</h:panelGroup>
				<h:panelGroup id="PGbeneficiary" styleClass="entryLinePadded" style="padding-top:20px">
					<h:outputLabel id="clientBeneficiaryHdrCol1" value="#{property.covPartyBeneficiary}" styleClass="entryLabel" />
					<h:selectOneMenu id="beneficaryDD" value="#{pc_covPartyBeneDist.ID}"
						valueChangeListener="#{pc_covPartyBeneDist.beneficiaryValChange}" 
						styleClass="entryFieldLong">
						<f:selectItems id="beneficiaryList" value="#{pc_covPartyBeneDist.beneficiaryList}" />
					</h:selectOneMenu> 
				</h:panelGroup>
				<h:panelGroup id="pgRelationship" styleClass="entryLinePadded">
					<h:outputLabel id="clientBeneficiaryHdrCol2" value="#{property.covPartyBeneRelationship}" styleClass="entryLabel" />
					<h:selectOneMenu id="RelationshipDD" value="#{pc_covPartyBeneDist.relationship}" 
						styleClass="entryFieldLong"> 
						<f:selectItems id="relationshipList" value="#{pc_covPartyBeneDist.relationList}" />
				</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup  id="pgDistribution" styleClass="entryLinePadded">
					<h:outputLabel id="clientBeneficiaryHdrCol4" value="#{property.covPartyBeneDist}" styleClass="entryLabel" />
					<h:selectOneMenu id="distDD" value="#{pc_covPartyBeneDist.distributionType}" 
						valueChangeListener="#{pc_covPartyBeneDist.distributionValChange}" 
						disabled="#{pc_covPartyBeneDist.contingent}" 
						styleClass="entryFieldLong"> 
						<f:selectItems id="distList" value="#{pc_covPartyBeneDist.distributionTypeList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup id="pgDistAmount" styleClass="entryLinePadded">
					<h:outputLabel id="clientBeneficiaryAmt" value="#{property.covPartyBeneAmount}" 
						styleClass="entryLabel" rendered="#{!pc_covPartyBeneDist.percent}" />
					<h:outputLabel id="clientBeneficiaryPercent" value="#{property.covPartyBenePercent}" 
						styleClass="entryLabel" rendered="#{pc_covPartyBeneDist.percent}"/>
					<h:inputText id="clientBeneficiaryCol5Amount" value="#{pc_covPartyBeneDist.interestAmount}" 
						styleClass="entryField"
						rendered="#{!pc_covPartyBeneDist.percent}"
						disabled="#{!pc_covPartyBeneDist.amount}">
						<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
					</h:inputText>
					<h:inputText id="clientBeneficiaryCol5Percent" value="#{pc_covPartyBeneDist.interestPercent}" 
						styleClass="entryField"
						rendered="#{pc_covPartyBeneDist.percent}" 
						immediate="true" >
						<f:convertNumber type="percent" pattern="###.##" /> 
					</h:inputText>
				</h:panelGroup>
				
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="cancel" value="#{property.buttonCancel}"
									action="#{pc_covPartyBeneDist.actionCancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="clear" value="#{property.buttonClear}"
									action="#{pc_covPartyBeneDist.actionClear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
					<h:commandButton id="update" value="#{property.buttonUpdate}"
						action="#{pc_covPartyBeneDist.actionUpdate}" onclick="setTargetFrame();" styleClass="buttonRight" 
						rendered="#{pc_covPartyBeneDist.update}" />
					<h:commandButton id="add" value="#{property.buttonAdd}"
						action="#{pc_covPartyBeneDist.actionAdd}" onclick="setTargetFrame();" styleClass="buttonRight-1" 
						rendered="#{!pc_covPartyBeneDist.update}"/>
					<h:commandButton id="addNew" value="#{property.buttonAddNew}"
						action="#{pc_covPartyBeneDist.actionAddNew}" onclick="resetTargetFrame();" styleClass="buttonRight" 
						rendered="#{!pc_covPartyBeneDist.update}"/>
				</h:panelGroup>
				<h:inputHidden id="refreshOnAddNew" value="#{pc_covPartyBeneDist.refreshBackground}" />  <!-- SPR3512 -->
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
			
		</body>
	</f:view>