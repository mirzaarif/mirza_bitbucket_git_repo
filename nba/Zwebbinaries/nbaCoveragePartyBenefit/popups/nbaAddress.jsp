<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="Address" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=400; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_address'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_address'].target='';
					return false;
				}
				function clearRadio() {
					zip = document.forms['form_address']['form_address:zipCode'];
					postal = document.forms['form_address']['form_address:postalCode'];
					zip.value = "";
					postal.value = "";
					return false;
				}
				function refreshOnAddNew() {
					refresh =  document.forms['form_address']['form_address:refreshOnAddNew'].value;
					if (refresh == "true") {
						top.mainContentFrame.contentRightFrame.file.location.href = top.mainContentFrame.contentRightFrame.file.location.href;
					}
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();refreshOnAddNew()">
			<h:form id="form_address" >
				<h:panelGroup styleClass="entryLinePadded" style="padding-top:20px">
					<h:outputLabel id="typeLabel" value="#{property.covPartyAddrType}" styleClass="entryLabel" />
					<h:selectOneMenu id="typeDD" value="#{pc_covPartyAddress.addressTypeCode}" styleClass="entryFieldLong" >
						<f:selectItems value="#{pc_covPartyAddress.addressTypeList}"/>
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="attnLine" value="#{property.covPartyAddrAttn}" styleClass="entryLabel" />
					<h:inputText id="itdrAttnLine1" value="#{pc_covPartyAddress.attentionLine}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLine" style="padding-bottom:0px">
					<h:outputLabel id="oldrAddr" value="#{property.covPartyAddrAddress}" styleClass="entryLabel" />
					<h:inputText id="itdrAddrLine1" value="#{pc_covPartyAddress.addressLine1}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePaddedNoLabel" style="padding-bottom:0px">
					<h:inputText id="itdrAddrLine2" value="#{pc_covPartyAddress.addressLine2}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePaddedNoLabel" style="padding-bottom:0px">
					<h:inputText id="itdrAddrLine3" value="#{pc_covPartyAddress.addressLine3}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePaddedNoLabel" style="padding-bottom:0px" rendered="#{pc_covPartyAddress.vantageBESSystem}">
					<h:inputText id="itdrAddrLine4" value="#{pc_covPartyAddress.addressLine4}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded" style="padding-top:5px">
					<h:outputLabel id="oldrAddrCity" value="#{property.covPartyAddrCity}" styleClass="entryLabel" />
					<h:inputText id="itdrAddrCity" value="#{pc_covPartyAddress.city}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="oldrState" value="#{property.covPartyAddrState}" styleClass="entryLabel" />
					<h:selectOneMenu id="itdrState" value="#{pc_covPartyAddress.state}" styleClass="entryFieldLong" >
						<f:selectItems value="#{pc_covPartyAddress.stateList}"/>
					</h:selectOneMenu>
				</h:panelGroup>

				<h:panelGrid columns="3" styleClass="tableLinePadded" columnClasses="tableColumnLabel,tableColumn100,tableColumn150" cellspacing="0" cellpadding="0">
					<h:column>
						<h:outputLabel id="oldrZipCode" value="#{property.appEntCode}" styleClass="entryLabelTop" />
					</h:column>
					<h:column>
						<h:selectOneRadio id="sordrZipCode" 
							value="#{pc_covPartyAddress.zipTC}" 
							layout="pageDirection" 
							valueChangeListener="#{pc_covPartyAddress.zipTCChange}"
							onclick="clearRadio();submit()" 
							immediate="true"  
							styleClass="radioMenu">
								<f:selectItems value="#{pc_covPartyAddress.zipTypeCodes}"/>
						</h:selectOneRadio>
					</h:column>
					<h:column>
						<h:panelGroup>
							<h:inputText id="zipCode" value="#{pc_covPartyAddress.zip}" styleClass="entryFieldShort" disabled="#{pc_covPartyAddress.zipDisabled}" > 
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> 
							</h:inputText> 
							<h:inputText id="postalCode" value="#{pc_covPartyAddress.postal}" styleClass="entryFieldShortRadioPage" disabled="#{pc_covPartyAddress.postalDisabled}" >
								<f:convertNumber type="postal" pattern="#{property.postalPattern}"/> 
							</h:inputText> 
						</h:panelGroup>
					</h:column>
				</h:panelGrid>
				<h:panelGroup id="pgaCountry" styleClass="entryLinePadded" style="width:100%">
					<h:outputLabel id="countryLabel" value="Country:" styleClass="entryLabel" />
					<h:selectOneMenu id="countryDD" value="#{pc_covPartyAddress.country}" styleClass="entryFieldLong" >
						<f:selectItems value="#{pc_covPartyAddress.countryList}"/>
					</h:selectOneMenu>			
				</h:panelGroup>
				<h:panelGroup id="pgaCounty" styleClass="entryLinePadded" style="width:100%">
					<h:outputLabel id="CountyLabel" value="County:" styleClass="entryLabel" />
					<h:inputText id="county" value="#{pc_covPartyAddress.county}" styleClass="entryFieldLong"/>
											
				</h:panelGroup>
				<h:panelGroup id="pgaddressStart" styleClass="entryLinePadded" style="width:100%">
					<h:outputLabel id="startDate" value="Start Date:" styleClass="entryLabel" />
					<h:inputText id="clientContactInfoDate" value="#{pc_covPartyAddress.startDate}" styleClass="entryFieldShort">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>						
				</h:panelGroup>
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="cancel" value="#{property.buttonCancel}"
									action="#{pc_covPartyAddress.actionCancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="clear" value="#{property.buttonClear}"
									action="#{pc_covPartyAddress.actionClear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
					<h:commandButton id="update" value="#{property.buttonUpdate}"
						action="#{pc_covPartyAddress.actionUpdate}" onclick="setTargetFrame();" styleClass="buttonRight" 
						rendered="#{pc_covPartyAddress.update}" />
					<h:commandButton id="add" value="#{property.buttonAdd}"
						action="#{pc_covPartyAddress.actionAdd}" onclick="setTargetFrame();" styleClass="buttonRight-1" 
						rendered="#{!pc_covPartyAddress.update}"/>
					<h:commandButton id="addNew" value="#{property.buttonAddNew}"
						action="#{pc_covPartyAddress.actionAddNew}" onclick="resetTargetFrame();" styleClass="buttonRight" 
						rendered="#{!pc_covPartyAddress.update}"/>
				</h:panelGroup>
				<h:inputHidden id="refreshOnAddNew" value="#{pc_covPartyAddress.refreshBackground}" /> 
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
		</body>
	</f:view>