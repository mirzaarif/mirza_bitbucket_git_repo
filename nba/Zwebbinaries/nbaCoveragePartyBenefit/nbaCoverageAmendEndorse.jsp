<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaCoverageAmendEndorse.faces';
		
		function setTargetFrame() {			  
			document.forms['form_AmendEndorse'].target='controlFrame'; 
			return false;
		}
		function resetTargetFrame() {			 
			document.forms['form_AmendEndorse'].target=''; 
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_AmendEndorse']['form_AmendEndorse:draftChanges'].value;  //NBA213
		}		
	</script>
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_AMEND_ENDORSE" value="#{pc_NbaAmendEndorse}" />
	<h:form id="form_AmendEndorse" > 
		<!-- ********************************************************
			coverage Table (optional)
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/nbaCoveragePartyBenefit/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		
		<!-- ********************************************************
			Amendment Endorsement fields
		 ******************************************************** -->
		<f:subview id="amend_endorse_area">
			<c:import url="/uw/subviews/NbaAmendEndorse.jsp" />
		</f:subview>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  
		</f:subview>
		</h:form>
		
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
