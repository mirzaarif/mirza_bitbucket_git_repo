<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID-->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	
	<h:panelGroup id="personalDetailsGroup" >
		<h:panelGroup id="personDetailsSubSection1" styleClass="formDataDisplayTopLine" style="">
			<h:outputLabel id="orgName" value="#{property.covPartyName}" styleClass="formLabel" />
			<h:inputText id="OrgNameText" value="#{pc_NbaClientDetails.name}" styleClass="formEntryText" style="width: 450px" />
		</h:panelGroup>		
		<h:panelGroup id="rolesGoup">
			<h:outputLabel id="roles" value="#{property.covPartyRoles}" styleClass="formLabel" style="vertical-align: top;width:125px; margin-top:10px" />
				
			<h:selectManyListbox id="rolesList" styleClass="formEntryTextHalf"
				value="#{pc_NbaClientDetails.roles}" style="height: 180px; width: 185px; margin-top:10px">
					<f:selectItems value="#{pc_NbaClientDetails.rolesList}" />
			</h:selectManyListbox>
		</h:panelGroup>
		
		<h:panelGrid id="pinsGovtIdPGrid" columnClasses="formDataEntryLineAppEntry" columns="3" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputLabel id="taxID" value="Tax Information:" styleClass="formLabel" style="margin-bottom:45px"/>
			</h:column>
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_NbaClientDetails.govtTC}" 
					layout="pageDirection" styleClass="radioLabel" style="width: 151px;margin-left:-5px" 
					immediate="true"> <!-- NBA340 -->
					<f:selectItem id="ssnRB" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
					<f:selectItem id="sinRB" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
					<f:selectItem id="TinRB" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
				</h:selectOneRadio>
			</h:column>
			<h:column>
			<!-- begin NBA340 -->
				<h:inputText id="govtIdInput" value="#{pc_NbaClientDetails.govtId}" styleClass="formEntryText"
							style="width: 115px;margin-top: 0px;margin-bottom:45px;" maxlength="9" /> 
				<h:inputHidden id="govtIdKey" value="#{pc_NbaClientDetails.govtIdKey}"></h:inputHidden>		
			<!-- end NBA340 -->
			</h:column>		
		</h:panelGrid>	

		<h:panelGroup id="verificationcol2" styleClass="formDataDisplayLine" style="margin-bottom:1px">
			<h:outputLabel id="verification" value="Verification:" styleClass="formLabel" />
			<h:selectOneMenu id="verificationDD" value="#{pc_NbaClientDetails.taxIDVerification}" styleClass="formEntryText" style="width: 260px;">
				<f:selectItems id="verificationList" value="#{pc_NbaClientDetails.taxIDVerificationList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="personDetailsSubSection8" styleClass="formDataDisplayLine" style="margin-bottom:15px">
			<h:outputLabel id="personWithholding" value="#{property.personWithholding}" styleClass="formLabel" />
			<h:selectOneMenu id="personWithholdingDD" value="#{pc_NbaClientDetails.withholding}" styleClass="formEntryText" style="width: 185px;"> 
				<f:selectItems id="personWithholdingList" value="#{pc_NbaClientDetails.withholdingList}" />
			</h:selectOneMenu>	
		</h:panelGroup>	
				
	</h:panelGroup>	
</jsp:root>


