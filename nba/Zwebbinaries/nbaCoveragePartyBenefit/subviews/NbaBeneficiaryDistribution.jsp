<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="createUpdateCoverageArea" styleClass="inputFormMat">
		<h:panelGroup id="updateBeneDistForm" styleClass="inputForm" style="height: 200px;"> 
			<h:outputLabel id="updateContractTitle" value="Update Beneficiary Distribution" styleClass="formTitleBar" />
			<h:panelGroup id="PGType" styleClass="formDataEntryLine" style="margin-top:20px;">
					<h:outputLabel id="clientBeneficiaryTypeLablel" value="Type:" styleClass="formLabel" />
					<h:outputText id="clientBeneficiaryTypeText"  value="#{pc_covPartyBeneDist.type}"  styleClass="formEntryText" />
			</h:panelGroup>
			<h:panelGroup id="PGCoverage" styleClass="formDataEntryLine"  rendered="#{pc_covPartyBeneDist.vantageBESSystem}">
					<h:outputLabel id="clientBeneficiaryHdrCov1" value="Coverage:" styleClass="formLabel" />
					<h:selectOneMenu id="coveragteDD" value="#{pc_covPartyBeneDist.coverageID}" styleClass="formEntryText" style="width: 350px;">
						<f:selectItems id="coverageList" value="#{pc_covPartyBeneDist.coverageList}" />
					</h:selectOneMenu> 
			</h:panelGroup>
				<h:panelGroup id="PGIrevokable" styleClass="formDataEntryLine" rendered="#{pc_covPartyBeneDist.vantageBESSystem}">
				<h:selectBooleanCheckbox value="#{pc_covPartyBeneDist.irrevokableInd}" styleClass="formEntryCheckbox"  style="margin-left: 125px;" />
				<h:outputLabel id="clientIrrevokableLabel" value="Irrevokable" styleClass="formLabelRight" />
			</h:panelGroup>
			<h:panelGroup id="pgRelationship" styleClass="formDataEntryLine">
				<h:outputLabel id="clientBeneficiaryHdrCol2" value="#{property.covPartyBeneRelationship}" styleClass="formLabel" />
				<h:selectOneMenu id="RelationshipDD" value="#{pc_covPartyBeneDist.relationship}" 
					styleClass="formEntryText" 
					style="width: 350px;"> 
					<f:selectItems id="relationshipList" value="#{pc_covPartyBeneDist.relationList}" />
			</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup  id="pgDistribution" styleClass="formDataEntryLine">
				<h:outputLabel id="clientBeneficiaryHdrCol4" value="#{property.covPartyBeneDist}" styleClass="formLabel" />
				<h:selectOneMenu id="distDD" value="#{pc_covPartyBeneDist.distributionType}" 
					valueChangeListener="#{pc_covPartyBeneDist.distributionValChange}"  
					disabled="#{pc_covPartyBeneDist.contingent}"
					styleClass="formEntryText" 
					style="width: 350px;"> 
					<f:selectItems id="distList" value="#{pc_covPartyBeneDist.distributionTypeList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="pgDistAmount" styleClass="formDataEntryLine">
				<h:outputLabel id="clientBeneficiaryAmt" value="#{property.covPartyBeneAmount}" 
					styleClass="formLabel" rendered="#{!pc_covPartyBeneDist.percent}" />
				<h:outputLabel id="clientBeneficiaryPercent" value="#{property.covPartyBenePercent}" 
					styleClass="formLabel" rendered="#{pc_covPartyBeneDist.percent}"/>
				<h:inputText id="clientBeneficiaryCol5Amount" value="#{pc_covPartyBeneDist.interestAmount}" 
					styleClass="formEntryText" 
					rendered="#{!pc_covPartyBeneDist.percent}"
					disabled="#{!pc_covPartyBeneDist.amount}">
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
				</h:inputText>
				<h:inputText id="clientBeneficiaryCol5Percent" value="#{pc_covPartyBeneDist.interestPercent}" 
					styleClass="formEntryText" 
					rendered="#{pc_covPartyBeneDist.percent}" 
					immediate="true" >
					<f:convertNumber type="percent" pattern="###.##" /> 
				</h:inputText>
			</h:panelGroup>

			<h:panelGroup styleClass="formButtonBar" style="margin-top: 20px;">
				<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
					action="#{pc_coveragePartyNavigation.actionCloseSubView}" immediate="true"  />
				<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
					action="#{pc_covPartyBeneDist.actionClear}" />
				<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}"  styleClass="formButtonRight" action="#{pc_covPartyBeneDist.actionUpdate}" />
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>