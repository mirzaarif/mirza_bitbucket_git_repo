<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<!-- ********************************************************
			Overview Navigation header - contains Client Icons and Images link
		 ******************************************************** -->	
	<h:panelGroup styleClass="pageHeader" rendered="#{pc_coveragePartyNavigation.overview}">
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon1}" title="#{pc_coveragePartyNavigation.icon1Title}"
			action="#{pc_coveragePartyNavigation.selectIcon1}" rendered="#{pc_coveragePartyNavigation.icon1Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon2}" title="#{pc_coveragePartyNavigation.icon2Title}"
			action="#{pc_coveragePartyNavigation.selectIcon2}" rendered="#{pc_coveragePartyNavigation.icon2Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon3}" title="#{pc_coveragePartyNavigation.icon3Title}"
			action="#{pc_coveragePartyNavigation.selectIcon3}" rendered="#{pc_coveragePartyNavigation.icon3Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon4}" title="#{pc_coveragePartyNavigation.icon4Title}"
			action="#{pc_coveragePartyNavigation.selectIcon4}" rendered="#{pc_coveragePartyNavigation.icon4Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon5}" title="#{pc_coveragePartyNavigation.icon5Title}"
			action="#{pc_coveragePartyNavigation.selectIcon5}" rendered="#{pc_coveragePartyNavigation.icon5Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon6}" title="#{pc_coveragePartyNavigation.icon6Title}"
			action="#{pc_coveragePartyNavigation.selectIcon6}" rendered="#{pc_coveragePartyNavigation.icon6Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon7}" title="#{pc_coveragePartyNavigation.icon7Title}"
			action="#{pc_coveragePartyNavigation.selectIcon7}" rendered="#{pc_coveragePartyNavigation.icon7Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_coveragePartyNavigation.icon8}" title="#{pc_coveragePartyNavigation.icon8Title}"
			action="#{pc_coveragePartyNavigation.selectIcon8}" rendered="#{pc_coveragePartyNavigation.icon8Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 200px" rendered="#{pc_coveragePartyNavigation.auth.visibility['Images']}"/><!-- FNB011 --> 
		<h:commandButton image="images/link_icons/documents-stack.gif"
			rendered="#{pc_coveragePartyNavigation.auth.visibility['Images']}"
			onclick="setTargetFrame();" action="#{pc_coveragePartyNavigation.viewAllSourcesForUW}"
			style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle" /> <!--  FNB011 -->
	</h:panelGroup>
	<!-- ********************************************************
			Subview Navigation header - contains back link and Images link
		 ******************************************************** -->
	<h:panelGroup styleClass="pageHeader" rendered="#{!pc_coveragePartyNavigation.overview}">
		<h:panelGroup styleClass="pageHeader">
			<h:commandLink value="#{property.backTo}" styleClass="phText" style="position: absolute; left: 20px"
						action="#{pc_coveragePartyNavigation.backToOverview}" />
			<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 200px"
						rendered="#{pc_coveragePartyNavigation.auth.visibility['Images']}" />  <!-- FNB011 -->
			<h:commandButton image="images/link_icons/documents-stack.gif"
						style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"
						rendered="#{pc_coveragePartyNavigation.auth.visibility['Images']}"
						action="#{pc_coveragePartyNavigation.viewAllSourcesForUW}"
						onclick="setTargetFrame();" />  <!-- FNB011 -->
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
