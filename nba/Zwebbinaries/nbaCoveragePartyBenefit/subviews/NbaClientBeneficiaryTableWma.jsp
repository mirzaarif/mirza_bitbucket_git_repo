<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-813      NB-1501     The Final Disposition Tab And Coverage Party Business Function Receive White Screen On Selecting or Updating The Beneficiary Table -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="wmAclientBeneficiaryTitleHeader" styleClass="sectionHeader" style="margin-top:20px">
		<h:outputLabel id="wmAclientBeneficiaryTableTitle" value="#{property.covPartyBeneficiaryTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	
	<!-- Table Column Headers -->
	<h:panelGroup id="wmAclientBeneficiaryHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText165,ovColHdrText125,ovColHdrText150,ovColHdrText75,ovColHdrText75" cellspacing="0">
			<h:outputLabel id="wmAclientBeneficiaryHdrCol1" value="" styleClass="ovColSortedFalse" />
			<h:outputLabel id="wmAclientBeneficiaryHdrCol2" value="#{property.clientBeneficiaryCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="wmAclientBeneficiaryHdrCol3" value="#{property.clientBeneficiaryCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="wmAclientBeneficiaryHdrCol4" value="#{property.clientBeneficiaryCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="wmAclientBeneficiaryHdrCol5" value="#{property.clientBeneficiaryCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="wmAclientBeneficiaryHdrCol6" value="#{property.clientBeneficiaryCol5}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="wmAclientBeneficiaryTablePG" styleClass="ovDivTableData"  style="height: 150px;"> 
		<h:dataTable id="wmAclientBeneficiaryTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientBeneficiaryTableData.htmlDataTable}"
			value="#{pc_NbaClientBeneficiaryTableData.clientBeneficiaryRows}" var="nbaBeneDist" rowClasses="#{pc_NbaClientBeneficiaryTableData.rowStyles}"
			columnClasses="ovColIcon,ovColText165,ovColText125,ovColText150,ovColText75,ovColText75">
			<h:column>
				<h:selectBooleanCheckbox value="#{nbaBeneDist.irrevokableInd}"  title="Irrevokable"
				valueChangeListener="#{nbaBeneDist.valChange}"
				onclick="getScrollXY('form_ContactDetails');submit()" immediate="true"
				styleClass="ovFullCellSelectCheckBox" />
			</h:column>
			
			<h:column>
				<h:commandLink id="wmAclientBeneficiaryCol2" value="#{nbaBeneDist.name}" styleClass="ovFullCellSelect" 
					immediate="true" />
			</h:column>
			<h:column>
				<h:selectOneMenu id="wmARelationshipDD" value="#{nbaBeneDist.relationship}" 
						valueChangeListener="#{nbaBeneDist.valChange}"
						onmousedown="getScrollXY('form_ContactDetails');"
						styleClass="formEntryText" style="width: 125px;"> 
						<f:selectItems id="wmArelationshipList" value="#{nbaBeneDist.relationList}" />
				</h:selectOneMenu>
			</h:column>
			<h:column>
				<h:commandLink id="wmAclientBeneficiaryCol4" value="#{nbaBeneDist.typeDescription}" styleClass="ovFullCellSelect" 
					immediate="true" />
			</h:column>
			<h:column>
				<h:selectOneMenu id="wmAdistDD" value="#{nbaBeneDist.distributionType}" 
						valueChangeListener="#{nbaBeneDist.distributionValChange}"
						onmousedown="getScrollXY('form_ContactDetails');"
						rendered = "#{!nbaBeneDist.contingent}"
						styleClass="formEntryText" style="width: 75px;"> 
						<f:selectItems id="wmAdistList" value="#{nbaBeneDist.distributionTypeList}" />
				</h:selectOneMenu>
				<h:outputText id="wmADistText" value="#{nbaBeneDist.distributionText}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneDist.contingent}" />
			</h:column>
			<h:column>
				<h:inputText id="wmAclientBeneficiaryCol6Amount" value="#{nbaBeneDist.interestAmount}" 
					valueChangeListener="#{nbaBeneDist.valChange}"
					onmousedown="getScrollXY('form_ContactDetails');"
					styleClass="ovFullCellSelect"
					style="margin-top: -2px;"
					rendered = "#{nbaBeneDist.amount}">  <!--SPRNBA-813 -->
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
				</h:inputText>
				<h:inputText id="wmAclientBeneficiaryCol5Percent" value="#{nbaBeneDist.interestPercent}" 
					valueChangeListener="#{nbaBeneDist.valChange}"
					onmousedown="getScrollXY('form_ContactDetails');"
					styleClass="ovFullCellSelect" style="margin-top: -2px;"
					rendered = "#{nbaBeneDist.percent}" 
					immediate="true" >  <!--SPRNBA-813 -->
					<f:convertNumber type="percent" pattern="###.##" /> 
				</h:inputText>
				<h:outputText id="wmAclientBeneficiaryCol5Text" value="#{nbaBeneDist.distributionText}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneDist.balanceEqualUnknown}" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup id="wmAclientBeneficiaryButtons" styleClass="ovButtonBar">
		<h:commandButton id="wmAbeneBtnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="setTargetFrame();"
			 action="#{pc_NbaClientBeneficiaryTableData.actionAddBeneficiary}"/>
	</h:panelGroup>
</jsp:root>