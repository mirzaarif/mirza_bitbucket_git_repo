<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="clientBeneficiaryTitleHeader" styleClass="sectionHeader">
		<h:outputLabel id="clientBeneficiaryTableTitle" value="#{property.covPartyBeneficiaryTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	
	<!-- Table Column Headers -->
	<h:panelGroup id="clientBeneficiaryHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" columnClasses="ovColHdrText165,ovColHdrText125,ovColHdrText150,ovColHdrText85,ovColHdrText85" cellspacing="0">
			<h:outputLabel id="clientBeneficiaryHdrCol1" value="#{property.clientBeneficiaryCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol2" value="#{property.clientBeneficiaryCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol3" value="#{property.clientBeneficiaryCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol4" value="#{property.clientBeneficiaryCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol5" value="#{property.clientBeneficiaryCol5}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="ovDivTableData"  style="height: 150px;"> 
		<h:dataTable id="clientBeneficiaryTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientBeneficiaryTableData.htmlDataTable}"
			value="#{pc_NbaClientBeneficiaryTableData.clientBeneficiaryRows}" var="nbaBeneficiary" rowClasses="#{pc_NbaClientBeneficiaryTableData.rowStyles}"
			columnClasses="ovColText165,ovColText125,ovColText150,ovColText85,ovColText85">
			<h:column>
				<h:commandLink id="clientBeneficiaryCol1" value="#{nbaBeneficiary.name}" styleClass="ovFullCellSelect" 
					immediate="true" />
			</h:column>
			<h:column>
				<h:selectOneMenu id="RelationshipDD" value="#{nbaBeneficiary.relationship}" 
						valueChangeListener="#{nbaBeneficiary.valChange}"
						onmousedown="getScrollXY('form_ContactDetails');"
						styleClass="formEntryText" style="width: 125px;"> 
						<f:selectItems id="relationshipList" value="#{nbaBeneficiary.relationList}" />
				</h:selectOneMenu>
			</h:column>
			<h:column>
				<h:commandLink id="clientBeneficiaryCol3" value="#{nbaBeneficiary.typeDescription}" styleClass="ovFullCellSelect" 
					immediate="true" />
			</h:column>
			<h:column>
				<h:selectOneMenu id="distDD" value="#{nbaBeneficiary.distributionType}" 
						valueChangeListener="#{nbaBeneficiary.distributionValChange}"
						onmousedown="getScrollXY('form_ContactDetails');"
						rendered = "#{!nbaBeneficiary.contingent}"
						styleClass="formEntryText" style="width: 85px;"> 
						<f:selectItems id="distList" value="#{nbaBeneficiary.distributionTypeList}" />
				</h:selectOneMenu>
				<h:outputText id="DistText" value="#{nbaBeneficiary.distributionText}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneficiary.contingent}" />
			</h:column>
			<h:column>
				<h:inputText id="clientBeneficiaryCol5Amount" value="#{nbaBeneficiary.interestAmount}" 
					valueChangeListener="#{nbaBeneficiary.valChange}"
					onmousedown="getScrollXY('form_ContactDetails');"
					styleClass="ovFullCellSelect"
					style="margin-top: -2px;"
					rendered = "#{nbaBeneficiary.amount}">
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
				</h:inputText>
				<h:inputText id="clientBeneficiaryCol5Percent" value="#{nbaBeneficiary.interestPercent}" 
					valueChangeListener="#{nbaBeneficiary.valChange}"
					onmousedown="getScrollXY('form_ContactDetails');"
					styleClass="ovFullCellSelect"
					style="margin-top: -2px;"
					rendered = "#{nbaBeneficiary.percent}" 
					immediate="true" >
					<f:convertNumber type="percent" pattern="###.##" /> 
				</h:inputText>
				<h:outputText id="clientBeneficiaryCol5Text" value="#{nbaBeneficiary.distributionText}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneficiary.balanceEqualUnknown}" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup id="clientBeneficiaryTitleSubHeader" styleClass="ovButtonBar">
		<h:commandButton id="beneBtnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="setTargetFrame();"
			 action="#{pc_NbaClientBeneficiaryTableData.actionAddBeneficiary}"/>
	</h:panelGroup>
</jsp:root>