<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<!-- Table Column Headers -->
	<h:panelGrid id="clientSectionHeader" columns="3" cellpadding="0" cellspacing="0"
				styleClass="sectionHeader" style="height: 56px; margin-top: 29px;">
		<h:column>
			<h:panelGroup style="width: 305px">
				<h:outputLabel value="#{pc_selectedFinalDispParty.name}" styleClass="shTextLarge" style="margin-left: 7px; text-transform: capitalize;"/>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="width: 100px" rendered="#{pc_selectedFinalDispParty.person}">
				<h:outputText value="#{pc_selectedFinalDispParty.gender}" styleClass="shText" />
			</h:panelGroup>
			<h:panelGroup style="text-align: right; width: 105px" rendered="#{pc_selectedFinalDispParty.person}">
				<h:outputText value="#{property.birthDate}" styleClass="shText" />
				<h:outputText value="#{pc_selectedFinalDispParty.birthDate}" styleClass="shText" style="margin-left: 7px">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="text-align: right; width: 90px; margin-right: 5px">
				<h:outputText value="#{property.age}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person}" />
				<h:outputText value="#{pc_selectedFinalDispParty.age}" styleClass="shText" style="margin-left: 7px"
							rendered="#{pc_selectedFinalDispParty.person}" />
			</h:panelGroup>
		</h:column>
		<!-- Header Row 2 -->
		<h:column>
		</h:column>
		<h:column>
			<h:panelGroup style="width: 200px; margin-bottom: 7px">
				<h:outputText value="#{property.personRateClassTitle}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person}" />
				<h:outputText value="#{pc_selectedFinalDispParty.rateClass}" styleClass="shText"
							style="margin-left: 7px"
							rendered="#{pc_selectedFinalDispParty.person}" />
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="text-align: right; width: 90px; margin-right: 5px; margin-bottom: 7px">
				<h:outputText value="#{pc_selectedFinalDispParty.tobacco}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person and !pc_finalDispNavigation.productDI}"/>  
				<h:outputText value="#{property.occupClass}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person and pc_finalDispNavigation.productDI}"/>
				<h:outputText value="#{pc_selectedFinalDispParty.employmentClass}" styleClass="shText" style="left: 7px;"
							rendered="#{pc_selectedFinalDispParty.person and pc_finalDispNavigation.productDI}"/>	
			</h:panelGroup>
		</h:column>
	</h:panelGrid>
		<h:panelGroup id="clientHeaderTable" styleClass="ovDivTableHeader">
			<h:panelGrid columns="2" styleClass="ovTableHeader"
					columnClasses="ovColHdrText155,ovColHdrText295"
					cellspacing="0">
				<h:outputLabel id="clientHdrCol1Table1" value="Role" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol2Table1" value="Detail" styleClass="ovColSortedFalse" />
			</h:panelGrid>
		</h:panelGroup>
		<!-- Table Columns -->
		<h:panelGroup id="clientTabDiv" styleClass="ovDivTableData" style="height: 135px;">
			<h:dataTable id="clientTable" styleClass="ovTableData" cellspacing="0" rows="0"
						binding="#{pc_selectedFinalDispParty.clientTable.dataTable}"
						value="#{pc_selectedFinalDispParty.clientTable.rows}" var="nbaClient"
						rowClasses="#{pc_selectedFinalDispParty.clientTable.rowStyles}"
						style="min-height:19px;"
						columnClasses="ovColText155,ovColText295ML">
				<h:column id ="clientCol1">
					<h:panelGroup id ="ClientConSpan2" styleClass="ovFullCellSelect" style="width: 200%;">
						<h:graphicImage id="clientCol1a" value="/images/hierarchies/#{nbaClient.icon1}"
									styleClass="#{nbaClient.icon1StyleClass}" style="margin-left: 5px; margin-top: -6px;"
									rendered="#{nbaClient.icon1Rendered}" />
						<h:graphicImage id="clientCol1b" value="/images/hierarchies/#{nbaClient.icon2}"
									styleClass="#{nbaClient.icon2StyleClass}" style="margin-left: 5px; margin-top: -6px;"
									rendered="#{nbaClient.icon2Rendered}" />
						<h:outputText  id="clientCol1c" value="#{nbaClient.col1}" style="vertical-align: top; color: #000000;" />
					</h:panelGroup>
				</h:column>
				<h:column id ="clientCol2">
					<h:outputText title="#{nbaClient.col3}" id="clientCol2Txt" value="#{nbaClient.col2}" styleClass="ovMultiLine#{nbaClient.draftText}"
									style="width:600px;"/>
				</h:column>
			</h:dataTable>   
		</h:panelGroup>
</jsp:root>