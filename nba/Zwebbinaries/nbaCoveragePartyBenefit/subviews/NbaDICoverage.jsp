<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="createUpdateCoverageArea" styleClass="inputFormMat">
		<h:panelGroup id="createUpdateCoverageForm" styleClass="inputForm"  style="height: 300px;"> 
			<h:outputLabel id="createTitle" value="#{property.addCoverageTitle}" rendered="#{pc_coverage.add}" styleClass="formTitleBar" />
			<h:outputLabel id="updateRatingTitle" value="#{property.updateCoverageTitle}" rendered="#{!pc_coverage.add}" styleClass="formTitleBar" />
			<h:panelGroup id="addUpdateIncreaseArea" styleClass="formDataEntryLine">
				<h:panelGroup id="coveragePGroup" styleClass="formDataEntryLine">
					<h:outputText id="coverageType" value="#{property.covPartyCoverage}" styleClass="formLabel" style="width: 157px;" />
					<h:selectOneMenu id="coverageTypeDD" value="#{pc_coverage.productCode}" 
						styleClass="formEntryText" style="width: 410px;" 
						disabled="#{pc_coverage.baseCoverage}">
						<f:selectItems id="coverageTypeList" value="#{pc_coverage.coverageTypeList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup id="issuePGroup" styleClass="formDataEntryLine">
					<h:outputText id="issue" value="#{property.covPartyIssue}" styleClass="formLabel" style="width: 157px;" />
					<h:inputText id="issueMontInpt" value="#{pc_coverage.issueMonth}" maxlength="2" styleClass="formEntryText" style="width: 90px;">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
		
					<h:outputText id="issueMonth" value="#{property.covPartyIssueMonth}" styleClass="formLabel"
						style="width: 70px; text-align: left; padding-left: 10px;" />
					<h:inputText id="issueYearInpt" value="#{pc_coverage.issueYear}" styleClass="formEntryText" style="width: 90px;" maxlength="4">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
					<h:outputText id="issueYear" value="#{property.covPartyIssueYear}" styleClass="formLabel"
						style="width: 70px; text-align: left; padding-left: 10px;" />
				</h:panelGroup>
				<h:panelGroup id="pinsPlanOccupationClassGroup" styleClass="formDataEntryLine">
					<h:outputText id="occupationClass" value="#{property.appEntOccupationClass}" styleClass="formLabel" style="width: 157px;"/>
					<h:selectOneMenu id="occupationClassVal" value="#{pc_coverage.occupationClass}" styleClass="formEntryText" 
							style="width: 410px;"> 
						<f:selectItems id="occupationClassValList" value="#{pc_coverage.occupationClassList}"/> 
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup id="amountPGroup" styleClass="formDataEntryLine">
					<h:outputText id="amountLabel" value="#{property.appEntAmount}" styleClass="formLabel" style="width: 157px;"/>
					<h:inputText id="amountEF" value="#{pc_coverage.amount}" styleClass="formEntryText" style="width: 120px;margin-top:-2px">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
					</h:inputText>
				</h:panelGroup>
				 <h:panelGroup id="benefitPeriodAccGroup"  styleClass="formDataEntryLine">
					<h:outputText id="AccidentBenefitPeriod" value="#{property.appEntBenefitPeriodAcc}" styleClass="formLabel" style="width: 157px;"/>
						<h:selectOneMenu id="benefitPeriodAcc" value ="#{pc_coverage.benefitPeriodAcc}" styleClass="formEntryText" 
							style="width: 410px;" disabled="#{pc_coverage.baseCoverage}" >
						   <f:selectItems id="benefitPeriodAccList" value="#{pc_coverage.benefitPeriodAccList}" />
		     			</h:selectOneMenu>
				 </h:panelGroup>
				 <h:panelGroup id="waitingPeriodAccGroup" styleClass="formDataEntryLine">
					<h:outputText id="AccidentWaitingPeriod" value="#{property.appEntWaitingPeriodAcc}" styleClass="formLabel" style="width: 157px;"/>
						<h:selectOneMenu id="waitingPeriodAcc" value ="#{pc_coverage.waitingPeriodAcc}" styleClass="formEntryText" 
							style="width: 410px;" disabled="#{pc_coverage.baseCoverage}">
						  <f:selectItems id="waitingPeriodAccList" value="#{pc_coverage.waitingPeriodAccList}" />
						</h:selectOneMenu>
				  </h:panelGroup>
				  <h:panelGroup id="benefitPeriodSickGroup"  styleClass="formDataEntryLine">
					 <h:outputText id="SicknessBenefitPeriod" value="#{property.appEntBenefitPeriodSick}" styleClass="formLabel" style="width: 157px;"/>
						 <h:selectOneMenu id="benefitPeriodSick" value ="#{pc_coverage.benefitPeriodSick}" styleClass="formEntryText" 
							style="width: 410px;" disabled="#{pc_coverage.baseCoverage}">
								<f:selectItems id="benefitPeriodSickList" value="#{pc_coverage.benefitPeriodSickList}" />
						  </h:selectOneMenu>
				  </h:panelGroup>
				  <h:panelGroup id="waitingPeriodSickGroup" styleClass="formDataEntryLine">
						<h:outputText id="SicknessWaitingPeriod" value="#{property.appEntWaitingPeriodSick}" styleClass="formLabel" style="text-align: right;width: 158px;"/>
								<h:selectOneMenu id="waitingPeriodSick" value ="#{pc_coverage.waitingPeriodSick}" styleClass="formEntryText" 
									style="width: 411px;" disabled="#{pc_coverage.baseCoverage}">
									 <f:selectItems id="waitingPeriodSickList" value="#{pc_coverage.waitingPeriodSickList}" />
								</h:selectOneMenu>
				  </h:panelGroup>
				<f:verbatim>
					<hr class="formSeparator" />
				</f:verbatim>
				<h:panelGroup styleClass="formButtonBar">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
						action="#{pc_coveragePartyNavigation.actionCloseSubView}" immediate="true"  />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
						action="#{pc_coverage.actionClear}" />
					<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_coverage.actionAddNew}"
						rendered="#{pc_coverage.add}" />
					<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_coverage.actionAdd}"
						rendered="#{pc_coverage.add}" />
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_coverage.actionUpdate}"
						rendered="#{!pc_coverage.add}" />
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>