<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245     	NB-1301   Coverage Party  Benefit User Interface Rewrite  -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<!-- Table Column Headers -->
	<h:panelGrid id="covSectionHeader" columns="3" cellpadding="0" cellspacing="0"
				styleClass="sectionHeader" style="height: 56px; margin-top: 29px; margin-left: 5px">
		<h:column>
			<h:panelGroup style="width: 305px">
				<h:outputLabel value="#{pc_selectedFinalDispParty.name}" styleClass="shTextLarge" style="margin-left: 7px; text-transform: capitalize;"/>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="width: 100px" rendered="#{pc_selectedFinalDispParty.person}">
				<h:outputText value="#{pc_selectedFinalDispParty.gender}" styleClass="shText" />
			</h:panelGroup>
			<h:panelGroup style="text-align: right; width: 105px" rendered="#{pc_selectedFinalDispParty.person}">
				<h:outputText value="#{property.birthDate}" styleClass="shText" />
				<h:outputText value="#{pc_selectedFinalDispParty.birthDate}" styleClass="shText" style="margin-left: 7px">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="text-align: right; width: 90px; margin-right: 5px">
				<h:outputText value="#{property.age}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person}" />
				<h:outputText value="#{pc_selectedFinalDispParty.age}" styleClass="shText" style="margin-left: 7px"
							rendered="#{pc_selectedFinalDispParty.person}" />
			</h:panelGroup>
		</h:column>
		<!-- Header Row 2 -->
		<h:column>
		</h:column>
		<h:column>
			<h:panelGroup style="width: 200px; margin-bottom: 7px">
				<h:outputText value="#{property.personRateClassTitle}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person}" />
				<h:outputText value="#{pc_selectedFinalDispParty.rateClass}" styleClass="shText"
							style="margin-left: 7px"
							rendered="#{pc_selectedFinalDispParty.person}" />
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="text-align: right; width: 90px; margin-right: 5px; margin-bottom: 7px">
				<h:outputText value="#{pc_selectedFinalDispParty.tobacco}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person and !pc_finalDispNavigation.productDI}"/>  <!-- FNB013 -->
				<h:outputText value="#{property.occupClass}" styleClass="shText"
							rendered="#{pc_selectedFinalDispParty.person and pc_finalDispNavigation.productDI}"/><!-- FNB013 -->
				<h:outputText value="#{pc_selectedFinalDispParty.employmentClass}" styleClass="shText" style="left: 7px;"
							rendered="#{pc_selectedFinalDispParty.person and pc_finalDispNavigation.productDI}"/>	<!-- FNB013 -->
			</h:panelGroup>
		</h:column>
	</h:panelGrid>

	<h:panelGroup id="covClient1TabHeader" styleClass="ovDivTableHeader" style="margin-left: 10px; margin-top: 5px">
		<h:panelGrid columns="5" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText225,ovColHdrText190,ovColHdrDate,ovColHdrDate"
			cellspacing="0" cellpadding="0">
			<h:outputLabel id="selCovHdrCol0" value="" styleClass="ovColSortedFalse" />
			<h:outputLabel id="selCovHdrCol1" value="#{property.uwCovClientCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="selCovHdrCol3" value="#{property.uwCovClientCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="selCovHdrCol4" value="#{property.uwCovClientCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="selCovHdrCol5" value="#{property.uwCovClientCol5}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="covClient1TabData" styleClass="ovDivTableData" style="margin-left: 10px; margin-bottom: 5px; height: 135px">
		 <h:dataTable id="selCovTable" 
						 styleClass="ovTableData"
						 cellspacing="0" cellpadding="0"
						 rows="0" 
						 binding="#{pc_selectedFinalDispParty.coverageTable.dataTable}"  
						 value="#{pc_selectedFinalDispParty.coverageTable.rows}" 
						 var="nbaCoverage" 
						 rowClasses="#{pc_selectedFinalDispParty.coverageTable.rowStyles}"
						style="min-height:19px;"
						columnClasses="ovColIcon,ovColText225,ovColText190,ovColText85,ovColText85" >

			<h:column id ="selCovCol1">
				<!-- begin NBA329 -->
				<h:graphicImage id="selCovCol1_insured" url="images/coverage/insured.gif" styleClass="ovViewIconTrue"
							rendered="#{nbaCoverage.insured and !nbaCoverage.denied}"/>
				<h:graphicImage id="selCovCol1_insuredDenied" url="images/coverage/insured-denied.gif" styleClass="ovViewIconTrue"
							rendered="#{nbaCoverage.insured and nbaCoverage.denied}"/>
				<h:graphicImage id="selCovCol1_base" url="images/coverage/base-coverage.gif" styleClass="ovViewIconTrue"
							rendered="#{nbaCoverage.baseCoverage}"/>
				<h:graphicImage id="selCovCol1_denied" url="images/coverage/denied.gif" styleClass="ovViewIconTrue"
							rendered="#{!nbaCoverage.insured and nbaCoverage.denied}"/>
				<h:graphicImage id="selCovCol1_none" url="images/needs_attention/clear.gif" styleClass="ovViewIconFalse"
							rendered="#{!(nbaCoverage.insured or nbaCoverage.denied or nbaCoverage.baseCoverage)}" />
				<!-- end NBA329 -->
			</h:column>
			<h:column id ="selCovCol2">
				<h:panelGroup id ="covClientSpan1"> <!-- FNB013 --> 
					<h:commandButton id="covROIcon11" image="images/hierarchies/#{nbaCoverage.icon1}" rendered="#{nbaCoverage.icon1Rendered}"
						styleClass="#{nbaCoverage.icon1StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" />
					<h:commandButton id="covROIcon21" image="images/hierarchies/#{nbaCoverage.icon2}" rendered="#{nbaCoverage.icon2Rendered}"
						styleClass="#{nbaCoverage.icon2StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" />
					<h:commandButton id="covROIcon31" image="images/hierarchies/#{nbaCoverage.icon3}" rendered="#{nbaCoverage.icon3Rendered}"
						styleClass="#{nbaCoverage.icon3StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" /> 
					<h:commandButton id="covROIcon41" image="images/hierarchies/#{nbaCoverage.icon4}" rendered="#{nbaCoverage.icon4Rendered}"
						styleClass="#{nbaCoverage.icon4StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" /> 
							<h:outputText id="ro_covCol2LabelLevel1_2"  value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:175px;" rendered="#{!nbaCoverage.icon3Rendered}"/>
							<h:outputText id="ro_covCol2LabelLevel3"  value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:165px;" rendered="#{nbaCoverage.icon3Rendered and !nbaCoverage.icon4Rendered}"/>
							<h:outputText id="ro_covCol2LabelLevel4" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:145px;" rendered="#{nbaCoverage.icon4Rendered}"/>
				</h:panelGroup>
			</h:column>				
			<h:column id ="selCovCol3">
					<h:outputText id="clientCol13Text" value="#{nbaCoverage.col3}" styleClass="ovMultiLine#{nbaCoverage.draftText}" /> 
			</h:column>
			<h:column id="sel2Col4">
					<h:outputText value="#{nbaCoverage.col4}"  styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:outputText>
			</h:column>
			<h:column id="selcov2Col5">
				<h:outputText value="#{nbaCoverage.col5}" styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>