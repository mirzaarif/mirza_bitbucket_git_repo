<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<!-- Table Title Bar -->
	<h:panelGroup id="clientContactInfoTitleHeader1" styleClass="formTitleBar"> 
		<h:outputLabel id="clientContactInfoTableTitle1" value="#{property.covPartyClientContactInfoTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="clientContactInfoHeader" styleClass="ovDivTableData" style="width:620px;"> 
		<h:panelGrid columns="3" styleClass="ovTableData" columnClasses="ovColTextTop" cellspacing="0" style="width:600px;">
			<h:outputLabel id="clientContactInfoHdrCol1" value="" style="height: 10px" />
			<h:outputLabel id="clientContactInfoHdrCol2" value="" style="height: 10px"/> 
			<h:outputLabel id="clientContactInfoHdrCol3" value="" style="height: 10px"/>
		
			<h:outputLabel id="clientContactInfoHdrCol4" value="#{property.clientContactInfoCol1}" style="color: gray; font-weight: bold;"/>
			<h:outputLabel id="clientContactInfoHdrCol5" value="#{property.clientContactInfoCol2}" style="color: gray; font-weight: bold;"/>
			<h:outputLabel id="clientContactInfoHdrCol6" value="#{property.clientContactInfoCol3}" style="color: gray; font-weight: bold;"/>
		</h:panelGrid>
		
		<h:panelGrid columns="3" styleClass="ovTableData" columnClasses="ovColTextTop"  
			style="vertical-align: text-top; width:600px;" cellspacing="0"> 
			<h:column>
				<h:panelGroup id="pgAddress11" styleClass="formDisplayLine" style="padding-bottom:19px">
					<h:commandButton id="AddrbtnAdd" value="#{property.buttonAdd}" styleClass="formButtonLeft" onclick="setTargetFrame();"
							style="width: 50px;margin-left: 120px;margin-top:-20px" action="#{pc_NbaClientContactInfoTableData.actionAddAddress}"/>

				</h:panelGroup>
				<h:dataTable id="addressTable" 
					styleClass="ovTableData" 
					cellspacing="0" 
					rows="0"
					binding="#{pc_NbaClientContactInfoTableData.addressHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.addressRows}" var="addr"
					style="min-height:19px;"
					columnClasses="ovColTextTop">
					<h:column>
					<h:panelGroup  id="pgAddress" styleClass="ovMultiLine" style="padding-bottom:19px">
							<h:panelGroup  id="pgAddress1" styleClass="formDisplayLine" style="padding-top:19px;width:100%">
								<h:outputLabel id="clientContactInfoAddressType" value="#{addr.displayType}" styleClass="formDisplayText" />		
							</h:panelGroup>
							<h:panelGroup  id="pgAddress2" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoAddressAttnLine" value="#{addr.attentionLine}" style="width:190px" styleClass="ovMultiLine" />					
							</h:panelGroup>
							<h:panelGroup id="pgAddressLn1" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoAddressline1" value="#{addr.addressLine1}"  style="width:190px" styleClass="ovMultiLine" />			
							</h:panelGroup>
							<h:panelGroup id="pgAddressLn2" styleClass="formDisplayLine" style="width:100%" rendered="#{addr.addr2Rendered}">
								<h:outputLabel id="clientContactInfoAddressline2" value="#{addr.addressLine2}" style="width:190px" styleClass="ovMultiLine" />			
							</h:panelGroup>
							<h:panelGroup id="pgAddressLn3" styleClass="formDisplayLine" style="width:100%" rendered="#{addr.addr3Rendered}">
								<h:outputLabel id="clientContactInfoAddressline3" value="#{addr.addressLine3}" style="width:190px" styleClass="ovMultiLine" />			
							</h:panelGroup>
							<h:panelGroup id="pgAddressLn4" styleClass="formDisplayLine" style="width:100%" rendered="#{addr.addr4Rendered}">
								<h:outputLabel id="clientContactInfoAddressline4" value="#{addr.addressLine4}" style="width:190px" styleClass="ovMultiLine" />			
							</h:panelGroup>
							<h:panelGroup id="pgAddressCity" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoCity" value="#{addr.formattedCityStateZip}" style="width:190px" styleClass="ovMultiLine" />		
							</h:panelGroup>
							<h:panelGroup id="pgAddressCntry" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoCountry" value="#{addr.displayCountry}" style="width:190px" styleClass="ovMultiLine" />						
							</h:panelGroup>
							<h:panelGroup id="pgAddressCnty" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoCounty" value="#{addr.county}"  style="width:190px" styleClass="ovMultiLine" />						
							</h:panelGroup>
							<h:panelGroup id="pgAddressDate" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="clientContactInfoDate" value="#{addr.startDate}" styleClass="ovMultiLine">
									<f:convertDateTime pattern="#{property.datePattern}" />
								</h:outputLabel>
	
							</h:panelGroup>
							<h:panelGroup id="pgAddressUpdate" styleClass="formDisplayLine" style="width:100%; padding-bottom:19px;">
								<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonLeft" onclick="setTargetFrame();"
									 style="width:50px;margin-left: 120px;" action="#{addr.actionLaunchPopUpUpdate}"/>	
	
							</h:panelGroup>
							
					</h:panelGroup>
					</h:column> 
				</h:dataTable>
				
			</h:column>
			<h:column>
				<h:panelGroup id="phoneAdd" styleClass="formDisplayLine" style="padding-bottom:19px">
					<h:commandButton id="phoneBtnAdd" value="#{property.buttonAdd}" styleClass="formButtonLeft" onclick="setTargetFrame();"
							style="width: 50px;margin-left: 320px;margin-top:-20px" action="#{pc_NbaClientContactInfoTableData.actionAddPhone}"/>

				</h:panelGroup>
				<h:dataTable id="clientContactInfoPhoneTable" 
					styleClass="ovTableData" 
					cellspacing="0" 
					rows="0"
					binding="#{pc_NbaClientContactInfoTableData.phoneHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.phoneRows}" var="phone"
					style="min-height:19px;"
					columnClasses="ovColTextTop"> 
					<h:column>
						<h:panelGroup  id="phonePGType" styleClass="formDisplayLine" style="padding-top:19px;width:100%">
								<h:outputLabel id="phoneTypeLine" value="#{phone.displayType}" styleClass="formDisplayText" />		
						</h:panelGroup>
							<h:panelGroup  id="phonePGNumber" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="phoneNumberLine" value="#{phone.number}" style="width:190px" styleClass="ovMultiLine">
									<f:convertNumber type="phone" integerOnly="true" pattern="#{property.phonePattern}"/>	
								</h:outputLabel>
						</h:panelGroup>				
						<h:panelGroup id="phonePGUpdate" styleClass="formDisplayLine" style="width:100%; padding-bottom:19px;">
								<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonLeft" onclick="setTargetFrame();"
									 style="width:50px;margin-left: 320px;" action="#{phone.actionLaunchPopUpUpdate}"/>	
						</h:panelGroup>
					</h:column>
				</h:dataTable>
			</h:column>
			<h:column>
				<h:panelGroup id="emailAdd" styleClass="formDisplayLine" style="padding-bottom:19px">
					<h:commandButton id="emailBtnAdd" value="#{property.buttonAdd}" styleClass="formButtonLeft" onclick="setTargetFrame();"
							style="width: 50px;margin-left: 520px;margin-top:-20px" action="#{pc_NbaClientContactInfoTableData.actionAddEmail}"/>

				</h:panelGroup>
				<h:dataTable id="clientContactInfoEmailTable" 
					styleClass="ovTableData" 
					cellspacing="0" 
					rows="0"
					binding="#{pc_NbaClientContactInfoTableData.emailHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.emailRows}" var="email"
					style="min-height:19px;"
					columnClasses="ovColTextTop"> 
					<h:column>
						<h:column>
						<h:panelGroup  id="emailPGType" styleClass="formDisplayLine" style="padding-top:19px;width:100%">
								<h:outputLabel id="emailTypeLine" value="#{email.displayType}" styleClass="formDisplayText" />		
						</h:panelGroup>
							<h:panelGroup  id="emailPGNumber" styleClass="formDisplayLine" style="width:100%">
								<h:outputLabel id="emailNumberLine" value="#{email.address}" style="width:190px" styleClass="ovMultiLine"/>
						</h:panelGroup>				
						<h:panelGroup id="emailPGUpdate" styleClass="formDisplayLine" style="width:100%; padding-bottom:19px;">
								<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonLeft" onclick="setTargetFrame();"
									 style="width:50px;margin-left: 520px;" action="#{email.actionLaunchPopUpUpdate}"/>	
						</h:panelGroup>
					</h:column>				
					</h:column>
				</h:dataTable>
			</h:column>
		</h:panelGrid>
	</h:panelGroup>
</jsp:root>