<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-723 	NB-1401     Add Banding Override Functionality -->
<!-- NBA329			NB-1401     Retain Denied Coverage and Benefit -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPRNBA-794     NB-1401   	Signed Date is Not Applicable and Should Be Removed from the View/Update Coverage -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	<h:panelGroup id="createUpdateCoverageArea" styleClass="inputFormMat">
		<h:panelGroup id="createUpdateCoverageForm" styleClass="inputForm"  style="height: 100%"> 
			<!-- begin NBA329 --> 
			<h:panelGroup id="coverageFormTitle" styleClass="formTitleBar">
				<h:outputText id="createTitle" value="#{property.addCoverageTitle}" rendered="#{pc_coverage.add}" />
				<h:outputText id="updateTitle" value="#{property.updateCoverageTitle}" rendered="#{!pc_coverage.add}" />
				<h:outputText id="updateTitleDenied" value="#{property.deniedSuffix}" style="padding-left: 5px;"
								rendered="#{!pc_coverage.add and pc_coverage.denied}" />
			</h:panelGroup>
			<!-- end NBA329 -->
			<h:panelGroup id="addUpdateCoverageeArea" styleClass="formDataEntryLine">				
				<h:panelGroup id="coveragePGroup" styleClass="formDataEntryLine">
					<h:outputText id="coverageType" value="#{property.covPartyCoverage}" styleClass="formLabel" style="width: 150px;" />
					<h:selectOneMenu id="coverageTypeDD" value="#{pc_coverage.productCode}" 
						valueChangeListener="#{pc_coverage.planChange}"
						disabled="#{pc_coverage.baseCoverage}"
						styleClass="formEntryText" style="width: 410px;"> 
						<f:selectItems id="coverageTypeList" value="#{pc_coverage.coverageTypeList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup id="issuePGroup" styleClass="formDataEntryLine">
					<h:outputText id="issue" value="#{property.covPartyIssue}" styleClass="formLabel" style="width: 150px;" />
					<h:inputText id="issueMontInpt" value="#{pc_coverage.issueMonth}" maxlength="2" styleClass="formEntryText" style="width: 50px;">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
		
					<h:outputText id="issueMonth" value="#{property.covPartyIssueMonth}" styleClass="formLabel"
						style="width: 70px; text-align: left; padding-left: 10px;" />
					<h:inputText id="issueYearInpt" value="#{pc_coverage.issueYear}" styleClass="formEntryText" style="width: 50px;" maxlength="4">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
					<h:outputText id="issueYear" value="#{property.covPartyIssueYear}" styleClass="formLabel"
						style="width: 70px; text-align: left; padding-left: 10px;" />
				</h:panelGroup>
				<h:panelGrid id="pinsBenefitPGroup" columnClasses="labelAppEntry,colMIBShort" 
					columns="2" cellpadding="0" cellspacing="0" style="padding-top: 12px; margin-left:150px">
					<h:column>
						<h:selectOneRadio id="pinsBenefitAmountUnitsRB" value="#{pc_coverage.unitsRB}" layout="pageDirection" 
						styleClass="radioLabel" style="width: 80px;margin-top:-3px; height:50px">
							<f:selectItem id="Item1" itemValue="1" itemLabel="Amount"/>
							<f:selectItem id="Item2" itemValue="2" itemLabel="Units"/>
						</h:selectOneRadio>	
					</h:column>
					<h:column>
					 	<h:inputText id="amountEF" value="#{pc_coverage.amount}" styleClass="formEntryText" style="width: 120px;margin-top:-2px">
							<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
						</h:inputText>
						<h:inputText id="unitsEF" value="#{pc_coverage.units}" styleClass="formEntryText" style="width: 120px; margin-top:5px"/>
					</h:column>
				<!-- SPRNBA-794 Code deleted -->	
				</h:panelGrid>
				<h:panelGrid id="deathBenefitGrid" columnClasses="labelAppEntry, labelAppEntry" 
					columns="2" cellpadding="0" cellspacing="0" style="padding-top: 12px;" rendered="#{pc_coverage.UL and pc_coverage.firstToDie}">
					<h:column>
						<h:outputText id="deathBenefitLabel" value="#{property.covPartyDeathBenefit}" styleClass="formLabel" 
						style="width: 150px;margin-top:12px" />
					</h:column>
					<h:column>
						<h:selectOneRadio id="deathBenefit" value="#{pc_coverage.deathBenefitOption}" layout="lineDirection" styleClass="radioLabel" 
							style="width: 200px; margin-top:5px">
							<f:selectItem id="Item3" itemValue="1" itemLabel="Level"/>
							<f:selectItem id="Item4" itemValue="2" itemLabel="Decreasing"/>
						</h:selectOneRadio>	
					</h:column>
				</h:panelGrid>
				<!-- begin SPRNBA-723 -->
				<h:panelGroup id="unisexBandingGroup" styleClass="formDataEntryLine" rendered="#{!pc_coverage.vantageBESSystem}" style="margin-bottom:20px;">
					<h:panelGroup id="unisexCBGroup" styleClass="formDataEntryLine" >
						<h:selectBooleanCheckbox id="overrideUnisexCB" 
							valueChangeListener="#{pc_coverage.valChangeUnisex}"
							onclick="submit();"
							value="#{pc_coverage.overrideUnisex}" style="margin-left: 145px;"  />
						<h:outputLabel value="#{property.covPartyOverrideUnisex}" styleClass="formLabelRight" />
						<h:selectBooleanCheckbox id="overrideBandCB" 
								valueChangeListener="#{pc_coverage.valChangeBandInd}"
								onclick="submit();"
								rendered="#{pc_coverage.bandedCoverage}"
								value="#{pc_coverage.overrideBandCode}" style="margin-left: 10px;"  />
						<h:outputLabel value="#{property.covPartyOverrideBand}" styleClass="formLabelRight"
								rendered="#{pc_coverage.bandedCoverage}" />
					</h:panelGroup>	 
					<h:panelGroup id="unisexCodeGroup" styleClass="formDataEntryLine" rendered="#{pc_coverage.overrideUnisex}" >
					<!-- end SPRNBA-723 -->
						<h:outputText id="unisexCode" value="#{property.covPartyUnisexCode}" styleClass="formLabel" style="width: 150px;" />
						<h:selectOneMenu id="unisexCodeDD" value="#{pc_coverage.unisexCode}" 
								styleClass="formEntryText" style="width: 410px;border-style:none">		
							<f:selectItems id="unisexCodeList" value="#{pc_coverage.unisexCodeList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup id="unisexSubGroup"  styleClass="formDataEntryLine" rendered="#{pc_coverage.overrideUnisex}"> <!--SPRNBA-723 -->
						<h:outputText id="unisexSubSeries" value="#{property.covPartyUnisexSubSeries}" styleClass="formLabel" style="width: 150px;" />
						<h:selectOneMenu id="unisexSubSeriesDD" value="#{pc_coverage.unisexSubSeries}" 
								styleClass="formEntryText" style="width: 410px;">	
							<f:selectItems id="unisexSubSeriesList" value="#{pc_coverage.unisexSubSeriesList}" />
						</h:selectOneMenu>
					</h:panelGroup>	 
					<!-- begin SPRNBA-723 -->
					<h:panelGroup id="bandingGroup2" styleClass="formDataEntryLine" rendered="#{pc_coverage.overrideBandCode}" >
						<h:outputText id="bandingOverrideGroup" value="#{property.covPartyOverrideBandCode}"
								styleClass="formLabel" style="width: 150px;" />
						<h:selectOneMenu id="bandingTableDD" value="#{pc_coverage.bandCode}" 
								styleClass="formEntryText" style="width: 410px;">	
							<f:selectItems id="bandingList" value="#{pc_coverage.bandCodes}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<!-- end SPRNBA-723 -->
				</h:panelGroup>
				<f:subview id="wmABeneCoverage" rendered="#{pc_coveragePartyNavigation.vantageBESSystem}">
					<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientBeneficiaryTableWma.jsp"/>
					
				</f:subview>
		
				<h:panelGroup styleClass="formButtonBar" style="margin-top:20px;">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
						action="#{pc_coveragePartyNavigation.actionCloseSubView}" immediate="true"  />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
						action="#{pc_coverage.actionClear}" />
					<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_coverage.actionAddNew}"
						rendered="#{pc_coverage.add}" />
					<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_coverage.actionAdd}"
						rendered="#{pc_coverage.add}" />
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_coverage.actionUpdate}"
						rendered="#{!pc_coverage.add}" disabled="#{pc_coverage.denied}"/>  <!-- NBA329 -->
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>