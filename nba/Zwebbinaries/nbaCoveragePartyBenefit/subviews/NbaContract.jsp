<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	<h:panelGroup id="createUpdateCoverageArea" styleClass="inputFormMat">
		<h:panelGroup id="updateContractForm" styleClass="inputForm" style="height: 100%"> 
			<h:outputLabel id="updateContractTitle" value="#{property.updateContractTitle}" styleClass="formTitleBar" />
			<h:panelGroup id="updateContractArea" styleClass="formDataEntryLine">
				<h:panelGroup id="planned1035PGroup" styleClass="formDataEntryLine">
					<h:selectBooleanCheckbox value="#{pc_contract.otherThanAppliedFor}" style="margin-left: 120px;"  />
					<h:outputLabel value="#{property.covPartyOTAF}" styleClass="formLabelRight" />
					<h:selectBooleanCheckbox value="#{pc_contract.cb1035Exchange}" style="margin-left: 30px;"  />
					<h:outputLabel value="#{property.covParty1035}" styleClass="formLabelRight" />
				</h:panelGroup>
				
				<h:panelGroup id="guidelineGroup" styleClass="formDataEntryLine" rendered="#{pc_contract.fixedPremiumProduct}">
					<h:selectBooleanCheckbox value="#{pc_contract.guidelineXXX}" style="margin-left: 120px;" disabled="#{pc_contract.vantageBESSystem}"/>
					<h:outputLabel value="#{property.covPartyGLXXX}" styleClass="formLabelRight" />
				</h:panelGroup>
				<h:panelGroup id="contractPGroup" styleClass="formDataEntryLine">
					<h:outputText id="initialAmt" value="Initial Premium:" styleClass="formLabel" />
					<h:inputText id="initialAmtEF" value="#{pc_contract.initialPrem}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>
					<h:outputText id="planed1035Amt" value="Planned 1035 Exchange Premium:" styleClass="formLabel" style="width:220px;" />
					<h:inputText id="planned1035AmtEF" value="#{pc_contract.planned1035ExhangePrem}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>
				</h:panelGroup>
				<!-- death benefit options -->
				<h:panelGroup id="dbOptionPGroup" styleClass="formDataEntryLine" rendered="#{pc_contract.advancedLifeProduct}">
						<h:panelGroup id="dbGroup" styleClass="formDataEntryLine">
							<h:outputText id="db" value="#{property.covPartyDeathBenefit}" styleClass="formLabel" />
							<h:selectOneMenu id="dbDD" value="#{pc_contract.deathBenefit}" styleClass="formEntryText" style="width: 460px;">	
								<f:selectItems id="dbList" value="#{pc_contract.deathBenefitList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="targetGroup" styleClass="formDataEntryLine" rendered="#{!pc_contract.vantageBESSystem}">
							<h:outputText id="targetAmt" value="#{property.covPartyTargetAmt}" styleClass="formLabel" />
							<h:inputText id="targetAmtEF" value="#{pc_contract.targetAmount}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
							</h:inputText>
							<h:outputText id="targetRatio" value="#{property.covPartyTargetRatio}" styleClass="formLabel" style="margin-left: 95px;"  />	
							<h:inputText id="targetRatioEF" value="#{pc_contract.targetRatio}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
								<f:convertNumber type="double"/>
							</h:inputText>
						</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup id="maturityGroup" styleClass="formDataEntryLine" rendered="#{pc_contract.flexiblePremiumProduct}">
						<h:panelGrid id="maturityPGrid" columnClasses="labelAppEntry,labelAppEntry,colMIBShort" 
						    columns="3" cellpadding="0" cellspacing="0" 
						   style="padding-top: 12px;">
							<h:column>
								<h:outputText id="maturity" value="Maturity:" styleClass="formLabel"/>
							</h:column>
							<h:column>
								<h:selectOneRadio id="maturityRB" value="#{pc_contract.maturityRB}" layout="pageDirection" 
									styleClass="formLabelRight" style="width: 100px;margin-top:-3px; height:50px; margin-left:-5px"
									valueChangeListener="#{pc_contract.maturityChange}" 
									onclick="submit()" immediate="true">
									<f:selectItem id="Item1" itemValue="1" itemLabel="Age"/>
									<f:selectItem id="Item2" itemValue="2" itemLabel="Date"/>
									<f:selectItem id="Item3" itemValue="3" itemLabel="Duration" itemDisabled="#{pc_contract.vantageBESSystem}"/>
								</h:selectOneRadio>	
							</h:column>
							<h:column>
					 			<h:inputText id="ageEF" value="#{pc_contract.maturityAge}" styleClass="formEntryText" style="width: 120px;margin-top:-2px" >
									<f:convertNumber type="integer"/>
								</h:inputText>
								<h:inputText id="dateEF" value="#{pc_contract.maturityDate}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
									<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
								<h:inputText id="durationEF" value="#{pc_contract.maturityDuration}" 
									styleClass="formEntryText" style="width: 120px; margin-top:5px" maxlength="4"  disabled="#{pc_contract.vantageBESSystem}">
									<f:convertNumber integerOnly="true" type="integer" />
								</h:inputText>
							</h:column>
						</h:panelGrid>
				</h:panelGroup>
				<!-- Annuities -->
				  <h:panelGroup id="annuitiesPGroup" styleClass="formDataEntryLine" rendered="#{pc_contract.annuity}">
					<h:panelGroup id="annuitiesSecondaryPGroup" styleClass="formDataEntryLine">
						<h:panelGroup id="annGroup" styleClass="formDataEntryLine">
							<h:outputText id="qual" value="#{property.covPartyQual}" styleClass="formLabel" />
							<h:selectOneMenu id="qualDD" value="#{pc_contract.qualification}" styleClass="formEntryText" style="width: 460px;">	
								<f:selectItems id="qualList" value="#{pc_contract.qualificationList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="SettlementGroup" styleClass="formDataEntryLine">
							<h:outputText id="settlement" value="#{property.covPartySettle}" styleClass="formLabel" />
							<h:selectOneMenu id="settlementDD" value="#{pc_contract.settlement}" styleClass="formEntryText" style="width: 460px;">	
								<f:selectItems id="settlementList" value="#{pc_contract.settlementList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="taxyrGroup" styleClass="formDataEntryLine">
							<h:outputText id="firstTaxYr" value="#{property.covPartyFirstTaxYr}" styleClass="formLabel" />
							<h:inputText id="firstTaxYrEF" value="#{pc_contract.firstTaxYear}" 
								styleClass="formEntryText" style="width: 120px; margin-top:5px" maxlength="4"
								disabled="#{pc_contract.vantageBESSystem}">
								<f:convertNumber integerOnly="true" type="integer" />
							</h:inputText>
						</h:panelGroup>
					</h:panelGroup>
					<!-- Roth (not for wmA)-->
					<h:panelGroup id="clifRothPGroup" styleClass="formDataEntryLine" rendered="#{!pc_contract.vantageBESSystem}">
						<h:outputText id="roth" value="Roth" styleClass="formSectionBar"/>
						<h:panelGroup id="rothNetContGP" styleClass="formDataEntryLine">
							<h:outputText id="netCont" value="#{property.covPartyNetContr}" styleClass="formLabel" />
							<h:inputText id="netContEF" value="#{pc_contract.netContribution}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
							</h:inputText>						
						</h:panelGroup>
						<h:panelGroup id="prevTaxYrGP" styleClass="formDataEntryLine">
							<h:outputText id="prevTaxYr" value="#{property.covPartyPrevTaxYr}" styleClass="formLabel" />
							<h:inputText id="prevTaxYrEF" value="#{pc_contract.prevTaxYear}" styleClass="formEntryText" style="width: 120px; margin-top:5px" maxlength="4">
								<f:convertNumber integerOnly="true" type="integer" />
							</h:inputText>
							<h:outputText id="prevTaxAmt" value="#{property.covPartyPrevAmount}" styleClass="formLabel" style="margin-left: 95px;"  />	
							<h:inputText id="prevTaxAmtEF" value="#{pc_contract.prevAmount}" styleClass="formEntryText" style="width: 120px; margin-top:5px">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
							</h:inputText>
						</h:panelGroup>
					</h:panelGroup>
				</h:panelGroup>
				<!-- Fixed Premium -->
				<h:panelGroup id="fixedPremiumGroup" styleClass="formDataEntryLine" rendered="#{pc_contract.fixedPremiumProduct}">
					<h:panelGroup id="nonForfeitureGroup" styleClass="formDataEntryLine">
							<h:outputText id="nonfor" value="#{property.covPartyNonforfeiture}" styleClass="formLabel" />
							<h:selectOneMenu id="nonforDD" value="#{pc_contract.nonforfeiture}" styleClass="formEntryText" style="width: 460px;" disabled="#{pc_contract.vantageBESSystem}">	
								<f:selectItems id="nonforList" value="#{pc_contract.nonforfeitureList}" />
							</h:selectOneMenu>
					</h:panelGroup>
					<h:outputText id="divOption" value="Dividend Option" styleClass="formSectionBar"/>
					<h:panelGroup id="DivOptPGroup" styleClass="formDataEntryLine">
						<h:panelGroup id="primaryGroup" styleClass="formDataEntryLine">
							<h:outputText id="primary" value="#{property.covPartyPrimaryDO}" styleClass="formLabel" />
							<h:selectOneMenu id="primaryDD" value="#{pc_contract.primaryDivOpt}" styleClass="formEntryText" style="width: 460px;" disabled="#{pc_contract.vantageBESSystem}">	
								<f:selectItems id="primaryList" value="#{pc_contract.primaryDivOptList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="secondaryGroup" styleClass="formDataEntryLine">
							<h:outputText id="secondary" value="#{property.covPartySecondaryDO}" styleClass="formLabel" />
							<h:selectOneMenu id="secondaryDD" value="#{pc_contract.secondaryDivOpt}" styleClass="formEntryText" style="width: 460px;">	
								<f:selectItems id="secondaryList" value="#{pc_contract.secondaryDivOptList}" />
							</h:selectOneMenu>
						</h:panelGroup>
					</h:panelGroup>
				</h:panelGroup>
				
				<f:subview id="wmABeneCoverage" rendered="#{pc_coveragePartyNavigation.vantageBESSystem and pc_coveragePartyNavigation.annuity}">
					<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientBeneficiaryTableWma.jsp"/>
					
				</f:subview>

				<h:panelGroup styleClass="formButtonBar" style="margin-top: 20px;">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
						action="#{pc_coveragePartyNavigation.actionCloseSubView}" immediate="true"  />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
						action="#{pc_contract.actionClear}" />
					
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_contract.actionUpdate}" />
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>