<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- NBA329			NB-1401     Retain Denied Coverage and Benefit -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="createUpdateCoverageArea" styleClass="inputFormMat">
		<h:panelGroup id="createUpdateCoverageForm" styleClass="inputForm" style="height: 100%">
			<!-- begin NBA329 --> 
			<h:panelGroup id="benefitFormTitle" styleClass="formTitleBar">
				<h:outputText id="createTitle" value="#{property.contractChgAddBenefitTitle}" rendered="#{pc_benefit.add}" />
				<h:outputText id="updateTitle" value="#{property.contractChgUpdateBenefitTitle}" rendered="#{!pc_benefit.add}" />
				<h:outputText id="updateTitleDenied" value="#{property.deniedSuffix}" style="padding-left: 5px;"
								rendered="#{!pc_benefit.add and pc_benefit.denied}" />
			</h:panelGroup>
			<!-- end NBA329 -->
			<h:panelGroup id="addUpdateBenefitArea" styleClass="formDataEntryLine">
				<h:panelGroup id="benefitPGroup" styleClass="formDataEntryLine">
					<h:outputText id="benefitType" value="#{property.contractChBenefit}" styleClass="formLabel" />
					<h:selectOneMenu id="benefitTypeDD" value="#{pc_benefit.benefitType}" 
						valueChangeListener="#{pc_benefit.benefitTypeChange}"
						styleClass="formEntryText" 
						disabled="#{pc_benefit.inherent}"
						style="width: 350px;">	<!-- NBA215 -->
						<f:selectItems id="benefitTypeList" value="#{pc_benefit.benefitTypeList}" />					
					</h:selectOneMenu>
					<h:outputLabel value="#{property.contractChJoint}" styleClass="formLabelRight" rendered="#{pc_benefit.joint}" />
				</h:panelGroup>
				<h:panelGroup id="effectivePGroup" styleClass="formDataEntryLine">
					<h:outputText id="effective" value="#{property.contractChEffDate}" styleClass="formLabel" />
					<h:inputText id="effectiveMontInpt" value="#{pc_benefit.effectiveMonth}" maxlength="2"  styleClass="formEntryText" style="width: 50px;" >	<!-- NBA215 -->
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
					<h:outputText id="effectiveMonth" value="#{property.contractChIssueMonth}" styleClass="formLabelRight"
						style="width: 70px; text-align: left; padding-left: 10px;" />
					<h:inputText id="effectiveYearInpt" value="#{pc_benefit.effectiveYear}" maxlength="4"  styleClass="formEntryText" style="width: 50px;" >	<!-- NBA215 -->
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
					<h:outputText id="effectiveYear" value="#{property.contractChIssueYear}" styleClass="formLabelRight"
						style="width: 70px; text-align: left; padding-left: 10px;" />
				</h:panelGroup>
				<h:panelGroup id="ceasePGroup" styleClass="formDataEntryLine">
					<h:outputText id="cease" value="#{property.contractChCease}" styleClass="formLabel" />
					<h:inputText id="ceaseMontInpt" value="#{pc_benefit.ceaseMonth}" maxlength="2" styleClass="formEntryText" style="width: 50px;">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>
					<h:outputText id="ceaseMonth" value="#{property.contractChIssueMonth}" styleClass="formLabelRight"
						style="width: 70px; text-align: left; padding-left: 10px;" />
					<h:inputText id="ceaseYearInpt" value="#{pc_benefit.ceaseYear}" maxlength="4" styleClass="formEntryText" style="width: 50px;">
						<f:convertNumber integerOnly="true" type="integer" />
					</h:inputText>	
					<h:outputText id="ceaseUnits" value="#{property.contractChIssueYear}" styleClass="formLabelRight"
						style="width: 70px; text-align: left; padding-left: 10px;" />
					<h:outputText id="units1" value="#{property.contractChUnits}" styleClass="formLabel" style="width: 70px; margin-left: -20px;"  rendered="#{!pc_benefit.amountRendered}"/>
					

					<h:inputText id="unitsInpt1" value="#{pc_benefit.units}" styleClass="formEntryText" style="width: 90px;" disabled="#{pc_benefit.disabledUnits}" rendered="#{!pc_benefit.amountRendered}">
						<f:convertNumber type="double"/>
					</h:inputText>
					


					<h:outputText id="amountLabel" value="#{property.covPartyAmount}" rendered="#{pc_benefit.amountRendered}" styleClass="formLabel" style="width: 70px; margin-left: -20px;" />
					<h:inputText id="amount" value="#{pc_benefit.amount}" rendered="#{pc_benefit.amountRendered}" styleClass="formEntryText" style="width: 90px;">
							<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>
				</h:panelGroup>
				<h:panelGroup id="IDBGroup" styleClass="formDataEntryLine" rendered="#{pc_benefit.IDB and pc_benefit.UL}">
					<h:panelGroup id="IDBCalcGroup" styleClass="formDataEntryLine">
						<h:outputText id="idbCalcMethod" value="#{property.covPartyIDBCalcMethod}" styleClass="formLabel" />
						<h:selectOneMenu id="calcMethodDD" value="#{pc_benefit.calcMethod}" styleClass="formEntryText" style="width: 410px;">	<!-- NBA215 -->
							<f:selectItems id="calcMethodList" value="#{pc_benefit.calcMethodList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup id="idbAmounts" styleClass="formDataEntryLine">
						<h:outputText id="idbAnnPcnt" value="#{property.covPartyIDBAnnPercent}" styleClass="formLabel" />
						<h:inputText id="idbAnnPcntInpt" value="#{pc_benefit.annPercentage}" maxlength="2"  styleClass="formEntryText" style="width: 50px;" >	<!-- NBA215 -->
							<f:convertNumber type="percent" pattern="###.##" />
						</h:inputText>
						<h:outputText id="idbYears" value="#{property.covPartyIDBYears}" styleClass="formLabel"
							style="width: 70px; text-align: left; padding-left: 20px;" />
						<h:inputText id="idbYearsInpt" value="#{pc_benefit.numberOfYears}" maxlength="4"  styleClass="formEntryText" style="width: 50px;" >	<!-- NBA215 -->
							<f:convertNumber integerOnly="true" type="integer" />
						</h:inputText>
					</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup id="wmaWPGroup" styleClass="formDataEntryLine" rendered="#{pc_benefit.WP and pc_benefit.vantageBESSystem}">
					<h:panelGroup id="wmAWaiverUsageGroup" styleClass="formDataEntryLine">
						<h:outputText id="pwRateUsage" value="#{property.covPartyPWUsage}" styleClass="formLabel" />
						<h:selectOneMenu id="pwRateUsageDD" value="#{pc_benefit.pwRateUsage}" styleClass="formEntryText" style="width: 410px;">	<!-- NBA215 -->
							<f:selectItems id="pwRateUsageList" value="#{pc_benefit.pwRateUsageList}" />
						</h:selectOneMenu>
					</h:panelGroup>					
					<h:panelGroup id="wmAWaiverRateFactorGroup" styleClass="formDataEntryLine" style="padding-top:10px">
						<h:outputText id="pwRateFactor" value="#{property.covPartyPWRateFactor}" styleClass="formLabel" />
						<h:inputText id="pwRateFactorInpt" value="#{pc_benefit.pwRateFactor}" maxlength="6"  styleClass="formEntryText" style="width: 70px;" >	<!-- NBA215 -->
							<f:convertNumber type="percent" pattern="###.##" />
						</h:inputText>
					</h:panelGroup>
				</h:panelGroup>
				
				<h:panelGroup styleClass="formButtonBar" style="margin-top: 20px;">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
						action="#{pc_coveragePartyNavigation.actionCloseSubView}" immediate="true"  />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
						action="#{pc_benefit.actionClear}" />
					<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_benefit.actionAddNew}"
						rendered="#{pc_benefit.add}" />
					<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_benefit.actionAdd}"
						rendered="#{pc_benefit.add}" />
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_benefit.actionUpdate}"
						rendered="#{!pc_benefit.add}" disabled="#{pc_benefit.denied}"/>  <!-- NBA329 -->
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>