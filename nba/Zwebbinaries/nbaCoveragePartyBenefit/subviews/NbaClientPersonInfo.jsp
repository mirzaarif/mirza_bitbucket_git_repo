<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID-->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	
	<h:panelGroup id="personalDetailsGroup" >
		<h:panelGroup id="personDetailsheadings1" styleClass="formDataDisplayTopLine" style="margin-bottom:-15px;">
			<h:outputLabel id="personPName" value="#{property.covPartyPrefix}" styleClass="formLabel" style="width: 50px;margin-left:125px;" />
			<h:outputLabel id="personFName" value="#{property.covPartyFirst}" styleClass="formLabel"  style="width: 65px;"/>
			<h:outputLabel id="personMName" value="#{property.covPartyMiddle}" styleClass="formLabel" style="width: 110px;"/>
			<h:outputLabel id="personLName" value="#{property.covPartyLast}" styleClass="formLabel" style="width: 70px;"/>
			<h:outputLabel id="personSName" value="#{property.covPartySuffix}" styleClass="formLabel" style="width: 155px;"/>
		</h:panelGroup>	

		<h:panelGroup id="personDetailsSubSection1" styleClass="formDataDisplayTopLine" style="">
			<h:outputLabel id="personName" value="#{property.covPartyName}" styleClass="formLabel" />
			<h:selectOneMenu id="prefixDD" value="#{pc_NbaClientDetails.prefix}" styleClass="formEntryText" style="width: 75px;">
				<f:selectItems id="prefixList" value="#{pc_NbaClientDetails.prefixList}" />
			</h:selectOneMenu>
			<h:inputText id="personFNameText" value="#{pc_NbaClientDetails.firstName}" styleClass="formEntryText" style="width: 100px" />
			<h:inputText id="personMNameText" value="#{pc_NbaClientDetails.middleInitial}" styleClass="formEntryText" style="width: 75px" />
			<h:inputText id="personLNameText" value="#{pc_NbaClientDetails.lastName}" styleClass="formEntryText" style="width: 150px" />
			<h:selectOneMenu id="suffixDD" value="#{pc_NbaClientDetails.suffix}" styleClass="formEntryText" style="width: 85px;">
				<f:selectItems id="suffixList" value="#{pc_NbaClientDetails.suffixList}" />
			</h:selectOneMenu>
		</h:panelGroup>		
		<h:panelGroup id="personDetailsSubSection2" styleClass="formDataDisplayLine">
			<h:outputLabel id="personBirth" value="#{property.covPartyDob}" styleClass="formLabel"  />
			<h:inputText id="DOBEF" value="#{pc_NbaClientDetails.birthDate}" styleClass="formEntryText" style="width: 125px;">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputText id="birthState" value="#{property.covPartyBirthState}" styleClass="formLabel" style="width: 175px;"/>
			<h:selectOneMenu id="birthStateDD" value="#{pc_NbaClientDetails.birthState}" styleClass="formEntryText" style="width: 185px;">
				<f:selectItems id="birthStateList" value="#{pc_NbaClientDetails.stateList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSectionGender" styleClass="formDataDisplayLine">
			<h:outputLabel id="personGender" value="#{property.covPartyGender}" styleClass="formLabel" />
			<h:selectOneMenu id="genderDD" value="#{pc_NbaClientDetails.gender}" styleClass="formEntryText" style="width: 185px;">
				<f:selectItems id="genderList" value="#{pc_NbaClientDetails.genderList}" />
			</h:selectOneMenu>
			<h:outputLabel id="personLicense" value="#{property.personLicense}" styleClass="formLabel" style="width: 115px;"/>
			<h:inputText id="personLicenseText" value="#{pc_NbaClientDetails.license}" styleClass="formDisplayText" style="width: 185px;"/>
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSectionLicense" styleClass="formDataDisplayLine">
			<h:outputLabel id="heightFt" value="#{property.covPartyHeight}" styleClass="formLabel" />
			<h:inputText id="heightFtEF" value="#{pc_NbaClientDetails.heightFeet}" styleClass="formEntryText" style="width: 50px" />
			<h:inputText id="heightInEF" value="#{pc_NbaClientDetails.heightInches}" styleClass="formEntryText" style="width: 50px" />
			<h:outputText id="licenseState" value="#{property.covPartyLicState}" styleClass="formLabel" style="width: 200px;"/>
			<h:selectOneMenu id="licenseDD" value="#{pc_NbaClientDetails.licenseState}" styleClass="formEntryText" style="width: 185px;"> 
				<f:selectItems id="licenseList" value="#{pc_NbaClientDetails.stateList}" />
			</h:selectOneMenu>
			
		</h:panelGroup>	
		<h:panelGrid id="persongrid"  columnClasses="formSplitLeft" style="margin-top:10px;margin-bottom:10px"
			columns="2" cellpadding="0" cellspacing="0">
			<h:column>
				<h:panelGroup id="personDetailsSubSection3" styleClass="formDataDisplayLine">
					<h:outputLabel id="weight" value="#{property.covPartyWeight}" styleClass="formLabel"/>
					<h:inputText id="WeightEF" value="#{pc_NbaClientDetails.weight}" styleClass="formDisplayText" style="width: 50px">
							<f:convertNumber type="double"/>
					</h:inputText>
				</h:panelGroup>	
				<h:panelGroup id="personDetailsSubSection4" styleClass="formDataDisplayLine">
					<h:outputLabel id="personResidenceState" value="#{property.personResidenceState}" styleClass="formLabel" />
					<h:selectOneMenu id="resStateDD" value="#{pc_NbaClientDetails.residenceState}" styleClass="formEntryText" style="width: 185px;">
						<f:selectItems id="resStateList" value="#{pc_NbaClientDetails.stateList}" />
					</h:selectOneMenu>
				</h:panelGroup>	
				<h:panelGroup id="personDetailsCit" styleClass="formDataDisplayLine">
					<h:outputLabel id="personCitizenship" value="#{property.personCitizenship}" styleClass="formLabel" />
					<h:selectOneMenu id="CitizenshipDD" value="#{pc_NbaClientDetails.citizenship}" styleClass="formEntryText" style="width: 185px;"> 
						<f:selectItems id="CitizenshipList" value="#{pc_NbaClientDetails.usCitizenshipList}" />
					</h:selectOneMenu>		
				</h:panelGroup>	
				<h:panelGroup id="personDetailsSubSection5" styleClass="formDataDisplayLine">
					<h:outputLabel id="personMaritalStatus" value="#{property.personMaritalStatus}" styleClass="formLabel" />
					<h:selectOneMenu id="maritalStatusDD" value="#{pc_NbaClientDetails.maritalStatus}" styleClass="formEntryText" style="width: 185px;"> 
						<f:selectItems id="maritalStatusList" value="#{pc_NbaClientDetails.marStatusList}" />
					</h:selectOneMenu>	
				</h:panelGroup>	
				<h:panelGroup id="personDetailsSubSection6" styleClass="formDataDisplayLine">
					<h:outputLabel id="personOccupation" value="#{property.personOccupation}" styleClass="formLabel" />
					<h:selectOneMenu id="occupationDD" value="#{pc_NbaClientDetails.occupation}" styleClass="formEntryText" style="width: 185px;"> 
						<f:selectItems id="occupationList" value="#{pc_NbaClientDetails.occupationList}" />
					</h:selectOneMenu>	
				</h:panelGroup>	
				<h:panelGroup id="personDetailsSubSection7" styleClass="formDataDisplayLine">
					<h:outputLabel id="personAnnualIncome" value="#{property.personAnnualIncome}" styleClass="formLabel" />
					<h:inputText id="personAnnualIncomeText" value="#{pc_NbaClientDetails.annualIncome}" styleClass="formEntryText" style="width: 185px" >
						<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>
				</h:panelGroup>	
				<h:panelGroup id="personDetailsSubSection9" styleClass="formDataDisplayLine">
					<h:outputLabel id="personNetWorth" value="#{property.personNetWorth}" styleClass="formLabel" />
					<h:inputText id="personNetWorthText" value="#{pc_NbaClientDetails.netWorth}" styleClass="formEntryText" style="width: 185px" > 
						<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:inputText>			
				</h:panelGroup>
			</h:column>
			<h:column>	
				<h:outputLabel id="roles" value="#{property.covPartyRoles}" styleClass="formLabel" style="vertical-align: top;width:110px; margin-top:10px" />
				<h:panelGroup>
					<h:selectManyListbox id="rolesList" styleClass="formEntryTextHalf"
						value="#{pc_NbaClientDetails.roles}" 
						valueChangeListener="#{pc_NbaClientDetails.rolesValChange}" onclick="submit()" immediate="true"
						style="height: 180px; width: 185px; margin-top:10px">
							<f:selectItems value="#{pc_NbaClientDetails.rolesList}" />
					</h:selectManyListbox>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
		<h:panelGrid id="pinsGovtIdPGrid" columnClasses="formDataEntryLineAppEntry" columns="3" cellpadding="0" cellspacing="0">
			<h:column>
				<h:outputLabel id="govtIDLabel" value="Tax Information:" styleClass="formLabel" style="margin-bottom:45px"/>  <!-- NBA340 -->
			</h:column>
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_NbaClientDetails.govtTC}" 
					layout="pageDirection" styleClass="radioLabel" style="width: 151px;margin-left:-5px" >  <!-- NBA340 -->
					<f:selectItem id="ssnRB" itemValue="1" itemLabel="#{property.appEntSsNumber}"/>
					<f:selectItem id="sinRB" itemValue="3" itemLabel="#{property.appEntSiNumber}"/>
					<f:selectItem id="TinRB" itemValue="2" itemLabel="#{property.appEntTiNumber}"/>
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<!-- begin NBA340 -->
				<h:inputText id="govtIdInput" value="#{pc_NbaClientDetails.govtId}" styleClass="formEntryText"
							style="width: 115px;margin-top: 0px;margin-bottom:45px;" maxlength="9" /> 
				<h:inputHidden id="govtIdKey" value="#{pc_NbaClientDetails.govtIdKey}"></h:inputHidden>		
				<!-- end NBA340 -->
			</h:column>		
		</h:panelGrid>	

		<h:panelGroup id="verificationcol2" styleClass="formDataDisplayLine" style="margin-bottom:1px">
			<h:outputLabel id="verification" value="#{property.covPartyVerification}" styleClass="formLabel" />
			<h:selectOneMenu id="verificationDD" value="#{pc_NbaClientDetails.taxIDVerification}" styleClass="formEntryText" style="width: 260px;">
				<f:selectItems id="verificationList" value="#{pc_NbaClientDetails.taxIDVerificationList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="personDetailsSubSection8" styleClass="formDataDisplayLine" style="margin-bottom:15px">
			<h:outputLabel id="personWithholding" value="#{property.personWithholding}" styleClass="formLabel" />
			<h:selectOneMenu id="personWithholdingDD" value="#{pc_NbaClientDetails.withholding}" styleClass="formEntryText" style="width: 185px;"> 
				<f:selectItems id="personWithholdingList" value="#{pc_NbaClientDetails.withholdingList}" />
			</h:selectOneMenu>	
		</h:panelGroup>	
				
		<h:panelGroup id="clientPersonRateClassHeader" styleClass="sectionSubheader">
			<h:outputLabel id="clientPersonRateClassTitle" value="#{property.personRateClassTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="personRateClassTobaccoSubSection3" styleClass="formDataDisplayLine">
			<h:outputLabel id="personRateClassOverrideCB" value="#{property.personRateClassOverride}" styleClass="formLabel" />
			<h:selectBooleanCheckbox id="personRateClassOverrideCheckBox" value="#{pc_NbaClientDetails.rateClassOverrideInd}"  
				styleClass="formEntryCheckbox" disabled="true"/>
			<h:outputLabel id="personRateClassOverrideText" value="#{pc_NbaClientDetails.rateClassSystem}" styleClass="formLabelRight" />
		</h:panelGroup>
		<h:panelGroup id="personRateClassSubSection1" styleClass="formDataDisplayTopLine" style="padding-bottom:10px">
			<h:outputLabel id="personRateClassAppliedFor" value="#{property.personRateClassAppliedFor}" styleClass="formLabel" />
			<h:selectOneMenu id="personRateClassAppliedForDD" value="#{pc_NbaClientDetails.rateClassAppliedFor}" styleClass="formEntryText" style="width: 175px;"> 
				<f:selectItems id="personRateClassAppliedForList" value="#{pc_NbaClientDetails.rateClassOverrideList}" />
			</h:selectOneMenu>	
		</h:panelGroup>				
	</h:panelGroup>	
</jsp:root>

