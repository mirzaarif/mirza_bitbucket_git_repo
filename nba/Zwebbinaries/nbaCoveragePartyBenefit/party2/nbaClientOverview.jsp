<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		NB-1301   Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPRNBA-747     NB-1401   General Code Clean Up -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>"> 
	<title>Client Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_ClientOverview2'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_ClientOverview2'].target='';
			return false;
		}
		function setParentFrame() {
				//alert('Setting Parent Frame');
			document.forms['form_ClientOverview2'].target='nbFile';		
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: hidden;"> 
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_COV_PARTY_CLIENT2" value="#{pc_covPartyClientTable2}" />
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_ClientOverview2">
			<h:panelGroup id="clientHeaderTable2" styleClass="ovDivTableHeader">
				<h:panelGrid columns="2" styleClass="ovTableHeader"
						columnClasses="ovColHdrText155,ovColHdrText295"
						cellspacing="0">
					<h:outputLabel id="clientHdrCol1Table2" value="Role" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol2Table2" value="Detail" styleClass="ovColSortedFalse" />
				</h:panelGrid>
			</h:panelGroup>
			<!-- Table Columns -->
			<h:panelGroup id="client2ConTab2" styleClass="ovDivTableData" style="height: 225px;">
				<h:dataTable id="clientContract2Table" styleClass="ovTableData" 
							cellspacing="0" 
							rows="0"
							binding="#{pc_covPartyClientTable2.dataTable}"
							value="#{pc_covPartyClientTable2.rows}" var="nbaClient"
							rowClasses="#{pc_covPartyClientTable2.rowStyles}"
							style="min-height:19px;"
							columnClasses="ovColText155,ovColText295ML">
					<h:column id ="Client2ConCol1">
						<h:panelGroup id ="ClientConSpan2" styleClass="ovFullCellSelect" style="width: 200%;">
							<h:commandButton id="clientIcon1Table1" image="images/hierarchies/#{nbaClient.icon1}"
										styleClass="#{nbaClient.icon1StyleClass}" style="margin-left: 5px; margin-top: -6px;"
										rendered="#{nbaClient.icon1Rendered}"
										action="#{pc_covPartyClientTable2.selectSingleRow}"
										immediate="true" />
							<h:commandButton id="clientIcon2Table1" image="images/hierarchies/#{nbaClient.icon2}"
										styleClass="#{nbaClient.icon2StyleClass}" style="margin-left: 5px; margin-top: -6px;"
										rendered="#{nbaClient.icon2Rendered}" 
										action="#{pc_covPartyClientTable2.selectSingleRow}"
										immediate="true" />
							<h:commandLink id="clientCol1Table1" value="#{nbaClient.col1}" style="vertical-align: top; color: #000000;"
										action="#{pc_covPartyClientTable2.selectSingleRow}" immediate="true" />
						</h:panelGroup>
					</h:column>
					<h:column id ="client1ConCol2">
						<h:commandLink id="clientCol2Table2" 
								action="#{pc_covPartyClientTable2.selectSingleRow}" 
								title="#{nbaClient.col3}"
								immediate="true"> 						
								<h:inputTextarea id="client2Title" readonly="true" value="#{nbaClient.col2}" styleClass="ovMultiLine#{nbaClient.draftText}"
									style="width:600px;"/>
						</h:commandLink> 
					</h:column>
					
				</h:dataTable>  
			</h:panelGroup>
			<!-- Button bar -->
			<h:panelGroup id="client2Button2" styleClass="ovButtonBar" style="width: 628px">
					<h:commandButton id="btnClient2Delete" value="#{property.buttonDelete}" styleClass="ovButtonLeft" style="margin-left: 5px"
							action="#{pc_covPartyClientTable2.actionDelete}"
							disabled="#{pc_covPartyClientTable2.deleteDisabled}"
							onclick="setTargetFrame()"
							immediate="true" />
			
				<h:commandButton id="btnClient2View" value="#{property.buttonViewUpdate}" styleClass="ovButtonRight-1"
							style="width: 100px;left: 432px;" 
							disabled="#{pc_covPartyClientTable2.viewDisabled}"
							action="#{pc_covPartyClientTable2.actionView}"
							onclick="setParentFrame()" 
							immediate="true" />  <!-- SPRNBA-747 -->
							
				<h:commandButton id="btnAddClient2" value="#{property.buttonAddClient}"styleClass="ovButtonRight" 
							action="#{pc_covPartyClientTable2.actionAddClient}"
							onclick="setParentFrame();"
							immediate="true" /> 
			</h:panelGroup>
			<h:outputLabel id="tabIndex" value="#{pc_covPartyClientTable2.currentIndex}" style="visibility:hidden;" />
		</h:form>
		<div id="Messages" style="display:none"><h:messages /></div>
	</f:view>
</body>
</html>