<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite --%>
<%-- SPRNBA-798      NB-1401   	 Change JSTL Specification Level --%>
<%-- SPRNBA-802  	 NB-1401	 Coverage Party Business Function Hangs On Commit --%>
<%-- SPRNBA-978  	 NB-1601	 Code Cleanup --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Coverage Party Benefit View</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>	
	<script language="JavaScript" type="text/javascript">
		<!--
			var contextpath = '<%=path%>';	
			var topOffset = 9;
			var leftOffset = 5;
			var fileLocationHRef = '<%=basePath%>' + 'nbaCoveragePartyBenefit/nbaCovPartyBenOverview.faces';

			function setTargetFrame() {
				//alert('Setting Target Frame');
				document.forms['form_covPartyOverview'].target='controlFrame';		
				return false;
			}
			function resetTargetFrame() {
				//alert('Resetting Target Frame');
				document.forms['form_covPartyOverview'].target='';
				return false;
			}
		
		//-->
	</script>
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll; height:100%">
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_COVPARTYNAV" value="#{pc_coveragePartyNavigation}" />
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		
		<h:form id="form_covPartyOverview">
			<f:subview id="navigation">
				<c:import url="/nbaCoveragePartyBenefit/subviews/NbaCoveragePartyNavigation.jsp" />
			</f:subview> 

			<iframe id="covPartyParty1" name="fdParty1" src="<%=path%>/nbaCoveragePartyBenefit/nbaCovPartyBenParty1.faces" onload=""
								height="345px" width="642px" frameborder="0" scrolling="no"></iframe>
<%
	if (com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getFinalDispParty2() != null) {
 %>
			<iframe id="covPartyParty2" name="fdParty2" src="<%=path%>/nbaCoveragePartyBenefit/nbaCovPartyBenParty2.faces" onload=""
								height="345px" width="642px" frameborder="0" scrolling="no"></iframe>
<%
	}
 %>

			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" /> 
			</f:subview>
			<h:panelGroup id="clientOverViewBut" styleClass="tabButtonBar">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"
						action="#{pc_coveragePartyNavigation.actionRefresh}" onclick="resetTargetFrame()"/>
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight"
						disabled="#{pc_coveragePartyNavigation.auth.enablement['Commit'] || 
									pc_coveragePartyNavigation.notLocked|| 
									pc_coveragePartyNavigation.negativeDisposition || 
									pc_coveragePartyNavigation.issued}" 
						action="#{pc_coveragePartyNavigation.actionCommit}" onclick="resetTargetFrame()"/> <%-- SPRNBA-802 --%>
			</h:panelGroup>
			<%-- SPRNBA-978 Code Deleted --%>
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>