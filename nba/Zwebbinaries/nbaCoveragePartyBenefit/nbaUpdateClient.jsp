<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340-->
<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->

<script language="JavaScript" type="text/javascript">
		var fileLocationHRef  = '<%=basePath%>' + 'nbaCoveragePartyBenefit/nbaUpdateClient.faces';	
		var contextpath = '<%=path%>'; //NBA340
		function setTargetFrame() {		  
			getScrollXY('form_ContactDetails');
			document.forms['form_ContactDetails'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {	
			getScrollXY('form_ContactDetails');	 
			document.forms['form_ContactDetails'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_ContactDetails']['form_ContactDetails:draftChanges'].value;  
		}

		//NBA340 new Function
		$(document).ready(function() {
			$("input[name$='govtIdType']").govtidType();			
	        $("input[name$='govtIdInput']").govtidValue();
		})	
		
		//NBA340 new Function
		function checkGovtIdError(){
		  if(!$(this).checkErrorMessage()){
		     setTargetFrame();
		  }else{
		     resetTargetFrame();
		  }
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> 	
</head>
<body class="whiteBody" onload="filePageInit();scrollToCoordinates('form_ContactDetails')" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<PopulateBean:Load serviceName="RETRIEVE_COV_PARTY_CLIENT_INFO" value="#{pc_NbaClientDetails}" />
		<PopulateBean:Load serviceName="RETRIEVE_COV_PARTY_CLIENT_INFO" value="#{pc_NbaClientContactInfoTableData}" />
		<PopulateBean:Load serviceName="RETRIEVE_COV_PARTY_CLIENT_INFO" value="#{pc_NbaClientBeneficiaryTableData}" />
		<h:form id="form_ContactDetails" onsubmit="getScrollXY('form_ContactDetails')">
			<h:inputHidden id="scrollx" value="#{pc_NbaClientDetails.scrollXC}"></h:inputHidden>
			<h:inputHidden id="scrolly" value="#{pc_NbaClientDetails.scrollYC}"></h:inputHidden>	
		<f:subview id="navigationtable"> 
			<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientTable.jsp" /> 
		</f:subview>
		<!-- SPRNBA-576 code deleted -->
			
			<div id="viewBenDetails" class="inputFormMat">
				<div class="inputForm">
					<h:panelGroup id="clientPersonDetailsHeader" styleClass="sectionHeader">
						<h:outputLabel id="clientPersonDetailsTitle1" value="Client" styleClass="shTextLarge" style="margin-left:5px"/>
					</h:panelGroup>
					<h:panelGroup id="corpOrPersonSection" >
						<h:selectOneRadio id="selectRadioGroup" value="#{pc_NbaClientDetails.partyType}" disabled="#{(pc_NbaClientDetails.personParty || pc_NbaClientDetails.organizationParty) && !pc_NbaClientDetails.add}" 
							valueChangeListener="#{pc_NbaClientDetails.typeChange}"  onclick="submit()" immediate="true"  
							layout="lineDirection" styleClass="radioLabel" style="width:180px; margin-left:120px; margin-top:10px">
							<f:selectItem id="individual" itemValue="1" itemLabel="#{property.appEntPerson}"/>
							<f:selectItem id="group" itemValue="2" itemLabel="#{property.appEntCorporation}"/>
					</h:selectOneRadio>	 			
					</h:panelGroup>	
					<f:subview id="clientPersonDetails" rendered="#{pc_NbaClientDetails.personRendered}">
						<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientPersonInfo.jsp" />
					</f:subview>
					<f:subview id="clientOrgDetails"  rendered="#{!pc_NbaClientDetails.personRendered}">
						<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientOrgInfo.jsp"/>
					</f:subview>
					<f:subview id="clientContactInfoTable">
						<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientContactInfoTable.jsp"/>
					</f:subview>
					
					
					<%
					if (!(Boolean) com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getCoveragePartyNavigation().isVantageBESSystem()
						&& (Boolean) com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getClientDetails().isInsurable()) {
 					%>
						<f:subview id="clientBeneficiaryTable">
							<c:import url="/nbaCoveragePartyBenefit/subviews/NbaClientBeneficiaryTable.jsp"/>
						</f:subview>
					<%
					} 
 					%>
					<hr class="formSeparator" />
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true" onclick="resetTargetFrame();" action="#{pc_finalDispNavigation.actionCloseSubView}" /> 
						<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
							action="#{pc_NbaClientDetails.actionClear}" />
						<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_NbaClientDetails.actionAddNew}" onclick = "checkGovtIdError();"
							rendered="#{pc_NbaClientDetails.add}" /> <!--NBA340-->
						<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" onclick = "checkGovtIdError();"
						action="#{pc_NbaClientDetails.actionAdd}"
							rendered="#{pc_NbaClientDetails.add}" /> <!--NBA340-->
						<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" 
							 action="#{pc_NbaClientDetails.actionUpdate}" onclick = "checkGovtIdError();"
							rendered="#{!pc_NbaClientDetails.add}" 	disabled="#{pc_NbaClientDetails.updateDisabled}" />  <!-- NBA329 --> <!--NBA340-->
					</h:panelGroup>
				</div>
			</div>
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />  
			</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
	</f:view>
</body>
</html>