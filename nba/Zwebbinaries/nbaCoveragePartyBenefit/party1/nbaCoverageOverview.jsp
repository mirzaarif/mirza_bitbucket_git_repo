<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA245     	NB-1301   Coverage/Party User Interface Rewrite  -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- SPRNBA-747     NB-1401   General Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Party 1 Coverage Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">		
		function setTargetFrame() {
				//alert('Setting Target Frame');
			document.forms['form_CoverageOverview'].target='controlFrame';		
			return false;
		}
		function resetTargetFrame() {
				//alert('Resetting Target Frame');
			document.forms['form_CoverageOverview'].target='';
			return false;
		}
		function setParentFrame() {
			document.forms['form_CoverageOverview'].target='nbFile';		
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> 
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: hidden">
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_COV_PARTY_BEN1" value="#{pc_covPartyCovTable1}" />
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_CoverageOverview">
			<!-- Table Column Headers -->
			<h:panelGroup id="covClient1TabHeader" styleClass="ovDivTableHeader">
				<h:panelGrid columns="5" styleClass="ovTableHeader"
							columnClasses="ovColHdrIcon,ovColHdrText225,ovColHdrText190,ovColHdrDate,ovColHdrDate"
							cellspacing="0">
					<h:outputLabel id="clientHdrCol0" value="" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol1" value="#{property.uwCovClientCol1}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol3" value="#{property.uwCovClientCol3}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol4" value="#{property.uwCovClientCol4}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol5" value="#{property.uwCovClientCol5}" styleClass="ovColSortedFalse" />
				</h:panelGrid>
			</h:panelGroup>
			<!-- Table Columns -->
			<h:panelGroup id="covClient1TabData" styleClass="ovDivTableData" style="height: 225px;">
				 <h:dataTable id="coverageTable1" styleClass="ovTableData"
								cellspacing="0" 
								cellpadding="0"
								rows="0" 
								binding="#{pc_covPartyCovTable1.dataTable}"  
								value="#{pc_covPartyCovTable1.rows}" 
								var="nbaCoverage" 
								rowClasses="#{pc_covPartyCovTable1.rowStyles}"
								style="min-height:19px;"
								columnClasses="ovColIcon,ovColText225,ovColText190,ovColText85,ovColText85" >
					<h:column id="covCol1">
						<!-- begin NBA329 -->
						<h:commandButton id="covCol1_insured" image="images/coverage/insured.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.insured && !nbaCoverage.denied}"
									action="#{pc_uwCovTable1.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_insuredDenied" image="images/coverage/insured-denied.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.insured && nbaCoverage.denied}"
									action="#{pc_uwCovTable1.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_base" image="images/coverage/base-coverage.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.baseCoverage}"
									action="#{pc_uwCovTable1.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_denied" image="images/coverage/denied.gif" styleClass="ovViewIconTrue"
									rendered="#{!nbaCoverage.insured && nbaCoverage.denied}"
									action="#{pc_uwCovTable1.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_none" image="images/needs_attention/clear.gif" styleClass="ovViewIconFalse"
									rendered="#{!(nbaCoverage.insured || nbaCoverage.denied || nbaCoverage.baseCoverage)}" />
						<!-- end NBA329 -->
					</h:column>
					<h:column id="covCol2">
						<h:panelGroup id ="covSpan1"> 
							<h:commandButton id="covIcon11" image="images/hierarchies/#{nbaCoverage.icon1}" rendered="#{nbaCoverage.icon1Rendered}"
								styleClass="#{nbaCoverage.icon1StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" />
							<h:commandButton id="covIcon21" image="images/hierarchies/#{nbaCoverage.icon2}" rendered="#{nbaCoverage.icon2Rendered}"
								styleClass="#{nbaCoverage.icon2StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;"/>
							<h:commandButton id="covIcon31" image="images/hierarchies/#{nbaCoverage.icon3}" rendered="#{nbaCoverage.icon3Rendered}"
								styleClass="#{nbaCoverage.icon3StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" /> 
							<h:commandButton id="covIcon41" image="images/hierarchies/#{nbaCoverage.icon4}" rendered="#{nbaCoverage.icon4Rendered}"
								styleClass="#{nbaCoverage.icon4StyleClass}" style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;" /> 
							<h:commandLink id="covCol2a" title="#{nbaCoverage.rateClassText}"
										action="#{pc_covPartyCovTable1.selectRow}" immediate="true"> 						
								<h:inputTextarea id="covCol2LabelLevel1_2" readonly="true" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:175px;" rendered="#{!nbaCoverage.icon3Rendered}"/>
								<h:inputTextarea id="covCol2LabelLevel3" readonly="true" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:165px;" rendered="#{nbaCoverage.icon3Rendered && !nbaCoverage.icon4Rendered}"/>
								<h:inputTextarea id="covCol2LabelLevel4" readonly="true" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:145px;" rendered="#{nbaCoverage.icon4Rendered}"/>
							</h:commandLink> 
						</h:panelGroup>
					</h:column>				
					<h:column id="covCol3" >
						<h:commandLink id="covCol3a" title="#{nbaCoverage.detailHoverText}"
									action="#{pc_covPartyCovTable1.selectRow}" immediate="true">
							<h:inputTextarea id="covita3" readonly="true" value="#{nbaCoverage.col3}" styleClass="ovMultiLine#{nbaCoverage.draftText}" style="width: 175px;"  />
						</h:commandLink> 
					</h:column>
					<h:column id="cov1Col4">
						<h:commandLink id="cov1col4a"  action="#{pc_covPartyCovTable1.selectRow}" immediate="true" styleClass="ovFullCellSelectPrf" > 
								<h:outputText value="#{nbaCoverage.col4}"  styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
									<f:convertDateTime pattern="#{property.datePattern}" />
								</h:outputText>
						</h:commandLink> 
					</h:column>
					<h:column id="cov1Col5">
						<h:commandLink id="cov1col5a"  action="#{pc_covPartyCovTable1.selectRow}" immediate="true" styleClass="ovFullCellSelectPrf"> 
								<h:outputText value="#{nbaCoverage.col5}" styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
									<f:convertDateTime pattern="#{property.datePattern}" />
								</h:outputText>
						</h:commandLink> 
					</h:column>
				</h:dataTable>
			</h:panelGroup>
			<h:panelGroup styleClass="ovButtonBar" style="width: 628px">
				<h:commandButton id="btnCovClientDelete" value="#{property.buttonDelete}" styleClass="ovButtonLeft" style="margin-left: 5px"
							action="#{pc_covPartyCovTable1.actionDelete}"
							disabled="#{pc_covPartyCovTable1.deleteDisabled}"
							onclick="setTargetFrame()"
							immediate="true" />
			
				
				<h:commandButton id="btnCovClientAmendEndorse" value="#{property.buttonAmendEndorse}" styleClass="ovButtonRight-2" style="width: 100px;left: 235px;"
							action="#{pc_covPartyCovTable1.actionAmendEndorse}"
							disabled="#{pc_covPartyCovTable1.amendDisabled}"
							onclick="setParentFrame()"
							immediate="true" />
				<h:commandButton id="btnCovClientView" value="#{property.buttonViewUpdate}" styleClass="ovButtonRight-2" style="left: 340px;"
							action="#{pc_covPartyCovTable1.actionView}"
							disabled="#{pc_covPartyCovTable1.viewDisabled}"
							onclick="setParentFrame();"
							immediate="true" />  <!-- SPRNBA-747 -->
				<h:commandButton id="btnAddCoverage" value="#{property.buttonAddCoverage}" styleClass="ovButtonRight-1" style="width: 100px;left: 432px;" 
							action="#{pc_covPartyCovTable1.actionAddCoverage}"
							disabled="#{pc_covPartyCovTable1.addCoverageDisabled}"
							onclick="setParentFrame();"
							immediate="true" /> 
				<h:commandButton id="btnAddBenefit" value="#{property.buttonAddBenefit}" styleClass="ovButtonRight" 
							action="#{pc_covPartyCovTable1.actionAddBenefit}"
							disabled="#{pc_covPartyCovTable1.addBenefitDisabled}"
							onclick="setParentFrame();"
							immediate="true" /> 
			</h:panelGroup>
			<h:outputLabel id="tabcount" value="#{pc_covPartyCovTable1.currentIndex}" style="visibility:hidden;" />
		</h:form>
	
		<div id="Messages" style="display:none"><h:messages /></div>
	</f:view>
</body>
</html>