<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA242       	NB-1401     Contract Information User Interface Rewrite  -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
    String path = request.getContextPath();
    String basePath = "";
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
		var contextpath = '<%=path%>';
		var fileLocationHRef  = '<%=basePath%>' + '/nbaContractInfo/nbacontractinformation.faces';
		function setTargetFrame() {
			document.forms['form_contractInfo'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractInfo'].target='';
			return false;
		}
	</script>
</head>
<body onload="filePageInit()" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACT_INFORMATION" value="#{pc_contractInformation}" />
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="form_contractInfo">
		<div class="inputFormMat">
			<div class="inputForm">
				<h:panelGroup styleClass="formTitleBar">
					<h:outputText id="contractInfoTitle" value="#{property.infoTitle}" styleClass="shTextLarge" />
				</h:panelGroup>
				<f:subview id="contractInfoReadOnly" rendered="#{!pc_contractInformation.annuityContract}">
					<c:import url="/nbaContractInfo/subviews/contractDetails.jsp" />
				</f:subview>
				<f:subview id="contractTargetInfo" rendered="#{pc_contractTargetTable.targetTableExists}">
					<c:import url="/nbaContractInfo/subviews/contractTargetInfo.jsp" />
				</f:subview>
				<f:subview id="contractInfo">
					<c:import url="/nbaContractInfo/subviews/contractInfo.jsp" />
				</f:subview>
			</div>
	   	</div>
	   	<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup id="contractInfoButtonBar" styleClass="tabButtonBar">
			<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_contractInformation.refresh}" onclick="resetTargetFrame()" styleClass="tabButtonLeft" />
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_contractInformation.commit}"styleClass="tabButtonRight" 
					disabled="#{pc_contractInformation.auth.enablement['Commit'] || pc_contractInformation.notLocked}"/>
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
