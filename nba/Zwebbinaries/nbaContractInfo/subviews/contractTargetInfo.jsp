<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA242      	NB-1401     Contract Information User Interface Rewrite  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="targetTitle" value="#{property.target}"
		styleClass="formSectionBar" style="width:595px; margin-left: 10px; " />
	<h:panelGroup id="targetHeader" styleClass="formDivTableHeader">
		<h:panelGrid columns="3" styleClass="formTableHeader" cellspacing="0"
			style="text-align: center;"
			columnClasses="ovColHdrText190,ovColHdrText190,ovColHdrText190">
			<h:commandLink id="trgtHdrCol1"
				value="#{property.trgtTableType}" styleClass="ovColSorted#{pc_contractTargetTable.sortedByCol1}" actionListener="#{pc_contractTargetTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
			<h:commandLink id="trgtHdrCol2"
				value="#{property.trgtTableAmount}" styleClass="ovColSorted#{pc_contractTargetTable.sortedByCol2}" actionListener="#{pc_contractTargetTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
			<h:commandLink id="trgtHdrCol3" value="#{property.trgtTableEndDate}"  styleClass="ovColSorted#{pc_contractTargetTable.sortedByCol3}" actionListener="#{pc_contractTargetTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="targetTableData" styleClass="formDivTableData12" style="height: 100px">
	<h:dataTable id="targetTableDataTable" styleClass="formTableData"
			cellspacing="0" cellpadding="0" rows="0" 
			binding="#{pc_contractTargetTable.dataTable}"
			value="#{pc_contractTargetTable.targetList}" var="targetItem"
			rowClasses="#{pc_contractTargetTable.rowStyles}"
			columnClasses="ovColText190,ovColText190,ovColText190" >
			<h:column>
				<h:commandLink id="targetCol1" styleClass="ovFullCellSelect" style="width: 100%;" immediate="true">
					<h:outputText value="#{targetItem.type}"> </h:outputText>
				</h:commandLink> 
			</h:column>
			<h:column>
				<h:commandLink id="targetCol2" styleClass="ovFullCellSelect" style="width:100%; text-align:right" immediate="true" >
					<h:outputText value="#{targetItem.amount}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
					</h:outputText>
				</h:commandLink> 
			</h:column>
			<h:column>
				<h:commandLink id="targetCol3" styleClass="ovFullCellSelect" style="width:100%"
					immediate="true">
					<h:outputText value="#{targetItem.endDate}" style="margin-left: 15px;">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:outputText>
					</h:commandLink> 
			</h:column>
		</h:dataTable> 
	</h:panelGroup> 
	<f:verbatim>
		<hr id="contractSeparator3" class="formSeparator" />
	</f:verbatim>
</jsp:root>