<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA242       	NB-1401     Contract Information User Interface Rewrite  -->
<!-- NBA298			NB-1501		MEC Processing Enhancement -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<!-- begin NBA298 -->
	<h:panelGroup id="contractGroup20" styleClass="formDataEntryLine" rendered="#{pc_contractInformation.indMEC}">
		<h:outputText id="mecReason" value="#{property.contractInfoMECReason}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="mecReasonDD" value="#{pc_contractInformation.mecReason}" styleClass="formEntryText" style="width: 424px;" >
			<f:selectItems value="#{pc_contractInformation.mecReasonList}" />
		</h:selectOneMenu>
		<f:verbatim>
		    <hr id="contractSeparator8" class="formSeparator" />
	    </f:verbatim>
	</h:panelGroup>	
	<!-- end NBA298 -->
	<h:panelGroup id="contractGroup5" styleClass="formDataEntryLine">
		<h:outputText id="area" value="#{property.payorResidenceArea}" styleClass="formLabel" style="width: 175px" />
		<h:inputText id="areaEF" value="#{pc_contractInformation.resArea}" styleClass="formEntryText" style="width: 425px"></h:inputText>
	</h:panelGroup>
	<h:panelGroup id="contractGroup6" styleClass="formDataEntryLine">
		<h:outputText id="state" value="#{property.payorResidenceState}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="stateDD" value="#{pc_contractInformation.resState}" styleClass="formEntryText" style="width: 425px" immediate="true" onchange="submit()">
			<f:selectItems value="#{pc_contractInformation.resStateList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup7" styleClass="formDataEntryLine">
		<h:outputText id="country" value="#{property.payorResidenceCountry}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="countryDD" value="#{pc_contractInformation.resCountry}" styleClass="formEntryText" style="width: 425px" immediate="true" onchange="submit()" >
			<f:selectItems value="#{pc_contractInformation.resCountryList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<f:verbatim>
		<hr id="contractSeparator4" class="formSeparator" />
	</f:verbatim>
	
	<h:panelGroup id="contractGroup8" styleClass="formDataEntryLine">
		<h:outputText id="reqIssueDate" value="#{property.requestedIssueDate}" styleClass="formLabel" style="width: 175px" />
		<h:inputText id="reqIssueDateEF" value="#{pc_contractInformation.reqIssueDate}" styleClass="formEntryText" style="width: 150px" disabled="#{pc_contractInformation.issueDateDisabled}">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:outputText id="issueState" value="#{property.issueState}" styleClass="formLabel" style="width: 125px" />
		<h:selectOneMenu id="issueStateDD" value="#{pc_contractInformation.issueState}" styleClass="formEntryText" style="width: 150px">
			<f:selectItems value="#{pc_contractInformation.issueStateList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup9" styleClass="formDataEntryLine">
		<h:outputText id="assignment" value="#{property.assignment}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="assignmentDD" value="#{pc_contractInformation.assignment}" styleClass="formEntryText" style="width: 150px">
			<f:selectItems value="#{pc_contractInformation.assignmentList}" />
		</h:selectOneMenu>
		<h:outputText id="restriction" value="#{property.restriction}" styleClass="formLabel" style="width: 125px" />
		<h:selectOneMenu id="restrictionDD" value="#{pc_contractInformation.restriction}" styleClass="formEntryText" style="width: 150px">
			<f:selectItems value="#{pc_contractInformation.restrictionList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup10" styleClass="formDataEntryLine">
		<h:outputText id="pension" value="#{property.pension}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="pensionDD" value="#{pc_contractInformation.pension}" styleClass="formEntryText" style="width: 150px" >
			<f:selectItems value="#{pc_contractInformation.pensionList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<f:verbatim>
		<hr id="contractSeparator5" class="formSeparator" />
	</f:verbatim>
	
	<h:panelGroup id="contractGroup11" styleClass="formDataEntryLine">
		<h:outputText id="changeType" value="#{property.contractInfoChngType}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="changeTypeDD" value="#{pc_contractInformation.changeType}" styleClass="formEntryText" style="width: 425px" disabled="#{pc_contractInformation.changeTypeDDDisabled}">
			<f:selectItems value="#{pc_contractInformation.changeTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup12" styleClass="formDataEntryLine">
		<h:outputText id="status" value="#{property.contractInfoStatus}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="statusDD" value="#{pc_contractInformation.status}" styleClass="formEntryText" style="width: 425px" disabled="#{pc_contractInformation.statusDDDisabled}">
			<f:selectItems value="#{pc_contractInformation.statusList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup13" styleClass="formDataEntryLine">
		<h:outputText id="orgEntry" value="#{property.contractInfoOrgEntry}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="orgEntryDD" value="#{pc_contractInformation.orgEntry}" styleClass="formEntryText" style="width: 425px" disabled="#{pc_contractInformation.originalEntryDDDisabled}">
			<f:selectItems value="#{pc_contractInformation.orgEntryList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractGroup14" styleClass="formDataEntryLine">
		<h:outputText id="lstEntry" value="#{property.contractInfoLstEntry}" styleClass="formLabel" style="width: 175px" />
		<h:selectOneMenu id="lstEntryDD" value="#{pc_contractInformation.lastEntry}" styleClass="formEntryText" style="width: 425px" disabled="#{pc_contractInformation.lastEntryDDDisabled}">
			<f:selectItems value="#{pc_contractInformation.lastEntryList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<f:verbatim>
		<hr id="contractSeparator6" class="formSeparator" />
	</f:verbatim>
	
	<h:panelGroup id="contractGroup15" styleClass="formDataEntryLine">
		<h:outputText id="lstAnniversry" value="#{property.lastAnnProcessed}" styleClass="formLabel" style="width: 174px" />
		<h:inputText id="lstAnniversryEF" value="#{pc_contractInformation.lastAnnProcessed}" styleClass="formEntryText" style="width: 150px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:outputText id="lstAccounting" value="#{property.lastAccounting}" styleClass="formLabel" style="width: 125px" />
		<h:inputText id="lstAccountingEF" value="#{pc_contractInformation.lastAccounting}" styleClass="formEntryText" style="width: 150px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="contractGroup16" styleClass="formDataEntryLine">
		<h:outputText id="lstFinancial" value="#{property.lastFinancial}" styleClass="formLabel" style="width: 175px" />
		<h:inputText id="lstFinancialEF" value="#{pc_contractInformation.lastFinancial}" styleClass="formEntryText" style="width: 150px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	<f:verbatim>
		<hr id="contractSeparator7" class="formSeparator" />
	</f:verbatim>
	
	<h:panelGroup id="contractGroup17" styleClass="formDataEntryLine">
		<h:outputText id="gracePeriodExp" value="#{property.gracePerExp}" styleClass="formLabel" style="width: 175px" />
		<h:inputText id="gracePeriodEF" value="#{pc_contractInformation.gracePeriodExp}" styleClass="formEntryText" style="width: 150px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:selectBooleanCheckbox id="gracePeriodExtCB" styleClass="formEntryCheckbox" value="#{pc_contractInformation.gracePeriodExt}" style="margin-left: 20px; width: 20px"/> 
		<h:outputText id="gracePeriodExt" value="#{property.gracePerExt}"  style="font-family: Verdana;font-size: 11px;font-weight: normal;font-style: normal;text-align: left;width: 155px; margin-left: 5px;" />
	</h:panelGroup>
	<h:panelGroup id="contractGroup18" styleClass="formDataEntryLine">
	</h:panelGroup>
</jsp:root>