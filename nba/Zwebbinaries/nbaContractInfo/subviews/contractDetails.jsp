<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA242       	NB-1401     Contract Information User Interface Rewrite  -->
<!-- NBA298			NB-1501		MEC Processing Enhancement -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	
	<h:panelGroup id="contractGroup1" styleClass="formDataEntryLine">
	<h:outputText id="qualType" value="#{property.contractInfoQualType}" styleClass="formLabel" style="width: 175px" />
	<h:outputText id="qualTypeVal" value="#{pc_contractInformation.qualType}" styleClass="formDisplayText" style="width: 220px; text-align:left" />
	</h:panelGroup>
	
	<h:panelGroup id="contractGroup2" styleClass="formDataEntryLine"> <!-- NBA298 -->
		<h:outputText id="mecOverriddenInd" value="#{property.contractInfoMECOverriddenInd}" styleClass="formDisplayText" style="width: 175px; text-align:right; padding-right: 12px;" rendered="#{pc_contractInformation.indMECOverridden}" />
		<h:outputText id="mecInd" value="#{property.contractInfoMECInd}" styleClass="formDisplayText" style="width: 300px; text-align:right;" rendered="#{pc_contractInformation.indMEC}"/>
		<h:outputText id="noMECInd" value="#{property.contractInfoNotaMECInd}" styleClass="formDisplayText" style="width: 400px; text-align:right;" rendered="#{!pc_contractInformation.indMEC}"/>
	</h:panelGroup>
	
	<f:verbatim>
		<hr id="contractSeparator1" class="formSeparator" />
	</f:verbatim>
	
	<h:panelGroup id="contractGroup19" styleClass="formDataEntryLine">
		<h:outputText id="grandfathered" value="#{property.contractInfoGrandfathered}" styleClass="formLabel" style="width: 175px" />
		<h:outputText id="grandfatheredVal" value="#{pc_contractInformation.grandfathered}" styleClass="formDisplayText" style="width: 350px; text-align:left" />
	</h:panelGroup>
	
	<h:panelGroup id="contractGroup3" styleClass="formDataEntryLine">
		<h:outputText id="tamraLvl" value="#{property.contrcatInfoTAMRA7PayLvl}" styleClass="formLabel" style="width: 175px" />
		<h:outputText id="tamraLvlVal" value="#{pc_contractInformation.tamraLevel}" styleClass="formDisplayText" style="width: 150px; text-align:left" >
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
		</h:outputText>
	</h:panelGroup>
	
	<h:panelGroup id="contractGroup4" styleClass="formDataEntryLine">
		<h:outputText id="guidelineLvl" value="#{property.contractInfoGuidelineLvl}" styleClass="formLabel" style="width: 175px" />
		<h:outputText id="guidelineLvlVal" value="#{pc_contractInformation.guidelineLevel}" styleClass="formDisplayText" style="width: 150px; text-align:left" >
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
		</h:outputText>
		<h:outputText id="guidelineSngl" value="#{property.contractInfoGuidelineSngl}" styleClass="formLabel" style="width: 125px" />
		<h:outputText id="guidelineSnglVal" value="#{pc_contractInformation.guidelineSingle}" styleClass="formDisplayText" style="width: 150px; text-align:left" >
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
		</h:outputText>
	</h:panelGroup>
	
	<f:verbatim>
		<hr id="contractSeparator2" class="formSeparator" />
	</f:verbatim>
	
</jsp:root>