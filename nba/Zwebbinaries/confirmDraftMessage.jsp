<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-658     NB-1301   Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench-->
<!-- SPR3454        NB-1401   Not Prompted for Uncommitted Draft Changes When Navigating to Another Tab or Another Business Function -->
<!-- NBA362			NB-1501   Save Draft Requirements and Impairments when navigating from case to case -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String dialogName = request.getParameter("viewName");
String tabnumber = request.getParameter("grNumber");
String nextTabPageUrl =request.getParameter("page");
%>
<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Confirm</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var titleName = null;
		var width=360;
		var height=140;
		//begin SPR3454
		var tabText= null;
		var tabImage= null;
		var pageURL= null;
		var appendText= null;
		//end SPR3454
		
	function initTitle(){
		try{
			titleName = document.getElementById("confirmationTitle").innerHTML;
		}catch(err){
		}
	}

	//SPR3454 new method
	function onTabSwitch(){
		this.tabText = top.mainContentFrame.contentRightFrame.draftTabText;
		this.tabImage = top.mainContentFrame.contentRightFrame.draftTabImage;
		this.pageURL = top.mainContentFrame.contentRightFrame.draftPageURL;
		this.appendText = top.mainContentFrame.contentRightFrame.draftAppendText;
		top.mainContentFrame.contentRightFrame.prevSelectedText = null;
		top.mainContentFrame.contentRightFrame.draftChanges = false;
		top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp = false;	//NBA362
		window.opener.swapTabs(tabImage, tabText, pageURL, appendText);
	}
	
	function yesaction(){
		if('<%= dialogName%>' != null){
			if('<%= dialogName%>' == 'Title'){
				top.showLeft();
			}else if('<%= dialogName%>' == 'tabGroupSwitch'){
					top.mainContentFrame.contentRightFrame.draftChanges = false; //SPR3454
					top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments =false;
					top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp = false;	//NBA362
					window.opener.swapGroup('<%= tabnumber%>','<%= nextTabPageUrl%>');
			}else if('<%= dialogName%>' == 'tabSwitch'){  //SPR3454
					onTabSwitch();						  //SPR3454
			}
		}
		closeErrorDialog();
	}
	
	function closeErrorDialog(){		
		top.hideWait();
		top.closeWindow(null,"false");
	}
				
	function noaction(){
		closeErrorDialog();
	}
	
	/*Renders message on popup on the basis of draft changes or draft comments.
	 Changes message receives precedence if both draft changes and draft comments are present.*/
	//SPR3454 new method
	function renderText(){
		if(top.mainContentFrame.contentRightFrame.draftChanges == "true" || top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp == "true")	//NBA362
			document.getElementById("draftMessageForm:commentsText").style.display="none";
		else
			document.getElementById("draftMessageForm:otherChangesText").style.display="none";
	}
		//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" onload="initTitle(); popupInit(); renderText();"> <!-- SPR3454  -->
<f:view>
	<div style="display: none"><h:outputText id="confirmationTitle" value="#{messagebundle.confirmDraftCommentsTitle}" styleClass="textLabel">
	</h:outputText></div>
	<h:form id="draftMessageForm">
	<f:loadBundle var="messagebundle" basename="com.csc.fs.accel.ui.config.ApplicationMessages" />
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
		<table width="350" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height: 100px">
					<td colspan="2" class="text">
						<h:outputText  id="commentsText" value="#{messagebundle.confirmDraftComments}" styleClass="textLabel" /> <!-- SPR3454  -->
						<h:outputText id="otherChangesText" value="#{messagebundle.confirmDraftChanges}" styleClass="textLabel" />  <!-- SPR3454  -->						
					</td>
				</tr>
				<tr valign="bottom">
					<td align="left">
						<h:commandButton styleClass="button" id="no" value="#{bundle.no}"  onclick=" return noaction();" />
					</td>
					<td align="right">
						<h:commandButton styleClass="button" id="yes" value="#{bundle.yes}" onclick="return yesaction();" />
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>
</f:view>
</body>
</html>
