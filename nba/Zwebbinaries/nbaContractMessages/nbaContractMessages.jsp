<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA166           6         Contract Messages Rewrite -->
<!-- NBA158 		     6		  	Websphere 6.0 upgrade -->
<!-- NBA213 		     7		  	Unified User Interface -->
<!-- SPR3812 		 8		  	java.lang.IllegalStateException on Navigating to Contract Messages  -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>  <!-- SPR3812 -->
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
	String basePath = "";
    if (request.getServerPort() == 80) {
    	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
 	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Messages</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<!-- NBA213 code deleted -->
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_contractmessageview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractmessageview'].target='';
			return false;
		}
</script>
<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll" >
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACT_VALIDATION_MESSAGES" value="#{pc_contractValidationMessages}" /> <!-- SPR3812 -->
	<h:form id="form_contractmessageview"> 
		<h:panelGroup styleClass="sectionSubheader" style="margin-left: -10px">  <!-- NBA213 -->
			<h:outputText id="contractMessagesTitle" value="#{property.contractMessagesTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="contractMsgHeader" styleClass="ovDivTableHeader">
			<h:panelGrid columns="5" styleClass="ovTableHeader" cellspacing="0" columnClasses="ovColHdrIcon,ovColHdrText65,ovColHdrText295,ovColHdrText75,ovColHdrText150 ">
				<!-- begin SPR3812 -->
				<h:commandLink id="contMsgHdrCol1" actionListener="#{pc_contractValidationMessages.sortColumn}" styleClass="ovColSorted#{pc_contractValidationMessages.sortedByCol1}" />
				<h:commandLink id="contMsgHdrCol2" value="#{property.type}" actionListener="#{pc_contractValidationMessages.sortColumn}" styleClass="ovColSorted#{pc_contractValidationMessages.sortedByCol2}" />
				<h:commandLink id="contMsgHdrCol3" value="#{property.message}" actionListener="#{pc_contractValidationMessages.sortColumn}" styleClass="ovColSorted#{pc_contractValidationMessages.sortedByCol3}" />
				<h:commandLink id="contMsgHdrCol4" value="#{property.overrideCB}" actionListener="#{pc_contractValidationMessages.sortColumn}" styleClass="ovColSorted#{pc_contractValidationMessages.sortedByCol4}" />
				<h:commandLink id="contMsgHdrCol5" value="#{property.overridden}" actionListener="#{pc_contractValidationMessages.sortColumn}" styleClass="ovColSorted#{pc_contractValidationMessages.sortedByCol5}" />
				<!-- end SPR3812 -->
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup id="contractDataA" styleClass="ovDivTableData" style="height: 595px;">  <!-- NBA213 SPR3812 -->
			<h:dataTable id="contractTablenewBusinessWebContent" styleClass="ovTableData" cellspacing="0" binding="#{pc_contractValidationMessages.dataTable}" value="#{pc_contractValidationMessages.msgList}" var="contmsg"
					rowClasses="#{pc_contractValidationMessages.rowStyles}" columnClasses="ovColIconTop,ovColText65,ovColText295ML,ovColText75,ovColText150"> <!-- SPR3812 -->
				<h:column>
					<!-- begin SPR3812 -->
					<h:commandButton id="Col1a" image="images/needs_attention/flag-onwhite.gif" rendered="#{contmsg.severeInd}" styleClass="ovViewIconTrue" action="#{pc_contractValidationMessages.selectRow}" immediate="true" />
					<h:commandButton id="Col1b" image="images/needs_attention/filledcircle-onwhite.gif" rendered="#{contmsg.overriddenInd}" styleClass="ovViewIconTrue" action="#{pc_contractValidationMessages.selectRow}" immediate="true" />
					<h:commandButton id="Col1d" image="images/needs_attention/yield.gif" rendered="#{contmsg.overridableInd}" styleClass="ovViewIconTrue" action="#{pc_contractValidationMessages.selectRow}" immediate="true" />
					<h:commandButton id="Col1c" image="images/needs_attention/clear.gif" styleClass="ovViewIconFalse" action="#{pc_contractValidationMessages.selectRow}" immediate="true" />
					<!-- end SPR3812 -->
				</h:column>
				<h:column>
					<h:commandLink value="#{contmsg.valType}" styleClass="ovFullCellSelectPrf" action="#{pc_contractValidationMessages.selectRow}" immediate="true" /> <!-- SPR3812 -->
				</h:column>
				<h:column>
					<h:commandLink value="#{contmsg.message}" style= "color: #000000;" action="#{pc_contractValidationMessages.selectRow}" immediate="true" />	<!-- SPR3812 -->
				</h:column>
				<h:column>
					<h:selectBooleanCheckbox value="#{contmsg.override}" rendered="#{contmsg.overridableInd || contmsg.overriddenInd}"
							styleClass="ovFullCellSelectCheckBox" immediate="true" />
					<h:commandLink value="" styleClass="ovFullCellSelect" rendered="#{!(contmsg.overridableInd || contmsg.overriddenInd)}" 
							action="#{pc_contractValidationMessages.selectRow}" immediate="true" /> <!-- SPR3812 -->
				</h:column>
				<h:column>
					<h:commandLink value="#{contmsg.overridden}" styleClass="ovFullCellSelect" action="#{pc_contractValidationMessages.selectRow}" immediate="true" /> <!-- SPR3812 -->
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		<h:panelGroup styleClass="ovStatusBar">
			<!-- begin SPR3812 -->
			<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_contractValidationMessages.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.previousPageAbsolute}" immediate="true" />
			<h:commandLink value="#{property.previousPage}" rendered="#{pc_contractValidationMessages.showPrevious}" styleClass="ovStatusBarText" 
						action="#{pc_contractValidationMessages.previousPage}" immediate="true" />
			<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_contractValidationMessages.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.previousPageSet}" immediate="true" />
			<h:commandLink value="#{pc_contractValidationMessages.page1Number}" rendered="#{pc_contractValidationMessages.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page1}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page1Number}" rendered="#{pc_contractValidationMessages.currentPage1}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page2Number}" rendered="#{pc_contractValidationMessages.showPage2}" styleClass="ovStatusBarText" 
						action="#{pc_contractValidationMessages.page2}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page2Number}" rendered="#{pc_contractValidationMessages.currentPage2}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page3Number}" rendered="#{pc_contractValidationMessages.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page3}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page3Number}" rendered="#{pc_contractValidationMessages.currentPage3}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page4Number}" rendered="#{pc_contractValidationMessages.showPage4}" styleClass="ovStatusBarText"
			 			action="#{pc_contractValidationMessages.page4}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page4Number}" rendered="#{pc_contractValidationMessages.currentPage4}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page5Number}" rendered="#{pc_contractValidationMessages.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page5}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page5Number}" rendered="#{pc_contractValidationMessages.currentPage5}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page6Number}" rendered="#{pc_contractValidationMessages.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page6}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page6Number}" rendered="#{pc_contractValidationMessages.currentPage6}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page7Number}" rendered="#{pc_contractValidationMessages.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page7}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page7Number}" rendered="#{pc_contractValidationMessages.currentPage7}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractValidationMessages.page8Number}" rendered="#{pc_contractValidationMessages.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.page8}" immediate="true" />
			<h:outputText value="#{pc_contractValidationMessages.page8Number}" rendered="#{pc_contractValidationMessages.currentPage8}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_contractValidationMessages.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.nextPageSet}" immediate="true" />
			<h:commandLink value="#{property.nextPage}" rendered="#{pc_contractValidationMessages.showNext}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.nextPage}" immediate="true" />
			<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_contractValidationMessages.showNext}" styleClass="ovStatusBarText"
						action="#{pc_contractValidationMessages.nextPageAbsolute}" immediate="true" />
			<!-- end SPR3812 -->
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnContRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft" action="#{pc_contractValidationMessages.refresh}" immediate="true" />
			<h:commandButton id="btnContCommit" value="#{property.buttonCommit}"
				styleClass="tabButtonRight-1"
				disabled="#{pc_contractValidationMessages.auth.enablement['Commit'] || 
							pc_contractValidationMessages.notLocked}"
				action="#{pc_contractValidationMessages.commit}" />			<!-- NBA213 SPR3812 -->
			<h:commandButton id="btnContValidate"
				value="#{property.buttonValidate}" styleClass="tabButtonRight"
				disabled="#{pc_contractValidationMessages.auth.enablement['Commit'] ||
							pc_contractValidationMessages.notLocked}"
				action="#{pc_contractValidationMessages.validate}" />			<!-- NBA213 SPR3812 -->
		</h:panelGroup>
	</h:form>
	<!-- NBA213 code deleted -->
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
