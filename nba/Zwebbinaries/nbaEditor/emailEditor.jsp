<%-- CHANGE LOG --%>
<%-- Audit Number  Version   Change Description --%>
<%-- NBA406        NB-1601   Rich Text Ad-Hoc Underwriter Emails Business Requirement --%>

<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Email Editor</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<script>
    var CKEDITOR_BASEPATH = '<%=basePath%>/nbaEditor/ckeditor/';
</script>
<script type="text/javascript" src="nbaEditor/ckeditor/ckeditor.js"></script>
</head>
<body class="updatePanel" leftmargin="0" rightmargin="0" topmargin="0" style="border:0px;">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="EmailEditor" style="background-color:white; ">
			<table cellpadding="0" align="left" cellspacing="0"
				style="vertical-align: top; background-color: white;" width="96%"
				border="0" style="background-color: white;">
				<tr>
					<td width="10%" align="left"><h:outputLabel
							value="#{property.impMessage}" styleClass="entryLabelTop"
							style="margin-top:10px;width:70px" /></td>
					<td width="90%" align="left" valign="top"><input
						type="textarea" name="editor1" id="editor1" value=" " /></td>
				</tr>
			</table>
		</h:form>
	</f:view>
	<script>
                var editor = CKEDITOR.replace( 'editor1', {
    toolbar: [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Preview', 'Print'] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll'] },
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
    { name: 'insert', items: [ 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'] },
    '/',
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
	'/',
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] }
]
});
              
				CKEDITOR.config.removePlugins = 'resize';
				editor.on( 'change', function( evt ) {
                window.parent.document.getElementById("form_commentsEmail:editorValue").value = evt.editor.getData();			
                 });
				CKEDITOR.config.height="270px"
				CKEDITOR.config.removePlugins = 'elementspath';
				CKEDITOR.instances.editor1.setData(window.parent.document.getElementById("form_commentsEmail:editorValue").value);
	    </script>
</body>
</html>