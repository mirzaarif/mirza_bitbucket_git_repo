<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>	
	<head>
		<base href="<%=basePath%>">
		<title>Manager Reports</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="javascript/global/file.js"></script>
		<script language="JavaScript" type="text/javascript">
		<!--
			var width=550;
			var height=120;
		
			function setTargetFrame() {
				//alert('Setting Target Frame');
				document.forms['form_reports'].target='controlFrame';
				return false; 
			}
			function resetTargetFrame() {
				//alert('Resetting Target Frame');
				document.forms['form_reports'].target='';
				return false;
			}
			//-->
		</script>
		<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	</head>

<body onload="popupInit();">
		<h:form id="form_reports">
			<h:panelGroup styleClass="inputFormMat" >
					<h:panelGroup id="formDataRowOne" styleClass="formDataEntryTopLine">   
						<h:outputLabel value="#{property.fromDate}" styleClass="formLabel" style="width: 150px" />
						<h:inputText id="From_Date" value="#{pc_managerReports.fromDate}" styleClass="formEntryDate" style="width: 120px">
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
						<h:outputLabel value="#{property.toDate}" styleClass="formLabel" style="width: 120px"/>
						<h:inputText id="Through_Date" value="#{pc_managerReports.toDate}" styleClass="formEntryDate" style="width: 120px">
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
					</h:panelGroup>	
					<h:panelGroup id="formDataRowTwo" styleClass="formDataEntryTopLine" style="height: 45px;" >  
						<h:outputLabel id="reports" value="#{property.reports}" styleClass="formLabel" style="width: 150px" />
						<h:selectOneMenu id="reportType" value="#{pc_managerReports.selectedReport}" immediate="false" styleClass="formEntryText" style="width: 360px">
							<f:selectItems value="#{pc_managerReports.reportList}"/>
						</h:selectOneMenu>
					</h:panelGroup>					
					<h:panelGroup id="buttonBar" styleClass="tabButtonBar" style="background-color: #BED8DE;">
						<h:commandButton id="btnGenerate" value="#{property.buttonGenerate}" action="#{pc_managerReports.generateManagerReports}" styleClass="tabButtonRight" 
							style="left: 440px;"onclick="resetTargetFrame();" />						
						<h:commandButton id="btnCancel" value="#{property.buttonCancel}" action="#{pc_managerReports.cancel}" styleClass="tabButtonLeft" 
							style="left: 15px;"onclick="setTargetFrame();" />										
					</h:panelGroup>
			</h:panelGroup>
		</h:form>
		<div id="Messages"><h:messages /></div>
		</body>
	</f:view>
</html>		