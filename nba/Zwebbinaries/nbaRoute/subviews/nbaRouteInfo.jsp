<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA318           1301     Quality Review Process -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
		<!--  Person Information -->
		<h:panelGroup id="routeSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="name" value="#{property.routeName}" styleClass="formLabel" />
			<h:outputText id="nameText" value="#{pc_routeInfo.name}" styleClass="formDisplayText" />
		</h:panelGroup>			
		<h:panelGroup id="routeSubSection2" styleClass="formDataDisplayLine" rendered="#{pc_routeInfo.renderGovtId}">
			<h:outputLabel id="socialSecurity" value="#{property.routeSocialSecurity}" styleClass="formLabel"  rendered="#{pc_routeInfo.socialSecurity}" />
			<h:outputLabel id="taxIdentification" value="#{property.routeTaxIdentification}" styleClass="formLabel"  rendered="#{pc_routeInfo.taxIdentification}"/>
			<h:outputLabel id="socialInsurance" value="#{property.routeSocialInsurance}" styleClass="formLabel"  rendered="#{pc_routeInfo.socialInsurance}"/>
			<h:outputText id="govtIdText"  value="#{pc_routeInfo.govtId}" styleClass="formDisplayText" />
		</h:panelGroup>			
		
		<f:verbatim>
			<hr id="routeSeparator1" class="formSeparator" />
		</f:verbatim>
	
		<!--  Workitem Information -->
		<h:panelGroup id="routeSubSection3" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="businessArea" value="#{property.routeBusinessArea}" styleClass="formLabel" />
			<h:outputText id="businessAreaText" value="#{pc_routeInfo.businessArea}" styleClass="formDisplayText" />
		</h:panelGroup>			
		<h:panelGroup id="routeSubSection4" styleClass="formDataDisplayLine">
			<h:outputLabel id="queue" value="#{property.routeQueue}" styleClass="formLabel" />
			<h:selectOneMenu id="queueText" value="#{pc_routeInfo.queue}" styleClass="formEntryText" 
				valueChangeListener="#{pc_routeInfo.queueChange}" immediate="true" onchange="resetTargetFrame(); submit()"
				style="width: 350px"><!-- NBA213 -->
				<f:selectItems value="#{pc_routeInfo.queueList}" />
			</h:selectOneMenu>		
		</h:panelGroup>			
		<h:panelGroup id="routeSubSection5" styleClass="formDataDisplayLine">
			<h:outputLabel id="status" value="#{property.routeStatus}:" styleClass="formLabel" />
			<h:selectOneMenu id="statusText" value="#{pc_routeInfo.status}" binding="#{pc_routeInfo.statusSelect}" styleClass="formEntryText" 
				valueChangeListener="#{pc_routeInfo.statusChange}" immediate="true" onchange="resetTargetFrame(); submit()"
				style="width: 350px"><!-- NBA213 -->
				<f:selectItems value="#{pc_routeInfo.statusList}" />
			</h:selectOneMenu>		
		</h:panelGroup>	
		<h:panelGroup id="routeSubSection6" styleClass="formDataDisplayLine">
			<h:outputLabel id="routingReason" value="#{property.routingReason}" styleClass="formLabel" />
			<h:inputText id="routingReasonText" value="#{pc_routeInfo.routingReason}" binding="#{pc_routeInfo.routingReasonText}" 
				maxlength="75" styleClass="formEntryTextFull" style="width: 350px"/>
		</h:panelGroup>			
		<h:panelGroup id="routeSubSection7" styleClass="formDataDisplayLine">
			<h:outputLabel id="overrideMaxSuspendDays" value="#{property.routeOverrideMaxSuspendDays}:" styleClass="formLabel" />
			<h:inputText id="overrideMaxSuspendDaysText" value="#{pc_routeInfo.overrideMaxSuspendDays}" 
				maxlength="3" onkeypress="checkPositiveInteger()" styleClass="formDisplayText" style="width: 30px">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
		</h:panelGroup>			
</jsp:root>