<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA167           6         Route Rewrite -->
<!-- NBA158 		  6		  	Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- NBA318         NB-1301     Quality Review Process -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view><!-- NBA213 -->
		<head>
		<base href="<%=basePath%>">
		<f:loadBundle basename="properties.nbaApplicationData" var="property" /><!-- NBA213 -->
		<title><h:outputText value="#{property.routeTitle}" /></title><!-- NBA213 -->
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" /><!-- NBA213 -->
		<!-- NBA213 code deleted -->
		<script type="text/javascript" src="javascript/global/popupWindow.js"></script><!-- NBA213 -->
		<SCRIPT type="text/javascript" src="javascript/formFunctions.js"></SCRIPT>
		<script language="JavaScript" type="text/javascript">
				var width=500;  //NBA213
				var height=300;  //NBA213
				function setTargetFrame() {
					document.forms['form_routeview'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_routeview'].target='';
					return false;
				}
		</script>
		<!-- NBA213 code deleted -->
		</head>
		<body onload="popupInit();"><!-- NBA213 -->
			<PopulateBean:Load serviceName="RETRIEVE_ROUTE_INFO" value="#{pc_routeInfo}" />
			
			<h:form id="form_routeview"> 
				<h:panelGroup id="routeInputForm" styleClass="updatePanel" style="height: 240px"><!-- NBA213 -->
					<!-- NBA318 code deleted -->
					<!-- begin NBA318 -->
					<f:subview id="nbaRouteInfo"> 
						<c:import url="/nbaRoute/subviews/nbaRouteInfo.jsp" />
					</f:subview>
					<!-- end NBA318 -->		
				</h:panelGroup>	
						
				<!--  NBA213 code deleted -->
				
				<!-- begin NBA213 -->	
				<h:panelGroup styleClass="buttonBar" style="padding-top: 0px;">
					<h:commandButton id="routeCancel" value="#{property.buttonCancel}" styleClass="buttonLeft" 
						action="#{pc_routeInfo.cancel}" onclick="setTargetFrame()" />
					<h:commandButton id="routeCommit" value="#{property.buttonCommit}" styleClass="buttonRight" 
						disabled="#{pc_routeInfo.auth.enablement['Commit'] || 
									pc_routeInfo.notLocked}"
						action="#{pc_routeInfo.commit}" onclick="setTargetFrame()"/>
				</h:panelGroup>
				<!-- end NBA213 -->
			</h:form>
	
			<!-- NBA213 code deleted -->
			<div id="Messages" style="display:none">
				<h:messages />
			</div>
		<!-- NBA213 code deleted -->
		</body>
	</f:view><!-- NBA213 -->
</html>
