<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR2766           5      Invalid Date formatting on HTML views. -->    

//<script language="Java Script">
function date_onKeyPress(field)
{	

	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = date_onKeyPress_NS;							
		return;	
	}
	
	var inputChar = window.event.keyCode;
	var strInput = field.value;	
	
	if(inputChar < 47 || inputChar > 57)	
		return window.event.returnValue = false;
	
	var month; 
	var day;
	var year;
	
	if(strInput.indexOf("/") > 0)
	{
		month = parseInt(strInput.substring(0, strInput.indexOf("/")));
		day = strInput.substring(strInput.indexOf("/") + 1);
		if(strInput.indexOf("/") != strInput.lastIndexOf("/"))
		{
			year = strInput.substring(strInput.lastIndexOf("/") + 1);
			if (document.selection.type == "None")
			{
			  if(year.length > 3)
			  {
			  	return window.event.returnValue = false;
			  }
			}
		}
	}

	if (inputChar == 47 && 
		(strInput.length == 0 ||								// no "/" at the first position
		 strInput.lastIndexOf("/") == strInput.length -1 ||	// no "/" after another
		 strInput.lastIndexOf("/") >= 3))						// no more than two "/" 
	{
		return window.event.returnValue = false;
	}
	if (strInput.length == 1 && 
		((parseInt(strInput) > 1  && inputChar != 47 ) || 
		(parseInt(strInput) == 1 && inputChar > 50) ))
	{
		field.value += "/";	
	}

	if (strInput.length == 2)
	{
		if(strInput.substring(1, 2) == "/" && inputChar == 47)	
			return window.event.returnValue = false;	
		
		if(strInput.substring(1, 2) != "/" && inputChar != 47)									
			field.value += "/";		  
	}
			
	if (strInput.length == 3)
	{							
		if(	(month == 2 && day > 2 || month !=2 && day > 3 && inputChar != 47) ||
			(month != 2 && day == 3 && inputChar > 49))					
			field.value += "/";				 				
	}	
		
	if (strInput.length == 4)
	{							
		if((month == 2 && day > 2 || month !=2 && day > 3) && inputChar != 47)								
			field.value += "/";
		
		if(month != 2 && day == 3 && inputChar > 49)
			return window.event.returnValue = false;
	}	
	if (strInput.length == 5)
	{
		if(strInput.lastIndexOf("/") < 3 && inputChar != 47)
			field.value += "/"; 	
	}	
	return window.event.returnValue = true;	
}

function date_onKeyPress_NS(e)
{	

	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 ))
		return false;				
		
	var strInput = this.value;		
	
	var month; 
	var day;
	var year;
	
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if(strInput.indexOf("/") > 0)
		{
			month = parseInt(strInput.substring(0, strInput.indexOf("/")));
			day = strInput.substring(strInput.indexOf("/") + 1);
			if(strInput.indexOf("/") != strInput.lastIndexOf("/"))
			{
				year = strInput.substring(strInput.lastIndexOf("/") + 1);				
				if(year.length > 3)
				  	return false;									
			}
		}

		if (inputChar == 47 && 
			(strInput.length == 0 ||								
			strInput.lastIndexOf("/") == strInput.length -1 ||	
			strInput.lastIndexOf("/") >= 3))							
			return false;
		
		if (strInput.length == 1 && 
			((parseInt(strInput) > 1  && inputChar != 47 ) || 
			(parseInt(strInput) == 1 && inputChar > 50) ))		
			this.value += "/";	
		
		if (strInput.length == 2)
		{
			if(strInput.substring(1, 2) == "/" && inputChar == 47)	
				return false;	
		
			if(strInput.substring(1, 2) != "/" && inputChar != 47)									
				this.value += "/";		  
		}
			
		if (strInput.length == 3)
		{							
			if(	(month == 2 && day > 2 || month !=2 && day > 3 && inputChar != 47) ||
				(month != 2 && day == 3 && inputChar > 49))					
				this.value += "/";				 				
		}	
		
		if (strInput.length == 4)
		{							
			if((month == 2 && day > 2 || month !=2 && day > 3) && inputChar != 47)								
				this.value += "/";
		
			if(month != 2 && day == 3 && inputChar > 49)
				return false;
		}	
		
		if (strInput.length == 5)
		{
			if(strInput.lastIndexOf("/") < 3 && inputChar != 47)
				this.value += "/"; 	
		}	
	}		
	return true;	
}

function date_onChange(field)
{	
	if(!date_Validate(field))
		return;
	
	field.value = date_Format(field.value);
	data_Changed(field);
}

function date_Validate(field)
{
	var strInput = field.value;	
	if(strInput.length == 0)
		return true;
	
	var month ;
	var day;
	var year;
	
	if(strInput.indexOf("/") > 0)
	{
		if(strInput.length == 10)//SPR2766
		{						//SPR2766
			month = strInput.substring(0, strInput.indexOf("/"));	
			var pos = strInput.lastIndexOf("/")
			day = strInput.substring(strInput.indexOf("/") + 1, pos);			
			if(strInput.indexOf("/") != strInput.lastIndexOf("/"))
				year = strInput.substring(strInput.lastIndexOf("/") + 1);
			else
				year = "";
		//begin SPR2766
		} 
		else 
		{
			raiseError("Invalid Date format, enter MMDDYYYY.");
			return false;
		}
		//end SPR2766
	}
	else
	{
		if(strInput.length == 8)
		{
			month = strInput.substring(0, 2);
			day = strInput.substring(2, 4);
			year = strInput.substring(4, 8);
		}
		else
		{		
			raiseError("Invalid date.", field);
			return false;
		}		
	}
	if (year.length != 4)
	{
		raiseError("4 digits must be entered for the year.", field);
		return false;
	}
	month = month - 0;
	day = day - 0;
	year = year - 0;
	if (month ==0 || month > 12 || day == 0 || day > getDaysInMonth(month, year))
	{		
		raiseError("Invalid date", field);
		return false;	
	}
	if (year > 2100)
	{
		raiseError("Date is too far in the future.", field);
		return false;	
	}
	if (year < 1900)
	{
		raiseError("Date is too far in the past.", field);
		return false;	
	}
	
	field.valid = strInput ;
	return true;
}

function date_Format(strInput)
{
	if(strInput.length == 0)
		return strInput;

	var month; 
	var day;
	var year;
	if(strInput.indexOf("/", 0) == -1) 
	{
		month = strInput.substring(0, 2) - 0;
		day = strInput.substring(2, 4) - 0;
		year = strInput.substring(4);
	}		
	else
	{
		var pos1 = strInput.indexOf("/");
		month = strInput.substring(0, pos1) - 0;
		var pos2 = strInput.lastIndexOf("/");
		day = strInput.substring(pos1 + 1, pos2) - 0;
		var pos3 = strInput.lastIndexOf(" ");
		if(pos3 == -1)
			year = strInput.substring(pos2 + 1);
		else
			year = strInput.substring(pos2 + 1, pos3) - 0;
	}

	if (month < 10)
		month = "0" + month;
	
	if (day < 10)
		day = "0" + day;
	
	if (year.length < 4 && (year - 0) < 100)
		year = (year - 0) + 1900;		
	
	return month + "/" + day + "/" + year;
}

function date_UnFormat(strInput)
{
	aCurrStripChars = new Array("/");
	
	if(strInput.length == 0)
		return strInput;
		
	return parseField(strInput, aCurrStripChars)	
}

function getDaysInMonth(month, year)
{
	var daysInMonth= new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (month == 2) // Test for leap year when Febrary is selected
		return ((year % 4 ==0) && (year % 100 != 0)) || (year % 400 == 0) ? 29 : 28;
	else
		return daysInMonth[month - 1];
}

//begin NBA077
function month_onKeyPress(field){
	var inputChar = window.event.keyCode;
	var strInput = field.value;	
	
	if(inputChar < 48 || inputChar > 57)	//only 0-9
		return window.event.returnValue = false;
		
	if (strInput.length == 1 && 
		((parseInt(strInput) == 1 && inputChar > 50) 
		|| parseInt(strInput) > 1)) //if first number IS 1 THAN allow only 1 and 2
		return window.event.returnValue = false;
		
	if (strInput.length >= 2) //not allow more than 2
		return window.event.returnValue = false;
		
	return window.event.returnValue = true;	
}

function month_onChange(field)
{	
	if(!month_Validate(field))
		return;
	
	data_Changed(field);
}

function month_Validate(field)
{
	var strInput = field.value;	
	if(strInput.length == 0)
		return true;
	
	if(strInput.length > 2)	{
		raiseError("Invalid Month.", field);
		return false;	
	}

	var month = strInput - 0;
	if (month < 1 || month > 12)
	{		
		raiseError("Invalid Month", field);
		return false;	
	}

	field.valid = strInput ;
	return true;
}
function year_onKeyPress(field){
	var inputChar = window.event.keyCode;
	var strInput = field.value;	
	
	if(inputChar < 48 || inputChar > 57)	//only 0-9
		return window.event.returnValue = false;
		
	if (strInput.length >= 4) //not allow more than 4
		return window.event.returnValue = false;
		
	return window.event.returnValue = true;	
}

function year_onChange(field)
{	
	if(!year_Validate(field))
		return;
	
	data_Changed(field);
}

function year_Validate(field)
{
	var strInput = field.value;	
	if(strInput.length == 0)
		return true;
	
	if (strInput.length != 4)
	{
		raiseError("4 digits must be entered for the year.", field);
		return false;
	}
	
	var year = strInput - 0;
	
	if (year > 2100)
	{
		raiseError("Yaer is too far in the future.", field);
		return false;	
	}
	if (year < 1900)
	{
		raiseError("Year is too far in the past.", field);
		return false;	
	}

	field.valid = strInput ;
	return true;
}
//end NBA077

