<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1072           2      This file has been added to facilitate Tree view creation -->
<!-- NBA028            3      Change view background color -->
<!-- NBA044            3      Architectural Changes -->
<!-- NBA055			3		View Case Hierarchy and Break/Create Relationships -->
<!-- SPR1937		   5	  New transaction work item cannot be associated with parent case work item -->
<!-- SPR3394		   7	  A carriage return entered in an Endorsement description causes a Java Script error -->

//<SCRIPT>
/**The Treeview Class constuctor
  *Parameter - divElement. A String representing the DOM element within which the Tree Structure needs to be built.
  *Parameter - refObject. A String representing instance of the Tree object.
 */ 
function Treeview(divElement, refObject){	
	this.containerElement = divElement;
	this.refObject = refObject;
	this.clickHandler = "";	
	this.domObject = eval("window.document.getElementById(\"" + this.containerElement  + "\")");	
}

/**This function builds the Tree Structure
  *Parameter - data. A String out of which the Tree is built.  
 */
function build(data){	  
	var sHtml = ""; 
	var nTagOpen = 0;
	var sDisabled = "";
	var nextNode = null;
	//NBA044 code deleted
		
	if (typeof(data) == "string")this.unMarshal(data);		
	
	for (i = 0; i < this.nodes.length; i++){			
		if(this.nodes[i].status != STATUS_JUNK){ //NBA044 show deleted rows
			for (n = 0; n <= this.nodes[i].level; n++){
				sHtml += HTML_SPACE ; //NBA044
			}			
			
			nextNode = null;
			n = i;			
			while(n++ < (this.nodes.length -1)){
				if(this.nodes[n].status != STATUS_JUNK){ //NBA044 show deleted rows
					nextNode = this.nodes[n];
					break;
				}			
			}
					
			sDisabled = (this.nodes[i].enabled == '0') ? " class=disabled  disabled=true" : "" ;
							
			if (nextNode && nextNode.level > this.nodes[i].level){			
				sHtml += '<SPAN class=Outline id="Row' + this.containerElement + i + '" onclick="toggleNode(this)" style="CURSOR: hand;FONT-FAMILY:webdings" >4</SPAN>&nbsp;<span id="' + this.containerElement + i + '" onclick="' + this.refObject + '.setActiveDomNode(this)" ondblclick="toggleNode(this)" style="CURSOR: hand"><A style ="TEXT-DECORATION:none" onclick="doCleanup = false;" href="javascript:function(){return false;}"><NOBR>' + this.nodes[i].text + '</NOBR></A></span><br>' ;	//NBA044			
				sHtml += '<DIV id=Row' + this.containerElement + i + 'd style="DISPLAY: none" >' ;			
				nTagOpen++ ;
			}else{						
				sHtml += '<SPAN id="' + this.nodes[i].tag + '" style="CURSOR: hand;FONT-FAMILY:webdings" >1</SPAN>&nbsp;<span id="' + this.containerElement + i + '" onClick="' + this.refObject + '.setActiveDomNode(this)" style="CURSOR: hand" ' + sDisabled + '><A ' + sDisabled + ' style="TEXT-DECORATION:none" onclick="doCleanup = false;" href="javascript:function(){return false;}"><NOBR>' + this.nodes[i].text + '</NOBR></A></span><br>' ;	//NBA044			
			}
					
			if (nextNode && (nextNode.level <  this.nodes[i].level) && (nextNode.level != "")){					
				for (var n= 0; n < (parseInt(this.nodes[i].level) - parseInt(nextNode.level)); n++){	
					if (nTagOpen > 0 ){
						sHtml += '</DIV>';
						nTagOpen-- ;
					}
				}						
			}			
		}				
	}
			
	for (n = 0; n < nTagOpen ; n++){
		sHtml += '</DIV>' ;
	}	
	
	this.domObject.innerHTML = '<DIV id="' + this.containerElement + '_Parent"></DIV>';
	this.domObject.innerHTML += '<INPUT type=hidden value="' + this.marshal() + '" name="' + this.containerElement + '_Data">';
	this.domObject.getElementsByTagName("DIV")[0].innerHTML = sHtml;			
}

/**This function hides/shows a tree branch
  *Parameter - field. A DOM element  
 */ 
function toggleNode(field){
	var trgElement,srcElement;	
	if (field.className == "Outline"){
		srcElement = field		
		trgElement = document.getElementById(srcElement.id + "d");
	}else{
		srcElement = document.getElementById('Row' + field.id)		
		trgElement = document.getElementById('Row' + field.id + "d");
	}
		
	if (trgElement.style.display == "none"){		
		trgElement.style.display = "";		
		srcElement.innerHTML = 6;					
	}else {		
		trgElement.style.display = "none";		
		srcElement.innerHTML = 4;		    					
	}		
}

//NBA028 selected text will reference style.html
function setActiveDomNode(domNode){
	//active node is an HTML Dom Node
	if(this.activeNode) {
		this.activeNode.getElementsByTagName("A")[0].className = "";
		this.activeNode.getElementsByTagName("A")[0].getElementsByTagName("NOBR")[0].className = ""; //NBA028
	}		
	if(domNode && domNode.className != "disabled"){						
		//begin NBA044
		this.activeNode = domNode; 
		if(!this.getActiveNode()){
			this.activeNode = null;
			return; 
		}
		//end NBA044
		domNode.getElementsByTagName("A")[0].className = "hilite";
		domNode.getElementsByTagName("A")[0].getElementsByTagName("NOBR")[0].className = "selectedText"; //NBA028
		//NBA044 code deleted
		if(this.clickHandler.length > 0 ) { // NBA055 
			eval(this.clickHandler + "()") 
		}	// NBA055
	}else{
		this.activeNode = null;
	}		
}

function expandCollapseTree(){
	var trgElement,targetId;	
	var elements = this.domObject.getElementsByTagName("SPAN");
	
	for (var i = 0; i < elements.length ; i++){		
		if (elements[i].className == "Outline")	{					
			targetId = elements[i].id + "d";
			trgElement = document.getElementById(targetId);
				
			if (trgElement.style.display == "none"){
				trgElement.style.display = "";				
				elements[i].innerHTML = 6;
			}else {
				trgElement.style.display = "none";
				elements[i].innerHTML = 4;				    
			}				
		}
	}		
}	

//return a Node object to the user
function getActiveNode(){		
	if(this.activeNode){
		var index = parseInt(this.activeNode.id.substring(this.containerElement.length));  
		return (this.nodes[index].status == STATUS_DELETED ? null: this.nodes[index]);	//NBA044
	}
}
function deleteNode(actionString){	//NBA044 parameter added			
	if(this.activeNode){
		var index = parseInt(this.activeNode.id.substring(this.containerElement.length)) + 1; 
		//begin NBA044		
		if(!actionString || typeof(actionString) != "string"){
			actionString = "";
		}
		var jsActiveNode = this.getActiveNode();
		if(!jsActiveNode) { //Redundant check, just to be sure
			return; 
		}		
		var level = jsActiveNode.level;				
		jsActiveNode.status = (jsActiveNode.status == STATUS_ADDED ? STATUS_JUNK : STATUS_DELETED);
		jsActiveNode.text += HTML_SPACE + actionString; //NBA044
		//end NBA044
		while(this.nodes[index] && this.nodes[index].level > level) {		
			//begin NBA044
			if(this.nodes[index].status != STATUS_DELETED){
				this.nodes[index].text += HTML_SPACE + actionString;
				this.nodes[index].status = (this.nodes[index].status == STATUS_ADDED ? STATUS_JUNK : STATUS_DELETED);
			}
			//end NBA044			
			++index;	
		}
		this.build(null);
		this.expandCollapseTree();
		this.activeNode = null;		
	}
}

function updateNode(){		
	if(this.activeNode && !this.activeNode.disabled){
		//begin NBA044
		var jsActiveNode = this.getActiveNode();
		if(!jsActiveNode) {  //Redundant check, just to be sure
			return; 
		}							
		this.activeNode.getElementsByTagName("NOBR")[0].innerHTML = jsActiveNode.text;
		jsActiveNode.status = (jsActiveNode.status == STATUS_ADDED ? STATUS_ADDED : STATUS_UPDATED);	
		//end NBA044
		this.domObject.getElementsByTagName("INPUT")[0].value = this.marshal();		
		this.setActiveDomNode(null);					
	}
}

function addNode(node){
	if(this.activeNode){		
		//begin NBA044
		if(!this.getActiveNode()) { //Redundant check, just to be sure
			return; 
		}	
		//end NBA044		
		//verify that the primary key is present		
		var index = parseInt(this.activeNode.id.substring(this.containerElement.length)); 
		if((!node) || (!node.tag) || node.tag == ""){
			alert("Row can't be added.");
			return;
		}
		//check for unitialized data
		node.level = (node.level? node.level: parseInt(this.nodes[index].level) + 1);
		node.text = (node.text? node.text: "");
		node.icon = (node.icon? node.icon: "-1");
		node.key = (node.key? node.key: "");
		node.enabled = (node.enabled? node.enabled: "1");
		node.status = STATUS_ADDED;
		
		var newRow = DELIMITER_ROW + node.toString();
		var newData = "";
		var level = -1;
		var check_nodes;
		for(i=0; i < this.nodes.length; i++){
			while(check_nodes && parseInt(this.nodes[i].level) <= level){
				newData += newRow;
				check_nodes = false;
			}
			
			if(i>0)	newData += DELIMITER_ROW;
			
			if(i == index){
				check_nodes = true;
				level = parseInt(this.nodes[i].level);
			}			 
			newData += this.nodes[i].toString();
		}
		//this is the last row
		if(check_nodes){
			newData += newRow;	
		}				
		this.build(newData);
		this.expandCollapseTree();
		this.activeNode = null;	
	}	
}

function addRootNode(node){		
	if((!node) || (!node.tag) || node.tag == ""){
		alert("Row can't be added.");
		return;
	}
	//check for unitialized data
	node.level = "0";
	node.text = (node.text? node.text: "");
	node.icon = (node.icon? node.icon: "-1");
	node.key = (node.key? node.key: "");
	node.enabled = (node.enabled? node.enabled: "1");
	node.status = STATUS_ADDED;
	//initialize hidden fields
	node.toString();
	this.nodes[this.nodes.length] = node;	
	this.build(null);
	this.expandCollapseTree();
	this.activeNode = null;			
}

//available only if a particular node is selected
function getAllNodes(includeDeleted,obj){
	var nodes;
	var addNodes = false;	
	var index = 0;
	var found = false; //NBA044
	if(typeof( obj ) == "object"){	
		while(index < this.nodes.length){
			if(this.nodes[index].toString() == obj.toString()){
				var found = true; //NBA044
				break;
			}
			index++;
		}
	}else if (this.activeNode){ //NBA044	
		index = parseInt(this.activeNode.id.substring(this.containerElement.length));			
		var found = true; //NBA044
	}
	
	if(found){	//NBA044		
		var level = parseInt(this.nodes[index].level);
		nodes = new Array();
		for(var i = index + 1; i < this.nodes.length ; i++ ){
			if(parseInt(this.nodes[i].level) > level){
				addNodes = true;
				if(!includeDeleted && (this.nodes[i].status == STATUS_DELETED || this.nodes[i].status == STATUS_JUNK)){
					addNodes = false;	
				}
				if(addNodes) nodes[nodes.length] = this.nodes[i];
			}else{
				break;
			}
		}
	}
	return nodes;
}

//available only if a particular node is selected
function getChildren(includeDeleted,obj){
	var children;
	var addNodes = false;	
	var index = 0;	
	var found = false; //NBA044
	if(typeof( obj ) == "object"){	
		while(index < this.nodes.length){
			if(this.nodes[index].toString() == obj.toString()){
				found = true; //NBA044
				break;
			}
			index++;
		}
	}else if (this.activeNode){ //NBA044	
		index = parseInt(this.activeNode.id.substring(this.containerElement.length));			
		found = true; //NBA044
	}
	
	if(found){	//NBA044	
		var level = parseInt(this.nodes[index].level);
		children = new Array();
		for(var i = index + 1; i < this.nodes.length ; i++ ){
			if(parseInt(this.nodes[i].level) == level + 1){
				addNodes = true;
				if(!includeDeleted && (this.nodes[i].status == STATUS_DELETED || this.nodes[i].status == STATUS_JUNK)){
					addNodes = false;	
				}
				if(addNodes) children[children.length] = this.nodes[i];
			}
			if(parseInt(this.nodes[i].level) <= level){
				break;
			}
		}
	}
	return children;
}

function getParent(){			
	if(this.activeNode){
		var index = parseInt(this.activeNode.id.substring(this.containerElement.length));
		var level = parseInt(this.nodes[index].level);
		for(i = index - 1; i >= 0; i--){
			if(this.nodes[i].level < level){
				return this.nodes[i];
			}
		}
	}
}

function getRoots(){
	var roots = new Array();	
	for (i = 0; i < this.nodes.length; i++) {
		if(parseInt(this.nodes[i].level) == 0){
			roots[roots.length] = this.nodes[i];
		}		
	}
	return roots;
}

function unMarshal(data){
	if(data ==""){
		this.nodes = new Array();
	}else{
		var rows = data.split(DELIMITER_ROW);
		this.nodes = new Array(rows.length);
		for(i=0;i < rows.length; i++){
			this.nodes[i] = new Node(rows[i]);		
		}	
	}
}

function marshal(){
	var str = "";
	for(i = 0 ; i < this.nodes.length; i++){		
		if(i > 0) str += DELIMITER_ROW;					
		str += this.nodes[i].toString(); 		
	}
	return str;
}
//NBA044 new method
function clear(){
	this.domObject.innerHTML = "";	
	this.nodes = new Array();
}
/**
 * Use this method to set an Active node based on a key, tag combination
 * Every node can uniquely be identified using a key node combination
 */	
//NBA044 new method
function setActiveNode(aKey, aTag, makeSelected){
	aKey = aKey.toUpperCase();
	aTag = aTag.toUpperCase();
	for(i = 0 ; i < this.nodes.length; i++){		
		if(aKey == this.nodes[i].key.toUpperCase() && aTag == this.nodes[i].tag.toUpperCase()){
			break;
		}							
	}
	if(makeSelected){
		this.setActiveDomNode(document.getElementById(this.containerElement + i));
	} else {
		this.activeNode = document.getElementById(this.containerElement + i);							
	}	
}

//Public variables for the Treeview Class
Treeview.prototype.containerElement;
Treeview.prototype.clickHandler;
Treeview.prototype.domObject;
	
//Public methods for the Treeview Class
Treeview.prototype.unMarshal = unMarshal;
Treeview.prototype.marshal = marshal;
Treeview.prototype.build = build;
Treeview.prototype.toggleNode = toggleNode;
Treeview.prototype.setActiveDomNode = setActiveDomNode;
Treeview.prototype.expandCollapseTree = expandCollapseTree;
Treeview.prototype.getActiveNode = getActiveNode;
Treeview.prototype.deleteNode = deleteNode;
Treeview.prototype.addNode = addNode;
Treeview.prototype.addRootNode = addRootNode; 
Treeview.prototype.updateNode = updateNode;
Treeview.prototype.getAllNodes = getAllNodes;
Treeview.prototype.getChildren = getChildren;
Treeview.prototype.getParent = getParent;
Treeview.prototype.getRoots = getRoots;
Treeview.prototype.clear = clear; //NBA044
Treeview.prototype.setActiveNode = setActiveNode; //NBA044
Treeview.prototype.getActiveNodeIndex = getActiveNodeIndex; //SPR1937

//Define a Node Class with members
function Node(row){
	if(row){
		var col = row.split(DELIMITER_CELL);
		this.level = col[0];		//an integer
		this.text = col[1];		//visible node text
		this.icon = col[2];		//display icon index
		this.tag = col[3];		//XML mapper tag or a file path
		this.key = col[4];		//XML key
		this.enabled = col[5];		//an integer 
		this.status = col[6];		//"A","D","U" or "N" an update status	
	
		if(col.length > 7){
			var field;
			var val;	//SPR3394
			this.hiddenMembers = new Array();
			for(var i = 7; i < col.length; i++){
				field = col[i].split(DELIMITER_HIDDEN);					 
				val = replaceAll(field[1],"\r","\\r"); //SPR3394		
				val = replaceAll(val,"\n","\\n"); //SPR3394					
				eval("this." + field[0] + " = \"" +  formatValue(val, field[3]) + "\"");  //SPR3394	
				eval("this." + field[0] + "_AutoField = \"" + field[2] + "\""); //NBA044
				eval("this." + field[0] + "_FormatType = \"" + field[3] + "\""); //NBA044
				this.hiddenMembers[i - 7] = field[0];				
			}
		}
	}
}

function toString(){
	var str = "";
	var temp;
	if(this.hiddenMembers){
		for(var i = 0; i < this.hiddenMembers.length; i++){
			temp = this.hiddenMembers[i]
			str += DELIMITER_CELL;			
			str += temp;
			str += DELIMITER_HIDDEN;	
			//initialize all hidden members atleast with " "
			eval("this." + temp + " = (!this." + temp + " || this." + temp + ".length == 0? '': this." + temp +")");	
			eval("str += this." + temp);
			//begin NBA044
			autoField = eval("this." + this.hiddenMembers[i] + "_AutoField");
			formatType = eval("this." + this.hiddenMembers[i] + "_FormatType");
			str += DELIMITER_HIDDEN;	
			str += autoField ? autoField : "";
			str += DELIMITER_HIDDEN;	
			str += formatType ? formatType : "0";
			//end NBA044
		}		
	}			
	return (this.level + DELIMITER_CELL + 
			this.text + DELIMITER_CELL + 
			this.icon + DELIMITER_CELL + 
			this.tag + DELIMITER_CELL + 
			this.key + DELIMITER_CELL + 
			this.enabled + DELIMITER_CELL + 
			this.status + str);
	
}
/**
 * This method populates the hidden attribute values into fields that 
 * were mapped for that attribute
 */
//NBA044 new method 
function popAttributes(){		
	if(this.status != STATUS_DELETED && this.hiddenMembers){
		for(var i = 0; i < this.hiddenMembers.length ; i++ ){				
			autoField = eval("this." + this.hiddenMembers[i] + "_AutoField");
			if(autoField != ""){				
				setFieldData(autoField,
						eval("this." + this.hiddenMembers[i]),
						eval("this." + this.hiddenMembers[i] + "_FormatType"));
			}			
		}
	}
}
/**
 * This method populates the hidden attribute values from fields that 
 * were mapped for that attribute
 */
//NBA044 new method 
function pushAttributes(){	
	var aValue = "";
	if(this.status != STATUS_DELETED && this.hiddenMembers){
		for(var i = 0; i < this.hiddenMembers.length ; i++ ){
			autoField = eval("this." + this.hiddenMembers[i] + "_AutoField");
			if(autoField != ""){
				aValue =  getFieldData(autoField);
				eval("this." + this.hiddenMembers[i] + " = \"" + formatValue(aValue, eval("this." + this.hiddenMembers[i] + "_FormatType")) + "\"");	
			}			
		}
	}
}

/**
 * This method sets the hidden attribute value, auto Field name and the formatType.
 * This method should be used when new nodes need to be added.
 */
//NBA044 new method 
function setHiddenAttribute(attr, field, formatType){	
	if(!this.hiddenMembers){	
		this.hiddenMembers = new Array();
	}	
	if(!formatType){
		formatType = 0;
	}	
	if(!eval("this." + attr)){
		this.hiddenMembers[this.hiddenMembers.length] = attr;
	}	
	var obj = getField(field);
	if(obj){
		autoField = field;
		field = formatValue(getFieldData(field), formatType);	
	} else { //a value was passed in
		autoField = "";
		
	}	
 	field = replaceAll(field,"\r","\\r"); //SPR3394		
 	field = replaceAll(field,"\n","\\n"); //SPR3394
	eval("this." + attr + " = \"" + field + "\"");
	eval("this." + attr + "_AutoField = \"" + autoField + "\"");	 
	eval("this." + attr + "_FormatType = \"" + formatType + "\"");	 	 	
}
Node.prototype.toString =toString;
Node.prototype.popAttributes = popAttributes; //NBA044
Node.prototype.pushAttributes = pushAttributes; //NBA044
Node.prototype.setHiddenAttribute = setHiddenAttribute; //NBA044

//Return index of active node object
//SPR1937 New Method
function getActiveNodeIndex(){		
	if(this.activeNode){
		return parseInt(this.activeNode.id.substring(this.containerElement.length));
	}
}