<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA151            6      UL and VUL Application Entry Rewrite -->
<!-- NBA213            7      Unified user Interface -->
<!-- NBA176            7      Annuity Application Entry Rewrite -->
<!-- FNB002         NB-1101   Application Entry  -->

//<script language="JavaScript">
//Define global variables here

//Define functions here
var newwindow = null; 
function doQualityAssurance(){
	if (newwindow != null && !newwindow.closed)
	{
			newwindow.close();
			newwindow = null;
	}
	var url = "../../nbaQualityCheck/nbaQualityCheck.faces"; //NBA213//NBA176
	newwindow = window.open(url,"newwin","width=580,height=380,scrollbars=no");
	if (newwindow.opener == null){
		newwindow.opener = newwindow;
	}	
	if (!newwindow.focus()) 
	{
		newwindow.focus();
	}
	return false;
}

function qualityCheckReturn(param) {
	var firstVal = param[0];
	document.forms['form_appEntryOverview']['form_appEntryOverview:QualityCheckModelResults'].value = param[1];  //FNB002
	submitForm(firstVal);
}

function getBooleanValue(value) {
	if(value == 0) {
		return value;
	} else {
		return 1;
	}
}

function qualityCheckOK(param) {
	document.forms['form_appEntryOverview']['form_appEntryOverview:QualityCheckModelResults'].value = param[0];  //FNB002
}

function qualCheckComplete() {
	var qcResults;
	var lsqIndex;
	var cDataIndex;
	var argIndex;
	var lsqstring;
	var laststring;
	qcResults = document.forms['form_appEntryOverview']['form_appEntryOverview:QualityCheckModelResults'].value;
	
	if( qcResults.length <= 0) {
		return false;
	}
	
	lsqIndex = qcResults.indexOf("lastSelectedQuestion");
	lsqstring = qcResults.substring(lsqIndex, qcResults.length);
	argIndex = lsqstring.indexOf("/lastSelectedQuestion");
	laststring = qcResults.substring(lsqIndex, lsqIndex+argIndex); 
	if( laststring.search("Q_SubmitApplication") != -1 ) {
		return true;
	} else {
		return false;
	}
}

function qualityCheckReview() {
	if (newwindow != null && !newwindow.closed)
	{
			newwindow.close();
			newwindow = null;
	}
	var qualChk = document.forms['form_appEntryOverview']['form_appEntryOverview:QualityCheckModelResults'].value;
	var url = "../../nbaQualityCheck/nbaQualityReview.faces" + "?QualityCheckModelResults=" + qualChk; //NBA213//NBA176
	newwindow = window.open(url,"newwin","width=580,height=380,scrollbars=no");
	if (newwindow.opener == null){
		newwindow.opener = newwindow;
	}	
	if (!newwindow.focus()) 
	{
		newwindow.focus();
	}
	return false;
}
