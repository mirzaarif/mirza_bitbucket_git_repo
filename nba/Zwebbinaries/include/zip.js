//<script language="JavaScript">
function zip_onKeyPress(field)
{
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = zip_onKeyPress_NS;							
		return;	
	}
	
	var inputChar = window.event.keyCode;
	if(inputChar < 48 || inputChar > 57)
		return window.event.returnValue = false;
	
	var strInput = field.value;	
	if (strInput.length == 5)
		field.value += "-";
		
	if (strInput.length > 9)
		return window.event.returnValue = false;
}

function zip_onKeyPress_NS(e)
{
	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 ))
		return false;				
		
	var strInput = this.value;	
		
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if (strInput.length == 5)
			this.value += "-";		
		if (strInput.length > 9)
			return false;
	}	
	return true;

}

function zip_onChange(field)
{
	if(!zip_Validate(field))
		return;
	field.value = zip_Format(field.value);
	data_Changed(field);
}

function zip_Validate(field)
{
	var strInput = field.value;
	if(strInput.length == 0)
		return true;

	if	((strInput.indexOf("-") == 5 && strInput.length != 10) ||
		 (strInput.length > 5 && strInput.indexOf("-") != 5) || 
		 (strInput.length < 5))
	{
		raiseError("Invalid Zip Code!", field);		
		return false;
	}
	return true;		
}

function zip_Format(strInput)
{	
	if(strInput.length == 0)
		return strInput;
	
	if(strInput.indexOf("-", 0) > -1) // aleady formated
		return strInput;

	if (strInput.length > 5)
		return strInput.substring(0, 5) + "-" + strInput.substring(5);
	else
		return strInput;
}

function zip_UnFormat(strInput)
{
	aCurrStripChars = new Array("-");
	
	if(strInput.length == 0)
		return strInput;
		
	return parseField(strInput, aCurrStripChars)	
}
