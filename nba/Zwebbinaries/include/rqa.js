<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- ACN007            4      Reflexive Questioning -->

//<script language="JavaScript">
//Define global variables here
var nbaRQ_JSF = "faces/"; 

//Define functions here
var newwindow = null; 
function doQualityAssurance(){
	if (newwindow != null && !newwindow.closed)
	{
			newwindow.close();
			newwindow = null;
	}
	var val1 = getField("CheckAttached").value;
	var val2 = getField("PacFormAttached").value;
	var val3 = getField("ReplacementFormAttached").value;
	var val4;
	if( getField("ReplacementCode_DD") ) {
		val4 = getField("ReplacementCode_DD").value;
	} else {
		val4 = 0;
	}
	var val5 = getField("QualityCheckModelResults").value;
	var url = "nbaQualityCheck/nbaQualityCheck.jsp" + "?CheckAttached=" + val1 + "&PacFormAttached=" + val2 + "&ReplacementFormAttached=" + val3 + "&ReplacementType=" + val4 + "&QualityCheckModelResults=" + val5;
	newwindow = window.open(nbaRQ_JSF+url,"newwin","width=580,height=380,scrollbars=no");
	if (newwindow.opener == null){
		newwindow.opener = newwindow;
	}	
	if (!newwindow.focus()) 
	{
		newwindow.focus();
	}
	return false;
}

function qualityCheckReturn(param) {
	var firstVal = param[0];
	getField("AppProposedInsuredSignatureOK").value = getBooleanValue(param[1]);
	getField("ReplacementFormSignatureOK").value = getBooleanValue(param[2]);
	getField("CheckSignedOK").value = getBooleanValue(param[3]);
	getField("PACValidationOK").value = getBooleanValue(param[4]);
	getField("QualityCheckModelResults").value = param[5];
	performTask(firstVal);
}

function getBooleanValue(value) {
	if(value == 0) {
		return value;
	} else {
		return 1;
	}
}

function qualityCheckOK(param) {
	getField("AppProposedInsuredSignatureOK").value = param[0];
	getField("ReplacementFormSignatureOK").value = param[1];
	getField("CheckSignedOK").value = param[2];
	getField("PACValidationOK").value = param[3];
	getField("QualityCheckModelResults").value = param[4];
}

function qualCheckComplete() {
	var qcResults;
	var lsqIndex;
	var cDataIndex;
	var argIndex;
	var lsqstring;
	var laststring;
	qcResults = getField("QualityCheckModelResults").value;
	if( qcResults.length <= 0) {
		return false;
	}
	
	lsqIndex = qcResults.indexOf("lastSelectedQuestion");
	lsqstring = qcResults.substring(lsqIndex, qcResults.length);
	argIndex = lsqstring.indexOf("/lastSelectedQuestion");
	laststring = qcResults.substring(lsqIndex, lsqIndex+argIndex); 
	if( laststring.search("Q_SubmitApplication") != -1 ) {
		return true;
	} else {
		return false;
	}
}

function qualityCheckReview() {
	if (newwindow != null && !newwindow.closed)
	{
			newwindow.close();
			newwindow = null;
	}
	var qualChk = getField("QualityCheckModelResults").value;
	var url = "nbaQualityCheck/nbaQualityReview.jsp" + "?QualityCheckModelResults=" + qualChk;
	newwindow = window.open(nbaRQ_JSF+url,"newwin","width=580,height=380,scrollbars=no");
	if (newwindow.opener == null){
		newwindow.opener = newwindow;
	}	
	if (!newwindow.focus()) 
	{
		newwindow.focus();
	}
	return false;
}