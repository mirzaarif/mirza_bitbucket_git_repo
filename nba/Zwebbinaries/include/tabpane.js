<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1072           2      This file has been added to generalize TAB creation -->
<!-- NBA028            3      Change view background color -->
<!-- NBA044            3      Architecture changes -->

//<SCRIPT>
//display a TAB based on index
function showTab(index){	
	var children = this.domElement.getElementsByTagName("BUTTON");
	if(children[index].style.width != "0px"){
		this.hideTabs();	
		this.tabs[index].domElement.style.visibility = "inherit";	
		children[index].style.height = focus_page_height;	
		children[index].style.top = focus_page_top;	
		children[index].style.backgroundColor = focus_page_color;
	
		if(this.tabs[index].clickHandler){
			eval(this.tabs[index].clickHandler + '(this.tabs[' + index +'])');
		}
		
	}else{
		return;
	}
}

//hide all TABS
function hideTabs(){
	var children = this.domElement.getElementsByTagName("BUTTON");
	
	for(var i = 0; i < this.tabs.length; i++){										
		this.tabs[i].domElement.style.visibility = "hidden";		
		children[i].style.height = nofocus_page_height;									
		children[i].style.top = nofocus_page_top ;			
		children[i].style.backgroundColor = nofocus_page_color;						
	}	
}

//Hide the TAB entirely, so that a user cannot access it
function setTabHidden(index){
	var children = this.domElement.getElementsByTagName("BUTTON");
	children[index].style.width = 0;
	var pLeft = parseInt(children[index].style.left);
	for(var i = index + 1; i < this.tabs.length; i++){
		children[i].style.left = pLeft;
		pLeft += parseInt(children[i].style.width);
	}	
}

//Make a TAB accessible to the user
function setTabVisible(index){
	var children = this.domElement.getElementsByTagName("BUTTON");
	children[index].style.width = this.tabs[index].headerWidth;
	var pLeft = parseInt(children[index].style.left) + parseInt(children[index].style.width);
	for(var i = index + 1; i < this.tabs.length; i++){		
		if(children[i].style.width != "0px"){
			children[i].style.left = pLeft;
			pLeft += parseInt(children[i].style.width);
		}
	}
}

//Test if a particular TAB is visible
function isTabVisible(index){
	return (this.domElement.getElementsByTagName("BUTTON")[index].style.width == "0px" ? false : true);
}

//Change TAB heading at runtime
function changeTabHeading(index, newName){
	this.domElement.getElementsByTagName("BUTTON")[index].value = newName;
}
//Disable or Enable the tab
//NBA044
function disableTab(index, disable){
	this.domElement.getElementsByTagName("BUTTON")[index].disabled = disable;
}

//Get the currently selected TAB
function getActiveTab(){
	for(var i = 0; i < this.tabs.length; i++){
		if(this.tabs[i].domElement.style.visibility == "inherit"){
			return this.tabs[i];
		}		
	}
}
//Get the currently selected TAB index
//NBA044
function getActiveTabIndex(){
	for(var i = 0; i < this.tabs.length; i++){
		if(this.tabs[i].domElement.style.visibility == "inherit"){
			return i;
		}		
	}
}

//Hide the entire Tab Pane
function hidePane(){	
	for(var i = 0; i < this.tabs.length; i++){										
		this.tabs[i].domElement.style.visibility = "hidden";							
	}
	this.domElement.style.visibility = "hidden";
}

//Show a Tab Pane
function showPane(){
	for(var i = 0; i < this.tabs.length; i++){										
		this.tabs[i].domElement.style.visibility = "inherit";							
	}
	this.domElement.style.visibility = "inherit";
}

//Add a Tab to a TAB pane, only at creation time
function addTab(aTab){
	this.tabs[this.tabs.length] = aTab;
}

//Construct the HTML for creating the TABS, use an "override" value if there are no inner TABS on a PAGE
function buildTabs(override){

	var str = "";
	var pLeft = 0;
	for(var i = 0; i < this.tabs.length; i++){
		str += '<BUTTON id="' + this.domElement.id + i;		
		str += '" onclick="' + this.refObject + '.showTab(' + i + ')"';		
		str += ' STYLE="position:absolute;LEFT:' + pLeft;
		str += ' ;TOP:0px';				
		str += ' ;WIDTH:' + this.tabs[i].headerWidth;
		str += ' ;HEIGHT:' + nofocus_page_height;
		str += ' ;BORDER-BOTTOM-STYLE:none" hidefocus>' + this.tabs[i].headerName + '</BUTTON>';		
		pLeft += this.tabs[i].headerWidth; 
		this.domElement.innerHTML += str;
		str = "";
		this.tabs[i].domElement.className ="tab";
		this.tabs[i].domElement.style.top = parseInt(this.domElement.style.top) + 23;
		this.tabs[i].domElement.style.left = this.domElement.style.left;
		if(this.tabs[i].width){
			this.tabs[i].domElement.style.width = this.tabs[i].width;
		}
		if(this.tabs[i].height){
			this.tabs[i].domElement.style.height = this.tabs[i].height;
		}
		if(navigator.appName == "Netscape"){
			this.tabs[i].domElement.style.overflow = (override ? "scroll" : "hidden");
			this.tabs[i].domElement.style.borderTop = "buttonhighlight 3px groove";
		}
	}	
	this.domElement.style.zIndex = 150		
}

//Change the X co-ordinate of a TAB PANE
function setX(X){
	this.domElement.style.left = X;
}

//Change the Y co-ordinate of a TAB PANE
function setY(Y){
	this.domElement.style.top = Y;
}

//The TabPane Constructor
function TabPane(domElement,refObject){								
	this.domElement = eval("window.document.getElementById(\"" + domElement  + "\")");
	this.refObject = refObject;
	this.tabs = new Array();
	
}

//The TabPane methods and properties
TabPane.prototype.domElement;
TabPane.prototype.refObject;
TabPane.prototype.tabs;
TabPane.prototype.addTab = addTab;
TabPane.prototype.setX = setX;
TabPane.prototype.setY = setY;
TabPane.prototype.showTab = showTab;
TabPane.prototype.hideTabs = hideTabs;
TabPane.prototype.showPane = showPane;
TabPane.prototype.hidePane = hidePane;
TabPane.prototype.setTabVisible = setTabVisible;
TabPane.prototype.setTabHidden = setTabHidden;
TabPane.prototype.isTabVisible = isTabVisible;
TabPane.prototype.buildTabs = buildTabs;
TabPane.prototype.getActiveTab = getActiveTab;
TabPane.prototype.getActiveTabIndex = getActiveTabIndex; //NBA044
TabPane.prototype.changeTabHeading = changeTabHeading; //new method
TabPane.prototype.disableTab = disableTab; //NBA044

//The Tab Constructor
function Tab(domElement){
	this.domElement = document.getElementById(domElement);		
}

//The Tab methods and properties
Tab.prototype.domElement;
Tab.prototype.headerName;
Tab.prototype.headerWidth;
Tab.prototype.height;
Tab.prototype.width;
Tab.prototype.clickHandler;

//constants
var focus_page_height = 27
var nofocus_page_height = 22
var focus_page_top = 0
var nofocus_page_top = 3
var focus_page_color = "white"  //NBA028 
var nofocus_page_color = "rgb(204, 204, 204)"  //NBA028 
