<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1419		   5	  Issues with UI features for Link Case View -->
<!-- SPR2261		   5	  In Link view for companion case SSN number search cannot be done with prefix as 0. -->
<!-- NBA119            5      Automated Process Suspend -->

//<script>
function number_onKeyPress(field)
{
	if (navigator.appName == 'Netscape'){		
		field.onkeypress = number_onKeyPress_NS;							
		return;	
	}	
	var strInput = field.value;		
	number_unformatted_onKeyPress(field); //SPR2261	
	//SPR2261 code deleted
	//if length is 0 & keypress is 0, then disable keypress (Leading zeroes)
	var inputChar = window.event.keyCode;	 //SPR2261
	if (strInput.length == 0 && inputChar == 48) 
			return window.event.returnValue = false;	
	//SPR2261 code deleted	
}

function number_onKeyPress_NS(e)
{	
	var strInput = this.value;
	//SPR2261 code deleted
	number_unformatted_onKeyPress_NS(e); //SPR2261
	//if length is 0 & keypress is 0, then disable keypress (Leading zeroes)
	var inputChar = window.event.keyCode;	 //SPR2261
	if (strInput.length == 0 && inputChar == 48) 
			return false;	
	//SPR2261 code deleted
}

function number_onChange(field)
{		
	field.value = number_Format(field.value);
	data_Changed(field);	
}

function number_Format(strInput)
{
	return strInput;
}
//SPR2261 new method
//Allow the entry of numeric fields, allowing decimal place and negative signs
function number_unformatted_onKeyPress(field)
{
	return number_unformatted_edit(field, true, true)	//SPR1419
}
 
//Allow the entry of numeric fields, disallowing decimal place and negative signs
//SPR1419 new method
function numberonly_unformatted_onKeyPress(field)
{
	return number_unformatted_edit(field, false, false)	
}
//Edit the entry of numeric fields, with optional decimal and sign fields
//SPR1419 new method	
function number_unformatted_edit(field, decimal, sign)
{
	if (navigator.appName == 'Netscape'){		
		field.onkeypress = number_unformatted_edit_NS(field, decimal, sign);							
		return;	
	}			
	var bPositive = (field.getAttribute("isPositive")== 1 ?  true : false) ;		
	var bInt = (field.getAttribute("isInteger") == 1 ?  true : false) ;
	var nDecimals = field.getAttribute("decimals") ;
	var nLen = field.getAttribute("LEN");
	 
	var inputChar = window.event.keyCode;	
	if(	(inputChar < 48 || inputChar > 57) &&	// not a number 
		((!bInt && inputChar!= 46) || bInt) &&	// not a "." when not a integer field		
		((!bPositive && inputChar!= 45) || bPositive))// If not "-" when not a positive field	
		return window.event.returnValue = false;		
	if (!decimal && inputChar == 46){ //reject if decimal is not allowed
		return window.event.returnValue = false;					
	}
	if (!sign && inputChar == 45){	//reject if negative sign not allowed
		return window.event.returnValue = false;						
	}
	var strInput = field.value;	
	if ( (inputChar == 46 && strInput.indexOf(".", 0) > -1) || // found "." already
		 (inputChar == 45 && strInput.length > 0) )    // "-" only allow at the beginning	
		return window.event.returnValue = false;	

	if (!bInt && !isNaN(nDecimals) && nDecimals > 0)	
	   if (strInput.indexOf(".") > -1 && 
	      (strInput.substr(strInput.indexOf(".") +1)).length == nDecimals ) // allow the input number of decimals points	   
		   return window.event.returnValue = false;	
	
	if(!isNaN(nLen) && nLen > 0 && inputChar != 45 && inputChar != 46) //If max integer length parameter supplied, do length validation
	{
		var val = strInput + (inputChar - 48); 
		if (val.indexOf("-") != -1) 
			val = val.substring(1); //Don't include - signs when calculating length of integer
						
		if 	(parseInt(val, 10).toString().length > nLen)
			return window.event.returnValue = false;
	}		
}

//Edit the entry of numeric fields, with optional decimal and sign fields
//SPR1419 new method	
function number_unformatted_edit_NS(e, decimal, sign)
{	
	var inputChar = e.which;	
	var bPositive = (this.getAttribute("isPositive") == 1 ?  true : false) ;	
	var bInt = (this.getAttribute("isInteger") == 1 ?  true : false) ; 
	var nDecimals = this.getAttribute("decimals") 
	var nLen = this.getAttribute("LEN")	
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) &&
		(inputChar < 48 || inputChar > 57) && 
		((!bInt && inputChar!= 46) || bInt) && 
		((!bPositive && inputChar!= 45) || bPositive))	
		return false;							
	if (!decimal && inputChar == 46){ //reject if decimal is not allowed
		return false;							
	}
	if (!sign && inputChar == 45){	//reject if negative sign not allowed
		return false;							
	}
	var strInput = this.value;			
	//format only if keystroke was a number

	if ( (inputChar == 46 && strInput.indexOf(".", 0) > -1) || // found "." already
		 (inputChar == 45 && strInput.length > 0) )    // "-" only allow at the beginning			 					
		return false;					

	if (!bInt && !isNaN(nDecimals) && nDecimals > 0) 
		if (strInput.indexOf(".") > -1 && 
	      (strInput.substr(strInput.indexOf(".") +1)).length == nDecimals ) // allow the input number of decimals points		   
			return false;		   		
		
	if(!isNaN(nLen) && nLen > 0 && inputChar != 45 && inputChar != 46) //If max integer length parameter supplied, do length validation
		{
		var val = strInput + (inputChar - 48); 			
		if (val.indexOf("-") != -1) 
			val = val.substring(1); //Don't include - signs when calculating length of integer
							
		if (parseInt(val, 10).toString().length > nLen)
			return  false;
		}
	 
}

//Format field for numbers. Allows 0 as prefix
//NBA119 New Method
function numberWithZeroPrefix_onKeyPress(field){
	if (navigator.appName == 'Netscape'){		
		field.onkeypress = numberWithZeroPrefix_onKeyPress_NS;							
		return;	
	}	
	var strInput = field.value;		
	number_unformatted_onKeyPress(field);
}

//Format field for numbers on Netscape. Allows 0 as prefix
//NBA119 New Method
function numberWithZeroPrefix_onKeyPress_NS(e){	
	var strInput = this.value;
	number_unformatted_onKeyPress_NS(e); 
}
