<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA024		   	    	  new js file -->
<!-- SPR1419		   5	  Issues with UI features for Link Case View -->

function time_Validate(field)
{
 var strInput = field.value;
 
 if(strInput.length == 0)
  return true;
 
 return true;
}

function time_Now(butfield)
{ 
 var sfname = butfield.name;
 var sname = "_tim_" + sfname; 
 var field = document.frm[sname];   
 
 for(index = 0; index < window.document.frm[sfname].length; index++)
 {     
  if( window.document.frm[sfname](index) == butfield)
  {
   field = window.document.frm[sname](index);   
   break;
  }
 }  
 var now = new Date();
 field.value = Time_Format(now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds());
 Time_onChange(field);
}

function time_onChange(field)
{ 
 field.value = time_Format(field.value);
 data_Changed(field);
}

function time_Format(strInput)
{
 if(strInput.length == 0)
  return strInput;
 var hour = 0;
 var minute = 0;
 var ampm = 0;
 var time = 0;
 
 if (strInput.indexOf(":") == -1)
 {
  hour = strInput.substring(0)
  minute = "00"  
 }
 
 if (strInput.indexOf(":") == 1)
 {
  hour = strInput.substring(0, 1);
  minute = strInput.substring(2);
  if (minute.length == 0)
     minute = "00";  
 }
 
 if (strInput.indexOf(":") == 2)
 {
  hour = strInput.substring(0, 2);
  minute = strInput.substring(3);
  if (minute.length == 0)
     minute = "00";  
 }
   
 if( hour < 10 && hour.length == 1)
  hour = "0" + hour;
 if( minute < 10 && minute.length == 1)
     minute = minute + "0";
  
     time = hour + ":" + minute;
      return time;
  }

function time_onKeyPress(field)
{
  if (navigator.appName == 'Netscape'){   
  field.onkeypress = time_onKeyPress_NS;       
  return; 
 }
 
 var inputChar = window.event.keyCode;

 if(inputChar < 48 || inputChar > 58)  
  return window.event.returnValue = false;
   
 var strInput = field.value;
 var hour=0; 
 var minute =0;
 var ampm =0;
 
 if(strInput.indexOf(":") > 0)
 {
  hour = parseInt(strInput.substring(0, strInput.indexOf(":")));
  minute = strInput.substring(strInput.indexOf(":") + 1);
  if((minute.length == 0 && inputChar > 53) || (inputChar == 58))
       return window.event.returnValue = false;
  if(minute.length > 1)
       return window.event.returnValue = false;    
 } 
 
 if (strInput == 0 && (inputChar == 58 || inputChar == 48))
 {
  return window.event.returnValue = false;
 }
 
 if (strInput.length == 1 && inputChar != 58) 
 {  
  if (inputChar == 48 && strInput == 0) //"00" edit
   return window.event.returnValue = false;

  if (inputChar > 53)	// "x:6" edit
   return window.event.returnValue = false;
 
  if(inputChar < 51 && strInput == 1)	//"1x" edit //SPR1419
   return window.event.returnValue = true;  
   
   //SPR1419 code deleted
   field.value += ":" ;
   return window.event.returnValue = true;
   //SPR1419 code deleted
 }
 
 if((inputChar == 58 && (strInput.indexOf(":") == strInput.length -1 )) ||
    (inputChar > 53 && strInput.length == 2 && inputChar != 58) ||
    (strInput.length == 1 && strInput > 2 && inputChar > 53 && inputChar != 58))
       return window.event.returnValue = false;
     
 if((inputChar != 58 && strInput.indexOf(":") != strInput.length -1) &&
  ((strInput.length == 1 && strInput > 2 ) ||
   (strInput.length == 1 && strInput == 2 && inputChar > 51 ) ||
   (strInput.length == 2 && strInput.indexOf(":") == -1) ))
   field.value += ":";
 
 return window.event.returnValue = true; 
}

function time_onKeyPress_NS(field)
{
 //Get the keycode
 var inputChar = e.which;
 
 //allow only numbers, or backpsace/delete/tab etc to be entered in this field 
 if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 ))
  return false; 

 if(inputChar < 48 || inputChar > 58)  
  return window.event.returnValue = false;
   
 var strInput = field.value;
 var hour=0; 
 var minute =0;
 var ampm =0;
 
 if(strInput.indexOf(":") > 0)
 {
  hour = parseInt(strInput.substring(0, strInput.indexOf(":")));
  minute = strInput.substring(strInput.indexOf(":") + 1);
  if((minute.length == 0 && inputChar > 53) || (inputChar == 58))
       return false;
  if(minute.length > 1)
       return false;    
 } 

 if (strInput == 0 && (inputChar == 58 || inputChar == 48))
 {
  return false;
 }
 
 if (strInput.length == 1 && inputChar != 58) 
 {  
  if (inputChar == 48 && strInput == 0){
   return false;
  }
  
  if (inputChar > 53)
  {
   return false;
  }
  
  if(inputChar < 50 && strInput == 1){
   return true;
  }
  
  if (inputChar <54) 
  {
   field.value += ":" ;
   return true;
  } 
 }
 
 if((inputChar == 58 && (strInput.indexOf(":") == strInput.length -1 )) ||
    (inputChar > 53 && strInput.length == 2 && inputChar != 58) ||
    (strInput.length == 1 && strInput > 2 && inputChar > 53 && inputChar != 58))
       return false;
     
 if((inputChar != 58 && strInput.indexOf(":") != strInput.length -1) &&
  ((strInput.length == 1 && strInput > 2 ) ||
   (strInput.length == 1 && strInput == 2 && inputChar > 51 ) ||
   (strInput.length == 2 && strInput.indexOf(":") == -1) ))
   this.value += ":";
 
 return true; 
}
