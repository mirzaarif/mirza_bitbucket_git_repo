<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1072           2      This file contains code to allow Table Pane creation-->
<!-- NBA024            2      Conversion of applet based views to HTML -->
<!-- NBA028            3      Change view background color -->
<!-- NBA044            3      Architecture Changes -->

//<script>

//-------------------------------------Table-----------------------------
/**This function is invoked when the user tries to navigate through records.
 * At this time, a table reference and navigation direction are passed on to a server component.
 * As such only one server component is used to accomplish this navigation for all table panes.
 * The table data is at all times stored in a session object.
 */
//NBA044 new function
function navigateRecords(arg1,arg2){	
	if(!getField("TABLE_REFERENCE")){		
		document.getElementById("WaitScreen").innerHTML += "<INPUT type=hidden name='TABLE_REFERENCE' >"; 
	}
	getField("TABLE_REFERENCE").value = arg2;	
	if(!getField("TP_NAVIGATION")){				
		document.getElementById("WaitScreen").innerHTML  += "<INPUT type=hidden name='TP_NAVIGATION' >";   
	}
	getField("TP_NAVIGATION").value = arg1;	
	getField("ACTION").value = "NAVIGATION";		
	invokeServer("NbaFoundationServlet");
}

//NBA022 new method added
function displayModal(aValue){		
	openDialog("nbagenericcomments.jsp",400,320,"comment","comment=" + aValue).comment;
}

function handleKeyDown(field,refObject){		
	var lastPos = field.id.lastIndexOf('_');
	var prevPos = field.id.substring(0,lastPos).lastIndexOf('_');	
	var rowIndex = parseInt(field.id.substring(prevPos + 1,lastPos));
	
	var blnDoIncrement = (event.keyCode == 40);
	var blnContinue = (event.keyCode == 40 || event.keyCode == 38);
	
	if(blnContinue){			
		var status_column = refObject.findColumnByName("row_status");
		while(blnContinue) {
			if(blnDoIncrement){
				if(rowIndex < status_column.data.length -1) rowIndex++;	
			} else{
				if(rowIndex > 0) rowIndex--;
			}	
			//window.status = rowIndex		
			if(status_column.data[rowIndex] != STATUS_DELETED) {
				blnContinue = false;
			//begin NBA044 	
			} else if (rowIndex == status_column.data.length -1){
				rowIndex--;
				blnContinue = false;
			} 
			//end NBA044 			
		}
		
		if(!blnContinue){
			var newField = getField(field.id.substring(0,prevPos + 1) + rowIndex + field.id.substring(lastPos));
			if(newField){
				newField.focus();	
			}
		}			
	}		
}	
//this method is invoked when a table cell receives focus
function handleFocus(field,refObject){		
	var lastPos = field.id.lastIndexOf('_');
	var prevPos = field.id.substring(0,lastPos).lastIndexOf('_');	
	var rowIndex = field.id.substring(prevPos + 1,lastPos);
	
	rowIndex = document.getElementById(refObject.t_container.id + "_" + rowIndex).rowIndex;
	
	//NBA044 code deleted
	//begin NBA044
	if(refObject.getStatus(rowIndex) == STATUS_DELETED){
		refObject.unSelectRows();
		return;
	}
	//end NBA044		
	refObject.selectRow(rowIndex); //NBA044	
	hideSelect(refObject);
	
	if(!field.readOnly){				
		var dCell = refObject.findColumnByName(field.id.substring(0,prevPos));
		var is_IE = (navigator.appName.toLowerCase() == 'microsoft internet explorer' ? true : false);
		if(dCell.type == cell_select){
			var select = refObject.getSelectObject(dCell);
			var td = document.getElementById( field.id.substring(0,lastPos));			
			select.value = dCell.translateText(field.value);			
			select.style.width = field.style.width;						
			//select.style.top = (is_IE ? td.offsetTop : refObject.t_container.offsetHeight - 5) ;
			//select.style.left = (is_IE ? td.offsetLeft + 1 :  td.offsetLeft - refObject.t_container.offsetLeft - 5) ;			
			select.style.top = td.offsetTop;
			select.style.left = td.offsetLeft;			
			refObject.activeCell = field.id;
			field.style.visibility = "hidden";	
		}
	}	
	//fire the external focus event
	if(refObject.cellOnFocus.length > 0){		
		eval(refObject.cellOnFocus + "(" + refObject.refObject + " ,\"" + field.id.substring(0,prevPos) + "\")");
	}
}

//handle a Value change -- for editable cells
function handleCellChange(field,refObject){	
	var lastPos = field.id.lastIndexOf('_');
	var prevPos = field.id.substring(0,lastPos).lastIndexOf('_');			
	refObject.setCellValue(field.id.substring(0,prevPos), refObject.activeRow, (field.type == "checkbox" ? field.checked : field.value));
		
	//fire the external change event
	if(refObject.cellOnChange.length > 0){		
		eval(refObject.cellOnChange + "(" + refObject.refObject + " ,\"" + field.id.substring(0,prevPos) + "\")");
	}
}

//set a row status
function setStatus(aRow, aStatus){
	if(aRow > -1){ //NBA044
		var aColumn = this.findColumnByName("row_status");
		aColumn.data[this.getActualRowIndex(aRow)] = aStatus;
		this.marshalCell("row_status");
	} //NBA044	
}

//get a row status
function getStatus(aRow){
	if(aRow > -1){ //NBA044
		return this.findColumnByName("row_status").data[this.getActualRowIndex(aRow)];
	} //NBA044	
}

//hide a dropdown element
function hideSelect(refObject){	
	if(refObject.selectObject && refObject.activeCell.length){		
		refObject.selectObject.style.left = "-1000";				
		getField(refObject.activeCell).style.visibility = "inherit";
		refObject.activeCell = "";
	}
}

//fire a dropdown_onchange event
function selectOnChange(refObject){							
	getField(refObject.activeCell).value = refObject.selectObject.value;
	handleCellChange(getField(refObject.activeCell),refObject);			
}

//get the dropdown element
function getSelectObject(dCell){	
	this.selectObject = getField(this.t_container.id + '_select');			
	addDataToDD(this.selectObject.id,-1,dCell.ddValue,dCell.ddText,false,true);	
	return this.selectObject;
}

//prepare for resize
function headerMouseDown(field,refObject){		
	if(refObject.enableDrag){
		hideSelect(refObject);	
		var name = field.id.substring(0,field.id.lastIndexOf('_'));	
		var td = document.getElementById(name + '_header');			
		
		var resizer = refObject.getResizer();	
		resizer.style.width = parseInt(getField(name + "_label").style.width) ;							
		resizer.style.left = td.offsetLeft - refObject.t_body.scrollLeft + 1;
		resizer.setCapture();		
		resizer.setAttribute("cell_name",name);			
	}		
}

//do resize
function move(refObject){
	var left = 0;
	var obj = refObject.t_container;	
	while(obj && obj.tagName != "BODY"){
		left += parseInt(obj.style.left);
		obj = obj.offsetParent;
	}	
	var width = event.clientX - parseInt(refObject.t_resizer.style.left) - left + 8 + document.body.scrollLeft;	
	refObject.t_resizer.style.width = (width > 10 ? width : 10)  ;
}

//re-laign all disturbed cells
function fix(refObject){
	var element;
	var name = refObject.t_resizer.getAttribute("cell_name");		
	refObject.t_resizer.releaseCapture();
	
	document.getElementById(name + "_header").style.width = parseInt(refObject.t_resizer.style.width);
	getField(name + "_label").style.width = parseInt(refObject.t_resizer.style.width);
	getField(name + "_label").getElementsByTagName("SPAN")[0].style.width = parseInt(refObject.t_resizer.style.width) - 8;		
	var column = refObject.findColumnByName(name);
	column.width = parseInt(refObject.t_resizer.style.width);
	
	for(var i = 0; i< refObject.totalRows; i++){
		element = getField(name + "_" + i + '_' + column.type);		
		if(element){		
			element.style.width = refObject.t_resizer.style.width;
		}
		element = document.getElementById(name + "_" + i);
		if(element){		
			element.style.width = refObject.t_resizer.style.width;
		}
	}
	refObject.t_body.innerHTML = refObject.t_body.innerHTML
	refObject.t_resizer.style.left = "-1000";	 		
	refObject.enableDrag = false;
}

//get the resizer object
function getResizer(){	
	return this.t_resizer;
}

//get a column object by name
function findColumnByName(name){
	for(var i = 0; i < this.columns.length; i++){		
		if(this.columns[i].name.toLowerCase() == name.toLowerCase())
			return this.columns[i];
	}
}

//select a row & deselect the previously selected row
function selectRow(aRow){
	if(this.allowSelection){ //NBA044
		var name = ""
		var new_RefIndex = this.getActualRowIndex(aRow);
		var prev_RefIndex = (this.activeRow > -1 ? this.getActualRowIndex(this.activeRow) : -1);
		
		for(var i = 0; i < this.columns.length; i++){
			if(this.columns[i].type != cell_hidden){
				//de-select previously selected row
				if(prev_RefIndex > -1){			
					name = this.columns[i].name + "_" + prev_RefIndex + "_" + this.columns[i].type;	
					getField(name).style.backgroundColor = "";
					getField(name).style.color = ""; //NBA028
				}
				name = this.columns[i].name + "_" + new_RefIndex + "_" + this.columns[i].type;	
				getField(name).style.backgroundColor = HILITE_COLOR;
				getField(name).style.color = "white"; //NBA028
			}		
		}
	} //NBA044		
	this.activeRow = aRow; //NBA044
}

//unselect all rows
function unSelectRows(makeReadonly){ //NBA044 parameter added to make deleted cells read only
	var name = "";
	var prev_RefIndex = (this.activeRow > -1 ? this.getActualRowIndex(this.activeRow) : -1);
	
	for(var i = 0; i < this.columns.length; i++){
		if(this.columns[i].type != cell_hidden){
			//de-select previously selected row
			if(prev_RefIndex > -1){			
				name = this.columns[i].name + "_" + prev_RefIndex + "_" + this.columns[i].type;	
				getField(name).style.backgroundColor = "";
				getField(name).style.color = ""; //NBA028
				if(makeReadonly) getField(name).readOnly = true; //NBA044
			}else{
				break;
			}						
		}		
	}	
	this.activeRow = -1;	
}

//add hidden fields to hold column data
function addColumnContainers(){
	for(var i = 0; i < this.columns.length; i++){		
		this.t_body.innerHTML += '<INPUT type="hidden" name="' + this.t_container.id + '_' + this.columns[i].name + '">';	
		this.marshalCell(i);
	}	
}

//marshal column data to a delimited string
function marshalCell(aColumn){	
	var str = "";
	aColumn = (typeof(aColumn) == "string" ? this.findColumnByName(aColumn) : this.columns[aColumn]) ;
	for(var i = 0; i < aColumn.data.length; i++){
		if(i > 0){
			str += DELIMITER_ROW;
		}	
		str += aColumn.data[i];
	}	
	getField(this.t_container.id + '_' + aColumn.name).value = str;			
}

//change the status of all rows to Not changed
function refreshRows(){
	var str = "";	 
	var cell = this.findColumnByName("row_status");	
	for(var i = 0; i < this.totalRows; i++){
		if(i > 0){
			str += DELIMITER_ROW;
		}	
		str += STATUS_NOCHANGE;
		cell.data[i] = STATUS_NOCHANGE; 
	}
	getField(this.t_container.id + '_' + cell.name).value = str;
}

function addRow(){
	//get the row holder
	var domObj = this.getMainTable();
	var cell_left = 0;
	domObj.insertRow(domObj.rows.length);
	var new_row = domObj.rows[domObj.rows.length - 1];
	new_row.id = this.t_container.id + "_" + this.totalRows;	
	var new_cell = null;
	var str = "";	
	var sType = "";
	var sPopup = "";
		
	for(var i = this.frozen_cells; i < this.columns.length; i++){
		this.columns[i].data[this.totalRows] = "";	//NBA044
		if(this.columns[i].type == cell_hidden){			
			if(this.columns[i].name == "row_status"){ //begin NBA044			
				this.columns[i].data[this.totalRows] = STATUS_ADDED;	
			} //NBA044
		}else{
			sType = (this.columns[i].type == cell_checkbox ? ' type=checkbox ': ' ');
			//NBA044 code deleted							
			new_row.insertCell(new_row.cells.length);
			new_cell = new_row.cells[new_row.cells.length - 1];
			new_cell.className = "tablecell";
			new_cell.id = this.columns[i].name + '_' + this.totalRows;
			new_cell.style.width = this.columns[i].width + "px";
			new_cell.style.left = cell_left + "px";
			sPopup = (this.columns[i].doPopup ? ' ondblclick="displayModal(this.value);" ' : ' ');						
			readOnly = (this.readOnly || this.columns[i].readOnly ? 'class="readonlyCell" '+ (this.columns[i].type == cell_checkbox ? 'disabled': 'readonly') : ' class="celltext" '); //NBA044
			str = '<INPUT onkeydown="handleKeyDown(this,' + this.refObject + ')" onfocus="handleFocus(this,' + this.refObject + ')" onchange="handleCellChange(this,' + this.refObject + ')" id="' + this.columns[i].name + '_' + this.totalRows + '_' + this.columns[i].type + '"';
			str += sType + 'STYLE="height:21px;width:' + this.columns[i].width + 'px;text-align:' + this.columns[i].align + ';"' + readOnly + sPopup + '>';			
			cell_left += parseInt(this.columns[i].width);
			new_cell.innerHTML = str;
		}
		this.marshalCell(i);
	}
	this.refreshTable = false;
	this.totalRows += 1 ;
	this.visibleRows += 1 ; 
}

function getCellValue(aColumn, aRow){
	if(aRow > -1){ //NBA044
		aColumn = (typeof(aColumn) == "string" ? this.findColumnByName(aColumn) : this.columns[aColumn]) ;	
		return aColumn.data[this.getActualRowIndex(aRow)];
	} //NBA044
}

function setCellValue(aColumn, aRow, aValue){		
	//begin NBA044
	if(aRow == -1 || this.getStatus(aRow) == STATUS_DELETED){
		return;
	}
	//end NBA044
	var rowIndex = this.getActualRowIndex(aRow);	
	aColumn = (typeof(aColumn) == "string" ? this.findColumnByName(aColumn) : this.columns[aColumn]) ;	
	aValue = formatValue(aValue, aColumn.mask); //NBA044
	aColumn.data[rowIndex] = aValue;
	this.marshalCell(aColumn.name);	
	this.setStatus(aRow, this.getStatus(aRow) == STATUS_ADDED ? STATUS_ADDED : STATUS_UPDATED);	
	//refresh the value again
	if(aColumn.type == cell_text){	
		getField(aColumn.name + "_" + rowIndex + "_" + aColumn.type).value = aValue;
	}else if(aColumn.type == cell_checkbox){	
		getField(aColumn.name + "_" + rowIndex + "_" + aColumn.type).checked = eval(aValue);	
	}else if(aColumn.type == cell_select){	
		getField(aColumn.name + "_" + rowIndex + "_" + aColumn.type).value = aColumn.translateValue(aValue);	
	}	
}

function getActualRowIndex(aVisibleRow){
	var firstCell = this.getMainTable().rows[parseInt(aVisibleRow)].cells[0];	
	var lastPos = firstCell.id.lastIndexOf('_');	
	return firstCell.id.substring(lastPos + 1);	
}

function getMainTable(){
	return document.getElementById(this.t_container.id + '_Btbl');
}

function deleteRow(){	
	if(this.activeRow > -1){
		var domObj = this.getMainTable();
		//begin NBA044
		var status = this.getStatus(this.activeRow); 
		if(status == STATUS_ADDED){ 
			this.setStatus(this.activeRow, STATUS_JUNK);
			domObj.deleteRow(this.activeRow);
			this.visibleRows -= 1;
			this.activeRow = -1;
		} else if (status != STATUS_DELETED){	
			this.setStatus(this.activeRow, STATUS_DELETED);			
			this.unSelectRows(true);
		} else {
			return;
		}
		//end NBA044
		hideSelect(this);				
	}	
}
//NBA044 added a new parameter
function addColumn(name,heading,title,width,data,ddValue,ddText,type,readOnly,allowDrag,sortable,align,mask,doPopup, mappingField){
	if(type == cell_select && !this.readOnly){
		this.buildSelect = true;
	}
	this.columns[this.columns.length] = new DColumn(name,heading,title,width,data,ddValue,ddText,type,readOnly,allowDrag,sortable,align,mask,doPopup, mappingField); //NBA044	
}


//NBA044 parameter added
function buildTable(buildAsRecords){			
	//add a dummy cell to hold cell row status
	if(!buildAsRecords){//NBA044
		this.addColumn("row_status","status","status","110",null,null,null,cell_hidden,false,false,false)
	}//NBA044	
	var count_cells = this.columns.length;	
	var frozenWidth = 0;
	var cell_left = 0;
	var str = "";
	var is_IE = (navigator.appName.toLowerCase() == 'microsoft internet explorer' ? true : false);
	var drag = "";
	var noDrag = "";
	var readOnly = "";
	
	this.visibleRows = (this.columns.length > 0 ? this.columns[0].data.length :0);
	this.totalRows = this.visibleRows;
	//add sider container - future use
	//str = '<DIV id="'+ this.t_container.id + '_sider" STYLE="position:absolute;background-color:blue;left:0px;"></DIV>';
	//add header container
	str += '<DIV id="'+ this.t_container.id + '_header" STYLE="position:absolute;top:0px;"></DIV>';
	//add body container
	str += '<DIV id="'+ this.t_container.id + '_body" STYLE="position:absolute;"></DIV>';	
	var map_events = ' onmousemove="move(' + this.refObject + ')" onmouseup="fix(' + this.refObject + ')" ' ;				
	str += '<DIV id="' + this.t_container.id + '_resizer" ' + map_events +' class="resizer"';  
	str +=  ' STYLE="left:-1000px;top:0px;height:'+ (parseInt(this.t_container.style.height) - top_padding)+ 'px;position:absolute;"></DIV>';	
	
	//begin NBA044
	if(buildAsRecords){
		str += '<A  onclick="doCleanup = false;" href="javascript: navigateRecords(\'previous\', \'' + this.t_container.id + '\');" id="' + this.t_container.id + '_previous" STYLE="POSITION:absolute;LEFT:0px;HEIGHT:20px;width:30px;TOP:'+ (parseInt(this.t_container.style.height) - 2) + 'px;FONT-FAMILY:sans-serif; FONT-WEIGHT: bold; FONT-SIZE:12px;" >Previous</A>';
		str += '<A  onclick="doCleanup = false;" href="javascript: navigateRecords(\'next\', \'' + this.t_container.id + '\');" id="' + this.t_container.id + '_next" STYLE="POSITION:absolute;LEFT:60px;HEIGHT:20px;width:25px;TOP:'+ (parseInt(this.t_container.style.height) - 2) + 'px;FONT-FAMILY:sans-serif; FONT-WEIGHT: bold; FONT-SIZE:12px;" >Next</A>';	
	}
	//end NBA044
	this.t_container.innerHTML = str ;	
	
	this.t_resizer = document.getElementById(this.t_container.id + '_resizer');
	
	//future use
	//this.t_sider = document.getElementById(this.t_container.id + "_sider");	
	//this.t_sider.style.height = parseInt(this.t_container.style.height) - top_padding - bottom_padding;
	//this.t_sider.style.width = left_padding;
	//this.t_sider.style.top = top_padding;	
	//this.t_sider.style.overflow = "hidden";
	
	this.t_header = document.getElementById(this.t_container.id + "_header");	
	this.t_header.style.height = top_padding;
	this.t_header.style.width = parseInt(this.t_container.style.width) - left_padding -right_padding -(is_IE ? ie_scrollpadding : 0);	
	this.t_header.style.left = left_padding;
	this.t_header.style.overflow = "hidden";
	
	this.t_body = document.getElementById(this.t_container.id + "_body");	
	this.t_body.style.left = left_padding;
	this.t_body.style.top = top_padding;
	this.t_body.style.width = parseInt(this.t_container.style.width) - left_padding - (is_IE ? ie_scrollpadding : 0) ;
	this.t_body.style.height = parseInt(this.t_container.style.height) - top_padding - (is_IE ? ie_scrollpadding : 0);
	this.t_body.style.overflow = "scroll"; 	
		
	//add Table Header
	str = '<TABLE id="' + this.t_container.id + '_Htbl" cellspacing="0" cellpadding="0"';
	str += ' STYLE="border-collapse:collapse;border-color:black" border="0">';		
	str += '<TR>';
	for(var i = this.frozen_cells; i < count_cells; i++){								
		if(this.columns[i].type != cell_hidden){		
			if(this.columns[i].allowDrag && is_IE){
				drag = ' onmouseover="' + this.refObject + '.enableDrag = true;this.style.cursor=\'e-resize\';" title="Drag to Resize" ';
				noDrag = 'onmouseover="' + this.refObject + '.enableDrag = false;" ';  
			}
			str += '<TD id="' + this.columns[i].name + '_header" STYLE="width:' + parseInt(this.columns[i].width)  + 'px;left:'+ cell_left + 'px;">'; 
			str += '<BUTTON id="' + this.columns[i].name + '_label" ' ; //titles turned-off
			str += '" STYLE="top:0px;height:23px;width:' + this.columns[i].width + 'px;" hidefocus onmousedown="headerMouseDown(this,' + this.refObject + ')">'; 			
			str += '<SPAN class="columnlabel" ' + noDrag + 'style="overflow:hidden;top:0px;height:22px;width:' + (parseInt(this.columns[i].width) - 8) + 'px;">&nbsp;&nbsp;';
			str += this.columns[i].heading;			
			str += '</SPAN><SPAN class="columnlabel" id="' + this.columns[i].name + '_resizer" ' + drag + ' STYLE="height:22px;width: 8px">&nbsp;&nbsp;</SPAN>'			
			str += '</BUTTON></TD>';
			drag = "";
			cell_left += parseInt(this.columns[i].width);
		}
	} 
	str += '</TR></TABLE>';
	this.t_header.innerHTML = str;	
	
	var sData = "";
	var sType = ""
	var sPopup = "";	
	//add body contents	
	str = '<TABLE id="' + this.t_container.id + '_Btbl" cellspacing="0" cellpadding="0"';
	str += ' STYLE="border-collapse:collapse;border-color:black" border="0">';		
	for(var n = 0; n < this.totalRows; n++){
		cell_left = 0;
		str += '<TR id="' + this.t_container.id + '_' + n + '">';
		for(var i = this.frozen_cells; i < count_cells; i++){									
			readOnly = (this.readOnly || this.columns[i].readOnly ? 'class="readonlyCell" '+ (this.columns[i].type == cell_checkbox ? 'disabled': 'readonly') : ' class="celltext" '); //NBA044
			
			if(this.columns[i].type != cell_hidden){
				sData = (this.columns[i].type == cell_select ? this.columns[i].translateValue(this.columns[i].data[n]) : this.columns[i].data[n]);				
				sType = (this.columns[i].type == cell_checkbox ? ' type = checkbox ' + (sData == "true" ? 'checked ':' ')  : ' value="' + sData + '" ');
				sPopup = (this.columns[i].doPopup ? ' ondblclick="displayModal(this.value);" ' : ' ');
				
				str += '<TD class="tablecell" id="' + this.columns[i].name + '_' + n + '" STYLE="width:' + this.columns[i].width  + 'px;left:'+ cell_left + 'px;">'; 							
				str += '<INPUT onkeydown="handleKeyDown(this,' + this.refObject + ')" onfocus="handleFocus(this,' + this.refObject + ')" onchange="handleCellChange(this,' + this.refObject + ')" id="' + this.columns[i].name + '_' + n + '_' + this.columns[i].type +'" ';
				str += sType + ' STYLE="height:21px;width:' + this.columns[i].width + 'px;text-align:' + this.columns[i].align + '" ' + readOnly + sPopup + '>';
				str += '</TD>';
				cell_left += parseInt(this.columns[i].width);
			}
		} 
		
		
		str += '</TR>'
	}	
	str += '</TABLE>';
	
	this.t_body.innerHTML = str;			
	
	if(this.buildSelect){
		var onchange = 'onchange="selectOnChange(' + this.refObject + ');' + (this.selectOnChange == '' ? '' : this.selectOnChange + '(this);') + '"';
		str = '<SELECT id="' + this.t_container.id + '_select" ';  
		str +=  onchange + ' class="cellselect" STYLE="height:21px;position:absolute;left:-1000px"></SELECT>';
		this.t_body.innerHTML += str;
	}
	
	bodyScroll = new Function('document.getElementById("' + this.t_header.id + '").scrollLeft = document.getElementById("' + this.t_body.id + '").scrollLeft');
	headerScroll = new Function('document.getElementById("' + this.t_body.id + '").scrollLeft = document.getElementById("' + this.t_header.id + '").scrollLeft');
	this.t_body.onscroll = bodyScroll;
	this.t_header.onscroll = headerScroll;				
	
	//add hidden fields to store row data
	this.addColumnContainers();
	//set all row status' to "N"
	if(!buildAsRecords){//NBA044
		this.refreshRows();//NBA044
	}//NBA044
	
}

//Destroy the real estate of the entire table
function destroy(){
	this.t_container.innerHTML = "";	
}

//Destroy the real estate of Table rows, keep th headers intact.
//This table cannot hold data again. To Add data create an instance of DTable again.
function destroyBody(){
	this.t_body.innerHTML = "";	
}
/**
 * Sets the cell values for a specified row to HMTL mapping fields.
 */
//NBA044 new method 
function setFieldsFromCells(aRow){
	if(aRow > -1 && this.getStatus(aRow) != STATUS_DELETED){ 
		for(var i = 0; i < this.columns.length; i++){		
			if(this.columns[i].autoField){
				setFieldData(this.columns[i].autoField, this.getCellValue(i, aRow), this.columns[i].mask);
			}	
		}
	}
}
/**
 * Pulls out values from HMTL mapping fields to update cells for a specified row.
 */
//NBA044 new method 
function setCellsFromFields(aRow){
	var aValue;
	if(aRow > -1 && this.getStatus(aRow) != STATUS_DELETED){
		for(var i = 0; i < this.columns.length; i++){		
			if(this.columns[i].autoField){
				aValue = getFieldData(this.columns[i].autoField);
				if(!aValue){
					aValue = "";
				}
				this.setCellValue(i, aRow, aValue);
			}	
		}
	}
}
function DTable(domElement,refObject){	
	this.t_container = eval("window.document.getElementById('" + domElement + "')"); 		
	this.refObject = refObject;
	this.columns = new Array();			
	this.activeRow = -1;	
	this.selectOnChange = "";
	this.cellOnFocus = "";
	this.cellOnChange = "";	
	this.frozen_cells = 0;
	this.totalRows = 0;
	this.visibleRows = 0;
	this.readOnly = false;
	this.allowSelection = false;
}
DTable.prototype.columns;
DTable.prototype.t_container;
DTable.prototype.t_body;
DTable.prototype.t_header;
//future use
DTable.prototype.t_sider;
DTable.prototype.t_resizer;
DTable.prototype.refObject;
//future use
DTable.prototype.frozen_cells;
DTable.prototype.totalRows;
DTable.prototype.visibleRows;
//drop down placeholder
DTable.prototype.selectObject;
DTable.prototype.activeCell;
DTable.prototype.readOnly;
DTable.prototype.allowSelection;
DTable.prototype.activeRow;
DTable.prototype.selectOnChange;
DTable.prototype.cellOnFocus;
DTable.prototype.cellOnChange;
DTable.prototype.findColumnByName = findColumnByName;
DTable.prototype.selectRow = selectRow;
DTable.prototype.getSelectObject = getSelectObject;
DTable.prototype.getResizer = getResizer;
DTable.prototype.addColumn = addColumn;
DTable.prototype.addColumnContainers = addColumnContainers;
DTable.prototype.marshalCell = marshalCell;
DTable.prototype.buildTable = buildTable;
DTable.prototype.refreshRows = refreshRows;
DTable.prototype.addRow = addRow;
DTable.prototype.getCellValue = getCellValue;
DTable.prototype.getActualRowIndex = getActualRowIndex;
DTable.prototype.setStatus = setStatus;
DTable.prototype.getStatus = getStatus;
DTable.prototype.setCellValue = setCellValue;
DTable.prototype.unSelectRows = unSelectRows;
DTable.prototype.deleteRow = deleteRow;
DTable.prototype.getMainTable = getMainTable;
DTable.prototype.destroy = destroy;
DTable.prototype.destroyBody = destroyBody;
DTable.prototype.setFieldsFromCells = setFieldsFromCells; //NBA044
DTable.prototype.setCellsFromFields = setCellsFromFields; //NBA044

//name,header,title,data,ddValue,ddText,allowDrag,sortable,align,mask,support popup, mapping field
function DColumn(name,heading,title,width,data,ddValue,ddText,type,readOnly,allowDrag,sortable,align,mask,doPopup, mappingField){
	this.name = name;
	this.heading = heading;
	this.title = title;	
	this.data = (data == null ? new Array(): data.split(DELIMITER_ROW));
	this.width = width;
	this.readOnly = readOnly;
	this.type = type;		
	if(type == cell_select){		
		this.ddValue =  ddValue.split(DELIMITER_ROW);
		this.ddText = ddText.split(DELIMITER_ROW);		
	}				
	this.allowDrag = allowDrag;	
	this.align = align;
	this.sortable = sortable;	
	this.mask = mask;
	this.doPopup = doPopup;
	//begin NBA044
	if(mappingField){
		this.autoField = mappingField;
	}
	//end NBA044
	this.applyMask();
}

function translateValue(aValue){
	var i = 0;		
	while(i < this.ddValue.length){
		if(this.ddValue[i].toLowerCase() == aValue.toLowerCase()){
			return this.ddText[i];
		}	
		i++;	
	}
	return aValue;	//NBA044			
}

function translateText(aText){
	var i = 0;		
	while(i < this.ddValue.length){
		if(this.ddText[i].toLowerCase() == aText.toLowerCase()){
			return this.ddValue[i];
		}
		i++;		
	}		
	return aText; //NBA044
}

function applyMask(){	
	if(this.mask > 0){
		for(i=0;i < this.data.length; i++){
			this.data[i] = formatValue(this.data[i], parseInt(this.mask)); //NBA044
			//NBA044 code deleted
		}
	}
}
DColumn.prototype.translateValue = translateValue;
DColumn.prototype.translateText = translateText;
DColumn.prototype.applyMask = applyMask;

var HILITE_COLOR = "rgb(51,51,153)";  //NBA028
var left_padding = 0;
var bottom_padding = 15;
var right_padding = 16;
var top_padding = 23;
var ie_scrollpadding = 3;