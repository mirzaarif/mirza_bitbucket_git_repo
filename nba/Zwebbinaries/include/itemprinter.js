<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA044            3      Architectural Changes -->
<!-- NBA033            3      Companion Case and HTML Indexing Views -->
<!-- NBA040            3      Coverage Party Benefits View -->
<!-- NBA043            3      Producer Business Function -->
<!-- PrLog 2376        3      LifePar Role code not translating properly -->
<!-- NBA093            3      Upgrade to ACORD 2.8 -->
<!-- NBA067            3      Client Search retrieval/update -->
<!-- NBA068            3      Inforce Payment -->
<!-- SPR1673           4      Amendments added are populated as Endorsements in the Table pane of Endorsement/Amendments of Coverage/Party B/F -->
<!-- SPR1532            4      Correct problems with updating base coverage.-->
<!-- NBA112 		   4	  Agent Name and Address -->
<!-- SPR2039			4		Annuity Base Plan updates -->
<!-- ACN011             4      Support for Canadian Provinces -->
<!-- SPR2073            4      WithHolding Tax Type DD not updating -->
<!-- SPR2010            5      State and Country values are not saved while adding an address -->
<!-- SPR2267            5      Unit amount not formated correctly in list string -->
<!-- SPR2419            5      Zip Code of a newly added address is disappearing after Add,Apply,Refresh -->
<!-- SPR3007            6      Endorsement causing Number Format exception in the new underwriter work bench -->
<!-- SPR3018            6      Modify Vantage Increase Tab For Contract Changes To Be Clearer -->

//<SCRIPT>	
var dataSep = "**" + HTML_SPACE;
/**
 * Constructor for the ItemPrinter Class. 
 */
function ItemPrinter(){
	this.str = null;
	this.actionIndicator = null;
	this.actionIndicatorCode = null;
	this.aNode = null;
	this.needNode = false;
	this.garbageCollectNode = true;
}
/**
 * Construct a concatenated string representing visible display text.
 */	
function appendFieldToBuffer(field, aPrefix, aSuffix){
	var obj = null;
	if(field.search('_') > -1){ //Every field must have the _DD etc suffix 
		obj = getField(field);
	}	
	if(obj){		
		field = obj.value;		
	}
	if(field.length > 0) {
		if(aPrefix){
			this.str += aPrefix;
		}
		this.str += field;			
		if(aSuffix){
			this.str += aSuffix;	
		}		
	}
}
/**
 * Create a hidden attribute for a new node.
 */	
function setAttribute(attr, field, formatType){
	if(this.aNode && !this.aNode.status){			
		this.aNode.setHiddenAttribute(attr, field, formatType);
	} 			
}
/**
 * Return a string representation to be displayed as visible text
 * It optionally sets hidden attributes on the node depending upon the invoked method.
 */	
function itemToString(aKey) {
	if(this.needNode){
		this.aNode = new Node();
	} else if (this.garbageCollectNode) {
		this.aNode = null;		
	}	
	this.str = "";
	eval("this." + aKey + "()");
	if(this.actionIndicator){
		this.str +=  dataSep + this.actionIndicator;
		this.actionIndicator = null;
	}
	
	if(this.actionIndicatorCode){
		this.setAttribute("ActionIndicator", this.actionIndicatorCode);	 //only for new nodes
		this.actionIndicatorCode = null;
	}	
	this.needNode = false;
	return this.str;	
}
/**
 * Create a new node to represent a particular object based on a key that
 * matches a method name on the NbaItemPrinter class.
 */	
function createNode(aKey){
	this.needNode = true;
	var text = this.itemToString(aKey);			
	this.aNode.text = text;
	return this.aNode;
}

/**
 * Create node data for an Address object.
 */	
function aAddressString(){		
	this.appendFieldToBuffer("address_Line1_EF", "Address: ");
	this.appendFieldToBuffer("address_City_EF", dataSep + "City: ");
	this.appendFieldToBuffer(getTranslatedValue("address_AddressStateTc_DD"), dataSep + "State: ");
	this.appendFieldToBuffer("address_Zip_EF", dataSep + "Zip: ");	
	this.setAttribute("Line1", "address_Line1_EF");
	this.setAttribute("City", "address_City_EF");
	this.setAttribute("AddressStateTC", "address_AddressStateTc_DD"); //SPR2010
	this.setAttribute("Zip", "address_Zip_EF", FORMAT_TYPE_ZIP);	
}
/**
 * Create node data for an Address object for CoverageBenefit View.
 */	
function aAddressStringForCovBenList(){
	var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();//ACN011	
//SPR2010 deleted Address line
	this.appendFieldToBuffer("address_City_EF", "City: "); //SPR2010
	this.appendFieldToBuffer(getTranslatedValue("address_AddressStateTc_DD"), dataSep + "State: ");
	//line deleted ACN011	
	this.setAttribute("AddressTypeCode", "address_addressType_DD");
	this.setAttribute("StartDate", "address_StartDate_EF");
	this.setAttribute("AttentionLine", "address_Addendum_EF");		
	this.setAttribute("Line1", "address_Line1_EF");
	this.setAttribute("Line2", "address_Line2_EF");
	this.setAttribute("Line3", "address_Line3_EF");
	this.setAttribute("Line4", "address_Line4_EF");
	this.setAttribute("City", "address_City_EF");
	this.setAttribute("AddressStateTC", "address_AddressStateTc_DD");//SPR2010

	//Begin ACN011
	if(getField("address_Zip_RB").checked)	{//SPR2419
		this.setAttribute("Zip", "address_Zip_EF", FORMAT_TYPE_ZIP);
		this.appendFieldToBuffer("address_Zip_EF", dataSep + "Zip: ");	
	}else if (getField("address_PostalCode_RB").checked){//SPR2419
		this.setAttribute("Zip", "address_PostalCode_EF", FORMAT_TYPE_POSTALCODE);
		this.appendFieldToBuffer("address_PostalCode_EF", dataSep + "PostalCode: ");
	}
	//End ACN011	
	this.setAttribute("AddressCountryTC", "address_AddressCountryTc_DD");	//SPR2010
	this.setAttribute("ZipCodeTC", "address_ZipTC_RB");//ACN011
}
/**
 * create node data for a Person object.
 */
function aPersonStringForNameOnly() {	
	this.appendFieldToBuffer("person_FirstName_EF", "Client: ");
	this.appendFieldToBuffer("person_MiddleName_EF", " ");
	this.appendFieldToBuffer("person_LastName_EF", " ");
	this.appendFieldToBuffer("person_Suffix_DD", dataSep);		
	this.setAttribute("FirstName", "person_FirstName_EF");
	this.setAttribute("MiddleName", "person_MiddleName_EF");
	this.setAttribute("LastName", "person_LastName_EF");
	this.setAttribute("Suffix", "person_Suffix_DD", "person_Suffix_EF");	
}
// NBA033 New Method
/**
 * create node data for a NbaSource object.
 */
function aNbaSourceString() {	
	this.appendFieldToBuffer("Wrkt_SourceType_EF", "Type: ");
	this.appendFieldToBuffer("Lsnm_LastName_EF1", dataSep + "Last Name: ");
	this.setAttribute("UNIT", "Unit_BusinessArea_EF1");
	this.setAttribute("STAT", "Wrkt_SourceType_EF");
	this.setAttribute("FRNB", "Frnb_FormNumber_EF");
	this.setAttribute("LSNM", "Lsnm_LastName_EF1");
	this.setAttribute("POLN", "Poln_ContractNumber_EF1");
	this.setAttribute("COID", "Coid_Company_DD1");
	this.setAttribute("CKNB", "Cknb_CheckNumber_EF");
	this.setAttribute("CWDT", "Cwdt_Date_EF");
	this.setAttribute("CKAM", "Ckam_CheckAmount_EF");
	this.setAttribute("CWAM","Cwam_AmountToApply_EF");
	this.setAttribute("EXRP", "Exrp_ReplaceExchange_DD");
	this.setAttribute("CBAS", "Cbas_CostBasis_EF");
	this.setAttribute("AppliesToContractsOnly", "AppliesToContract_CB");

}

// NBA033 New Method
/**
 * create node data for a NbaAdditionalContract object.
 */
function aAdditionalContractString() {	
	this.appendFieldToBuffer("Poln_ContractNumber_EF2", "Contract Number: ");
	this.appendFieldToBuffer("Cwam_AmountToApply_EF1", dataSep);

	this.setAttribute("CompanyID", "Coid_Company_DD2");
	this.setAttribute("ContractNumber", "Poln_ContractNumber_EF2");
	this.setAttribute("AmountToApply", "Cwam_AmountToApply_EF1", FORMAT_TYPE_CURRENCY);
	this.setAttribute("ReplaceExchange", "Exrp_ReplaceExchange_DD1");
	this.setAttribute("CostBasis", "Cbas_CostBasis_EF1", FORMAT_TYPE_CURRENCY);
}

/**
 * create node data for a Phone object.
 */
function aPhoneString() {
	this.appendFieldToBuffer(getTranslatedValue("phone_PhoneTypeCode_DD"), "Phone: ");
	this.setAttribute("PhoneTypeCode", "phone_PhoneTypeCode_DD");
	this.setAttribute("DialNumber", "phone_DialNumber_EF", FORMAT_TYPE_TEXT);	
	this.setAttribute("AreaCode", "phone_AreaCode_EF", FORMAT_TYPE_TEXT);	
}
/**
 * create node data for a Email object.
 */
function aEmailString() {
	this.appendFieldToBuffer(getTranslatedValue("emailAddress_EmailType_DD"), "Email: ");
	this.appendFieldToBuffer("emailAddress_EmailAddress_EF", dataSep);		
	this.setAttribute("EMailType", "emailAddress_EmailType_DD");
	this.setAttribute("EMailAddress", "emailAddress_EmailAddress_EF");	
}
/**
 * create node data for a CovOption object.
 */
function aCovOptionString() {
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
var optionAmt = getField("covOption_OptionAmt_EF").value + ",000";//SPR2267
	this.appendFieldToBuffer(getTranslatedValue("covOption_LifeCovOptTypeCode_DD"), "CovOption: ");
	if(selectedNode.IncAnnPercentage != null){
	this.appendFieldToBuffer("covOption_OptionAmt_EF", dataSep + "%");	
	}else{
	this.appendFieldToBuffer(optionAmt, dataSep);	//SPR2267
	}
	this.appendFieldToBuffer("covOption_EffDate_EF", dataSep + "Effective: ");
	this.setAttribute("LifeCovOptTypeCode", "covOption_LifeCovOptTypeCode_DD");
	this.setAttribute("OptionNumberOfUnits", "covOption_OptionAmt_EF");//SPR2267
	this.setAttribute("AnnualPremAmt", "covOption_AnnualPremAmt_EF",FORMAT_TYPE_CURRENCY);
	this.setAttribute("CovOptionStatus", "covOptionExtension_LifeCovOptStatus_EF");//NBA093
	this.setAttribute("IncCalculationMethod", "covOptionExtension_IncCalculationMethod_DD");
	this.setAttribute("IncAnnPercentage", "covOptionExtension_IncAnnPercentage_EF");
	this.setAttribute("IncNoOfYears", "covOptionExtension_IncNoYears_EF");
	this.setAttribute("EffDate", "covOption_EffDate_EF");
		
}
/**
 * create node data for a Endorsement object.
 */
function aEndorsementString() {
	//begin SPR1673
	var endorsementCode = getField("endorsementInfo_EndorsementType_DD").value;
	if(endorsementCode.charAt(0) == 'E')	
		this.appendFieldToBuffer(getTranslatedValue("endorsementInfo_EndorsementType_DD"), "Endorsement: ");
	else
		this.appendFieldToBuffer(getTranslatedValue("endorsementInfo_EndorsementType_DD"), "Amendment: ");
	//end SPR1673
	this.appendFieldToBuffer("endorsementInfo_EndDate_EF", dataSep + "Removal: ");		
	this.appendFieldToBuffer("endorsementInfo_amendEndorseDesc", dataSep + "Description: ");	//NBA079
	this.setAttribute("EndorsementCode", "endorsementInfo_EndorsementType_DD");
	this.setAttribute("EndDate", "endorsementInfo_EndDate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("EndorsementInfo", "endorsementInfo_EndorsementDetails_CB");	//SPR3007
	this.setAttribute("Description", "endorsementInfo_amendEndorseDesc");	
}
/**
 * create node data for a Coverage object.
 */
function aCoverageString() {
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
	if(selectedNode.Base == "Yes"){// begin SPR1532
		this.appendFieldToBuffer(selectedNode.baseText, "Coverage: ");
	}else{
		this.appendFieldToBuffer(getTranslatedValue("coverage_ProductCode_DD"), "Coverage: ");
	}
	if(getField("CurrentAmt_Amt_RB").checked){
		this.appendFieldToBuffer("coverage_CurrentAmt_EF", dataSep);
		this.setAttribute("CurrentAmt", "coverage_CurrentAmt_EF",FORMAT_TYPE_CURRENCY);
		getField("coverage_CurrentAmt2_EF").value = "";
	}else{
		this.appendFieldToBuffer("coverage_CurrentAmt2_EF", dataSep);
		this.setAttribute("CurrentAmtUnits", "coverage_CurrentAmt2_EF");	
		getField("coverage_CurrentAmt_EF").value = "";
	}//end SPR1532
	this.appendFieldToBuffer("coverage_effDate_EF", dataSep + "Effective: ");
	if(selectedNode.Base == "Yes"){// begin SPR1532
		this.setAttribute("PlanName", selectedNode.baseType);
	}else{			
		this.setAttribute("PlanName", "coverage_ProductCode_DD");
	}//end SPR1532
	this.setAttribute("EffDate", "coverage_effDate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("RateClass", "coverageExtension_rateClass_DD");
	this.setAttribute("AnnualPremAmt", "coverage_coveragePremium_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("LifeCovStatus", "coverage_lifeCovStatus_EF");
	this.setAttribute("DeathBenefitOptType", "coverage_deathbenefitOptType_RB");
	this.setAttribute("BlendedInsTargetDBAmtPct", "coverage_targetAmount_EF");
	this.setAttribute("TargetDBAmtRatio", "coverage_targetRatio_EF");
	//deleted 2 lines SPR1532
	this.setAttribute("HasCoverageUnits", "CurrentAmtRB");
	this.setAttribute("FormNumber", "coverageExtension_formNumber_EF");
	this.setAttribute("GuidelineExemptInd","coverageExtension_guidelineExempt_CB");
}
/**
 * add info to  Contract object.
 */
function aContractString() {
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
	this.appendFieldToBuffer("Contract");
	this.setAttribute("RequestedMaturityAge", "reqMaturity_requestedMaturityAge_EF");
	this.setAttribute("RequestedMaturityDuration", "reqMaturity_requestedMaturityDuration_EF");
	this.setAttribute("RequestedMaturityDate", "reqMaturity_requestedMaturityDate_EF");
	this.setAttribute("RequestedMaturityType", "reqMaturity_RB");
	this.setAttribute("DivType", "life_divType_DD");
	this.setAttribute("NonFortProv", "life_nonFortProv_DD");
	this.setAttribute("SecondaryDividendType", "lifeExtension_secondaryDividendType_DD");
	this.setAttribute("InitialAnnPremAmt", "life_InitialNetPrem_EF",FORMAT_TYPE_CURRENCY);
	this.setAttribute("InitialPremAmt", "life_InitialPrem_EF",FORMAT_TYPE_CURRENCY);
	this.setAttribute("Amount1035", "lifeUSA_amount1035_EF",FORMAT_TYPE_CURRENCY);
	this.setAttribute("Internal1035", "lifeUSA_internal1035_CB");
	this.setAttribute("PlannedAdditionalPremium", "policyExtension_plannedAdditionalSinglePremiumAmt_EF",FORMAT_TYPE_CURRENCY);
	this.setAttribute("PaidUpAdditions", "policyExtension_paidUpAdditions_CB");
	this.setAttribute("FirstTaxYear", "annuity_FirstTaxYear_EF");//SPR2039
	this.setAttribute("QualPlanType", "qualPlanType_DD");
	this.setAttribute("RothIRANetContrib", "rothIraNetContribution_EF");
	this.setAttribute("IssueOthrApplied","issueOthrAppliedLOB_CB");
}
/**
 * create node data for a LifeParticipant object.
 */
function aLifeParticipantString() {
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
	this.appendFieldToBuffer(selectedNode.primaryRole, "Person: Role: ");
	if(selectedNode.PartyType == "1"){
		this.appendFieldToBuffer(selectedNode.FirstName + " " + selectedNode.MiddleName + " " + selectedNode.LastName, dataSep + "Name:  ");
		this.setAttribute("ParticipantName", selectedNode.FirstName + " " + selectedNode.MiddleName + " " + selectedNode.LastName);
		this.setAttribute("IssueAge", selectedNode.Age);
		this.setAttribute("IssueGender", selectedNode.Gender);
		this.setAttribute("RateClass", selectedNode.RateClass);
	}
	else if(selectedNode.PartyType == "2"){
		this.appendFieldToBuffer(selectedNode.FirstName + " " + selectedNode.MiddleName + " " + selectedNode.LastName, dataSep + "Name:  ");
		this.setAttribute("ParticipantName", selectedNode.CorpName);
		this.appendFieldToBuffer(selectedNode.CorpName, dataSep + "Name:  ");
	}
	this.setAttribute("id", selectedNode.key);
	this.setAttribute("LifeParticipantRoleCode", selectedNode.lifeRoleCode);//prob log 2376
	
}

//deleted method for Life Participant Beneficiary-NBA093

/**
 * create node data for a BeneficiaryString object.
 */
function aBeneficiaryString() {
var selectedClientNode = Nba_ClientList_LP.getActiveNode();
	if(selectedClientNode.PartyType == "1"){
		this.appendFieldToBuffer(selectedClientNode.FirstName + " " + selectedClientNode.MiddleName + " " + selectedClientNode.LastName + " " + selectedClientNode.Suffix, "Beneficiary: ");//NBA093
		this.setAttribute("FirstName", selectedClientNode.FirstName);
		this.setAttribute("MiddleName", selectedClientNode.MiddleName);
		this.setAttribute("LastName", selectedClientNode.LastName);
		this.setAttribute("Suffix", selectedClientNode.Suffix);
		this.setAttribute("BeneficiaryType", selectedClientNode.BeneficiaryType);//NBA093
		this.setAttribute("roleCode", selectedClientNode.roleCode);//NBA093
	}else if(selectedClientNode.PartyType == "2"){
	 	this.appendFieldToBuffer(selectedClientNode.CorpName);
	 	this.setAttribute("DBA", selectedClientNode.DBA);
	 }
	this.appendFieldToBuffer(selectedClientNode.BeneficiaryType, dataSep + "Beneficiary Type:  ");//NBA093
	this.appendFieldToBuffer(getTranslatedValue("relationExtension_distributionType_DD"), dataSep + "Distribution:  ");
	//NBA093 deleted line
	this.setAttribute("BeneficiaryDesignation", "relation_relationshipType_DD");
	this.setAttribute("InterestPercent", "relation_distributionPercent_EF");
	this.setAttribute("InterestAmount", "relationExtension_distributionAmount_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("BeneficiaryDistributionOption", "relationExtension_distributionType_DD");		
	this.setAttribute("PartyId", selectedClientNode.key);
}
/**
 * update a node data for a BeneficiaryString object.
 */
function aBeneficiaryStringUpdate() {
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
	 if(selectedNode.CorpName != null){
	 	this.appendFieldToBuffer(selectedNode.CorpName);
	 	this.setAttribute("DBA", selectedNode.DBA);
	 }else if(selectedNode.LastName != null){
		this.appendFieldToBuffer(selectedNode.FirstName + " " + selectedNode.MiddleName + " " + selectedNode.LastName + " " + selectedNode.Suffix, "Beneficiary: ");//NBA093
		this.setAttribute("FirstName", selectedNode.FirstName);
		this.setAttribute("MiddleName", selectedNode.MiddleName);
		this.setAttribute("LastName", selectedNode.LastName);
		this.setAttribute("Suffix", selectedNode.Suffix);
		this.setAttribute("BeneficiaryType", selectedNode.BeneficiaryType);//NBA093
		this.setAttribute("roleCode", selectedNode.roleCode);//NBA093
	}
	this.appendFieldToBuffer(selectedNode.BeneficiaryType, dataSep + "Beneficiary Type:  ");//NBA093
	this.appendFieldToBuffer(getTranslatedValue("relationExtension_distributionType_DD"), dataSep + "Distribution:  ");
	
	//NBA093-deleted line  
	this.setAttribute("BeneficiaryDesignation", "relation_relationshipType_DD");
	this.setAttribute("InterestPercent", "relation_distributionPercent_EF");
	this.setAttribute("InterestAmount", "relationExtension_distributionAmount_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("BeneficiaryDistributionOption", "relationExtension_distributionType_DD");		
	
}

/**
 * create node data for a Person For Coverage object.
 */
function aPersonStringForCoverageList() {	
var selectedNode = Nba_CoveragePartyList_LP.getActiveNode();
	this.appendFieldToBuffer("person_FirstName_EF", "Client: ");
	this.appendFieldToBuffer("person_MiddleName_EF", " ");
	this.appendFieldToBuffer("person_LastName_EF", " ");
	this.appendFieldToBuffer(getTranslatedValue("person_Suffix_DD"), dataSep);
	this.appendFieldToBuffer(getTranslatedValue("Nba_Roles_DD"), dataSep + "Primary Role:  ");
	this.appendFieldToBuffer("person_Birthdate_EF", dataSep + "BirthDate:  ");
	this.setAttribute("PrimaryRole", "Nba_Roles_DD");
	this.setAttribute("FirstName", "person_FirstName_EF");
	this.setAttribute("MiddleName", "person_MiddleName_EF");
	this.setAttribute("LastName", "person_LastName_EF");
	this.setAttribute("Suffix", "person_Suffix_DD");
	this.setAttribute("Prefix", "person_Prefix_DD");	
	this.setAttribute("Gender", "person_Gender_DD");
	this.setAttribute("WithholdingCalcType", "partyExtension_WithholdingCalcType_DD");//SPR2073
	this.setAttribute("BirthJurisdiction", "person_BirthJurisdiction_DD");
	this.setAttribute("SmokerStat", "personExtension_RateClass2_DD"); //NBA093
	this.setAttribute("Age", "person_Age2_EF");
	this.setAttribute("DriversLicenseState", "person_DriversLicenseState_DD");
	this.setAttribute("DriversLicenseNum", "person_DriversLicenseNum_EF");
	this.setAttribute("Height", "person_Feet_EF");//SPR1285
	this.setAttribute("Height2", "person_Inches_EF");//SPR1285
	this.setAttribute("Weight", "person_Weight_EF");//SPR1285
	this.setAttribute("EstSalary", "person_EstSalary_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("EstNetWorth", "party_EstNetWorth_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("ResidenceState", "party_ResidenceState_DD");
	this.setAttribute("MarStat", "person_MarStat_DD");
	this.setAttribute("Occupation", "person_Occupation_DD");
	this.setAttribute("Citizenship", "person_Citizenship_DD");
	//SPR2073 deleted 2 lines
	this.setAttribute("BirthDate", "person_Birthdate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("SIN", "party_SIN_EF", FORMAT_TYPE_SIN);//ACN011
	this.setAttribute("TaxType", "taxtypeRB");//ACN011
	
	//Begin SPR2073
	if(getField("taxtypeRB")[0].checked){
		this.setAttribute("GovtID", "party_GovtID_EF", FORMAT_TYPE_SSN);
		getField("party_TaxId_EF").value = "";
		if(selectedNode != null){
			selectedNode.TaxID = "";
		}
	}
	if(getField("taxtypeRB")[1].checked){
		this.setAttribute("TaxID", "party_TaxId_EF");
		getField("party_GovtID_EF").value = "";
		getField("GovtID_OTH_EF").value = "";
		if(selectedNode != null){
			selectedNode.GovtID = "";
		}
	}

	//end SPR2073
}
/**
 * create node data for a Person For Coverage object.
 */
function aOrganizationStringForCoverageList() {	
	this.setAttribute("DBA", "organization_CorporationName_EF", 0);
	this.appendFieldToBuffer("organization_CorporationName_EF");

}
/**
 * create node data for a TaxReporting object.
 */
function ataxReportingString() {	
	this.appendFieldToBuffer("taxReporting_TaxYear_EF", "Tax:   Year: ");
	this.appendFieldToBuffer("taxReporting_TaxableAmt_EF", dataSep);
	this.setAttribute("TaxYear", "taxReporting_TaxYear_EF", 0);
	this.setAttribute("TaxableAmt", "taxReporting_TaxableAmt_EF", FORMAT_TYPE_CURRENCY);
}

/**
 * create node data from the NbaProducerVO for a display object.
 */
//NBA043 New method	
function aNbaProducerVOString() {
	this.appendFieldToBuffer("carrierAppointment_CompanyProducerID_EF3","Identification: ");
	this.appendFieldToBuffer(getField("producerDisplayName").value,dataSep +  "Producer: ");
	this.appendFieldToBuffer("relation_InterestPercent_EF",dataSep +  "Commission Share: ");
	if(getField("agencySystem").value == AGENCY_CONTROL_FILE){
		this.appendFieldToBuffer("relation_VolumeSharePct_EF",dataSep +  "Volume Share: "); //NBA093
	}
	this.appendFieldToBuffer(getTranslatedValue("relationProducer_SituationCode_DD"),dataSep +  "Situation: ");
	
	this.setAttribute("CompanyProducerID","carrierAppointment_CompanyProducerID_EF3");
	this.setAttribute("producerName","producerDisplayName");
	this.setAttribute("FirstName","");
	this.setAttribute("MiddleName","");
	this.setAttribute("LastName","");	
	this.setAttribute("primaryAgent","nba_PrimaryAgent_CB");
	this.setAttribute("ProfileCode","relationProducer_SituationCode_EF");//NBA093
	this.setAttribute("OverrideCommissionPct","relationProducerExtension_OverrideCommissionPct_EF"); //NBA093
	this.setAttribute("SituationCode","relationProducer_SituationCode_DD");
	this.setAttribute("VolumeSharePct","relation_VolumeSharePct_EF"); //NBA093
	this.setAttribute("CommAdvancedInd","relationProducerExt_CommAdvancedInd_DD"); //NBA093
	this.setAttribute("CommAdvancedPct","relationProducerExt_CommAdvancedPct_EF"); //MNA)93
	this.setAttribute("CommAdvancedAmt","relationProducerExt_CommAdvancedAmt_EF"); //NBA093
	this.setAttribute("CommAdvancedMinInd","relationProducerExt_CommAdvancedMinInd_CB"); //NBA093
	this.setAttribute("InterestPercent","relation_InterestPercent_EF");
	this.setAttribute("RenewalInterestPercent","relationProducerExt_RenewalInterestPercent_EF"); //NBA093
	//NBA112 code deleted
	this.setAttribute("AdvanceGB","nba_Percent_Amount_GB");
}
/**
 * create node data for a Rider object.
 */
function aRiderString() {
	this.appendFieldToBuffer(getTranslatedValue("coverage_ProductCode_DD"), "Plan: ");
	this.appendFieldToBuffer("coverage_effDate_EF", dataSep + "Effective: ");			
	this.setAttribute("PlanName", "coverage_ProductCode_DD");
	this.setAttribute("EffDate", "coverage_effDate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("AnnualPremAmt", "coverage_coveragePremium_EF", FORMAT_TYPE_CURRENCY);
	this.setAttribute("LifeCovStatus", "coverage_lifeCovStatus_EF");
	this.setAttribute("LifeCovStatus", "coverage_lifeCovStatus_EF");
	this.setAttribute("DeathBenefitOptType", "coverage_deathbenefitOptType_RB");
	this.setAttribute("BlendedInsTargetDBAmtPct", "coverage_targetAmount_EF");
	this.setAttribute("TargetDBAmtRatio", "coverage_targetRatio_EF");
	this.setAttribute("FormNumber", "coverageExtension_formNumber_EF");
	this.setAttribute("GuidelineExemptInd","coverageExtension_guidelineExempt_CB");
}

/**
 * create node data for a Dst WorkItem (transaction or case) object.
 */
// NBA055 New Method
function aDstWorkItemString() {
		var tagName = "Case: ";
		if (this.aNode.tag == "vo.NbaTransaction" ) {
			tagName = "Transaction: ";
		}
		this.appendFieldToBuffer(tagName, null, null);
		this.appendFieldToBuffer(this.aNode.LockedBy_SRCH,  "Locked By: ", dataSep );
		this.appendFieldToBuffer(this.aNode.BusArea_SRCH, "Business Area: ", null);
		this.appendFieldToBuffer(this.aNode.Contract_SRCH, dataSep + "Contract Number: ", null);	
		this.appendFieldToBuffer(this.aNode.Name_SRCH, dataSep, " ");
		this.appendFieldToBuffer(this.aNode.GovtID_SRCH, dataSep + "Government ID: ", null);
		this.appendFieldToBuffer(this.aNode.WorkType_SRCH, dataSep, null);
		this.appendFieldToBuffer(this.aNode.Status_SRCH, dataSep + "Status: ", null);
		this.appendFieldToBuffer(this.aNode.SuspDate_SRCH, dataSep + "Suspend: ", null);
		this.appendFieldToBuffer(this.aNode.SuspOrig_SRCH, dataSep + "Suspend Originator: ", null);
		this.appendFieldToBuffer(this.aNode.ActDate_SRCH, dataSep + "Activation: ", null);
		this.appendFieldToBuffer(this.aNode.ActStatus_SRCH, dataSep + "Activation Status: ", null);
		this.appendFieldToBuffer(this.aNode.ActionIndicator, dataSep + "[", "]");
}

/**
 * create node data for an NbaSource object.
 */
// NBA055 New Method
function aNbaSourceStringForRelate() {
		this.appendFieldToBuffer(this.aNode.SourceType_SRCH,  "Source: ", null);
		this.appendFieldToBuffer(this.aNode.Contract_SRCH, dataSep + "Contract Number: ", null);	
		this.appendFieldToBuffer(this.aNode.Name_SRCH, dataSep, " ");
		this.appendFieldToBuffer(this.aNode.GovtID_SRCH, dataSep + "Government ID: ", null);
		this.appendFieldToBuffer(this.aNode.ActionIndicator, dataSep + "[", "]");
}
/**  Begin of NBA067
 * Create node data for an Address object for Client Search View.
 */	
function aAddressStringForClientSearch(){	 
  var selectedNode = Nba_ClientSearch_LP.getActiveNode();
    this.appendFieldToBuffer(selectedNode.Address1, " ", " ");
	this.appendFieldToBuffer(selectedNode.Address2, " ", " ");  
	this.appendFieldToBuffer(selectedNode.Address3, " ", " ");
	this.appendFieldToBuffer(selectedNode.City, "City: ", " ");
	this.appendFieldToBuffer(selectedNode.State, "State: ", " ");
	this.appendFieldToBuffer(selectedNode.Zip, "Zip: ", " ");
	this.appendFieldToBuffer(selectedNode.Country, "Country: ", " ");
		
	this.setAttribute("Address1", selectedNode.Address1);
	this.setAttribute("Address2", selectedNode.Address2);
	this.setAttribute("Address3", selectedNode.Address3);
	this.setAttribute("City", selectedNode.City);
	this.setAttribute("State", selectedNode.State);
    this.setAttribute("StateCode", selectedNode.StateCode);
	this.setAttribute("Zip", selectedNode.Zip);
	this.setAttribute("CountryCode", selectedNode.CountryCode);
	
} 
/**
 * create node data for a Phone object.
 */
function aPhoneStringForClientSearch() {
  var selectedNode = Nba_ClientSearch_LP.getActiveNode();	
    this.appendFieldToBuffer(selectedNode.PhoneType, " ", ": ");
    this.appendFieldToBuffer(selectedNode.AreaCode, "(", ")");
	this.appendFieldToBuffer(selectedNode.DialNumber, " ", " ");
	
	this.setAttribute("PhoneTypeCode", selectedNode.PhoneTypeCode);
	this.setAttribute("PhoneType", selectedNode.PhoneType);
	this.setAttribute("AreaCode", selectedNode.AreaCode);
	this.setAttribute("DialNumber", selectedNode.DialNumber);	
}
/**
 * create node data for a Email object.
 */ 
function aEmailStringForClientSearch() {
  var selectedNode = Nba_ClientSearch_LP.getActiveNode();	
   
   this.appendFieldToBuffer(selectedNode.EMailType, " ",": ");
   this.appendFieldToBuffer(selectedNode.EMailAddress, "  ",null);
  
   this.setAttribute("EMailTypeCode", selectedNode.EMailTypeCode);
   this.setAttribute("EMailType", selectedNode.EMailType);
   this.setAttribute("EMailAddress", selectedNode.EMailAddress);	
}
// End of NBA067
// NBA068 New Method
/**
 * create node data for an Inforce Payment object.
 */
function aInforcePaymentString() {	
	this.appendFieldToBuffer(getTranslatedValue("IFPT_IFPaymentType_DD"), "Inforce Payment Type: ");
	this.appendFieldToBuffer("IFPD_IFPaymentDate_EF", dataSep);
	if(getField("IFMP_IFPmntManualInd_CB").checked == false){
		this.appendFieldToBuffer("No",dataSep +  "Manual: ");
	}else{
		this.appendFieldToBuffer("Yes",dataSep +  "Manual: "); 
	}

	this.setAttribute("IFPaymentType", "IFPT_IFPaymentType_DD");
	this.setAttribute("IFPaymentDate", "IFPD_IFPaymentDate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("IFPmntManualInd", "IFMP_IFPmntManualInd_CB");
}

/**
 * create node data for a Coverage object on Increase Tab.
 */
//NBA077 New Method
function aCoverageStringForIncrease() {
	var selectedNode = objIncreaseTree.getActiveNode();
	//begin SPR3018
	if(getField("backendId").value == SYST_VANTAGE)	{
		this.appendFieldToBuffer(selectedNode.PlanTranslation, "Coverage: ");
	} else { 
		this.appendFieldToBuffer(getTranslatedValue("productCode_Coverage_DD"), "Coverage: ");
	}
	//end SPR3018
	this.appendFieldToBuffer("currentNumberOfUnits_Coverage_EF", dataSep);

	//calculate new effective date
	var effDate = new Date(getField("effYear_Coverage_EF").value, getField("effMonth_Coverage_EF").value, selectedNode.IssueDays);
	var effDateString = effDate.getMonth() + "/" + effDate.getDate() + "/" + effDate.getYear();
	this.appendFieldToBuffer(effDateString, dataSep + "Effective: ");
	this.appendFieldToBuffer("rateClass_Coverage_DD", dataSep + "RateClass: ");	
}

/**
 * create node data for a CovOption object on Increase tab.
 */
//NBA077 New Method
function aCovOptionStringForIncrease() {
	this.appendFieldToBuffer(getTranslatedValue("productCode_CovOption_DD"), "");
	this.appendFieldToBuffer("optionNumberofUnits_CovOption_EF", dataSep);
	this.appendFieldToBuffer("effDate_CovOption_EF", dataSep + "Effective: ");	
	this.appendFieldToBuffer("termDate_CovOption_EF", dataSep + "Cease Date: ");
}

/**
 * create node data from the CommissionCalcActivity on Increase tab.
 */
//NBA077 New method	
function aNbaCommissionCalcActivityStringForIncrease() {
	this.appendFieldToBuffer("companyProducerID_CommissionCalcActivity_EF","Identification: ");
	if(getField("agencySystem").value == AGENCY_CONTROL_FILE){
		this.appendFieldToBuffer("volumeSharePct_CommissionCalcActivity_EF",dataSep +  "Volume Share: ");
		this.appendFieldToBuffer(getTranslatedValue("situationCode_CommissionCalcActivity_DD"),dataSep +  "Situation: ");	
	}
	if(getField("agencySystem").value == PERFORMANCE_PLUS){
		this.appendFieldToBuffer("situationCode_CommissionCalcActivity_EF",dataSep +  "Profile: ");	
	}	
}
/**
 * create node data for a Annuity object.
 * New Method for SPR2039
 */
function aAnnuityString() {
	this.appendFieldToBuffer(getTranslatedValue("coverage_ProductCode_DD"), "Plan: ");
	this.appendFieldToBuffer("coverage_effDate_EF", dataSep + "Effective: ");
	this.appendFieldToBuffer(getTranslatedValue("qualPlanType_DD"), dataSep + "Qualification Type:  ");				
	if(getField("CurrentAmt_Amt_RB").checked){
		this.setAttribute("CurrentAmt", "coverage_CurrentAmt_EF",FORMAT_TYPE_CURRENCY);
		getField("coverage_CurrentAmt2_EF").value = "";
	}else{
		this.setAttribute("CurrentAmtUnits", "coverage_CurrentAmt2_EF");	
		getField("coverage_CurrentAmt_EF").value = "";
	}
	this.setAttribute("PlanName", "coverage_ProductCode_DD");
	this.setAttribute("EffDate", "coverage_effDate_EF", FORMAT_TYPE_DATE);
	this.setAttribute("LifeCovStatus", "coverage_lifeCovStatus_EF");
	this.setAttribute("RequestedMaturityType", "reqMaturity_RB");
	if(getField("reqMaturity_RB")[0].checked){
		this.setAttribute("RequestedMaturityAge", "reqMaturity_requestedMaturityAge_EF");
		getField("reqMaturity_requestedMaturityDate_EF").value = "";
		getField("reqMaturity_requestedMaturityDuration_EF").value = "";
		this.setAttribute("RequestedMaturityDate", "");
		this.setAttribute("RequestedMaturityDuration", "");
	}else if(getField("reqMaturity_RB")[1].checked){
		this.setAttribute("RequestedMaturityDate", "reqMaturity_requestedMaturityDate_EF");
		getField("reqMaturity_requestedMaturityAge_EF").value = "";
		getField("reqMaturity_requestedMaturityDuration_EF").value = "";
		this.setAttribute("RequestedMaturityDuration", "");
		this.setAttribute("RequestedMaturityAge", "");
	}else if(getField("reqMaturity_RB")[2].checked){
		this.setAttribute("RequestedMaturityDuration", "reqMaturity_requestedMaturityDuration_EF");
		getField("reqMaturity_requestedMaturityAge_EF").value = "";
		getField("reqMaturity_requestedMaturityDate_EF").value = "";
		this.setAttribute("RequestedMaturityDate", "");
		this.setAttribute("RequestedMaturityAge", "");
	}
	this.setAttribute("FirstTaxYear", "annuity_FirstTaxYear_EF");
	this.setAttribute("QualPlanType", "qualPlanType_DD");

}



/**
 * A list of functions exposed by the ItemPrinter.
 * The methods that should be invoked externally are 
 * 1. itemToString(aKey) To be invoked when display strings are needed
 * 2. createNode(aKey) To be invoked when need new nodes are needed 
 */	 
ItemPrinter.prototype.appendFieldToBuffer = appendFieldToBuffer;
ItemPrinter.prototype.itemToString = itemToString;
ItemPrinter.prototype.setAttribute = setAttribute;
ItemPrinter.prototype.createNode = createNode;
ItemPrinter.prototype.aAddressString = aAddressString;
ItemPrinter.prototype.aPersonStringForNameOnly = aPersonStringForNameOnly;
ItemPrinter.prototype.aPhoneString = aPhoneString;
ItemPrinter.prototype.aEndorsementString = aEndorsementString;
ItemPrinter.prototype.aCoverageString = aCoverageString;
ItemPrinter.prototype.aLifeParticipantString = aLifeParticipantString;
ItemPrinter.prototype.aNbaSourceString = aNbaSourceString; //NBA033
ItemPrinter.prototype.aAdditionalContractString = aAdditionalContractString; //NBA033
ItemPrinter.prototype.aAddressStringForCovBenList = aAddressStringForCovBenList;
ItemPrinter.prototype.aEmailString = aEmailString;
ItemPrinter.prototype.aCovOptionString = aCovOptionString;
ItemPrinter.prototype.aBeneficiaryString = aBeneficiaryString;
ItemPrinter.prototype.aPersonStringForCoverageList = aPersonStringForCoverageList;
ItemPrinter.prototype.aOrganizationStringForCoverageList = aOrganizationStringForCoverageList;
ItemPrinter.prototype.ataxReportingString = ataxReportingString;
ItemPrinter.prototype.aContractString = aContractString;
//NBA093 deleted
ItemPrinter.prototype.aBeneficiaryStringUpdate = aBeneficiaryStringUpdate;
ItemPrinter.prototype.aNbaProducerVOString = aNbaProducerVOString; //NBA043
ItemPrinter.prototype.aRiderString = aRiderString;
ItemPrinter.prototype.aDstWorkItemString = aDstWorkItemString; // NBA055
ItemPrinter.prototype.aNbaSourceStringForRelate = aNbaSourceStringForRelate; // NBA055
ItemPrinter.prototype.aAddressStringForClientSearch = aAddressStringForClientSearch; //NBA067
ItemPrinter.prototype.aPhoneStringForClientSearch = aPhoneStringForClientSearch; //NBA067
ItemPrinter.prototype.aEmailStringForClientSearch = aEmailStringForClientSearch; //NBA067
ItemPrinter.prototype.aInforcePaymentString = aInforcePaymentString; //NBA068
//begin NBA077
ItemPrinter.prototype.aCoverageStringForIncrease = aCoverageStringForIncrease; 
ItemPrinter.prototype.aCovOptionStringForIncrease = aCovOptionStringForIncrease; 
ItemPrinter.prototype.aNbaCommissionCalcActivityStringForIncrease = aNbaCommissionCalcActivityStringForIncrease; 
//end NBA077
ItemPrinter.prototype.aAnnuityString = aAnnuityString; //SPR2039






