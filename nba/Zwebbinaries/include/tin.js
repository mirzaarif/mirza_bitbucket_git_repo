<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA096           4       Modification of indexing view -->

//<script language="JavaScript">
//function for IE, we assume default browser as IE
function tin_onKeyPress(field)
{	
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = tin_onKeyPress_NS;							
		return;	
	}
	
	//Retrieve keycode, allow only numbers
	var inputChar = window.event.keyCode;
	if(inputChar < 48 || inputChar > 57){
		return window.event.returnValue = false;		
	}
	var strInput = field.value;
	
	//format the field	
	if (strInput.length == 7){
		field.value += "-";	
	}		
	if (strInput.length > 9){
		return window.event.returnValue = false;
	}
	return window.event.returnValue = true;
}

//function for netscape
function tin_onKeyPress_NS(e) {
	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 )){
		return false;		
	}
	var strInput = this.value;	
	
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if (strInput.length == 7)
			this.value += "-";					
		if (strInput.length > 10)
			return false;
	}		
	return true;
}       

function tin_onChange(field)
{				
	if(!tin_Validate(field)){
		return;
	}
	field.value = tin_Format(field.value);
	data_Changed(field);	
}

function tin_Validate(field)
{
	var strInput = field.value;

	if(strInput.length == 0){
		return true;
	}
	if( strInput.indexOf("-") != 7 || strInput.length < 10){
		raiseError("Invalid Tax Identification Number!",field);	
		return false;
	}
	field.valid = strInput ;
	field.style.color = '';
	return true;	
}

function tin_Format(strInput)
{
	if(strInput.length == 0){
		return strInput;
	}
	if(strInput.indexOf("-", 0) > -1){ // aleady formated
		return strInput;
	}
	return strInput.substring(0, 7) + "-" + strInput.substring(7);
}

function tin_UnFormat(strInput)
{
	aCurrStripChars = new Array("-");
	
	if(strInput.length == 0){
		return strInput;
	}
	return parseField(strInput, aCurrStripChars);	
}