<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA024            2      Conversion of applet based views to HTML -->
<!-- NBA044            3      Architecture changes -->

//<script language="JavaScript">
//NBA044 new parameters added
function alphanumeric_onKeyPress(field,event, isCapital, isAlphaOnly)
{
	if (navigator.appName == 'Netscape'){		
		field.onkeypress = alphanumeric_onKeyPress_NS;							
		return;	
	}			
	//begin NBA044
	if(isCapital){
		field.setAttribute("isCapital", "1");
	}
	if(isAlphaOnly){
		field.setAttribute("isAlphaOnly", "1");
	}
	//end NBA044
	var bCapital = (field.getAttribute("isCapital")== 1 ?  true : false) ;		
	var bAlphaOnly = (field.getAttribute("isAlphaOnly") == 1 ?  true : false) ;
	
	var inputChar = window.event.keyCode;	
	
	if(	(inputChar < 65 || inputChar > 90) &&	
		(inputChar < 97 || inputChar > 122) &&
		((!bAlphaOnly && (inputChar < 48 || inputChar > 57)) || bAlphaOnly)) //Allow numeric characters only if bAlphaOnly is false
		return window.event.returnValue = false;	
		
	if (bCapital) {
		var c = event.keyCode;
		event.keyCode = String.fromCharCode(c).toUpperCase().charCodeAt(); 
		return true;		
	}
		
		
	
}

function alphanumeric_onKeyPress_NS(e)
{	
	var inputChar = e.which;	
	var bCapital = (field.getAttribute("isCapital")== 1 ?  true : false) ;		
	var bAlphaOnly = (field.getAttribute("isAlphaOnly") == 1 ?  true : false) ;
	
	if(	(inputChar < 65 || inputChar > 90) &&	
		(inputChar < 97 || inputChar > 122) &&
		((!bAlphaOnly && (inputChar < 48 || inputChar > 57)) || bAlphaOnly)) //Allow numeric characters only if bAlphaOnly is false
		return window.event.returnValue = false;							
	
	if (bCapital) {
		var strInput = this.value;
		this.value = strInput.toUpperCase() + inputChar.toUpperCase();
	}
}

function alphanumeric_onChange(field)
{		
	field.value = alphanumeric_Format(field.value);
	data_Changed(field);	
}

function alphanumeric_Format(strInput)
{
	return strInput;
}