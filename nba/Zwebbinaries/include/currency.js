//<script language="JavaScript">
function currency_onKeyPress(field)
{	
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = currency_onKeyPress_NS;							
		return;	
	}
	
	var inputChar = window.event.keyCode;
	var strInput = field.value;
	var strVal = strInput + (inputChar - 48); 
	var nMaxLenInt = parseInt(field.getAttribute("LEN"));
	
	var nIndexDollar = strInput.indexOf('$');

	var temp =''; //temp Variable
		
	if(	inputChar!= 46 &&						// not a "."
		(inputChar < 48 || inputChar > 57)) 	// not a number 			
		return window.event.returnValue = false ;		

	if( inputChar == 46 && strInput.indexOf(".", 0) > -1 ) // found "." already	
		return window.event.returnValue = false ;
	
	if (nMaxLenInt > 0 && inputChar != 46) //If max integer length parameter supplied, do length validation
	{
		strVal = currency_UnFormat(strVal);
		temp = parseInt(strVal, 10).toString(); //Get integer portion of float & convert to String
				
		if 	(temp.length > nMaxLenInt)		
			return window.event.returnValue = false;
	}  
	
	if ( strInput.indexOf(".") > -1 &&  strInput.indexOf(".") == strInput.length - 3 ) // allow two decimals points	
		return window.event.returnValue = false;
	
	if (strInput.length == 1 && strInput != "$" )		
		field.value = "$" + strInput;			
	
	return window.event.returnValue = true;
}

function currency_onKeyPress_NS(e)
{	
	var inputChar = e.which;	
	var strInput = this.value;
	var strVal = strInput + (inputChar - 48); 
	var nMaxLenInt = parseInt(this.getAttribute("LEN"));
	
	var nIndexDollar = strInput.indexOf('$');
								
	var temp =''; //temp Variable	
		
	//allow only numbers,".", or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8))&& (inputChar!= 46) && (inputChar < 48 || inputChar > 57 ))
		return false;		
	
	//format only if keystroke was a number,"."
	if (!(inputChar == 0 || inputChar == 8)){	
		if( inputChar == 46 && strInput.indexOf(".", 0) > -1 ) // found "." already	
			return false ;
	
		if (nMaxLenInt > 0 && inputChar != 46) //If max integer length parameter supplied, do length validation
		{
			strVal = currency_UnFormat(strVal);
			temp = parseInt(strVal, 10).toString(); //Get integer portion of float & convert to String
				
			if 	(temp.length > nMaxLenInt)		
				return false;
		}  
	
		if ( strInput.indexOf(".") > -1 &&  strInput.indexOf(".") == strInput.length - 3 ) // allow two decimals points	
			return false;
		
		if (strInput.length == 1 && strInput != "$" )		
			this.value = "$" + strInput;			
	}	
	return true;
}

function currency_onChange(field)
{
	field.value = currency_Format(field.value,parseInt(field.getAttribute("LEN")));	
	data_Changed(field);	
}

function currency_Format(strInput,len)
{		
	if(strInput.length == 0)
		return strInput;				
		
	strInput =	currency_UnFormat(strInput);		
	var pos = strInput.indexOf(".", 0); 
	if(pos == -1)  // no decimal point yet
		if(len && strInput.length > len)			
			strInput = 	strInput.substring(0,len) + "." + strInput.substring(len)				
		else
			strInput += ".00";				
			
	//recalculate again		
	var pos = strInput.indexOf(".", 0);
	
	if(pos == strInput.length - 1)
		strInput += "00"

	if(pos == strInput.length - 2)
		strInput += "0";
	
	if(strInput.indexOf("$") == -1) 		
		strInput = "$" + strInput;	
	
	//decimals are there for sure
	pos = strInput.indexOf(".", 0);
		 
	var strTail = strInput.substring(pos);	
	strInput = strInput.substring(0, pos);	
	
	strHead = strInput.substring(0, 1);	
	strInput = strInput.substring(1); 		

	if (strInput.length == 0)
		strInput = "0";

	if(strInput.indexOf(",", 0) == -1 && strInput.length > 3)  // not formated yet
	{	
		while(strInput.indexOf("0", 0) == "0")
		{
			strInput = strInput.substring(1);
		}
		
		while(strInput.length > 3)
		{
			pos = strInput.length - 3		
			strTail = "," + strInput.substring(pos) + strTail;		
			strInput = strInput.substring(0, pos);		
		}
	}		
	
	return strHead + strInput + strTail;		
}
function currency_UnFormat(strInput)
{
	aCurrStripChars = new Array("$",",");
	
	if(strInput.length == 0)
		return strInput;
		
	return parseField(strInput, aCurrStripChars)	
}