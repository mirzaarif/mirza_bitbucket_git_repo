<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1072           2      Make the code more abstract so that it can be used by all applications -->
<!-- NBA009            2      Cashiering Workbench -->
<!-- NBA024            2      Conversion of applet based views to HTML -->
<!-- NBA009            2      Cashiering -->
<!-- NBA025            2      Business Function Security -->
<!-- NBA031            3      Rewrite logon/menu/status bar in HTML -->
<!-- NBA028            3      Change view background color -->
<!-- SPR1234           3      Generic cleanup -->
<!-- NBA044            3      Architectural changes -->
<!-- NBA061            3      Adopt Struts Framework -->
<!-- NBA079            3      Underwriter Workbench Enhancements -->
<!-- NBA097            4      Work Routing Reason Displayed -->
<!-- ACN008            4      Underwriter Workflow Changes -->
<!-- ACN011            4      Support for Canadian Provinces -->
<!-- ACN007            4      Reflexive Questioning -->
<!-- SPR1437           5      Refresh button is disabled in view-only mode. -->
<!-- SPR2674           5      Tabs are disabled during a server round trip from nbA views -->
<!-- SPR3394		   7	  A carriage return entered in an Endorsement description causes a Java Script error -->
<!-- NBA267		   	   8	  nbA IE 7.0 Certification Project -->
<!-- FNB004		   	 NB-1101  PHI -->
<!-- SPRNBA-478		NB-1101	Database Errors Occur when Comments on Requirements Exceed 100 Characters -->

//<script language="JavaScript">
//Define global variables here

//begin SPR1072
//status constants - used by Treeview and Table
//Deleted
var STATUS_DELETED = "B";
//Added
var STATUS_ADDED = "A";
//Updated
var STATUS_UPDATED = "U";
//Nochange
var STATUS_NOCHANGE = "N";
//Ordered
var STATUS_ORDERED = "O"; //NBA079
//Added & Deleted without saving
var STATUS_JUNK = "J";
//Delimiters
var DELIMITER_ROW = "�";
var DELIMITER_CELL = "�";
var DELIMITER_HIDDEN = "�";
//field identifier constants
var cell_text = "0";
var cell_select = "1";
var cell_checkbox = "2";
var cell_hidden = "4";
//end SPR1072
//begin NBA044
var FORMAT_TYPE_TEXT = 0;
var FORMAT_TYPE_CURRENCY = 1;
var FORMAT_TYPE_DATE = 2;
var FORMAT_TYPE_PHONE = 3;
var FORMAT_TYPE_SSN = 4;
var FORMAT_TYPE_ZIP = 5;
var FORMAT_TYPE_SIN = 6;//ACN011
var FORMAT_TYPE_POSTALCODE = 7;//ACN011
var HTML_SPACE = "&nbsp;&nbsp;" ;
var isLoad_Context = true;
//end NBA044
//global multi-purpose variable
var call_focus = false;

function getCheckedValue(aButtonGroup){
	var rb_group = getField(aButtonGroup);
	for( var i = 0; i < rb_group.length; i++){
		if(rb_group[i].checked) return rb_group[i].value;
	}	
}

function getTranslatedValue(aField){
	var aDropdown = getField(aField);	
	return (aDropdown.selectedIndex > -1 ? aDropdown.options[aDropdown.selectedIndex].text: "");
}
/**
 * Returns selected values for a multi- select element
 */
//NBA044 new method
function getMultiSelectValues(aField){
	var values = new Array();
	var aDropdown = getField(aField);	
	for(i = 0; i < aDropdown.options.length; i++){
		if(aDropdown.options[i].selected){
			values[values.length] = aDropdown.options[i].value;
		}
	}
	return values;
}
/**
 * Selects values on a multi- select element
 * example 
 * var values = new Array();
 * values[0] = "TX"; values[1] = "MN";
 * selectMultiSelectValues(aFieldName, values);
 */
//NBA044
function selectMultiSelectValues(aField, values){	
	var aDropdown = getField(aField);		
	for(i = 0; i < aDropdown.options.length; i++){
		if(values.length > 0){
			for (j = 0; j < values.length; j++){
				if (values[j] == aDropdown.options[i].value){
					aDropdown.options[i].selected = true;
					break;
				} else {
					aDropdown.options[i].selected = false;
				}
			} 
		} else {
			aDropdown.options[i].selected = false;
		}		
	}		
}	
//SPR1072 lines deleted

//NBA009 method added
function createDIV(aParent, aName, aClass, aHeight, aWidth, aTop, aLeft){	
	var str = '<DIV id="' + aName + '" style="visibility:inherit;position:absolute; LEFT: ' + aLeft + 'px; TOP: ' + aTop + 'px; WIDTH: ' + aWidth + 'px; HEIGHT: ' + aHeight + 'px;" class="' + aClass + '"></DIV>';    
	var aParent = eval('window.document.getElementById("' + aParent + '")');		
	aParent.innerHTML += str;
}

//NBA009 method added
function createLabelledDIV(aParent, aName, aLabel, aClass, aHeight, aWidth, aTop, aLeft){		
	var str = '<FIELDSET STYLE="LEFT:' + aLeft + 'px; WIDTH:' + aWidth + 'px; POSITION: absolute; TOP:' + aTop + 'px; HEIGHT:' + aHeight + 'px" ID="container_' + aName + '">';
	str += '<LEGEND ALIGN=left VALIGN="TOP"><SPAN>'+ aLabel + '</SPAN></LEGEND>'; 
	str += '<DIV id="' + aName + '" style="visibility:inherit;position:absolute; LEFT: 10px; TOP: 20px; WIDTH: ' + parseInt(aWidth - 20) + 'px; HEIGHT: ' + parseInt(aHeight - 30) + 'px;" class="' + aClass + '"></DIV></FIELDSET>';		
	var aParent = eval('window.document.getElementById("' + aParent + '")');		
	aParent.innerHTML += str;
}

//NBA009 method added
function createSPAN(aParent, aLabel, aHeight, aWidth, aTop, aLeft){		
	var str = '<SPAN STYLE="LEFT:' + aLeft + 'px; WIDTH:' + aWidth + 'px; POSITION: absolute; TOP:' + aTop + 'px; HEIGHT:' + aHeight + 'px" >' + aLabel + '</SPAN>';
	var aParent = eval('window.document.getElementById("' + aParent + '")');		
	aParent.innerHTML += str;
}

//NBA024 method added
function openDialog(aScreen, aWidth, aHeight, returnFields, inputFields){		
	var queryString = "?";
	var options = "";						
				
	if (arguments.length > 4){		
		for (var i = 4; i < arguments.length; i++) {		    
			queryString += arguments[i];
						    
			if (i < (arguments.length -1)) {
				queryString += "&";
			}
		}
	}
				
	var dialogArguments = new Array(queryString, returnFields);		
	options = "resizable:0;help:0;status:0;dialogWidth:" + aWidth + "px;dialogHeight:" + aHeight + "px";		
	return showModalDialog(aScreen, dialogArguments, options);
}

//disable/enable a list of available buttons
function disableFields(disable, names){
	for( var i = 1; i <  arguments.length; i++){
		disableField(disable, arguments[i]);
	}
}		
//disable/enable a button
function disableField(disable, aName){
	//begin NBA044
	var aField = getField(aName);
	if(aField){		
		getField(aName).disabled = disable;		
	}
	//end NBA044	
}		

//NBA044 method added
function disableLabel(disable, aName){		
	document.getElementById(aName).disabled = disable;		
}

/*
This function disables/enables buttons	based on the "disable" parameter.
skipComponents decides whether or not to skip enabling/disabling of buttons embedded within components.
skipClose decides whether to disable/enable a close button
e.g. disableAllButtons(true , true, true); for privilege implementation
*/
function disableAllButtons(disable, skipComponents, skipClose){
	var fields = window.document.forms[0].getElementsByTagName("button");
	
	for( i= 0 ; i < fields.length  ; i ++ )	{
		if(fields[i].name.length > 0 || !skipComponents){
			if((fields[i].name.toLowerCase() == "close" || 
				fields[i].name.toLowerCase() == "cancel" ||
				fields[i].name.toLowerCase() == "nba_refresh_pb" || // SPR1437
				fields[i].name.toLowerCase() == "nba_cancel_pb") && skipClose){ //NBA044
				continue;
			}
			if(disable){
				fields[i].setAttribute("wasdisabled",fields[i].disabled);
			}			
			if(!fields[i].getAttribute("wasdisabled")) {					
				fields[i].disabled = disable;	
			}					
		}	
	}
		
	fields = window.document.forms[0].getElementsByTagName("input");
	for( i= 0 ; i < fields.length  ; i ++ )	{	
		if( fields[i].type == "button" && (fields[i].name.length > 0 || !skipComponents)){			
			if((fields[i].name.toLowerCase() == "close" || 
				fields[i].name.toLowerCase() == "cancel" ||
				fields[i].name.toLowerCase() == "nba_cancel_pb") && skipClose){ //NBA044
				continue;
			}
			if(disable) {
				fields[i].setAttribute("wasdisabled",fields[i].disabled);
			}			
			if(!fields[i].getAttribute("wasdisabled")) {
				fields[i].disabled = disable;
			}					
		}
	}						
}
	
////clent -server communication methods begin here
function invokeServer(servletName){
	var oldAction = window.document.forms[0].action; //NBA044 	
	window.document.forms[0].action = "servlet/" + servletName;		
	window.document.forms[0].submit();	
	window.document.forms[0].action = oldAction; //NBA044 put back the old action
	window.document.forms[0].style.cursor = "wait";	
	if (isLoad_Context) {		
		resetRBs();
		handleDDFocus();		
	}		
	isLoad_Context = false;	
	disableScreen(true);	
}
/**
 * This method POSTs an HTML FORM to the Struts ActionServlet
 */
//NBA061 New Method
function invokeActionServlet(){	
	window.document.forms[0].submit();	
	window.document.forms[0].style.cursor = "wait";	
	disableScreen(true);	
}
/**
 * This method refreshes a view
 */
//NBA061 New Method
function refreshView(){	
	window.location = window.document.forms[0].action;
}

//parameter added to allow custom handling of errors - SPR1072
function displayData(e){
	//if there is only one form than skip following process as this page will not have any visible field.
	if(window.document.forms.length > 1 && window.document.forms[0].innerHTML != ""){ //NBA044, ACN008
		disableScreen(false);
		resetCBs();
		disableAllButtons(disable_update, true, true);	//NBA025;
		e = (arguments.length == 0 ? "": e); //SPR1072 pass on the error to the handle_callBack implementation
		if(handle_callBack){ //NBA044
			handle_callBack(e); //SPR1072 line changed. This function must be implemented by every nbA screen
		} //NBA044	
		window.document.forms[0].style.cursor = "auto";
	}	//NBA044
}

function disableScreen(indicator)
{	
	//begin SPR1072
	//disable/enable buttons	
	disableAllButtons(indicator , true, false);	//SPR1234
	//end SPR1072
	
	//SPR2674 code deleted
		
	var visibility = ( indicator == true ? "inherit" : "hidden")
	//show/hide waitscreen
	window.document.getElementById("WaitScreen").style.visibility = visibility;			
}

////clent -server communication methods end here 

////Applet communication methods begin here

//Lets the applet return to its default state
function resetApplets(){
	//NBA025 code deleted
}
////Applet communication methods end here

////utitlity methods begin here
//new method - SPR1072
function destroyForm(){
	//NBA031 line deleted
	updateStatus("status1", "");	//NBA025 //NBA031
	destroySession(); //NBA044	
	window.document.forms[0].innerHTML = "";
}	

//on F1 show help file
window.document.onhelp = handleHelp;
function handleHelp(){	
	openHelp();
	return false;
}

function handlePaste(){return false;}

//a function written for Post Phase I
function data_Changed(field){};

//  Displays a message box and sets focus to a control specified in the field parameter.
function raiseError(sError, field, type)
{
	if (!type){
		var ret = window.confirm(sError + "\nPress OK to make the correction or Cancel to revert back to the old value."); 	
		if(field != null && !field.disabled && ret)	{				
			setTimeout("document.forms[0]." + field.name + ".select()",1,"JavaScript");	  	  			
		}else{
			field.value = ( (!field.valid) ? "" : field.valid);			
		}
	}
}

//Tis is a Form field helper method
//method signature changed -- NBA031
function getField(name, formIndex){
	var index = formIndex ? formIndex : 0;	
	//begin NBA044
	var field = null;
	if(name){	
		try{
			field = eval("window.document.forms[" + index + "]." +  name); 
		} catch(e){}
	}
	//end NBA044	
	return field ;
}

function parseField(strInput, aStripChars)
{
	var strOutput = ""
	var i, j
	var nInputLen = strInput.length
	var nArrayLen = aStripChars.length

	if (!strInput || strInput.length == 0)	{
		return ""
	}
	for (i = 0 ; i < nInputLen ; i++)
	{
		for (j = 0 ; j < nArrayLen ; j++){
			if (strInput.charAt(i) == aStripChars[j]){
				break;
			}
		}
		if (strInput.charAt(i) == aStripChars[j]){
			continue;
		}
		strOutput += strInput.charAt(i)
	}
	return strOutput;
}

//new method added to parse an HTML space - SPR1072
function parseHTMLSpace(text){
	var newString = "";
	var start = 0;
	var end = 0;
	end = text.indexOf("&nbsb;", start);	
	while (end > -1) {
		newString += text.substring(start, end);
		newString += " ";
		start = end + "&nbsb;".length;
		end = text.indexOf("&nbsb;", start);
	}
	newString += text.substring(start);
	return newString;
}
////utitlity methods end here --SPR1072

////DHTML data setters grouped together
function addSecureField(name)
{	
	document.write("<INPUT TYPE=HIDDEN NAME=" + name + ">")    
}

/**
 * Unformats a value based on format type.
 */
//NBA044 new method
function unFormatValue(aValue, formatType){	 
	switch (parseInt(formatType))	{
		case FORMAT_TYPE_TEXT: aValue = aValue; break;	
		case FORMAT_TYPE_CURRENCY: aValue = currency_UnFormat(aValue); break;	
		case FORMAT_TYPE_DATE: aValue = date_UnFormat(aValue); break;	
		case FORMAT_TYPE_PHONE: aValue = phone_UnFormat(aValue); break;	
		case FORMAT_TYPE_SSN: aValue = ssn_UnFormat(aValue); break;	
		case FORMAT_TYPE_ZIP: aValue = zip_UnFormat(aValue); break;	
		case FORMAT_TYPE_SIN: aValue = sin_UnFormat(aValue); break;//ACN011
		case FORMAT_TYPE_POSTALCODE: aValue = postal_UnFormat(aValue); break;//ACN011
		case 8: break;
		default:
	}	
	return aValue;
} 
/**
 * Formats a value based on format type.
 */
//NBA044 new method
function formatValue(aValue, formatType){	 
	switch (parseInt(formatType))	{
		case FORMAT_TYPE_TEXT: aValue = aValue; break;	
		case FORMAT_TYPE_CURRENCY: aValue = currency_Format(aValue); break;	
		case FORMAT_TYPE_DATE: aValue = date_Format(aValue); break;	
		case FORMAT_TYPE_PHONE: aValue = phone_Format(aValue); break;	
		case FORMAT_TYPE_SSN: aValue = ssn_Format(aValue); break;	
		case FORMAT_TYPE_ZIP: aValue = zip_Format(aValue); break;
		case FORMAT_TYPE_SIN: aValue = sin_Format(aValue); break;//ACN011
		case FORMAT_TYPE_POSTALCODE: aValue = postal_Format(aValue); break;//ACN011	
		case 8: break;
		default:
	}	
	return aValue;
}
/**
 * Returns field data irrespective of whether it is a
 * a dropdown, checkbox, radio button, textarea or a text field (text or hidden)
 */
//NBA044 new method
function getFieldData(fieldName){
	var field = eval("window.document.forms[0]." +  fieldName);	
	var fieldValue = "";
	if(field.length && field[0].type =="radio"){
		fieldValue = getCheckedValue(fieldName);
	} else if(field.type && field.type =="checkbox"){
		fieldValue = field.checked;
	} else {
		fieldValue = field.value;			
	}
	return fieldValue;
}
/**
 * Sets field data irrespective of whether it is a
 * a dropdown, checkbox, radio button, text area or a text field (text or hidden)
 */
//NBA044 new method
function setFieldData(fieldName, fieldValue, formatType){	
	var field = eval("window.document.forms[0]." +  fieldName);	
	if(field.length && field[0].type =="radio"){		
		setRB(fieldName, fieldValue);
	} else if(field.type && field.type =="checkbox"){
		setCB(fieldName, fieldValue);
	} else { //DD or Text		 		
		setEF(fieldName, fieldValue, parseInt(formatType));
	}	
}
//Use this function to set text values
//Pass fieldName/Value as a string 
//Specify the formatType as indicated below
function setEF(fieldName, fieldValue, formatType, disabled)
{  
	var field = eval("window.document.forms[0]." +  fieldName);   
    if(arguments.length > 3){ //NBA044
    	field.disabled = disabled;  
    } //NBA044
	//begin NBA044	
	if(formatType == -1 && field.getAttribute("formatType")){
		formatType = field.getAttribute("formatType");
	}	
	//end NBA044
	//NBA044 code deleted	
	//begin NBA044
	fieldValue = formatValue(fieldValue, formatType);
	if(formatType > 0){
		field.valid = fieldValue;
	}
	field.value = fieldValue;    
	//end NBA044
}

//set Radio button selection here
//SPR1072 changed method signature --added another parameter "disabled"
function setRB(fieldName, fieldValue, disabled)
{  
	var field = eval("window.document.forms[0]." +  fieldName);
	for( i= 0 ; i < field.length  ; i ++ ){	//NBA044		
		if( field[i].value == fieldValue ){
			field[i].checked = true;			
		//begin NBA044			
		} else {
			field[i].checked = false;
		}
		if(arguments.length > 2){
    		field[i].disabled = disabled;  
    	}
    	//end NBA044
	} //NBA044	
	 
}

//Selects an Option in a specified Drop Down
function setDD(fieldName, fieldValue, disabled)
{  
	var field = eval("window.document.forms[0]." +  fieldName); 	
	if(arguments.length > 2){ //NBA061	
		field.disabled = disabled;
	} //NBA061	
	field.value = fieldValue ;	
}

//checks/unchecks a checkbox
function setCB(fieldName, checked, disabled)
{		
	var field = eval("window.document.forms[0]." +  fieldName);  	
    field.checked = eval(checked);  //NBA044
    if(arguments.length > 2){ //NBA044
    	field.disabled = disabled;  
    } //NBA044	
}

//Adds data to a Drop Down
function addDataToDD(fieldName, value, encode, decode, blankRequired, clear)
{  
	var newOpt = "";
	var selLength = "";	
	var i;  
	var field = eval("window.document.forms[0]." +  fieldName);  
		
	if ( typeof( encode ) == "string" )	{
		encode = encode.split("|");    
	}
	if ( typeof( decode ) == "string"){ //SPR1072 line changed
		decode = decode.split("|");    
	}		
	if(clear){
		while(field.length){
			field.remove(0);
		}	    
	}		
  
	if (blankRequired){
		newOpt  = new Option("", "");				
		field.options[0] = newOpt;
	}	
	
	for (i = 0 ; i < encode.length ; i++){
		newOpt  = new Option(decode[i], encode[i]);
		selLength = field.length;
		field.options[selLength] = newOpt;	
	}		
	field.value = value ;
}

////Default data setter methods end here 

////Methods provided below customize Radio Button values/Check Box values and Drop down options

function resetRBs()
{	
	var fields = window.document.forms[0].getElementsByTagName("input");	
	for( i= 0 ; i < fields.length  ; i ++ ){					
		if( fields[i].type == "radio" )	{	
			var field = eval("window.document.forms[0]." +  fields[i].name);			
			for( j= 0 ; j < field.length  ; j ++ )							
				field[j].checked = false;						
		}
	}		
}

function handleDDFocus(){
	if (navigator.appName != 'Netscape'){
		var fields = window.document.forms[0].getElementsByTagName("SELECT");		
	
		for( i= 0 ; i < fields.length  ; i ++ )	{		
			fields[i].onfocusin = setBackground; //NBA267
			fields[i].onblur = resetBackround;	
		}
	}		
}

function setBackground(){		
	this.style.backgroundColor = "rgb(51,51,153)" //NBA028 CHANGED FROM "highlight"	
	for( i=0 ; i < this.options.length ; i++ )
		this.options[i].style.background = "white"
}

function resetBackround(){			
	this.style.backgroundColor = ""
}

//This function marks an unchecked "checkbox" element and changes its "value" attribute
function defaultCBs(){
	var field = window.document.forms[0].getElementsByTagName("input");	
	for( i= 0 ; i < field.length  ; i ++ ){		;					
		if( field[i].type == "checkbox" && !field[i].checked ){				
			field[i].value = "false";
			field[i].checked = true;
		}
	}
}

//This function resets the "value" attribute back to its actual value
function resetCBs(){
	var field = window.document.forms[0].getElementsByTagName("input");		
	for( i= 0 ; i < field.length  ; i ++ ){									
		if( field[i].type == "checkbox" && field[i].value == "false" ){				
			field[i].value = "true";
			field[i].checked = false;							
		}		
	}
}

////-- default methods end here SPR1072

////-- These functions are specific to Application Entry. The reason they are here is that they are
////   needed by every application entry screen	
////Positions the App Entry screen
function positionView(){	
	var X,Y;	
	Y = (window.screen.availHeight);
  	X = window.screen.availWidth/2 + 10; 	 	
  	window.resizeTo(X,Y);
  	window.moveTo(0,0);   	
  	//NBA031 lines deleted	
}

//begin NBA025
// Refresh the enabled menu items
function refreshEnabledFunctions(){	
	//NBA031 line deleted
	//begin NBA061
	if(window.document.forms[0].action.search(".do") > -1){
		window.location = "welcome.jsp";
	} else { //end NBA061
		window.location = "../welcome.jsp";
	}//begin NBA061 
}

//end NBA025

// this functions needs to be called inorder to reset the screen 
function handleDefaultState(){	
	//NBA031 lines deleted
	updateStatuses("","","","","","","");	// NBA097
	destroyForm(); //SPR1072 line modified
	closeImage();
}


//NBA031 method deleted

//message the applet to open the Image Window
//NBA031 method re-named
function openAppImage(){	
	var height,width;	
	height = (window.screen.availHeight);
  	width = window.screen.availWidth/2; 	 	  	  	  	  	
	//begin NBA031
	//NBA025 code deleted		
	openImage(width + 5, 0 , width, height, getField("SID").value);
	//end NBA031
}
//ACN007 New Method
//message the applet to open the Image Window for multiple images
function openAppImages(){	
	var height,width;
	var sids;
	var sid = getField("SID").value;
	height = (window.screen.availHeight);
  	width = window.screen.availWidth/2; 	 	  	  	  	  	
	sids = sid.split("/");
	var part_num = 0;
	while( part_num < sids.length) {
		part_num+=1;
	}
	openImages(width + 5, 0 , width, height, sids);
}
//-- Application Entry specific code ends here

////these functions provide the auto page transition functionality
//check for the tab key
function checkKeystroke(){
	if(!event.shiftKey && event.keyCode == 9){	
		window.document.forms[0].setAttribute("newPage","true")
	}
}

//if the key was the "tab" key, go to the next page
function gotoNext(field,event){	
	if(window.document.forms[0].getAttribute("newPage") == "true"){			
		eval(event);		
	}	
	window.document.forms[0].setAttribute("newPage","false")
}

function getLabelText(aLabel){	
	return window.document.getElementById(aLabel).innerHTML;
}

//added a new message formatter to display messages
function MessageFormatter(){
	this.suffix = Mx_ValidationError; //NBA044 by default point to validation error constant
	this.fields = new Array();
	this.skipAppender = false; //NBA044 - provide an option for skipping an appender
}

function add(aString){
	this.fields[this.fields.length] = aString;
}

function getMessage(){
	var str = "";
	var skipSeparator = (this.fields.length == 1);
	if(!this.skipAppender){ 
		this.suffix = (this.fields.length > 1 ? APPENDER_ARE: APPENDER_IS) + this.suffix;
	}
	
	for (var i = 0; i < this.fields.length; i++){
		str += this.fields[i];
		if(i == this.fields.length - 2){ 
			str += " and ";	
			skipSeparator = true;
		}
		if(!skipSeparator) str += ", ";			
	}
	return str + this.suffix;
}

function hasErrors(){
	return (this.fields.length > 0); 
}

function clear(){
	this.fields = new Array();
}

MessageFormatter.prototype.add = add;
MessageFormatter.prototype.getMessage = getMessage;
MessageFormatter.prototype.hasErrors = hasErrors;
MessageFormatter.prototype.clear = clear;

var APPENDER_IS = " is";
var APPENDER_ARE = " are";
//begin NBA044
/*************** List of constants for Standard Messages *****************/
var Mx_DeleteConfirmation = "Are you sure you want to delete?";
var Mx_InvalidActionForSelection = "Unable to perform action for selected item.";
var Mx_SelectionRequired = "A selection from the list is required.";
var Mx_ValidationError = " missing or invalid. Please Correct";
var Mx_NotApplicable = " not applicable and will be ignored.";
var Mx_RepeatersMaximum = "The maximum number of updates has been reached - apply current updates first.";
var Mx_SubRepeatersMaximum = "The maximum number of updates has been reached.";
var Mx_PercentMustTotal100 = " percentage must total 100.";  
var Mx_100PercentRequired = " percentage must equal 100.";
/************************************************************************/
//end NBA044
//NBA031 functions for forcing to upper case
function convertToUpperLetters(field,event){

	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = convertToUpperLetters_NS;							
		return;	
	}
	if (document.all) {
		var c = event.keyCode;
		//begin NBA031	
		if(c == 32) {	//space 
			event.keyCode = 0;
			return false;
		}		
		//end NBA031
		var C = String.fromCharCode(c).toUpperCase().charCodeAt(); 
		event.keyCode = C;
		return true;
	} 
}

function convertToUpperLetters_NS(e){	
	var inputChar = e.which;	
	var strInput = this.value;
	this.value = strInput.toUpperCase() + inputChar.toUpperCase();
}
/**
 * Replace characters
 */
//SPR3394 new method
function replaceAll( str, from, to ) {
	var replacedStr = "";
	if (str == null) {
		return str;
	}
	for (var i=0; i < str.length; i++) {
		if(str.charAt(i) == from) {
			replacedStr = replacedStr + to;
		} else {
			replacedStr = replacedStr + str.charAt(i);
		}
	}
	return replacedStr;
}	
 
 //FNB004 New Function
 function toggleCBGroup(formName, selectedCB, prefix,indicator) {
		var form = document.getElementById (formName);
		selectedCB = formName + ':' + selectedCB;		// JSFs ids are formName:id
		prefix = formName + ':' + prefix;
		var inputs = form.elements; 		
		for (var i = 0; i < inputs.length; i++) {   
			if (inputs[i].type == "checkbox" && inputs[i].name != selectedCB) {	//Iterate over all checkboxes on the form
				if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
					if(inputs[i].checked==true && indicator==true){
						inputs[i].checked = false ;
						form.submit();
					}else{
						inputs[i].checked = false ;
					}
				}
			}   
		}   
	}

/*
 * Limit the number of characters in a field
 */
//SPRNBA-478 New Function
 function limitText(limitField, limitNum) {
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0, limitNum);
		}
}
 
 


     