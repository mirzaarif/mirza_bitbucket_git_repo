<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA097            4      New File.  Work Routing Reason Displayed -->
<!-- NBA118			   5	  Work Item Identification Project -->

//<script language = "javascript">
<!--
document.onkeypress=handleKeyPress

var initialize = 0
var doc, layerName, sty
var Ex, Ey, tipContent

var HEIGHT_TO_WIDTH_RATIO = 2.1/3; //NBA118
var POINT_TO_PIXEL_RATIO = 4/3; //NBA118
var HEIGHT_ADJUSTMENT_FACTOR = 1.85; //NBA118

var newIframeHeight="150pt"; //NBA118
var newIframeWidth= ""; //NBA118

doc = "document.all."
layerName = "ToolTip"
sty = ".style"
Ex = "event.x"
Ey = "event.y"

var newIframe = '<iframe id=iframM2 src="javascript:false;" scrolling="no" marginHeight="0" marginWidth="0"  frameBorder="0" ></iframe>'; //NBA118

function moveToolTip(FromTop, FromLeft){
 eval(doc + layerName + sty + ".top = "  + (eval(FromTop) + document.body.scrollTop));
 eval(doc + layerName + sty + ".left = " + (eval(FromLeft) + 15));
}

function activate(){
	//NBA118 begin
	document.all[layerName].innerHTML = newIframe;
	document.getElementById('iframM2').style.width = newIframeWidth ;
	document.getElementById('iframM2').style.height =newIframeHeight;
	var frameContent = getToolTipFrameContent(tipContent)
	document.frames.iframM2.document.open();
	document.frames.iframM2.document.write(frameContent);
	document.frames.iframM2.document.close();
	//NBA118 end
	initialize=1
}

function deActivate(){
	document.all[layerName].innerHTML = ""; //NBA118
	initialize=0
}

function overTip(){
	if(initialize){
		moveToolTip(Ey, Ex)
	    eval(doc + layerName + sty + ".visibility = 'visible'")
	} else{
		moveToolTip(0, 0)
		eval(doc + layerName + sty + ".visibility = 'hidden'")
	}
}

/*
	Sets and calculate the tooltip content.
*/
function setContent(TContent){
	//NBA118 begin
	if(tipContent != TContent) {
		var stylesheet = document.styleSheets[0];
		var j;
		var ruleList = stylesheet.rules;
	
		for (j=0; j<ruleList.length; j++)
		{
		    if (ruleList[j].selectorText == '.tooltipcontent'){
	    		break;
		    }   
		}
		var rawFont = ruleList[j].style.fontSize;
		var rawWidth = document.getElementById('Nba_status1').style.width;
		var styleFont = rawFont.split('pt');
		var styleWidth = rawWidth.split('px');
		styleFont = styleFont[0]*1;
		styleWidth = styleWidth[0]*1.5;
		tipContent = widthCalculation(TContent,styleWidth,styleFont);
	}
	//NBA118 end
}

/*
	Get the formatted tooltip content for display
*/
//NBA118 new method
function getToolTipFrameContent(content) {
	var toolTipFrame = '<html><head><LINK rel="stylesheet" type="text/css" name="stylesheet" href="include/styles.css"><head>'+'<body>'+
	'<table class="tooltipbackground" border="1" width="100%" height="100%" cellspacing="0" cellpadding="0">'+
	'<tr valign="top"><td width="99%" class="tooltipcontent">'+
    '<div><p class="indent">'+content+'</p></div>'+
	'</td></tr>'+
	'</table></body></html>';
	return toolTipFrame;
}

function handleKeyPress() { 
	// Trap Ctrl+Q to open the tooltip
	if ((window.event.ctrlKey) && (window.event.keyCode == 17)) {	
		if (!tipContent) return true;	
			if(initialize==0){
				initialize=1;
				moveToolTip(40, 0 );
				eval(doc + layerName + sty + ".visibility = 'visible'");
				activate(); //NBA118	  
			} else{
	  			initialize=0;
				moveToolTip(0, 0);
				eval(doc + layerName + sty + ".visibility = 'hidden'");
			  	deActivate(); //NBA118
	 		}
		 return false; 
	} 
	return true;
}


/*
	1. Calculate the Font height
	2. Calculate the font width
	3. Check the maximum length word in the tooltip string and determine the tooltip width
		a. if the word is more than the screen width, truncate word, 
		b. if the word is more than the default tooltip width but less than screen 
		width increase tooltip width
	4. Count number of lines depending on total characters on the tooltip width
	5. determine the table hight depending on number of lines
*/
//NBA118 new method
function widthCalculation(tipContent,tooltipWidth,fontSizeOrig){
	var heightToWidthRatio = HEIGHT_TO_WIDTH_RATIO;
	var pointToPixelRatio = POINT_TO_PIXEL_RATIO;
	// Tool tip width is in pixels
	//preserve the original width, this is the default width
	var tooltipWidthOrig = tooltipWidth;
	// font size is in points
	// font height
	var fontHeightInPixels = fontSizeOrig * pointToPixelRatio;
	// font width
	var fontWidthInPixels = fontHeightInPixels * heightToWidthRatio;
	var maxCharInOneline = tooltipWidth/fontWidthInPixels;
	// split the sentances in words
	var individualWords = tipContent.split(" "); 
	var newTipContent = "";
	// calculate the longest word and verify if that can fit in the current tooltipWidth
	for(i=0;i<individualWords.length;i++){
		var str = individualWords[i]; // get each word
		var numberOfCharactersInOneWord = str.length +1; // considering one blank space with the word
		if(numberOfCharactersInOneWord > maxCharInOneline){	// calculate the lenght of the word which is larger than the charactersinoneline .					
			tooltipWidth = (numberOfCharactersInOneWord *fontWidthInPixels)*110/100;  // so the new width will 110 % of the biggest word considering 10 % margine
			// checking if the word length is more than 60% screen width.. than truncate the word
			var displayWordWidth = document.body.offsetWidth*60/100; 
			if(tooltipWidth > displayWordWidth) {
				tooltipWidth = displayWordWidth*110/100; // new width is display word width + 110 %
				var newWordLength = parseInt(tooltipWidth /fontWidthInPixels); 
				var oldStr = str
				str = str.substring(0,newWordLength);
				if(oldStr != str) {
					str = str + "...";
				}
			}
			numberOfCharactersInOneWord = str.length +1 ;			
			maxCharInOneline = numberOfCharactersInOneWord;
		}// end of inner if
		newTipContent = newTipContent+str+" ";
	}
	// calculation for height
	var noOfWords = individualWords.length;
	var noOflines =1; // intializing number of lines to be 1
	var line ="";
	for(var i=0;i<noOfWords;i++){ // for each word
		var totalLength= parseInt(line.length)+parseInt(individualWords[i].length); // length of line adding word
		if(totalLength<=maxCharInOneline){ // the word can accomdate in same line
			line=line+individualWords[i]+" ";
		} else{	// the word cannot be accomodated in the same line	
			noOflines++; // increment number of lines
			line=""+individualWords[i]+" ";	// start new line	
		}
	}//end of for
	if(noOflines == 1) {
		// readjusting the width of tool tip if just one line
		if( (tooltipWidthOrig * 2 / 3) > ( newTipContent.length*fontWidthInPixels) ) {
			tooltipWidth = tooltipWidthOrig *2/3;
		}  
	} 
	noOflines = noOflines + HEIGHT_ADJUSTMENT_FACTOR ; // Applying adjustment factor to height
	// got number of lines
	var heightOfTable=parseFloat(fontHeightInPixels*noOflines); // height of the table
	// return height and width in pixels
	newIframeWidth=tooltipWidth+"px";
	newIframeHeight=heightOfTable.toString()+"px";
	return newTipContent;
}
/*
	Wrapper method to show tool tip	
	see activate()
*/
//NBA118 new method
function showToolTip() {	 
	activate();
}

/*
	Wrapper method to show tool tip
	see deActivate()
	see overTip()
*/
//NBA118 new method
function removeToolTip() {
	deActivate();
	overTip();
}