<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- ACN011           4      Support for Canadian provinces -->

//<script language="JavaScript">
function postal_onKeyPress(field)
{
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = postal_onKeyPress_NS;							
		return;	
	}
	
	var inputChar = window.event.keyCode;
	if(((inputChar < 48) || (inputChar > 57 && inputChar < 65) || (inputChar > 90 && inputChar < 97) || (inputChar > 122)))
		return window.event.returnValue = false;
	
	var strInput = field.value;
	if (strInput.length == 3)
		field.value += " ";
		
	if (strInput.length > 6)
		return window.event.returnValue = false;
}

function postal_onKeyPress_NS(e)
{
	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && ((inputChar < 48) || (inputChar > 57 && inputChar < 65) || (inputChar > 90 && inputChar < 97) || (inputChar > 122 )))
		return false;				
		
	var strInput = this.value;	
		
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if (strInput.length == 3)
			this.value += " ";		
		if (strInput.length > 6)
			return false;
	}	
	return true;

}

function postal_onChange(field)
{
	if(!postal_Validate(field))
		return;
	field.value = postal_Format(field.value);
	data_Changed(field);
}

function postal_Validate(field)
{
	var strInput = field.value;
	if(strInput.length == 0)
		return true;

	if	((strInput.indexOf(" ") == 3 && strInput.length != 7) ||
		 (strInput.length > 7 && strInput.indexOf(" ") != 3) || 
		 (strInput.length < 7))
	{
		raiseError("Invalid Postal Code!", field);		
		return false;
	}
	return true;		
}

function postal_Format(strInput)
{	
	if(strInput.length == 0)
		return strInput;
	
	if(strInput.indexOf(" ", 0) > -1) // aleady formated
		return strInput;

	if (strInput.length > 3)
		return strInput.substring(0, 3) + " " + strInput.substring(3);
	else
		return strInput;
}

function postal_UnFormat(strInput)
{
	aCurrStripChars = new Array(" ");
	
	if(strInput.length == 0)
		return strInput;
		
	return parseField(strInput, aCurrStripChars)	
}
