<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR1103           2      HTML backspace issue. -->
<!-- SPR1018           2      Cleanup. -->  
<!-- NBA025            2      Business Function Security -->  
<!-- NBA044            3      Architectural Changes -->
<!-- NBA061            3      Adopt Struts framework -->
<!-- SPR1317		   3      Disabled CTRL-R and CTRL-W --> 
<!-- SPR1851           4      Locking Issues -->
<!-- NBA077            4      Reissue and Complex Change -->
<!-- NBA213			   7	  Unified User Interface -->
<!-- SPRNBA-440     NB-1101   Failed to find resource nbalogon.jsp when a session timeout occurs on a non-JSF view -->

<SCRIPT LANGUAGE="JavaScript">
<%@ page import="com.csc.fsg.nba.foundation.NbaConstants,java.util.*" %>	//NBA025
<% if (session.getAttribute(com.csc.fsg.nba.foundation.NbaConstants.S_KEY_USER) == null) { %>
  //SPRNBA-440 code deleted
  window.location="sessionTimeout.html";  //SPRNBA-440
<% } %>
<%session.setAttribute(com.csc.fsg.nba.foundation.NbaConstants.S_KEY_INITIAL_LOAD, "true");%> <!-- SPR1851 -->				
//NBA025 begin
<%
    boolean disable_update = false;
	//NBA213 code deleted
%>
var disable_update = false  //NBA213
//NBA025 end
//begin NBA044
<% 
	boolean xTab = false; 
	String scroll = "yes";
	int top = 80; 	
%>
<% if(request.getParameter("X") == null) { %>		
	//NBA213 deleted
<%} else {	
	xTab = true; 
	scroll = "no"; 
	top = 0;
 }	
%>


//end NBA044
//IE only - automatic logoff
function logoff(){
	if (document.readyState =="complete" ){				
		window.open("authorization/logoff.jsp");  //NBA213
	}
}
//Returns true if view is open in read only
//NBA077 new method
function isViewOpenInReadOnly(){
	return disable_update;
}
//IE only - disable hot keys
//SPR1103 method changed
window.document.onkeydown = controlKeys;
var flag=false;//SPR1317
function controlKeys()
{		
	//begin SPR1103
	//alert(document.activeElement.type)
	if (event.keyCode == 122 || event.keyCode == 16 || event.keyCode == 8 ){					
		if (!((document.activeElement.type == "text" &&
				!document.activeElement.readOnly) || 
				document.activeElement.tagName == "TEXTAREA")) {
			event.keyCode = 0;			
			return false;
		}
	}
	//end SPR1103		
	
	if(event.altKey && (event.keyCode == 37 || event.keyCode == 39)){			
		return false;
	}						
	//begin SPR1317
	if(event.ctrlKey){
		flag=true;
	}
	if(flag){
		flag=false;
		if(event.keyCode==82 || event.keyCode==87){
			return false;
		}
	}
	//end SPR1317		
	
	if( event.keyCode == 116 ){
		event.keyCode = 0;
		return false;
	}	
}
//IE only -- disable right click
//window.document.oncontextmenu = suppressContextMenu;
function suppressContextMenu(){
	return false;
}
var doCleanup = true; //NBA044
//begin SPR1018
window.onbeforeunload =  destroySession;
function destroySession(){	
	//begin NBA044		
	//There will always be a form using the new menu, status bar framework
	if(doCleanup && getField("ACTION") && window.document.forms.length > 1){				
		var qs = "?ACTION=CLEANUP";
		qs +=  getField("NAVIGATION") ? "&NAVIGATION=" : "";
		//use the menu form to submit requests
		window.document.forms[1].action = "servlet/NbaFoundationServlet" + qs ;	
		window.document.forms[1].submit();
		doCleanup = false;		
	} else {
		doCleanup = true;		
	}
	//end NBA044 code deleted
}
//end SPR1018
</script>