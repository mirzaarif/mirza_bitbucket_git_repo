<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!--    NBA317      NB-1301   PCI Credit Card Enhancement  -->   

/*
 * PREREQUISITES
 * In order to use creditCard.js plugin in any jsp, following prerequisites must be satisfied- 
 * 
 * 1. Include 'javascript/global/file.js' plugin (creditCard.js utilizes filePageInit() method from file.js)
 * 2. Include 'javascript/global/jquery.js' plugin as many jquery features are utilized in the creditCard.js methods
 * 3. Provide an implementation of the authorizeCommit() method in the jsp to enable/disable state of commit/save/submit buttons
 */


/*************** List of constants for Standard Messages and Field types and lengths *****************/
var VALIDATIONPASS = 'VALIDATIONPASSED';
var DATA_INCOMPLETE = 'DATA_INCOMPLETE';
var DATA_ERASED = 'DATA_ERASED';
var DELAY = 500;

var AMEX_CARD_TYPE = '4';
var DINERS_CARD_TYPE = '5';
var AMEX_CARDNUMBER_LENGTH = 15;
var DINERS_CARDNUMBER_LENGTH = 14;
var OTHERS_CARDNUMBER_LENGTH = 16;

var TOKENIZATION_REQUEST_TYPE = "POST";
var TOKENIZATION_REQUEST_DATATYPE = "xml";
var TOKENIZATION_REQUEST_CONTENTTYPE =	"text/xml; charset=\"UTF-8\"";

var TOKENIZATION_SERVICE_ERROR = "Credit Card tokenization failed: ";
var INVALID_CREDIT_CARD_DATA = "Credit Card data is missing or invalid. Please correct.";
var ERROR_WEBSERVICE_UNAVAILABLE = 'Credit Card tokenization failed: Web service is unavailable.';
var ERROR_404 = 'Credit Card tokenization failed: Requested page not found. [404]';
var ERROR_500 = 'Credit Card tokenization failed: Internal Server Error [500].';
var ERROR_PARSING_FAILURE = 'Credit Card tokenization failed: Requested stream parse failed.';
var ERROR_TIMED_OUT = 'Credit Card tokenization failed: Request Timed out.';
var ERROR_REQUESTE_ABORTED = 'Credit Card tokenization failed: Tokenization request aborted.';
var ERROR_UNCAUGHT_EXCEPTION = 'Credit Card tokenization failed: Uncaught Web Service Error.';
var ERROR_PROCESSING_REQUEST = 'Unable to construct credit card tokenization request. Please correct the request template.';
var ERROR_PROCESSING_RESULT = 'Invalid response received from tokenization service.';
var PARSERERROR = 'parsererror';
var TIMEOUT = 'timeout';
var ABORT = 'abort';
var RESULT_UNDEFINED = 'undefined';

var TOKENIZATION_REQUEST_START_TAG = '<tokenizationRequest>';
var TOKENIZATION_REQUEST_END_TAG = '</tokenizationRequest>';
var REQUEST_TAG = 'requestTag';
var TAG_NAME = 'tagName';
var TAG_VALUE = 'tagValue';
var RESULT_TAG = 'resutlTag';

var TOKENIZATION_RESULT_CODE_TAG = "TOKENIZATION_RESULT_CODE_TAG";
var TOKENIZATION_RESULT_CODE_ATTRIBUTE = "TOKENIZATION_RESULT_CODE_ATTRIBUTE";
var TOKENIZATION_RESULT_SUCCESS_CODE = "TOKENIZATION_RESULT_SUCCESS_CODE"; // for base implementation anything less than 5 is successful result
var TOKENIZED_NUMBER_TAG = "TOKENIZED_NUMBER_TAG";

/******************************** Process Credit Card Tokenization *********************************/

//Performs the validations/tokenization operations over the passed numbers based on their card type
function processCreditCardTokenization(formID,subviewID,skipCCValidation,tokenizationURL,tokenizationRequest,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv){
	updateCCLabel(formID,subviewID,cardTypeID,cardLabelID);//call to update the hidden credit card label attribute
	
	var validationStatus = processCCValidation(formID,subviewID,isVerifyFieldAvailable,cardTypeID,cardNumberID,verifyCardNumberID,skipCCValidation);
	
	if(validationStatus == DATA_ERASED){
		// If the commit button is disabled and user clears the card number and verify card number field and the card type then the commit button should be enabled
		authorizeCommit(formID,subviewID,true);
	}else if(validationStatus == DATA_INCOMPLETE){
		authorizeCommit(formID,subviewID,false);//If there is data in either of these three fields, disable the save/commit buttons
	}else if(validationStatus == VALIDATIONPASS){
		showWait();
		setTimeout(
			function() {
				processCCTokenization(formID,subviewID,tokenizationURL,tokenizationRequest,isVerifyFieldAvailable,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv);
			}
		,DELAY);				
	}else{
		processErrorMessage(validationStatus,errorMessageDiv);
		authorizeCommit(formID,subviewID,false);//authorizeCommit needs to be defined by the respective jsps using this js plugin, there is no default implementation available for this method.
	}		
}

//Performs validation steps on the credit card number passed as per the card type selected.
function processCCValidation(formName,subviewID,isVerifyFieldAvailable,cardTypeID,cardNumberID,verifyCardNumberID,skipCCValidation){
	var cType = getValue(formName, subviewID,cardTypeID);
	var cNumber =  getValue(formName, subviewID,cardNumberID);
	var verifyNumber =  isVerifyFieldAvailable ? getValue(formName,subviewID,verifyCardNumberID) : cNumber;
	var validationMessage = INVALID_CREDIT_CARD_DATA;
	
	if(checkEmptyFields(cType,cNumber,verifyNumber,true)){
		validationMessage = DATA_ERASED; // data erased from all fields
	}else if(checkEmptyFields(cType,cNumber,verifyNumber,false)){
		validationMessage = DATA_INCOMPLETE; //mandatory fields are not provided
	}else if(checkCardNumberEquality(cNumber,verifyNumber) && performCardNumberLengthAndCheckDigitValidation(cType, cNumber,skipCCValidation)){
		validationMessage = VALIDATIONPASS; //Card Number and Verify Card Number fields do not match OR Check digit validation fails;
	}	
	return validationMessage;
}

//Performs the tokenization steps on the valid credit card number
function processCCTokenization(formID,subviewID,tokenizationURL,tokenizationRequest,isVerifyFieldAvailable,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv){
	var tokenizationStatus = tokenizeCreditCardNumber(formID,subviewID,tokenizationURL,tokenizationRequest);
	if(isCreditCardNumberTokenized(tokenizationStatus)){
		updatedCreditCardFields(formID,subviewID,tokenizationStatus,isVerifyFieldAvailable,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID);
		authorizeCommit(formID,subviewID,true);
	}else{
		processErrorMessage(tokenizationStatus,errorMessageDiv);
		authorizeCommit(formID,subviewID,false);
	}
	hideWait();
}

/********************************Credit Card Validation Operations*********************************/

//check if the card number and verify card number fields are equal
function checkCardNumberEquality(cNumber,verifyNumber){
	if(cNumber == verifyNumber){ 
		return true;
	}
	return false;
}

//check if the card number length is valid (Credit Card Number Rule) & Perform the check digit validation if skipCreditCardValidation = false
function performCardNumberLengthAndCheckDigitValidation(cType, cNumber,skipCreditCardValidation){
	if(skipCreditCardValidation == 'true'){
		return true;
	}else{
		if(isCardNumberLengthValid(cType, cNumber)){ 
			if(performCheckDigitValidation(cType, cNumber)){
				return true;
			}
		}
	}
	return false;		
}

// Check the card number length field 
function isCardNumberLengthValid(cType, cardNumber){
	if(cType == AMEX_CARD_TYPE){ //American Express
		if(cardNumber.length != AMEX_CARDNUMBER_LENGTH){
			return false;
		}
	}else if(cType == DINERS_CARD_TYPE){ //Diners Club
		if(cardNumber.length != DINERS_CARDNUMBER_LENGTH){
			return false;
		}
	}else if(cardNumber.length != OTHERS_CARDNUMBER_LENGTH){
		return false;
	}
	return true;
}

// Perform the credit card check digit validation 
function performCheckDigitValidation(cType, cardNumber){
	if(cType == AMEX_CARD_TYPE){
		return validateAmex(cardNumber);
	}else if(cType == DINERS_CARD_TYPE){
		return validateDiners(cardNumber);
	}else{
		return validateOthers(cardNumber);
	}
	return true;
}

// Validate Amex credit card numbers
function validateAmex(cardNumber){
	var checkDigit = getCheckDigit(cardNumber);
	var amexSOP = getAmexSumProduct(cardNumber);
	var roundedSOP = roundUp(amexSOP);
	if(checkDigit == (roundedSOP-amexSOP)){
		return true;
	}
	return false;
}

// This function computes the sum of products for American Express credit cards(15 digits). This algorithm starts with the 
// 2nd to the last digit of the card number, and moving right to left, alternately multiplies each successive digit by 1 and 2 respectively.
function getAmexSumProduct(cardNumber){
    var sum     = 0;
    var alt     = false;//14th digit should be multiplied with 1 and 13th with 2 and so on
    var i       = cardNumber.length-2;//second to the last digit
    var num;
    
    while (i >= 0){
        //get the next digit
        num = parseInt(cardNumber.charAt(i));
        //if it's an alternate number...
        if (alt) {
            num *= 2;
            if (num > 9){
                num = (num % 10) + 1;
            }
        } 
        //flip the alternate bit
        alt = !alt;
        //add to the rest of the sum
        sum += num;
        //go to next digit
        i--;
    }
    return sum;
}

// Validate Diners credit card numbers
function validateDiners(cardNumber){
	var checkDigit = getCheckDigit(cardNumber);
	var amexSOP = getDinersSumProduct(cardNumber);
	var roundedSOP = roundUp(amexSOP);
	if(checkDigit == (roundedSOP-amexSOP)){
		return true;
	}
	return false;
}

// This function computes the sum of products for Diners credit cards(14 digits). This algorithm starts with the 
// 2nd to the last digit of the card number, and moving right to left, alternately multiplies each successive digit by 2 and 1 respectively.
function getDinersSumProduct(cardNumber){
    var sum     = 0;
    var alt     = true;//13th digit should be multiplied with 2 and 12th with 1 and so on
    var i       = cardNumber.length-2;
    var num;
    
    while (i >= 0){
        //get the next digit
        num = parseInt(cardNumber.charAt(i));
        //if it's an alternate number...
        if (alt) {
            num *= 2;
            if (num > 9){
                num = (num % 10) + 1;
            }
        } 
        //flip the alternate bit
        alt = !alt;
        //add to the rest of the sum
        sum += num;
        //go to next digit
        i--;
    }
    return sum;
}

// Validate credit card numbers other than Amex/Diners
function validateOthers(cardNumber){
	var checkDigit = getCheckDigit(cardNumber);
	var amexSOP = getSumProduct(cardNumber);
	var roundedSOP = roundUp(amexSOP);
	if(checkDigit == (roundedSOP-amexSOP)){
		return true;
	}
	return false;
}

// This function computes the sum of products for all credit cards(16 digits) except Diners and American Express. This algorithm starts with the 
// 2nd to the last digit of the card number, and moving right to left, alternately multiplies each successive digit by 2 and 1 respectively.
function getSumProduct(cardNumber){
    var sum     = 0;
    var alt     = true;
    var i       = cardNumber.length-2;
    var num;
    
    while (i >= 0){
        //get the next digit
        num = parseInt(cardNumber.charAt(i));
        //if it's an alternate number...
        if (alt) {
            num *= 2;
            if (num > 9){
                num = (num % 10) + 1;
            }
        } 
        //flip the alternate bit
        alt = !alt;
        //add to the rest of the sum
        sum += num;
        //go to next digit
        i--;
    }
    return sum;
}

// Returns the last digit of the passed string
function getCheckDigit(cardNumber){
	 var lastDigitPosition  = cardNumber.length-1;
	 //get the last digit
     var checkDigit = parseInt(cardNumber.toString().charAt(lastDigitPosition));
	 return checkDigit;
}

/********************************Credit Card Tokenization Operations*********************************/

//method to tokenize the credit card number by making an ajax call to the web service
function tokenizeCreditCardNumber(formName,subviewID,tokenizationServiceURL,tokenizationServiceRequest){
	var tokenizationResult = TOKENIZATION_SERVICE_ERROR;	
	var tokenizationResultXML = sendTokenizationRequest(formName,subviewID,tokenizationServiceURL,tokenizationServiceRequest);
	if(!isEmpty(tokenizationResultXML)){
		tokenizationResult = parseTokenizationResult(tokenizationServiceRequest,tokenizationResultXML);
		if(isEmpty(tokenizationResult)){
			tokenizationResult = TOKENIZATION_SERVICE_ERROR;
		}				
	}
	return tokenizationResult;
}

//method that checks if the tokenization result is successful
function isCreditCardNumberTokenized(tokenizationResult){
	return tokenizationResult.substring(0, TOKENIZATION_SERVICE_ERROR.length) != TOKENIZATION_SERVICE_ERROR && 
			tokenizationResult != ERROR_PROCESSING_REQUEST && 
				tokenizationResult != ERROR_PROCESSING_RESULT;
}

//Tokenize the provided card number 
function sendTokenizationRequest(formName,subviewID,tokenizationServiceURL,tokenizationRequestTemplate){
   var tokenizationResult;
   var tokenizationRequest = processTokenizationRequest(formName,subviewID,tokenizationRequestTemplate); 
   if(tokenizationRequest == ERROR_PROCESSING_REQUEST){
   	  tokenizationResult = ERROR_PROCESSING_REQUEST;
   }else{
   		$.ajax({
    		    url: tokenizationServiceURL,
    		    type: TOKENIZATION_REQUEST_TYPE,
    		    dataType: TOKENIZATION_REQUEST_DATATYPE,
    		    data: tokenizationRequest,
    		    processData: false,
				async: false,
    		    contentType: TOKENIZATION_REQUEST_CONTENTTYPE,
				success: function(xml){
				    tokenizationResult = $(xml);
				},
    		    error: function (jqXHR, status, exception) {
					tokenizationResult = TOKENIZATION_SERVICE_ERROR;
					if (jqXHR.status === 0 || jqXHR.status == 500) {
						tokenizationResult  = ERROR_WEBSERVICE_UNAVAILABLE;
					} else if (jqXHR.status == 404) {
						tokenizationResult  = ERROR_404;
					} else if (exception === PARSERERROR) {
						tokenizationResult = ERROR_PARSING_FAILURE;
					} else if (exception === TIMEOUT) {
						tokenizationResult = ERROR_TIMED_OUT;
					} else if (exception === ABORT) {
						tokenizationResult = ERROR_REQUESTE_ABORTED;
					} else {
						tokenizationResult = ERROR_UNCAUGHT_EXCEPTION + "\n" + jqXHR.responseText;
					}
    			}
    		});
	}
	return tokenizationResult;
}

//Parse the result xml from web service,if parseError is true return the error message sent by webservice else parse the tokenized number 
function parseTokenizationResult(tokenizationServiceRequest,tokenizationResultXML){ 
	var parsedResult = ERROR_PROCESSING_RESULT;
	if(isTypeOfString(tokenizationResultXML) && (startsWith(tokenizationResultXML,TOKENIZATION_SERVICE_ERROR) || tokenizationResultXML == ERROR_PROCESSING_REQUEST)){
		parsedResult = tokenizationResultXML;
	}else{		
	    var tokenizationResultCodeTag = getTagValue(tokenizationServiceRequest,TOKENIZATION_RESULT_CODE_TAG);
		if(ERROR_PROCESSING_RESULT != tokenizationResultCodeTag){
			var tokenizationResultAttrTag = getTagValue(tokenizationServiceRequest,TOKENIZATION_RESULT_CODE_ATTRIBUTE);
			if(ERROR_PROCESSING_RESULT != tokenizationResultCodeTag){			   
				try{
					var resultCodeTC = $(tokenizationResultXML).find(tokenizationResultCodeTag).attr(tokenizationResultAttrTag);
					if(typeof resultCodeTC != RESULT_UNDEFINED){ // possible when either the resultcodetag or attribute name is not present in response xml
						var tokenizationSuccessful = resultCodeTC < getTagValue(tokenizationServiceRequest,TOKENIZATION_RESULT_SUCCESS_CODE);				
						if(tokenizationSuccessful){
							parsedResult =  $(tokenizationResultXML).find(getTagValue(tokenizationServiceRequest,TOKENIZED_NUMBER_TAG)).text();	
						}else{
							parsedResult = TOKENIZATION_SERVICE_ERROR + $(tokenizationResultXML).find(tokenizationResultCodeTag).text();
						}
					}
				}catch(err){
					parsedResult = ERROR_PROCESSING_RESULT;
				}
			}
		}
	}
	return parsedResult;
}

//Method to replace the place holders from the tokenization request with the actual values as presented in the UI
function processTokenizationRequest(formName,subviewID,tokenizationRequestTemplate){
	var request = ERROR_PROCESSING_REQUEST;
	var requestProcessed = false;
	var startSubString = tokenizationRequestTemplate.indexOf(TOKENIZATION_REQUEST_START_TAG) + TOKENIZATION_REQUEST_START_TAG.length;
	var endSubString = tokenizationRequestTemplate.indexOf(TOKENIZATION_REQUEST_END_TAG);
	request = tokenizationRequestTemplate.substring(startSubString , endSubString );	
	try{
		var xmlDoc = $.parseXML(tokenizationRequestTemplate);	
		$(xmlDoc ).find(REQUEST_TAG).each(function(){
			var tagName = $(this).find(TAG_NAME).text();
			var tagValueVariable = $(this).find(TAG_VALUE).text();
			var tagValue = getValue(formName,subviewID,tagValueVariable);
			request = request.replace(new RegExp(tagName, 'g'),tagValue);
		});
	}catch(error){
		request = ERROR_PROCESSING_REQUEST;
	}
	return request;
}

//Method that scans the passed input and determines if a matching tagName is available
function getTagValue(input, tagName){
	var tagValue = ERROR_PROCESSING_RESULT;
	try{
		var xmlDoc = $.parseXML( input );
		$(xmlDoc ).find(RESULT_TAG).each(function(){
			var templateTagName = $(this).find(TAG_NAME).text();
			if(templateTagName === tagName){
				tagValue = $(this).find(TAG_VALUE).text();
				return false; //breaks the jquery each() loop
			}	   
		});
	}catch(error){
		tagValue = ERROR_PROCESSING_RESULT;
	}	
	return tagValue;
}

/********************************Credit Card Format Operations*********************************/

//Method used across UI to format the credit card number with '*'
function formatCardNumber(formID,subviewID,field,hiddenField) {
	var originalValue = getValue(formID,subviewID, field);
	setValue(formID,subviewID,hiddenField,originalValue);		
	setValue(formID,subviewID,field,maskCreditCardNumber(originalValue,false));				
	return true;
}

//method to update the fields on the jsps with the tokenized card number information
function updatedCreditCardFields(formName,subviewID,tokenizedNumber,isVerifyFieldAvailable,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID){
	var maskedCardNumber = maskCreditCardNumber(tokenizedNumber,true);
	setValue(formName,subviewID,cardNumberID,tokenizedNumber);	
	setValue(formName,subviewID,viewCardNumberID,maskedCardNumber);	
	if(isVerifyFieldAvailable){
		setValue(formName,subviewID,verifyCardNumberID,tokenizedNumber);	
		setValue(formName,subviewID,viewVerifyCardNumberID,maskedCardNumber);
	}	
}

//check for empty card type or number field, returns false only when all the passed input fields contain some data
function checkEmptyFields(cType,cNumber,verifyNumber, checkDataErased){
	if(checkDataErased){
		return isEmpty(cType) && isEmpty(cNumber) && isEmpty(verifyNumber);
	}else{
		if(!isEmpty(cType) && !isEmpty(cNumber) && !isEmpty(verifyNumber)){ 
			return false;
		}
	}
	return true;
}

//Returns a string where all characters are replaced with *
function maskCreditCardNumber(input, maskAllDigits) {
	var str = "";
	if(maskAllDigits){
		for(var i = 0; i < input.length; i++){
			str+= String.fromCharCode(42);//ASCII value for *
		}
	}else{
		if(input.length > 4){
			var endValue = input.substring(input.length-4, input.length);
			var ast = input.substring(0, input.length-4);
			for(var i = 0; i < ast.length; i++){
				str+= String.fromCharCode(42);//ASCII value for *
			}
			str+=endValue;
		}
	}
	return str;
}

function processErrorMessage(errorMessage, divID) {
	if(!isEmpty(divID)){
		document.getElementById(divID).innerHTML = errorMessage;
		filePageInit();//this method is available in file.js plugin, the same should be imported in the jsps prior to importing creditCard.js
	}else{
		alert(errorMessage);
	}
}

/********************************Credit Card UI Operations*********************************/

//Method to check if the card number and verify card number fields are both empty
function isCardNumberAndVerifyFieldsEmpty(formID,subviewID,isVerifyFieldAvailable,cardNumberID,verifyCardNumberID){
	var cardNumber = getValue(formID,subviewID,cardNumberID);
	var verifyCardNumber =  isVerifyFieldAvailable ? getValue(formID,subviewID,verifyCardNumberID) : cardNumber;
	return isEmpty(cardNumber) && isEmpty(verifyCardNumber); 
}

// Updates the hidden cc label field on the input formID:subviewID:cardLabelID
function updateCCLabel(formID,subviewID,cardTypeID,cardLabelID){
	var ccTypeDD = getUIField(formID,subviewID,cardTypeID);
	if(ccTypeDD != null && ccTypeDD.selectedIndex > -1){
		var newValue = ccTypeDD.options[ccTypeDD.selectedIndex].text;
		setValue(formID,subviewID,cardLabelID,newValue);
	}
}

// Returns the field identified with the formName:subViewID:fieldID
function getUIField(formName,subviewID,fieldID){
	var field;
	if(isEmpty(formName)){
		field = isEmpty(subviewID) ? eval("window.document.forms[0]." +  fieldID) : eval("window.document.forms[0]."+ subviewID + "." + fieldID);	
	}else{
		if(isEmpty(subviewID)){
			field = document.getElementById(formName+':'+fieldID);
		}else{
				field = document.getElementById(formName+':'+subviewID+':'+fieldID);
				if(field == null){
					//search in parent jsp to see if the variable was defined at that level instead of a subview
					field = document.getElementById(formName+':'+fieldID);
				}
			}
		}
	return field;
}

//Returns the value of the field identified by fieldID attribute
function getValue(formName,subviewID,fieldID){
	var field = getUIField(formName,subviewID,fieldID);
	var fieldValue;
	if(field != null){
		fieldValue = field.value;
		if(fieldValue == 'true'){
			fieldValue = true;
		}else if(fieldValue == 'false'){
			fieldValue = false;
		}
	}
	return fieldValue;
}

//Sets the passed newValue to the field identified by the fieldID attribute
function setValue(formName,subviewID,fieldID,newValue){
	var field = getUIField(formName,subviewID,fieldID);
	if(field != null){
		field.value = newValue;
	}
}

/********************************Util Operations*********************************/

// Checks if the passed argument is of the String type 
function isTypeOfString(input){
	return typeof(input) == 'string';
}

//Utility method to check if the string starts with the passed prefix
function startsWith(input,prefix){
	return input.indexOf(prefix) === 0;
}

//Returns the next multiple of 10 for the passed integer
function roundUp(intgr){
	return Math.ceil(intgr / 10) * 10;
}

//Check if the string passed is empty
function isEmpty(s) {
	return ((s == null) || (s.length == 0) || (s == '-1'));
}

//Shows the wait cursor
function showWait(){
	top.showTimedWait('processing request',DELAY);
	window.document.forms[0].style.cursor = "wait";	
}

//Hides the wait cursor
function hideWait(){
	top.hideWait();
	window.document.forms[0].style.cursor = "auto";
}