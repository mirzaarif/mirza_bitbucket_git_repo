<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA024            2      Conversion of applet based views to HTML -->
<!-- NBA031            3      Rewrite logon/menu/status bar in HTML -->

<SCRIPT LANGUAGE="JavaScript">
window.onload = queryStringToFields;

function queryStringToFields(){
	var queryString;
	var index = 0;
	var name = ""; //NBA031
	//convert the query string to an array		
	queryString = dialogArguments[0] == "?" ? "" : dialogArguments[0].substr(1).split("&");	
	
	for (var i = 0 ; i < queryString.length; i++){
		index = queryString[i].indexOf("=");			
		name = queryString[i].substr( 0, index); //NBA031
		if(getField(name)){ //NBA031 Could be a FORM field
			getField(name).value = queryString[i].substr( index + 1);	
		} else if(document.getElementById(name)) { //NBA031 A DOM element 
			document.getElementById(name).innerHTML = queryString[i].substr( index + 1); //NBA031			
		} //NBA031	
	}	
}

window.onunload = createReturnValues;

function createReturnValues() {		
	var returnFields;
	var objReturn = new Object();	  
	var isLoaded = false;
			
	if (document.readyState == "complete"){		
		isLoaded = true;
	}	
		  
	if (dialogArguments){
		var evaluator = "";
		returnFields = dialogArguments[1].split(",");
		
		for (var i = 0; i < returnFields.length; i++){				
					
			if (returnFields[i]){			
				if (isLoaded){				
					evaluator = "objReturn." + returnFields[i] + " = getField('" + returnFields[i] + "').value";       
				}else{		
		 			evaluator = "objReturn." + returnFields[i] + " = '' ";
				}				
				eval(evaluator);      
			}
		}
	}	
	window.returnValue = objReturn;
	return true;
}
</SCRIPT>