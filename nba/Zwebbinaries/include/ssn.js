//<script language="JavaScript">
//function for IE, we assume default browser as IE
function ssn_onKeyPress(field)
{	
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = ssn_onKeyPress_NS;							
		return;	
	}
	
	//Retrieve keycode, allow only numbers
	var inputChar = window.event.keyCode;
	if(inputChar < 48 || inputChar > 57)
		return window.event.returnValue = false;		
	
	var strInput = field.value;
	
	//format the field	
	if (strInput.length == 3 || strInput.length == 6)
		field.value += "-";			
	if (strInput.length > 10)
		return window.event.returnValue = false;

	return window.event.returnValue = true;
}

//function for netscape
function ssn_onKeyPress_NS(e) {
	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 ))
		return false;		
		
	var strInput = this.value;	
	
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if (strInput.length == 3 || strInput.length == 6)
			this.value += "-";					
		if (strInput.length > 10)
			return false;
	}		
	return true;
}       

function ssn_onChange(field)
{				
	if(!ssn_Validate(field))
		return;
	field.value = ssn_Format(field.value);
	data_Changed(field);	
}

function ssn_Validate(field)
{
	var strInput = field.value;

	if(strInput.length == 0)
		return true;

	if( strInput.indexOf("-") != 3 ||
		strInput.lastIndexOf("-") != 6 || 
		strInput.length < 11)
		{	
			raiseError("Invalid Social Security Number!",field);	
			return false;
		}
	field.valid = strInput ;
	field.style.color = '';
	return true;	
}

function ssn_Format(strInput)
{
	if(strInput.length == 0)
		return strInput;
	
	if(strInput.indexOf("-", 0) > -1) // aleady formated
		return strInput;
	
	return strInput.substring(0, 3) + "-" + strInput.substring(3, 5) + "-" + strInput.substring(5);
}

function ssn_UnFormat(strInput)
{
	aCurrStripChars = new Array("-");
	
	if(strInput.length == 0)
		return strInput;

	return parseField(strInput, aCurrStripChars)	
}