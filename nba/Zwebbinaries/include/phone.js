//<script language="JavaScript">
function phone_onKeyPress(field)
{  
	if (navigator.appName == 'Netscape'){ 		
		field.onkeypress = phone_onKeyPress_NS;							
		return;	
	}
	
	var inputChar = window.event.keyCode;
	if(inputChar < 48 || inputChar > 57)
		return window.event.returnValue = false;
		
	var strInput = field.value;	
	
	if (strInput.length ==1 && strInput !="(")
		field.value = "(" + strInput;
	if (strInput.length == 4)
		field.value += ")";
	if (strInput.length == 8)
		field.value += "-";	
	
	if (strInput.length > 12)
		return window.event.returnValue = false;
}

function phone_onKeyPress_NS(e)
{  
	//Get the keycode
	var inputChar = e.which;
	
	//allow only numbers, or backpsace/delete/tab etc to be entered in this field 
	if ((!(inputChar == 0 || inputChar == 8)) && (inputChar < 48 || inputChar > 57 ))
		return false;		
		
	var strInput = this.value;	
	
	//format only if keystroke was a number
	if (!(inputChar == 0 || inputChar == 8)){
		if (strInput.length ==1 && strInput !="(")
			this.value = "(" + strInput;
		if (strInput.length == 4)
			this.value += ")";
		if (strInput.length == 8)
			this.value += "-";		
		if (strInput.length > 12)
			return false;
	}	
	return true;
}

function phone_onChange(field)
{	
	//field.value = Phone_Format(field.value);	
	if(!phone_Validate(field))
		return;
	field.value = phone_Format(field.value);
	data_Changed(field);
}

function phone_Validate(field)
{
	var strInput = field.value;
	if(strInput.length == 0)
		return true;
	
	if( strInput.indexOf("(") != 0 || 
		strInput.indexOf(")") != 4 || 
		strInput.indexOf("-") != 8 ||
		strInput.length != 13)
	{
		raiseError("Invalid Phone Number!", field);	
		return false;
	}
	field.valid = strInput;
	return true;
}

function phone_Format(strInput)
{
	if(strInput.length == 0)
		return strInput;
	
	if(strInput.indexOf("-", 0) > -1 && 
		strInput.indexOf("(") > -1 && 
		strInput.indexOf(")") > -1)
		return strInput;
			
	return "(" + strInput.substring(0, 3) + ")" + strInput.substring(3, 6) + "-" + strInput.substring(6); 		
}

function phone_UnFormat(strInput)
{
	aCurrStripChars = new Array("(", ")", "-")
	
	if(strInput.length == 0)
		return strInput;

	return parseField(strInput, aCurrStripChars)	
}