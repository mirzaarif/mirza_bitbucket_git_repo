<!-- CHANGE LOG -->
<!-- Audit Number   Version     Change Description -->
<!-- NBA340         NB-1501     Mask Government ID -->
<!--SPRNBA-982		NB-1601		Joint Annuitant Government ID Click Event Error and Not Masked -->

(function($) {	//method to initialize variables on page load.
	$.govtID = {
			SSN: {
				type: "1",
				pattern: "###-##-####",
				validateMessage: "Invalid SSN format specified {value} for field govtIdInput,should be in the form of '###-##-####'"
			},
			TIN: {
				type: "2",
				pattern: "##-#######",
				validateMessage: "Invalid TIN format specified {value} for field govtIdInput,should be in the form of '##-#######'"
			},
			SIN: {
				type: "3",
				pattern: "###-###-###",
				validateMessage: "Invalid SIN format specified {value} for field govtIdInput,should be in the form of '###-###-###'"
			},
			
			maskURL:"/rest/maskservice/govtId",
			regex: /^[0-9]{9}$/,
			emptyStyleClass: "textPattern",
			notifyOnError: true
	}

	var supportedFormats = [$.govtID.SSN, $.govtID.SIN, $.govtID.TIN];
	$.fn.extend({ 
		//method to handle events related to govtIdType radio button.
		govtidType: function() {
			return this.each(function() {
				if ($(this).attr("checked")) {
					var format = getFormat($(this).val());
					var input = $($(this).getInputID());
					input.data("format", format);
				}
				//method to handle click event of govtIdType radio button.
				$(this).click(function() {
					try {
						var format = getFormat($(this).val());
						var input = $($(this).getInputID());
						var inputFormat = input.data("format");
						if (format != null) {
							if (inputFormat == null || inputFormat.type != format.type) {
								var inputVal = input.val();
								if (inputVal == null || inputVal == '' || inputVal == inputFormat.pattern) {
									input.val(format.pattern);
								}else{
								    input.val($(this).formatGovtID());
								}
								input.data("format", format);
							}
						} else {	
							input.removeData("format");
						}
						input.removeAttr("disabled");
						input.focus();
					} catch (err) {
						if ($.govtID.notifyOnError) {
							alert("Click event failed: " + err.message);
						}
					}
				})
                //method to return pattern to be displayed on inputText based on govtIdType value.				
				function getFormat(type) {
					for (i=0; i<supportedFormats.length; i++) {
						if (type == supportedFormats[i].type) {
							return supportedFormats[i];
						}
					}
					return null;
				}				    
			})
			
		},
		//method to handle events related to govtIdValue inputText field.
		govtidValue: function() {
			$(this).data("styleClass", $(this).attr("class"));
			this.each(function() {  // SPRNBA-982
				if ($(this).data("format") == null && !$(this).isSearchView()) {
					$(this).attr("disabled", "true");
				}
			}); // SPRNBA-982
            //method to handle event when user leaves govtIdValue inputText field.
			$(this).blur(function() {
				var format = $(this).data("format");
				if(format!=undefined){
				   if ($(this).val() == null || $(this).val() == '' || $(this).val() == format.pattern) {
					   $(this).val(format.pattern);
					   $(this).attr("class", $.govtID.emptyStyleClass);
				   } else {
					   $(this).attr("class", $(this).data("styleClass"));
				   }
				}
			});
			//method to handle event when value in the govtIdValue inputText field is changed.
			$(this).change(function() {
			    var key = $($(this).getKeyID($(this).attr("id"),5));
				var format = $(this).data("format");
				key.val("");
				if(format!=undefined){
				   if ($(this).val() == null || $(this).val() == '' || $(this).val() == format.pattern) {
					   $(this).val(format.pattern);
					   $(this).attr("class", $.govtID.emptyStyleClass);									  
                       $(this).removeData("invalid");					  
					   return;
				   }	
                }	
                				
				if ($(this).validateGovtID()) {
					 $(this).maskGovtID();
				}				
			});
			//method to handle event when user clicks on govtIdValue inputText field.
			$(this).focus(function() {
				var format = $(this).data("format");
				if(format!=undefined){
				   if ($(this).val() == null || $(this).val() == '' || $(this).val() == format.pattern) {
					$(this).attr("class", $(this).data("styleClass"));
				   }
				}
				$(this).select();
			});
			
		},
		
		//method to handle masking of govtIdValue.
		maskGovtID: function() {
			var input = $(this);
			var key = $($(this).getKeyID($(this).attr("id"),5));			 
            key.val(""); 
            var govtIdType = "0";			
            if(!$(this).isSearchView()){
            	 govtIdType = $(this).data("format").type;
            }				
			$.ajax({
					url: contextpath + $.govtID.maskURL + "/" + $(this).val() + "/" + govtIdType,
					context: document.body,
					type: "POST",
					dataType: "text",
					success: function(data) {
						var arr = data.split("~");
						input.val(arr[0]);
						key.val(arr[1]);                       
					},
					error: function (jqXHR, status, exception) {
						if (jqXHR.status === 0 || jqXHR.status == 500) {
							$("#Messages").html("[" + jqXHR.status + "] " + jqXHR.responseText);
						} else if (jqXHR.status == 404) {
							$("#Messages").html("[" + jqXHR.status + "] " + jqXHR.responseText);
						} else {
							$("#Messages").html(jqXHR.responseText);
						}
						filePageInit();
					}		
			}); 		
			
		},
		
		//method to validate value entered in govtId inputText field.
		validateGovtID: function() {           
			var regEx = $.govtID.regex;			
			if (!regEx.test($(this).val())) {	
                if(!$(this).isSearchView()){                             
					$("#Messages").html($(this).formatValidationMessage());				  
				    filePageInit();	
                }				
				return false;
			}
			else{
			    $(this).removeData("invalid");
			}
			return true;
		},
		
		//method to format validation message to be displayed when invalid govIdValue is entered.
		formatValidationMessage: function() {
			var format = $(this).data("format");
			var message = format.validateMessage;
			message = message.replace("{value}", $(this).val());
			message = message.replace("{id}", $(this).attr("id"));
			$(this).data("invalid", message);
			return message;
		},
		
		//method to get ID of hidden field storing govtIdkey.
		getKeyID : function(myID,prefixLength){		
			myID = myID.substring(0, myID.length-prefixLength);  //remove 'Input'
			if(myID.substring(myID.length-4,myID.length) == 'Corp'){
				myID = myID.substring(0, myID.length-4); // remove 'Corp'
			}
			var idNodes = myID.split(":");
			var keyID = "#";
			for (i=0; i<idNodes.length; i++) {
				if (i > 0) {
					keyID += "\\:";
				}
				keyID += idNodes[i];
			}
			keyID += "Key";
			return keyID;
		},
		
		// method to get ID of govtIdValue inputText field.
		getInputID : function() {
		    myID = $(this).attr("name");
			myID = myID.substring(0, myID.length-4);  //remove 'Type'
			if(myID.substring(myID.length-4,myID.length) == 'Corp'){
		        myID = myID.substring(0, myID.length-4); // remove 'Corp'
		    }
			var idNodes = myID.split(":");
			var inputID = "#";
			for (i=0; i<idNodes.length; i++) {
				if (i > 0) {
					inputID += "\\:";
				}
				inputID += idNodes[i];
			}
			inputID += "Input";
			return inputID;
		},
		
		//method to format government ID value when government ID type is changed.		
		formatGovtID : function(){
		    var govtIdTypeField = $(this).attr("name");
		    var key =$($(this).getKeyID(govtIdTypeField,4));
		    var inputVal = $($(this).getInputID()).val();
		    if(key.val()!= ""){		   
		        inputVal=inputVal.replace(/-/g,"");
		        var govtIdType = $(this).val();
		        if(govtIdType == $.govtID.SSN.type){		   
			        return inputVal.substring(0,3) +'-'+inputVal.substring(3,5)+'-'+ inputVal.substring(5,9);
		        }
		        if(govtIdType == $.govtID.SIN.type){
		            return inputVal.substring(0,3) +'-'+inputVal.substring(3,6)+'-'+ inputVal.substring(6,9);		
		        }
		        if(govtIdType == $.govtID.TIN.type){
		            return inputVal.substring(0,2) +'-'+inputVal.substring(2,9);
		        }		   
		    }
		    return inputVal;
	    },
		
	    //method to check for the error messages related to government Id field.		
	    checkErrorMessage : function(){
		var errorMessages = "";
			$("input[name$='govtIdInput']").each(function() {
				var errMsg = $(this).data("invalid");
				if (errMsg != null) {
					errorMessages = errorMessages + errMsg + "\n";
				}
			})
			if (errorMessages.length > 0) {			    
				$("#Messages").html(errorMessages);
				filePageInit();
				return false;
			}
			return true;
		},   
	    
	    isSearchView : function(){
	    	var isSearchView = ($(this).attr("id")).indexOf("Criteria") >= 0 || ($(this).attr("id")).indexOf("Search") >= 0?true:false;
	    	return isSearchView;
	    }
		
	});
}) (jQuery);
