<%-- CHANGE LOG
	Audit Number	Version		Change Description
	NBA180           7         	Contract Copy Rewrite
	NBA187           7         	Trial Application
	NBA139           7         	Plan Code Determination
	NBA213			 7      	Unified User Interface
	SPR3474		  	 8      	Contract Number Entry Field is Editable When Automatic Radio Button is Turned On
	SPR3475		  	 8      	Plan Drop Down Not Re-Set on Refresh
	FNB013 			NB-1101	    DI Support for nbA
	SPRNBA-798      NB-1401   	Change JSTL Specification Level
	SPRNBA-334      NB-1601   	Unable to Add Comments on Contract Copy 
--%>

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
	String basePath = "";
    if (request.getServerPort() == 80) {
    	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
 	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Copy</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- SPRNBA-334 --%>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			document.forms['form_contractCopy'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractCopy'].target='';
			return false;
		}
		//Enable the validate button when a plan is selected, as long as the commit button is enabled.
		//If the commit button is disabled, then we have a trial application and neither buttons should be enabled.
		//NBA187 New Method
		function enableValidateOnPlan() {
			if (document.getElementById('form_contractCopy:btnCommit').disabled == true) {
				document.getElementById('form_contractCopy:btnValidate').disabled = true;
			} else if (document.getElementById('form_contractCopy:planDD').value == "-1") {
				document.getElementById('form_contractCopy:btnValidate').disabled = true;
			} else {
				document.getElementById('form_contractCopy:btnValidate').disabled = false;
			}
		}

		//Perform initialization of the page
		function initPage() {
			filePageInit();
			enableValidateOnPlan();
			toggleContractText() //SPR3474

			if (document.getElementById('form_contractCopy:nbaValidationMessages:contractData') != null) {
				if (document.getElementById('form_contractCopy:genericPlanFieldsPGroup') != null) {
					document.getElementById('form_contractCopy:nbaValidationMessages:contractData').style.height = 335;
				} else {
					document.getElementById('form_contractCopy:nbaValidationMessages:contractData').style.height = 370;
				}
			}
		}
		//Disables the contractnumber textfield if automatic radiobutton is turned ON and vice versa
		//SPR3474 New Method
		function toggleContractText() {			
			if(document.getElementById("form_contractCopy").elements("form_contractCopy:contractNumberOption")[0].checked){
				document.getElementById("form_contractCopy:contractNumberText").value = '';
				document.getElementById("form_contractCopy:contractNumberText").disabled = true;
			} else {
				document.getElementById("form_contractCopy:contractNumberText").disabled = false;
			}
		}
</script>
</head>
<%-- NBA213 Common Tab Style --%>
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden; overflow-y: scroll" >
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<PopulateBean:Load serviceName="RETRIEVE_CONTRACT_COPY" value="#{pc_contractCopy}" />
		<h:form id="form_contractCopy"> 
			<div class="inputFormMat">
				<div class="inputForm">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputText id="contractCopyTitle" value="#{property.copyTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryTopLine">
						<h:selectBooleanCheckbox value="#{pc_contractCopy.openCase}" styleClass="formEntryCheckbox" style="margin-left: 125px;" />
						<h:outputText id="openNewCaseSpacer" value="#{property.copyOpenNew}" styleClass="formLabelRight" />
					</h:panelGroup>
					<h:panelGrid columns="2" styleClass="formDataEntryLine" columnClasses="formLabelTable,formEntryTextFull"
									style="width: 605px" cellspacing="0" cellpadding="0">
						<h:column>
							<h:outputText value="#{property.copyType}" styleClass="formLabel" style="margin-top: 7px" />
						</h:column>
						<h:column>
							<h:selectOneRadio value="#{pc_contractCopy.type}" layout="pageDirection" styleClass="formEntryTextFull"
											style="margin-left: -3px">
								<f:selectItems value="#{pc_contractCopy.types}" />
							</h:selectOneRadio>
						</h:column>
					</h:panelGrid>
					<h:panelGrid columns="3" styleClass="formDataEntryLine" columnClasses="formLabelTable,formLabel,formEntryTextFull"
									style="width: 605px" cellspacing="0" cellpadding="0">
						<h:column>
							<h:outputText value="#{property.copyContractNumber}" styleClass="formLabel" style="margin-top: 7px" />
						</h:column>
						<h:column>
							<h:selectOneRadio id="contractNumberOption" value="#{pc_contractCopy.contractNumberOption}" layout="pageDirection" styleClass="formEntryText"
											style="width: 125px; margin-left: -5px" onchange="toggleContractText();"><%-- SPR3474 --%>
								<f:selectItems value="#{pc_contractCopy.contractNumberList}" />
							</h:selectOneRadio>
						</h:column>
						<h:column>
							<h:inputText id="contractNumberText" value="#{pc_contractCopy.contractNumber}" styleClass="formEntryTextHalf" style="margin-top: 30px"><%-- SPR3474 --%>
							</h:inputText>
						</h:column>
					</h:panelGrid>
					<h:panelGroup styleClass="formDataEntryLine" style="margin-top: 10px; padding-bottom: 10px"><%-- NBA139 --%>
						<h:outputText value="#{property.copyPlan}" styleClass="formLabel" />
						<h:selectOneMenu id="planDD" value="#{pc_contractCopy.plan}" styleClass="formEntryTextFull"
													 onchange="enableValidateOnPlan();submit();" valueChangeListener="#{pc_contractCopy.planValueChange}"><%-- NBA187 FNB013--%>
							<f:selectItems value="#{pc_contractCopy.planList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					
					<%-- begin FNB013--%>
					<h:panelGroup id="benefitPeriodAccGroup" rendered="#{pc_contractCopy.di && pc_contractCopy.genericPlanInd}" styleClass="formDataEntryLine">
							<h:outputText id="AccidentBenefitPeriod" value="#{property.appEntBenefitPeriodAcc}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="benefitPeriodAcc" value ="#{pc_contractCopy.benefitPeriodAcc}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="benefitPeriodAccList" value="#{pc_contractCopy.benefitPeriodAccList}" />
							</h:selectOneMenu>
				        </h:panelGroup>
						<h:panelGroup id="benefitPeriodSickGroup" rendered="#{pc_contractCopy.di && pc_contractCopy.genericPlanInd}" styleClass="formDataEntryLine">
							<h:outputText id="SicknessBenefitPeriod" value="#{property.appEntBenefitPeriodSick}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="benefitPeriodSick" value ="#{pc_contractCopy.benefitPeriodSick}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="benefitPeriodSickList" value="#{pc_contractCopy.benefitPeriodSickList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="waitingPeriodAccGroup" rendered="#{pc_contractCopy.di && pc_contractCopy.genericPlanInd}" styleClass="formDataEntryLine">
							<h:outputText id="AccidentWaitingPeriod" value="#{property.appEntWaitingPeriodAcc}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="waitingPeriodAcc" value ="#{pc_contractCopy.waitingPeriodAcc}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="waitingPeriodAccList" value="#{pc_contractCopy.waitingPeriodAccList}" />
							</h:selectOneMenu>
					    </h:panelGroup>
						<h:panelGroup id="waitingPeriodSickGroup" rendered="#{pc_contractCopy.di && pc_contractCopy.genericPlanInd}" styleClass="formDataEntryLine">
							<h:outputText id="SicknessWaitingPeriod" value="#{property.appEntWaitingPeriodSick}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="waitingPeriodSick" value ="#{pc_contractCopy.waitingPeriodSick}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="waitingPeriodSickList" value="#{pc_contractCopy.waitingPeriodSickList}" />
							</h:selectOneMenu>
					 </h:panelGroup>
					<%-- end FNB013--%>	
					<f:subview id="nbaValidationMessages" rendered="#{pc_contractCopy.showMessages}">
						<c:import url="/common/subviews/NbaValidationMessages.jsp" />
					</f:subview>
				</div>
			</div>
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />
			</f:subview>
			<h:panelGroup styleClass="tabButtonBar">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft" action="#{pc_contractCopy.refresh}" /><%-- SPR3475 --%>
				<h:commandButton id="btnValidate" value="#{property.buttonValidate}" styleClass="tabButtonRight-1" action="#{pc_contractCopy.validate}" />
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" action="#{pc_contractCopy.commit}"
					disabled="#{pc_contractCopy.auth.enablement['Commit'] || 
								pc_contractCopy.notLocked ||
								pc_contractCopy.commitDisabled}" 
					onclick="setTargetFrame()" />  <%-- NBA187 NBA213 --%>
			</h:panelGroup>
		</h:form>
		<div id="Messages" style="display:none"><h:messages /></div>
	</f:view>
</body>
</html>
