<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA251           8       nbA Case Manager and Companion Case Assignment -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>	
		<head>
			<base href="<%=basePath%>">
			<f:loadBundle basename="properties.nbaApplicationData" var="property" />
			<title><h:outputText value="#{property.reassignQueuesTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
				var width=550; 
				var height=220; 
				function setTargetFrame() {
					document.forms['form_queues'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_queues'].target='';
					return false;
				}
				
			</script>

		</head>
		<body onload="popupInit();"> 
			<PopulateBean:Load serviceName="RETRIEVE_REASSIGN_QUEUES" value="#{pc_NbaReassignQueues}" />
			
			<h:form id="form_queues">
				<h:panelGroup id="reassignUWQView" styleClass="formDataEntryTopLine" style="height: 30px"> 
					<h:outputLabel id="uwQueue" value="#{property.underwriterQueue}" styleClass="formLabel" style="width: 160px" />
					<h:selectOneMenu id="uwQueueList" styleClass="formEntryTextFull" style="width: 325px" onchange="resetTargetFrame();submit();"
						value="#{pc_NbaReassignQueues.underwriterQueue}">
						<f:selectItems value="#{pc_NbaReassignQueues.newUnderwriterList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup id="reassignCMQView" styleClass="formDataEntryTopLine" style="height: 30px"> 
					<h:outputLabel id="cmQueue" value="#{property.caseManagerQueue}" styleClass="formLabel" style="width: 160px" />
					<h:selectOneMenu id="cmQueueList" styleClass="formEntryTextFull" style="width: 325px" onchange="resetTargetFrame();submit();"
						value="#{pc_NbaReassignQueues.casemanagerQueue}">
						<f:selectItems value="#{pc_NbaReassignQueues.newCaseManagerList}" />
					</h:selectOneMenu>
				</h:panelGroup>	
				<h:panelGroup id="companionCase" styleClass="formDataEntryLine" style="height: 100px">
					<h:selectBooleanCheckbox id="companionCaseId" value="#{pc_NbaReassignQueues.applyToAllCompanionCases}" styleClass="ovFullCellSelectCheckBox" style="margin-left: 20px;width: 15px;"  
					 onchange="resetTargetFrame();submit();" disabled="#{! pc_NbaReassignQueues.companionCaseFlag}"
					/>
					<h:outputText id="delTobaccoInformatio" value="#{property.applyToAllCompanionCase}" style="position: relative;left: 10px;width: 200px;text-align: left;" styleClass="formLabel"/>
				</h:panelGroup>						
				<h:panelGroup styleClass="buttonBar" style="padding-top: 0px;">
					<h:commandButton id="btncancel" value="#{property.buttonCancel}" styleClass="buttonLeft"  
						action="#{pc_NbaReassignQueues.cancel}" onclick="setTargetFrame()" /> 
					<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="buttonRight"
						disabled="#{pc_NbaReassignQueues.auth.enablement['Commit'] || 
									pc_NbaReassignQueues.notLocked ||
									pc_NbaReassignQueues.issued}"
						action="#{pc_NbaReassignQueues.actionCommit}" onclick="setTargetFrame();" /> 
				</h:panelGroup>
			</h:form>
			
			<div id="Messages" style="display:none">
				<h:messages />
			</div>
		</body>
	</f:view> 
</html>
