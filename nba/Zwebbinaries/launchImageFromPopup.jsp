<%-- CHANGE LOG
Audit Number	Version   Change Description
NBA389			NB-1601	  Contract Print Manual Form Selection Enhancement --%>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String query = request.getQueryString();
boolean closeWindow = (query != null && query.indexOf("close=true") >= 0);
boolean closeAllAndOpen = (query != null && query.indexOf("closeAllAndOpen=true") >= 0);
boolean activate = (query != null && query.indexOf("activate=true") >= 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Images</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		function process(){
			if(top.lastButtonClicked != null){
				try{
					top.lastButtonClicked.isClicked="false";
				}catch(err){
				}
			}
			var data = document.all.ImageDefinition.innerHTML;
			data = data.replace(/(&lt;)/g,'<');
			data = data.replace(/(&gt;)/g,'>');
			if ('<%=closeWindow%>' == 'true'){
				top.myOpener.top.ImgViewer.closeImages(data);
			} else if ('<%=closeAllAndOpen%>' == 'true'){
				top.myOpener.top.ImgViewer.reopenImages(data);
			}else if ('<%=activate%>' == 'true'){
				top.myOpener.top.ImgViewer.activateImages(data); 
			}else {
				top.myOpener.top.ImgViewer.showImages(data);
			}			
		}
	</script>
</head>
<body onload="process();"> 
<f:view>
	<h:outputText id="ImageDefinition" 
		value="#{pc_Desktop.ids['IMAGEATTRIB']}" style="display:none;"></h:outputText>
</f:view>
</body>
</html>
