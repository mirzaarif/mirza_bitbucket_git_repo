<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      NbaUnderwriter Workbench Rewrite -->
<!-- NBA212            7      Content Services -->

		var appBasePath = "";
		var context = "/nba/";
		if(window.document.location.port != ""){
			appBasePath = window.document.location.protocol + "//" + window.document.location.hostname +":" + window.document.location.port + context;
		} else {
			appBasePath = window.document.location.protocol + "//" + window.document.location.hostname + context;
		}

		function getWork(){			
			parent.nbaHiddenForm.MENU_ACTION.value="GetWork";
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
			parent.nbaHiddenForm.submit();
		}
		
		function closeCase(promptWarning){
			var cont = true;
			if(promptWarning){
			
				var path = appBasePath + "nbamessageprompt.jsp";				
				cont = openDialog(path,400,50,"result","message=The current work item will be closed. Do you want to continue ?").result == "true"; // SPR1221 Change "case" to "work item"
			}			
			if(cont){
				parent.nbaHiddenForm.MENU_ACTION.value="Close";
				parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
				parent.nbaHiddenForm.target = "controlFrame";				
				parent.nbaHiddenForm.submit();
			}		
		}
		
		function menuCallBack(e){
			if(e.length > 0){ 
				alert(e);
				window.document.body.style.cursor = "auto";
			}
		}			

		function viewSource(){
			parent.nbaHiddenForm.MENU_ACTION.value="ViewSource";
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
			parent.nbaHiddenForm.target = "controlFrame";
			parent.nbaHiddenForm.submit();
		}
		
		function viewAllSource(){
			parent.nbaHiddenForm.MENU_ACTION.value="ViewAllSource";
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
			parent.nbaHiddenForm.target = "controlFrame";
			parent.nbaHiddenForm.submit();
		}
		
		
		function imageCallBack(sourceID, reqFlag){ //NBA212
			init();
			//NBA212 code deleted
			var width = window.screen.availWidth/2; 
			var height = window.screen.availHeight; 
			if(sourceID.length > 0){ 
				//NBA212
				try {
				    top.ImgViewer.showImages(sourceID); //since 640 screen size is standard going forward we open the image starting 641
				} catch(e) {}
				//NBA212
			}
			window.document.body.style.cursor = "auto";
			if (reqFlag) { 
				alert("Medical results restricted from view.");
			}
			menu_busy = false;
		}			

		function workCompleted(){
			parent.nbaHiddenForm.MENU_ACTION.value="WorkCompleted";
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
			parent.nbaHiddenForm.target = "_top";
			parent.nbaHiddenForm.submit();
		}
		
		function openIndexView(){
			parent.nbaHiddenForm.MENU_ACTION.value="index";
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";
			parent.nbaHiddenForm.target = "_top";
			parent.nbaHiddenForm.submit();
		}
		
		function reopenCase(){
			parent.nbaHiddenForm.action = "/nba/reopenAction.do";
			parent.nbaHiddenForm.submit();
		}
		
		function evaluate(){
			parent.nbaHiddenForm.action = "/nba/evaluateAction.do";			
			parent.nbaHiddenForm.submit();
		}
		
		function submitForm(targetURL){
			parent.nbaHiddenForm.redirectURL.value = targetURL;
			parent.nbaHiddenForm.MENU_ACTION.value = "Redirect";
			parent.nbaHiddenForm.target = "_top";		
			parent.nbaHiddenForm.action="servlet/NbaMenuServlet";			
			parent.nbaHiddenForm.submit();
		}		
		
		function confirmDialog(returnPath) {
			var cont = true;
			var colonIndex = returnPath.indexOf(":");
			var message = returnPath.substring(0, colonIndex);
			var viewInfo = returnPath.substring(colonIndex + 1);
			colonIndex = viewInfo.indexOf("&");
			var targetURL = viewInfo.substring(0, colonIndex);
			viewName = viewInfo.substring(colonIndex + 1);
			var skipDialogPrompt = false;
			if (viewName == "BusinessProcessGuide" || viewName == "Contents" || viewName == "FAQ" || viewName == "About" || viewName == "View Images" || viewName == "View All Images") {
				skipDialogPrompt = true;
			}
			
			if(returnPath && !skipDialogPrompt){
				var path = appBasePath + "nbadialogprompt.jsp";				
				cont = openDialog(path,400,50,"result","message=" + message + "?").result == "true";
			}			
			if(cont){
				if (targetURL.indexOf("javascript") > -1) {
					eval(targetURL);
				} else {
					submitForm(targetURL);
				}
			}		
		
		}