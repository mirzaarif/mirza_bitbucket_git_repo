/** CHANGE LOG 
*  Audit Number   Version		Change Description -->
*  FNB004         NB-1101		PHI -->
*  NBA291         NB-1101		NBA291 nbA Windows 7 and IE 8.0 Certification Project -->
*  SPRNBA-658     NB-1301   	Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench -->
*  SPR3454	  	  NB-1401   	Not Prompted for Uncommitted Draft Changes When Navigating to Another Tab or Another Business Function -->
*  SPR3103        NB-1401   	Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View  -->
*  NBA356		  NB-1501		Comments Floating View -->
*  NBA362		  NB-1501		Save Draft Requirements and Impairments when navigating from case to case -->
**/

var captureFunctionScriptContent = 
		'if ((window.event.altKey) || ((window.event.keyCode == 8) && ' +
		'	(window.event.srcElement.type != \'text\' &&' +
		'    window.event.srcElement.type != \'textarea\' &&' +
		'    window.event.srcElement.type != \'password\')) || ' +
		'    ((window.event.ctrlKey) && ((window.event.keyCode == 78) || (window.event.keyCode == 82)) ) ||' +
 		'	(window.event.keyCode == 116) ) {' +
		'				if (window.event.keyCode == 116) top.refreshDT();' +
		'				window.event.cancelBubble = false;' +
		'				window.event.keyCode = 0;' +
		'				window.event.returnValue = false;' +
 	 	'}' +
 	 	'if ((window.event.srcElement.type == \'text\' ||' +
		'    window.event.srcElement.type == \'textarea\' ||' +
		'    window.event.srcElement.type == \'password\') && ((window.event.ctrlKey && (window.event.keyCode == 78 || window.event.keyCode == 82)) ||' +
 		'	 window.event.keyCode == 116)){' +
 		'		if (window.event.keyCode == 116) top.refreshDT();' +
		'		window.event.cancelBubble = false;' +
		'		window.event.keyCode = 0;' +
		'		window.event.returnValue = false;' +
		'};';

var onkeyCaptureFunctionScript = ' onKeyDown="' + captureFunctionScriptContent + '" ';

var documentKeyCaptureFunctionScript = '<SCRIPT> document.onkeydown = cancelBack;\n' +
		'function cancelBack(){' + captureFunctionScriptContent + '}</SCRIPT>';
		
var numTabsPerRow = 8;

	var rowLeft = 1;
	var topRowOffset = 10;
	var topRowZ=50;
	var bottomRowZ=100;
	var rowTop = 4;
	var rowBottom = 25;
	var groupBottom = -3;
	//begin SPR3454
	var draftTabText = null;
	var draftTabImage = null;
	var draftPageURL = null;
	var draftAppendText = null;
	//end SPR3454
	
	try{
		if(window.maxTabsPerRow){
			numTabsPerRow = window.maxTabsPerRow;
		}
	}catch(overridenumtabs){
	}
	
	try{
		if (window.topOffset){
			rowTop += topOffset;
			rowBottom += topOffset;
			groupBottom += topOffset;
		}			
	} catch(offsetvaluesVert){
	}
	try{
		if (window.leftOffset){
			rowLeft += window.leftOffset;
		}			
	} catch(offsetvaluesHoriz){
	}
	
	var tabWidth = 90;
	var textLeftMargin = 8;
	var textRightMargin = 20;
	var textTopMargin = 2;
	
	var oMenuPopup = null;
	if(window.createPopup != null){
		oMenuPopup = window.createPopup();
	}
	var menuTimer=null;
	var timerDelay=300;
	var MENU_WIDTH = 170;
	var currentTabOnClicks = new Array();
	var currentTabPages = new Array();


function disabledMethod(){
	return false;
}

	function pause(numberMillis) {
		var start = window.opener.startLog("Q&A pause");
		var dialogScript = 
   			'window.setTimeout(' +
   			' function () { window.close(); }, ' + numberMillis + ');';
		var result = window.showModalDialog( 'javascript:document.writeln(' +
    		'"<script>' + dialogScript + '<' + '/script>")');
		window.opener.completeLog("Q&A pause", start);
	}

		function loadPage(page){
			try {
				if(page.indexOf("DIV:") == 0){
					page = page.substr(4);
					// locate other "folder" Divs on page and set them to display:none
					elements = document.getElementsByTagName("div");
					for (var i = 0; i < elements.length; i++) {
						if(elements[i].getAttribute("id").indexOf("folder") == 0){
							// This is a folder DIV
							elements[i].style.display = "none";
						}
					}
					document.all[page].style.display = "inline";
					if (submitContents != null){
						submitContents();
					}
				} else {
					if (file.submitContents != null){
						file.submitContents();
					}
				var retries = 25; //NBA208-19
					
					var displayText = 					'<HTML><HEAD>' +
					'<LINK REL="stylesheet" TYPE="text/css" HREF="' +  context + '/css/accelerator.css">' +
					documentKeyCaptureFunctionScript +
					'</HEAD><BODY class="';
					displayText += "mainBody";
					displayText += ('" leftmargin="0" topmargin="0" ' + onkeyCaptureFunctionScript + '>' +
					'<table width="100%"><tr><td align="center" style="margin: 2px;	font: bold 10px \'Verdana\'; text-align: left; vertical-align: middle; color: navy; text-decoration: none;">' +
								'<div align="center" id="RefreshingDIV">' +
									'<br ID="imgLocation"/>' +
									'<br/>' +
									'<P/>' +
									'Please wait loading file...' +
								'</div>' +
						'</td></tr></table>' +
					'</BODY></HTML>');
		    		file.document.write(displayText);
				file.document.oncontextmenu = new Function("if (!top.enableContextMenu) { return false; }");
					file.location.href = page;

				pause(10); //NBA208-19
				var testurl = "[" + file.location + "]";
				while (testurl == "[]" && retries > 0 ){
		    		file.location.href = url;
					pause(100); //NBA208-19
					testurl = "[" + file.location + "]";
					retries--;
				}			
				if(testurl == "[]"){
					alert("Failed to load requested page.  Please contact System Administrator");
			}
				}			
		} catch(err){
			}
	}

			function showMenu(menu, control, x, y, subWidth) {
				try{
					if(window.createPopup){
						oMenuPopup = window.createPopup();
						var context = menu.context;
						oMenuPopup.document.write(
							'<HTML><HEAD>' +
							'<LINK REL="stylesheet" TYPE="text/css" HREF="' + context+ '/css/accelerator.css">' +
							'</HEAD>' +
							'<BODY class="menuBody" leftmargin="0" topmargin="0" >' +
							'</BODY></HTML>');
						oMenuPopup.document.getElementsByTagName("BODY")[0].insertAdjacentHTML("BeforeEnd",	menu.innerHTML);
						oMenuPopup.show(0, 0, subWidth, 0);
					    var height = oMenuPopup.document.body.scrollHeight + 5;
					    oMenuPopup.hide();
					    oMenuPopup.show(x, y, subWidth, height, control);
					  }
				} catch(er){
					top.reportException(er, "menus-showMenu");
				}
			}

			function hideMenu() {
				try {
					if(oMenuPopup){
					    oMenuPopup.hide();
					 }
				} catch(er){
					top.reportException(er, "menus-hideMenu");
				}
			}

			function onSubMenuOver(cell){
				try {
					cell.className = "menuItemSelected";
					timer(1);
				} catch(er){
					top.reportException(er, "menus-onSubMenuOver");
				}
			}

			function onSubMenuOverNoHighlight(cell){
				try {
					timer(1);
				} catch(er){
					top.reportException(er, "menus-onSubMenuOverNoHighlight");
				}
			}
		
			function onSubMenuOut(cell){
				try {
					cell.className = "menuItem";
					timer(0);
				} catch(er){
					top.reportException(er, "menus-onSubMenuOut");
				}
			}
		
			function timer(offon) {
				try {
					if (offon == 0)	{
						menuTimer = setTimeout('hideMenu()', timerDelay);
				    }
					if (offon ==1)	{
						clearTimeout(menuTimer);
					}
				} catch(er){
					top.reportException(er, "menus-timer");
				}
			}

			
			function tabMouseOver(tabImgNumber){
				var textElem = document.getElementById("TabText" + tabImgNumber);
				if(textElem != currentSelectedText){
					if(textElem.style.visibility=="visible"){
						//textElem.className="tabTextHover";
					}
				}
			}

			function tabMouseOut(tabImgNumber){
				var textElem = document.getElementById("TabText" + tabImgNumber);
				if(textElem != currentSelectedText){
					if(textElem.style.visibility=="visible"){
						//textElem.className="tabText";
					}
				}
			}

			

			function positionRowsBottom(){
				try{
					var pos = numTabsPerRow +1;
					var currentLeft = rowLeft;
					for(i = 1; i <= numTabsPerRow; i++){
						try{
							var imgElemstart = document.getElementById("TabImgStart" + i);
							var textElem = document.getElementById("TabText" + i);
							var imgElem = document.getElementById("TabImg" + i);
									var itemWidth = 90;
									try{
										var textValue = "";
										if(textElem.innerText){
											textValue = textElem.innerText;
										} else if(textElem.textContent){
											textValue = textElem.textContent;
										}
										itemWidth = (textValue.length * 8);
										if(textValue.indexOf(" ") > -1){
											var position = textValue.indexOf(" ");
											//alert(position);
											if(position < 3){
												position = textValue.indexOf(" ", position+1);
												//alert(position);
											}
											if(position > 3){
												if((textValue.length - position) > position){
													position = (textValue.length - position);
												}
													//alert(position);
													itemWidth =  (position * 7);
											}
										} 
									}catch(err2){
										itemWidth = 90;
									}
							if(itemWidth < 40){
								itemWidth=40;
							}
							
							if(imgElemstart != null){
								imgElemstart.className = "backtabBarLeft"; 	
								imgElemstart.style.visibility="visible";
								imgElemstart.style.top = rowBottom;
								imgElemstart.style.left = currentLeft + rowLeft;
								imgElemstart.style.height= "28px";
								imgElemstart.style.zIndex = (bottomRowZ + pos);
							}
	
							if(imgElem != null) {
								imgElem.className = "backtabBarCenter"; 
								imgElem.style.visibility="visible";
								imgElem.style.top = rowBottom;
								imgElem.style.left = currentLeft + rowLeft + 9;
								imgElem.style.width = itemWidth;
								imgElem.style.height= "28px";
								imgElem.style.zIndex = (bottomRowZ + pos);
							}
	
							var imgElem1 = document.getElementById("TabBack" + i);
							if(imgElem1 != null) {
								imgElem1.className = "backtabBarRight";	
								imgElem1.style.visibility="visible";
								imgElem1.style.top = rowBottom;
								imgElem1.style.left = currentLeft + rowLeft + itemWidth + 9;
								imgElem1.style.height= "28px";
								imgElem1.style.zIndex = (bottomRowZ + pos);
							}
						
							textElem.style.visibility="visible";
							textElem.style.width = itemWidth -5 ;
							textElem.style.top = rowBottom + textTopMargin;
							textElem.style.left = (currentLeft + rowLeft) + textLeftMargin;
							textElem.style.zIndex = (bottomRowZ + pos + 1);
							currentLeft += itemWidth + 8 + 10;
	
							try{
								imgElem = document.getElementById("TabImg" +( i + numTabsPerRow));
								if(imgElem != null){
									if(imgElem != null) {
										imgElem.style.top = rowTop; 
										imgElem.style.left = (((i-1) * tabWidth) + rowLeft) + topRowOffset;
										imgElem.style.zIndex = (topRowZ + pos);
									}
									imgElem1 = document.getElementById("TabBack" +( i + numTabsPerRow));
									if(imgElem1 != null) {
										imgElem1.style.visibility="hidden";
									}
									textElem = document.getElementById("TabText" + (i + numTabsPerRow));
									if(textElem != null) {
										textElem.style.width = imgElem.width - textRightMargin;
										textElem.style.top = rowTop + textTopMargin;
										textElem.style.left = (((i-1) * tabWidth) + rowLeft) + topRowOffset + textLeftMargin;
										textElem.style.zIndex = (topRowZ + pos +1);
									}
								}
							} catch (err){
							}
						}catch (err1){
						}
						pos = pos - 3;
					}
				}catch(err2){
					top.reportException(err2,"positionRowsBottom");
				}

			}

			function positionRowsTop(){
				try{
					var pos = numTabsPerRow+1;
					var currentLeft = rowLeft + topRowOffset;
					for(i = 1; i <= numTabsPerRow; i++){
						try{
							var imgElemstart = document.getElementById("TabImgStart" + i);
							var textElem = document.getElementById("TabText" + i);
							var imgElem = document.getElementById("TabImg" + i);
									var itemWidth = 90;
									try{
										var textValue = "";
										if(textElem.innerText){
											textValue = textElem.innerText;
										} else if(textElem.textContent){
											textValue = textElem.textContent;
										}
										itemWidth = (textValue.length * 8);
										if(textValue.indexOf(" ") > -1){
											var position = textValue.indexOf(" ");
											//alert(position);
											if(position < 3){
												position = textValue.indexOf(" ", position+1);
												//alert(position);
											}
											if(position > 3){
												if((textValue.length - position) > position){
													position = (textValue.length - position);
												}
													//alert(position);
													itemWidth =  (position * 7);
											}
										} 
									}catch(err2){
										itemWidth = 90;
									}
							if(itemWidth < 40){
								itemWidth=40;
							}
							
							if(imgElemstart != null){
								imgElemstart.className = "tabBarLeft"; 
								imgElemstart.style.visibility="visible";
								imgElemstart.style.top = rowTop;
								imgElemstart.style.left = currentLeft + rowLeft;
								imgElemstart.style.height= "28px";
								imgElemstart.style.zIndex = (bottomRowZ + pos);
							}
	
							if(imgElem != null) {
								imgElem.className = "tabBarCenter"; 
								imgElem.style.visibility="visible";
								imgElem.style.top = rowTop;
								imgElem.style.left = currentLeft + rowLeft + 9;
								imgElem.style.width = itemWidth;
								imgElem.style.height= "28px";
								imgElem.style.zIndex = (bottomRowZ + pos);
							}
	
							var imgElem1 = document.getElementById("TabBack" + i);
							if(imgElem1 != null) {
								imgElem1.className = "tabBarLeft"; 
								imgElem1.style.visibility="visible";
								imgElem1.style.top = rowTop;
								imgElem1.style.left = currentLeft + rowLeft + itemWidth;
								imgElem1.style.height= "28px";
								imgElem1.style.zIndex = (bottomRowZ + pos);
	
							}
						
							textElem.style.visibility="visible";
							textElem.style.width = itemWidth -5 ;
							textElem.style.top = rowTop + textTopMargin;
							textElem.style.left = (currentLeft + rowLeft) + textLeftMargin;
							textElem.style.zIndex = (bottomRowZ + pos + 1);
							currentLeft += itemWidth + 8 + 10;
	
							try{
								imgElem = document.getElementById("TabImg" + (i + numTabsPerRow));
								if(imgElem != null) {
									imgElem.style.top = rowBottom;
									imgElem.style.left = ((i-1) * tabWidth) + rowLeft;
									imgElem.style.zIndex = (bottomRowZ + pos);
								}
								imgElem1 = document.getElementById("TabBack" + (i + numTabsPerRow));
								if(imgElem1 != null) {
									imgElem1.style.visibility="hidden";
								}
								textElem = document.getElementById("TabText" + (i + numTabsPerRow));
								if(textElem != null){
									textElem.style.width = imgElem.width - textRightMargin;
									textElem.style.top = rowBottom + textTopMargin;
									textElem.style.left = (((i-1) * tabWidth) + rowLeft) + textLeftMargin;
									textElem.style.zIndex = (bottomRowZ + pos +1);
								}
							} catch(err){
							}
						} catch (err1){
						}
						pos = pos - 3;
					}
				}catch(err2){
					top.reportException(err2,"positionRowsTop");
				}
			}

			function swapTabs(newTabImg, newTabText, page, appendTabText){
				try{
					//begin SPR3454
					if (top.mainContentFrame && top.mainContentFrame.contentRightFrame) {  
						draftChanges = top.mainContentFrame.contentRightFrame.draftChanges;
						//begin NBA362
						if(top.mainContentFrame.contentRightFrame.nbaContextMenu != null){
							draftReqImp = top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp;
						}else {
							draftReqImp = "false";	
						}
						//end NBA362
						if(currentSelectedText != null && currentSelectedText != undefined){
							if (String(draftChanges) == "true" || String(draftReqImp) == "true") {	//NBA362
								//Do not invoke draft changes popup for UW comments
								//begin SPR3103
								if(top.mainContentFrame.contentRightFrame.ignoreDraftChangesForTab != null && (top.mainContentFrame.contentRightFrame.ignoreDraftChangesForTab(newTabText))){    
									top.mainContentFrame.contentRightFrame.prevSelectedText = currentSelectedText;
								}else{
									if(top.mainContentFrame.contentRightFrame.setPrevSelectedTabText != null){
										top.mainContentFrame.contentRightFrame.setPrevSelectedTabText(top.mainContentFrame.contentRightFrame.prevSelectedText,newTabText);
									}
									//Do not invoke the draft changes popup when Previous Selected and New Selected Tab Text is same
									if(newTabText != top.mainContentFrame.contentRightFrame.prevSelectedText){
										viewId="tabSwitch";
										draftTabText = newTabText;
										draftTabImage = newTabImg;
										draftPageURL = page;
										draftAppendText = appendTabText;
										top.showWindow(top.basePath,'faces/confirmDraftMessage.jsp?viewName='+viewId, this);
										return;
									}
								}
								//end SPR3103
							}
						}
					}
					//end SPR3454
					if(page.indexOf(".menu") < 0){
						if(currentSelectedImg != undefined && currentSelectedText != null){
							var tabID = currentSelectedText.id;

							if(tabID != null){
								tabID = tabID.substr(7);
								var ntabID = newTabText.id;
								ntabID = ntabID.substr(7);
								if(currentSelectedImg.origZ != undefined){ //NBA291
									currentSelectedImg.style.zIndex = currentSelectedImg.origZ;
								//begin NBA291
								} else {
									currentSelectedImg.style.zIndex = 0;
								}
								//end NBA291
								var i =0;
								if(tabID  > numTabsPerRow && ntabID  <=numTabsPerRow){
									// make lower numbers bottom
									alert("1");
									positionRowsBottom();
								} else if(tabID  <= numTabsPerRow && ntabID > numTabsPerRow){
									// make upper numbers bottom
									alert("2");
									positionRowsTop();
								} else {
									if(currentSelectedImg != null){
										currentSelectedImg.style.zIndex = currentSelectedImgZ;
										currentSelectedText.style.zIndex = currentSelectedTextZ;
										//Begin SPR2926
										if (currentSelectedImg.title=="notify"){
											currentSelectedImg.className = "notifyBacktabBarCenter";
										} 
										else {
										currentSelectedImg.className = "backtabBarCenter"; 
										}
										//End SPR2926
										var tabID = currentSelectedText.id;
										if(tabID != null){
											tabID = tabID.substr(7);
											var imgElemstart = document.getElementById("TabImgStart" + tabID);
											if(imgElemstart != null){
											//Begin SPR2926
												if (imgElemstart.title=="notify"){
													imgElemstart.className = "notifyBacktabBarLeft";
												} 
												else {
													imgElemstart.className = "backtabBarLeft"; 
												}
											//End SPR2926
											}
											
											var imgElem1 = document.getElementById("TabBack" + tabID);
											if(imgElem1 != null){
											//Begin SPR2926
												if (imgElem1.title=="notify"){
													imgElem1.className = "notifyBacktabBarRight";
												} 
												else {
													imgElem1.className = "backtabBarRight";
												}
												//End SPR2926
											}
		
											if (currentSelectedText.className != "disabledTabText"){
												currentSelectedText.className = "tabText";
											}
										}
									}
								}
							}
						} else {
							// ensure correct row on bottom on initial population
							var tabID = newTabText.id;
							if(tabID != null){
								tabID = tabID.substr(7);
								if(tabID <=  numTabsPerRow){
									// make lower numbers bottom
									positionRowsBottom();
								} else {
									// make upper numbers bottom
									positionRowsTop();
								} 
							}
						}
						
						newTabText.className = "selectedTabText";
						currentSelectedText = newTabText;
						currentSelectedImg = newTabImg;
						currentSelectedImgZ = currentSelectedImg.style.zIndex;
						currentSelectedTextZ = currentSelectedText.style.zIndex;
						currentSelectedText.style.zIndex="150";
						currentSelectedImg.style.zIndex="149";
						//Begin SPR2926
						if (currentSelectedImg.title=="notify"){
							currentSelectedImg.className = "notifyTabBarCenter";
						} 
						else {
							currentSelectedImg.className = "tabBarCenter";
						}
						//End SPR2926
						var tabID = currentSelectedText.id;
						if(tabID != null){
							tabID = tabID.substr(7);
							var imgEnd = document.getElementById("TabBack" + tabID);
							if(imgEnd  != null){
								//Begin SPR2926
								if (imgEnd.title=="notify"){
									imgEnd.className = "notifyTabBarRight";
								} 
								else {
									imgEnd.className = "tabBarRight";
								}			
								//End SPR2926
							}
							var imgStart = document.getElementById("TabImgStart" + tabID);
							if(imgStart  != null){
							//Begin SPR2926
								if (imgStart.title=="notify"){
									imgStart.className = "notifyTabBarLeft";
								} 
								else {
									imgStart.className = "tabBarLeft"; 
								}				
								//End SPR2926
							}
						}
					}
				} catch(err){
					top.reportException(err, "swap tabs");
				}
					
				if(page.indexOf(".menu") < 0){
					if(appendTabText != null){
						try{
							var currentTextNode = document.getElementById("TabTextValue" + currentSelectedImg.priority);
							if(currentTextNode.innerText){
								currentTextNode.innerText = appendTabText;
							} else if(currentTextNode.textContent){
								currentTextNode.textContent = appendTabText;
							}
						} catch(err){
						}
					}
					loadPage(page);
					hideMenu();
				} else {
					var eMenu = document.all["menu" + newTabImg.priority];
					if (eMenu) {
						showMenu(eMenu, newTabText, 0, 20, MENU_WIDTH);
					}
				}
			}

			function swapGroupedTabs(newTabImg, newTabText, page){
				try{
					if(currentSelectedImg != undefined){
						currentSelectedImg.style.zIndex = currentSelectedImg.origZ;
						if(currentSelectedText != null){
							currentSelectedText.className= "tabText";
						}
						var i =0;
						positionGroupRows();
					} else {
						positionGroupRows();
					}
					newTabText.className = "selectedTabText";
					currentSelectedText = newTabText;
					currentSelectedImg = newTabImg;
					currentSelectedImgZ = currentSelectedImg.style.zIndex;
					currentSelectedTextZ = currentSelectedText.style.zIndex;
					//Begin SPR2926
					if (currentSelectedImg.title=="notify"){
						currentSelectedImg.className = "notifyTabBarCenter";
					} 
					else {
						currentSelectedImg.className = "tabBarCenter";
					}
					//End SPR2926
					var tabID = currentSelectedText.id;
					if(tabID != null){
						tabID = tabID.substr(7);
						var imgEnd = document.getElementById("TabBack" + tabID);
						if(imgEnd  != null){
						//Begin SPR2926
							if (imgEnd.title=="notify"){
								imgEnd.className = "notifyTabBarRight";
							} 
							else {
								imgEnd.className = "tabBarRight"; 
							}
						//End SPR2926	
						}
						var imgStart = document.getElementById("TabImgStart" + tabID);
						if(imgStart  != null){
							//Begin SPR2926	
							if (imgStart.title=="notify"){
								imgStart.className = "notifyTabBarLeft";
							} 
							else {
								imgStart.className = "tabBarLeft"; 
							}
							//End SPR2926	
						}
					}

				} catch(err){
					//top.reportException(err, "swap grouped tabs");
				}
				try{
					loadPage(page);
				}catch(err1){
				}
			}


			function groupMouseOver(groupNumber){
			}

			function groupMouseOut(groupNumber){
			}
			
			function positionGroup(){
				var groupWidth = 80;  // need calculate based on width and numGroups global var
				var currentLeft = textLeftMargin;
				for(i = 1; i < numTabsPerRow+1; i++){
					try{
						var textElem = document.getElementById("GroupText" + i);
						if(textElem != null) { 
							textElem.style.top = groupBottom + textTopMargin;
							textElem.style.left = currentLeft;
							textElem.style.zIndex="150";
							try{
								if(textElem.innerText){
									currentLeft += (textElem.innerText.length * 8);
								}else if(textElem.textContent){
									currentLeft += (textElem.textContent.length * 9);
								}
								currentLeft += 10;
							}catch(err){
								currentLeft += 105;
							}
						}
					}catch (err1){
					}
				}
			}

			function positionGroupRows(){
				var pos = numTabsPerRow +1;
				var currentLeft = rowLeft;
				for(j = 1; j <= numTabsPerRow; j++){
					for(i = 1; i <= numTabsPerRow; i++){
						try{
							var identifier = j + "_" + i;
							if(j == currentGroupNumber){
								var textElem = document.getElementById("TabText" + identifier);
								if(textElem != null){
									var imgElemstart = document.getElementById("TabImgStart" + identifier);
									var imgElem = document.getElementById("TabImg" + identifier);
									var itemWidth = 90;
									try{
										var textValue = "";
										if(textElem.innerText){
											textValue = textElem.innerText;
										} else if(textElem.textContent){
											textValue = textElem.textContent;
										}
										itemWidth = (textValue.length * 8);
										if(textValue.indexOf(" ") > -1){
											var position = textValue.indexOf(" ");
											//alert(position);
											if(position < 3){
												position = textValue.indexOf(" ", position+1);
												//alert(position);
											}
											if(position > 3){
												if((textValue.length - position) > position){
													position = (textValue.length - position);
												}
													//alert(position);
													itemWidth =  (position * 6);
											}
										} 
									}catch(err2){
										itemWidth = 90;
									}
									if(itemWidth < 40){
										itemWidth=40;
									}

									if(imgElemstart != null){
										imgElemstart.className = "backtabBarLeft"; 
										imgElemstart.style.visibility="visible";
										imgElemstart.style.top = rowBottom;
										imgElemstart.style.left = currentLeft + rowLeft;
										imgElemstart.style.height= "28px";
										imgElemstart.style.zIndex = (bottomRowZ + pos);
									}
			
									if(imgElem != null) {
										imgElem.className = "backtabBarCenter"; 
										imgElem.style.visibility="visible";
										imgElem.style.top = rowBottom;
										imgElem.style.left = currentLeft + rowLeft + 9;
										imgElem.style.width = itemWidth;
										imgElem.style.height= "28px";
										imgElem.style.zIndex = (bottomRowZ + pos);
									}
			
									var imgElem1 = document.getElementById("TabBack" + identifier);
									if(imgElem1 != null) {
										imgElem1.className = "backtabBarRight"; 
										imgElem1.style.visibility="visible";
										imgElem1.style.top = rowBottom;
										imgElem1.style.left = currentLeft + rowLeft + itemWidth + 9;
										imgElem1.style.height= "28px";
										imgElem1.style.zIndex = (bottomRowZ + pos);
			
									}
									textElem.style.visibility="visible";
									textElem.style.width = itemWidth -5 ;
									textElem.style.top = rowBottom + textTopMargin;
									textElem.style.left = (currentLeft + rowLeft) + textLeftMargin;
									textElem.style.zIndex = (bottomRowZ + pos + 1);
									currentLeft += itemWidth + 8 + 10;

								}
						pos = pos - 3;

							} else {
								try{
									var imgElemstart = document.getElementById("TabImgStart" + identifier);
									if(imgElemstart != null) { imgElemstart.style.visibility="hidden";}
									var imgElem = document.getElementById("TabImg" + identifier);
									if(imgElem != null) { imgElem.style.visibility="hidden";}
									var imgElem1 = document.getElementById("TabBack" + identifier);
									if(imgElem1 != null) { imgElem1.style.visibility="hidden";}
									var textElem = document.getElementById("TabText" + identifier);
									if(textElem != null) { textElem.style.visibility="hidden";}
								} catch (err){
								}
							}
						}catch (err1){
							//top.reportException("showTab",err1);
						}
					}
				}
		}

			function swapGroup(grNumber, page){
				try{
					// begin SPRNBA-658
					if (top.mainContentFrame && top.mainContentFrame.contentRightFrame && top.mainContentFrame.contentRightFrame.nbaContextMenu) {
						draftChanges = top.mainContentFrame.contentRightFrame.draftChanges; //SPR3454
						draftComments = top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments;
						draftReqImp = top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp;	//NBA362
						if (String(draftComments) == "true" || String(draftChanges) == "true" || String(draftReqImp) == "true"){  //SPR3454	//NBA362
							// Display draft comments popup
							viewId="tabGroupSwitch";
							top.showWindow(top.basePath,'faces/confirmDraftMessage.jsp?viewName='+viewId+'&grNumber='+grNumber+'&page='+page, this);
							return;
						}
					}
					// end SPRNBA-658
					
					currentGroupNumber = grNumber;
					positionGroup();
					var group = document.getElementById("GroupText" + grNumber);
					if(currentGroupText != null){
						currentGroupText.className = "tabGroup";
					}
					currentGroupText = group;
					currentGroupText.className = "selectedTabGroup";
					positionGroupRows();
					//Begin FNB004 - VEPL1224
					if (top.mainContentFrame && top.mainContentFrame.contentRightFrame && top.mainContentFrame.contentRightFrame.nbaContextMenu) {
						phipopup = top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup;
						//Begin NBA356 Close UW Comments Pop up View when major tab changes
						commentView = top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView;
						if (commentView && !commentView.closed) {	
							commentView.close();
						}
						// End NBA356 
						if (phipopup && !phipopup.closed) {	//Close PHI Briefcase when major tab changes
							phipopup.close();
						}
					}
					//end FNB004 - VEPL1224					
					//begin NBA213
					//Iterate thru the sub tabs to find the first tab that is not
					//disabled and select it. If no tab is enabled, the view should
					//remain the same.
					var subtab = 1;
					do {
						var tabText = document.getElementById("TabText" + grNumber + "_" + subtab);
						if (tabText != null) {
							if (tabText.className != "disabledTabText") {
								var tabImage = document.getElementById("TabImg" + grNumber + "_" + subtab);
								if(tabImage != null){
									tabImage.onclick();
									return;
								}
							}
							subtab++;
						}
					} while(tabText != null);
					//end NBA213
				} catch(err){

					top.reportException(err, "swap Group");
				}
			}
			
			function swapGroupWithTab(grNumber, page, tabIndex){
				

				try{
					currentGroupNumber = grNumber;
					positionGroup();
					var group = document.getElementById("GroupText" + grNumber);
					if(currentGroupText != null){
						currentGroupText.className = "tabGroup";
					}
					currentGroupText = group;
					currentGroupText.className = "selectedTabGroup";
					var tabImage = document.getElementById("TabImg" + grNumber + "_" + tabIndex);
					var tabText = document.getElementById("TabText" + grNumber + "_" + tabIndex);
					swapGroupedTabs(tabImage, tabText, page);
					tabImage.className = "tabBarCenter";
					//Begin SPR2926
					if (tabImage.title=="notify"){
						tabImage.className = "notifyTabBarCenter";
					} 
					else {
						tabImage.className = "tabBarCenter"; 
					}
					//End SPR2926
					var imgEnd = document.getElementById("TabBack" + grNumber + "_" + tabIndex);
					if(imgEnd  != null){
					//Begin SPR2926
						if (imgEnd.title=="notify"){
							imgEnd.className = "notifyTabBarRight";
						} 
						else {
							imgEnd.className = "tabBarRight"; 
						}
					//End SPR2926							
					}
					var imgStart = document.getElementById("TabImgStart" + grNumber + "_" + tabIndex);
					if(imgStart  != null){
					//Begin SPR2926	
							if (imgStart.title=="notify"){
								imgStart.className = "notifyTabBarLeft";
							} 
							else {
								imgStart.className = "tabBarLeft"; 
							}
					//End SPR2926				 			
					}
				} catch(err){
					top.reportException(err, "swap Group");
				}
			}
			
			function setTabImage(){
				try{
					if(window.forceUpdate != null && window.forceUpdate == 'true'){
						tabImageToUse = updateTabImage;
					} else {
						if(document.body.className == "updatePanel"){
							tabImageToUse = updateTabImage;
						}
					}
				} catch(ex){
				}
			}
			
			function disableTab(identifier){
				try{
					var imgElem = document.getElementById("TabImg" + identifier);
					if(imgElem != null) { 
						var temp = "TabImg" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = imgElem.onclick;
							currentTabOnClicks[temp + 'onmouseenter'] = imgElem.onmouseenter;							
						}
						imgElem.onclick = disabledMethod;
						imgElem.onmouseenter = disabledMethod;
					}
					var imgElem1 = document.getElementById("TabBack" + identifier);
					if(imgElem1 != null) { 
						var temp = "TabBack" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = imgElem1.onclick;
							currentTabOnClicks[temp + 'onmouseenter'] = imgElem1.onmouseenter;
						}
						imgElem1.onclick = disabledMethod;
						imgElem1.onmouseenter = disabledMethod;
					}
					var textElem = document.getElementById("TabText" + identifier);
					if(textElem != null){
						var temp = "TabText" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = textElem.onclick;						
							currentTabOnClicks[temp + 'onmouseenter'] = textElem.onmouseenter;						
						}						
						textElem.onclick = disabledMethod;
						textElem.onmouseenter = disabledMethod;		
						textElem.className = "disabledTabText";
					}
				}catch(err){
				}
			}
			
			function disableGroupSection(identifier) {
				var divGroup = document.getElementById('GroupText' + identifier);
				if(currentTabOnClicks["TabText" + identifier] == null) {
					currentTabOnClicks["GroupText" + identifier] = divGroup.onclick;
					temp = "GroupText" + identifier;
				}
				divGroup.className="disabledTabGroup";
				divGroup.onclick = disabledMethod;
			}
			
			function disableAllTabs() {
				try{
					identifier = _TabIndexes;
					var tabIndexes = identifier.split(',');
					var groupSection = -1;
					for(i=0; i < tabIndexes.length; i++){
						if (tabIndexes[i].indexOf('_') > 0) {
							var temp = tabIndexes[i].split('_')[0];
							if (temp != groupSection) {
								disableGroupSection(temp);
								groupSection = temp;
							}
						}
						disableTab(tabIndexes[i]);
					}
				}catch(err){
				}
			}

			function enableAllTabs() {
				try{
					var identifier = _TabIndexes;
					var tabIndexes = identifier.split(',');
					var groupSection = -1;
					for(i=0; i < tabIndexes.length; i++){			
						if (tabIndexes[i].indexOf('_') > 0) {
							var temp = tabIndexes[i].split('_')[0];
							if (temp != groupSection) {
								enableGroupSection(tabIndexes[i].split('_')[0]);
								groupSection = temp;
							}
						}
						enableTab(tabIndexes[i]);
					}
				}catch(err){
				}
			}
			
			function enableGroupSection(identifier) {
				var divGroup = document.getElementById('GroupText' + identifier);
				if(currentTabOnClicks["GroupText" + identifier] != null) {
					temp = "GroupText" + identifier;
					divGroup.onclick = currentTabOnClicks["GroupText" + identifier] ;
				}
				divGroup.style.display = "inline";
			}
			
			function enableTab(identifier){
				try{
					var imgElem = document.getElementById("TabImg" + identifier);
					temp = "TabImg" + identifier;
					if(imgElem != null && currentTabOnClicks[temp + 'onclick'] != null) { 
						imgElem.onmouseenter = currentTabOnClicks[temp + 'onmouseenter'];
						imgElem.onclick = currentTabOnClicks[temp + 'onclick'];
						imgElem.style.display = "inline";
					}
					
					var imgElem1 = document.getElementById("TabBack" + identifier);
					var temp = "TabBack" + identifier;
					if(imgElem1 != null && currentTabOnClicks[temp + 'onclick'] != null) { 
						imgElem1.onmouseenter = currentTabOnClicks[temp + 'onmouseenter'];
						imgElem1.onclick = currentTabOnClicks[temp + 'onclick'];
						imgElem1.style.display = "inline";
					}
					var textElem = document.getElementById("TabText" + identifier);
					var temp = "TabText" + identifier
					if(textElem != null && currentTabOnClicks[temp + 'onclick'] != null){
						textElem.onmouseenter = currentTabOnClicks[temp + 'onmouseenter'];
						textElem.className = "tabText";
						textElem.onclick = currentTabOnClicks[temp + 'onclick'];
						textElem.style.display = "inline";
					}
				}catch(err){
				}
			}
			
			function hideTab(identifier){
				try{
					var imgElem = document.getElementById("TabImg" + identifier);
					if(imgElem != null) { 
						var temp = "TabImg" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = imgElem.onclick;
							currentTabOnClicks[temp + 'onmouseenter'] = imgElem.onmouseenter;							
						}
						imgElem.onclick = disabledMethod;
						imgElem.onmouseenter = disabledMethod;
						imgElem.style.display = "none";
					}
					
					var imgElem1 = document.getElementById("TabBack" + identifier);
					if(imgElem1 != null) { 
						var temp = "TabBack" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = imgElem1.onclick;
							currentTabOnClicks[temp + 'onmouseenter'] = imgElem1.onmouseenter;
						}
						imgElem1.onclick = disabledMethod;
						imgElem1.onmouseenter = disabledMethod;
						imgElem1.style.display = "none";
					}
					var textElem = document.getElementById("TabText" + identifier);
					if(textElem != null){
						var temp = "TabText" + identifier;
						if(currentTabOnClicks[temp + 'onclick'] == null) {
							currentTabOnClicks[temp + 'onclick'] = textElem.onclick;						
							currentTabOnClicks[temp + 'onmouseenter'] = textElem.onmouseenter;						
						}						
					
						textElem.onclick = disabledMethod;
						textElem.onmouseenter = disabledMethod;
						textElem.className = "disabledTabText";
						textElem.style.display = "none";
					}
				}catch(err){
				}
			}
			
			function hideGroupSection(identifier) {
				var divGroup = document.getElementById('GroupText' + identifier);
				if(currentTabOnClicks["TabText" + identifier] == null) {
					currentTabOnClicks["GroupText" + identifier] = divGroup.onclick;
					temp = "GroupText" + identifier;
				}
				divGroup.onclick = disabledMethod;
				divGroup.style.display = "none";
			}
			
			//SPR2926 New Method
			function notifyTab(identifier, value){
				try{
					var imgElem = document.getElementById("TabImg" + identifier);
					var imgElemstart = document.getElementById("TabImgStart" + identifier);
					var imgElem1 = document.getElementById("TabBack" + identifier);
					if(imgElem != null) { 
						if (value == 'true') {
							imgElem.className = "notifyTabBarCenter";
							imgElem.title ="notify";
						} else {  //SPRNBA-469
							imgElem.className = "tabBarCenter";  //SPRNBA-469 
							imgElem.title = "";  //SPRNBA-469
						}
					}
					if(imgElemstart != null) { 
						if (value == 'true') {
							imgElemstart.className = "notifyTabBarLeft";
							imgElemstart.title ="notify";
						} else {  //SPRNBA-469
							imgElemstart.className = "tabBarLeft";  //SPRNBA-469 
							imgElemstart.title = "";  //SPRNBA-469
						}
					}
					if(imgElem1 != null) { 
						if (value == 'true') {
							imgElem1.className = "notifyTabBarRight";
							imgElem1.title ="notify";
						} else {  //SPRNBA-469
							imgElem1.className = "tabBarRight";  //SPRNBA-469 
							imgElem1.title = "";  //SPRNBA-469
						}
					}
				}catch(err){
				}
			}
			