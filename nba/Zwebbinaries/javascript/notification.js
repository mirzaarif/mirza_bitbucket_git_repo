var debug = false;

var username="";
var password="";

// - callback function to handle events from server
function processEvent(responseXML){
	try{
		var command = getResponseCommand(responseXML);
		var chatData = getChatData(responseXML);
		var callData = getCallData(responseXML);
		if(command == "CALL"){
			// inbound call launch screen pop
			// reformat call data appropriately
			launchInboundCall(callData);
		} else if(command == "CHAT"){
			launchInboundChat(chatData);
		} else if(command == "CALL_ENDED"){
			// current call terminated by other end
			launchCallTerminated(callData);
		} else if(command == "UPDATE_WAIT"){
			//var message = getMessage(responseXML);
			//top.setWaitMessage(message);
		} else if(command == "CHAT_ENDED"){
			// current call terminated by other end
			indicateChatTerminated(callData);
		} else if(command == "SEND_TEXT"){
			setText(responseXML);
		} else if(command == "SEND_UPDATE"){
			setUpdate(responseXML);
		} else {
			// unknown event so ignore for now
		}
	}catch(err){
		alert("Failed to process event");
	}
}


function launchInboundChat(data){
	document.getElementById("chatData").innerHTML = data;
	parent.showWindow(path,'faces/chat/chatDetails.jsp', parent);
}

//
// - launches the inbound call terminated on remote side alert
//
function indicateChatTerminated(data){
	try{
		var currentWindowNumber = top.currentWindow;
		var chatWindowNumber = currentWindowNumber--;
		var element = top.document.getElementById('FWin' + chatWindowNumber);	
		var chatWindow = element.contentWindow.document.getElementsByTagName("iframe")[0].contentWindow;
		if(chatWindow != null){
			chatWindow.chatDisconnected();
		}
	}catch(err){
	}
}

function setText(xml){
	var text = getChatText(xml);
	var sourceUserName = getChatSourceUser(xml);
	try{
		var currentWindowNumber = top.currentWindow;
		var chatWindowNumber = currentWindowNumber --;
		var element = top.document.getElementById('FWin' + chatWindowNumber);	
		var chatWindow = element.contentWindow.document.getElementsByTagName("iframe")[0].contentWindow;
		if(chatWindow != null){
			chatWindow.addText(sourceUserName, text);
		}
	}catch(err){
	}
}

function setUpdate(xml){
	var text = getChatText(xml);
	var sourceUserName = getChatSourceUser(xml);
	try{
		var currentWindowNumber = top.currentWindow;
		var chatWindowNumber = currentWindowNumber --;
		var element = top.document.getElementById('FWin' + chatWindowNumber);	
		var chatWindow = element.contentWindow.document.getElementsByTagName("iframe")[0].contentWindow;
		if(chatWindow != null){
			chatWindow.addUpdate(sourceUserName, text);
		}
	}catch(err){
	}
}


function registerChat(){
	try{
		var response = notifyClient.registerChatSession(username, username, password);
		handleErrors(response);
	} catch(err){
		top.reportException(err, "notification - register chat");
	}
	return getExtension();
}

function sendText(text){
	try{
		notifyClient.sendText(text);
	}catch(err){
		top.reportException(err, "chat - send Text");
	}
}

//
// - unregister current user with Chat environment
//
function unregisterChat(){
	try{
		var response = notifyClient.unregisterChatSession();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "chat - unregisterChat");
	}
		
}

//
// - disconnects chat
//
function disconnect(){
	try{
		var response = notifyClient.disconnectChat();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "chat - disconnect");
	}
}

function connectToAvailableCSR(firstName, lastName){
	try{
		var response = notifyClient.connectCSR(firstName, lastName);
		handleErrors(response);
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "chat - connect");
	}
}
//
// - connects to CSR
//
function connect(){
	try{
		var response = notifyClient.acceptChat();
		handleErrors(response);
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "chat - acceptChat");
	}
}

//
// - makes operator available for chats
//
function switchAvailableChat(){
	try{
		var response = notifyClient.switchAvailableForChat();
		handleErrors(response);
	} catch(err){
		top.reportException(err, "chat - switchAvailableForChat");
	}
}

function getAvailableChat(){
	try{
		return notifyClient.isAvailableForChats();
	} catch(err){
		top.reportException(err, "chat - getAvailable");
	}
	return "false";
}


//
// - launches the inbound call screen popup with provided data XML
//
function launchInboundCall(data){
	document.getElementById("callData").innerHTML = data;
	parent.showWindow(path,'faces/cti/callDetails.jsp', parent);
}

//
// - launches the inbound call terminated on remote side alert
//
function launchCallTerminated(data){
	document.getElementById("callData").innerHTML = data;
	parent.showWindow(path,'faces/cti/callTerminated.jsp', parent);
}


function register(){
	try{
		var response = notifyClient.registerWorkstation(username, password);
		handleErrors(response);
	} catch(err){
		top.reportException(err, "cti - register");
	}
	return getExtension();
}

//
// - unregister current user with CTI ACD environment
//
function unregisterCTI(){
	try{
		var response = notifyClient.unregisterWorkstation();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - unregisterCTI");
	}
		
}

//
// - hangs up current call
//
function hangupCall(){
	try{
		var response = notifyClient.hangupCall();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - hangupCall");
	}
}

//
// - accepts current inbound call
//
function acceptCall(){
	try{
		var response = notifyClient.acceptCall();
		handleErrors(response);
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - acceptCall");
	}
}

//
// - completes transfer initiated with a consult
//
function completeTransfer(){
	try{
		var response = notifyClient.completeTransfer();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - completeTransfer");
	}
}

//
// - initiates a consultation with another extension (step 1 for transfer or conference)
//
function consult(targetExtension){
	try{
		var response = notifyClient.consult(targetExtension);
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - consult");
	}

}

//
// - dials an outbound call if not in current call session
//
function dialCall(targetNumber){
	try{
		if(notifyClient.getEnabled() == "true"){
			var response = notifyClient.dialCall(targetNumber);
			handleErrors(response);
			if(debug){
				alert(response);
			}
			return isSuccess(response);
		} else {
			alert("CTI is currently not enabled, please dial the number entered in the 'Number:' field to place the call.\n\r Then click close to contine.");
			return false;
		}
	} catch(err){
		top.reportException(err, "cti - dialCall");
	}
}

//
// - retrieves current assigned extension
//
function getExtension(){
	var value = notifyClient.getExtension();
	return value;
}

//
// - places current call on hold
//
function hold(){
	try{
		var response = notifyClient.hold();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - hold");
	}
}

//
// - resumes current call
//
function resume(){
	try{
		var response = notifyClient.resume();
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - resume");
	}

}

//
// - makes operator available for calls
//
function switchAvailable(){
	try{
		var response = notifyClient.switchAvailableForCalls();
		handleErrors(response);
	} catch(err){
		top.reportException(err, "cti - switchAvailable");
	}
}

function getAvailable(){
	try{
		return notifyClient.isAvailableForCalls();
	} catch(err){
		top.reportException(err, "cti - getAvailable");
	}
	return "false";
}

//
// - terminates an initiated consultation call to given extension
//
function terminateConsult(consultExtension){
	try{
		var response = notifyClient.terminateConsult(consultExtension);
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - terminateConsult");
	}
}

//
// - places consultation call on hold
//
function holdConsult(targetExtension){
	try{
		var response = notifyClient.holdConsult(targetExtension);
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - holdConsult");
	}
		
}

//
// - resumes consultation call
//
function resumeConsult(targetExtension){
	try{
		var response = notifyClient.resumeConsult(targetExtension);
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - resumeConsult");
	}
	
}

//
// - adds given consultation call into current call - conferences them in with client
//
function conference(targetExtension){
	try{
		var response = notifyClient.conference(targetExtension);
		handleErrors(response);
		if(debug){
			alert(response);
		}
		return isSuccess(response);
	} catch(err){
		top.reportException(err, "cti - conference");
	}
}

function isSuccess(responseXML){
	if(responseXML.indexOf("<success>true</success>") > -1){
		return true;
	} else {
		return false;
	}
}

function handleErrors(responseXML){
	var responseMessages = "";
	if(responseXML != null && responseXML != ""){
		var startPoint = responseXML.indexOf("<error>");
		if(startPoint != -1){
			var endPoint = responseXML.indexOf("</error>");
			responseMessages = responseXML.substr(startPoint + 7, endPoint);
			if(responseMessages != null && responseMessages != ""){
				document.getElementById("Messages").innerHTML = responseMessages;
				parent.showWindow(path,'faces/error.jsp', parent);
			}
		}
	}
}

function getChatData(responseXML){
	try{
		var calldata = "";
		var startPoint = responseXML.indexOf("<chat-data>");
		var endPoint = responseXML.indexOf("</chat-data>");
		var length = endPoint - (startPoint + 11);
		calldata = responseXML.substr(startPoint + 11, length);
		return calldata;
	} catch(err){
		top.reportException(err, "chat-getChatData");
	}
}

function isCurrentCallHeld(){
	try{
		return notifyClient.isCurrentCallHeld();
	} catch(err){
		top.reportException(err, "cti-isCallHeld");
	}
	return false;
}


function getCallData(responseXML){
	try{
		var calldata = "";
		var startPoint = responseXML.indexOf("<call-data>");
		var endPoint = responseXML.indexOf("</call-data>");
		var length = endPoint - (startPoint + 11);
		calldata = responseXML.substr(startPoint + 11, length);
		return calldata;
	} catch(err){
		top.reportException(err, "cti-getCallData");
	}
}

function getMessage(responseXML){
	try{
		var message = "";
		var startPoint = responseXML.indexOf("<messagetext><![CDATA[");
		var endPoint = responseXML.indexOf("]]></messagetext>");
		var length = endPoint - (startPoint + 22);
		messafe = responseXML.substr(startPoint + 22, length);
		return message;
	} catch(err){
		top.reportException(err, "notification-getMesage");
	}
}

function getResponseCommand(responseXML){
	try{
		var command = "";
		var startPoint = responseXML.indexOf("<command>");
		var endPoint = responseXML.indexOf("</command>");
		var length = endPoint - (startPoint + 9);
		command = responseXML.substr(startPoint + 9, length);
		return command;
	} catch(err){
		top.reportException(err, "notification-getResponseCommand");
	}
}

function getChatText(responseXML){
	try{
		var command = "";
		var startPoint = responseXML.indexOf("<text><![CDATA[");
		var endPoint = responseXML.indexOf("]]></text>");
		var length = endPoint - (startPoint + 15);
		command = responseXML.substr(startPoint + 15, length);
		return command;
	} catch(err){
		top.reportException(err, "chat-getText");
	}
}

function getChatSourceUser(responseXML){
	try{
		var command = "";
		var startPoint = responseXML.indexOf("<source-user>");
		var endPoint = responseXML.indexOf("</source-user>");
		var length = endPoint - (startPoint + 13);
		command = responseXML.substr(startPoint + 13, length);
		return command;
	} catch(err){
		top.reportException(err, "chat-get source user");
	}
}

function logToServer(source, text){
	try{
		if(serverLoggingEnabled != null && serverLoggingEnabled == true){
			notifyClient.sendLogEntry(username, source, text);
		}
	}catch(err){
		top.reportException(err, "notification-logToServer");
	}
}

function setAppletLogging(enabled){
	try{
		notifyClient.setLogging(enabled);
	}catch(err){
		top.reportException(err, "notification-setAppletLogging");
	}
}