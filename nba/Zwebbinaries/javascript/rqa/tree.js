var path = 'images/rqa/';
var TREE_TPL = {
	'target'  : 'frameset',	// name of the frame links will be opened in
							// other possible values are: _blank, _parent, _search, _self and _top

	'icon_e'  : path + 'empty.gif', // empty image
	'icon_l'  : path + 'line.gif',  // vertical line

        'icon_32' : path + 'base.gif',   // root leaf icon normal
        'icon_36' : path + 'base.gif',   // root leaf icon selected
	
	'icon_48' : path + 'base.gif',   // root icon normal
	'icon_52' : path + 'base.gif',   // root icon selected
	'icon_56' : path + 'base.gif',   // root icon opened
	'icon_60' : path + 'base.gif',   // root icon selected
	
	'icon_16' : path + 'folder.gif', // node icon normal
	'icon_20' : path + 'folderopen.gif', // node icon selected
	'icon_24' : path + 'folderopen.gif', // node icon opened
	'icon_28' : path + 'folderopen.gif', // node icon selected opened

	'icon_100'  : path + 'page.gif', // disabled normal
	'icon_101'  : path + 'page.gif', // disabled  selected
	'icon_102'  : path + 'greenpage.gif', // optional normal
	'icon_103'  : path + 'greenpage.gif', // optional selected
	'icon_104'  : path + 'redpage.gif', // required normal
	'icon_105'  : path + 'redpage.gif', // required selected
	'icon_110'  : path + 'checkmark.gif', // required selected
	
	'icon_2'  : path + 'joinbottom.gif', // junction for leaf
	'icon_3'  : path + 'join.gif',       // junction for last leaf
	'icon_18' : path + 'plusbottom.gif', // junction for closed node
	'icon_19' : path + 'plus.gif',       // junctioin for last closed node
	'icon_26' : path + 'minusbottom.gif',// junction for opened node
	'icon_27' : path + 'minus.gif'       // junctioin for last opended node
};

function rqaTree (a_currentItem, a_items, a_template) {

	this.a_tpl      = a_template;
	this.a_config   = a_items;
	this.o_root     = this;
	this.a_index    = [];
	this.o_selected = null;
	this.n_depth    = -1;

	var o_icone = new Image(),
		o_iconl = new Image();
	o_icone.src = a_template['icon_e'];
	o_iconl.src = a_template['icon_l'];
	a_template['im_e'] = o_icone;
	a_template['im_l'] = o_iconl;
	for (var i = 0; i < 64; i++)
		if (a_template['icon_' + i]) {
			var o_icon = new Image();
			a_template['im_' + i] = o_icon;
			o_icon.src = a_template['icon_' + i];
		}
	
	this.toggle = function (n_id) {	var o_item = this.a_index[n_id]; o_item.open(o_item.b_opened) };
	this.select = function (n_id) { return this.a_index[n_id].select(); };
	this.mout   = function (n_id) { this.a_index[n_id].upstatus(true) };
	this.mover  = function (n_id) { this.a_index[n_id].upstatus() };

	this.a_children = [];
	for (var i = 0; i < a_items.length; i++)
		new tree_item(this, i);

	this.n_id = trees.length;
	trees[this.n_id] = this;
	var length = this.a_children.length;
	
	for (var i = 0; i < this.a_children.length; i++) {
		document.write(this.a_children[i].init());
		this.a_children[i].open();
		var root = this.a_children[i];
	    for (var j = 0; j < root.a_children.length; j++) {
	        var section = root.a_children[j];
	        for (var k = 0; k < section.a_children.length; k++) {
                if (section.a_children[k].a_config[0] == a_currentItem) {
                    this.o_root.o_selected = section.a_children[k];                 
    	            section.open();
	                break;
	            }
	        }
		}
	}
	if (this.o_selected != null) {
        get_element('i_txt' + this.o_root.n_id + '_' + this.o_selected.n_id).style.fontWeight = 'bold';	
	}
}
function tree_item (o_parent, n_order) {

	this.n_depth  = o_parent.n_depth + 1;
	this.a_config = o_parent.a_config[n_order + (this.n_depth ? 6 : 0)];
	if (!this.a_config) return;

	this.o_root    = o_parent.o_root;
	this.o_parent  = o_parent;
	this.n_order   = n_order;
	this.b_opened  = !this.n_depth;

	this.n_id = this.o_root.a_index.length;
	this.o_root.a_index[this.n_id] = this;
	o_parent.a_children[n_order] = this;

	this.a_children = [];
	for (var i = 0; i < this.a_config.length - 6; i++)
		new tree_item(this, i);

	this.get_icon = item_get_icon;
	this.get_folder_icon = item_get_folder_icon;
	this.open     = item_open;
	this.select   = item_select;
	this.init     = item_init;
	this.upstatus = item_upstatus;
	this.is_last  = function () { return this.n_order == this.o_parent.a_children.length - 1 };
}

function item_open (b_close) {
	var o_idiv = get_element('i_div' + this.o_root.n_id + '_' + this.n_id);
	if (!o_idiv) return;
	
	if (!o_idiv.innerHTML) {
		var a_children = [];
		for (var i = 0; i < this.a_children.length; i++)
			a_children[i]= this.a_children[i].init();
		o_idiv.innerHTML = a_children.join('');
//		alert("item-open");
	}
	o_idiv.style.display = (b_close ? 'none' : 'block');
	
	this.b_opened = !b_close;
	var o_jicon = document.images['j_img' + this.o_root.n_id + '_' + this.n_id],
		o_iicon = document.images['i_img' + this.o_root.n_id + '_' + this.n_id];
	if (o_jicon) o_jicon.src = this.get_icon(true);
	if (o_iicon) o_iicon.src = this.get_folder_icon();
	this.upstatus();
}

function item_select (b_deselect) {
    if  (this.a_config[3] == 'required' || this.a_config[3] == 'optional') {
	if (!b_deselect) {
		    var o_olditem = this.o_root.o_selected;
		    if (o_olditem) o_olditem.select(true);
		    this.o_root.o_selected = this;
//		    alert("Select Tree: " + this.a_config[0]);
		    submitForm("select", this.a_config[0]); 
   	    	this.upstatus();
	    } 
	    var o_iicon = document.images['i_img' + this.o_root.n_id + '_' + this.n_id];
	    if (o_iicon) o_iicon.src = this.get_folder_icon();
	    get_element('i_txt' + this.o_root.n_id + '_' + this.n_id).style.fontWeight = b_deselect ? 'normal' : 'bold';    
	}
    return false;
//	return Boolean(this.a_config[1]);
}

function item_upstatus (b_clear) {
	window.setTimeout('window.status="' + (b_clear ? '' : ' ['+ this.a_config[0] + ']' + (this.a_config[2] ? ' - ' + this.a_config[2] : '')) + '"', 10);
}

function item_init () {
	var a_offset = [],
		o_current_item = this.o_parent;
	for (var i = this.n_depth; i > 1; i--) {
		a_offset[i] = '<img src="' + this.o_root.a_tpl[o_current_item.is_last() ? 'icon_e' : 'icon_l'] + '" border="0" align="absbottom">';
		o_current_item = o_current_item.o_parent;
	}
	
	var shortText = this.a_config[1];
	if (this.a_children.length == 0 && this.a_config[3] != 'required' && this.a_config[3] != 'optional') {
	    shortText = '<FONT color="#c0c0c0">' + this.a_config[1] + '</FONT>'
	}
	var hint = ' ['+ this.a_config[0] + ']' + (this.a_config[2] ? ' - ' + this.a_config[2] : '');
	return '<table cellpadding="0" cellspacing="0" border="0"><tr><td>' + (this.n_depth ? a_offset.join('') + (this.a_children.length
		? '<a onmouseover="trees[' + this.o_root.n_id + '].mover(' + this.n_id + 
		')" onmouseout="trees[' + this.o_root.n_id + '].mout(' + this.n_id + 
		')" onclick="trees[' + this.o_root.n_id + '].toggle(' + this.n_id + 
		')"><img src="' + this.get_icon(true) + '" border="0" align="absbottom" name="j_img' + this.o_root.n_id + '_' + this.n_id + '"></a>'
		: '<img src="' + this.get_icon(true) + '" border="0" align="absbottom">') : '') 
		+ '<a href="' + this.a_config[1] + '" target="' + this.o_root.a_tpl['target'] + 
		'" onclick="return trees[' + this.o_root.n_id + '].select(' + this.n_id + 
		')" ondblclick="trees[' + this.o_root.n_id + '].toggle(' + this.n_id + 
		')" onmouseover="trees[' + this.o_root.n_id + '].mover(' + this.n_id + 
		')" onmouseout="trees[' + this.o_root.n_id + '].mout(' + this.n_id + ')" class="t' + this.o_root.n_id + 'i" id="i_txt' + this.o_root.n_id + '_' + this.n_id + 
		'"><img src="' + this.get_folder_icon() + '"  alt="' + hint + '" border="0" align="absbottom" name="i_img' + this.o_root.n_id + '_' + this.n_id + '" class="t' + this.o_root.n_id + 'im">' + 
		shortText + 
		'</a></td></tr></table>' + (this.a_children.length ? '<div id="i_div' + this.o_root.n_id + '_' + this.n_id + '" style="display:none"></div>' : '');
}

function item_get_icon (b_junction) {
	return this.o_root.a_tpl['icon_' + ((this.n_depth ? 0 : 32) + (this.a_children.length ? 16 : 0) + (this.a_children.length && this.b_opened ? 8 : 0) + (!b_junction && this.o_root.o_selected == this ? 4 : 0) + (b_junction ? 2 : 0) + (b_junction && this.is_last() ? 1 : 0))];
}

function item_get_folder_icon () {
    var iconName = 'icon_100';
    if (!this.n_depth) {
        iconName = 'icon_32';
    } else if (this.a_children.length) {
        if (this.b_opened) {
            iconName = 'icon_24';
         } else {
            iconName = 'icon_16';
        }
    } else {
        if (this.a_config[4] == '1' && (this.a_config[3] == 'required' || this.a_config[3] == 'optional')) {
            iconName = 'icon_110';
        } else if (this.a_config[3] == 'required') {
            if (this.o_root.o_selected != this) {
                iconName = 'icon_104';
            } else {
                iconName = 'icon_105';
            }
        } else if (this.a_config[3] == 'optional') {
            if (this.o_root.o_selected != this) {
                iconName = 'icon_102';
            } else {
                iconName = 'icon_103';
            }
        } else if (this.a_config[3] == 'disabled') {
            if (this.o_root.o_selected != this) {
                iconName = 'icon_100';
            } else {
                iconName = 'icon_101';
            }
        }
    }
	return this.o_root.a_tpl[iconName];
}

var trees = [];
get_element = document.all ?
	function (s_id) { return document.all[s_id] } :
	function (s_id) { return document.getElementById(s_id) };
