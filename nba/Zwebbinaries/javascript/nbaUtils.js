<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-659     NB-1301   Issues in entering data on indexing view  -->

var lastactiveField = 'actionMenuForm:lastactiveField';		//Hidden field on nbFile.jsp
var fieldWithFocus = 'actionMenuForm:lastFieldWithFocus';		//Hidden field on nbFile.jsp

//Add an event handler to each entry field on the form to handle the 'onfocusin' event.
//If, prior to being reloaded the form had a field with focus, restore focus to that field
function restoreFocus(aForm) {				 
	createFocusEvent(aForm);	//Add 'onfocusin' event handlers.  Event handlers are lost when the form is reloaded.

	var lastFieldWithFocus = parent.document.getElementById(fieldWithFocus);
	var lastField = parent.document.getElementById(lastactiveField);
	if (lastFieldWithFocus != null) {		 		
		var lastIdValue = lastFieldWithFocus.value;
		if (lastIdValue.length > 0) {
			if (document.getElementById(lastIdValue)) {
				var lastEle = document.getElementById(lastIdValue);		//If the form had a field with focus prior to being reloaded, restore focus to that field if it is still rendered.						 
				if (lastEle != null) {
					if (lastEle.isDisabled) {	//If the field is currently disabled, find the next entry field which is enabled.
						lastEle = findNextEditable(lastEle);
					}
					if (lastEle != null) {
						lastEle.focus();
						try {
							lastEle.select();	//Reselect it
						} catch (err) {	// ignore if select is not supported
						}
					}
				}			
			} else if (lastField != null) {	//The previous field with focus is no longer rendered. Look for the field immediately after the last field that was changed. 
				var lastIdValue = lastField.value;
				if (lastIdValue.length > 0) {
					if (document.getElementById(lastIdValue)) {
						var lastEle = document.getElementById(lastIdValue);						 
						if (lastEle != null) {
								lastEle = findNextEditable(lastEle);
							if (lastEle != null) {
								lastEle.focus();
								try {
									lastEle.select();	//Reselect it
								} catch (err) {	// ignore if select is not supported
								}
							}
						}
					}
				}		 		
			}			
		}
	}			 		

	lastFieldWithFocus.value = null;	//Clear the value of the hidden field.
	lastField.value = null;	//Clear the value of the hidden field.
}	

//Check each entry field on the form. Add a 'saveCurrentFocus' handler for the 'onfocusin' event.
function createFocusEvent(aForm) {				 
	var allElements = window.document.getElementById(aForm).elements;
	var length = allElements.length;
	var i;
	for (i=0; i < length; i++) {
	  	var anObject = allElements[i];
	  	if (!anObject.onFocus && isEditable(anObject) && anObject.id.length > 0) { 
	    		var onFocusValue = anObject.onfocusin;				    	
	    		if (onFocusValue == null ||  typeof(onFocusValue) == "undefined") {	//create new event
	    			anObject.onfocusin = function() {saveCurrentFocus(this);};
	    		} else {
	    			var onFocusString = onFocusValue.toString();	//modify event
	    			if (onFocusString.indexOf('saveCurrentFocus') < 0) {
	    				onFocusString = onFocusString.replace('{', '{\nsaveCurrentFocus(this);');
	    				anObject.onfocusin = onFocusString;
	    			}
				} 
	    	}
	  }
}

//Find find the next entry field which is enabled.
function findNextEditable(elem){				 
	var next = elem.nextSibling; 
	if (next != null) {				
		if (isEditable(next) && !next.isDisabled){						
			return next;
		} else {						
			return findNextEditable(next);
		}				
	} else {
		var nextParent = elem.parentNode.nextSibling;
		if (nextParent != null) {
			next = nextParent.firstChild;
			if (next != null) {	
				if (isEditable(next)){
					return next;
				} else {
					return findNextEditable(next);
				}
			}
		}
	}
	return null; 
}

//Determine if the field allows user entry.
function isEditable(elem) {
	var elemType = elem.type;
	return  elemType != null && (elem.isContentEditable || elemType == "text" || elemType == "textarea"  || elemType == "number"   || elemType == "checkbox" || elemType == "radio" || elemType == "select-one"  || elemType == "select-multiple");
}

//Save the id of the element in a hidden field on nbFile.jsp
function saveCurrentFocus(elem) {			
	if (elem != null) {					
		parent.setHiddenFieldValues(fieldWithFocus,elem.id);		
	}
} 		