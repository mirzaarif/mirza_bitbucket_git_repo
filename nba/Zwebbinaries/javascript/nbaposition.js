<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3356           7	  Memory to memory replicatiion -->		



// Resize the browser to the correct height and width depending on available real-estate on screen
// Browser is located on the left-half side of a 1280x1024 desktop.
// 659 pixels = (1280 / 2) + 19(scroll bar)
function sizeView() {

	prefWidth=662;  //SPR2965 //SPR3356
	//SPR2965 deleted code

	winHeight=window.screen.availHeight;
	//SPR2965 deleted code

	window.resizeTo(prefWidth,winHeight);  //SPR2965
	window.onresize = sizeView;  //NBA136
}

// Position the browser in the top right corner of the window and size it appropriately
function positionView() {

	sizeView();
	window.top.moveTo(0,0);   	 
}

// Resize the browser to the correct height and width depending on available real-estate on screen
// Browser should be resized to 1280 x WindowHeight, if the window width is greater than or equal
// to 1280.
// NBA136 New Method
function fullsizeView() {

	prefWidth=1280;
	winWidth=window.screen.availWidth;
	winHeight=window.screen.availHeight;

	if (winWidth < prefWidth) {
		prefWidth = winWidth;
	}

	window.resizeTo(prefWidth,winHeight);
	window.onresize = fullsizeView;
}

