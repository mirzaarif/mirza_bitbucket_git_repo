

function showImages(imageXML){
	ImgViewer.ImageViewer.showAlerts = true;
	ImgViewer.ImageViewer.OpenDocumentsByXML(imageXML);
}

//NBA350 New Function
function showImages(imageXML,url,roserver){ 
	if (ImgViewer.showAWDImageWindow){
		ImgViewer.showAWDImageWindow(imageXML,url);	
		if(roserver=="ROSServer"){	
			ImgViewer.loadImageByAWDInterface(imageXML); 
		}
	}
	else {
		ImgViewer.ImageViewer.showAlerts = true;
		ImgViewer.ImageViewer.OpenDocumentsByXML(imageXML);		
	}
}

function closeImages(imageXML){
	if (ImgViewer.showAWDImageWindow){	//NBA350
		//No close function on applet
	} else { //NBA350
		ImgViewer.ImageViewer.showAlerts = true;
		ImgViewer.ImageViewer.CloseDocumentsByXML(imageXML);		
	} //NBA350
}