/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

// Global Variables and configuration parameters for accelerator client tier

// -----------------------------------------------
// Default Screen Size and location parameters
// -----------------------------------------------

var FULL_SCREEN = "full";
var HALF_SCREEN_LEFT_PANEL_ON_LEFT = "half-left"; // top left position, left hand internal panel only
var HALF_SCREEN_RIGHT_PANEL_ON_LEFT = "half-right"; // top left position right hand internal panel only
var HALF_SCREEN_LEFT_PANEL_ON_RIGHT = "half-left-on-right"; // right aligned potion, left hand internal panel only
var HALF_SCREEN_RIGHT_PANEL_ON_RIGHT = "half-left-on-right"; // right aligned potion, right hand internal panel only
var CUSTOM_WIDTH = "CUSTOM WIDTH";
// use one of the above constants to set default window size and position
var defaultWindowOrientation = HALF_SCREEN_LEFT_PANEL_ON_LEFT;

// ------------------------------------------------------------------------------
// Height of title bar frame (across the top of application)
// ------------------------------------------------------------------------------
var titleHeight = 38;

// ------------------------------------------------------------------------------
// Height of Windows BAR/status bar forced by IE7 browser
// ------------------------------------------------------------------------------
//var IE7Offset = 28;
var IE7Offset = 5;
// ------------------------------------------------------------------------------
// Minimum width and height for content area (will provide scroll bars if 
// physical screen is too small to show
// ------------------------------------------------------------------------------
var minWidth = 1280;  //FNB016
var windowBarOverhead = 50;
var minHeight = 500;  //FNB016

var maxWidth = 1360;  //FNB016

// ------------------------------------------------------------------------------
// Support for flipping between half screen mode and full screen mode
// ------------------------------------------------------------------------------
var minWidthFullScreen = 1280;  //NBA213
var initialMinWidth = minWidth;  //NBA213
var initialWindowOrientation = defaultWindowOrientation;   //NBA213


// ------------------------------------------------------------------------------
// Show Button Bar (Restore, Left Only, Right Only buttons at top left of screen
// ------------------------------------------------------------------------------
var showWindowButtonBar=true;

// -----------------------------------------------
// Default Help Context to use when none specified
// -----------------------------------------------
var defaultHelpContext = "0";

// ------------------------------------------------------------------------------
// Force a "Restore" of application window when pop up window is shown to enable
// full dialog to be displayed
// ------------------------------------------------------------------------------
var resizeForPopup = true;

// -----------------------------------------------
// Enable right-click context menu
// -----------------------------------------------
var enableContextMenu = true;


// -----------------------------------------------
// Reports Integration
// -----------------------------------------------
var reportsWindowName = "accelReports";
var reportsURL = "http://cscappaus022/crystal/Enterprise10/websamples/en/database/logonform.csp";

// -----------------------------------------------
// Wait Panel Timeouts
// -----------------------------------------------
var LONG_DEFAULT_TIMEOUT = 180000; // 3 minutes
var CHECK_PERIOD = 500; // .5 seconds

// -----------------------------------------------
// Number of retries to attempt to load a dialog window
// -----------------------------------------------
var MAX_RETRIES = 1;
// -----------------------------------------------
// Wait period between dialog page load retry
// -----------------------------------------------
var LAUNCH_DIALOG_WAIT = 4000;

//-----------------------------------------------
//Wait period (in milliseconds) before refreshing the global and context menus
//-----------------------------------------------
var REFRESH_MENU_WAIT = 3000;  //SPRNBA-439

// ---------------------------------------------------------------------------------------------
// starting Z order index for popup windows (has to be high enough to "float" over desktop pane
// ---------------------------------------------------------------------------------------------
var INITIAL_WIN_Z_ORDER = 1000;

// -----------------------------------------------
// Help and product information Window Names
// -----------------------------------------------
var helpWindowName = "accelHelp";
var infoBrowserWindowName = "accelInfoBrowser";

// -----------------------------------------------
// Browser Hot Key constants used by script to repress the
// click events for these keys
// -----------------------------------------------
var F1_KEY = 112;
var BACKSPACE_KEY = 8;
var F5_KEY = 116;
var R_KEY = 82;
var N_KEY = 78;

// -------------------------------------------------------------------------
// Images path for reflexive questions (needs FULL path including appname)
// -------------------------------------------------------------------------
var rqapath = '/csa/images/rqa/';


// -------------------------------------------------------------------------
// Show Alert Exceptions for Javascript errors
// -------------------------------------------------------------------------
var showExceptions = 0;


// -------------------------------------------------------------------------
// Resets the default orientation of the browser
// -------------------------------------------------------------------------
//NBA213 New Method
function resetDefaultOrientation() {
	defaultWindowOrientation = initialWindowOrientation;
	minWidth = initialMinWidth;
	top.restoreWindow();
}
