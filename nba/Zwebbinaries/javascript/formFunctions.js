/*************************************************************************
 *
 * Copyright Notice (2003)
 * (c) CSC Financial Services Limited 1996-2003.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

/*
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA167            6      Route View Rewrite  -->
*/

// ---------------------------------------
// Declarations for forms processing
// ---------------------------------------
<!--
var otherThanSpaceKeyPressed = 1;

// Date Constants
var dtCh = "/";
var minYear = 1850;
var maxYear = 2500;
var count=0;
// Contact Numbers Constants
var phoneNumberDelimiters = "()- ";
var digitsInUSPhoneNumber = 10;
var iUSPhone ="This field must be a 10 digit U.S. phone number (like 415 555 1212). Please reenter it now.";

// Zip/Postal Code Constants
var defaultEmptyOK = false;
var digitsInZIPCode1 = 5;
var digitsInZIPCode2 = 9;
var ZIPCodeDelimiters = "-";
var iZIPCode = "This field must be a 5 or 9 digit U.S. ZIP Code";

// SSN Constants
var SSNDelimiters = "- ";
var digitsInSocialSecurityNumber = 9;
var iSSN = "Please enter a valid SSN/TIN.";
var result = true;

var error='';
//-------------------------------------------------
//E mail Validation
//--------------------------------------------------
function validEmail(formField,fieldLabel,required){
		if (required && !validRequired(formField,fieldLabel)){
			result = false;
		}
		if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) ){
			alert("Please enter a complete email address in the form: yourname@yourdomain.com");
			result = false;
		}
	   
	 	return result;

	}
	function checkForValidEmail(element){
		if(result == false){
			element.value = "";
			element.focus();
			result = true;
		}		
	}
	function isEmailAddr(email){

		  var result = false;
		  var theStr = new String(email);
		  var index = theStr.indexOf("@");
		  if (index > 0){
			    var pindex = theStr.indexOf(".",index);
			    if ((pindex > index+1) && (theStr.length > pindex+1)){
					result = true;
				}
		  }
		  return result;
	}

// ---------------------------------------------------
// Form and Field Validation and Formatting Functions
// ---------------------------------------------------

	function selectAll(frm) {
		try{
			for (var i = 0; i < frm.elements.length; i++) {
				if (frm.elements[i].type == "checkbox")
					frm.elements[i].checked = true;
			}
		} catch (er){
			reportException(er, "formFunctions - selectAll");
		}
	}
	
	function clearAll(frm) {
		try{
			for (var i = 0; i < frm.elements.length; i++) {
				if (frm.elements[i].type == "checkbox")
					frm.elements[i].checked = false;
			}
		} catch (er){
			reportException(er, "formFunctions - clearAll");
		}
		
	}
	
	//DESCRIPTION:
	//	This function handles the following:  
	//	
	//	1)	If a space is not allowed as the first character in the form element
	//		and the value of the form element is equal to "" (empty) and the key
	//		pressed was the space bar, cancel the space bar key pressed event.
	//		Otherwise, allow the key pressed event.
	//
	//REQUIRED INPUTS:
	//	firstCharDoesNotAllowSpace: 1=true, 0=false
	//
	function onFieldKeyPress(firstCharDoesNotAllowSpace) {
		try{
			var formElement = event.srcElement;
			if (firstCharDoesNotAllowSpace) {
				if (formElement.value == "") {
					if (event.keyCode == 32) { //If the spacebar was pressed
						event.returnValue = false; //Cancel the key press event
						otherThanSpaceKeyPressed = 0;
					}
				}
			}
		} catch (er){
			reportException(er, "formFunctions - onFieldKeyPress");
		}
	}
	
	//DESCRIPTION:
	//	This function handles the following:  
	//	
	//	1)	Check the form's required fields and enable or disable the form's
	//		submit and reset buttons accordingly.
	//
	//REQUIRED INPUTS:
	//	rfArray:	required fields array, see customerSearch.jsp  or logon.jsp for an example
	//
	function onFieldKeyUp(rfArray) {
		try{	
			if (otherThanSpaceKeyPressed) { //Only execute if a non-space character was typed as the first character.
				if (checkReqFields(rfArray)) {
					enableFormControls();
				} else {
					disableFormControls();
				}
		
				if (event.keyCode == 13) { //If the enter key was pressed
					if (event.srcElement.tagName == "SELECT" || event.srcElement.tagName == "TEXTAREA") {
						document.forms(0).submit.click();
					}
				}
			}
		
			otherThanSpaceKeyPressed = 1;
		} catch (er){
			reportException(er, "formFunctions - onFieldKeyUp");
		}
	}
	
	
	function enableFormControls() {
		try{
			if (document.getElementsByName("reset") (0) != null) {
				document.forms(0).reset.disabled = false;
			}
			document.forms(0).submit.disabled = false;
		} catch (er){
			reportException(er, "formFunctions - enableFormControls");
		}
	}
	
	function disableFormControls() {
		try{
			if (document.getElementsByName("reset") (0) != null) {
				document.forms(0).reset.disabled = true;
			}
			document.forms(0).submit.disabled = true;
		} catch (er){
			reportException(er, "formFunctions - disableFormControls");
		}
	}
	
	//*********************************************************
	//	This function restricts the user from entering more than
	//  the specified characters for a given textarea in a given 
	//  formField. Use this method along with onkeypress event.
	//**********************************************************
	function limitText(formField, maxChars) {
		var result = true;
		try{
			if (formField.value.length >= maxChars)
				result = false;
		
			if (window.event)
				window.event.returnValue = result;
		} catch (er){
			reportException(er, "formFunctions - limitText");
			result = false;
		}
		return result;
	}
	
	//*************************************************
	// This method checks for a valid number
	//************************************************* 
	function allDigits(str) {
		return inValidCharSet(str, "0123456789");
	}
	
	function inValidCharSet(str, charset) {
		try{
			var result = true;
			for (var i = 0; i < str.length; i++)
				if (charset.indexOf(str.substr(i, 1)) < 0) {
					result = false;
					break;
				}
			return result;
		} catch (er){
			reportException(er, "formFunctions - inValidCharSet");
			return false;
		}
	}
	
	//*************************************
	// Checks for empty field
	//*************************************
	function validRequired(formField, fieldLabel) {
		try{
			var result = true;
			if (formField.value.length == '0') {
				alert('Please enter a  '+fieldLabel + '.');
				formField.focus();
				result = false;
			}
			return result;
		} catch (er){
			reportException(er, "formFunctions - validRequired");
			return false;
		}
	}
	
	//***************************************************************************
	// Checks for a valid number (a string where all the characters are digits)
	//***************************************************************************
	function validNum(formField, fieldLabel, required) {
		try{
			var result = true;
			if (required && !validRequired(formField, fieldLabel))
				result = false;
			if (result) {
				if (!allDigits(formField.value)) {
					alert('Please enter a number for the "' + fieldLabel +'" field.'); formField.focus();
					result = false;
				}
			}
			return result;
		} catch (er){
			reportException(er, "formFunctions - validNum");
			return false;
		}
	}
	
	//*********************************
	// This method compares two dates
	//**********************************
	function compare(formSDate, formEDate) {
		try{
			var date = new Date();
			var sdate = new Date(formSDate.value);
			var edate = new Date(formEDate.value);
			if (edate < sdate) {
				alert("Expiration date must be greater than Effective date");
			}
		} catch (er){
			reportException(er, "formFunctions - compare");
		}
	}
	
	function isEmpty(s) {
		try {
			return ((s == null) || (s.length == 0));
		} catch (er){
			reportException(er, "formFunctions - isEmpty");
		}
	}
	
	//************************************************************************
	// The methods below are used in validating and masking the
	//  Zip,SSN and Phone numbers.
	// Removes all characters which appear in string bag from string s.
	//************************************************************************
	function stripCharsInBag(s, bag) {
		try{
			var i;
			var returnString = "";
			// Search through string's characters one by one.
			// If character is not in bag, append to returnString.
		
			for (i = 0; i < s.length; i++) {
				// Check that current character isn't whitespace.
				var c = s.charAt(i);
				if (bag.indexOf(c) == -1)
					returnString += c;
		
			}
			return returnString;
		} catch (er){
			reportException(er, "formFunctions - stripCharsInBag");
			return "";
		}
	}
	
	function isInteger(s) {
		try{
			var i;
		
			if (isEmpty(s))
				if (isInteger.arguments.length == 1)
					return defaultEmptyOK;
				else
					return (isInteger.arguments[1] == true);
		
			// Search through string's characters one by one
			// until we find a non-numeric character.
			// When we do, return false; if we don't, return true.
		
			for (i = 0; i < s.length; i++) {
				// Check that current character is number.
				var c = s.charAt(i);
		
				if (!isDigit(c))
					return false;
			}
		
			// All characters are numbers.
			return true;
		} catch (er){
			reportException(er, "formFunctions - isInteger");
			return false;
		}
	}
	
	function isDigit(c) {
		try {
			return ((c >= "0") && (c <= "9"));
		} catch (er){
			reportException(er, "formFunctions - isDigit");
		}
	}
	
	

	
	//************************************************************
	//
	//  Function:       formatAsMoney
	//
	//  Description:    Takes a numeric value and formats it as US$ currency.
	//
	//  Arguments:      val - value to be formatted
	//
	//  Returns:        "" (empty string) if sValue is an empty string
	//                  "$" + sValue - parsed and formatted currency       
	//
	//*******************************************************************
	function formatAsMoney(val) {
		try{
			var sValue = val +"";
			if (sValue.length == 0)
				return "$0.00";
			if (sValue.length > 0) {
				if (sValue.charAt(0) == "$") {
					sValue = sValue.substring(1, sValue.length);
				}
		
				var twoValues = sValue.split(".");
				var decimalValue = "00";
		
				//Check decimal value
				if (twoValues[1]) {
					if (twoValues[1].length == 1) {
						decimalValue = twoValues[1] + "0";
					} else {
						//It is two decimal value
						decimalValue = twoValues[1];
					}
				}
		
				var commaValues = twoValues[0].split(",");
				var thValue = "";
		
				for (var i = (commaValues.length - 1); i >= 0; i--) {
					if (commaValues[i].length > 0) {
						thValue = commaValues[i] + thValue;
					} else {
						if (i <= 0) {
							thValue = "0";
						}
					}
				} 
		
				//Convert to string
				var noCommas = thValue + '';
		
				//Add commas
				var withCommas = "";
				var count = 0;
				for (i = (noCommas.length - 1); i >= 0; i--) {
					if ((count == 3) && (i >= 0)) {
						withCommas = "," + withCommas;
						count = 0;
					}
					withCommas = noCommas.charAt(i) + withCommas;
					count++;
				}
		
				return ("$" + withCommas + "." + decimalValue);
		
			} //if(sValue.length > 0
		} catch (er){
			reportException(er, "formFunctions - formatAsMoney");
			return false;
		}
	}
	
	//************************************
	// Date Validation
	//*************************************
	// Declaring valid date character, minimum year and maximum year
	// Call isDate(formField) function to utilize this method
	//*************************************************************************
	function daysInFebruary(year) {
		try{
			// February has 29 days in any year evenly divisible by four,
			// EXCEPT for centurial years which are not also divisible by 400.
			return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
		} catch (er){
			reportException(er, "formFunctions - daysInFebruary");
			return false;
		}
	}
	
	function DaysArray(n) {
		try{
			for (var i = 1; i <= n; i++) {
				this[i] = 31;
				if (i == 4 || i == 6 || i == 9 || i == 11) {
					this[i] = 30;
				}
				if (i == 2) {
					this[i] = 29;
				}
			}
			return this;
		} catch (er){
			reportException(er, "formFunctions - DaysArray");
			return this;
		}
	}
	
	function isSlash(s, slash) {
		try{
			var result = false;
			for (i = 0; i < s.length; i++) {
				var c = s.charAt(i);
				if (slash.indexOf(c) == 0)
					result = true;
					
			}
			return result;
		} catch (er){
			reportException(er, "formFunctions - isSlash");
			return false;
		}
	}
	
	function isDate(formField) {
		try{
			var dtStr = formField.value;
			if (dtStr.length < 8 && dtStr != "") {
				//alert("Please enter a valid date");
				error = 'Please enter a valid date';
				dtStr = "";
				formField.value = dtStr;
				formField.select();
				formField.focus();
				return false;
			}
			var strYr = "";
			if (dtStr.length == 8) {
				var daysInMonth = DaysArray(12);
				var strMonth = dtStr.substring(0, 2);
				var strDay = dtStr.substring(2, 4);
				var strYear = dtStr.substring(4, 8);
				
				strYr = strYear;
				if (strDay.charAt(0) == "0" && strDay.length > 1) {
					strDay = strDay.substring(1);
				}
				if (strMonth.charAt(0) == "0" && strMonth.length > 1) {
					strMonth = strMonth.substring(1);
				}
				for (var i = 1; i <= 3; i++) {
					if (strYr.charAt(0) == "0" && strYr.length > 1)
						strYr = strYr.substring(1);
				}
		
				month =	parseInt(strMonth);
				day = parseInt(strDay);
				year = parseInt(strYr);
				if (strMonth.length < 1 || month < 1 || month > 12) {
					alert("Please enter a valid Date");
					dtStr = "";
					formField.value = dtStr;
					formField.select();
					formField.focus();
					return false;
				}
				if (strDay.length < 1
					|| day < 1
					|| day > 31
					|| (month == 2 && day > daysInFebruary(year))
					|| day > daysInMonth[month]) {
					alert("Please enter a valid Date");
					dtStr = "";
					formField.value = dtStr;
					formField.select();
					formField.focus();
					return false;
				}
				if (strYear.length != 4 || year == 0) {
					alert("Please enter a valid Date ");
					dtStr = "";
					formField.value = dtStr;
					formField.select();
					formField.focus();
					return false;
				}
				if (strMonth.length == 1) {
					strMonth = "0" + strMonth;
				}
				if (strDay.length == 1) {
					strDay = "0" + strDay;
				}
				dtStr = strMonth + dtCh + strDay + dtCh + strYr;
				formField.value = dtStr;
				return true;
			}
		} catch (er){
			reportException(er, "formFunctions - isDate");
			return false;
		}
	}
	
	
	//*************************************************************************
	// isUSPhoneNumber (STRING s [, BOOLEAN emptyOK])
	// 
	// isUSPhoneNumber returns true if string s is a valid U.S. Phone
	// Number.  Must be 10 digits.
	//
	// NOTE: Strip out any delimiters (spaces, hyphens, parentheses, etc.)
	// from string s before calling this function.
	//
	// For explanation of optional argument emptyOK,
	// see comments of function isInteger.
	//*******************************************************************************
	function isUSPhoneNumber(s) {
		try{
			if (isEmpty(s))
				if (isUSPhoneNumber.arguments.length == 1)
					return defaultEmptyOK;
				else
					return (isUSPhoneNumber.arguments[1] == true);
			return (isInteger(s) && s.length == digitsInUSPhoneNumber);
		} catch (er){
			reportException(er, "formFunctions - isUSPhoneNumber");
			return false;
		}

	}
	
	// takes USPhone, a string of 10 digits
	// and reformats as (123) 456-789
	function reformatUSPhone(USPhone) {
		try {
			return (reformat(USPhone, "(", 3, ") ", 3, "-", 4));
		} catch (er){
			reportException(er, "formFunctions - reformatUSPhone");
		}
		return "";
	}
	
	//****************************************************************
	//SSN Validation
	// isSSN (STRING s [, BOOLEAN emptyOK])
	// 
	// isSSN returns true if string s is a valid U.S. Social
	// Security Number.  Must be 9 digits.
	//
	// NOTE: Strip out any delimiters (spaces, hyphens, etc.)
	// from string s before calling this function.
	//
	// For explanation of optional argument emptyOK,
	// see comments of function isInteger.
	//*********************************************************************
	// takes SSN, a string of 9 digits
	// and reformats as 123-45-6789
	function reformatSSN(SSN) {
		try {
			return (reformat(SSN, "", 3, "-", 2, "-", 4));
		} catch (er){
			reportException(er, "formFunctions - reformatSSN");
		}
		return "";
	}
	
	function reformatTIN(SSN) {
		try {
			return (reformat(SSN, "", 2, "-", 7));
		} catch (er){
			reportException(er, "formFunctions - reformatTIN");
		}
		return "";
	}
	
	// Check that string theField.value is a valid SSN.
	//
	// For explanation of optional argument emptyOK,
	// see comments of function isInteger.
	function checkSSN(theField, emptyOK) {
		try{
			if (checkSSN.arguments.length == 1)
				emptyOK = defaultEmptyOK;
			if ((emptyOK == true) && (isEmpty(theField.value)))
				return true;
			else {
				var normalizedSSN = stripCharsInBag(theField.value, SSNDelimiters);
				if ((!isSSN(normalizedSSN, false)) || (normalizedSSN == '000000000')) {
					theField.focus();
					return warnInvalid(theField, iSSN);
				} else { // if you don't want to reformats as 123-456-7890, comment next line out
					theField.value = reformatSSN(normalizedSSN);
					return true;
				}
			}
		} catch (er){
			reportException(er, "formFunctions - checkSSN");
			return false;
		}
	}
	
	function checkTIN(theField, emptyOK) {
		try{
			if (checkTIN.arguments.length == 1)
				emptyOK = defaultEmptyOK;
			if ((emptyOK == true) && (isEmpty(theField.value)))
				return true;
			else {
				var normalizedSSN = stripCharsInBag(theField.value, SSNDelimiters);
				if ((!isSSN(normalizedSSN, false)) || (normalizedSSN == '000000000')) {
					theField.focus();
					return warnInvalid(theField, iSSN);
				} else { // if you don't want to reformats as 123-456-7890, comment next line out
					theField.value = reformatTIN(normalizedSSN);
					return true;
				}
			}
		} catch (er){
			reportException(er, "formFunctions - checkTIN");
			return false;
		}
	}
	
	function isSSN(s) {
		try{
			if (isEmpty(s))
				if (isSSN.arguments.length == 1)
					return defaultEmptyOK;
				else
					return (isSSN.arguments[1] == true);
			return (isInteger(s) && s.length == digitsInSocialSecurityNumber);
		} catch (er){
			reportException(er, "formFunctions - isSSN");
			return false;
		}
	}
	
	//*************************************************************
	//ZipCode Validation
	// isZIPCode (STRING s [, BOOLEAN emptyOK])
	// 
	// isZIPCode returns true if string s is a valid 
	// U.S. ZIP code.  Must be 5 or 9 digits only.
	//
	// NOTE: Strip out any delimiters (spaces, hyphens, etc.)
	// from string s before calling this function.  
	//
	// For explanation of optional argument emptyOK,
	// see comments of function isInteger.
	//***************************************************************
	function isZIPCode(s) {
		try{
			if (isEmpty(s))
				if (isZIPCode.arguments.length == 1)
					return defaultEmptyOK;
				else
					return (isZIPCode.arguments[1] == true);
			return (isInteger(s) && ((s.length == digitsInZIPCode1) || (s.length == digitsInZIPCode2)));
		} catch (er){
			reportException(er, "formFunctions - isZIPCode");
			return false;
		}
	}
	

	// takes ZIPString, a string of 5 or 9 digits;
	// if 9 digits, inserts separator hyphen
	function reformatZIPCode(ZIPString) {
		try{
			if (ZIPString.length == 5)
				return ZIPString;
			else
				return (reformat(ZIPString, "", 5, "-", 4));
		} catch (er){
			reportException(er, "formFunctions - reformatZIPCode");
			return false;
		}
	}
	
	//***********************************
	//This function returns current date
	//**********************************
	function getCurrentDate() {
		try{
			date = new Date();
			date.getDate() < 10 ? day = "0" + date.getDate().toString() : day = date.getDate().toString();
			date.getMonth() < 9 ? month =
				"0" + (date.getMonth() + 1).toString() : month = (date.getMonth() + 1).toString();
			return month + "/" + day + "/" + date.getYear().toString();
		} catch (er){
			reportException(er, "formFunctions - getCurrentDate");
			return false;
		}
	}
	

	//***********************************************************************
	// This method sets focus to given field in a given form.
	//********************************************************************
	function putFocus(formName, fieldName) {
		try{
			if (document.forms.length > 0) {
				document.forms[formName].elements[fieldName].focus();
			}
		} catch (er){
			reportException(er, "formFunctions - putFocus");
			return false;
		}
	}
	
	//************************************************************************
	// This function checks whether entered Character is Numeric value or not and
	//  exception to this is It allows users to enter "." and "-".
	// This method should be used along with onkeypress event.
	//*******************************************************************
	function CheckNum() {
		try{
			var key = window.event.keyCode;
			if (key > 44 && key < 58) {
				return;
			} else {
				window.event.returnValue = null;
			}
		} catch (er){
			reportException(er, "formFunctions - CheckNum");
			return false;
		}
	}
	
	//************************************************************************
	// This function checks whether entered Character is a positive integer value or not.
	// This method should be used along with onkeypress event.
	//*******************************************************************
	//NBA167 New Method
	function checkPositiveInteger() {
		try{
			var key = window.event.keyCode;
			if (key > 47 && key < 58) {
				return;
			} else {
				window.event.returnValue = null;
			}
		} catch (er){
			reportException(er, "formFunctions - checkPositiveInteger");
			return false;
		}
	}
	//************************************************************************
	// This function validates positive currency values 1,123.45
	// This method should be used along with onkeypress event.
	//*******************************************************************
	function checkPositiveMoney(field,evt) {
		try{
			
			var key = evt.keyCode;
			var currencyValue = field.value;
					
			if ((key == 46) || (key > 47 && key < 58)) {
     	    	if(key == 46){
     	    	  if(currencyValue.indexOf(".") > -1)
     	    		evt.returnValue = null;
     	    	}
     	    	
				if (currencyValue != "") {
					var checkVal = currencyValue.split(".");
					if (checkVal[1]) { 
						if ((checkVal[1].length == 2) ) {
						//	evt.returnValue = null;
						}
					}
				}
				return;
			} else {
			   	evt.returnValue = null;
			}

		} catch (er){
			reportException(er, "formFunctions - checkPositiveMoney");
			return false;
		}
	}
	

	//************************************************************************
	// This function validates alpha-numeric w/NO special chars (spaces allowed)
	// This method should be used along with onkeypress event.
	//*******************************************************************
	function checkAlphaNum( qaEvt ) {
		try{
			var evnt = qaEvt;
			
			if (qaEvt == null) 
				evnt = window.event;
		
			var key = evnt.keyCode;
			if ((key > 47 && key < 58) || (key > 64 && key < 91) || (key > 96 && key < 123) || key == 32) {
				return;
			} else {
				evnt.returnValue = null;
			}
		} catch (er){
			reportException(er, "formFunctions - checkAlphaNum");
		}
	}

	//************************************************************************
	// This function validates alpha-numeric w/NO special chars ( NO spaces)
	// This method should be used along with onkeypress event.
	//*******************************************************************
	function checkAlphaNumStrict( qaEvt ) {
		try{
			var evnt = qaEvt;
			
			if (qaEvt == null) 
				evnt = window.event;
		
			var key = evnt.keyCode;
			if ((key > 47 && key < 58) || (key > 64 && key < 91) || (key > 96 && key < 123)) {
				return;
			} else {
				evnt.returnValue = null;
			}
		} catch (er){
			reportException(er, "formFunctions - checkAlphaNum");
		}
	}

	
	//************************************************************************
	// This converts lower case alpha to upper case alpha	
	// This method should be used along with onkeypress event.
	//*******************************************************************
	function toUpperCase( qaEvt ) {
	  	try{

			var evnt = qaEvt;
			
			if (qaEvt == null) 
				evnt = window.event;

			var key = evnt.keyCode;
     	    if (key >96 && key < 123) {
     	    	evnt.keyCode = key -32;
     	    }
     	    
			return true;
		} catch (er){
		    reportException(er, "formFunctions - toUpperCase");
		}

	}
	
	
	//************************************************************************
	// This function sets the cookie with the given name and value
	//*******************************************************************
	function setCookie(name, value, expires, path, domain, secure) {
		try{
			var curCookie =
				name
					+ "="
					+ escape(value)
					+ ((expires) ? "; expires=" + expires.toGMTString() : "")
					+ ((path) ? "; path=" + path : "")
					+ ((domain) ? "; domain=" + domain : "")
					+ ((secure) ? "; secure" : "");
			document.cookie = curCookie;
		} catch (er){
			reportException(er, "formFunctions - setCookie");
		}
	}

	//************************************************************************
	// This function returns the cookie with the given name
	//*******************************************************************
	function getCookie(name) {
		try{
	  		var dc = document.cookie;
			var prefix = name + "=";
			var begin = dc.indexOf("; " + prefix);
			if (begin == -1) {
				begin = dc.indexOf(prefix);
				if (begin != 0) return null;
  	        } else
				begin += 2;
			  	var end = document.cookie.indexOf(";", begin);
			  	if (end == -1)
			    	end = dc.length;
			  	return unescape(dc.substring(begin + prefix.length, end));
	  	} catch (er) {
	  		reportException(er, "formFunctions - getCookie");
	  	}
	}


	//************************************************************************
	// This function rounds to a given decimal point 
	//   Example 1: value= 3.14159 decLength = 2, returns 3.14
	//	 Example 2: value= 3.496757 decLength = 2, returns 3.50
	//*******************************************************************
	function roundToDecimalPlace(value, decLength){
		try {
			var strValue;
			var splitValue;
			var lastDigit;
			var last2Digits;
			var newValue;
		
			strValue = value + "";
			var splitValue = strValue.split('.');
			if (splitValue[1]){
				if (splitValue[1].length > decLength) {
					lastDigit = splitValue[1].substring(decLength,decLength+1);
					if (lastDigit >= 5) {
						last2Digits = (splitValue[1].substring(decLength -2,decLength) - 0) + 1;
					} else {
						last2Digits = splitValue[1].substring(decLength -2, decLength);
					}
					newValue = splitValue[0] + '.' + splitValue[1].substring(0,decLength-2) + last2Digits;
				} else {
					newValue = value;
				}
			} else {
				newValue = value;
			}
					
			return newValue;
	  	} catch (er) {
	  		reportException(er, "formFunctions - roundToDecimalPlace");
	  	}
	}
	
	
		
	function trim(inputString) {
	/**
	 *	Removes leading and trailing spaces from the passed string. Also removes
	 *  consecutive spaces and replaces it with one space. If something besides
	 *  a string is passed in (null, custom object, etc.) then return the input.
	 *	INPUT object  = inputString
	 *	RETURN String
	 */
	
	   var retValue = "";
	   try
	   {
		   if (typeof inputString != "string") { return inputString; }
		   retValue = inputString;
		   var ch = retValue.substring(0, 1);
		   while (ch == " ") { 
		      // Check for spaces at the beginning of the string
		      retValue = retValue.substring(1, retValue.length);
		      ch = retValue.substring(0, 1);
		   }
		   ch = retValue.substring(retValue.length-1, retValue.length);
		   while (ch == " ") { 
		      // Check for spaces at the end of the string
		      retValue = retValue.substring(0, retValue.length-1);
		      ch = retValue.substring(retValue.length-1, retValue.length);
		   }
		   while (retValue.indexOf("  ") != -1) { 
		      // Note that there are two spaces in the string - look for multiple spaces within the string
		      retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length);
		      // Again, there are two spaces in each of the strings
		   }
	   } catch (er) {
	  		reportException(er, "formFunctions - trim");
	  		return inputString;
       }		
	   return retValue; // Return the trimmed string back to the user
	} // Ends the "trim" function
	
	
	//checks for first digit in year it allows 1 0r 2 in first position
	function checkFirstDigitInYear(field)
    {
    try
    	{
	        var yrvalue=field.value;
	        var key=window.event.keyCode;
	        if((yrvalue.length==4) && (key>50 ||key<49))
	        {
	           	window.event.returnValue=null;
	        }else
	        {
	           return;
	        }
	    } catch (er) {
	  		reportException(er, "formFunctions - checkFirstDigitInYear");
	  	}
    }

	//********************************************************************
	// This method masks for percentage values
	//********************************************************************
	function maskPercent(field) {
		try{
			var val = field.value;
			if (val != "") {
				if (val.indexOf("%") < 0) {
					field.value = val + "%";
				}
			}
		} catch (er){
			reportException(er, "formFunctions - maskPercent");
		}

		
	}

	//********************************************************************
	// This method returns the given date is valid or not
	//********************************************************************
	function isValidDate (input)
	{  
		try{
			input = input+"";			
			input = input.split("/").join("");
			if( input.length != 8 ){
				return false;
			}
			var month = input.substring(0,2);
			var day  = input.substring(2,4);
			var year  = input.substring(4,8);	
			
			if( month.substring(0, 1) == "0" ){		 
				month = month.substring(1,2);
			}
			if( day.substring(0, 1) == "0" ){		 
				day = day.substring(1,2);
			}	
			if((checkDate(month,day,year))){
				return true;
			}else{
				return false;
			}
	    } catch (er) {
	  		reportException(er, "formFunctions - isValidDate");
	  		return false;
	  	}	   	
	}
	
	function checkDate(month,day,year)
	{
		try{	
			var daysInMonth = makeArray(12);
			daysInMonth[1] = 31;
			daysInMonth[2] = 29;   // must programmatically check this
			daysInMonth[3] = 31;
			daysInMonth[4] = 30;
			daysInMonth[5] = 31;
			daysInMonth[6] = 30;
			daysInMonth[7] = 31;
			daysInMonth[8] = 31;
			daysInMonth[9] = 30;
			daysInMonth[10] = 31;
			daysInMonth[11] = 30;
			daysInMonth[12] = 31;
		
			if (! (isYear(year, false) && isMonth(month, false) && isDay(day, false))) return false;
		
		    var intYear = parseInt(year);
		    var intMonth = parseInt(month);
		    var intDay = parseInt(day);
		
		    // catch invalid days, except for February
		    if (intDay > daysInMonth[intMonth]) return false; 
		
		    if ((intMonth == 2) && (intDay > daysInFebruary(intYear))) return false;			
		    return true;
	    } catch (er) {
	  		reportException(er, "formFunctions - checkDate");
	  		return false;
	  	}
	}
	
	
	function makeArray(n) {
		try{
		   for (var i = 1; i <= n; i++) {
		      this[i] = 0
		   } 
		   return this
	    } catch (er) {
	  		reportException(er, "formFunctions - makeArray");
	  		return false;
	  	}
	}
	//********************************************************************
	// This method returns the given month is valid or not
	//********************************************************************
	function isMonth (s) {
		try {
			return isIntegerInRange (s, 1, 12);
	    } catch (er) {
	  		reportException(er, "formFunctions - isMonth");
	  	}
	  	return false;
	}

	//********************************************************************
	// This method returns the given year is valid or not
	//********************************************************************
	function isYear (s) {
		try {
	    	return ((s.length == 4));
	    } catch (er) {
	  		reportException(er, "formFunctions - isYear");
	  	}
	  	return false;
	}

	//********************************************************************
	// This method returns the given day is valid or not
	//********************************************************************
	function isDay (s) {
		try {
		    return isIntegerInRange (s, 1, 31);
	    } catch (er) {
	  		reportException(er, "formFunctions - isDay");
	  	}
	  	return false;
	}
	
	function isIntegerInRange (s, a, b) {
		try{
		    var num = parseInt (s);
		    return ((num >= a) && (num <= b));
	    } catch (er) {
	  		reportException(er, "formFunctions - isIntegerInRange");
	  		return false;
	  	}
	}
	
	function trimSpecificChar(inputString, charToTrim){
		try{
		   // Removes leading and trailing specific char from the passed string.
		   // a string is passed in (null, custom object, etc.) then return the input.
		   if (typeof inputString != "string") { return inputString; }
		   var retValue = inputString;
		   var ch = retValue.substring(0, 1);
		   while (ch == charToTrim) { // Check for spaces at the beginning of the string
		      retValue = retValue.substring(1, retValue.length);
		      ch = retValue.substring(0, 1);
		   }
		   ch = retValue.substring(retValue.length-1, retValue.length);
		   while (ch == charToTrim) { // Check for spaces at the end of the string
		      retValue = retValue.substring(0, retValue.length-1);
		      ch = retValue.substring(retValue.length-1, retValue.length);
		   }
			return retValue;
	    } catch (er) {
	  		reportException(er, "formFunctions - trimSpecificChar");
	  		return inputString;
	  	}
	}	 
	
	//ValidateSSN  - check that the current answer is a valid SSN/TIN
	function validateSSN( SSNToValidate  ){
 		if( SSNToValidate.length <= 9 && isInteger(SSNToValidate) ){ 		   
		    return true;
		}else{
	       	parent.showMsgBox("Value entered is not a valid SSN or TIN", "Validation Error" );
	       	return false;
	   	}   
	}

-->