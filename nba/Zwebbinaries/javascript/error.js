/*************************************************************************
 *
 * Copyright Notice (2003)
 * (c) CSC Financial Services Limited 1996-2003.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

// indicator that enables or disables the visibility of Exception alerts


	/** standard Exception Reporting function **/
	function reportException(exception, functionname){
		if(top.showExceptions == 1){
			if(functionname == null){
				functionname = "";
			}
			if(exception != null){
				var name = exception.name;
				var description = exception.description;
				if( (name!= null && name.indexOf("Permission denied") > -1) ||
					(description != null && description.indexOf("Permission denied") > -1)){
					
					// do nothing ALWAYS ignore permission denied exceptions as they sometimes
					// occur during refresh but have no impact of usability
					
				} else {
					var number = exception.number;	
					if(top.showExceptions == 1){
						alert("An Unhandled Exception has occured in Presentation Tier.\n" +
								"In File [" + exception.fileName + "]\n" +
								"at line [" + exception.lineNumber + "]\n" +
								"Provided text Detail [" + functionname + "]\n" +
								"Error Name:" + exception.name + "\n" +
								"Error Description:" + exception.description + "\n" +
								"Error Message:" + exception.message + "\n" +
								"Error Facility Code Number (IE Only):" + (exception.number>>16 & 0x1FFFF) + "]\n\n" +
								"Error Number:" + exception.number + "(for IE it is [" + (exception.number & 0xFFFF) + "])\n\n" +
								"Please contact your system administrator.");
					} 
				}
			} else {
				if(top.showExceptions == 1){
					alert("An Unhandled Exception has occured in Presentation Tier at function [" + functionname + "]\nPlease contact your system administrator.");
				}
			}
		}
	}
	