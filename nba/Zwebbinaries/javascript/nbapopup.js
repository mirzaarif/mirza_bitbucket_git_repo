<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3073 			6		The "add comments" dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- SPR3456 			8		Error Message Popup for Missing or Invalid is Not Visible When Special Instruction Type is Not Selected -->
<!-- NBA267 			8		nbA IE 7.0 Certification -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- FNB004 				NB-1101	PHI -->
<!-- NBA244				NB-1401 Combine Contract Status and Contract Summary View -->
<!-- NBA337				NB-1401 Email Enhancement & support email attachment -->
<!-- NBA356       		NB-1501 Comments Floating View -->
<!-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View -->
<!-- NBA406			NB-1601   Rich Text Ad-Hoc Underwriter Emails Business Requirement -->

var APP_POPUP_FRAMESET = "/nba/applicationPopupFrameset.html"; //NBA356

function launchPopup(popupName, popupURL, prefWidth, prefHeight) {
	winWidth = window.screen.availWidth;	
	winHeight = window.screen.availHeight;
    // begin NBA406
	if(popupName == 'CommentPopUp')
	{
		prefLeft = winWidth/2;
		prefTop = 0;
	}
	else{
		prefLeft = winWidth/2 - prefWidth/2;
		prefTop = winHeight/2 - prefHeight/2;
	}
	//end NBA406
	return window.open(popupURL, popupName,'width=' + prefWidth + ',height=' + prefHeight + ',left=' + prefLeft + ',top=' + prefTop +',toolbar=no,menubar=no,status=no, resizable=yes,scrollbars=no');//NBA267
}
//NBA244 New Method
function launchPopupWithScrollBar(popupName, popupURL, prefWidth, prefHeight) {
	winWidth = window.screen.availWidth;	
	winHeight = window.screen.availHeight;

	prefLeft = winWidth/2;
	prefTop = 0;
	return window.open(popupURL, popupName,'width=' + prefWidth + ',height=' + prefHeight + ',left=' + prefLeft + ',top=' + prefTop +',toolbar=no,menubar=no,status=no, resizable=yes,scrollbars=yes');
}
function closePopup(popup){
	if (popup && !popup.closed) {
		popup.close();
	}
}

function launchModalPopup(popupSource, prefHeight, prefWidth) {
	window.showModalDialog(popupSource, '','dialogHeight:' + prefHeight + ';dialogWidth:' + prefWidth + ';status:no;help:no');
	
}
//SPRNBA-306 code deleted
//SPR3073 New Function
function launchCommentPopUp(commentType) {
	//begin SPRNBA-306
	try {
		//begin NBA356
		if (top.location.pathname == APP_POPUP_FRAMESET) {
			top.opener.top.mainContentFrame.contentRightFrame.nbaContextMenu.launchComment(commentType);
		} else if (top.mainContentFrame.contentRightFrame.nbaContextMenu) {
		//end NBA356
			top.mainContentFrame.contentRightFrame.nbaContextMenu.launchComment(commentType);
		}
	} catch (err) {
		top.reportException(err, "Unable to launch comment popup.");
		alert('Unable to launch comment popup. ' + err);
	}
	//end SPRNBA-306
	return false;
}
/*
*  Resizes the Commentspopup if tab is switched to 
*  Phone tab or General Instruction tab
*/
// NBA337 New Function
function resizeCommentPopUp()	{
	top.resizeTo(650,375);
}

//SPRNBA-306 code deleted

var commentView;// NBA356
//NBA356 New Function
function launchCommentView(){
	winWidth = window.screen.availWidth;	
	winHeight = window.screen.availHeight-40;
	prefWidth = 650;
	prefTop = 0;
	prefLeft = winWidth-prefWidth-20;
		
	if (!commentView) {
		commentView =window.open('' + contextpath + '/applicationPopupFrameset.html?popup=' + contextpath + '/uw/file/nbaCommentsOverview.faces','CommentsOverview','width=' + prefWidth + ',height=' + winHeight + ',left=' + prefLeft + ',top=' + prefTop +',toolbar=no,menubar=no,status=no, resizable=no,scrollbars=no');
	} else if (commentView.closed) {
		commentView =window.open('' + contextpath + '/applicationPopupFrameset.html?popup=' + contextpath + '/uw/file/nbaCommentsOverview.faces','CommentsOverview','width=' + prefWidth + ',height=' + winHeight + ',left=' + prefLeft + ',top=' + prefTop +',toolbar=no,menubar=no,status=no, resizable=no,scrollbars=no');
	}
	commentView.focus();
	top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView = commentView;
}

//NBA356 New Function
function setAppFrameExt() {
	if(top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView != null){
		top.mainContentFrame.contentRightFrame.nbaContextMenu.refreshAppFrameExtn = true;
	}
}

//NBA356 New Function
function refreshAppFrameExt() {
	if(top.mainContentFrame.contentRightFrame.nbaContextMenu.refreshAppFrameExtn){
		if(!top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView.closed){
			top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView.location.href = top.mainContentFrame.contentRightFrame.nbaContextMenu.commentView.location.href;
		}
		top.mainContentFrame.contentRightFrame.nbaContextMenu.refreshAppFrameExtn = false;
	}
}