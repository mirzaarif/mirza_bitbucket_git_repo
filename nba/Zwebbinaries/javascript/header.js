var helpBasePath = "";
var context = "/nba/";
if(window.document.location.port != ""){
	helpBasePath = window.document.location.protocol + "//" + window.document.location.hostname +":" + window.document.location.port + context;
} else {
	helpBasePath = window.document.location.protocol + "//" + window.document.location.hostname + context;
}


var helpfile = "";
var viewName;

if (viewName != null) {
  helpfile = viewName.toLowerCase(); 
 } else {
 	helpfile = "nba";
 }
 
var helptarget = helpBasePath + "docs/" + helpfile + "help.htm";
var processGuidetarget = helpBasePath + "docs/nbaBusinessGuide.htm"; //NBA128


function openFAQ()
{
	var path = helpBasePath + "docs/nbafaq.htm"; //NBA122
    window.open(path,'FAQ','status=no,resizable=yes,scrollbars=yes,width=600,height=400'); //NBA122
}
function openHelp()
{
	var path = helptarget; //NBA122 
   	window.open(path ,'Help','status=no,resizable=yes,scrollbars=yes,width=600,height=400'); //NBA122
}

function openAbout()
{
	var path = "nbaaboutdialog.jsp"; //NBA122  	
	openDialog(path , "400","630", ""); //SPR2380 NBA122 SPR2829
}

//SPR2679 new method
//This method closes the open Image viewer then calls logoff jsp.
function performLogOff()
{ 
	closeImage();
	//begin NBA122
	var path = appBasePath + "logoff.jsp";
	var target = "_top";	
	window.open(path, target);
	//end NBA122
}

//NBA128 New Method
function openProcessGuide()
{
	var path = processGuidetarget; //NBA122   
   	window.open(path ,'Guide','status=no,resizable=yes,scrollbars=yes,width=600,height=400'); //NBA122
}