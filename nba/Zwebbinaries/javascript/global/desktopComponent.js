/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

document.onkeydown = cancelBack; 
document.onhelp = helpExit; 

	function helpExit(){
		window.event.cancelBubble = false;
		window.event.keyCode = 0;
		window.event.returnValue = false;
		return false;
	}

	function cancelBack(event){
		if(event == null){
			event = window.event;
		}
		// keycode constants
		var F1_KEY = 112;
		var BACKSPACE_KEY = 8;
		var F5_KEY = 116;
		var F11_KEY = 122;
		var R_KEY = 82;
		var N_KEY = 78;

		if(top.inProgress){
			event.cancelBubble = false;
			event.keyCode = 0;
			event.returnValue = false;
		} else {		
			// first check for F1 and intercept HELP
			if (window.event.keyCode == F1_KEY){
				top.showAccelHelp(top.defaultHelpContext);
				event.cancelBubble = false;
				event.keyCode = 0;
				event.returnValue = false;
			} else {
				// check for either ALT or (BACKSPACE and NOT edit field) or ctrl-N or ctrl-R or F5
				if ( event.altKey || 
					(event.keyCode == BACKSPACE_KEY && (event.srcElement.type != 'text' && event.srcElement.type != 'textarea' && event.srcElement.type != 'password')) || 
				    (event.ctrlKey && ( event.keyCode == N_KEY || event.keyCode == R_KEY)) || 
				    (event.keyCode == F5_KEY) || (event.keyCode == F11_KEY)) {
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
		 	 	}
	
				// check for ctrl-R, ctrl-N and F5 in edit fields	 	 	
		 	 	if ((event.srcElement.type == 'text' || event.srcElement.type == 'textarea' || event.srcElement.type == 'password') && 
				    (event.ctrlKey && (event.keyCode == N_KEY || event.keyCode == R_KEY)) ||
				    (event.keyCode == F5_KEY)|| (event.keyCode == F11_KEY)){
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
				}
			}
		}
	}
	
	function toggleMenu(item){
		if(utilMenu != null){
			if(utilMenu.style.visibility=='visible'){
				utilMenu.style.visibility='hidden';
			} else {
				if(item != null){
					leftPos = item.offsetLeft;
					topPos = item.offsetHeight;
					try{
						if(item.parentElement != null){
							menuLeftPos = item.parentElement.offsetLeft;
							if(menuLeftPos != null){
								leftPos = leftPos + menuLeftPos;
							}
						}
					} catch(err){
					}
					if((leftPos + utilMenu.clientWidth) > (top.document.body.clientWidth - 29)){
						leftPos = top.document.body.clientWidth - utilMenu.clientWidth - 10;
					}
					utilMenu.style.left = leftPos;
					utilMenu.style.top = topPos + 5;
				}
				utilMenu.style.visibility='visible';
			}
		}
		return false;
	}
	
	var menuTimer=null;
	var timerDelay=300;

	function timer(offon) {
		try {
			if (offon == 0)	{
				menuTimer = setTimeout("utilMenu.style.visibility='hidden'", timerDelay);
		    }
			if (offon ==1)	{
				clearTimeout(menuTimer);
			}
		} catch(er){
			reportException(er, "menus-timer");
		}
	}

	function onSubMenuOver(col, link){
		timer(1);
		col.className = "menuItemSelected";
		link.className = "menuItemSelected";
	}

	function onSubMenuOut(col, link){
		timer(1);			
		col.className = "menuItem";
		link.className = "menuItem";
	}
	
	var callTime = 0;

	function updateTime(){
		callTime++;
		try{
			var today = new Date()
			element = top.titleFrame.document.getElementById("CurrentDate");
			if(element == null){
				element = document.getElementById("CurrentDate");
			}
			if(element != null){
				element.innerHTML = today.toLocaleString() + "&nbsp;";
			}
			element = top.titleFrame.document.getElementById("CallTime");
			if(element == null){
				element = document.getElementById("CallTime");
			}
			if(element != null){
				minutes = 0;
				seconds = callTime;
				if(callTime > 60){
					minutes = callTime / 60;
					seconds = callTime % 60;
					minutes = parseInt(minutes)
				}
				if(minutes < 10){
					minutes = "0" + minutes;
				}
				if(seconds < 10){
					seconds = "0" + seconds;
				}
				element.innerHTML = minutes + ":" + seconds  + "&nbsp;";	
			}
		}catch(err){
		}
		dateTime = setTimeout("updateTime();", 1000);
	}
	
	var dateTime = setTimeout("updateTime();", 1000);
	//top.setContentAreaSize();
	
	
	function toggleHelpMenu(id){
		var helpFrame = top.document.all.helpMenuFrame;
		if(helpFrame.style.visibility == 'visible'){
			top.document.all.helpMenuFrame.style.visibility = 'hidden';
		} else {
			buttonBar = document.getElementById(id);
			leftPos = buttonBar.offsetLeft;
			topPos = buttonBar.offsetHeight;
			try{
				if(buttonBar.parentElement != null){
					menuLeftPos = buttonBar.parentElement.offsetLeft;
					if(menuLeftPos != null){
						leftPos = leftPos + menuLeftPos;
					}
				}
			} catch(err){
			}
			if((leftPos + top.document.all.helpMenuFrame.clientWidth) > (top.document.body.clientWidth - 29)){
				leftPos = top.document.body.clientWidth - top.document.all.helpMenuFrame.clientWidth - 10;
			}
			helpFrame.style.left = leftPos;
			helpFrame.style.top = topPos + 5;
			top.document.all.helpMenuFrame.style.visibility='visible';
			top.document.all.helpMenuFrame.align = 'right';
		}
		return false;
	}			
	

top.setContentAreaSize();	