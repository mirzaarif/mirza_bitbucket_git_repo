/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/
 <!-- CHANGE LOG -->
 <!-- Audit Number   Version   Change Description -->
 <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->

document.onkeydown = cancelBack; 
document.onhelp = helpExit; 

//function to disable the context menu
	document.oncontextmenu = new Function("if (!top.enableContextMenu) { return false; }");

	function helpExit(){
		window.event.cancelBubble = false;
		window.event.keyCode = 0;
		window.event.returnValue = false;
		return false;
	}

	function cancelBack(event){
		if(event == null){
			event = window.event;
		}
		// keycode constants
		var F1_KEY = 112;
		var BACKSPACE_KEY = 8;
		var F5_KEY = 116;
		var F11_KEY = 122;
		var R_KEY = 82;
		var N_KEY = 78;

		if(top.inProgress){
			event.cancelBubble = false;
			event.keyCode = 0;
			event.returnValue = false;
		} else {		
			// first check for F1 and intercept HELP
			if (event.keyCode == F1_KEY){
				top.showAccelHelp(top.defaultHelpContext);
				event.cancelBubble = false;
				event.keyCode = 0;
				event.returnValue = false;
			} else {
				// check for either ALT or (BACKSPACE and NOT edit field) or ctrl-N or ctrl-R or F5
				if ( event.altKey || 
					(event.keyCode == BACKSPACE_KEY && (event.srcElement.type != 'text' && event.srcElement.type != 'textarea' && event.srcElement.type != 'password')) || 
				    (event.ctrlKey && ( event.keyCode == N_KEY || event.keyCode == R_KEY)) || 
				    (event.keyCode == F5_KEY) || (event.keyCode == F11_KEY)) {
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
		 	 	}
	
				// check for ctrl-R, ctrl-N and F5 in edit fields	 	 	
		 	 	if ((event.srcElement.type == 'text' || event.srcElement.type == 'textarea' || event.srcElement.type == 'password') && 
				    (event.ctrlKey && (event.keyCode == N_KEY || event.keyCode == R_KEY)) ||
				    (event.keyCode == F5_KEY) || (event.keyCode == F11_KEY)){
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
				}
			}
		}
	}

	function showWindow(){
	}

	function hideWindow(){
		try{
			alert("hideWindow");
			var element = top.document.getElementById('Win' + myWindowNumber);
			if(element != null){
				element.style.left = -1000;
				element.style.top = 0;
			}
		}catch(err){
		}
	}
 
/*
 * Initializes a popup window, by resizing the dialog to that specified in the
 * width and height local variables, and sets the window title bar to the
 * value in the page title
 */
function popupInit(){
	// set the pop up window title bar
	var myWindowNumber = parent.name.substring(4);
	var titleElement = parent.document.getElementById("Title" + myWindowNumber);
	var docTitle = document.documentElement.getElementsByTagName("Title")[0].innerHTML;
	
	if(top.resizeForPopup == true){
		top.restoreWindow();
	}
	
	if(titleElement != null){
		try{
			if(window.titleName != null && window.titleName != ""){
				titleElement.innerHTML = titleName;
			} else {
				titleElement.innerHTML = docTitle;
			}
		} catch(titleErr){
			titleElement.innerHTML = docTitle;
		}
	}
	
		try{

			// now resize the popup elements
			var padHeight = 29;
			var padWidth = 2;
			
			var widthToUse = width - 0 + padWidth;
			var heightToUse = height - 0 + padHeight;
			var targetFrameWidth = width;
			var targetFrameHeight = height-2;
			var centerBarWidth = widthToUse;
			var targetLeft = ((screen.availWidth-20) / 2) - (widthToUse / 2) - (padWidth /2);
			var targetTop = (screen.availHeight /2) - (heightToUse /2) - (padHeight /2);
			var element = top.document.getElementById('Win' + myWindowNumber);
			if(element != null){
				element.style.left = targetLeft;
				element.style.top = targetTop;
				element.style.width = widthToUse;
				element.style.height = heightToUse;
			}
			element = top.document.getElementById('FWin' + myWindowNumber);
			if(element != null){
				element.width = widthToUse;
				element.height = heightToUse;
				var targetFrame = element.contentWindow.document.getElementsByTagName("iframe")[0];
				var barElement = element.contentWindow.document.getElementById("centerBar" + myWindowNumber);
				if(barElement != null){
					barElement.style.width = centerBarWidth;
				}			
				if(targetFrame != null){
					targetFrame.width = targetFrameWidth;
					targetFrame.height = targetFrameHeight;
				}
			}
		} catch (exp){
			top.reportException(err, "popupInit for dialog inner - " + window.document.title);
		}
		try{
			if (parent.name.indexOf("FWin") < 0 ){
				if(top.newWindow.document.forms[0] != null){
					top.newWindow.document.forms[0].target = '_self';
				}
			}
			if (document.getElementById("Messages") != null){
			    //Begin SPR3356	
		       var innerText=document.getElementById("Messages").innerHTML;
		       innerText=top.getInnerHTML(innerText);		    
		        document.getElementById("Messages").innerHTML= innerText;  		   
		       //End SPR3356 
				if (document.getElementById("Messages").innerHTML != ""){
					top.showWindow(top.basePath,'faces/error.jsp', this);
				}
			}
		} catch (err){
			top.reportException(err, "popupInit for dialog outer - " + window.document.title);
		}
	
	try {
		if (myWindowNumber == top.currentWindow){
			window.focus();
			top.windowLaunching = false;
		}
	}catch (err){
	}
	top.hideWait();
}