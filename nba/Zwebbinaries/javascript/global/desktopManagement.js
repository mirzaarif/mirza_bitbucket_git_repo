/*************************************************************************
 *
 * Copyright Notice (2003)
 * (c) CSC Financial Services Limited 1996-2003.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

// Functions Related to Desktop Management

// CHANGE LOG 
// Audit Number   Version   Change Description
// NBA213         7         Unified User Interface
// ACEL3241                 Parties: Change Insured function removes original insured from the claim entirely.
// SPRNBA-439     NB-1101   Receive this message in nbA - "Please wait initializing menu...."
// SPRNBA-436          NB-1101  Page scrolling with screen resolution other than 1280 x 1024
// NBA341		  NB-1401	Internet Explorer 11 Certification	


// ----------------------------------
// Globals
// ----------------------------------
var lastLaunchedWindow = '';	// last launched external system window
var customWindow = '';			// the window number of the window that forced a custom width change

// ------------------------------------------------------------------------------
// Number of Refreshes currently in progress (reference count)
// ------------------------------------------------------------------------------
var numRrefreshes = 0;

var currentOrientation = top.FULL_SCREEN;

var captureFunctionScriptContent = 
		'if ((window.event.altKey) || ((window.event.keyCode == 8) && ' +
		'	(window.event.srcElement.type != \'text\' &&' +
		'    window.event.srcElement.type != \'textarea\' &&' +
		'    window.event.srcElement.type != \'password\')) || ' +
		'    ((window.event.ctrlKey) && ((window.event.keyCode == 78) || (window.event.keyCode == 82)) ) ||' +
 		'	(window.event.keyCode == 116) || (event.keyCode == 122) ) {' +
		'				if (window.event.keyCode == 116) top.refreshDT();' +
		'				window.event.cancelBubble = false;' +
		'				window.event.keyCode = 0;' +
		'				window.event.returnValue = false;' +
 	 	'}' +
 	 	'if ((window.event.srcElement.type == \'text\' ||' +
		'    window.event.srcElement.type == \'textarea\' ||' +
		'    window.event.srcElement.type == \'password\') && ((window.event.ctrlKey && (window.event.keyCode == 78 || window.event.keyCode == 82)) ||' +
 		'	 window.event.keyCode == 116) || (event.keyCode == 122)){' +
 		'		if (window.event.keyCode == 116) top.refreshDT();' +
		'		window.event.cancelBubble = false;' +
		'		window.event.keyCode = 0;' +
		'		window.event.returnValue = false;' +
		'};';

var onkeyCaptureFunctionScript = ' onKeyDown="' + captureFunctionScriptContent + '" ';

var documentKeyCaptureFunctionScript = '<SCRIPT> document.onkeydown = cancelBack;\n' +
		'function cancelBack(){' + captureFunctionScriptContent + '}</SCRIPT>';

	function initApplicationWindowSize(){
		try{
			top.document.all.mainContentFrame.style.width = "100%";//screen.availWidth-29;
			top.document.all.titleFrame.style.height = top.titleHeight; // defined in desktopManagement.js
			top.document.all.mainContentFrame.style.top = top.titleHeight; // defined in desktopManagement.js
			if(screen.availHeight < top.minHeight){
				top.document.all.mainContentFrame.style.height = minHeight;
			} else {
				top.document.all.mainContentFrame.style.height = screen.availHeight- titleHeight -20;
			}
		}catch(err1){
			top.reportException(err1, "initApplicationWindowSize - main content frame resize");
		}
		try{
			restoreWindow();
		}catch(err){
			top.reportException(err, "initApplicationWindowSize");
		}
	}
	
	function setDefaultWindowPosition(){
		try{
			if(top.defaultWindowOrientation == null || top.defaultWindowOrientation == top.FULL_SCREEN){
				top.moveTo(0,0);
			} else if(top.defaultWindowOrientation != null){
				if(top.defaultWindowOrientation == top.HALF_SCREEN_LEFT_PANEL_ON_LEFT || top.defaultWindowOrientation == top.HALF_SCREEN_RIGHT_PANEL_ON_LEFT){
					top.moveTo(0,0);
				} else {
					top.moveTo((screen.availWidth/2),0);
				}
			}
		}catch(err){
			top.reportException(err,"setDefaultWindowPosition");
		}
	}

	function setDefaultWindowSize(){
		try{
			if(top.defaultWindowOrientation == null || top.defaultWindowOrientation == top.FULL_SCREEN){
				//begin FNB016
				if (screen.availWidth < top.maxWidth) {
					top.resizeTo(screen.availWidth, screen.availHeight);
				} else {
					top.resizeTo(top.maxWidth, screen.availHeight);
				}
				//end FNB016
			} else if(top.defaultWindowOrientation != null){
				top.setHalfWindowSize();
			}
		}catch(err){
			top.reportException(err,"setDefaultWindowPosition");
		}
		currentOrientation = top.FULL_SCREEN;
	}

	function setHalfWindowSize(){
		try{
			top.resizeTo((top.maxWidth/2+15), screen.availHeight);  //FNB016,SPRNBA-436, NBA341
		}catch(err){
			top.reportException(err,"setHalfWindowSize");
		}
		currentOrientation = top.HALF_SCREEN_LEFT_PANEL_ON_LEFT;
	}

	function setWindowWidth(windowNumber, widthToUse){
		try{
			customWindow = windowNumber;
			try{
				top.resizeTo(widthToUse+20, screen.availHeight);
			}catch(err){
				top.reportException(err,"setHalfWindowSize");
			}
			currentOrientation = top.CUSTOM_WIDTH;			
		} catch(err){
		}
		top.scrollTo(0,0);
		top.setButtonBar();
	}

	function showFullScreen(){
		try{
			if(top.mainContentFrame != null){
				top.mainContentFrame.document.getElementById("leftPane").style.display = "inline";
				top.mainContentFrame.document.getElementById("rightPane").style.display = "inline";
				top.setContentAreaSize();
			}
		} catch(err){
		}
		top.setDefaultWindowPosition();
		top.setDefaultWindowSize();
		top.scrollTo(0,0);
		top.setButtonBar();
	}
	
	function showAdminScreen(){
		try{
			if(top.mainContentFrame != null){
				top.mainContentFrame.document.getElementById("leftPane").style.display = "inline";
				top.mainContentFrame.document.getElementById("rightPane").style.display = "inline";
				top.setContentAreaSize();
			}
		} catch(err){
		}
		top.moveTo(0,0);
		top.scrollTo(0,0);
		top.resizeTo(top.ADMINCONSOLE_WIDTH, top.ADMINCONSOLE_HEIGHT);
		top.setButtonBar();
		currentOrientation = top.ADMIN_CONSOLE;
	}

	function restoreWindow(){
		try{
			if(top.defaultWindowOrientation != null){
				if(top.defaultWindowOrientation == top.FULL_SCREEN){
					top.showFullScreen();
				} else if(top.defaultWindowOrientation == top.HALF_SCREEN_LEFT_PANEL_ON_LEFT){
					top.showLeft();
				} else if(top.defaultWindowOrientation == top.HALF_SCREEN_RIGHT_PANEL_ON_LEFT ){
					top.showRight();
				} else if(top.defaultWindowOrientation == top.HALF_SCREEN_LEFT_PANEL_ON_RIGHT){
					top.showLeft();
				} else if(top.defaultWindowOrientation == top.HALF_SCREEN_RIGHT_PANEL_ON_RIGHT){
					top.showRight();
				} else if(top.defaultWindowOrientation == top.ADMIN_CONSOLE){
					top.showAdminScreen();
				} else {
					top.showFullScreen();
				}
			} else {
				top.showFullScreen();
			}
		}catch(err){
			top.reportException(err, "restoreWindow");
		}	
	
	}
	
	function showLeft(){
		try{
			if(top.mainContentFrame != null){
				top.mainContentFrame.width = (screen.availWidth/2)+20;
				top.mainContentFrame.document.getElementById("leftPane").style.display = "inline";
				top.mainContentFrame.document.getElementById("rightPane").style.display = "none";
			}
		} catch(err){
		}
		top.scrollTo(0,0);
		top.setHalfWindowSize();
	}

	function showRight(){
		try{
			if(top.mainContentFrame != null){
				top.mainContentFrame.width = (screen.availWidth/2)+20;
				top.mainContentFrame.document.getElementById("leftPane").style.display = "none";
				top.mainContentFrame.document.getElementById("rightPane").style.display = "inline";
			}
		} catch(err){
		}
		top.moveTo(0,0);
		top.setHalfWindowSize();
	}

	function setButtonBar(){
		try{
			var buttonBarElement = document.getElementById("windowButtonBar");
			if(buttonBarElement != null){
				if(top.showWindowButtonBar){
					buttonBarElement.style.visibility = "visible";
					// find left position and place
					buttonBarElement.style.position = "absolute";
					var targetLeft = document.body.clientWidth - 100;
					var targetTop = 10;
					buttonBarElement.style.left = targetLeft;
				} else {
					buttonBarElement.style.display = "none";
				}
			}
		} catch(err){
			top.reportException(err,"setButtonBar - button bar area");
		}
		try{
			var desktopMenuElement = getDesktopMenu();
			if(desktopMenuElement != null){
				top.resizeGlobalMenuBar();
			}
			var contextMenuElement = getContextMenu();
			if(contextMenuElement != null){
				top.resizeContextMenuBar();
			}
			var contentAreaElement = top.mainContentFrame.document.getElementById("contentArea");
			if(contentAreaElement != null){
				top.setContentAreaSize();
			}
		} catch(err1){
			top.reportException(err,"setButtonBar - content and menu area");
		}

	}

	function resizeGlobalMenuBar(){
		try{
			var desktopMenuElement = getDesktopMenu();
			if(desktopMenuElement != null){
				desktopMenuElement.height = desktopMenuElement.document.body.scrollHeight;
			}
		}catch(err){
			top.reportException(err, "resizeDesktopMenuBar");
		}
	}
	
	
	function resizeContextMenuBar(){
		try{
			var contextMenuContainerElement = top.mainContentFrame.document.getElementById("contextMenuContainer");
			if(contextMenuContainerElement != null){
				var typeContainerElement = top.mainContentFrame.document.getElementById("typeMenuContainer");
				var dateContainerElement = top.mainContentFrame.document.getElementById("dateMenuContainter");
				var typeWidth=0;
				var dateWidth=0;
				if(typeContainerElement != null){
					typeWidth = typeContainerElement.width;
				}
				if(dateContainerElement != null){
					dateWidth = dateContainerElement.width;
				}
				var checkVal1 = typeWidth + '';
				var checkVal2 = dateWidth + '';
				if(!(checkVal1.indexOf('%') > -1 || checkVal2.indexOf('%')>-1)){
					contextMenuContainerElement.width = document.body.clientWidth - typeWidth - dateWidth;
				}
			}
			var contextMenuElement = getContextMenu();
			var contextBarElement = top.mainContentFrame.document.getElementById("contextBar");
			if(contextMenuElement != null){
				contextMenuElement.height = contextMenuElement.document.body.scrollHeight;
			}
			if(contextBarElement != null){
				contextBarElement.height = contextMenuElement.height;
			}
		}catch(err){
			top.reportException(err, "resizeContextMenuBar");
		}
	}
	
	function initializeButtonBar(){
		try{
			var titleAreaElement = top.titleFrame;
			if(titleAreaElement != null){
				titleAreaElement.width = "100%";
				if(top.titleHeight != null && top.titleHeight != 0){
					titleAreaElement.height = top.titleHeight;
				} else {
					titleAreaElement.height = 33;
				}
			}
		}catch(err2){
			top.reportException(err2, "initializeButtonBar - set titleAreaElement");
		}
		var buttonBarElement = top.titleFrame.document.getElementById("windowButtonBar");
		if(buttonBarElement != null){
			if(showWindowButtonBar){
				buttonBarElement.style.visibility = "visible";
			} else {
				buttonBarElement.style.display = "none";
			}
		}
	}

	function getContentAreaWidth(){
		try{
			var contentAreaElement = top.mainContentFrame.document.getElementById("contentArea");
			if(contentAreaElement != null){
				var value = contentAreaElement.style.width;
				if(value != null && value.indexOf("px") == value.length - 2){
					return value.substr(0, value.length -2);
				} else if(vaue != null){
					return value;
				} else {
					return screen.availWidth;
				}
			}
		}catch(err){
			if(currentOrientation == top.FULL_SCREEN){
				return screen.availWidth;
			} else {
				return screen.availWidth/2;
			}
		}
	}
	
	function getContentAreaHeight(){
		try{
			var contentAreaElement = top.mainContentFrame.document.getElementById("contentArea");
			if(contentAreaElement != null){
				var value = contentAreaElement.style.height;
				if(value != null && value.indexOf("px") == value.length - 2){
					return value.substr(0, value.length -2);
				} else if(vaue != null){
					return value;
				} else {
					return screen.availHeight;
				}
			}
		}catch(err){
			return screen.availHeight;
		}
	}
	
	function getDesktopMenuHeight(){
		try{
			var desktopMenuElement = getDesktopMenu();
			if(desktopMenuElement != null){
				if(desktopMenuElement.height > 0){
					return desktopMenuElement.height;
				}
			}
		}catch(err1){
			return 0;
		}
		return 0;
	}
	
	function getContextMenuHeight(){
		try{
			var contextMenuElement = getContextMenu();
			if(contextMenuElement != null){
				try{
					if(contextMenuElement.document.body != null){
						return contextMenuElement.document.body.clientHeight;
					} else {
						return contextMenuElement.height;
					}
				}catch(err){
					if(contextMenuElement.height > 0){
						return contextMenuElement.height;
					}
				}
			}
		}catch(err1){
			return 0;
		}
		return 0;
	}
	
	function setContentAreaSize(){
		var baseHeight = 0;
		var heightOffset = 0;
		try{
			var contentAreaElement = top.mainContentFrame.document.getElementById("contentArea");
			if(contentAreaElement != null){
				var dtMenuHeight = getDesktopMenuHeight();
				var ctxMenuHeight = getContextMenuHeight();
				if(ctxMenuHeight > 22){
					ctxMenuHeight = 22;
				}
				baseHeight = top.document.body.clientHeight;
				heightOffset = 0;
				try{
					if(BrowserDetect.browser == "Explorer"){
						if(BrowserDetect.version == "7"){
							heightOffset = top.IE7Offset;
							baseHeight = baseHeight - top.IE7Offset;
						}
					}
				}catch(err2){
				}
				var currentContentFrameHeight = screen.availHeight - heightOffset - top.titleHeight;
				var contentAreaTableElement = top.mainContentFrame.document.getElementById("contentAreaTable");
				if(contentAreaTableElement != null) {
					if(currentContentFrameHeight < top.minHeight){
						contentAreaTableElement.height =  (top.minHeight - heightOffset - dtMenuHeight - ctxMenuHeight - windowBarOverhead)  + "px";
					} else {
						contentAreaTableElement.height =  (currentContentFrameHeight  - heightOffset - dtMenuHeight - ctxMenuHeight - windowBarOverhead) + "px";
					}
					if(top.currentOrientation == null || top.currentOrientation == top.FULL_SCREEN){
						if(screen.availWidth < top.minWidth){
							contentAreaTableElement.width = top.minWidth - 15;
						} else if (top.maxWidth != null && top.maxWidth < screen.availWidth) {  //FNB016
							contentAreaTableElement.width = top.maxWidth - 15;  //FNB016
						} else {
							contentAreaTableElement.width = screen.availWidth - 15;  //FNB016
						}
					} else {
						contentAreaTableElement.width = (top.maxWidth/2)-10;  //FNB016
					}
				}
				contentAreaElement.style.overflow = "auto";					
				contentAreaElement.style.width = top.document.body.clientWidth;
				contentAreaElement.style.height = baseHeight - dtMenuHeight - top.titleHeight - ctxMenuHeight -5;
			}
		}catch(err1){
			top.reportException(err1, "setContentAreaSize - set contentAreaElement");
		}
		
		try{
			var rightPaneElement = top.mainContentFrame.document.getElementById("rightPane");
			var leftPaneElement = top.mainContentFrame.document.getElementById("leftPane");		
			if(leftPaneElement != null && rightPaneElement != null){
				try{
					if(screen.availHeight < top.minHeight){
						leftPaneElement.height = top.minHeight - baseHeight- dtMenuHeight - ctxMenuHeight - windowBarOverhead;
						rightPaneElement.height = top.minHeight - baseHeight -dtMenuHeight - ctxMenuHeight - windowBarOverhead;
					} else {
						leftPaneElement.height = screen.availHeight - baseHeight -dtMenuHeight - top.titleHeight - ctxMenuHeight - windowBarOverhead;
						rightPaneElement.height = screen.availHeight - baseHeight- dtMenuHeight - top.titleHeight - ctxMenuHeight - windowBarOverhead;
					}
				}catch(errheight){
					top.reportException(errheight, "setContentAreaSize - set left and right panel heights");
				}
				try{
					if(rightPaneElement.style.visibility == null || rightPaneElement.style.visibility == "" || rightPaneElement.style.visibility == "visible"){
						if(leftPaneElement.style.visibility == null || leftPaneElement.style.visibility == "" || leftPaneElement.style.visibility == "visible"){
							//begin FNB016
							if ((top.currentOrientation == null || top.currentOrientation == top.FULL_SCREEN) && top.maxWidth != null) {
								if (screen.availWidth < top.maxWidth) {
									leftPaneElement.width = screen.availWidth - (top.maxWidth/2) - 8;
								} else {
									leftPaneElement.width = (top.maxWidth/2) - 8;
									top.mainContentFrame.width = top.maxWidth;
								}
							} else {
								if(screen.availWidth < top.minWidth){
									leftPaneElement.width = ((top.minWidth/2) - 8) + "";
									top.mainContentFrame.width = top.minWidth + "";
								} else if (top.maxWidth != null && top.maxWidth < screen.availWidth) {
									leftPaneElement.width = (top.maxWidth/2) - 8;
								} else {
									leftPaneElement.width = (screen.availWidth /2) -8;
								}
							}
							//end FNB016
						}
					} else {
						if(leftPaneElement.style.visibility == "visible"){
							leftPaneElement.width = "100%";
						}
					}
				} catch(lpe){
					top.reportException(lpe, "setContentAreaSize - set left panel size");
				}
				try{
					if(leftPaneElement.style.visibility == null || leftPaneElement.style.visibility == "" || leftPaneElement.style.visibility == "visible"){
						if(rightPaneElement.style.visibility == null || rightPaneElement.style.visibility == "" || rightPaneElement.style.visibility == "visible"){
							//begin FNB016
							if ((top.currentOrientation == null || top.currentOrientation == top.FULL_SCREEN) && top.maxWidth != null) {
								rightPaneElement.width = (top.maxWidth/2) - 8;
							} else {
								if(screen.availWidth < top.minWidth){
									rightPaneElement.width = ((top.minWidth/2)-6) + "";
								} else if (top.maxWidth != null && top.maxWidth < screen.availWidth) {
									rightPaneElement.width = (top.maxWidth/2) - 8;
								} else {
									rightPaneElement.width = (screen.availWidth/2) -6;
								}
							}
							//end FNB016
						}
					} else {
						if(rightPaneElement.style.visibility == "visible"){
							rightPaneElement.width = "100%";
						}
					}
				} catch(rpe){
					top.reportException(rpe, "setContentAreaSize - set right panel size");
				}
			}
		}catch(err4){
			top.reportException(err4, "setContentAreaSize - set panel sizes");
		}		
	}

// ----------------------------------
// Static Calls and Invocations
// ----------------------------------

	function scrollHighlight(divElement, index){
		if (divElement != null && index != null){
			 if(divElement.childNodes[0] == null){
					divElement.scrollTop = (index * 20); 
			 }else{		
				var height = 0;			
				for (var i=0; i < index; i++){
					try{
						if (divElement.childNodes[0].children[0] != null && divElement.childNodes[0].children[0].childNodes[i] != null){
							height += divElement.childNodes[0].children[0].childNodes[i].scrollHeight;
						}else {
							height += 17;
						}				
					} catch(er){
						height += 17;					
					}
				}		
				divElement.scrollTop= height;			
			}
		}
	}
	
	function calculateTotal(document, formId, tableId, elementId){
		var total = 0;
		var rowNum = 0;
		var currentElement = document.getElementById(formId + ":" + tableId + ":" + rowNum + ":" + elementId);
		while(currentElement != null){
			if(currentElement.value != null){
				total += Number(currentElement.value);
			}
			rowNum ++;
			currentElement = document.getElementById(formId + ":" + tableId + ":" + rowNum + ":" + elementId);
		}
		return total;
	}

	function addToFavorites() {
		try {
			var url = "http://" +  window.opener.getServer() + window.opener.getContext();
			if (window.external != null && window.external.AddFavorite != null) {
				window.external.AddFavorite(url, "CSA");
			} 
		} catch (er) {
			top.reportException(err, "desktopManagement - addToFavorites");					
		}
	}

	//ACEL3241
	function refreshLeftSide() {
		if(top.mainContentFrame.contentLeftFrame != null){
			top.mainContentFrame.contentLeftFrame.location.href = top.mainContentFrame.contentLeftFrame.location.href; 
		}
	}
	
	function refreshRightSide() {
		if(top.mainContentFrame.contentRightFrame != null){
			top.mainContentFrame.contentRightFrame.location.href = top.mainContentFrame.contentRightFrame.location.href; 
		}
	}
	//ACEL3241
	
	function refreshLeftFile(){
		try {
			if(top.mainContentFrame.contentLeftFrame != null){
				//if (top.mainContentFrame.contentLeftFrame.file != null && top.mainContentFrame.contentLeftFrame.file.submitContents != null) {
				//		top.mainContentFrame.contentLeftFrame.file.submitContents();
				//}
				
				if(top.mainContentFrame.contentLeftFrame.location.href.indexOf('policyFile') != -1){
					top.mainContentFrame.contentLeftFrame.location.href=top.mainContentFrame.contentLeftFrame.location.href;
				}

				if(top.mainContentFrame.contentLeftFrame.location.href.indexOf('blank') == -1){
					if(top.mainContentFrame.contentLeftFrame.policySelection != null){
						var currPolicySel = top.mainContentFrame.contentLeftFrame.policySelection.location.href;
						top.mainContentFrame.contentLeftFrame.policySelection.location.href = currPolicySel; 
					}
					if(top.mainContentFrame.contentLeftFrame.file.document.getElementById('imgLocation') == null){
						refreshFrame(top.mainContentFrame.contentLeftFrame.file);
					    if( top.mainContentFrame.contentLeftFrame.wffile != null){
						    refreshFrame(top.mainContentFrame.contentLeftFrame.wffile);
						}
						if (top.mainContentFrame.contentLeftFrame.claimPortfolio!=null){
						    refreshFrame(top.mainContentFrame.contentLeftFrame.claimPortfolio);
						}
						if (top.mainContentFrame.contentLeftFrame.examinerPortfolio!=null){
						    refreshFrame(top.mainContentFrame.contentLeftFrame.examinerPortfolio);
						}
					}
				}
			}
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshLeftFile");
		}
	}

	function refreshRightFile(){
		try {
			if(top.mainContentFrame.contentRightFrame != null){
				if (top.mainContentFrame.contentRightFrame.file != null && top.mainContentFrame.contentRightFrame.file.submitContents != null) {
						top.mainContentFrame.contentRightFrame.file.submitContents();
				}
				if(top.mainContentFrame.contentRightFrame.location != null && top.mainContentFrame.contentRightFrame.location.href != null && top.mainContentFrame.contentRightFrame.location.href.indexOf('blank') == -1){
					if(top.mainContentFrame.contentRightFrame.file.document != null && top.mainContentFrame.contentRightFrame.file.document.getElementById('imgLocation') == null){
						refreshFrame(top.mainContentFrame.contentRightFrame.file);
					}
				}
				if (top.mainContentFrame.contentRightFrame.policyPortfolio!=null){
					refreshFrame(top.mainContentFrame.contentRightFrame.policyPortfolio);
				}
				if (top.mainContentFrame.contentRightFrame.claimForm!=null){
					refreshFrame(top.mainContentFrame.contentRightFrame.claimForm);
				}
			}
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshRightFile");
		}
	}

	function refreshRightPortFolioFile(){
		try {
			if(top.mainContentFrame.contentRightFrame != null){
				refreshFrame(top.mainContentFrame.contentRightFrame.policyPortfolio);
			}
			if(top.mainContentFrame.desktopMenu != null){
				top.mainContentFrame.desktopMenu.location.href = top.mainContentFrame.desktopMenu.location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshRightPortFolioFile");
		}
	}
	
	function refreshLeftPortFolioFile(){
		try {
			if(top.mainContentFrame.contentLeftFrame != null){
				refreshFrame(top.mainContentFrame.contentLeftFrame.claimPortfolio);
				refreshFrame(top.mainContentFrame.contentLeftFrame.examinerPortfolio);
				refreshFrame(top.mainContentFrame.contentLeftFrame.wffile);
				refreshFrame(top.mainContentFrame.contentLeftFrame.file);
			}

			if(top.mainContentFrame.desktopMenu != null){
				top.mainContentFrame.desktopMenu.location.href = top.mainContentFrame.desktopMenu.location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshLeftPortFolioFile");
		}
	}	

	
function getDesktopComments(){
		if(top.mainContentFrame.contentLeftFrame != null){
			if(top.mainContentFrame.contentLeftFrame.location.href.indexOf('blank') == -1){
				if(	(top.mainContentFrame.contentLeftFrame.location.href.indexOf('customerFile') > -1) ||
					(top.mainContentFrame.contentLeftFrame.location.href.indexOf('workflowFile') > -1)
					&& top.mainContentFrame.contentLeftFrame.comments != null){
					return top.mainContentFrame.contentLeftFrame.comments.comments.value;
				}
			}
		}
		return "";
	}

	function getClaimComments(){
		if(top.mainContentFrame.contentRightFrame != null){
			if(top.mainContentFrame.contentRightFrame.location.href.indexOf('blank') == -1){
				if(	top.mainContentFrame.contentRightFrame.comments.comments.value != null){
					return top.mainContentFrame.contentRightFrame.comments.comments.value;
				}
			}
		}
		return "";
	}

	// refreshes current desktop
	function refreshDT() {
		try {
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			//Demo fix for consistent Claim Comment Dialog Show
			if(top.mainContentFrame.contentRightFrame.commentsSection !=  null){
				top.mainContentFrame.contentRightFrame.commentsSection.location.href = 
							top.mainContentFrame.contentRightFrame.commentsSection.location.href;
			}			
			refreshLeftFile();
			refreshRightFile();
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshDT");
		}
	}
	
	function loadRightFile(url){
		try{
			var bsePath=top.basePath;//NBA213
			top.mainContentFrame.contentRightFrame.document.write(
				'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'+
				'<html>'+
				'<head><link rel="stylesheet" type="text/css" href="'+ bsePath + '"/css/accelerator.css"></head>'+
				'<body class="mainBody"><table width="100%" height="100%"><tbody>' +
				'<tr><td align="center" class="textWait"><div align="center"><br id="imgLocation" />' +
				'<img src="'+ bsePath + '"/images/wait.gif" /> <br /><p />Please wait Initializing File...</div>' +
				'</td></tr></tbody></table</body></html>');//NBA213
			top.mainContentFrame.contentRightFrame.location.href = url;
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshDT");
		}
	}	

	function loadNewBusiness(urlBase) {
		try {
			loadRightFile(urlBase + "faces/newBusiness/nbFile.jsp");
			window.setTimeout(refreshMenus, REFRESH_MENU_WAIT);  //SPRNBA-439
		} catch(er) {
			top.reportException(er,"desktopManagement - loadNewBusiness");
		}
	}

	//SPRNBA-439 New Function
	function refreshMenus() {
		try {
			if (getDesktopMenu() != null) {
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if (top.mainContentFrame.contextMenu != null) {
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
		} catch(er) {
			top.reportException(er,"desktopManagement - refreshMenus");
		}
	}

	// initializes a new account folder on the services desktop
	function loadPolicyOnRight(urlBase ) {
		try {
			refreshLeftFile();
			loadRightFile(urlBase + "faces/policy/policyFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}

			
		} catch(er) {
			top.reportException(er,"desktopManagement - loadPolicyOnRight");
		}
	}


	function loadNewBusinessAndInvoke(urlBase ) {
		try {
			refreshLeftFile();
			loadRightFile(urlBase + "faces/newBusiness/nbFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			top.showRight();
			top.controlFrame.location.href = urlBase + "faces/launchUIProcess.jsp";
		} catch(er) {
			top.reportException(er,"desktopManagement - loadNewBusinessAndInvoke");
		}
	}

	// initializes a new account folder on the services desktop
	function loadPolicyAndInvokeOnRight(urlBase) {
		try {
			refreshLeftFile();
			loadRightFile(urlBase + "faces/policy/policyFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			top.controlFrame.location.href = urlBase + "faces/launchUIProcess.jsp";
		} catch(er) {
			top.reportException(er,"desktopManagement - loadPolicyOnRight");
		}
	}

// 
	function invoke(urlBase) {
		try {
			refreshLeftFile();
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			top.controlFrame.location.href = urlBase + "faces/launchUIProcess.jsp";
		} catch(er) {
			top.reportException(er,"desktopManagement - invoke");
		}
	}


	function loadLeftFile(url){
		try{
			
			
			top.mainContentFrame.contentLeftFrame.document.write(
				'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'+
				'<html>'+
				'<head><link rel="stylesheet" type="text/css" href="css/accelerator.css"></head>'+
				'<body class="mainBody"><table width="100%" height="100%"><tbody>' +
				'<tr><td align="center" class="textWait"><div align="center"><br id="imgLocation" />' +
				'<img src="images/wait.gif" /> <br /><p />Please wait Initializing File...</div>' +
				'</td></tr></tbody></table</body></html>');
			top.mainContentFrame.contentLeftFrame.location.href = url;
		} catch(er) {
			top.reportException(er,"desktopManagement - loadLeftFile");
		}
	}	


	function resetLeft(urlBase){
		if(top.mainContentFrame.reloadLeft != null){
			try{
				top.mainContentFrame.reloadLeft();
			}catch(err){
			}
		}
	}

	function loadClaimOnRight(urlBase ) {
		try {
			loadRightFile(urlBase + "faces/claim/claimFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
		} catch(er) {
			top.reportException(er,"desktopManagement - loadClaimOnRight");
		}
	}

	function loadClaimAndInvokeOnRight(urlBase ) {
		try {
			loadRightFile(urlBase + "faces/claim/claimFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			top.controlFrame.location.href = urlBase + "faces/launchUIProcess.jsp";
		} catch(er) {
			top.reportException(er,"desktopManagement - loadClaimOnRight");
		}
	}

	function loadCustomerOnRight(urlBase ) {
		try {
			loadRightFile(urlBase + "faces/customer/customerFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
		} catch(er) {
			top.reportException(er,"desktopManagement - loadClaimOnRight");
		}
	}

	function loadCustomerAndInvokeOnRight(urlBase ) {
		try {
			loadRightFile(urlBase + "faces/customer/customerFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			top.controlFrame.location.href = urlBase + "faces/launchUIProcess.jsp";
		} catch(er) {
			top.reportException(er,"desktopManagement - loadClaimOnRight");
		}
	}

	function loadCustomerOnLeft(urlBase ) {
		try {
			loadLeftFile(urlBase + "faces/customer/customerFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - loadClaimOnRight");
		}
	}

	function loadWorkflowOnLeft(urlBase ) {
		try {
			loadLeftFile(urlBase + "faces/workflow/workflowFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - loadWorkflowOnLeft");
		}
	}


	function loadPolicyOnLeft(urlBase ) {
		try {

			refreshRightFile();
			loadLeftFile(urlBase + "faces/claimpolicy/policyFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - loadPolicyOnRight");
		}
	}

	
	function loadPolicyAndClientOnRight(urlBase){
		try {
			refreshLeftFile();
			loadRightFile(urlBase + "faces/desktops/combinedFile.jsp");
			if(getDesktopMenu() != null){
				getDesktopMenu().location.href = getDesktopMenu().location.href;
			}
			if(top.mainContentFrame.contextMenu != null){
				top.mainContentFrame.contextMenu.location.href = top.mainContentFrame.contextMenu.location.href;
			}
			
		} catch(er) {
			top.reportException(er,"desktopManagement - loadPolicyAndClientOnRight");
		}
	}
	
	function embedSystem(context, url, systemName){
		try{
			showTimedWait("Launching - " + systemName, 3000);
			if (!lastLaunchedWindow) {
				lastLaunchedWindow = window.open(url);
				lastLaunchedWindow.focus();			
			} else {
				if (!lastLaunchedWindow.closed) {
					lastLaunchedWindow.open(url);
					lastLaunchedWindow.focus();			
				} else {
					lastLaunchedWindow = window.open(url);
					lastLaunchedWindow.focus();			
				}
			}
			sysWindowlaunched = true;
		} catch (err){
			reportException(err, "desktopManagement - embedSystem");
		}
	}

	function pause(numberMillis) {
	    var dialogScript = 'window.setTimeout(' + ' function () { window.close(); }, ' + numberMillis + ');';
	    var result = window.showModalDialog('javascript:document.writeln(' +'"<script>' + dialogScript + '<' + '/script>")');
	}
	
	function getDesktopMenu(){
		return top.mainContentFrame.desktopMenu;
		//return top.titleFrame.desktopMenu;
	}
	
	function getContextMenu(){
		return top.mainContentFrame.contextMenu;
		//return top.titleFrame.contextMenu;
	}
	
	function loadMenuBar(context, url){
		try{
			if(context.charAt(context.length) != '/'){
				if(url.charAt(0) != '/'){
					url = '/' + url;
				}
			}
			getDesktopMenu().location.href = context + url;
		} catch(err){
			top.reportException(err,"loadMenuBar");
		}
	}

	function loadContextBar(context, url){
		try{
			if(context.charAt(context.length) != '/'){
				if(url.charAt(0) != '/'){
					url = '/' + url;
				}
			}
			getContextMenu().location.href = context + url;
		} catch(err){
			top.reportException(err,"loadContextBar");
		}
	}
	
	function refreshFrame(currFrame){
		try{
			if(currFrame !=null){
				var refreshObj = currFrame.document.getElementById("RefreshingDIV");
				if(refreshObj == null){
					if(currFrame.location != null){
						var currentRef = currFrame.location.href;
						//begin NBA213
						if (currFrame.fileLocationHRef != null && currFrame.fileLocationHRef != "" && currFrame.fileLocationHRef.indexOf('blank') == -1) {
							currentRef = currFrame.fileLocationHRef;
						}
						//end NBA213
//						if(currFrame != null){
//							alert("Refresh CurrentFrame [" + currFrame.name + "] [" + currFrame.location.href + "] " + currentRef);
//						}
						if(currentRef != null && currentRef != "" && (currentRef.indexOf("blank") == -1)){
							var index = currentRef.indexOf("launchPolicy=true");
							if(index > -1){
								currentRef = currentRef.substr(0, index);
							}
//							alert("Refresh CurrentFrame [" + currFrame.name + "] [" + currFrame.location.href + "] " + currentRef);
							try{
								numRrefreshes++;
								if(currentRef != null && currentRef != "" && currentRef.indexOf('blank') == -1){
									var displayText = '<HTML><HEAD>' +
										'<LINK REL="stylesheet" TYPE="text/css" HREF="' + top.basePath + '/css/accelerator.css">' +
											documentKeyCaptureFunctionScript +
											'</HEAD>' + 
											'<BODY class="mainBody" leftmargin="0" topmargin="0" ' + onkeyCaptureFunctionScript + '>' +
											'<table width="100%"><tr><td align="center" style="margin: 2px;	font: bold 10px \'Verdana\'; text-align: left; vertical-align: middle; color: navy; text-decoration: none;">' +
													'<div align="center" id="RefreshingDIV">' +
														'<br/>' +
														'<P/>' +
														'Please wait refreshing...' +
													'</div>' +
											'</td></tr></table>' +
											'</BODY></HTML>';
						    		currFrame.document.write(displayText);
									currFrame.document.oncontextmenu = new Function("if (!top.enableContextMenu) { return false; }");
								}
							} catch(err1){
								// DO NOTHING 
							}
							try{
								currFrame.location.href = currentRef;
							}catch(err3){
								// DO NOTHING 
							}
						}
					}
				}
			}
		}catch(ex){
			top.reportException(ex, "refreshFrame - unhandled exception");
		}
	}
	
	function setDesktopType(desktopImageSrc, desktopTypeName){
		try{
			var dtImage = top.titleFrame.document.getElementById("DesktopTypeImage");
			var dtText = top.titleFrame.document.getElementById("DesktopTypeText");
			try{
				if(desktopImageSrc != null && desktopImageSrc.innerHTML != null){
					desktopImageSrc = desktopImageSrc.innerHTML
				}
			}catch(err1){
			}
			
			try{
				if(desktopTypeName != null && desktopTypeName.innerHTML != null){
					desktopTypeName = desktopTypeName.innerHTML
				}
			}catch(err2){
			}
			
			if(dtImage != null){
				if(desktopImageSrc == null || desktopImageSrc == ""){
					dtImage.style.display = "none";
				} else {
					dtImage.src = desktopImageSrc;
					dtImage.style.display = "inline";
				}
			}
			
			if(dtText != null){
				var callTime = top.titleFrame.document.getElementById("CallTime");
				if(desktopTypeName== null || desktopTypeName == ""){			
					dtText.style.display = "none";
					if(callTime != null){
						callTime.style.display = "none";
					}
				} else {			
					if(callTime != null){
						callTime.style.display = "inline";
					}
					dtText.innerHTML = desktopTypeName;
					dtText.style.display = "inline";
				}
			}
		}catch(err){
		}
	}
	
	
	
	
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();	
	