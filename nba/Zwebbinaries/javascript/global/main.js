/*************************************************************************
 *
 * Copyright Notice (2003)
 * (c) CSC Financial Services Limited 1996-2003.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/

// Globals
var usePopups = false;
var oPopup = null;
var waitTimer = null;

var basePath = '';
var server = "";
var inProgress = false;

function PopupWindowObject(){
	this.win = null;
	this.div = null;
	this.waitTimer = null;
	try{
		this.createWin();
	} catch(err){
		reportException(err, "PopupWindowObject.new");
	}
}

PopupWindowObject.prototype.createWin = function(){
	try{
		if(usePopups && window.createPopup != null){
			this.win = top.window.createPopup();
		} else {
			this.div = top.document.getElementById('popupWindowDiv');
			if(this.div != null){
				this.div.style.cursor = "wait";
			}
		}
	} catch(err){
		reportException(err, "PopupWindowObject.createWin");
	}
}

PopupWindowObject.prototype.isOpen = function(){
	try{
		if(window.createPopup != null){
			if(this.win != null){
				return this.win.isOpen;
			} else {
				return false;
			}
		} else {
			if(this.div != null){
				return this.div.isOpen;
			} else {
				return false;
			}
		}
		return false;
	} catch(err){
		reportException(err, "PopupWindowObject.isOpen");
	}
}

PopupWindowObject.prototype.getINNERHTML = function(){
	try{
		if(usePopups && window.createPopup != null){
			if(this.win == null){
				this.createWin();
			}
			if(this.win != null && this.win.document != null && this.win.document.body != null){
				return this.win.document.body.innerHTML;
			}
		} else {
			if(this.div == null){
				this.createWin();
			}
			return this.div.innerHTML;
		}
		return "";
	} catch(err){
		reportException(err, "PopupWindowObject.getINNERHTML");
	}
}

PopupWindowObject.prototype.setINNERHTML = function(innerHTML){
	try{
		if(usePopups && window.createPopup != null){
			if(this.win == null){
				this.createWin();
			}
			if(this.win.document != null && this.win.document.body != null){
				this.win.document.body.innerHTML = innerHTML;
			}
		} else {
			if(this.div == null){
				this.createWin();
			}
			this.div.innerHTML = innerHTML;
		}
	} catch(err){
		reportException(err, "PopupWindowObject.setINNERHTML");
	}
}

PopupWindowObject.prototype.show = function(targetLeft, targetTop, width, height, parentNode){
	try{
		if(usePopups && window.createPopup != null){
			if(this.win == null){
				this.createWin();
			}
			if(this.win.show != null){
				this.win.show(targetLeft, targetTop, width, height, parentNode);
			}
		} else {
			if(this.div == null){
				this.createWin();
			}
			this.div.style.position = "absolute";
			this.div.style.zIndex = 2000;
//			this.div.style.left = targetLeft;
//			this.div.style.top = targetTop;
//			this.div.style.width = width;
//			this.div.style.height = height;
			this.div.style.left = 0;
			this.div.style.top = 10;
			this.div.style.width = top.getContentAreaWidth();
			this.div.style.height = top.getContentAreaHeight();
			this.div.style.visibility = "visible";
		}
		currentCount = 0;
		inProgress = true;
		if(this.div.setCapture){
			this.div.setCapture(true);
		}
		// set timeout - but only one timeout at a time
		// if one is already created, that is sufficient
		if (this.waitTimer == null) {  //SPRNBA-802
			this.waitTimer = setTimeout("javascript:top.oPopup.showTimeoutMessage();", top.LONG_DEFAULT_TIMEOUT);
		}
	} catch(err){
		reportException(err, "PopupWindowObject.show");
	}
}

PopupWindowObject.prototype.modalBlock = function(modalWindow){
	if(modalWindow != null){
		modalWindow.initializeBlock();
		//modalWindow.close();
	} else {
		top.document.all.controlFrame.contentWindow.document.write("<SCRIPT>window.showModalDialog(top.basePath+'/modalblock.html', top, 'dialogHeight: 100px; dialogWidth: 100px; dialogTop: -200px; dialogLeft: -200px;');</SCRIPT>");
	}
}

PopupWindowObject.prototype.checkIsVisible = function(){
	try{
		if(inProgress){
			return true;
		} else {
			return false;
		}
	} catch(err){
		reportException(err, "PopupWindowObject.isVisible");
	}
	return false;
}

PopupWindowObject.prototype.showTimeoutMessage = function(){
	try{
		if (inProgress) {
			inProgress = false;
		//alert("Operation timed out");
			this.hide();
		}
		//window.top.controlFrame.location.href = "operationTimedout.jsp":
	} catch(err){
		reportException(err, "PopupWindowObject.isVisible");
	}
}

PopupWindowObject.prototype.hide = function(){
	try{
		inProgress = false;
		if(this.div.releaseCapture){
			this.div.releaseCapture();
		}
		clearTimeout(this.waitTimer);
		this.waitTimer = null;
		if(usePopups && window.createPopup != null){
			if(this.win != null && this.win.hide != null){
				this.win.hide();
			}
		} else {
			if(this.div != null){
				this.div.style.position = "absolute";
				this.div.style.width = 0;
				this.div.style.height = 0;
				this.div.style.zIndex = 0;
				this.div.style.visibility = "hidden";
			}
		}
	} catch(err){
		reportException(err, "PopupWindowObject.hide");
	}
}


	function getCookieValue(cookieName){
		var cookies = document.cookie;
		var result = null;
		var start = cookies.indexOf(cookieName + "=");
		if(start > -1){
			var end = cookies.indexOf(';', start);
			if(end == -1) end = cookies.length;
			result = cookies.substring(start,end);
		}
		return result;
	}
	
	
	



	/**
	 * Shows the wait panel with given message
	 * @author Channel Services Development Team
	 * @param message to display
	 */
	function showWait(userMessage) {
		try {
			initializePopup();
			clearTimeout(waitTimer);
			if(top.oPopup != null && (top.oPopup.isOpen() == false)){
				var width = 500;
				var height = 8;
				var sHeadHTML = '<head>' +
					'<link REL="stylesheet" TYPE="text/css" HREF="' + basePath + '/css/accelerator.css">' +
					'</head>';
				var sBodyHTML = '<body>'+
						'<link REL="stylesheet" TYPE="text/css" HREF="' + basePath + '/css/accelerator.css">' +
						"<table width='100%' height='100%' border='0' valign='top'>" +
							'<tr style="border:none;" valign="top">' +
							'<td class="textWait" style="text-align: right;vertical-align:top; color:white;" >' +
							'<span id="waitText">' + userMessage + '</span></td><td style="width:130px"></td></tr>' +
						"</table>"+
					'</p></body>';
				top.oPopup.setINNERHTML(sBodyHTML);
				var targetLeft = (top.getContentAreaWidth()-130) - width;
				var targetTop = 9;
				top.oPopup.show(targetLeft, targetTop, width, height, document.body);
			}
		}catch(er){
			reportException(er, "common - showWait");
		}
	}
	
	function setWaitMessage(newText){
		try{
			alert(newText);
			element = top.getElemenByID("waitText");
			if(element != null){
				element.innerHTML = newText;
			}
		} catch(err){
		}
	}

	/**
	 * Shows the wait panel with given message and closes after timeout
	 * @author Channel Services Development Team
	 * @param message to display
	 * @param max time to wait
	 */
	function showTimedWait(userMessage, maxTime) {
		try {
			//initializeWaitTimer(maxTime);
			showWait(userMessage);
		}catch(er){
			reportException(er, "common - showTimedWait");
		}
	}

	/**
	 * Hides the wait panel if present
	 * @author Channel Services Development Team
	 */
	function hideWait() {
		try {
			if(numRrefreshes<=0){
				if(!top.windowLaunching){
					clearTimeout(waitTimer);
					if(top.oPopup != null){
						top.oPopup.hide();
					}
				}
			}
		}catch(er){
			reportException(er, "common - hideWait");
		}
	}
	
	function initializePopup() {
		try {
			if(top.oPopup == null){
				top.oPopup = new PopupWindowObject();
			}
		}catch(er){
			reportException(er, "common - initializePopup");
		}
	}

	function initializeWaitTimer(maxTime){
		try {
			waitTimer = setTimeout('hideWait()', maxTime);
		}catch(er){
			reportException(er, "common - initializeWaitTimer");
		}
	}

	/**
	 * retrieves the Query String parameters into a map like array
	 * Usage:
	 * 		var args = getArgs();
	 * 		var values = args["ParamName"];
	 * 
	 * @author Channel Services Development Team
	 * @return the array of parameters
	 */
	function getArgs() {
		try {
			var args = new Object();
			var query = location.search.substring(1);
			var pairs = query.split("&");
			for (var i = 0; i < pairs.length; i++) {
				var pos = pairs[i].indexOf('=');
				if (pos == -1) {
					continue;
				}
				var argname = pairs[i].substring(0, pos);
				var value = pairs[i].substring(pos + 1);
				args[argname] = value;
			}
			return args;
		}catch(er){
			reportException(er, "common - getArgs");
		}
	}

  /**
   * retrieves value from core page
   * @param name of variable
   * @return value
   */
   function retrieveGlobalVariable(varname){
	try{
		return eval("top." + varname);
	}catch(er){
		reportException(er, "common - retrieveGlobalVariable");
	}
	return "";
   }

   /**
    * retrieves internal transmission formatted Current Date from Machine where browser resides
    * @return internal transmission formatted Date (-1 on error)
    */
    function getClientDate(){
    	try {
			var date = new Date();
			date.getDate()<10 ? day="0"+date.getDate().toString() : day=date.getDate().toString();
			date.getMonth()<9 ? month="0"+(date.getMonth()+1).toString() : month=(date.getMonth()+1).toString();
			var clientDate = day + month + date.getYear().toString();
			return clientDate;
						
		} catch (er) {
			reportException(er, "common - getClientDate");
			return -1;
		}
    }
    

   /**
    * retrieves internal transmission formatted Current Time from Machine where browser resides
    * @return internal transmission formatted Time (-1 on error)
    */
    function getClientTime(){
    	try {
			var date = new Date();
					
			date.getHours()<10 ? hours="0"+date.getHours().toString() : hours=date.getHours().toString();
			date.getMinutes()<10 ? minutes="0"+date.getMinutes().toString() : minutes=date.getMinutes().toString();
			date.getSeconds()<10 ? seconds="0"+date.getSeconds().toString() : seconds=date.getSeconds().toString();
			if (date.getMilliseconds().toString().length == 1){
				milliseconds = "000"+date.getMilliseconds().toString();
			} else if (date.getMilliseconds().toString().length == 2){
				milliseconds = "00"+date.getMilliseconds().toString();
			} else if (date.getMilliseconds().toString().length == 3){
				milliseconds = "0"+date.getMilliseconds().toString();
			}
	
			var clientTime = hours + minutes + seconds +milliseconds;
			return clientTime;

		} catch (er) {
			reportException(er, "common - getClientTime");
			return -1;
		}
    }


    function getClientDateTime(){
    	try {
			var date = new Date();
			date.getDate()<10 ? day="0"+date.getDate().toString() : day=date.getDate().toString();
			date.getMonth()<9 ? month="0"+(date.getMonth()+1).toString() : month=(date.getMonth()+1).toString();
			//var clientDate =  month + "/" + day + "/" + date.getYear().toString();
			
			date.getHours()<10 ? hours="0"+date.getHours().toString() : hours=date.getHours().toString();
			date.getMinutes()<10 ? minutes="0"+date.getMinutes().toString() : minutes=date.getMinutes().toString();
			date.getSeconds()<10 ? seconds="0"+date.getSeconds().toString() : seconds=date.getSeconds().toString();
			
			var clientDateTime = date.getYear().toString() + "-" + month + "-" + day + "-" + hours + "." + minutes + "." + seconds;
			return clientDateTime;
						
		} catch (er) {
			reportException(er, "common - getClientDateTime");
			return -1;
		}
    }

	function sendLogText(source, message){
		try{
			top.notificationClientFrame.logToServer(source, message);
		}catch(err){
		}
	}

//SPRNBA-947 code deleted
