
/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/
  <!-- CHANGE LOG -->
  <!-- Audit Number   Version   Change Description -->
  <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->
  <!-- SPRNBA-305	  NB-1501	Dragging Pop Up Unable to Drop Window and Moves /*with*/ Each Mouse Click -->
  <!-- NBA356         NB-1501   Comments Floating View -->
// Functions Related to Window and Dialog Management

// ----------------------------------
// Globals
// ----------------------------------

var processingLaunch = '';		// currently processing dialog launch flag
var currentParent = '';			// current Parent (Top Level Dialog Window if num Windows > 1)
var newWindow = null;			// current Active Dialog Window

var currentNamedWindowsList = new Object();

var currentElementContent = '';	// current Dialog Window Container
var currentDialogContext = ''; // window context for current dialog
var currentParentArray = new Array();	// active parent Dialog Windows
var lastButtonClicked = null;

var currentWindow = -1;			// current window handle -1 = no window currently set
var topLevelZOrder = INITIAL_WIN_Z_ORDER; // latest highest zOrder for popup windows - reinitialized to 0 when all windows are closed

var windowLaunching = false;

function windowsOpen(){
	return (currentWindow != null && currentWindow != -1 && currentWindow != '');
}

var captureFunctionScriptContent = "" +
				"	if (event.keyCode == top.F1_KEY){ " +
				"		top.showAccelHelp(null); " +
				"		event.cancelBubble = false; " +
				"		event.keyCode = 0; " +
				"		event.returnValue = false; " +
				"	} else { " +
				"		if ( event.altKey ||  " +
				"			(event.keyCode == top.BACKSPACE_KEY && (event.srcElement.type != 'text' && event.srcElement.type != 'textarea' && event.srcElement.type != 'password')) ||  " +
				"		    (event.ctrlKey && ( event.keyCode == top.N_KEY || event.keyCode == top.R_KEY)) ||  " +
				"		    (event.keyCode == top.F5_KEY) || (event.keyCode == top.F11_KEY)) { " +
				"		    	if (event.keyCode == top.F5_KEY) top.refreshDT(); " +
				"				event.cancelBubble = false; " +
				"				event.keyCode = 0; " +
				"				event.returnValue = false; " +
				"	 	} " +
				"	 	if ((event.srcElement.type == 'text' || event.srcElement.type == 'textarea' || event.srcElement.type == 'password') &&  " +
				"		    (event.ctrlKey && (event.keyCode == top.N_KEY || event.keyCode == top.R_KEY)) || " +
				"		    (event.keyCode == top.F5_KEY) || (event.keyCode == top.F11_KEY)){ " +
				"		    	if (event.keyCode == top.F5_KEY) top.refreshDT(); " +
				"				event.cancelBubble = false; " +
				"				event.keyCode = 0; " +
				"				event.returnValue = false; " +
				"		} " +
				"	}";

var helpFunctionScriptContent = "" +
				"	event.cancelBubble = false;" +
				"	event.keyCode = 0;" +
				"	event.returnValue = false;" +
				"	return false;";

var onkeyCaptureFunctionScript = ' onKeyDown="' + captureFunctionScriptContent + '" ';

var documentKeyCaptureFunctionScript = '<SCRIPT type="text/javascript" ><!-- document.onkeydown = cancelBack;\n document.onhelp= helpExit;\n' +
		'function cancelBack(event){if(event == null){event=window.event;};' + captureFunctionScriptContent + '}\n' +
		'function helpExit(event){if(event == null){event=window.event;};' + helpFunctionScriptContent + '}\n' +		
		'--></SCRIPT>';

// -----------------------------------
// Function Implementations
// -----------------------------------

function showWindowInternal(context, url, targetparent,resetopener){ //NBA356 Changed no of Params for retaining opener of appframeExt
	try{
		top.showWait("Initializing dialog " + url);
		windowLaunching =  true;
		
		currentWindow ++;
		var width = -2;
		var height = -2;
			
			var padHeight = 28;
			var padWidth = 2;
			
			if(newWindow != null) {
				if (currentParent != null && currentParent != '') {
					addToCurrentParentArray(currentParent);
				}
				currentParent = newWindow;
			}
			if(targetparent == null || targetparent == '') {
				targetparent = this;
			}
			currentDialogContext = context;
	
			var widthToUse = width + padWidth;
			var heightToUse = height + padHeight;
			var targetFrameWidth = width;
			var targetFrameHeight = height-3;
			var centerBarWidth = widthToUse;
			
		//var targetLeft = ((screen.availWidth-20) / 2) - (widthToUse / 2) - (padWidth /2);
		//var targetTop = (screen.availHeight /2) - (heightToUse /2) - (padHeight /2);
	
	//	var targetLeft = -1000;
	//	var targetTop = 0;
	
		var targetLeft = 200;
		var targetTop = 200;
	
			insertHTML( winList,
				'<DIV ID="Win' + currentWindow + '"' + onkeyCaptureFunctionScript +
				' style="background:transparent; position:absolute;zIndex:1000;left:' + targetLeft + ';top:' + targetTop + ';width:' + widthToUse + ';height:' + heightToUse + ';visiblity:hidden" moving="false" curxmouse=0 curymouse=0 >' +
				'<IFRAME style="background:transparent;position:relative;" frameborder="0" id="FWin'+ currentWindow + '" name="FWin'+ currentWindow + '" width="' + widthToUse + '" height="' + heightToUse + '" HSPACE="0" VSPACE="0" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="NO" ATOMICSELECTION="true"/' + onkeyCaptureFunctionScript + '>' + 
				'</DIV>');
	
			currentElementContent = top.document.getElementById("FWin" + currentWindow);
			currentElementContent.window = this;
			if(!resetopener){ //NBA356
				currentElementContent.window.opener = targetparent.opener; //NBA356
			} else{ //NBA356
				currentElementContent.window.opener = targetparent;
			} //NBA356
	
			var urltouse = context + "/" + url;
			var basePath = "";
			if(window.document.location.port != ""){
				basePath = window.document.location.protocol + "//" + window.document.location.hostname +":" + window.document.location.port + context;
			} else {
				basePath = window.document.location.protocol + "//" + window.document.location.hostname + context;
			}
			
		
			currentElementContent.contentWindow.document.write(
				'<HTML><HEAD>' +
				documentKeyCaptureFunctionScript +			
				'<LINK REL="stylesheet" TYPE="text/css" HREF="' + context+ '/css/accelerator.css"></LINK></HEAD><BODY></BODY></HTML>');
			
		currentElementContent.contentWindow.document.oncontextmenu = new Function("return false;");
		
			insertHTML(currentElementContent.contentWindow.document.documentElement,	
				'<BODY class="windowTable" ' + onkeyCaptureFunctionScript + ' >' +
				'<div ' + onkeyCaptureFunctionScript + '>' +
				'<table class="windowTable" CELLSPACING="0" CELLPADDING="0" border="0" width="100%" >' + 
					'<tr style="background: transparent;">' + 
						'<td style="background: transparent;">' +
							'<table class="windowTable" CELLSPACING="0" CELLPADDING="0">' +
								'<tr style="border:none;">' + 
									'<td ALIGN="center">' + 
										'<div UNSELECTABLE="on" onselectstart="return false;">' +								
										'<table width="100%" class="windowBar" CELLSPACING="0" CELLPADDING="0" onmousedown="top.beginDrag(this, document.all.Title' + currentWindow + ',  \'' + currentWindow + '\', window.event);">' + 
											'<tbody><tr class="windowBar">'+
												'<td class="windowBarLeft" onselectstart="return false;"></td>' +
												'<td id="centerBar' + currentWindow + '" class="windowBarCenter" style="width:' + centerBarWidth + 'px" onselectstart="return false;">' + 
													'<span ATOMICSELECTION="true" class="textWindowBar" id="Title' + currentWindow + '" onmousedown="return false;" onmouseover="return false;" onselectstart="return false;" >Please wait ...</span>' +
												'</td>' +
												'<td class="windowBarRight" onselectstart="return false;"></td>' +
											'</tr>' +
										'</table>' +
										'</div>'+
									'</td>' +
								'</tr>' +
								'<tr style="border:none;" class="updatePanel">' +
									'<td>' + 
									'<iframe name="Content' + currentWindow + '" class="windowPane" id="Content' + currentWindow + '" frameborder="0" SCROLLING="NO" width="' + targetFrameWidth + '" height="' + targetFrameHeight + '" onblur="if(this.contentWindow.currentWindow == parent.currentWindow ){ if (parent.sysWindowlaunched == true){ parent.sysWindowlaunched = false; } else { this.focus();}}"/>' +
									'</td>' +
								'</tr>' + 
							'</table>'+
						'</td>'+
					'</tr>'+
				'</table>'+
				'</div>' +
				'</BODY>');
			var targetFrameElement = currentElementContent.contentWindow.document.getElementsByTagName("iframe")[0];
			targetFrameElement.contentWindow.document.write(
			'<HTML><HEAD>' +
				'<LINK REL="stylesheet" TYPE="text/css" HREF="' +  context + '/css/accelerator.css">' +
				documentKeyCaptureFunctionScript +
				'</HEAD>' +
				'<BODY class="updatePanel" leftmargin="0" topmargin="0" ' + onkeyCaptureFunctionScript + '>' +
				'</BODY></HTML>');
			
			targetFrameElement.contentWindow.location.href = urltouse;
			targetFrameElement.contentWindow.opener = targetparent;
	
			var currentElement = top.document.getElementById('Win' + currentWindow);
			currentElement.style.position = "absolute";
			currentElement.style.zIndex = topLevelZOrder++;
			currentElement.style.visibility = "visible";
	
			newWindow = currentElementContent.contentWindow.document.getElementsByTagName("iframe")[0].contentWindow;
			newWindow.currentWindow = currentWindow;
			newWindow.titleElement = eval("currentElementContent.contentWindow.document.all.Title" + currentWindow);
	
			window.top.focus(); 			
		}catch(err){
			top.reportException(err,"showWindowInternal");
		}
	}


var lastContext = null;
var lastURL = null;
var lastTargetParent = null;
var oldCurrentWindow = currentWindow;
var oldNewWindow = newWindow;
var numRetries = 0;

function checkIfLaunched(){
	if(currentWindow != oldCurrentWindow && oldNewWindow != newWindow && newWindow != null){
	} else {
		numRetries++;
		if(numRetries < MAX_RETRIES){
			showWindowRetry(lastContext, lastURL, lastTargetParent);
		} else {
			// Timed out error
		}
	}
}

/*
 * shows/launches a new popup window
 * context - the web application context (e.g. '/csA')
 * url to load (not including the context) (e.g. '/error,jsp')
 * parent object that will be set as the windows window.opener - (e.g. this)
 */
function showWindow(context, url, targetparent){
	oldCurrentWindow = currentWindow;
	oldNewWindow = newWindow;
	top.hideWait();
	top.showWait("Launching Dialog " + url);
	showWindowInternal(context,url,targetparent,true); //NBA356
	window.setTimeout(checkIfLaunched, LAUNCH_DIALOG_WAIT);
	processingLaunch = '';
}

function showWindowRetry(context, url, targetparent){
	top.showWait("Retry (" + numRetries + ") Launching Dialog " + url);
	showWindowInternal(context,url,targetparent,true); //NBA356
	window.setTimeout(checkIfLaunched, LAUNCH_DIALOG_WAIT);
	processingLaunch = '';
}

/*
 * closes given window (if none specified closes last - current - created)
 * window number to close - if not specified closes current window
 */
function closeWindow(windownumber, fromError){
	try{
		if(windownumber == null){
			windownumber = currentWindow;
		}
		
		if (newWindow != null || windownumber >= 0) {
			try {
				// first call destructor if exists
				if(eval("newWindow.destructor") != null) {
					try {
						newWindow.destructor();
					} catch(dserr) {
						top.reportException(dserr, "Error calling destructor on current window");
					}
				}
			} catch( er ) {
				var description = er.description;
				if (description != "Permission denied") {
					top.reportException(er, "dialogManagement - newWindow.destructor");
				}
			}
			
			var elementContent = top.document.getElementById('Win' + windownumber);
			if(elementContent != null){
				var currentWindowList = top.document.all.winList;
				currentWindowList.removeChild(elementContent);
				// now we check to see if we have a handler for success						
				if(windownumber == currentWindow){
					currentWindow--;
				}
			}
			currentElementContent = '';
		}
		
		if(top.currentOrientation == top.CUSTOM_WIDTH){
			if(top.customWindow == windownumber){
				top.restoreWindow();
			}
		}

		if(currentParent != null && currentParent != ''){
			newWindow = currentParent;
			if(newWindow != null){
				// current window is newWindow and so set current window elements
				if(newWindow.parent != null){
					windowName = newWindow.parent.name;
					currentWindow = windowName.substr(4);
					currentElementContent = newWindow.parent;
				} else {
					currentWindow = null;
					currentElementContent = null;
				}
				if(fromError == null || fromError =="false"){
					newWindow.document.location.href = newWindow.document.location.href;
				}
			}
			if (currentParentArray.length > 0) {
				currentParent = returnLastCurrentParentArray();
			} else {
				currentParentArray = new Array();
				currentParent = '';
			}
		} else {
			// no more windows so reset window management variables
			newWindow = null;
			currentElementContent = '';
			currentWindow = -1;
		}
		if(currentWindow < 0) {
			topLevelZOrder = INITIAL_WIN_Z_ORDER;
		}
	} catch (err){
		top.reportException(err, "dialogManagement - closeWindow");
	}
}

/*
 * closes ALL windows
 */
function closeAllWindows() {
	try{
		for(windownumber = currentWindow; windownumber >=0 ; windownumber --){
			closeWindow(windownumber);
		}
		topLevelZOrder = INITIAL_WIN_Z_ORDER;
	} catch (err){
		top.reportException(err, "dialogManagement - closeAllWindows");
	}
}

// -----------------------------------

/*
 * loads a desktop
 */
function launchDesktop(context, url) {
	try {
		top.mainContentFrame.location.href = context + url;
	} catch(er) {
		top.reportException(er,"desktopManagement - launchDesktop");
	}
}

// -------------------------------------

/*
 * Add window to current Parent Array
 * new parent window object to add
 */
function addToCurrentParentArray(currParentWin) {
	try {
		currentParentArray.push(currParentWin);
	} catch (err) {
		top.reportException(err, "dialogManagement - addToCurrentParentArray");
	}
}

/*
 * retrieve last window object from parent array
 * returns last added parent window object
 */
function returnLastCurrentParentArray() {
	try {
		return currentParentArray.pop();
	} catch (err) {
		top.reportException(err, "dialogManagement - returnLastCurrentParentArray");
	}
}

/*
 * Clear current Parent Array
 */
function clearCurrentParentArray() {
	try {
		currentParentArray = new Array();
	} catch (err) {
		top.reportException(er, "dialogManagement - clearCurrentParentArray");
	}
}
	
// -------------------------------------

/*
 * escape message and then alert it
 * message to alert
 */
function alertMSG(msg) {
	try {
		msg = unescape(msg);
		if(msg != "null"){
			alert(msg);
		}
		processingLaunch='';
	} catch (err) {
		top.reportException(er, "dialogManagement - alertMSG");
	}
}

// -------------------------------------

/*
 * popup window movement event handler
 * element to move
 * div element (title bar)
 * window number being moved
 * event object for the action
 */
function beginDrag(element, titleDiv, windowNumber, event){
	try{
	
		var elementToDrag = top.document.getElementById('Win' + windowNumber);
		var movePane = top.document.getElementById('moveWindowDiv');
		if(upHandler){
			upHandler();
		}
		if(movePane != null && elementToDrag != null && event != null){
		
			movePane.style.left = 0;
			movePane.style.top = 0;
			movePane.style.width = window.screen.availWidth;
			movePane.style.height = window.screen.availHeight;
			movePane.style.visibility = "visible";
			movePane.style.zIndex=10000;
			movePane.style.cursor="move";
			
			var windowText = '';
			if(titleDiv != null){
				windowText = titleDiv.innerHTML;
			}
		
			var docToUse = movePane;
			movePane.curxmouse = event.screenX;
			movePane.curymouse = event.screenY;
			
			if(titleDiv != null){
				titleDiv.innerHTML = windowText + " - Moving...";
			}
			
			if(elementToDrag.style.zIndex < topLevelZOrder){
				elementToDrag.style.zIndex = topLevelZOrder++;
			}
			
			if(docToUse.addEventListener){
				docToUse.addEventListener("mousemove", moveHandler, true);
				docToUse.addEventListener("mouseup", upHandler, true);	
				docToUse.addEventListener("mousedown", upHandler, true);	
				docToUse.addEventListener("mouseout", moveHandler, true);
				docToUse.addEventListener("click", upHandler, true);
				docToUse.addEventListener("dblclick", upHandler, true);
			} else if(docToUse.attachEvent){
				docToUse.attachEvent("onmousemove", moveHandler);
				docToUse.attachEvent("onmouseup", upHandler);	
				docToUse.attachEvent("onmousedown", upHandler);	
				docToUse.attachEvent("onmouseout", moveHandler);
				docToUse.attachEvent("onclick", upHandler);
				docToUse.attachEvent("ondblclick", upHandler);
			} else {
				//var oldMoveHandler = docToUse.onmousemove;
				//var oldUpHandler = docToUse.onmouseup;
				//var oldOutHandler = docToUse.onmouseout;
				docToUse.onmousemove = moveHandler;
				docToUse.onmouseup =  upHandler;
				docToUse.onmouseout = moveHandler;
				docToUse.onmousedown = upHandler;
				docToUse.onclick = upHandler;
				docToUse.ondblclick = upHandler;
				
			}
			//begin SPRNBA-305
			if(titleDiv.addEventListener){
				titleDiv.addEventListener("mouseup", upHandler, true);	
			} else if(titleDiv.attachEvent){
				titleDiv.attachEvent("onmouseup", upHandler);
			} else {
				titleDiv.onmouseup =  upHandler;
			}
			//end SPRNBA-305
			if(event.stopPropogation) {
				event.stopPropogation();
			} else { 
				event.cancelBubble = true;
			}

			if(event.preventDefault) {
				event.preventDefault();
			} else {
				event.returnValue = false;
			}
			
			function moveHandler(event){
				
				if(!event){
					event = window.event;
				}

				// change the absolute position of the the
				// element to that of the relative new mouse position
				
				var x = parseFloat(elementToDrag.style.left);
				var y = parseFloat(elementToDrag.style.top);
				
				if(movePane.curxmouse < event.screenX){
					x = x - ( movePane.curxmouse - event.screenX );
				} else {
					x = x + ( event.screenX - movePane.curxmouse );
				}

				if(movePane.curymouse < event.screenY){
					y = y - ( movePane.curymouse - event.screenY );
				} else {
					y = y + ( event.screenY - movePane.curymouse );
				}
				
				movePane.curxmouse=event.screenX;
				movePane.curymouse=event.screenY;

				if(titleDiv != null){
					titleDiv.innerHTML = windowText + " - Moving...";
				}
				
				elementToDrag.style.left = x;
				elementToDrag.style.top = y;

				if(event.stopPropogation) {
					event.stopPropogation();
				} else { 
					event.cancelBubble = true;
				}
			}
				
			function upHandler(event){
				try{
					var movePane1 = top.document.getElementById('moveWindowDiv');
					movePane1.style.left = 0;
					movePane1.style.top = 10;
					movePane1.style.width = 0;
					movePane1.style.height = 0;
					movePane1.style.visibility = "hidden";
					movePane1.style.zIndex=0;
				}catch(ex){
				}
			
				try{
					if(!event){
						event = window.event;
					}
	
					if(titleDiv != null && windowText != null){
						titleDiv.innerHTML = windowText;
					}
	
					if(docToUse.removeEventListener){
						docToUse.removeEventListener("mouseup",upHandler,true);
						docToUse.removeEventListener("mouseout",moveHandler,true);
						docToUse.removeEventListener("mousemove",moveHandler,true);
						docToUse.removeEventListener("mousedown", upHandler, true);
						docToUse.removeEventListener("click", upHandler, true);
						docToUse.removeEventListener("dblclick", upHandler, true);
						
					} else if(docToUse.detachEvent){
						docToUse.detachEvent("onmouseup", upHandler);
						docToUse.detachEvent("onmousedown", upHandler);
						docToUse.detachEvent("onmouseout", moveHandler);
						docToUse.detachEvent("onmousemove", moveHandler);
						docToUse.detachEvent("onclick", upHandler);
						docToUse.detachEvent("ondblclick", upHandler);
					} else {
						docToUse.onmouseup = null;
						docToUse.onmousedown = null;
						docToUse.onmouseout = null;
						docToUse.onmousemove = null;
						docToUse.onclick = null;
						docToUse.ondblclick = null;
					}
					
					
					if(event.stopPropogation) {
						event.stopPropogation();
					} else { 
						event.cancelBubble = true;
					}
				}catch(exp){
				}
			}
		}
	} catch (err){
		alert("beginDrag " + err.description);
	}
}

function insertHTML(element, htmlContent, position){
	if(element != null){
		if(position == null){
			position = "BeforeEnd";
		}
		try{
			if(element.insertAdjacentHTML != null){
				element.insertAdjacentHTML(position, htmlContent);
			} else {
				// OK Not IE so do it the hard way			
				if(position == "BeforeEnd"){
					element.innerHTML += htmlContent;
				} else {
					var content = element.parentNode.innerHTML;
					var pos = content.indexOf(elememt.innerHTML);
					if(pos != -1){
						content = content.substr(0, pos + elememt.innerHTML.length()) + htmlContent + content.substr(pos + elememt.innerHTML.length() + 1);
					}
					element.parentNode.innerHTML = content;
				}
			}
		}catch(err){
			top.reportException(err, "insertHTML");
		}
	}
}

	function refreshCurrentWindow(){
		try{
			newWindow.document.location.href = newWindow.document.location.href;
		} catch(err){
		}
	}
	
	function showNamedWindow(windowname){
		try{
			var elementContent = top.document.getElementById('Win' + windowname);
			if(elementContent != null){
				alert(top.currentNamedWindowsList[windowname]);
				var namedContentFrame = top.currentNamedWindowsList[windowname];
				if(namedContentFrame != null){
					alert(namedContentFrame);
					alert(namedContentFrame.name);
					alert(namedContentFrame.id);
					namedContentFrame.showWindow();
				}
			}
		}catch(err){
		}
	}
	
	function hideNamedWindow(windowname){
		try{
			var elementContent = top.document.getElementById('Win' + windowname);
			if(elementContent != null){
				var namedContentFrame = currentNamedWindowsList[windowname];
				if(namedContentFrame != null){
					namedContentFrame.hideWindow();
				}
			}
		}catch(err){
		}
	}

	function hasNamedWindow(windowname){
		try{
			alert(windowname);
			var elementContent = top.document.getElementById('Win' + windowname);
			alert(elementContent);
			if(elementContent != null){
				return true;
			}
		}catch(err){
		}
		return false;
	}
	
	function getNamedWindow(windowname){
		return currentNamedWindowsList[windowname];
	}

	
	function addNamedWindowInternal(windowname, context, url, targetparent){

	windowLaunching =  true;
	
	var width = -2;
	var height = -2;
		
		var padHeight = 28;
		var padWidth = 2;
		
		if(newWindow != null) {
			if (currentParent != null && currentParent != '') {
				addToCurrentParentArray(currentParent);
			}
			currentParent = newWindow;
		}
		if(targetparent == null || targetparent == '') {
			targetparent = this;
		}
		currentDialogContext = context;

		var widthToUse = width + padWidth;
		var heightToUse = height + padHeight;
		var targetFrameWidth = width;
		var targetFrameHeight = height-3;
		var centerBarWidth = widthToUse;
		
	//var targetLeft = ((screen.availWidth-20) / 2) - (widthToUse / 2) - (padWidth /2);
	//var targetTop = (screen.availHeight /2) - (heightToUse /2) - (padHeight /2);

	//var targetLeft = -1000;
	//var targetTop = 0;
	var targetLeft = 200;
	var targetTop = 200;

		insertHTML( winList,
			'<DIV ID="Win' + windowname + '"' + onkeyCaptureFunctionScript +
			' style="background:transparent; position:absolute;zIndex:1000;left:' + targetLeft + ';top:' + targetTop + ';width:' + widthToUse + ';height:' + heightToUse + ';visiblity:hidden" moving="false" curxmouse=0 curymouse=0 >' +
			'<IFRAME style="background:transparent;position:relative;" frameborder="0" id="FWin'+ windowname + '" name="FWin'+ windowname + '" width="' + widthToUse + '" height="' + heightToUse + '" HSPACE="0" VSPACE="0" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="NO" ATOMICSELECTION="true"/' + onkeyCaptureFunctionScript + '>' + 
			'</DIV>');

		currentElementContent = top.document.getElementById("FWin" + windowname);
		currentElementContent.window = this;
		currentElementContent.window.opener = targetparent;

		var urltouse = context + "/" + url;
		var basePath = "";
		if(window.document.location.port != ""){
			basePath = window.document.location.protocol + "//" + window.document.location.hostname +":" + window.document.location.port + context;
		} else {
			basePath = window.document.location.protocol + "//" + window.document.location.hostname + context;
		}
		
	
		currentElementContent.contentWindow.document.write(
			'<HTML><HEAD>' +
			documentKeyCaptureFunctionScript +			
			'<LINK REL="stylesheet" TYPE="text/css" HREF="' + context+ '/css/accelerator.css"></LINK></HEAD><BODY></BODY></HTML>');
		
	currentElementContent.contentWindow.document.oncontextmenu = new Function("return false;");
	
		insertHTML(currentElementContent.contentWindow.document.documentElement,	
			'<BODY class="windowTable" ' + onkeyCaptureFunctionScript + ' >' +
			'<div ' + onkeyCaptureFunctionScript + '>' +
			'<table class="windowTable" CELLSPACING="0" CELLPADDING="0" border="0" width="100%" >' + 
				'<tr style="background: transparent;">' + 
					'<td style="background: transparent;">' +
						'<table class="windowTable" CELLSPACING="0" CELLPADDING="0">' +
							'<tr style="border:none;">' + 
								'<td ALIGN="center">' + 
									'<div UNSELECTABLE="on" onselectstart="return false;">' +								
									'<table width="100%" class="windowBar" CELLSPACING="0" CELLPADDING="0" onmousedown="top.beginDrag(this, document.all.Title' + windowname + ',  \'' + windowname + '\', window.event);">' + 
										'<tbody><tr class="windowBar">'+
											'<td class="windowBarLeft" onselectstart="return false;"></td>' +
											'<td id="centerBar' + windowname + '" class="windowBarCenter" style="width:' + centerBarWidth + 'px" onselectstart="return false;">' + 
												'<span ATOMICSELECTION="true" class="textWindowBar" id="Title' + windowname + '" onmousedown="return false;" onmouseover="return false;" onselectstart="return false;" >Please wait ...</span>' +
											'</td>' +
											'<td class="windowBarRight" onselectstart="return false;"></td>' +
										'</tr>' +
									'</table>' +
									'</div>'+
								'</td>' +
							'</tr>' +
							'<tr style="border:none;" class="updatePanel">' +
								'<td>' + 
								'<iframe name="Content' + windowname + '" class="windowPane" id="Content' + windowname + '" frameborder="0" SCROLLING="NO" width="' + targetFrameWidth + '" height="' + targetFrameHeight + '" onblur="if(this.contentWindow.windowname == parent.windowname ){ if (parent.sysWindowlaunched == true){ parent.sysWindowlaunched = false; } else { this.focus();}}"/>' +
								'</td>' +
							'</tr>' + 
						'</table>'+
					'</td>'+
				'</tr>'+
			'</table>'+
			'</div>' +
			'</BODY>');
		var targetFrameElement = currentElementContent.contentWindow.document.getElementsByTagName("iframe")[0];
		targetFrameElement.contentWindow.document.write(
		'<HTML><HEAD>' +
			'<LINK REL="stylesheet" TYPE="text/css" HREF="' +  context + '/css/accelerator.css">' +
			documentKeyCaptureFunctionScript +
			'</HEAD>' +
			'<BODY class="updatePanel" leftmargin="0" topmargin="0" ' + onkeyCaptureFunctionScript + '>' +
			'</BODY></HTML>');
		
		targetFrameElement.contentWindow.location.href = urltouse;
		targetFrameElement.contentWindow.opener = targetparent;
		

		var currentElement = top.document.getElementById('Win' + windowname);
		currentElement.style.position = "absolute";
		currentElement.style.zIndex = topLevelZOrder++;
		currentElement.style.visibility = "visible";

		var namedWindow = currentElementContent.contentWindow.document.getElementsByTagName("iframe")[0].contentWindow;
		currentNamedWindowsList[windowname] = namedWindow;
		namedWindow.currentWindow = windowname;
		namedWindow.titleElement = eval("currentElementContent.contentWindow.document.all.Title" + windowname);

		window.top.focus(); 			

	}
	
	//SPR3356 new method
  function getInnerHTML(innerText){
      var regex=new Array(4); 
		     regex[0]=/<UL>/gi;
		     regex[1]=/<\/UL>/gi;
		     regex[2]=/<LI>/gi;
		     regex[3]=/<\/LI>/gi;  	
		     var message=innerText;		    
		    for(var count=0;count<regex.length;count++)
		    {		    
		         message=message.replace(regex[count], "");
		    }  
		    return message;	
	}	    
		
  //NBA356 new Method
  function showWindowFromAppExt(context, url, targetparent){
	  oldCurrentWindow = currentWindow;
	  oldNewWindow = newWindow;
	  top.hideWait();
	  top.showWait("Launching Dialog " + url);
	  showWindowInternal(context,url,targetparent,false);
	  window.setTimeout(checkIfLaunched, LAUNCH_DIALOG_WAIT);
	  processingLaunch = '';
  }  