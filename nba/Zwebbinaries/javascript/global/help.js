

function showAccelHelp(helpContextId, helpPrefix) {
	if(helpContextId == null || helpContextId == ""){
		 // no valid context specified so launch default
		helpContextId = 0;
	}
	if(top.RH_ShowHelp != null){
		if(helpContextId == 0){
			top.RH_ShowHelp(0, helpPrefix + ">" + top.helpWindowName, top.HH_DISPLAY_TOPIC, 0);
		} else {
			top.RH_ShowHelp(0, helpPrefix + ">" + top.helpWindowName, top.HH_HELP_CONTEXT, helpContextId);		
		}
	} else {
		alert("nohelp");
	}
}

function showAccelInfoBrowser(helpContextId, helpPrefix) {
	if(helpContextId == null || helpContextId == ""){ // no valid context specified so launch default
		helpContextId = 0;
	}
	if(top.RH_ShowHelp != null){
		if(helpContextId == 0){
			top.RH_ShowHelp(0, helpPrefix + ">" + top.infoBrowserWindowName, top.HH_DISPLAY_TOPIC, helpContextId);
		} else {
			top.RH_ShowHelp(0, helpPrefix + ">" + top.infoBrowserWindowName, top.HH_HELP_CONTEXT, helpContextId);		
		}
	} else {
		alert("nohelp");
	}
}