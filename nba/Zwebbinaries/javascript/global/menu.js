/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/
 <!-- CHANGE LOG -->
 <!-- Audit Number   Version   Change Description -->
 <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->

document.onkeydown = cancelBack; 
document.onhelp = helpExit; 

//function to disable the context menu
	document.oncontextmenu = new Function("if (!top.enableContextMenu) { return false; }");


	function helpExit(){
		window.event.cancelBubble = false;
		window.event.keyCode = 0;
		window.event.returnValue = false;
		return false;
	}

	function cancelBack(){
		
		if(top.inProgress){
			window.event.cancelBubble = false;
			window.event.keyCode = 0;
			window.event.returnValue = false;
		} else {
			// first check for F1 and intercept HELP
			if (window.event.keyCode == top.F1_KEY){
				top.showAccelHelp(top.defaultHelpContext);
				window.event.cancelBubble = false;
				window.event.keyCode = 0;
				window.event.returnValue = false;
			} else {
				// check for either ALT or (BACKSPACE and NOT edit field) or ctrl-N or ctrl-R or F5
				if ( window.event.altKey || 
					(window.event.keyCode == top.BACKSPACE_KEY && (window.event.srcElement.type != 'text' && window.event.srcElement.type != 'textarea' && window.event.srcElement.type != 'password')) || 
				    (window.event.ctrlKey && ( window.event.keyCode == top.N_KEY || window.event.keyCode == top.R_KEY)) || 
				    (window.event.keyCode == top.F5_KEY) || (window.event.keyCode == top.F11_KEY)) {
				    	if (window.event.keyCode == top.F5_KEY) top.refreshDT();
						window.event.cancelBubble = false;
						window.event.keyCode = 0;
						window.event.returnValue = false;
		 	 	}
	
				// check for ctrl-R, ctrl-N and F5 in edit fields	 	 	
		 	 	if ((window.event.srcElement.type == 'text' || window.event.srcElement.type == 'textarea' || window.event.srcElement.type == 'password') && 
				    (window.event.ctrlKey && (window.event.keyCode == top.N_KEY || window.event.keyCode == top.R_KEY)) ||
				    (window.event.keyCode == top.F5_KEY) || (window.event.keyCode == top.F11_KEY)){
				    	if (window.event.keyCode == top.F5_KEY) top.refreshDT();
						window.event.cancelBubble = false;
						window.event.keyCode = 0;
						window.event.returnValue = false;
				}
			}
		}
	}
	

/*
 * Initializes a file page
 */
function menuBarInit(){
	try{
		if (document.getElementById("Messages") != null){
		   //Begin SPR3356	
		   var innerText=document.getElementById("Messages").innerHTML;
		   innerText=top.getInnerHTML(innerText);		    
		   document.getElementById("Messages").innerHTML= innerText;  		   
		   //End SPR3356 
			if (document.getElementById("Messages").innerHTML != ""){
				top.showWindow(top.basePath,'faces/error.jsp', this);
			}
		}
	} catch (err){
		top.reportException(err, "filePageInit for file - " + window.document.title);
	}
}

