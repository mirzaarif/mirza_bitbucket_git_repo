<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB016		    NB1101	  Federated Configuration Changes -->

var oWindow;

	function launchAcceleratorProduct(applicationTitle){
		if(applicationTitle == null){
			applicationTitle = 'Accelerator Desktop';
		}
		openWindowCloseSelf('applicationFrameset.html?embedded=true', applicationTitle);
	}
        
	function openWindowCloseSelf(url,name){
		var scrtop = 0; //window.screen.availTop;
		var scrleft = 0; //window.screen.availLeft;
            
       	var scrwidth = 10;
       	var scrheight = 10;
       	//"height=" + scrheight + ",width=" + scrwidth + ",left=" +scrleft + ",top=" + scrtop +",status=no,toolbar=no,menubar=no,location=no"
		oWindow = window.open(url,"",
		"height=" + scrheight + ",width=" + scrwidth + ",left=" +scrleft + ",top=" + scrtop +",status=no,toolbar=no,menubar=no,location=no");
	}

	  
	//FNB016 new method
	function closeSelf(){
		window.open('','_self','');
		self.opener=null;
		self.close();
	}