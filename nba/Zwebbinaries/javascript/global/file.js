/*************************************************************************
 *
 * Copyright Notice (2004)
 * (c) CSC Financial Services Limited 1996-2004.
 * All rights reserved. The software and associated documentation
 * supplied hereunder are the confidential and proprietary information
 * of CSC Financial Services Limited, Austin, Texas, USA and
 * are supplied subject to licence terms. In no event may the Licensee
 * reverse engineer, decompile, or otherwise attempt to discover the
 * underlying source code or confidential information herein.
 *
 *************************************************************************/
  <!-- CHANGE LOG -->
  <!-- Audit Number   Version   Change Description -->
  <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->
  <!-- SPRNBA-947 	NB-1601 	Null Pointer Exception May Occur in Inbox if User Selects Different Work -->
  <!-- SPRNBA-976	NB-1601     Null session variables result from overlapping transactions initiated on the To-Do List user interface -->
 

document.onkeydown = cancelBack; 
document.onhelp = helpExit; 

//function to disable the context menu
	document.oncontextmenu = new Function("if (!top.enableContextMenu) { return false; }");


	function helpExit(){
		window.event.cancelBubble = false;
		window.event.keyCode = 0;
		window.event.returnValue = false;
		return false;
	}

	function cancelBack(event){
		if(event == null){
			event = window.event;
		}
		// keycode constants
		var F1_KEY = 112;
		var BACKSPACE_KEY = 8;
		var F5_KEY = 116;
		var F11_KEY = 122;
		var R_KEY = 82;
		var N_KEY = 78;

		if(top.inProgress){
			event.cancelBubble = false;
			event.keyCode = 0;
			event.returnValue = false;
		} else {		
			// first check for F1 and intercept HELP
			if (event.keyCode == F1_KEY){
				top.showAccelHelp(top.defaultHelpContext);
				event.cancelBubble = false;
				event.keyCode = 0;
				event.returnValue = false;
			} else {
				// check for either ALT or (BACKSPACE and NOT edit field) or ctrl-N or ctrl-R or F5
				if ( event.altKey || 
					(event.keyCode == BACKSPACE_KEY && (event.srcElement.type != 'text' && event.srcElement.type != 'textarea' && event.srcElement.type != 'password')) || 
				    (event.ctrlKey && ( event.keyCode == N_KEY || event.keyCode == R_KEY)) || 
				    (event.keyCode == F5_KEY) || (event.keyCode == F11_KEY)) {
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
		 	 	}
	
				// check for ctrl-R, ctrl-N and F5 in edit fields	 	 	
		 	 	if ((event.srcElement.type == 'text' || event.srcElement.type == 'textarea' || event.srcElement.type == 'password') && 
				    (event.ctrlKey && (event.keyCode == N_KEY || event.keyCode == R_KEY)) ||
				    (event.keyCode == F5_KEY)|| (event.keyCode == F11_KEY)){
				    	if (event.keyCode == F5_KEY) top.refreshDT();
						event.cancelBubble = false;
						event.keyCode = 0;
						event.returnValue = false;
				}
			}
		}
	}
	

/*
 * Initializes a file page
 */
function filePageInit(){
		initMessagesAndScroll(); //SPRNBA-947
		top.hideWait();
		//begin SPRNBA-976
		if (top.todoPopup && !top.todoPopup.closed) {
			top.todoPopup.top.hideWait();
		}
		//end SPRNBA-976
}

	/*
	 * Scrolls the current window to the given x,y coordinates
	 */
	 //NBA213 New Function
	function scrollToCoordinates(formName) {
		window.scrollTo(document.forms[formName][formName+':scrollx'].value,
						document.forms[formName][formName+':scrolly'].value); 
	} 

	/*
	 * Captures the x,y coordinates for the scroll bar position in current window.
	 */
	//NBA213 New Function
	function getScrollXY(formName) {
		var scrOfX = 0, scrOfY = 0;
		if( typeof( window.pageYOffset ) == 'number' ) {
			scrOfY = window.pageYOffset; scrOfX = window.pageXOffset;
		} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
			scrOfY = document.body.scrollTop; scrOfX = document.body.scrollLeft;
		} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
			scrOfY = document.documentElement.scrollTop; scrOfX = document.documentElement.scrollLeft;
		}
		document.forms[formName][formName+':scrollx'].value = scrOfX;
		document.forms[formName][formName+':scrolly'].value = scrOfY;
	}
	/*
	 * Display any messages. 
	 * Set scrolling
	 */
	//SPRNBA-947 New Function - code refactored from filePageInit()
	function initMessagesAndScroll(){
		try{
			top.numRrefreshes--;
			if (document.getElementById("Messages") != null){
			    //Begin SPR3356	
			   var innerText=document.getElementById("Messages").innerHTML;
			   innerText=top.getInnerHTML(innerText);		    
			   document.getElementById("Messages").innerHTML= innerText;  		   
			   //End SPR3356    
				if (document.getElementById("Messages").innerHTML != ""){
					top.showWindow(top.basePath,'faces/error.jsp', this);
				}
			}
		} catch (err){
			top.reportException(err, "filePageInit for file - " + window.document.title);
		}

		try{
			if(document.body.scrollHeight > 0){
				if(window.parent != null){
					if(window.parent.document.all.file != null){
						if(window.document.body.scrollHeight > window.parent.document.all.file.document.body.clientHeight){
							window.parent.document.all.file.scrolling = "yes";
						} else {
							window.parent.document.all.file.scrolling = "no";
						}
					}
				}
			}
		}catch(err1){
			// do nothing
			try{
				window.parent.document.all.file.scrolling = "no";
			}catch(err2){
			}
		}		
	
	}
