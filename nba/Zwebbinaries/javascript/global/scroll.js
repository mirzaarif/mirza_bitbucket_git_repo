<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA213           7	  		Unified User Interface -->		

/**
 * Retrieves the current scrolling position of the divID html element and
 * stores the value in a html element represented by scrollPosID.
 */
function saveScrollPosition(divID, scrollPosID) {
	var divElement = document.getElementById(divID);
	if (divElement != null) {
		var scrollPos = document.getElementById(scrollPosID);
		scrollPos.value = divElement.scrollTop;
	}
}

/**
 * Scrolls a divID html element to a vertical position stored in the value
 * of a html element represented by scrollPosID.
 */
function scrollToPosition(divID, scrollPosID) {
	var divElement = document.getElementById(divID);
	if (divElement != null) {
		var scrollPos = document.getElementById(scrollPosID);
		divElement.scrollTop = scrollPos.value;
	}
}