//-- Keyboard Support
var keystrokes = 1;
var dm_actKey  = 113;
var dm_focus   = 1;

var dm_writeAll=1;

//-- Delays
var smShowPause = 200;
var smHidePause = 1000;

//-- Submenus appearance
var smViewType = 0;
var smColumns = 1;
var smWidth   = 0;
var smHeight  = 0;

// -- CSS Support
var cssStyle     = 0;
var cssSubmenu   = '';
var cssItem      = ['',''];
var cssItemText  = ['',''];

//------- Common -------
var smOrientation = 0;
var noWrap = 1;
var isHorizontal = 1;
var saveNavigationPath = 1;
var showByClick = 1;
var pressedItem = -2;
var blankImage = "javascript/menu/images/blank.gif";
var statusString = "link";

var pathPrefix_img = "javascript/menu/images/";
var pathPrefix_link = "/";


//------- Menu -------
var menuWidth = "";
var menuBorderWidth = 0;
var menuBorderStyle = "solid";
var menuBackImage = "";

//------- Menu Positioning -------
var absolutePos = 0;
var posX = 0;
var posY = 0;

//------- Floatable -------
var floatable = 0;
var floatIterations = 6;
var floatableX=1;
var floatableY=1;

//------- Movable -------
var movable = 0;
var smMovable = 0;
var moveWidth = 12;
var moveHeight = 20;
var moveCursor = "default";
var moveImage = "";
var moveColor = "#AAAAAA";

//------- Submenu Positioning -------
var topDX = 0;
var topDY = 3;
var DX = 3;
var DY = 0;

//------- Font -------
var fontStyle = "normal 11px Verdana";
var fontColor = ["#FFFFFF","#3B3B3B"];
var fontDecoration = ["none","none"];
var fontColorDisabled = "#AAAAAA";

//------- Items -------
var itemBorderWidth = 0;
var itemBorderStyle = ["solid","solid"];
var itemBackImage = ["",""];
var itemAlign = "left";
var subMenuAlign = "";
var itemSpacing = 2;
var itemPadding = 3;
var itemCursor = "pointer";
var itemTarget = "_blank";

//------- Colors -------
var menuBackColor = "#3B3B3B";
var menuBorderColor = "#5CCBE2";
var itemBackColor = ["#3B3B3B","#3B3B3B"];
var itemBorderColor = ["#E4E1DE","#3B3B3B"];

//------- Icons -------
var iconTopWidth = 24;
var iconTopHeight = 24;
var iconWidth = 30;
var iconHeight = 15;
var arrowImageMain = ["arrow_main3.gif","arrow_main3.gif"];
var arrowImageSub = ["arrow_sub5.gif","arrow_sub5.gif"];
var arrowWidth = 11;
var arrowHeight = 11;

//------- Separators -------
var separatorWidth = "100%";
var separatorHeight = "3";
var separatorAlignment = "left";
var separatorImage = "";
var separatorVWidth = "3";
var separatorVHeight = "100%";
var separatorVImage = "";

//------- Visual Effects -------
var transparency = "95";
var transition = 26;
var transDuration = 0;
var transOptions = "";
var shadowLen = 0;
var shadowTop = 0;
var shadowColor = "#3B3B3B";


var itemStyles = [
    ["fontStyle=normal 11px Verdana", "itemBackColor=#3B3B3B,#C6D3EF"],
];
