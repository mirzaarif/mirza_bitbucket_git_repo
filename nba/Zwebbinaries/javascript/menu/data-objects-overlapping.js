
//------- Keyboard Support -------
var keystrokes = 1;

//------- Common -------
var smOrientation = 0;
var noWrap = 1;
var isHorizontal = 1;
var saveNavigationPath = 1;
var showByClick = 0;
var pressedItem = -2;
var blankImage = "menu/images/blank.gif";
var pathPrefix_img = "menu/images/";
var pathPrefix_link = "/";
var statusString = "link";

//------- Menu -------
var menuWidth = "";
var menuBorderWidth = 1;
var menuBorderStyle = "dotted";
var menuBackImage = "";

//------- Menu Positioning -------
var absolutePos = 0;
var posX = 0;
var posY = 0;
var floatable = 0;
var floatIterations = 6;
var movable = 0;
var moveWidth = 12;
var moveHeight = 20;
var moveCursor = "default";
var moveImage = "";

//------- Submenu Positioning -------
var topDX = 0;
var topDY = 3;
var DX = 3;
var DY = 0;

//------- Font -------
var fontStyle = "normal 11px Verdana";
var fontColor = ["#FFFFFF","#3B3B3B"];
var fontDecoration = ["none","none"];

//------- Items -------
var itemBorderWidth = 0;
var itemBorderStyle = ["solid","solid"];
var itemBackImage = ["",""];
var itemAlign = "left";
var subMenuAlign = "";
var itemSpacing = 1;
var itemPadding = 5;
var itemCursor = "default";
var itemTarget = "_blank";

//------- Colors -------
var menuBackColor = "#3B3B3B";
var menuBorderColor = "#5CCBE2";
var itemBackColor = ["#BFFFFC","#FFFFFF"];
var itemBorderColor = ["#E4E1DE","#FFFFFF"];

//------- Icons -------
var iconTopWidth = 24;
var iconTopHeight = 24;
var iconWidth = 30;
var iconHeight = 15;
var arrowImageMain = ["arrow_main1.gif","arrow_main1.gif"];
var arrowImageSub = ["arrow_sub1.gif","arrow_sub1.gif"];
var arrowWidth = 11;
var arrowHeight = 11;

//------- Separators -------
var separatorWidth = "100%";
var separatorHeight = "3";
var separatorAlignment = "left";
var separatorImage = "";
var separatorVWidth = "3";
var separatorVHeight = "100%";
var separatorVImage = "";

//------- Visual Effects -------
var transparency = "85";
var transition = 26;
var transDuration = 300;
var transOptions = "";
var shadowLen = 5;
var shadowTop = 0;
var shadowColor = "#BBBBBB";


var itemStyles = [
    ["fontStyle=bold 12px Arial, Tahoma", "itemBackColor=#D3F7FF,#FFFFFF"],
];


var menuItems = [

    ["Home","testlink.htm",,,,, "0"],
    ["Product Info","",,,,, "0"],
        ["|Features","testlink.htm"],
        ["|Installation",""],
            ["||Description of Files","testlink.htm"],
            ["||How To Setup","testlink.htm"],
        ["|Parameters Info","testlink.htm"],
        ["|Dynamic Functions","testlink.htm"],
        ["|Supported Browsers",""],
            ["||Windows OS",""],
                ["|||Internet Explorer",""],
                ["|||Firefox",""],
                ["|||Mozilla",""],
                ["|||Opera",""],
                ["|||Netscape Navigator",""],
            ["||MAC OS",""],
                ["|||Firefox",""],
                ["|||Safari",""],
                ["|||Internet Explorer",""],
            ["||Unix/Linux OS",""],
                ["|||Firefox",""],
                ["|||Konqueror",""],
    ["Samples","",,,,, "0"],
        ["|Sample 1","testlink.htm"],
        ["|Sample 2","testlink.htm"],
        ["|Sample 3","testlink.htm"],
        ["|Sample 4","testlink.htm"],
        ["|Sample 5","testlink.htm"],
        ["|Sample 6","testlink.htm"],
    ["Purchase","testlink.htm",,,,, "0"],
    ["Contact Us","testlink.htm",,,,, "0"],
];

dm_init();
