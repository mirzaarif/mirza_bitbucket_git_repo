//***********************************************
//
//  Javascript Menu (c) 2006, by Deluxe-Menu.com
//  version 1.71
//  E-mail:  cs@deluxe-menu.com
//
//***********************************************
//
// Obfuscated by Javascript Obfuscator
// http://javascript-source.com
//
//***********************************************


function dm_ext_changeItem(mi,ci,iy,dip){var dm=dm_menu[mi],ce=dm.m[ci],iv=ce.i[iy],co=_dmni(ce);with(iv){text=(dip[0]?dip[0]:text);link=_dmll(dip[1]);target=_dmsl(dip[5]);status=_dmst(statusString,text,link);tip=_dmpr(dip[4],'');itt=_dmls(dm,dip[6]);dii=_dmkl([_dmpr(dip[2],dii[0]),_dmpr(dip[3],dii[1])],pathPrefix_img);dss=(target=='_')?1:0;_dmoi(iv.id+'tdText').innerHTML=text;with(_dmoi(id+'tbl')){yr=tip;style.width=itt.iw;};};_dmh(iv,0);};function _dme(){if(_o&&_v<7)return alert("Deluxe Menu (http://deluxe-menu.com): "+String.fromCharCode(13)+"This browser doesn't support dynamic functions.");};function dm_ext_addItem(mi,ci,dpa){_dme();var dm=dm_menu[mi],ce=dm.m[ci],iy=ce.i.length;_dmip(dm,ce,iy,dpa,statusString);var iv=ce.i[iy];_dmni(ce);var co=_dmoi(ce.id+'tbl'),dfr=(iv.ci&&dm.dcf&&dt==1)?'parent.frames['+dm.dim+'].':'',io=document.createElement('TD');io.innerHTML=_dmit(dm,ce,iv,dfr);with(co)var ow=(ce.dhz)?rows[0]:insertRow(rows.length);ow.appendChild(io);};function dm_ext_deleteItem(mi,ci,iy){_dme();var ce=dm_menu[mi].m[ci],iv=ce.i[iy];_dmni(ce);var itd=_dmoi(iv.id+'td');itd.style.display='none';iv.ded=1;};function dm_ext_changeItemVisibility(mi,ci,iy,vis){_dme();var ce=dm_menu[mi].m[ci],iv=ce.i[iy];_dmni(ce);var itd=_dmoi(iv.id+'td');itd.style.display=(vis?'':'none');iv.qiv=vis;};function dm_ext_getItemParams(mi,ci,iy){with(dm_menu[mi].m[ci].i[iy])var dip=[id,(dcd?1:0),text,link,target,status,tip,da,dii,dss,dpr,qiv,ded];return dip;};function dm_ext_getSubmenuParams(mi,ci){with(dm_menu[mi].m[ci])var cp=[id,i.length,qri,le,dhz];return cp;};function dm_ext_getMenuParams(mi){with(dm_menu[mi])var _mp=[m.length,dcs,dcp];return _mp;};
