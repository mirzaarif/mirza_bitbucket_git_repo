
// SPR1020/Version 2/JSPs should display a message to the user, 
// if browser security settings do not permit ActiveX controls to run. 
// NBA007/Version 2/View All Sources on Underwriter Workbench
// NBA027/Version 3/Performance Tuning
//SPR2679 removed the reference of SPR2254
//NBA283/Version-1301/AWD10 Upgrade

//Define global variables here
var oImgWin	 = null; // Reference to active X Object //NBA096
var sourceIdArr = new Array(); // This array caches all the open sources so if the same source is opened next time, //NBA096
var message = null; //SPR2679


//SPR2679 new method
//	This method is used to initialize ActiveXObject.
function initImage(){
	message	= "Tiff Image Viewer Startup ERROR.\n"; 
    message += "Please change browser security ";					
    message += "options and restart your browser."; 
	oImgWin	= new ActiveXObject( "AWDCView.AWDCViewer" );//NBA283
	oImgWin.SetUser(userId, password );
	oImgWin.SetNetServer(netserverIp, netserverPort);

}
// it just maximizes the one open in the image viewer rather than opening a new one. 
/*
	This method should be used when we need to open only single source. 
	To open multiple sources in loop see openImages
*/
//NBA031 parameters changed
function openImage(x,y, width, height, sourceID)
{
   //SPR1020 code deleted
   //begin SPR1020	   
   //SPR2679 code deleted.
   			
	// Get the active X Object for image viewer
	try{	
		//SPR2679 code deleted.
		initImage();												
  	}catch(exception){												
	  //Display a message								
	  alert(message);												
      return;
   }
   //end SPR1020
	
	// NBA096 Code deleted
   //We dont need password.
   //SPR2679 code deleted.
   oImgWin.OpenDocumentsByKey( sourceID );
   sourceIdArr.push(sourceID); // update the list of open source // NBA096
   oImgWin.ExitOnRelease( false ); //SPR2679
   oImgWin.SetAppWindowPos(x,y, width, height);         
}

//NBA096 new method
// This method opens the image if it is not already open,
// if the image is opened than it just maximizes the image so that it can appear in 
// foreground
function openImageIfNotOpen(x,y, width, height, sourceID)
{
   	//SPR2679 code deleted

	// Get the active X Object for image viewer
	try{															
		if(oImgWin == null ) {
		//SPR2679 code deleted.
		initImage(); //SPR2679    		
    	}
	}catch(exception){												
		//Display a message								
		alert(message);												
		return;
	}

	// check if this source is already open, if so maximize the selected source
	// If there is an exception while maximizing it means that the source was manually closed by the user
	// in that case open the source again.
	// While opening the source update the source array cache with the source Id.

	var isPresent = false; // flag to check if this object is present in the array
	for (var i=0;i<sourceIdArr.length;i++ ) {
		if(sourceIdArr[i] == sourceID) {
			isPresent = true; // source is present
			try {
				// Source is present maximize the source
				oImgWin.DocMaximize( sourceID ); 
				return;
			} catch(e) {
				// exception while maximizing, the source might have been closed manually : PageFault
				// or the image viewer application has been closed
				// do nothing and try opening the source again
	    		//SPR2679 code deleted.
				initImage(); //SPR2679  
			}
		}
	}
    // Opening the document back
	oImgWin.OpenDocumentsByKey( sourceID );
	if(!isPresent) { // the source id is not present in the cached list of sources.
		sourceIdArr.push(sourceID); // update the list of open source
	}
	oImgWin.ExitOnRelease( false ); //SPR2679
	oImgWin.SetAppWindowPos(x,y, width, height);         
}

//NBA096 new method
// This method opens the array of images passed as input argument
// takes care of the fact that if the image is already open then 
// it doesnt not open it again, instead it just brings it back to the foreground
function openImages(x,y, width, height, sourceIDArrToOpen) {
	//SPR2679 code deleted.
		 
	// Get the active X Object for image viewer
	try{					
		if(oImgWin == null ) {										
	    	//SPR2679 code deleted.
			initImage(); //SPR2679  
	    }
	}catch(exception){												
		//Display a message								
		alert(message);												
		return;
	}
	
	// loop through all the source id's
	var sourceID = null;
	for (var j=0;j<sourceIDArrToOpen.length;j++ ) {
		sourceID = sourceIDArrToOpen[j];
		// check if this source is already open, if so maximize the selected source
		// If there is an exception while maximizing it means that the source was manually closed by the user
		// in that case open the source again.
		// While opening the source update the source array cache with the source Id.

		var isPresent = false; // flag to check if this object is present in the array
		for (var i=0;i<sourceIdArr.length;i++ ) {
			if(sourceIdArr[i] == sourceID) {
				isPresent = true; // source is present
				try {
					// Source is present maximize the source
					oImgWin.DocMaximize( sourceID ); 
					break;
				} catch(e) {
					// exception while maximizing, the source might have been closed manually : PageFault
					// or the image viewer application has been closed
					// do nothing and try opening the source again
	    			//SPR2679 code deleted.
					initImage(); //SPR2679  
				}
			}
		}
	    // Opening the document back
		oImgWin.OpenDocumentsByKey( sourceID );
		if(!isPresent) { // the source id is not present in the cached list of sources.
			sourceIdArr.push(sourceID); // update the list of open source
		}
	}
	oImgWin.ExitOnRelease( false ); //SPR2679
	oImgWin.SetAppWindowPos(x,y, width, height);         
}

function closeImage() {
	// Get the active X Object for image viewer
	//begin SPR2679
	try{					
		initImage();
	   	oImgWin.ExitOnRelease( true );
		oImgWin	= null;
		sourceIdArr = null; 
	}catch(exception){												
		//Display a message								
		alert(message);												
		return;
	}
	//end SPR2679
}

//NBA007 new method
function closeAllDocuments() {
 try{	
      oImgWin.closeAllDocuments();
	  sourceIdArr = new Array(); // all documents closed //NBA096, NBA153
	  oImgWin = null; //NBA096
   }catch(exception){												
	//do nothing
   }
}
