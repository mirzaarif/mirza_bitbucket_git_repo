<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite  -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3175           6      Browser becomes unavailable with wait cursor -->
<!-- NBA173            7      Indexing View Rewrite-->

// Launch the image viewer displaying all images on the right side of a 1280x1024 desktop
// 659 pixels = (1280 / 2) + 19(scroll bar)
function viewAllImages(sid){	
	var height,width;
	var sids;

	height = window.screen.availHeight;
  	width = window.screen.availWidth - 659;  //SPR2965
	sids = sid.split("#");

	openImages(659, 0 , width, height, sids);  //SPR2965
	top.hideWait();  //SPR3175
}

function viewImage(sid) {
	var height,width;

	height = window.screen.availHeight;
  	width = window.screen.availWidth - 640;  //window.screen.availWidth/2;
	openImage(640, 0 , width, height, sid);
	top.hideWait();  //SPR3175
}

// NBA173 New Method
function prioritizeImage(sid) {
	var height,width;

	height = window.screen.availHeight;
  	width = window.screen.availWidth - 640;  //window.screen.availWidth/2;
	openImageIfNotOpen(640, 0 , width, height, sid);
	top.hideWait();  
}
