<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA168           6         Suspend Rewrite -->
<!-- NBA158 		  6		  	Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<!--  begin NBA213  -->
	<f:view>	
		<head>
			<base href="<%=basePath%>">
			<f:loadBundle basename="properties.nbaApplicationData" var="property" />
			<title><h:outputText value="#{property.suspendTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
				var width=590; 
				var height=300; 
				<!--  end NBA213  -->
				function setTargetFrame() {
					document.forms['form_suspendview'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_suspendview'].target='';
					return false;
				}
			</script>
			<!-- NBA213 code deleted -->
		</head>
		<body onload="popupInit();"> <!--  NBA213  -->
			<PopulateBean:Load serviceName="RETRIEVE_SUSPEND_INFO" value="#{pc_suspendInfo}" />

			<h:form id="form_suspendview">
				<h:panelGroup id="suspendForm" styleClass="updatePanel" style="height: 240px"> <!-- NBA213 -->
					<!--  Person Information -->
					<h:panelGroup styleClass="formDataEntryTopLine">
						<h:outputText id="name" value="#{property.suspendName}" styleClass="formLabel" style="width: 150px"/>
						<h:outputText id="nameText" value="#{pc_suspendInfo.name}" styleClass="formDisplayText" />
					</h:panelGroup>
	
					<f:verbatim>
						<hr class="formSeparator" />
					</f:verbatim>
	
					<!--  Workitem Information -->
					<h:panelGrid columns="2" styleClass="formDataEntryLine" cellspacing="0" cellpadding="0" columnClasses="formLabelTable,formEntryTextFull" > <!--  NBA213 -->
						<h:column>
							<h:outputText id="suspend" value="#{property.suspend}" styleClass="formLabelTable" style="width: 138px; margin-top: 7px;" /> <!--  NBA213 -->
						</h:column>
						<h:column>
							<h:selectOneRadio id="suspendTypeSelection" value="#{pc_suspendInfo.suspendType}" layout="pageDirection" 
											styleClass="formEntryText" style="width: 450px;" disabled="#{pc_suspendInfo.workitemSuspended}"
											valueChangeListener="#{pc_suspendInfo.suspendTypeChange}" onclick="resetTargetFrame();submit();" immediate="true">
								<f:selectItem id="Item1" itemValue="suspendDateAndTime" itemLabel="#{property.suspendDateAndTime}" />
								<f:selectItem id="Item2" itemValue="suspendDays" itemLabel="#{property.suspendDays}" />
							</h:selectOneRadio>
						</h:column>
					</h:panelGrid>
					<h:panelGrid styleClass="formDataEntryLine" columns="3" cellspacing="0" cellpadding="0">
						<h:column>
							<h:inputText id="suspendDays" maxlength="3" value="#{pc_suspendInfo.suspendDays}"
										styleClass="formEntryText" style="width: 90px; margin-left: 150px"
										rendered="#{pc_suspendInfo.suspendForDays}" disabled="#{pc_suspendInfo.workitemSuspended}">
								<f:convertNumber integerOnly="true" type="integer" />
							</h:inputText>
							<h:inputText id="activationDate" value="#{pc_suspendInfo.activationDate}"
										styleClass="formEntryDate" style="width: 100px; vertical-align: top; margin-left: 150px"
										rendered="#{!pc_suspendInfo.suspendForDays}" disabled="#{pc_suspendInfo.workitemSuspended}">  <!-- NBA213 -->
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:inputText>
							<h:inputText id="activationTime" maxlength="5" value="#{pc_suspendInfo.activationTime}"
										styleClass="formEntryText" style="width: 90px; margin-left: 10px"
										rendered="#{!pc_suspendInfo.suspendForDays}" disabled="#{pc_suspendInfo.workitemSuspended}">  <!-- NBA213 -->
							</h:inputText>
						</h:column>
						<h:column>
							<h:selectOneRadio id="ampmSelection" value="#{pc_suspendInfo.suspendTimeType}" layout="lineDirection"
										styleClass="formEntryText" style="width: 100px; margin-left: 10px"
										rendered="#{!pc_suspendInfo.suspendForDays}" disabled="#{pc_suspendInfo.workitemSuspended}">
								<f:selectItem itemValue="suspendTimeAM" itemLabel="#{property.suspendTimeAM}" />
								<f:selectItem itemValue="suspendTimePM" itemLabel="#{property.suspendTimePM}" />
							</h:selectOneRadio>
						</h:column>
						<h:column>
							<!-- Needed force the small column for the am/pm section -->
							<h:outputText style="width: 150px" />
						</h:column>
					</h:panelGrid>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText id="suspendReason" value="#{property.suspendReason}" styleClass="formLabel" style="width: 150px"/>
						<h:selectOneMenu id="suspendReasonText" value="#{pc_suspendInfo.suspendReason}" styleClass="formEntryText" style="width: 400px"
											disabled="#{pc_suspendInfo.workitemSuspended}">
							<f:selectItems value="#{pc_suspendInfo.reasonList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText id="activationStatus" value="#{property.suspendActivationStatus}" styleClass="formLabel" style="width: 150px" />
						<h:selectOneMenu id="statusText" value="#{pc_suspendInfo.activationStatus}" styleClass="formEntryText" style="width: 400px"
											disabled="#{pc_suspendInfo.workitemSuspended}">
							<f:selectItems value="#{pc_suspendInfo.activationStatusList}" />
						</h:selectOneMenu>
					</h:panelGroup>
				</h:panelGroup>
				<!--NBA213 code deleted -->	
				<h:panelGroup styleClass="buttonBar" style="padding-top: 0px;">
					<h:commandButton id="suspendCancel" value="#{property.buttonCancel}" styleClass="buttonLeft" 
						action="#{pc_suspendInfo.cancel}" onclick="setTargetFrame()" /> <!-- NBA213 -->
					<h:commandButton id="suspendCommit" value="#{property.buttonCommit}" styleClass="buttonRight" 
						action="#{pc_suspendInfo.commit}" onclick="setTargetFrame()" 
						disabled="#{pc_suspendInfo.auth.enablement['Commit'] ||
									pc_suspendInfo.notLocked ||
									pc_suspendInfo.workitemSuspended}" /> <!-- NBA213 -->
				</h:panelGroup>
			</h:form>

			<!-- NBA213 code deleted -->
			<div id="Messages" style="display:none"><h:messages /></div>
		</body> <!-- NBA213 -->
	</f:view> <!-- NBA213 -->
</html>