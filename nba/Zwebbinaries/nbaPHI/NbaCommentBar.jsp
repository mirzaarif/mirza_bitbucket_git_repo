<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- FNB004       NB1101      PHI -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> 
	<!--  NBA158 code deleted -->
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:panelGrid id="commentBarPanelGroup1" styleClass="commentBar" cellpadding="0" cellspacing="0" > 
			<h:column id="commentBarColumn"> <!--  NBA152 SPR3558 -->
			<h:panelGroup id="commentBarPanelGroup2" styleClass="commentBar">	
				
				<h:commandButton id="lockClosed" image="images/comments/lock-closed-forbar.gif" title="#{property.addSecureComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('SECURE');top.hideWait();return false;" style="position: absolute; left: 27%"
								rendered="#{pc_commentsPopUpData.secureAddRendered}"/> <!-- NBA152, NBA225, FNB004 -->
				<h:commandButton id="lockOpen" image="images/comments/lock-open-forbar.gif" title="#{property.addGeneralComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('GENERAL');top.hideWait();return false;" style="position: absolute; left: 35%" />
				<h:commandButton id="exclamation" image="images/comments/exclamation-forbar.gif" title="#{property.addSpecialInstruction}" styleClass="commentIcon"
								onclick="launchCommentPopUp('INSTRUCTION');top.hideWait();return false;" style="position: absolute; left: 43%" />
				<h:commandButton id="PHI" image="images/comments/PHI-forbar.gif" title="#{property.addPHIComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('PHI');top.hideWait();return false;" style="position: absolute; left: 50%"  rendered="#{pc_commentsPopUpData.phiRendered}"/>
				
				<h:commandButton id="phone" image="images/comments/phone-forbar.gif" title="#{property.addPhoneRecord}" styleClass="commentIcon"
									onclick="launchCommentPopUp('Phone');top.hideWait();return false;" style="position: absolute; left: #{pc_commentsPopUpData.posPhoneIcon}%" />
				<h:commandButton id="envelope" image="images/comments/envelope-forbar.gif" title="#{property.sendAnEmail}" styleClass="commentIcon"
								onclick="launchCommentPopUp('Email');top.hideWait();return false;" style="position: absolute; left: #{pc_commentsPopUpData.posEmailIcon}%" />

				<h:commandButton id="defaultLabel" value="#{property.addComment}" rendered="#{pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								style="width: 100px;"  onclick="launchCommentPopUp('Secure'); top.hideWait();return false;" /> 
				<h:commandButton id="defaultLabe2" value="#{property.addComment}" rendered="#{!pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								style="width: 100px;"  onclick="launchCommentPopUp('General'); top.hideWait();return false;" /> 
				
		</h:panelGroup>
		</h:column>
	</h:panelGrid> 
</jsp:root>
