<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB004         NB-1101   PHI -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->
<!-- SPRNBA-583	 NB-1301 	  General Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Comments Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		//NBA213 deleted
		function setTargetFrame() {
			//alert('calling set targeet frame');
			document.forms['form_commentsOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_commentsOverview'].target='';
			return false;
		}
		function initPage(){
			filePageInit();
			if (document.getElementById('form_commentsOverview:commentsTable:commentData') != null) {
				document.getElementById('form_commentsOverview:commentsTable:commentData').style.height = window.screen.availHeight - 520;
			}	
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden; overflow-y: hidden"> <!-- SPR2965 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_COMMENTS" value="#{pc_commentsNavigation}" />  <!-- NBA213 -->
		<h:form id="form_commentsOverview">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaCommentNavigation.jsp" />
			</f:subview>
			<!-- begin NBA213 -->
			<h:panelGroup id="checkBoxes" styleClass="sectionSubheader" style="padding-top: 0px"> <!-- NBA225 -->
				<h:selectBooleanCheckbox id="commCB1" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.secure}"
							rendered="#{!pc_nbAAuth.auth.enablement['SecureComment']}" />
				<h:graphicImage value="images/comments/lock-closed-forsectionhead.gif" styleClass="commentIcon"
							rendered="#{!pc_nbAAuth.auth.enablement['SecureComment']}" />
				<h:outputText value="#{property.secureLabel}" styleClass="shText" rendered="#{!pc_nbAAuth.auth.enablement['SecureComment']}"/><!-- SPRNBA-583	 -->
				<h:selectBooleanCheckbox id="commCB2" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.general}" style="margin-left: 12px" />
				<h:graphicImage value="images/comments/lock-open-forsectionhead.gif" styleClass="commentIcon"  />
				<h:outputText value="#{property.generalLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="commCB3" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.special}" style="margin-left: 12px; width: 20px" />
				<h:graphicImage value="images/comments/exclamation-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.specialInstructionLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<!-- begin FNB004 NBA324 -->
				<h:selectBooleanCheckbox id="commCB3a" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.PHI}" rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage id="phiImage" value="images/comments/PHI-forsectionhead.gif"  rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" styleClass="commentIcon" />
				<h:outputText id="phiCheckBox" value="#{property.phiLabel}" rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" styleClass="shText" /><!-- SPRNBA-583	 -->
				<!-- end FNB004 NBA324 -->	

				<h:selectBooleanCheckbox id="commCB4" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.automated}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/zigzag-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.automatedLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="ccommCB5" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.phone}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/phone-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.phoneLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="commCB6" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.email}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/envelope-forsectionsubhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.emailLabel}" styleClass="shText" />	<!-- SPRNBA-583	 -->
			</h:panelGroup>
			<!-- NBA225 -->
			<h:panelGroup id="voidOption" styleClass="sectionSubheader" style="padding-top: 0px">
				<h:outputText id="voidedLabel" value="#{property.voidedLabel}" styleClass="formLabel" style="width: 90px; margin-left: -5px;"/>
				<h:selectOneMenu id="voidedList" styleClass="shMenu" style="width: 300px" value="#{pc_commentsNavigation.voidedOption}"
							 	 disabled="#{pc_commentsNavigation.voidOptionDisabled}">
					<f:selectItems id="voidedListValue" value="#{pc_commentsNavigation.voidedList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="clientRelation" styleClass="sectionSubheader" style="padding-top: 0px"> <!-- NBA225 -->
				<h:outputText value="#{property.commentRelates}" styleClass="formLabel" style="width: 90px; margin-left: -5px;" />		
				<h:selectOneMenu value="#{pc_commentsNavigation.selectedInsured}" styleClass="shMenu" style="width: 300px">
					<f:selectItems value="#{pc_commentsNavigation.insuredClients}"/>
				</h:selectOneMenu>
				<h:commandButton value="Go" styleClass="formButtonRight" style="margin-top: 0px" action="#{pc_commentsTable.retrieveSelectedRecords}"/>
			</h:panelGroup>
			<!-- NBA213 -->
			<f:subview id="commentsTable">
				<c:import url="/uw/subviews/NbaCommentsTable.jsp" />
			</f:subview>
			<f:subview id="nbaCommentBar">
				<c:import url="/nbaComments/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
			</f:subview>

			<h:panelGroup id="commandButtons" styleClass="tabButtonBar">  <!-- SPR2836 --> <!-- NBA225 -->
				<h:commandButton id="btnCommRefresh" value="#{property.buttonRefresh}" styleClass="ovButtonLeft" action="#{pc_commentsNavigation.refresh}" immediate="true"/>
				<h:commandButton id="btnCommDelete" action="#{pc_commentsNavigation.allComments.deleteComment}" onclick="setTargetFrame();" 
					value="#{property.buttonDelete}" disabled="#{pc_commentsNavigation.allComments.deleteDisabled}" styleClass="ovButtonLeft-1" immediate="true"/>
				<!-- NBA225 -->
				<h:commandButton id="btnCommVoid" action="#{pc_commentsNavigation.allComments.voidComment}" onclick="setTargetFrame();" 
					value="#{property.buttonVoid}" rendered="#{pc_commentsNavigation.allComments.voidRendered}" disabled="#{pc_commentsNavigation.allComments.voidDisabled}"
					styleClass="ovButtonLeft-1" immediate="true"/>
				<h:commandButton id="btnCommCommit" action="#{pc_commentsNavigation.allComments.commitComment}" value="#{property.buttonCommit}"
					disabled="#{pc_commentsNavigation.auth.enablement['Commit'] || pc_commentsNavigation.notLocked}"
					styleClass="ovButtonRight-1" />	<!-- NBA213, NBA186 -->
				<h:commandButton id="btnCommUpdate"  onclick="launchCommentPopUp('Update');" value="#{property.buttonUpdate}" disabled="#{pc_commentsNavigation.allComments.updateDisabled}" styleClass="ovButtonRight" /> <!--SPR3073 -->
			</h:panelGroup>
		</h:form>
		<!-- SPR2836 deleted code -->
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	