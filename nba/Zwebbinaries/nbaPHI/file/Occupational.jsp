<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Occupational</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
                getScrollXY('page');			
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_OCCUPATIONAL" value="#{pc_Occupational}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Occupational.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Occupational.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="430">
							<col width="150">
								<col width="50">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputText style="width: 220px" value="#{componentBundle.Occupational}" id="labelOccupation" rendered="#{pc_Occupational.showLabelOccupation}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="3" align="left"></td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="3" align="left">
											<h:outputText style="width: 393px;text-align:left;margin-left:5px;" value="#{componentBundle.EDUCATION}" styleClass="formLabel" id="labelEducation" rendered="#{pc_Occupational.showLabelEducation}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="1" align="left">
											<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Whateducationor}" styleClass="formLabel" id="labelWhatEducation" rendered="#{pc_Occupational.showLabelWhatEducation}"></h:outputText>
										</td>
										<td align="left" colspan="1"></td>
										<td>											
											<h:commandButton id="occupationalEducationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalEducationDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showWhatEducationDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasEducationDetails}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="2" align="left">
											<h:outputText style="width: 459px;text-align:left;margin-left:15px;" value="#{componentBundle.Doyouhaveacerti}" styleClass="formLabel" id="labelDoYouHaveCert" rendered="#{pc_Occupational.showLabelDoYouHaveCert}"></h:outputText>
										</td>										
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="1" align="left"></td>
										<td colspan="1" align="left">
											<h:selectBooleanCheckbox value="#{pc_Occupational.doYouHaveCertificationNo}" id="doYouHaveCertificationNo" onclick="toggleCBGroup(this.form.id, 'doYouHaveCertificationNo',  'doYouHaveCertification')"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Occupational.doYouHaveCertificationYes}" id="doYouHaveCertificationYes" onclick="toggleCBGroup(this.form.id, 'doYouHaveCertificationYes',  'doYouHaveCertification');if(document.getElementById(this.id).checked){launchDetailsPopUp('occupationalCertificationDetails')};"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
										<td>											
											<h:commandButton id="occupationalCertificationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalCertificationDetails');" style="margin-right:80px;margin-left:10px;" rendered="#{pc_Occupational.showDoYouHaveCertDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasCertificationDetails}"/>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<tr style="padding-top: 5px;">
							<td colspan="3" style="padding-top: 15px;" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:5px;" value="#{componentBundle.EMPLOYMENT}" styleClass="formLabel" id="labelEmployment" rendered="#{pc_Occupational.showLabelEmployment}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Whatisyourjobti}" styleClass="formLabel" id="labelJobTitle" rendered="#{pc_Occupational.showLabelJobTitle}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:inputText style="width: 150px;" styleClass="formEntryText" id="jobTitle" value="#{pc_Occupational.jobTitle}" rendered="#{pc_Occupational.showJobTitle}" disabled="#{pc_Occupational.disableJobTitle}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Natureofbusines}" styleClass="formLabel" id="labelNatureOfBusiness" rendered="#{pc_Occupational.showLabelNatureOfBusiness}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalNatureOfBusinessDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalNatureOfBusinessDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showNatureOfBusinessDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasNatureOfBusinessDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Howlonghaveyoubpresentpos}" styleClass="formLabel" id="labelHowLongHaveYou" rendered="#{pc_Occupational.showLabelHowLongHaveYou}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:inputText style="width: 50px;" styleClass="formEntryText" id="howLongHaveYouBeenPosition" value="#{pc_Occupational.howLongHaveYouBeenPosition}" rendered="#{pc_Occupational.showHowLongHaveYouBeenPosition}" disabled="#{pc_Occupational.disableHowLongHaveYouBeenPosition}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Numberoffulltim}" styleClass="formLabel" id="labelNumberOfFullTime" rendered="#{pc_Occupational.showLabelNumberOfFullTime}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:inputText style="width: 50px;" styleClass="formEntryText" id="numberOfFullTime" value="#{pc_Occupational.numberOfFullTime}" rendered="#{pc_Occupational.showNumberOfFullTime}" disabled="#{pc_Occupational.disableNumberOfFullTime}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" style="padding-top: 15px;" align="left">
								<h:outputText value="#{componentBundle.LOCATION}" styleClass="formLabel" style="width: 393px;text-align:left;margin-left:5px;" id="labelLocation" rendered="#{pc_Occupational.showLabelLocation}"></h:outputText>
							</td>
						</tr>
						<tr>
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Wheredoyouperfo}" styleClass="formLabel" id="labelJobDuties" rendered="#{pc_Occupational.showLabelJobDuties}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalJobDutiesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalJobDutiesDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showJobDutiesDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasJobDutiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.ieatabusinesslo}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="labelBusinessLocation" rendered="#{pc_Occupational.showLabelBusinessLocation}"></h:outputText>
							</td>
							<td align="left" colspan="2"></td>
						</tr>
						<tr style="padding-bottom: 5px;">
							<td>
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Doesyourjobrequ}" styleClass="formLabel" id="labelJobRequireTravel" rendered="#{pc_Occupational.showLabelJobRequireTravel}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Occupational.jobRequireTravelNo}" id="jobRequireTravelNo" onclick="toggleCBGroup(this.form.id, 'jobRequireTravelNo',  'jobRequireTravel')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Occupational.jobRequireTravelYes}" id="jobRequireTravelYes" onclick="toggleCBGroup(this.form.id, 'jobRequireTravelYes',  'jobRequireTravel');if(document.getElementById(this.id).checked){launchDetailsPopUp('occupationalModeOfTravelDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.IfsoWhatisthemo}" styleClass="formLabel" style="width: 515px;text-align:left;margin-left:30px;" id="labelModeOfTravel" rendered="#{pc_Occupational.showLabelModeOfTravel}"></h:outputText>
							</td>
							<td colspan="2"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalModeOfTravelDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalModeOfTravelDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showModeOfTravelDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasModeOfTravelDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Howmuchofyourti}" styleClass="formLabel" style="width: 370px;text-align:left;margin-left:30px;" id="labelTimeSpent" rendered="#{pc_Occupational.showLabelTimeSpent}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalTimeSpentDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalTimeSpentDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showTimeSpentDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasTimeSpentDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Doyoutravelouts}" styleClass="formLabel" style="width: 370px;text-align:left;margin-left:30px;" id="labelTravelOutSideCountry" rendered="#{pc_Occupational.showLabelTravelOutSideCountry}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Occupational.travelOutSideCountryNo}" id="travelOutSideCountryNo" onclick="toggleCBGroup(this.form.id, 'travelOutSideCountryNo',  'travelOutSideCountry')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Occupational.travelOutSideCountryYes}" id="travelOutSideCountryYes" onclick="toggleCBGroup(this.form.id, 'travelOutSideCountryYes',  'travelOutSideCountry');if(document.getElementById(this.id).checked){launchDetailsPopUp('occupationalTravelOutSideCountryDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>								
								<h:commandButton id="occupationalTravelOutSideCountryDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalTravelOutSideCountryDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showTravelOutSideCountryDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasTravelOutsideCountryDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" style="padding-top: 15px;" align="left">
								<h:outputText value="#{componentBundle.DUTIES}" styleClass="formLabel" style="width: 393px;text-align:left;margin-left:5px;" id="labelDuties" rendered="#{pc_Occupational.showLabelDuties}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText style="width: 491px;text-align:left;margin-left:15px;" value="#{componentBundle.Describeindetai}" styleClass="formLabel" id="labelDescribeDuties" rendered="#{pc_Occupational.showLabelDescribeDuties}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalDescribeDutiesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalDescribeDutiesDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showDescribeDutiesDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasDescribeDutiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Whattoolsmachin}" styleClass="formLabel" id="labelTools" rendered="#{pc_Occupational.showLabelTools}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalToolsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalToolsDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showToolsDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasToolsDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Whatmaterialsdo}" styleClass="formLabel" id="labelMaterial" rendered="#{pc_Occupational.showLabelMaterial}"></h:outputText>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="occupationalMaterialDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalMaterialDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showMaterialDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasMaterialDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left" colspan="3" style="padding-top: 15px;">
								<h:outputText value="#{componentBundle.OTHEREMPLOYMENT}" styleClass="formLabel" style="width: 393px;text-align:left;margin-left:5px;" id="labelOtherEmployment" rendered="#{pc_Occupational.showLabelOtherEmployment}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 393px;text-align:left;margin-left:15px;" value="#{componentBundle.Doyouconductany}" styleClass="formLabel" id="labelDoYouConductAnyBusiness" rendered="#{pc_Occupational.showLabelDoYouConductAnyBusiness}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Occupational.doYouConductAnyBusinessNo}" id="doYouConductAnyBusinessNo" onclick="toggleCBGroup(this.form.id, 'doYouConductAnyBusinessNo',  'doYouConductAnyBusiness')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Occupational.doYouConductAnyBusinessYes}" id="doYouConductAnyBusinessYes" onclick="toggleCBGroup(this.form.id, 'doYouConductAnyBusinessYes',  'doYouConductAnyBusiness');if(document.getElementById(this.id).checked){launchDetailsPopUp('occupationalDoYouConductAnyBusinessDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>								
								<h:commandButton id="occupationalDoYouConductAnyBusinessDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalDoYouConductAnyBusinessDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showDoYouConductAnyBusinessDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasDoYouConductAnyBusDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Doyouhaveapartt}" styleClass="formLabel" style="width: 393px;text-align:left;margin-left:15px;" id="labelPartTimeSeasonalJob" rendered="#{pc_Occupational.showLabelPartTimeSeasonalJob}"></h:outputText>
							</td>
							<td colspan="2" align="left">
								<h:selectBooleanCheckbox value="#{pc_Occupational.partTimeSeasonalJobNo}" id="partTimeSeasonalJobNo" onclick="toggleCBGroup(this.form.id, 'partTimeSeasonalJobNo',  'partTimeSeasonalJob')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Occupational.partTimeSeasonalJobYes}" id="partTimeSeasonalJobYes" onclick="toggleCBGroup(this.form.id, 'partTimeSeasonalJobYes',  'partTimeSeasonalJob');if(document.getElementById(this.id).checked){launchDetailsPopUp('occupationalDutiesActivitiesDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Ifsowhatdutieso}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="labelDutiesActivities" rendered="#{pc_Occupational.showLabelDutiesActivities}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="occupationalDutiesActivitiesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalDutiesActivitiesDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showDutiesActivitiesDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasDutiesActivitiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" style="padding-top: 15px;" align="left"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText style="width: 518px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="labelAdditionalInfo" rendered="#{pc_Occupational.showLabelAdditionalInfo}"></h:outputText>
							</td>
							<td align="left" colspan="1">								
								<h:commandButton id="occupationalAdditionalInfoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('occupationalAdditionalInfoDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Occupational.showAdditionalInfoDetailsImg}" styleClass="ovitalic#{pc_Occupational.hasAdditionalInfoDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="ok" action="#{pc_Occupational.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Occupational.showOk}" disabled="#{pc_Occupational.disableOk}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clear" action="#{pc_Occupational.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Occupational.showClear}" disabled="#{pc_Occupational.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Occupational.okAction}" onclick="resetTargetFrame(this.form.id);" id="_0" rendered="#{!pc_Links.showIdOccupational}" disabled="#{pc_Occupational.disable_0}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_Occupational.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdOccupational}" disabled="#{pc_Occupational.disable_0}"></h:commandButton>
							</td>
						</tr>
						<tr>
							<td style="height: 15px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
