<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Gastrointestinal IN</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_GASTROINTESTINAL" value="#{pc_Gastrointestinal}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Gastrointestinal.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Gastrointestinal.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="250">
							<col width="50">
								<col width="60">
									<col width="110">
										<col width="110">
											<tr style="padding-top: 5px">
												<td colspan="5" class="sectionSubheader"> <!-- NBA324 -->
													<h:outputLabel value="#{componentBundle.Gastrointestina}" style="width: 220px" id="_0" rendered="#{pc_Gastrointestinal.show_0}"></h:outputLabel>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="5" align="left" style="padding-top: 15px;">
													<h:outputText style="width: 518px;text-align:left;padding-bottom:10px;margin-left:5px;" value="#{componentBundle.DoyoucurrentlyoIN}" styleClass="formLabel" id="experiencefollowingconditionsIN" rendered="#{pc_Gastrointestinal.showExperiencefollowingconditionsIN}"></h:outputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1"></td>
												<td align="left" colspan="2">
													<h:outputLabel value="#{componentBundle.DateofOnset}" style="width: 64px;text-align:left;padding-bottom:10px;" styleClass="formLabel" id="dateofonset" rendered="#{pc_Gastrointestinal.showDateofonset}"></h:outputLabel>
												</td>
												<td align="left">
													<h:outputLabel value="#{componentBundle.DateofmostRecen}" style="width: 100px;text-align:left;padding-bottom:10px;" styleClass="formLabel" id="dateofrecentepisodes" rendered="#{pc_Gastrointestinal.showDateofrecentepisodes}"></h:outputLabel>
												</td>
												<td align="left">
													<h:outputLabel value="#{componentBundle.Frequency}" style="width: 75px;text-align:left;padding-bottom:10px;" styleClass="formLabel" id="frequency_1" rendered="#{pc_Gastrointestinal.showFrequency_1}"></h:outputLabel>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.Abdominalpain}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="abdominalpain" rendered="#{pc_Gastrointestinal.showAbdominalpain}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="abdominalPainOnsetDate" value="#{pc_Gastrointestinal.abdominalPainOnsetDate}" rendered="#{pc_Gastrointestinal.showAbdominalPainOnsetDate}" disabled="#{pc_Gastrointestinal.disableAbdominalPainOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.abdominalPainRecentEpisodesDate}" id="abdominalPainRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showAbdominalPainRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disableAbdominalPainRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="abdominalPainFrequency" value="#{pc_Gastrointestinal.abdominalPainFrequency}" rendered="#{pc_Gastrointestinal.showAbdominalPainFrequency}" disabled="#{pc_Gastrointestinal.disableAbdominalPainFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.FrequentNausea}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="frequentnausea" rendered="#{pc_Gastrointestinal.showFrequentnausea}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="frequentNauseaOnsetDate" value="#{pc_Gastrointestinal.frequentNauseaOnsetDate}" rendered="#{pc_Gastrointestinal.showFrequentNauseaOnsetDate}" disabled="#{pc_Gastrointestinal.disableFrequentNauseaOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.frequentNauseaRecentEpisodesDate}" id="frequentNauseaRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showFrequentNauseaRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disableFrequentNauseaRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="frequentNauseaFrequency" value="#{pc_Gastrointestinal.frequentNauseaFrequency}" rendered="#{pc_Gastrointestinal.showFrequentNauseaFrequency}" disabled="#{pc_Gastrointestinal.disableFrequentNauseaFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.Heartburn}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="heartburn" rendered="#{pc_Gastrointestinal.showHeartburn}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="heartBurnOnsetDate" value="#{pc_Gastrointestinal.heartBurnOnsetDate}" rendered="#{pc_Gastrointestinal.showHeartBurnOnsetDate}" disabled="#{pc_Gastrointestinal.disableHeartBurnOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.heartBurnRecentEpisodesDate}" id="heartBurnRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showHeartBurnRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disableHeartBurnRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="heartBurnFrequency" value="#{pc_Gastrointestinal.heartBurnFrequency}" rendered="#{pc_Gastrointestinal.showHeartBurnFrequency}" disabled="#{pc_Gastrointestinal.disableHeartBurnFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.PersistentDiarr}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="persistentdiarrhea" rendered="#{pc_Gastrointestinal.showPersistentdiarrhea}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="persistentDiarrheaOnsetDate" value="#{pc_Gastrointestinal.persistentDiarrheaOnsetDate}" rendered="#{pc_Gastrointestinal.showPersistentDiarrheaOnsetDate}" disabled="#{pc_Gastrointestinal.disablePersistentDiarrheaOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.persistentDiarrheaRecentEpisodesDate}" id="persistentDiarrheaRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showPersistentDiarrheaRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disablePersistentDiarrheaRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="persistentDiarrheaFrequency" value="#{pc_Gastrointestinal.persistentDiarrheaFrequency}" rendered="#{pc_Gastrointestinal.showPersistentDiarrheaFrequency}" disabled="#{pc_Gastrointestinal.disablePersistentDiarrheaFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.Bloodyortarryst}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="bloodyortarrystools" rendered="#{pc_Gastrointestinal.showBloodyortarrystools}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="bloodyOrTarrysToolsOnsetDate" value="#{pc_Gastrointestinal.bloodyOrTarrysToolsOnsetDate}" rendered="#{pc_Gastrointestinal.showBloodyOrTarrysToolsOnsetDate}" disabled="#{pc_Gastrointestinal.disableBloodyOrTarrysToolsOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.bloodyOrTarrysToolsRecentEpisodesDate}" id="bloodyOrTarrysToolsRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showBloodyOrTarrysToolsRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disableBloodyOrTarrysToolsRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="bloodyOrTarrysToolsFrequency" value="#{pc_Gastrointestinal.bloodyOrTarrysToolsFrequency}" rendered="#{pc_Gastrointestinal.showBloodyOrTarrysToolsFrequency}" disabled="#{pc_Gastrointestinal.disableBloodyOrTarrysToolsFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="1" align="right">
													<h:outputText value="#{componentBundle.RectalBleeding}" styleClass="formLabel" style="width: 150px;text-align:left;margin-left:30px;" id="rectalbleeding" rendered="#{pc_Gastrointestinal.showRectalbleeding}"></h:outputText>
												</td>
												<td align="left" colspan="2">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" id="rectalBleedingOnsetDate" value="#{pc_Gastrointestinal.rectalBleedingOnsetDate}" rendered="#{pc_Gastrointestinal.showRectalBleedingOnsetDate}" disabled="#{pc_Gastrointestinal.disableRectalBleedingOnsetDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 95px;"  styleClass="formEntryText" value="#{pc_Gastrointestinal.rectalBleedingRecentEpisodesDate}" id="rectalBleedingRecentEpisodesDate" rendered="#{pc_Gastrointestinal.showRectalBleedingRecentEpisodesDate}" disabled="#{pc_Gastrointestinal.disableRectalBleedingRecentEpisodesDate}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
												</td>
												<td align="left">
													<h:inputText style="width: 65px;"  styleClass="formEntryText" id="rectalBleedingFrequency" value="#{pc_Gastrointestinal.rectalBleedingFrequency}" rendered="#{pc_Gastrointestinal.showRectalBleedingFrequency}" disabled="#{pc_Gastrointestinal.disableRectalBleedingFrequency}"></h:inputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="5"></td>
											</tr>
											<tr style="padding-top:15px">
												<td colspan="2">
													<h:outputText style="width: 324px;text-align:left;margin-left:5px;" value="#{componentBundle.HaveyouseenaphyIN}" styleClass="formLabel" id="seenphysicianIN" rendered="#{pc_Gastrointestinal.showSeenphysicianIN}"></h:outputText>
												</td>
												<td colspan="2" align="left">
													<h:selectBooleanCheckbox id="seenByPhysicianNo" value="#{pc_Gastrointestinal.seenByPhysicianNo}" onclick="toggleCBGroup(this.form.id, 'seenByPhysicianNo',  'seenByPhysician',true);valueChangeEvent();"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
													<h:selectBooleanCheckbox id="seenByPhysicianYes" value="#{pc_Gastrointestinal.seenByPhysicianYes}" valueChangeListener="#{pc_Gastrointestinal.collapsePhysicianDetail}" onclick="toggleCBGroup(this.form.id, 'seenByPhysicianYes',  'seenByPhysician',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('gastroPhysicianNameAddressDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
												</td>
											</tr>
										</col>
									</col>
								</col>
							</col>
						</col>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Gastrointestinal.expandPhyscianDetailsInd}">
						<col>
							<col>
								<col>
									<col>
										<col>
											<td align="left"></td>
											<tr style="padding-top: 5px">
												<td colspan="4">
													<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 504px;text-align:left;margin-left:15px;" id="ifyes" rendered="#{pc_Gastrointestinal.showIfyes}"></h:outputText>
												</td>
												<td align="left"></td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="5">
													<h:outputText value="#{componentBundle.Pleaseprovideth}" styleClass="formLabel" style="width: 550px;text-align:left;margin-left:30px;" id="nameofphysicain" rendered="#{pc_Gastrointestinal.showNameofphysicain}"></h:outputText>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="3"></td>
												<td align="left"></td>
												<td align="left">
													<h:commandButton id="gastroPhysicianNameAddressDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroPhysicianNameAddressDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_1}" styleClass="ovitalic#{pc_Gastrointestinal.hasPhysicianAddressDetails}"/>
												</td>
											</tr>
											<tr style="padding-top: 5px">
												<td colspan="5"></td>
											</tr>
											<tr style="padding-top: 5px;">
												<td colspan="2">
													<h:outputText value="#{componentBundle.Didthephysician}" styleClass="formLabel" style="width: 286px;text-align:left;margin-left:30px;" id="physicianrecommendtesting" rendered="#{pc_Gastrointestinal.showPhysicianrecommendtesting}"></h:outputText>
												</td>
												<td colspan="2">
													<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.physicianRecommendationNo}" id="physicianRecommendationNo" onclick="toggleCBGroup(this.form.id, 'physicianRecommendationNo',  'physicianRecommendation')"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
													<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.physicianRecommendationYes}" id="physicianRecommendationYes" onclick="toggleCBGroup(this.form.id, 'physicianRecommendationYes',  'physicianRecommendation');if(document.getElementById(this.id).checked){launchDetailsPopUp('gastroTestDetails')};"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
												</td>
											</tr>
										</col>
									</col>
								</col>
							</col>
						</col>
						<td></td>
						<tr style="padding-top: 5px">
							<td colspan="4">
								<h:outputText value="#{componentBundle.Ifso}" styleClass="formLabel" style="width: 379px;text-align:left;margin-left:30px;" id="ifso" rendered="#{pc_Gastrointestinal.showIfso}"></h:outputText>
								<h:outputText value="#{componentBundle.whattestsweredo}" styleClass="formLabel" style="width: 379px;text-align:left;margin-left:45px;" id="testsperformed" rendered="#{pc_Gastrointestinal.showTestsperformed}"></h:outputText>
							</td>
							<td align="left">
								<h:commandButton id="gastroTestDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroTestDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_2}" styleClass="ovitalic#{pc_Gastrointestinal.hasTestDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.WhatwasthediagnGQ}" styleClass="formLabel" style="width: 271px;text-align:left;margin-left:45px;" id="diagnosis_1" rendered="#{pc_Gastrointestinal.showDiagnosis_1}"></h:outputText>
							</td>
							<td align="left" colspan="2"></td>
							<td align="left"></td>
							<td align="left">
								<h:commandButton id="gastroDiagnosisDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroDiagnosisDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_3}" styleClass="ovitalic#{pc_Gastrointestinal.hasDiagnosisDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Whattreatmentwa}" styleClass="formLabel" style="width: 271px;text-align:left;margin-left:45px;" id="treatmentprescribed" rendered="#{pc_Gastrointestinal.showTreatmentprescribed}"></h:outputText>
							</td>
							<td align="left" colspan="2"></td>
							<td align="left"></td>
							<td align="left">
								<h:commandButton id="gastroTreatmentPrescribedDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroTreatmentPrescribedDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_4}" styleClass="ovitalic#{pc_Gastrointestinal.hasTreatmentPrescribedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.AreyoucurrentlyUnderTreatment}" styleClass="formLabel" style="width: 271px;text-align:left;margin-left:45px;" id="undertreatment" rendered="#{pc_Gastrointestinal.showUndertreatment}"></h:outputText>
							</td>
							<td colspan="2" align="left">
								<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.treatementNo}" id="treatementNo" onclick="toggleCBGroup(this.form.id, 'treatementNo',  'treatement')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.treatementYes}" id="treatementYes" onclick="toggleCBGroup(this.form.id, 'treatementYes',  'treatement');if(document.getElementById(this.id).checked){launchDetailsPopUp('gastroCurrentTreatmentDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="left">
								<h:commandButton id="gastroCurrentTreatmentDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroCurrentTreatmentDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_5}" styleClass="ovitalic#{pc_Gastrointestinal.hasCurrentTreatmentDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px">
							<td colspan="5" style="padding-top: 15px;">
								<h:outputText style="width: 407px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouexperien}" styleClass="formLabel" id="experiencedweightloss" rendered="#{pc_Gastrointestinal.showExperiencedweightloss}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2"></td>
							<td colspan="2" align="left">
								<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.weightLossNo}" id="weightLossNo" onclick="toggleCBGroup(this.form.id, 'weightLossNo',  'weightLoss')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Gastrointestinal.weightLossYes}" id="weightLossYes" onclick="toggleCBGroup(this.form.id, 'weightLossYes',  'weightLoss') ;if(document.getElementById(this.id).checked){launchDetailsPopUp('gastroWeightLossDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="left"></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="3">
								<h:outputText style="width: 322px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleasedescr}" styleClass="formLabel" id="howweightchanged" rendered="#{pc_Gastrointestinal.showHowweightchanged}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="gastroWeightLossDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroWeightLossDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_6}" styleClass="ovitalic#{pc_Gastrointestinal.hasWeightLossDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="5" style="padding-top: 15px;"></td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="5">
								<h:outputText style="width: 518px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="additionalresponse" rendered="#{pc_Gastrointestinal.showAdditionalresponse}"></h:outputText>
								<h:commandButton id="gastroAdditionalDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('gastroAdditionalDetails');" style="margin-right:10px" rendered="#{pc_Gastrointestinal.show_7}" styleClass="ovitalic#{pc_Gastrointestinal.hasAdditionalInfoDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1"></td>
							<td align="left" colspan="2"></td>
							<td align="left"></td>
							<td align="left"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancel" action="#{pc_Gastrointestinal.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Gastrointestinal.showCancel}" disabled="#{pc_Gastrointestinal.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clear" action="#{pc_Gastrointestinal.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Gastrointestinal.showClear}" disabled="#{pc_Gastrointestinal.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="ok" action="#{pc_Gastrointestinal.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdGastrointestinal}" disabled="#{pc_Gastrointestinal.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_Gastrointestinal.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdGastrointestinal}" disabled="#{pc_Gastrointestinal.disableOk}"></h:commandButton>
							</td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
