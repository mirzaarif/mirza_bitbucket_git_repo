<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Aerial Sports MN</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_AERIALSPORTS" value="#{pc_AerialSports}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_AerialSports.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_AerialSports.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="150">
							<col width="100">
								<col width="150">
									<col width="120">
										<col width="105">
											<col width="5">
												<tr style="padding-top: 5px;">
													<td colspan="6" class="sectionSubheader"> <!-- NBA324 -->
														<h:outputLabel value="#{componentBundle.AerialSports}" style="width: 220px" id="aerialSportsMN" rendered="#{pc_AerialSports.showAerialSportsMN}"></h:outputLabel>
													</td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="4">
														<h:outputText style="width: 500px;text-align:left;margin-left:5px;" value="#{componentBundle.Whichofthefollo}" styleClass="formLabel" id="aerialSportsMNQ1" rendered="#{pc_AerialSports.showAerialSportsMNQ1}"></h:outputText>
													</td>
													<td colspan="1"></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText value="#{componentBundle.SkyDivingParach}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="skydivingMNlabelQ1" rendered="#{pc_AerialSports.showSkydivingMNlabelQ1}"></h:outputText>
														<h:outputText value="#{componentBundle.Whatisthetotaln}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="noOfJumpsMN" rendered="#{pc_AerialSports.showNoOfJumpsMN}"></h:outputText>
													</td>
													<td colspan="2"></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left"></td>
													<td colspan="2"></td>
													<td colspan="2"></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="sdlast12monthslabel" rendered="#{pc_AerialSports.showSdlast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="sdlast12months" value="#{pc_AerialSports.sdlast12months}" rendered="#{pc_AerialSports.showSdlast12months}" disabled="#{pc_AerialSports.disableSdlast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" escape="sd1to2yearsagolabel" id="sd1to2yearsagolabel" rendered="#{pc_AerialSports.showSd1to2yearsagolabel}"></h:outputText>
													</td>
													<td colspan="2">
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="sd1to2yearsago" value="#{pc_AerialSports.sd1to2yearsago}" rendered="#{pc_AerialSports.showSd1to2yearsago}" disabled="#{pc_AerialSports.disableSd1to2yearsago}"></h:inputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="sdnext12monthslabel" rendered="#{pc_AerialSports.showSdnext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="sdnext12months" value="#{pc_AerialSports.sdnext12months}" rendered="#{pc_AerialSports.showSdnext12months}" disabled="#{pc_AerialSports.disableSdnext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="sdlastactivitydatelabel" rendered="#{pc_AerialSports.showSdlastactivitydatelabel}"></h:outputText>
													</td>
													<td colspan="2">
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="sdlastactivitydate" value="#{pc_AerialSports.sdlastactivitydate}" rendered="#{pc_AerialSports.showSdlastactivitydate}" disabled="#{pc_AerialSports.disableSdlastactivitydate}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="sdlocationlabel" rendered="#{pc_AerialSports.showSdlocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="sdlocation" value="#{pc_AerialSports.sdlocation}" rendered="#{pc_AerialSports.showSdlocation}" disabled="#{pc_AerialSports.disableSdlocation}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialSdlocationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialSdlocationDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showSdlocationDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialSdlocationDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="sdTerrainLabel" rendered="#{pc_AerialSports.showSdTerrainLabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="sdterrain" value="#{pc_AerialSports.sdterrain}" rendered="#{pc_AerialSports.showSdterrain}" disabled="#{pc_AerialSports.disableSdterrain}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialSdTerraindetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialSdTerraindetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showSdTerraindetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialSdTerraindetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<h:outputText value="#{componentBundle.HangGliding}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="handGlidinglabel" rendered="#{pc_AerialSports.showHandGlidinglabel}"></h:outputText>
														<h:outputText value="#{componentBundle.Whatisthetotaln_0}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="noOfHgFlightsQ1B" rendered="#{pc_AerialSports.showNoOfHgFlightsQ1B}"></h:outputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="hglast12monthslabel" rendered="#{pc_AerialSports.showHglast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="hglast12months" value="#{pc_AerialSports.hglast12months}" rendered="#{pc_AerialSports.showHglast12months}" disabled="#{pc_AerialSports.disableHglast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" id="hg1to2yearsagolabel" rendered="#{pc_AerialSports.showHg1to2yearsagolabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="hg1to2yearsago" value="#{pc_AerialSports.hg1to2yearsago}" rendered="#{pc_AerialSports.showHg1to2yearsago}" disabled="#{pc_AerialSports.disableHg1to2yearsago}"></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="hgnext12monthslabel" rendered="#{pc_AerialSports.showHgnext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="hgnext12months" value="#{pc_AerialSports.hgnext12months}" rendered="#{pc_AerialSports.showHgnext12months}" disabled="#{pc_AerialSports.disableHgnext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="hgdatelastactivityLabel" rendered="#{pc_AerialSports.showHgdatelastactivityLabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="hglastactivitydate" value="#{pc_AerialSports.hglastactivitydate}" rendered="#{pc_AerialSports.showHglastactivitydate}" disabled="#{pc_AerialSports.disableHglastactivitydate}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="hglocationlabel" rendered="#{pc_AerialSports.showHglocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" value="#{pc_AerialSports.hglocation}" id="hglocation" rendered="#{pc_AerialSports.showHglocation}" disabled="#{pc_AerialSports.disableHglocation}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialHgLocationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialHgLocationDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showHgLocationDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialHgLocationDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="hgTerrainlabel" rendered="#{pc_AerialSports.showHgTerrainlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" value="#{pc_AerialSports.hgterrain}" id="hgterrain" rendered="#{pc_AerialSports.showHgterrain}" disabled="#{pc_AerialSports.disableHgterrain}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialHgTerrainDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialHgTerrainDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showHgTerrainDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialHgTerrainDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText value="#{componentBundle.Bungeejumping}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="bungeeJumpinglabel" rendered="#{pc_AerialSports.showBungeeJumpinglabel}"></h:outputText>
														<h:outputText value="#{componentBundle.Whatisthetotaln_0_1}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="bungeejumpingQ1C" rendered="#{pc_AerialSports.showBungeejumpingQ1C}"></h:outputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bjlast12monthslabel" rendered="#{pc_AerialSports.showBjlast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bjlast12months" value="#{pc_AerialSports.bjlast12months}" rendered="#{pc_AerialSports.showBjlast12months}" disabled="#{pc_AerialSports.disableBjlast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" id="bj1to2yearsagolabel" rendered="#{pc_AerialSports.showBj1to2yearsagolabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bj1to2yearsago" value="#{pc_AerialSports.bj1to2yearsago}" rendered="#{pc_AerialSports.showBj1to2yearsago}" disabled="#{pc_AerialSports.disableBj1to2yearsago}"></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bjnext12monthslabel" rendered="#{pc_AerialSports.showBjnext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bjnext12months" value="#{pc_AerialSports.bjnext12months}" rendered="#{pc_AerialSports.showBjnext12months}" disabled="#{pc_AerialSports.disableBjnext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="bjlastactivitydatelabel" rendered="#{pc_AerialSports.showBjlastactivitydatelabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bjlastactivitydate" value="#{pc_AerialSports.bjlastactivitydate}" rendered="#{pc_AerialSports.showBjlastactivitydate}" disabled="#{pc_AerialSports.disableBjlastactivitydate}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bjlocationlabel" rendered="#{pc_AerialSports.showBjlocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="bjlocation" value="#{pc_AerialSports.bjlocation}" rendered="#{pc_AerialSports.showBjlocation}" disabled="#{pc_AerialSports.disableBjlocation}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialBjLocationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialBjLocationDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showBjLocationDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialBjLocationDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bjTerrainlabel" rendered="#{pc_AerialSports.showBjTerrainlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="bjterrain" value="#{pc_AerialSports.bjterrain}" rendered="#{pc_AerialSports.showBjterrain}" disabled="#{pc_AerialSports.disableBjterrain}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialBjTerraindetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialBjTerraindetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showBjTerraindetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialBjTerraindetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText value="#{componentBundle.Ballooning}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="ballooninglabel" rendered="#{pc_AerialSports.showBallooninglabel}"></h:outputText>
														<h:outputText value="#{componentBundle.Whatisthetotaln_0_1_2}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="ballooningQ1D" rendered="#{pc_AerialSports.showBallooningQ1D}"></h:outputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bllast12monthslabel" rendered="#{pc_AerialSports.showBllast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bllast12months" value="#{pc_AerialSports.bllast12months}" rendered="#{pc_AerialSports.showBllast12months}" disabled="#{pc_AerialSports.disableBllast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" id="bl1to2yearsagolabel" rendered="#{pc_AerialSports.showBl1to2yearsagolabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bl1to2yearsago" value="#{pc_AerialSports.bl1to2yearsago}" rendered="#{pc_AerialSports.showBl1to2yearsago}" disabled="#{pc_AerialSports.disableBl1to2yearsago}"></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="blnext12monthslabel" rendered="#{pc_AerialSports.showBlnext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="blnext12months" value="#{pc_AerialSports.blnext12months}" rendered="#{pc_AerialSports.showBlnext12months}" disabled="#{pc_AerialSports.disableBlnext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="bldatelastactivitylabel" rendered="#{pc_AerialSports.showBldatelastactivitylabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="bldatelastactivity" value="#{pc_AerialSports.bldatelastactivity}" rendered="#{pc_AerialSports.showBldatelastactivity}" disabled="#{pc_AerialSports.disableBldatelastactivity}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="bllocationlabel" rendered="#{pc_AerialSports.showBllocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="bllocation" value="#{pc_AerialSports.bllocation}" rendered="#{pc_AerialSports.showBllocation}" disabled="#{pc_AerialSports.disableBllocation}"></h:inputText>
													</td>
													<td>														
														<h:commandButton id="aerialBllocationdetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialBllocationdetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showBllocationDetailsImage}" styleClass="ovitalic#{pc_AerialSports.hasaerialBllocationdetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="blTerrainlabel" rendered="#{pc_AerialSports.showBlTerrainlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="blTerrain" value="#{pc_AerialSports.blTerrain}" rendered="#{pc_AerialSports.showBlTerrain}" disabled="#{pc_AerialSports.disableBlTerrain}"></h:inputText>
													</td>
													<td>														
														<h:commandButton id="aerialBlterraindetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialBlterraindetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showBlterrainDetailsImage}" styleClass="ovitalic#{pc_AerialSports.hasaerialBlterraindetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText value="#{componentBundle.Parasailing}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="parasailinglabel" rendered="#{pc_AerialSports.showParasailinglabel}"></h:outputText>
														<h:outputText value="#{componentBundle.Whatisthetotaln_0_1_2}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="parasailingQ1E" rendered="#{pc_AerialSports.showParasailingQ1E}"></h:outputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="palast12monthslabel" rendered="#{pc_AerialSports.showPalast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="palast12months" value="#{pc_AerialSports.palast12months}" rendered="#{pc_AerialSports.showPalast12months}" disabled="#{pc_AerialSports.disablePalast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" id="pa1to2yearsagolabel" rendered="#{pc_AerialSports.showPa1to2yearsagolabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="pa1to2yearsago" value="#{pc_AerialSports.pa1to2yearsago}" rendered="#{pc_AerialSports.showPa1to2yearsago}" disabled="#{pc_AerialSports.disablePa1to2yearsago}"></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="panext12monthslabel" rendered="#{pc_AerialSports.showPanext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="panext12months" value="#{pc_AerialSports.panext12months}" rendered="#{pc_AerialSports.showPanext12months}" disabled="#{pc_AerialSports.disablePanext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="padatelastactivitylabel" rendered="#{pc_AerialSports.showPadatelastactivitylabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="padatelastactivity" value="#{pc_AerialSports.padatelastactivity}" rendered="#{pc_AerialSports.showPadatelastactivity}" disabled="#{pc_AerialSports.disablePadatelastactivity}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="palocationlabel" rendered="#{pc_AerialSports.showPalocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="palocation" value="#{pc_AerialSports.palocation}" rendered="#{pc_AerialSports.showPalocation}" disabled="#{pc_AerialSports.disablePalocation}"></h:inputText>
													</td>
													<td>														
														<h:commandButton id="aerialPllocationdetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialPllocationdetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showPllocationDetailsImage}" styleClass="ovitalic#{pc_AerialSports.hasaerialPllocationdetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="paTerrainlabel" rendered="#{pc_AerialSports.showPaTerrainlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="paTerrain" value="#{pc_AerialSports.paTerrain}" rendered="#{pc_AerialSports.showPaTerrain}" disabled="#{pc_AerialSports.disablePaTerrain}"></h:inputText>
													</td>
													<td>														
														<h:commandButton id="aerialPlterraindetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialPlterraindetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showPlterrainDetailsImage}" styleClass="ovitalic#{pc_AerialSports.hasaerialPlterraindetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText value="#{componentBundle.Otheraerialspor}" styleClass="formLabel" style="width: 416px;text-align:left;padding-bottom:10px;margin-left:15px;" id="otherActiviteslabel" rendered="#{pc_AerialSports.showOtherActiviteslabel}"></h:outputText>
														<h:outputText value="#{componentBundle.Totalnumberofju}" styleClass="formLabel" style="width: 416px;text-align:left;margin-left:30px;" id="otheraerialsportactivitiesQ1F" rendered="#{pc_AerialSports.showOtheraerialsportactivitiesQ1F}"></h:outputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="oalast12monthslabel" rendered="#{pc_AerialSports.showOalast12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="oalast12months" value="#{pc_AerialSports.oalast12months}" rendered="#{pc_AerialSports.showOalast12months}" disabled="#{pc_AerialSports.disableOalast12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.Field12yearsago}" styleClass="formLabel" style="width: 150px;text-align:right;" id="oa1to2yearsagolabel" rendered="#{pc_AerialSports.showOa1to2yearsagolabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="oa1to2yearsago" value="#{pc_AerialSports.oa1to2yearsago}" rendered="#{pc_AerialSports.showOa1to2yearsago}" disabled="#{pc_AerialSports.disableOa1to2yearsago}"></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabel" style="width: 120px;text-align:right;" id="oanext12monthslabel" rendered="#{pc_AerialSports.showOanext12monthslabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="oanext12months" value="#{pc_AerialSports.oanext12months}" rendered="#{pc_AerialSports.showOanext12months}" disabled="#{pc_AerialSports.disableOanext12months}"></h:inputText>
													</td>
													<td align="right">
														<h:outputText value="#{componentBundle.DateofLastActiv}" styleClass="formLabel" style="width: 150px;text-align:right;" id="oadatelastactivitylabel" rendered="#{pc_AerialSports.showOadatelastactivitylabel}"></h:outputText>
													</td>
													<td>
														<h:inputText style="width: 95px;" readonly="false" styleClass="formEntryText" id="oalastactivitydate" value="#{pc_AerialSports.oalastactivitydate}" rendered="#{pc_AerialSports.showOalastactivitydate}" disabled="#{pc_AerialSports.disableOalastactivitydate}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
													</td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left"></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Location}" styleClass="formLabel" style="width: 120px;text-align:right;" id="oalocationlabel" rendered="#{pc_AerialSports.showOalocationlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="oalocation" value="#{pc_AerialSports.oalocation}" rendered="#{pc_AerialSports.showOalocation}" disabled="#{pc_AerialSports.disableOalocation}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialOaLocationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialOaLocationDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showOaLocationDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialOaLocationDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="right">
														<h:outputText value="#{componentBundle.Terrain}" styleClass="formLabel" style="width: 120px;text-align:right;" id="oaTerrainlabel" rendered="#{pc_AerialSports.showOaTerrainlabel}"></h:outputText>
													</td>
													<td colspan="3">
														<h:inputText readonly="false" styleClass="formEntryText" style="width: 345px" id="oaterrain" value="#{pc_AerialSports.oaterrain}" rendered="#{pc_AerialSports.showOaterrain}" disabled="#{pc_AerialSports.disableOaterrain}"></h:inputText>
													</td>
													<td>
														<h:commandButton id="aerialOaTerrainDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialOaTerrainDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showOaTerrainDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialOaTerrainDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<hr/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Areyouamemberof}" styleClass="formLabel" id="aerialSportsclubQ2" rendered="#{pc_AerialSports.showAerialSportsclubQ2}"></h:outputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="1"></td>
													<td colspan="0" align="right"></td>
													<td style="width: 300px" colspan="2" align="right">
														<h:selectBooleanCheckbox id="clubQ2CBNo" value="#{pc_AerialSports.clubQ2CBNo}" onclick="toggleCBGroup(this.form.id, 'clubQ2CBNo',  'clubQ2CB')"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.No}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
														<h:selectBooleanCheckbox id="clubQ2CBYes" value="#{pc_AerialSports.clubQ2CBYes}" onclick="toggleCBGroup(this.form.id, 'clubQ2CBYes',  'clubQ2CB');if(document.getElementById(this.id).checked){launchDetailsPopUp('aerialOrganiztionDetails')};"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.Yes}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
													</td>
												</tr>
											</col>
										</col>
									</col>
								</col>
							</col>
						</col>
					</col>
												<tr style="padding-top: 5px;">
													<td colspan="3">
														<h:outputText value="#{componentBundle.Ifyesgivenameof}" styleClass="formLabel" style="width: 224px;text-align:left;padding-bottom:10px;margin-left:15px;" id="organizationDetailslabel" rendered="#{pc_AerialSports.showOrganizationDetailslabel}"></h:outputText>
													</td>
													<td></td>
													<td>
												<h:commandButton id="aerialOrganiztionDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialOrganiztionDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showOrganiztionDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialOrganiztionDetails}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverbeen_1}" styleClass="formLabel" id="aerialSportsaccidentQ3" rendered="#{pc_AerialSports.showAerialSportsaccidentQ3}"></h:outputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="1"></td>
													<td colspan="0" align="right"></td>
													<td style="width: 300px" colspan="2" align="right">
														<h:selectBooleanCheckbox id="accidentQ3CBNo" value="#{pc_AerialSports.accidentQ3CBNo}" onclick="toggleCBGroup(this.form.id, 'accidentQ3CBNo',  'accidentQ3CB')"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.No}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
														<h:selectBooleanCheckbox id="accidentQ3CBYes" value="#{pc_AerialSports.accidentQ3CBYes}" onclick="toggleCBGroup(this.form.id, 'accidentQ3CBYes',  'accidentQ3CB');if(document.getElementById(this.id).checked){launchDetailsPopUp('aerialAccidentDetails')};"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.Yes}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
													</td>
													<td></td>
													<td></td>
												</tr>											
												<tr style="padding-top: 5px;">
													<td align="left" colspan="2">
														<h:outputText value="#{componentBundle.Ifyespleaseprov}" styleClass="formLabel" style="width: 202px;text-align:left;padding-bottom:10px;margin-left:15px;" id="accidentDetailsLabel" rendered="#{pc_AerialSports.showAccidentDetailsLabel}"></h:outputText>
													</td>
													<td></td>
													<td></td>
													<td>
								<h:commandButton id="aerialAccidentDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialAccidentDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showAccidentDetailsimage}" styleClass="ovitalic#{pc_AerialSports.hasaerialAccidentDetails}"/>
													</td>
													<td></td>
												</tr>											
												<tr style="padding-top: 5px;">
													<td colspan="5">
														<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Isyourstatusint}" styleClass="formLabel" id="activityStatuslabel" rendered="#{pc_AerialSports.showActivityStatuslabel}"></h:outputText>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
													<td align="left" colspan="1"></td>
													<td colspan="0" align="right"></td>
													<td style="width: 300px" colspan="2" align="right">
														<h:selectBooleanCheckbox id="activityQ4CBAmateur" value="#{pc_AerialSports.activityQ4CBAmateur}" onclick="toggleCBGroup(this.form.id, 'activityQ4CBAmateur',  'activityQ4CB')"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.Amateur}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
														<h:selectBooleanCheckbox id="activityQ4CBProfessional" value="#{pc_AerialSports.activityQ4CBProfessional}" onclick="toggleCBGroup(this.form.id, 'activityQ4CBProfessional',  'activityQ4CB');if(document.getElementById(this.id).checked){launchDetailsPopUp('aerialProfessionalDetails')};"></h:selectBooleanCheckbox>
														<h:outputLabel value="#{componentBundle.Professional}" style="width: 30px;text-align: left;" styleClass="formLabel"></h:outputLabel>
													</td>
													<td></td>
													<td></td>
												</tr>											
												<tr style="padding-top: 5px;">																								
													<td align="left" colspan="2">
														<h:outputText value="#{componentBundle.Ifprofessionalp}" styleClass="formLabel" style="width: 254px;text-align:left;padding-bottom:10px;margin-left:15px;" id="professionalDetailsLabel" rendered="#{pc_AerialSports.showProfessionalDetailsLabel}"></h:outputText>
													</td>													
													<td></td>
													<td></td>
													<td>
								<h:commandButton id="aerialProfessionalDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialProfessionalDetails');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showProfessionalDetails}" styleClass="ovitalic#{pc_AerialSports.hasaerialProfessionalDetails}"/>
													</td>
													<td></td>
												</tr>																					
												<tr style="padding-top: 5px;">
													<td align="left" colspan="4">
														<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="addtionalInformation" rendered="#{pc_AerialSports.showAddtionalInformation}"></h:outputText>
													</td>
													<td>
								<h:commandButton id="aerialAdditionalInformation" image="images/circle_i.gif"  onclick="launchDetailsPopUp('aerialAdditionalInformation');" style="margin-left:10px;margin-right:25px;" rendered="#{pc_AerialSports.showAdditionalInformation}" styleClass="ovitalic#{pc_AerialSports.hasaerialAdditionalInformation}"/>
													</td>
													<td></td>
												</tr>
												<tr style="padding-top: 5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td valign="top" colspan="2">
								<h:commandButton value="Cancel" styleClass="formButton"  style="margin-left: 5px" id="cancelButton" action="#{pc_AerialSports.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_AerialSports.showCancelButton}" disabled="#{pc_AerialSports.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="formButton" id="clearButton" style="margin-left:10" rendered="#{pc_AerialSports.showClearButton}" disabled="#{pc_AerialSports.disableClearButton}" action="#{pc_AerialSports.clearAction}" onclick="resetTargetFrame(this.form.id);"></h:commandButton>
							</td>							
							<td></td>
							<td></td>
							<td valign="top">
								<h:commandButton value="OK" styleClass="formButton" id="okButton" action="#{pc_AerialSports.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdAerialSports}" disabled="#{pc_AerialSports.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="formButton" id="updateButton" action="#{pc_AerialSports.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdAerialSports}" disabled="#{pc_AerialSports.disableOkButton}"></h:commandButton>
							</td>							
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
