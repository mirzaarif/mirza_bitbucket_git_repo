<!-- CHANGE LOG -->
<!-- Audit Number   	Version   Change Description -->
<!-- FNB004         	NB-1101	PHI-->
<!-- FNB004 - VEPL1206  NB-1101	Look into why "java.lang.IllegalArgumentException" is appearing onbottom of interview screen.-->
<!-- NBA324         	NB-1301	nbAFull Personal History Interview -->
<!-- SPRNBA-798         NB-1401   Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 6 Health Quest</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame() {
				getScrollXY('phiinterview');
				document.forms['phiinterview'].target='controlFrame';
				return false;
			}
			
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('phiinterview');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}			
			function toggleCBGroup(formName,id,prefix) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					   alert(inputs[i].name);					  
						if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
							inputs[i].checked = false ;
						}
					}   
				}   
			}
			
	
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE6HEALTHQUEST" value="#{pc_Page6HealthQuest}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>			
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				<div id="Messages" style="display: none"><h:messages></h:messages></div> <!-- FNB004 - VEPL1206  -->
				<h:inputHidden id="scrollx" value="#{pc_Page6HealthQuest.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page6HealthQuest.scrollYC}"></h:inputHidden>
				<div class="inputForm">
                                 <h:outputLabel value="#{componentBundle.HealthQuestiona}" style="width:508px" styleClass="sectionSubheader"> <!-- NBA324 -->
                                 </h:outputLabel>
                                 <h:panelGroup id="familyhistory">
				           <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Inyourimmediate}" style="text-align:left; width: 440px;margin-left:5px;" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.checkbox1No}" onclick="toggleCBGroup(this.form.id, 'checkbox1No',  'checkbox1')" id="checkbox1No"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.checkbox1Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox1Yes',  'checkbox1');if(document.getElementById(this.id).checked){launchDetailsPopUp('page6FamilyHistory')};" id="checkbox1Yes"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6FamilyHistory" styleClass="ovitalic#{pc_Page6HealthQuest.hasFamilyHistory}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6FamilyHistory');" style="margin-right:10px;margin-left:40px;"/>

 

				         
				     </h:panelGroup> 
				     <Help:Link contextId="P3_H_FamilyHistory" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="P3_W_FamilyHistory" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="fatherhistory">
				           <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 25px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.FamilyHistory}" style=" text-align:left; width: 440px;margin-left:5px;" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Father}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.living}" id="fatherliving" onclick="toggleCBGroup(this.form.id, 'fatherliving',  'father')" ></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.deceased}" id="fatherdeceased" onclick="toggleCBGroup(this.form.id, 'fatherdeceased',  'father')" ></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 91px; margin-right:10px;" styleClass="formLabelRight"></h:outputLabel>
                                 </h:panelGroup>
						 <Help:Link contextId="P4A_H_Father" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
						<h:panelGroup id="fatherhistory_1">
                                    <h:outputLabel value="#{componentBundle.AgeStateofHealt}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="stateofhealth" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageStateofHealth}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6FatherHealthDetails" styleClass="ovitalic#{pc_Page6HealthQuest.hasFatherHealthDetails}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6FatherHealthDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="#{componentBundle.AgeatDeathCause}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="ageatdeath" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageatDeathCauseofDeath}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6FatherCauseOfDeath" styleClass="ovitalic#{pc_Page6HealthQuest.hasFatherCauseOfDeath}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6FatherCauseOfDeath');" style="margin-right:10px;margin-left:15px;"/>

                                 	         
				     </h:panelGroup> 
                            
                             <h:panelGroup id="motherhistory">
				           <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 25px;" ></h:outputLabel>                                  
                                   <h:outputLabel value="#{componentBundle.Mother}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.living_0}" id="motherliving" onclick="toggleCBGroup(this.form.id, 'motherliving',  'mother')" ></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page6HealthQuest.deceased_0}" id="motherdeceased" onclick="toggleCBGroup(this.form.id, 'motherdeceased',  'mother')" ></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 91px; margin-right:10px;" styleClass="formLabelRight"></h:outputLabel>
                                   </h:panelGroup>
						 <Help:Link contextId="P4A_H_Mother" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
						<h:panelGroup id="motherhistory_1">
                                    <h:outputLabel value="#{componentBundle.AgeStateofHealt}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="stateofhealth_0" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageStateofHealth_0}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6MotherHealthDetails" styleClass="ovitalic#{pc_Page6HealthQuest.hasMotherHealthDetails}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6MotherHealthDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="#{componentBundle.AgeatDeathCause}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="ageatdeath_0" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageatDeathCauseofDeath_0}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6MotherCauseOfDeath" styleClass="ovitalic#{pc_Page6HealthQuest.hasMotherCauseOfDeath}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6MotherCauseOfDeath');" style="margin-right:10px;margin-left:15px;"/>

                                 	         
				     </h:panelGroup> 
                             <h:panelGroup id="brotherhistory">
				           <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 25px;" ></h:outputLabel>                                  
                                   <h:outputLabel value="#{componentBundle.Brothers}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:95px;"></h:outputLabel>                                   
                                   </h:panelGroup>
							 <Help:Link contextId="P4A_H_Brother" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
							<h:panelGroup id="brotherhistory_1">
                                    <h:outputLabel value="#{componentBundle.AgeStateofHealt}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="stateofhealth_1" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageStateofHealth_1}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6BrotherHealthDetails" styleClass="ovitalic#{pc_Page6HealthQuest.hasBrotherHealthDetails}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6BrotherHealthDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="#{componentBundle.AgeatDeathCause}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="ageatdeath_1" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageatDeathCauseofDeath_1}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6BrotherCauseOfDeath" styleClass="ovitalic#{pc_Page6HealthQuest.hasBrotherCauseOfDeath}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6BrotherCauseOfDeath');" style="margin-right:10px;margin-left:15px;"/>

                                 	         
				     </h:panelGroup> 
                             <h:panelGroup id="sisterhistory">
				           <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 25px;" ></h:outputLabel>                                  
                                   <h:outputLabel value="#{componentBundle.Sisters}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:95px;"></h:outputLabel>                                   
                                     </h:panelGroup>
							 <Help:Link contextId="P4A_H_Sister" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
							<h:panelGroup id="sisterhistory_1">
                                    <h:outputLabel value="#{componentBundle.AgeStateofHealt}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="stateofhealth_2" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageStateofHealth_2}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Living}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6SisterHealthDetails" styleClass="ovitalic#{pc_Page6HealthQuest.hasSisterHealthDetails}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6SisterHealthDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="#{componentBundle.AgeatDeathCause}" styleClass="formLabel" style="width: 200px;"></h:outputLabel>
                                    <h:inputText id="ageatdeath_2" styleClass="formEntryTextHalf" style="margin-right:10px;width:80px;" value="#{pc_Page6HealthQuest.ageatDeathCauseofDeath_2}"></h:inputText>
						<h:outputLabel value="#{componentBundle.Deceased}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
                                    <h:commandButton id="page6SisterCauseOfDeath" styleClass="ovitalic#{pc_Page6HealthQuest.hasSisterCauseOfDeath}" image="images/circle_i.gif"  
								onclick="launchDetailsPopUp('page6SisterCauseOfDeath');" style="margin-right:10px;margin-left:15px;"/>

                                 	         
				     </h:panelGroup> 

 

 


                         
					
                           <f:subview id="tabHeader">
				       <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			        </f:subview>

				</div>
                         <h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page6HealthQuest.commitAction}"></h:commandButton>

			</h:form>
			<h:messages></h:messages>
		</f:view>
</body>
</html>
