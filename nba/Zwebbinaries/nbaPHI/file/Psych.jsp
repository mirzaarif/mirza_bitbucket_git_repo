<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Psych</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
                getScrollXY('page');			
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			function valueChangeEvent() {	
			    getScrollXY('page');
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PSYCH" value="#{pc_Psych}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Psych.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Psych.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="350">
							<col width="150">
								<col width="130">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Psych}" style="width: 220px" id="labePsych" rendered="#{pc_Psych.showLabePsych}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="3">
											<h:outputLabel style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.diagnosedwithstress}" styleClass="formLabel" id="labelHaveYouEver" rendered="#{pc_Psych.showLabelHaveYouEver}"></h:outputLabel>
										</td>
									</tr>
								<tr style="padding-top: 5px;">
										<td colspan="1"></td>
										<td colspan="1" align="left">
											<h:selectBooleanCheckbox value="#{pc_Psych.diagnosedStressNo}" id="diagnosedStressNo" onclick="toggleCBGroup(this.form.id, 'diagnosedStressNo',  'diagnosedStress',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Psych.diagnosedStressYes}" id="diagnosedStressYes" valueChangeListener="#{pc_Psych.collapseDiagnosedWithStress}" onclick="toggleCBGroup(this.form.id, 'diagnosedStressYes',  'diagnosedStress',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('psychWhatWasDiagnosisDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td style="margin-left: 124px"></td>
						<DynaDiv:DynamicDiv id="diagnosedStressSection" rendered="#{pc_Psych.expandDiagnosedWithStressInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.IfYes}" styleClass="formLabel" id="labelIFYes" rendered="#{pc_Psych.showLabelIFYes}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.WhatwasthediagnGQ}" styleClass="formLabel" id="labelWhatWasDiagnosis" rendered="#{pc_Psych.showLabelWhatWasDiagnosis}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>							
								<h:commandButton id="psychWhatWasDiagnosisDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhatWasDiagnosisDetails');" style="margin-right:200px;" rendered="#{pc_Psych.showWhatWasDiagnosisDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhatWasDiagnosisDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Whenwereyoufirs}" styleClass="formLabel" id="labelWhenYouFirstDiagnosis" rendered="#{pc_Psych.showLabelWhenYouFirstDiagnosis}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychWhenYouFirstDiagnosisDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhenYouFirstDiagnosisDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWhenYouFirstDiagnosisDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhenYourFirstDiagnosisDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Nameandaddresso}" styleClass="formLabel" id="labelNameAndAddress" rendered="#{pc_Psych.showLabelNameAndAddress}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychNameAndAddressDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychNameAndAddressDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showNameAndAddressDetailsImg}" styleClass="ovitalic#{pc_Psych.hasNameAndAddressDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 296px;text-align:left;margin-left:45px;" value="#{componentBundle.Isthispersoname}" styleClass="formLabel" id="labelIsThisPersonMedicalDoctor" rendered="#{pc_Psych.showLabelIsThisPersonMedicalDoctor}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychIsThisPersonMedicalDoctorDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychIsThisPersonMedicalDoctorDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showIsThisPersonMedicalDoctorDetailsImg}" styleClass="ovitalic#{pc_Psych.hasIsThisPersonMedicalDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Whenwasyoulastv}" styleClass="formLabel" id="labelWhenWasYouLastVisit" rendered="#{pc_Psych.showLabelWhenWasYouLastVisit}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>							
								<h:commandButton id="psychWhenWasYouLastVisitDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhenWasYouLastVisitDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWhenWasYouLastVisitDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhenWasYourLastVisitDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Doyouhaveregula}" styleClass="formLabel" id="labelDoYouRegularFollowups" rendered="#{pc_Psych.showLabelDoYouRegularFollowups}"></h:outputLabel>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouRegularFollowupsNo}" id="doYouRegularFollowupsNo" onclick="toggleCBGroup(this.form.id, 'doYouRegularFollowupsNo',  'doYouRegularFollowups')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouRegularFollowupsYes}" id="doYouRegularFollowupsYes" onclick="toggleCBGroup(this.form.id, 'doYouRegularFollowupsYes',  'doYouRegularFollowups');if(document.getElementById(this.id).checked){launchDetailsPopUp('psychIfSoHowOftenDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 296px;text-align:left;margin-left:45px;" value="#{componentBundle.Ifsohowoften}" styleClass="formLabel" id="labelIfSoHowOften" rendered="#{pc_Psych.showLabelIfSoHowOften}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>							
								<h:commandButton id="psychIfSoHowOftenDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychIfSoHowOftenDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showIfSoHowOftenDetailsImg}" styleClass="ovitalic#{pc_Psych.hasIsSoHowOftenDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoutakenany}" styleClass="formLabel" id="labelHaveYouTakenAnyMedication" rendered="#{pc_Psych.showLabelHaveYouTakenAnyMedication}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.takenAnyMedicationLast5YearsNo}" id="takenAnyMedicationLast5YearsNo" onclick="toggleCBGroup(this.form.id, 'takenAnyMedicationLast5YearsNo',  'takenAnyMedicationLast5Years',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.takenAnyMedicationLast5YearsYes}" id="takenAnyMedicationLast5YearsYes" valueChangeListener="#{pc_Psych.collapseMedicationSection}" onclick="toggleCBGroup(this.form.id, 'takenAnyMedicationLast5YearsYes',  'takenAnyMedicationLast5Years',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('psychWhatistheNameOfMedicationDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="medicationSection" rendered="#{pc_Psych.expandMedicationSectionInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.IfYes}" styleClass="formLabel" id="labeltakenAnyMedicationIfYes" rendered="#{pc_Psych.showLabeltakenAnyMedicationIfYes}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Whatisthenameof}" styleClass="formLabel" id="labelWhatistheNameOfMedication" rendered="#{pc_Psych.showLabelWhatistheNameOfMedication}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychWhatistheNameOfMedicationDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhatistheNameOfMedicationDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWhatistheNameOfMedicationDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhatIsTheNameOfMedicationDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Whatisthedosage}" styleClass="formLabel" id="labelWhatisTheDosage" rendered="#{pc_Psych.showLabelWhatisTheDosage}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychWhatisTheDosageDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhatisTheDosageDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWhatisTheDosageDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhatIsTheDosageDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Doyoutakeitaspr}" styleClass="formLabel" id="labelDoYouTakeItAsAPrescribed" rendered="#{pc_Psych.showLabelDoYouTakeItAsAPrescribed}"></h:outputLabel>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouTakeItAsPrescribedNo}" id="doYouTakeItAsPrescribedNo" onclick="toggleCBGroup(this.form.id, 'doYouTakeItAsPrescribedNo',  'doYouTakeItAsPrescribed');if(document.getElementById(this.id).checked){launchDetailsPopUp('psychIfNoWhyNotDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouTakeItAsPrescribedYes}" id="doYouTakeItAsPrescribedYes" onclick="toggleCBGroup(this.form.id, 'doYouTakeItAsPrescribedYes',  'doYouTakeItAsPrescribed')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;padding-bottom:10px;margin-left:45px;" value="#{componentBundle.Ifnowhynot}" styleClass="formLabel" id="labelIfNoWhyNot" rendered="#{pc_Psych.showLabelIfNoWhyNot}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychIfNoWhyNotDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychIfNoWhyNotDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showIfNoWhyNotDetailsImg}" styleClass="ovitalic#{pc_Psych.hasIfNoWhyNOtDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Howlonghaveyoubeenmedicines}" styleClass="formLabel" id="labelHowLongHaveYouBeenOnThisMedicine" rendered="#{pc_Psych.showLabelHowLongHaveYouBeenOnThisMedicine}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychHowLongHaveYouBeenOnThisMedicineDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychHowLongHaveYouBeenOnThisMedicineDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showHowLongHaveYouBeenOnThisMedicineDetailsImg}" styleClass="ovitalic#{pc_Psych.hasHowLongHaveYouBeenOnThisMedicineDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Doesthemedicati}" styleClass="formLabel" id="labeldoesMedicationHelp" rendered="#{pc_Psych.showLabeldoesMedicationHelp}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychDoesMedicationHelpDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychDoesMedicationHelpDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showDoesMedicationHelpDetailsImg}" styleClass="ovitalic#{pc_Psych.hasDoesMedicationDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Haveyoubeenonan}" styleClass="formLabel" id="labelHaveYouBeenOnAnyOtherMedications" rendered="#{pc_Psych.showLabelHaveYouBeenOnAnyOtherMedications}"></h:outputLabel>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.takenAnyMedicationNo}" id="beenOnMedicationNo" onclick="toggleCBGroup(this.form.id, 'beenOnMedicationNo',  'beenOnMedication')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.takenAnyMedicationYes}" id="beenOnMedicationYes" onclick="toggleCBGroup(this.form.id, 'beenOnMedicationYes',  'beenOnMedication');if(document.getElementById(this.id).checked){launchDetailsPopUp('psychHaveYouBeenOnAnyOtherMedications')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>								
								<h:commandButton id="psychHaveYouBeenOnAnyOtherMedications" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychHaveYouBeenOnAnyOtherMedications');" style="margin-right:10px;" rendered="#{pc_Psych.show_0}" styleClass="ovitalic#{pc_Psych.hasHaveYouBeenOnOtherMedicationDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 300px;text-align:left;padding-bottom:10px;" value="#{componentBundle.Whatwereareyour}" styleClass="formLabel" id="labelWhatWasSyptom" rendered="#{pc_Psych.showLabelWhatWasSyptom}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.Forexample}" styleClass="formLabel" id="labelWhatWasSyptomForEx" rendered="#{pc_Psych.showLabelWhatWasSyptomForEx}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychWhatWasSyptomForExDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWhatWasSyptomForExDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWhatWasSyptomForExDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWhatWasSymptomForDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.FatigueInsomnia}" styleClass="formLabel" id="labelFatigue" rendered="#{pc_Psych.showLabelFatigue}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Lossofappetite}" styleClass="formLabel" id="labelLossOfApetite" rendered="#{pc_Psych.showLabelLossOfApetite}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Feelingbluecryi}" styleClass="formLabel" id="labelFeelingBlue" rendered="#{pc_Psych.showLabelFeelingBlue}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Memoryloss}" styleClass="formLabel" id="labelMemoryLoss" rendered="#{pc_Psych.showLabelMemoryLoss}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Panic}" styleClass="formLabel" id="labelPanic" rendered="#{pc_Psych.showLabelPanic}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Anxious}" styleClass="formLabel" id="labelAnxious" rendered="#{pc_Psych.showLabelAnxious}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Anythingelse}" styleClass="formLabel" id="labelAnyThingElse" rendered="#{pc_Psych.showLabelAnyThingElse}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Inthepast3years}" styleClass="formLabel" id="labelInThePast3Years" rendered="#{pc_Psych.showLabelInThePast3Years}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.inThePast3YearsHaveYouMissedNo}" id="inThePast3YearsHaveYouMissedNo" onclick="toggleCBGroup(this.form.id, 'inThePast3YearsHaveYouMissedNo',  'inThePast3YearsHaveYouMissed',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.inThePast3YearsHaveYouMissedYes}" id="inThePast3YearsHaveYouMissedYes" valueChangeListener="#{pc_Psych.collapseMissedWorkSection}" onclick="toggleCBGroup(this.form.id, 'inThePast3YearsHaveYouMissedYes',  'inThePast3YearsHaveYouMissed',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('psychifSoMuchHowDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="missedWorkSection" rendered="#{pc_Psych.expandMissedWorkSectionInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Ifsohowmuch}" styleClass="formLabel" id="labelIfSoHowMuch" rendered="#{pc_Psych.showLabelIfSoHowMuch}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>							
								<h:commandButton id="psychifSoMuchHowDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychifSoMuchHowDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showIfSoMuchHowDetailsImg}" styleClass="ovitalic#{pc_Psych.hasIfSoHowMuchDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Forwhatreasons}" styleClass="formLabel" id="labelForWhatReasons" rendered="#{pc_Psych.showLabelForWhatReasons}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>							
								<h:commandButton id="psychForWhatReasonsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychForWhatReasonsDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showForWhatReasonsDetailsImg}" styleClass="ovitalic#{pc_Psych.hasForWhatReasonDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Weretheseconsec}" styleClass="formLabel" id="labelWereTheseConsecutiveDays" rendered="#{pc_Psych.showLabelWereTheseConsecutiveDays}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychWereTheseConsecutiveDaysDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychWereTheseConsecutiveDaysDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showWereTheseConsecutiveDaysDetailsImg}" styleClass="ovitalic#{pc_Psych.hasWereTheseConsecutiveDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyoufeelyourco}" styleClass="formLabel" id="labelDofeelYourCondition" rendered="#{pc_Psych.showLabelDofeelYourCondition}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouFeelYourConditionRelatedToBusinessNo}" id="doYouFeelYourConditionRelatedToBusinessNo" onclick="toggleCBGroup(this.form.id, 'doYouFeelYourConditionRelatedToBusinessNo',  'doYouFeelYourConditionRelatedToBusiness')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.doYouFeelYourConditionRelatedToBusinessYes}" id="doYouFeelYourConditionRelatedToBusinessYes" onclick="toggleCBGroup(this.form.id, 'doYouFeelYourConditionRelatedToBusinessYes',  'doYouFeelYourConditionRelatedToBusiness');if(document.getElementById(this.id).checked){launchDetailsPopUp('psychDoYouFeelYourConditionRelatedToBusinessIfYesDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" value="#{componentBundle.Ifyespleaseexplain}" styleClass="formLabel" id="labelDoYouFeelYourConditionRelatedToBusinessIfYes" rendered="#{pc_Psych.showLabelDoYouFeelYourConditionRelatedToBusinessIfYes}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychDoYouFeelYourConditionRelatedToBusinessIfYesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychDoYouFeelYourConditionRelatedToBusinessIfYesDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showDoYouFeelYourConditionRelatedToBusinessIfYesDetailsImg}" styleClass="ovitalic#{pc_Psych.hasDoYouFeelYourConditionDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left" colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Isthereanyfamilyhistory}" styleClass="formLabel" id="labelIsThereAnyFamilyHistory" rendered="#{pc_Psych.showLabelIsThereAnyFamilyHistory}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_Psych.isThereAnyFamilyHistoryNo}" id="isThereAnyFamilyHistoryNo" onclick="toggleCBGroup(this.form.id, 'isThereAnyFamilyHistoryNo',  'isThereAnyFamilyHistory')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Psych.isThereAnyFamilyHistoryYes}" id="isThereAnyFamilyHistoryYes" onclick="toggleCBGroup(this.form.id, 'isThereAnyFamilyHistoryYes',  'isThereAnyFamilyHistory');if(document.getElementById(this.id).checked){launchDetailsPopUp('psychRelationshipDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" value="#{componentBundle.Ifyes}" styleClass="formLabel" id="isThereAnyFamilyHistoryIfYes" rendered="#{pc_Psych.showIsThereAnyFamilyHistoryIfYes}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputLabel style="width: 300px;text-align:left;margin-left:30px;" value="#{componentBundle.Relationshipand}" styleClass="formLabel" id="labelRelationship" rendered="#{pc_Psych.showLabelRelationship}"></h:outputLabel>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="psychRelationshipDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('psychRelationshipDetails');" style="margin-right:10px;" rendered="#{pc_Psych.showRelationshipDetailsImg}" styleClass="ovitalic#{pc_Psych.hasRelationshipDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left" style="height: 27px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Psych.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_Psych.show_1}" disabled="#{pc_Psych.disable_1}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_Psych.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{pc_Psych.show_2}" disabled="#{pc_Psych.disable_2}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Psych.okAction}" onclick="resetTargetFrame(this.form.id);" id="_3" rendered="#{!pc_Links.showIdPsych}" disabled="#{pc_Psych.disable_3}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Psych.okAction}" onclick="resetTargetFrame(this.form.id);" id="_33" rendered="#{pc_Links.showIdPsych}" disabled="#{pc_Psych.disable_3}"></h:commandButton>
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 24px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
