<!-- CHANGE LOG -->
<!-- Audit Number   	Version   Change Description -->
<!-- FNB004         	NB-1101	PHI-->
<!-- FNB004 - VEPL1206  NB-1101	Look into why "java.lang.IllegalArgumentException" is appearing onbottom of interview screen.-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>final Review Popup</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: hidden;" onload="filePageInit();scrollToCoordinates('finalReviewPopup') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_FINALREVIEWPOPUP" value="#{pc_FinalReviewPopup}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<div id="Messages" style="display: none"><h:messages></h:messages></div><!-- FNB004 - VEPL1206  -->
			<h:form styleClass="inputFormMat" id="finalReviewPopup">
				<h:inputHidden id="scrollx" value="#{pc_FinalReviewPopup.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_FinalReviewPopup.scrollYC}"></h:inputHidden>
				<div style="overflow: scroll;overflow-x: hidden; height:610px;width:630;margin-left:20px" Class="inputForm">
					<table align="left" border="0" width="600px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed;" cellpadding="0" cellspacing="0">
						<col width="455"></col>
						<col width="35"></col>
						<tr style="padding-top: 5px">
								<td colspan="5" class="sectionSubheader" align="left"> <!-- NBA324 -->
														<h:outputLabel value="Final Review" style="width: 220px" id="finalReviewHeader"></h:outputLabel>
													</td>
						</tr>
						<tr>
							<td align="left">
								<h:outputLabel style="width:450px;text-align:left;margin-left:5px;margin-bottom:5px;margin-top:5px;" value="#{componentBundle.Thankyouforansw}" styleClass="formLabel" id="_0" rendered="#{pc_FinalReviewPopup.show_0}"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="left"></td>
							<td></td>
						</tr>
						<tr>
							<td align="left"></td>
							<td></td>
						</tr>
						<tr>
							<td align="left">
								<h:outputLabel style="width:450px;text-align:left;margin-left:5px;margin-bottom:5px;margin-top:5px;" value="#{componentBundle.Haveyoureceived}" styleClass="formLabel" id="_1" rendered="#{pc_FinalReviewPopup.show_1}"></h:outputLabel>
								<h:outputLabel style="width: 440px;text-align:left;margin-left:15px;padding-top:10px; padding-bottom:5px;" value="#{componentBundle.Ifapplicanthasn}" styleClass="formLabel" id="_2" rendered="#{pc_FinalReviewPopup.show_2}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.BeforeIletyougo}" styleClass="formLabel" id="_3" rendered="#{pc_FinalReviewPopup.show_3}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Yourapplication}" styleClass="formLabel" id="_4" rendered="#{pc_FinalReviewPopup.show_4}"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="left">
								<h:outputLabel style="width: 391px;text-align:left;padding-top:5px;margin-left:55px;" value="#{componentBundle.Field1Examples}" styleClass="formLabel" id="_5" rendered="#{pc_FinalReviewPopup.show_5}"></h:outputLabel>
							</td>
							<td align="right"></td>
						</tr>
						<tr>
							<td colspan="1" align="left" class="">
								<h:outputLabel style="width: 369px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:75px;" value="#{componentBundle.Aparamedexamphy}" styleClass="formLabel" id="_6" rendered="#{pc_FinalReviewPopup.show_6}"></h:outputLabel>
								<h:outputLabel style="width: 369px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:75px;" value="#{componentBundle.Aparamedexamphy_0}" styleClass="formLabel" id="_7" rendered="#{pc_FinalReviewPopup.show_7}"></h:outputLabel>
								<h:outputLabel style="width: 369px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:75px;" value="#{componentBundle.Aparamedphysici}" styleClass="formLabel" id="_8" rendered="#{pc_FinalReviewPopup.show_8}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Anameofcompanyw}" styleClass="formLabel" id="_9" rendered="#{pc_FinalReviewPopup.show_9}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Thisexamwillbec}" styleClass="formLabel" id="_10" rendered="#{pc_FinalReviewPopup.show_10}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Ifparamedexam}" styleClass="formLabel" id="_11" rendered="#{pc_FinalReviewPopup.show_11}"></h:outputLabel>
								<h:outputLabel style="width: 400px;text-align:left;padding-top:5px;margin-left:55px;" value="#{componentBundle.Field1Theparamedexam}" styleClass="formLabel" id="_12" rendered="#{pc_FinalReviewPopup.show_12}"></h:outputLabel>
								<h:outputLabel style="width: 400px;text-align:left;padding-top:5px;margin-left:55px;" value="#{componentBundle.Field2Theexaminercan}" styleClass="formLabel" id="_13" rendered="#{pc_FinalReviewPopup.show_13}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.IfPhysicianexam}" styleClass="formLabel" id="_14" rendered="#{pc_FinalReviewPopup.show_14}"></h:outputLabel>
								<h:outputLabel style="width: 400px;text-align:left;padding-top:5px;margin-left:55px;" value="#{componentBundle.Field1Mostlikelythee}" styleClass="formLabel" id="_15" rendered="#{pc_FinalReviewPopup.show_15}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Toprepareforexa}" styleClass="formLabel" id="_16" rendered="#{pc_FinalReviewPopup.show_16}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Nocaffeineinclu}" styleClass="formLabel" id="_17" rendered="#{pc_FinalReviewPopup.show_17}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Nojuices}" styleClass="formLabel" id="_18" rendered="#{pc_FinalReviewPopup.show_18}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Ifyouarediabeti}" styleClass="formLabel" id="_19" rendered="#{pc_FinalReviewPopup.show_19}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Ifyouhavemedici}" styleClass="formLabel" id="_20" rendered="#{pc_FinalReviewPopup.show_20}"></h:outputLabel>
								<h:outputLabel style="width: 440px;text-align:left;margin-left:15px;padding-top:10px; padding-bottom:5px;" value="#{componentBundle.Ifapplicanthass}" styleClass="formLabel" id="_21" rendered="#{pc_FinalReviewPopup.show_21}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Toprepareforexa}" styleClass="formLabel" id="_22" rendered="#{pc_FinalReviewPopup.show_22}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Nocaffeineinclu}" styleClass="formLabel" id="_23" rendered="#{pc_FinalReviewPopup.show_23}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Nojuices}" styleClass="formLabel" id="_24" rendered="#{pc_FinalReviewPopup.show_24}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Ifyouarediabeti}" styleClass="formLabel" id="_25" rendered="#{pc_FinalReviewPopup.show_25}"></h:outputLabel>
								<h:outputLabel style="width: 419px;text-align:left;padding-top:5px;padding-bottom:5px;margin-left:30px;" value="#{componentBundle.Ifyouhavemedici_0}" styleClass="formLabel" id="_26" rendered="#{pc_FinalReviewPopup.show_26}"></h:outputLabel>
								<h:outputLabel style="width: 440px;text-align:left;margin-left:15px;padding-top:10px; padding-bottom:5px;" value="#{componentBundle.Ifexamhasbeenco}" styleClass="formLabel" id="_27" rendered="#{pc_FinalReviewPopup.show_27}"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="right"></td>
							<td></td>
						</tr>
					</table>
				</div>
				<table align="left" border="0" width="480px">
					<tr>
						<td style="padding-bottom:30px;">
							<h:commandButton styleClass="buttonLeft" value="Cancel" action="#{pc_FinalReviewPopup.cancelAction}" onclick="resetTargetFrame(this.form.id);"></h:commandButton>
						</td>
					</tr>
				</table>
			</h:form>
			<!-- FNB004 - VEPL1206 deleted -->
		</f:view>
</body>
</html>
