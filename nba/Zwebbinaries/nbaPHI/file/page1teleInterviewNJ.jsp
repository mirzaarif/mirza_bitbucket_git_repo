<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            
            String basePath = "";            
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }            
            
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 1 Tele Interview</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
		<script type="text/javascript" src="javascript/global/main.js"></script>
        <script type="text/javascript" src="javascript/global/file.js"></script>
        <script type="text/javascript" src="javascript/global/scroll.js"></script>
        <script type="text/javascript" src="javascript/nbapopup.js"></script>       
        <script type="text/javascript">
			var contextpath = '<%=path%>';
			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);				
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);				
				document.forms[formName].target='';
				return false;
			}			
			function submitContents() {			    
			    var x=document.getElementById("interviewpage");			    
				x.contentWindow.document.forms['phiinterview']['phiinterview:hiddenSaveButton'].click();							  
			    return false;
		    }	
		    function closePHI(){			    
			    document.forms['page1TeleInterview']['page1TeleInterview:hiddenSaveButton'].click();
			    		    
		     }	
		     function submitQuestionnaire() {		      
		        document.getElementById("interviewpage").contentWindow.document.forms['phiinterview'].submit();				
		        return true;
		     }	   
		     function launchDetailsPopUp(componentID) {	
		getScrollXY('phiinterview');
			} 
			//-->
		
     </script>
     <script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
     </head>
     <body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page1TeleInterview');">
		 <f:view>
		 <PopulateBean:Load serviceName="RETRIEVE_PHISTATUS"	value="#{pc_PHIStatus}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE1TELEINTERVIEW" value="#{pc_Page1TeleInterview}"></PopulateBean:Load>				
			<PopulateBean:Load serviceName="RETRIEVE_LINKS" value="#{pc_Links}"></PopulateBean:Load>			
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form id="page1TeleInterview">
				<h:inputHidden id="scrollx" value="#{pc_Page1TeleInterview.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page1TeleInterview.scrollYC}"></h:inputHidden>
				<table width="100%" align="center" border="0" class="inputForm">	
				   <tr>
						<td align="left" colspan="2" class="pageHeader"> <!-- NBA324 -->							
							<h:commandButton id="linkBackToStatus"  action="#{pc_Page1TeleInterview.actionPHIStatus}" type="submit"  rendered="#{pc_PHIStatus.phiBriefCase}" image="images/backToStatus.gif" >
								<!-- NBA324 deleted -->
							</h:commandButton>						
							<h:commandLink id="_0"  onmousedown="submitContents();closePHI();" rendered="#{!pc_PHIStatus.phiBriefCase}">
								<h:outputText styleClass="phText" value="Back To Status" id="backstatus" ></h:outputText> <!-- NBA324 -->
							</h:commandLink>
						</td>
					</tr>	
                 <tr>
							     <td align="right">
							     </td>
							     <td style="padding-bottom: 3px;">
								     <h:commandButton id="links_x" style="display:none" action="#{pc_Page1TeleInterview.finalReviewAction}" >
								     </h:commandButton>
							    </td>
						      </tr>		  
				 
					<tr>
						<td style="width: 87%">
						  <iframe name="interviewpage" id="interviewpage" width="100%" overflow-x="hidden"; 
							height="600" frameborder="0" src=<c:out value="${FinalPage}"/> ></iframe>
						</td>
						<td style="width: 16%">
						    <table align="left" border="0" width="120" cellpadding="0" cellspacing="0">
						      <tr>
							     <td align="right" style="width:6px">
								     <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idAerialSports" rendered="#{pc_Links.showIdAerialSports}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px; padding-top: 10px">
								    <h:commandLink id="links_0" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionAerialSports}" rendered="#{pc_Links.show_0}">
									   <h:outputText value="#{componentBundle.AerialSports}" styleClass="phiLink"  id="idAerialSportsText" rendered="#{pc_Links.showIdAerialSportsText}"></h:outputText> <!-- NBA324 -->
								    </h:commandLink>
							    </td>
						     </tr>
						      <tr>
							     <td align="right">
								      <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idalcohol" rendered="#{pc_Links.showIdAlcohol}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								     <h:commandLink id="links_1" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionAlcohol}" rendered="#{pc_Links.show_1}">
									    <h:outputText value="#{componentBundle.Alcohol}" styleClass="phiLink"  id="idAlcoholText" rendered="#{pc_Links.showIdAlcoholText}"></h:outputText> <!-- NBA324 -->
								     </h:commandLink>
							    </td>
						      </tr>
						      <tr>
							     <td align="right">
								       <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idArthritis" rendered="#{pc_Links.showIdArthritis}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								      <h:commandLink id="links_2" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionArthritis}" rendered="#{pc_Links.show_2}">
									     <h:outputText value="#{componentBundle.Arthritis}" styleClass="phiLink"  id="idArthritisText" rendered="#{pc_Links.showIdArthritisText}"></h:outputText> <!-- NBA324 -->
								      </h:commandLink>
							    </td>
						      </tr>
						      <tr>
							     <td align="right">
								     <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idAviation" rendered="#{pc_Links.showIdAviation}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								    <h:commandLink id="links_3" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionAviation}" rendered="#{pc_Links.show_3}">
									   <h:outputText value="#{componentBundle.Aviation}" styleClass="phiLink"  id="idAviationText" rendered="#{pc_Links.showIdAviationText}"></h:outputText> <!-- NBA324 -->
								    </h:commandLink>									   
							     </td>
						       </tr>
						       <tr>
							     <td align="right">
								    <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idBackAndNeck" rendered="#{pc_Links.showIdBackAndNeck}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								     <h:commandLink id="links_4" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionBackAndNeck}" rendered="#{pc_Links.show_4}">
									    <h:outputText value="#{componentBundle.BackandNeck}" styleClass="phiLink"  id="idBackAndNeckText" rendered="#{pc_Links.showIdBackAndNeckText}"></h:outputText> <!-- NBA324 -->
								     </h:commandLink>
							     </td>
						       </tr>
						       <tr>
							     <td align="right">
								    <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idChestPain" rendered="#{pc_Links.showIdChestPain}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								    <h:commandLink id="links_5" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionChestPain}" rendered="#{pc_Links.show_5}">
									   <h:outputText value="#{componentBundle.ChestPain}" styleClass="phiLink"  id="idChestPainText" rendered="#{pc_Links.showIdChestPainText}"></h:outputText> <!-- NBA324 -->
								    </h:commandLink>
							     </td>
						       </tr>
						       <tr>
						 	     <td align="right">
								    <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idColonoscopy" rendered="#{pc_Links.showIdColonoscopy}"></h:outputText>
							     </td>
							     <td style="padding-bottom: 5px;">
								    <h:commandLink id="links_6" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionColonoscopy}" rendered="#{pc_Links.show_6}">
									    <h:outputText value="#{componentBundle.Colonoscopy}" styleClass="phiLink"  id="idColonoscopyText" rendered="#{pc_Links.showIdColonoscopyText}"></h:outputText> <!-- NBA324 -->
								    </h:commandLink>
							    </td>
						      </tr>
						      <tr>
							    <td align="right">
								   <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDiabetic" rendered="#{pc_Links.showIdDiabetic}"></h:outputText>
							    </td>
							     <td style="padding-bottom: 5px;">
								    <h:commandLink id="links_7" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionDiabetic}" rendered="#{pc_Links.show_7}">
									   <h:outputText value="#{componentBundle.Diabetic}" styleClass="phiLink"  id="id_QYDF90Gk" rendered="#{pc_Links.showId_QYDF90Gk}"></h:outputText> <!-- NBA324 -->
								    </h:commandLink>
							    </td>
						      </tr>
						      <tr>
							     <td align="right">
								    <h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDriving" rendered="#{pc_Links.showIdDriving}"></h:outputText>
						     	</td>
							    <td style="padding-bottom: 5px;">
								   <h:commandLink id="links_8" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionDriving}" rendered="#{pc_Links.show_8}">
									   <h:outputText value="#{componentBundle.Driving}" styleClass="phiLink"  id="idDrivingText" rendered="#{pc_Links.showIdDrivingText}"></h:outputText> <!-- NBA324 -->
								   </h:commandLink>
							    </td>
						     </tr>
						    <tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDrugUsage" rendered="#{pc_Links.showIdDrugUsage}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="links_9" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionDrugUsage}" rendered="#{pc_Links.show_9}">
									<h:outputText value="#{componentBundle.DrugUsage}" styleClass="phiLink"  id="idDrugUsageText" rendered="#{pc_Links.showIdDrugUsageText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idExtremeSports" rendered="#{pc_Links.showIdExtremeSports}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_10" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionExtremeSports}" rendered="#{pc_Links.show_10}" >
									<h:outputText value="#{componentBundle.ExtremeSports}" styleClass="phiLink"  id="idExtremeSportsText" rendered="#{pc_Links.showIdExtremeSportsText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idForeignTravel" rendered="#{pc_Links.showIdForeignTravel}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_11" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionForeignTravel}" rendered="#{pc_Links.show_11}">
									<h:outputText value="#{componentBundle.ForeignTravel}" styleClass="phiLink"  id="idForeignTravelText" rendered="#{pc_Links.showIdForeignTravelText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idGastrointestinal" rendered="#{pc_Links.showIdGastrointestinal}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink  id="_12" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionGastrointestinal}" rendered="#{pc_Links.show_12}">
									<h:outputText value="#{componentBundle.Gastrointestina}" styleClass="phiLink"  id="idGastrointestinalText" rendered="#{pc_Links.showIdGastrointestinalText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idMilitary" rendered="#{pc_Links.showIdMilitary}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_13" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionMilitary}" rendered="#{pc_Links.show_13}">
									<h:outputText value="#{componentBundle.Military}" styleClass="phiLink"  id="idMilitaryText" rendered="#{pc_Links.showIdMilitaryText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idOccupational" rendered="#{pc_Links.showIdOccupational}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_23" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionOccupational}" action="#{pc_Links.actionOccupational}">
									<h:outputText value="#{componentBundle.Occupational}" styleClass="phiLink"  id="idOccupationalText" ></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>						
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idOAA" rendered="#{pc_Links.showIdOlderAgeApplicant}"></h:outputText>
							</td>
							<td align="left" style="padding-bottom: 5px;">								
								<h:commandLink id="_15" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionOlderAgeApplicant}" rendered="#{pc_Links.show_15}">
									<h:outputText value="#{componentBundle.OlderAgeApplica}" styleClass="phiLink" style="width: 99px;" id="idOAAText" rendered="#{pc_Links.showIdOAAText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idPsych" rendered="#{pc_Links.showIdPsych}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_16" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionPsych}"  rendered="#{pc_Links.show_16}">
									<h:outputText value="#{componentBundle.Psych}" styleClass="phiLink"  id="idPsychText" rendered="#{pc_Links.showIdPsychText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idRacing" rendered="#{pc_Links.showIdRacing}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_17" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionRacing}" rendered="#{pc_Links.show_17}">
									<h:outputText value="#{componentBundle.Racing}" styleClass="phiLink"  id="idRacingText" rendered="#{pc_Links.showIdRacingText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idRespiratory" rendered="#{pc_Links.showIdRespiratory}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_18" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionRespiratory}" rendered="#{pc_Links.show_18}" >
									<h:outputText value="#{componentBundle.Respiratory}" styleClass="phiLink"  id="idRespiratoryText" rendered="#{pc_Links.showIdRespiratoryText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idSleepApnea" rendered="#{pc_Links.showIdSleepApnea}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_19" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionSleepApnea}" rendered="#{pc_Links.show_19}" >
									<h:outputText value="#{componentBundle.SleepApnea}" styleClass="phiLink"  id="idSleepApneaText" rendered="#{pc_Links.showIdSleepApneaText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idStoli" rendered="#{pc_Links.showIdSTOLI}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_20" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionSTOLI}" rendered="#{pc_Links.show_20}" >
									<h:outputText value="#{componentBundle.STOLI}" styleClass="phiLink"  id="idStoliText" rendered="#{pc_Links.showIdStoliText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idTobacco" rendered="#{pc_Links.showIdTobacco}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_21" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionTobacco}" rendered="#{pc_Links.show_21}" >
									<h:outputText value="#{componentBundle.Tobacco}" styleClass="phiLink"  id="idTobaccoText" rendered="#{pc_Links.showIdTobaccoText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idUnderwaterDiving" rendered="#{pc_Links.showIdUnderwaterDiving}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_22" onmousedown="submitQuestionnaire();" action="#{pc_Links.actionUnderwaterDiving}" rendered="#{pc_Links.show_22}" >
									<h:outputText value="#{componentBundle.UnderwaterDivin}" styleClass="phiLink"  id="idUnderwaterDivingText" rendered="#{pc_Links.showIdUnderwaterDivingText}"></h:outputText> <!-- NBA324 -->
								</h:commandLink>
							</td>
						</tr>
                       </table>
					</td>
				  </tr>					
				</table>
				<c:if test="${!pc_PHIStatus.phiBriefCase}">
				<f:subview id="nbaCommentBar">
						<c:import url="/nbaPHI/NbaCommentBar.jsp" />
				</f:subview>
				</c:if> 
				<h:panelGroup styleClass="tabButtonBar" >
								<h:commandButton value="Refresh" styleClass="tabButtonLeft" style="width: 71px" action="#{pc_Page1TeleInterview.refreshAction}" onclick="resetTargetFrame(this.form.id);" id="refreshid" rendered="#{pc_Page1TeleInterview.show_0}" disabled="#{pc_Page1TeleInterview.disable_0}"></h:commandButton>
								<h:commandButton value="Commit" styleClass="tabButtonRight"   id="commitid"  onclick="resetTargetFrame(this.form.id);submitContents();return false;" rendered="#{pc_Page1TeleInterview.show_1}" disabled="#{pc_Page1TeleInterview.disable_1||pc_Page1TeleInterview.notLocked||pc_Page1TeleInterview.auth.enablement['Commit']||pc_Page1TeleInterview.phiCompletedInd}"></h:commandButton>						
				</h:panelGroup>
				<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="setTargetFrame('page1TeleInterview');" action="#{pc_Page1TeleInterview.actionBackToStatus}"></h:commandButton>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
