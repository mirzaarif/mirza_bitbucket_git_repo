<!-- CHANGE LOG -->
<!-- Audit Number   	Version   Change Description -->
<!-- FNB004         	NB-1101	PHI-->
<!-- FNB004 - VEPL1206  NB-1101	Look into why "java.lang.IllegalArgumentException" is appearing onbottom of interview screen.-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 2 Employment</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('phiinterview');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			function toggleCBGroup(formName, prefix, id) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					   alert(inputs[i].name);					  
						if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
							inputs[i].checked = false ;
						}
					}   
				}   
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE2EMPLOYMENT" value="#{pc_Page2Employment}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_PAGINGBOTTOM" value="#{pc_PagingBottom}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<div id="Messages" style="display: none"><h:messages></h:messages></div>  <!-- FNB004 - VEPL1206  -->
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
			    <h:inputHidden id="scrollx" value="#{pc_Page2Employment.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page2Employment.scrollYC}"></h:inputHidden>			   
			    <div class="inputForm">	
			      			
					      	<h:outputLabel value="#{componentBundle.EmploymentInfor}" style="width: 508px" styleClass="sectionSubheader" id="employmentinfo" rendered="#{pc_Page2Employment.showEmploymentinformation}"> <!-- NBA324 -->
					      	</h:outputLabel>
				    <h:panelGroup>
			                <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			         </h:panelGroup>
			           
			             
				          <h:panelGroup id="employmentinfoid" rendered="#{pc_Page2Employment.showEmploymentinfoid}">
					           <h:outputLabel value="#{componentBundle.Howlonghaveyoub}" style="width: 450px;text-align:left;margin-left=5px;" styleClass="formLabel" id="employeeownership" rendered="#{pc_Page2Employment.showEmployeeownership}"></h:outputLabel>
				          </h:panelGroup>				         
				          
				          <h:panelGroup id="employmeeownershipidinfo" rendered="#{pc_Page2Employment.showEmploymeeownershipidinfo}" style="padding-top: 20px;">
				                <h:outputLabel value="" styleClass="formLabel" style="width: 345px;" ></h:outputLabel>
				                <h:commandButton id="page2ownershipdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasOwnerShipDetails}" 
								onclick="launchDetailsPopUp('page2ownershipdetails');" style="margin-right:10px;margin-left:35px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>		 
				          </h:panelGroup>
				           <Help:Link contextId="M5_H_LengthEmployment" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   			<Help:Link contextId="M5_W_LengthEmployment" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				          <h:panelGroup>
			       				<h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 15px;" ></h:outputLabel>
			      
			      </h:panelGroup>
				    <h:panelGroup id="otherjobsid" rendered="#{pc_Page2Employment.showOtherjobsid}">
					<h:outputLabel style="width: 450px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveanyoth}" styleClass="formLabel" id="otherjobs" rendered="#{pc_Page2Employment.showOtherjobs}"></h:outputLabel>					
					<h:outputLabel value="#{componentBundle.Givedetailsincl}" style="width: 450px;text-align:left;margin-left:15px;" styleClass="formLabel" id="details" rendered="#{pc_Page2Employment.showDetails}"></h:outputLabel>
				</h:panelGroup>
				
				<h:panelGroup id="otherjobsinfo" rendered="#{pc_Page2Employment.showOtherjobsinfo}">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 268px;" ></h:outputLabel>
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixNo}" id="checkbox1No" onclick="toggleCBGroup(this.form.id, 'checkbox1',  'checkbox1No')" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixYes}" id="checkbox1Yes" onclick="toggleCBGroup(this.form.id, 'checkbox1',  'checkbox1Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page2volunteerdetails')};" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
				    <h:commandButton id="page2volunteerdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasVolunteerDetails}"
								onclick="launchDetailsPopUp('page2volunteerdetails');" style="margin-right:10px;margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>		 
			   
				</h:panelGroup>
				  <Help:Link contextId="M6_H_OtherJobs" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M6_W_OtherJobs" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
				
				<h:panelGroup id="bysinessentityid" styleClass="phispacer" rendered="#{pc_Page2Employment.showBysinessentityid}">
					<h:outputLabel style="width: 195px;text-align:left;left;margin-left:5px;" value="#{componentBundle.BusinessEntityT}" styleClass="formLabel" id="businesstype" rendered="#{pc_Page2Employment.showBusinesstype}"></h:outputLabel>
					<h:selectOneMenu styleClass="textInput" style="width: 160px" value="#{pc_Page2Employment.businessEntityType}" id="businessEntityType" rendered="#{pc_Page2Employment.showBusinessEntityType}" disabled="#{pc_Page2Employment.disableEmploymentInfo||pc_Page2Employment.disableM7AToM10}">
					<f:selectItems value="#{pc_Page2Employment.statecodeList}"></f:selectItems>
					</h:selectOneMenu>
					 <h:commandButton id="page2businessentitydetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasBusinessEntityDetails}" 
								onclick="launchDetailsPopUp('page2businessentitydetails');" style="margin-right:10px;margin-left:20px;" disabled="#{pc_Page2Employment.disableEmploymentInfo||pc_Page2Employment.disableM7AToM10}"/>		
				</h:panelGroup>	
				 <Help:Link contextId="M7A_H_TypeOfBusiness" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M7A_W_TypeOfBusiness" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>					
					
					
				<h:panelGroup id="netprofitlossid"   rendered="#{pc_Page2Employment.showNetprofitlossid}">
					<h:outputLabel value="" styleClass="formLabel" style="width: 450px;" ></h:outputLabel>
					<h:outputLabel style="width: 450px;text-align:left;padding-bottom:10px; margin-left:5px;" value="As of the business Year end did the business show a net profit or loss? What was the approximate figure?Loss - reason ? If Loss, has the business historically shown a profit?" styleClass="formLabel" id="netprofitloss" rendered="#{pc_Page2Employment.showNetprofitloss}"></h:outputLabel>
				</h:panelGroup>
				
				 <h:panelGroup id="profitlossinfo" rendered="#{pc_Page2Employment.showProfitlossinfo}" >					                 			                 
				                 <h:outputLabel value="" styleClass="formLabel" style="width: 373px;" ></h:outputLabel>
				                <h:commandButton id="page2profitlossdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasProfitLossDetails}" 
								onclick="launchDetailsPopUp('page2profitlossdetails');" style="margin-right:10px;margin-left:7px;" disabled="#{pc_Page2Employment.disableEmploymentInfo||pc_Page2Employment.disableM7AToM10}"/>		 
			   </h:panelGroup>
				 <Help:Link contextId="M8_H_ProfitLoss" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M8_W_ProfitLoss" location="PHI" imageName="images/why.gif" tooltipText="Why"/>          			
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>	
					
					          
				<h:panelGroup id="businessvalueid" rendered="#{pc_Page2Employment.showBusinessvalueid}">
					<h:outputLabel style="width: 373px;text-align:left;padding-bottom:10px;margin-left:5px;" value="What is the Market value of the business?" styleClass="formLabel" id="businessvalue" rendered="#{pc_Page2Employment.showBusinessvalue}"></h:outputLabel>
					<h:commandButton id="page2marketvaluedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasMarketValueDetails}" 
								onclick="launchDetailsPopUp('page2marketvaluedetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo||pc_Page2Employment.disableM7AToM10}"/>	
				</h:panelGroup>
				<Help:Link contextId="M9_H_MarketValue" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M9_W_MarketValue" location="PHI" imageName="images/why.gif" tooltipText="Why"/>   
				 
				
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
			     
				<h:panelGroup id="annualearnedincomeid" rendered="#{pc_Page2Employment.showAnnualearnedincomeid}">
					<h:outputLabel style="width: 373px;text-align:left;padding-bottom:10px;margin-left:5px;" value="#{componentBundle.WhatisyourAnnua}" styleClass="formLabel" id="annualearnedincome" rendered="#{pc_Page2Employment.showAnnualearnedincome}"></h:outputLabel>
					<h:commandButton id="page2annualincomedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasAnnualIncomeDetails}" 
								onclick="launchDetailsPopUp('page2annualincomedetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo||pc_Page2Employment.disableM7AToM10}"/>	
					
					</h:panelGroup>
					<Help:Link contextId="M10_H_AnnualIncome" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M10_W_AnnualIncome" location="PHI" imageName="images/why.gif" tooltipText="Why"/> 
					
				<h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 25px;" ></h:outputLabel>
			     </h:panelGroup>
			     
				<h:panelGroup id="receivedamountforannualprofitlossid" rendered="#{pc_Page2Employment.showReceivedamountforannualprofitlossid}">
					<h:outputLabel style="width: 450px;text-align:left;padding-bottom:10px;margin-left:5px;" value="#{componentBundle.Whatamountdidyo}" styleClass="formLabel" id="receivedamountforannualprofitloss" rendered="#{pc_Page2Employment.showReceivedamountforannualprofitloss}"></h:outputLabel>
				</h:panelGroup>
				
				<h:panelGroup id="amountimages">
				  <h:outputLabel value="" styleClass="formLabel" style="width: 380px;" ></h:outputLabel>
					<h:commandButton id="page2amountdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasAmountDetails}" 
								onclick="launchDetailsPopUp('page2amountdetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>
					
				</h:panelGroup>
				<Help:Link contextId="M11_H_Bonus" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M11_W_Bonus" location="PHI" imageName="images/why.gif" tooltipText="Why"/> 
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
			     
				<h:panelGroup id="othersourceincomeid" rendered="#{pc_Page2Employment.showOthersourceincomeid}">
					<h:outputLabel style="width: 450px;text-align:left;padding-bottom:10px; margin-left:5px;" styleClass="formLabel" id="othersourceincome" value="#{componentBundle.Doyouhaveanyoth_0}" rendered="#{pc_Page2Employment.showOthersourceincome}"></h:outputLabel>
				</h:panelGroup>			
				
				<h:panelGroup id="othersourceincomeinfo" rendered="#{pc_Page2Employment.showOthersourceincomeinfo}">
				 <h:outputLabel value="" styleClass="formLabel" style="width: 269px;" ></h:outputLabel>
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixNo_1}" id="checkbox2No" onclick="toggleCBGroup(this.form.id, 'checkbox2',  'checkbox2No')" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixYes_1}" id="checkbox2Yes" onclick="toggleCBGroup(this.form.id, 'checkbox2',  'checkbox2Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page2otherincomedetails')};" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
					<h:commandButton id="page2otherincomedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasOtherIncomeDetails}" 
								onclick="launchDetailsPopUp('page2otherincomedetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>
					
				</h:panelGroup>
				<Help:Link contextId="M12_H_AdditionalSource" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M12_W_AdditionalSource" location="PHI" imageName="images/why.gif" tooltipText="Why"/> 
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
				
				<h:panelGroup id="rentalincomeid" rendered="#{pc_Page2Employment.showRentalincomeid}">
					<h:outputLabel style="width: 400px;text-align:left;padding-bottom:10px;margin-left:15px;" value="#{componentBundle.RentalincomeCla}" styleClass="formLabel" id="rentalincome" rendered="#{pc_Page2Employment.showRentalincome}"></h:outputLabel>
				</h:panelGroup>
				<h:panelGroup id="commercialincomeid" rendered="#{pc_Page2Employment.showCommercialincomeid}">
					<h:outputLabel style="width: 400px;text-align:left;padding-bottom:10px;margin-left:15px;" value="#{componentBundle.Ifcommercialinc}" styleClass="formLabel" id="commercialincome" rendered="#{pc_Page2Employment.showCommercialincome}"></h:outputLabel>
				</h:panelGroup>
			     
				<h:panelGroup id="sourceid" rendered="#{pc_Page2Employment.showSourceid}">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 323px;" ></h:outputLabel>
					<h:outputLabel value="#{componentBundle.Source}" styleClass="formLabel" style="width: 50px; " id="source" rendered="#{pc_Page2Employment.showSource}"></h:outputLabel>
					<h:commandButton id="page2sourcedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasSourceDetails}" onclick="launchDetailsPopUp('page2sourcedetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>					
				</h:panelGroup>
				
				 <h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
			     
				<h:panelGroup id="amountid" rendered="#{pc_Page2Employment.showAmountid}">
			        <h:outputLabel value="" styleClass="formLabel" style="width: 323px;" ></h:outputLabel>
					<h:outputLabel value="#{componentBundle.Amount}" styleClass="formLabel" style="width: 50px;" dir="ltr" id="amount" rendered="#{pc_Page2Employment.showAmount}"></h:outputLabel>
					<h:commandButton id="page2amount2details" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasAmount2Details}" 
								onclick="launchDetailsPopUp('page2amount2details');" style="margin-right:8px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>
				</h:panelGroup>
				
				<h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
			     
				<h:panelGroup id="newworthpersonalid" rendered="#{pc_Page2Employment.showNewworthpersonalid}">
					<h:outputLabel style="width: 450px;text-align:left;padding-bottom:10px;margin-left:5px;" value="#{componentBundle.Whatisyourappro}" styleClass="formLabel" id="newworthpersonal" rendered="#{pc_Page2Employment.showNewworthpersonal}"></h:outputLabel>				
				</h:panelGroup>
				
				<h:panelGroup id="newworthpersonalimgid" rendered="#{pc_Page2Employment.showNewworthpersonalid}">
				     <h:outputLabel value="" styleClass="formLabel" style="width: 379px;" ></h:outputLabel>
				    <h:commandButton id="page2networthdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasNetWorthDetails}" 
								onclick="launchDetailsPopUp('page2networthdetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>				
					
				</h:panelGroup>
				<Help:Link contextId="M13_H_NetWorth" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="M13_W_NetWorth" location="PHI" imageName="images/why.gif" tooltipText="Why"/> 
				<h:panelGroup>
			        <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 19px;" ></h:outputLabel>
			     </h:panelGroup>
				
				<h:panelGroup id="bankruptcyid" rendered="#{pc_Page2Employment.showBankruptcyid}">
					<h:outputLabel style="width: 360px;text-align:left;padding-bottom:10px;margin-left:5px;" value="#{componentBundle.Haveyoueverfile}" styleClass="formLabel" id="bankruptcy" rendered="#{pc_Page2Employment.showBankruptcy}"></h:outputLabel>
					<h:outputLabel style="width: 360px;text-align:left;padding-bottom:10px;margin-left:15px;" value="#{componentBundle.Ifyeswattypeandwhen}" styleClass="formLabel" id="bankruptcydetails" rendered="#{pc_Page2Employment.showBankruptcy}"></h:outputLabel>
				</h:panelGroup>
				
				<h:panelGroup id="bankruptcyinfo" rendered="#{pc_Page2Employment.showBankruptcyinfo}">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 268px" ></h:outputLabel>	
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixNo_2}" id="checkbox3No" onclick="toggleCBGroup(this.form.id, 'checkbox3',  'checkbox3No')" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
					<h:selectBooleanCheckbox value="#{pc_Page2Employment.prefixYes_2}" id="checkbox3Yes" onclick="toggleCBGroup(this.form.id, 'checkbox3',  'checkbox3Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page2bankruptcydetails')};" disabled="#{pc_Page2Employment.disableEmploymentInfo}"></h:selectBooleanCheckbox>
					<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
					<h:commandButton id="page2bankruptcydetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page2Employment.hasBankRuptcyDetails}"  
								onclick="launchDetailsPopUp('page2bankruptcydetails');" style="margin-right:10px;" disabled="#{pc_Page2Employment.disableEmploymentInfo}"/>		
				</h:panelGroup>
				   <Help:Link contextId="M14_H_Bankruptcy" location="PHI" imageName="images/help.gif" tooltipText="Help"/>				 
			 <f:subview id="tabHeader">
				  <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			 </f:subview>
					
		</div>
		<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page2Employment.commitAction}"></h:commandButton>
		</h:form>
	 </f:view>
		
</body>
</html>
