<!-- CHANGE LOG -->
<!-- Audit Number   	Version   Change Description -->
<!-- FNB004         	NB-1101	PHI-->
<!-- FNB004 - VEPL1206  NB-1101	Look into why "java.lang.IllegalArgumentException" is appearing onbottom of interview screen.-->
<!-- FNB004 - VEPL1387   NB-1101   PHI page 5 of Interview - answers are deselecting -->
<!-- NBA324         	NB-1301		nbAFull Personal History Interview -->
<!-- SPRNBA-798         NB-1401   Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 5 Health Quest</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame() {
				document.forms['phiinterview'].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('phiinterview');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}			
			function toggleCBGroup(formName, id,prefix,indicator) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					  					  
						if(getElementName(inputs[i].name) == prefix){ <!--FNB004 - VEPL1387-->
							if(inputs[i].checked==true && indicator==true){
									inputs[i].checked = false ;
									form.submit();
								}else{
									inputs[i].checked = false ;
								}
						}
					}   
				}   
			}	
			function valueChangeEvent() {	
			    getScrollXY('phiinterview');
			}
			<!--New Function FNB004 - VEPL1387-->
			function getElementName(str) { 
			   if(str.match("Yes$")=="Yes"){
			        return str.substr(0,str.length-3);
			    }
			   else if(str.match("No$")=="No"){
			        return str.substr(0,str.length-2);
			    }
			}
			//-->

		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE5HEALTHQUEST" value="#{pc_Page5HealthQuest}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<div id="Messages" style="display: none"><h:messages></h:messages></div>   <!-- FNB004 - VEPL1206  -->
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				<h:inputHidden id="scrollx" value="#{pc_Page5HealthQuest.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page5HealthQuest.scrollYC}"></h:inputHidden>
				<div class="inputForm">
                         
                             	<h:outputLabel value="#{componentBundle.HealthQuestiona}" style="width:508px" styleClass="sectionSubheader"> <!-- NBA324 -->
                             	</h:outputLabel>
                              <h:panelGroup id="natureofillness">
				          <h:outputLabel style="width: 460px;text-align:left;margin-left:5px;" value="#{componentBundle.Foryesanswersin}" styleClass="formLabel"></h:outputLabel>
				          <h:outputLabel style="width: 460px;text-align:left;margin-left:5px;" value="#{componentBundle.treatmentresult}" styleClass="formLabel"></h:outputLabel>
					    <h:outputLabel style="width: 460px;text-align:left;margin-left:5px;" value="#{componentBundle.involved}" styleClass="formLabel"></h:outputLabel>
				     </h:panelGroup> 
                              <h:panelGroup id="ekgtest">
                                 <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 15px;" ></h:outputLabel>
				         <h:outputLabel style="width: 460px;text-align:left;margin-left:5px;" value="#{componentBundle.Withinthelast5y}" styleClass="formLabel"></h:outputLabel>
				         <h:outputLabel styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;" value="#{componentBundle.Hadanydiagnoses}"></h:outputLabel>
                                 <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox1No}" onclick="toggleCBGroup(this.form.id, 'checkbox1No',  'checkbox1',true);valueChangeEvent();" id="checkbox1No"></h:selectBooleanCheckbox>
					   <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox1"></h:outputLabel>
					   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox1Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox1Yes',  'checkbox1',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5EKGDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_Page5HealthQuest.collapseDiagnosticTestInd}" id="checkbox1Yes"></h:selectBooleanCheckbox>
					   <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox1_0"></h:outputLabel>
                                 <h:commandButton id="page5EKGDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasEkgDetails}"  
								onclick="launchDetailsPopUp('page5EKGDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P1A_H_DiagnosticTests" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                            <h:panelGroup id="ekgtestconditions" rendered="#{pc_Page5HealthQuest.expandDiagnosticTestInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.WhattypeoftestF}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel>
                                     <h:commandButton id="page5Symptoms" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSymptoms}"  
								onclick="launchDetailsPopUp('page5Symptoms');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
				   		
                             <h:panelGroup id="mobiletest" rendered="#{pc_Page5HealthQuest.expandDiagnosticTestInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Mobileunitforte}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel>
                                     <h:commandButton id="page5MobileUnit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasMobileUnit}"  
								onclick="launchDetailsPopUp('page5MobileUnit');" style="margin-right:10px;margin-left:40px;"/>                                 

                             </h:panelGroup> 
                              <h:panelGroup id="testoffered" rendered="#{pc_Page5HealthQuest.expandDiagnosticTestInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wasthetestoffer}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>

                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox2No}" onclick="toggleCBGroup(this.form.id, 'checkbox2No',  'checkbox2');" id="checkbox2No"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox2"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox2Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox2Yes',  'checkbox2');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5TestOfferedToCommumity')};" id="checkbox2Yes"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox2_0"></h:outputLabel>

                                     <h:commandButton id="page5TestOfferedToCommumity" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasTestOfferedtoCommunity}"  
								onclick="launchDetailsPopUp('page5TestOfferedToCommumity');" style="margin-right:10px;margin-left:40px;"/>
                                                                      

                             </h:panelGroup> 
                              <h:panelGroup id="notdonesurgery">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Beenadvisedtoha}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>


                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox3No}" onclick="toggleCBGroup(this.form.id, 'checkbox3No',  'checkbox3');" id="checkbox3No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox3"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox3Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox3Yes',  'checkbox3');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5NotDoneSurgery')};" id="checkbox3Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox3_0"></h:outputLabel>

                                      
                                     <h:commandButton id="page5NotDoneSurgery" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasNotDoneSurgery}"  
								onclick="launchDetailsPopUp('page5NotDoneSurgery');" style="margin-right:10px;margin-left:40px;"/>                                 
                             </h:panelGroup>
                             <Help:Link contextId="P1B_H_Hospitalization" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                             <h:panelGroup id="seizure">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel style="width: 392px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouinthepas}" styleClass="formLabel" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Epilepsyseizure}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;" ></h:outputLabel>



                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox4No}" onclick="toggleCBGroup(this.form.id, 'checkbox4No',  'checkbox4',true);valueChangeEvent();" id="checkbox4No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox4"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox4Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox4Yes',  'checkbox4',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureDetails')};valueChangeEvent();submit();" id="checkbox4Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseSeizureInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox4_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5SeizureDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureDetails}"  
								onclick="launchDetailsPopUp('page5SeizureDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P2A_H_Seizures" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                              <h:panelGroup id="seizuredetails" rendered="#{pc_Page5HealthQuest.expandSeizureInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox5No}" onclick="toggleCBGroup(this.form.id, 'checkbox5No',  'checkbox5');" id="checkbox5No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox5"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox5Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox5Yes',  'checkbox5');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureSymptomsDetails')};" id="checkbox5Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox5_0"></h:outputLabel>
                                      
                                     <h:commandButton id="page5SeizureSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5SeizureSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                     <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox6No}" onclick="toggleCBGroup(this.form.id, 'checkbox6No',  'checkbox6');" id="checkbox6No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox6"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox6Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox6Yes',  'checkbox6');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureTreatment')};" id="checkbox6Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox6_0"></h:outputLabel>
                                    <h:commandButton id="page5SeizureTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureTreatment}"  
								onclick="launchDetailsPopUp('page5SeizureTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5SeizureDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureDoctor}"   
								onclick="launchDetailsPopUp('page5SeizureDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox7No}" onclick="toggleCBGroup(this.form.id, 'checkbox7No',  'checkbox7');" id="checkbox7No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox7"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox7Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox7Yes',  'checkbox7');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureInjuryDetails')};" id="checkbox7Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox7_0"></h:outputLabel>
                                   <h:commandButton id="page5SeizureInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureInjuryDetails}"  
								onclick="launchDetailsPopUp('page5SeizureInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" id="seizurelimb"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox8No}" onclick="toggleCBGroup(this.form.id, 'checkbox8No',  'checkbox8');" id="checkbox8No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox8"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox8Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox8Yes',  'checkbox8');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureLimbDetails')};" id="checkbox8Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox8_0"></h:outputLabel>
                                   <h:commandButton id="page5SeizureLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureLimbDetails}"  
								onclick="launchDetailsPopUp('page5SeizureLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox9No}" onclick="toggleCBGroup(this.form.id, 'checkbox9No',  'checkbox9');" id="checkbox9No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox9"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox9Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox9Yes',  'checkbox9');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5SeizureFollowUpVisit')};" id="checkbox9Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox9_0"></h:outputLabel>
                                  <h:commandButton id="page5SeizureFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasSeizureFollowupVisit}"  
								onclick="launchDetailsPopUp('page5SeizureFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup> 
                             <h:panelGroup id="stress">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Anxietydepressi}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>



                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox10No}" onclick="toggleCBGroup(this.form.id, 'checkbox10No',  'checkbox10',true);valueChangeEvent();" id="checkbox10No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox10"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox10Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox10Yes',  'checkbox10',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressDetails')};valueChangeEvent();submit();" id="checkbox10Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseStressInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox10_0"></h:outputLabel>



                                      
                                     <h:commandButton id="page5StressDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressDetails}"   
								onclick="launchDetailsPopUp('page5StressDetails');" style="margin-right:10px;margin-left:40px;"/>                        
                             </h:panelGroup> 
                             <Help:Link contextId="P2B_H_Anxiety" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                              <h:panelGroup id="stressdetails" rendered="#{pc_Page5HealthQuest.expandStressInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox11No}" onclick="toggleCBGroup(this.form.id, 'checkbox11No',  'checkbox11');" id="checkbox11No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox11"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox11Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox11Yes',  'checkbox11');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressSymptomsDetails')};" id="checkbox11Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox11_0"></h:outputLabel>

                                      
                                     <h:commandButton id="page5StressSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressSymptomsDetails}"   
								onclick="launchDetailsPopUp('page5StressSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox12No}" onclick="toggleCBGroup(this.form.id, 'checkbox12No',  'checkbox12');" id="checkbox12No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox12"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox12Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox12Yes',  'checkbox12');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressTreatment')};" id="checkbox12Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox12_0"></h:outputLabel>

                                    <h:commandButton id="page5StressTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressTreatment}"  
								onclick="launchDetailsPopUp('page5StressTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5StressDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressDoctor}"   
								onclick="launchDetailsPopUp('page5StressDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox13No}" onclick="toggleCBGroup(this.form.id, 'checkbox13No',  'checkbox13');" id="checkbox13No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox13"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox13Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox13Yes',  'checkbox13');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressInjuryDetails')};" id="checkbox13Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox13_0"></h:outputLabel>

                                   <h:commandButton id="page5StressInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressInjuryDetails}"  
								onclick="launchDetailsPopUp('page5StressInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox14No}" onclick="toggleCBGroup(this.form.id, 'checkbox14No',  'checkbox14');" id="checkbox14No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox14"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox14Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox14Yes',  'checkbox14');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressLimbDetails')};" id="checkbox14Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox14_0"></h:outputLabel>

                                   <h:commandButton id="page5StressLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStresslimbDetails}"  
								onclick="launchDetailsPopUp('page5StressLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox15No}" onclick="toggleCBGroup(this.form.id, 'checkbox15No',  'checkbox15');" id="checkbox15No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox15"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox15Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox15Yes',  'checkbox15');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5StressFollowUpVisit')};" id="checkbox15Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox15_0"></h:outputLabel>

                                  <h:commandButton id="page5StressFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasStressFollowupVisit}"  
								onclick="launchDetailsPopUp('page5StressFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup> 
                               <h:panelGroup id="cancer">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Cancerdiabetesg}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:15px;" ></h:outputLabel>



                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox16No}" onclick="toggleCBGroup(this.form.id, 'checkbox16No',  'checkbox16',true);valueChangeEvent();" id="checkbox16No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox16"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox16Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox16Yes',  'checkbox16',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerDetails')};valueChangeEvent();submit();" id="checkbox16Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseCancerInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox16_0"></h:outputLabel>




                                      
                                     <h:commandButton id="page5CancerDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerDetails}"   
								onclick="launchDetailsPopUp('page5CancerDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P2C_H_CancerDiabetesGlands" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2C_W_CancerDiabetesGlands" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="cancerdetails" rendered="#{pc_Page5HealthQuest.expandCancerInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox17No}" onclick="toggleCBGroup(this.form.id, 'checkbox17No',  'checkbox17');" id="checkbox17No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox17"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox17Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox17Yes',  'checkbox17');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerSymptomsDetails')};" id="checkbox17Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox17_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5CancerSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5CancerSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox18No}" onclick="toggleCBGroup(this.form.id, 'checkbox18No',  'checkbox18');" id="checkbox18No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox18"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox18Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox18Yes',  'checkbox18');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerTreatment')};" id="checkbox18Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox18_0"></h:outputLabel>


                                    <h:commandButton id="page5CancerTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerTreatment}"   
								onclick="launchDetailsPopUp('page5CancerTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5CancerDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerDoctor}"  
								onclick="launchDetailsPopUp('page5CancerDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox19No}" onclick="toggleCBGroup(this.form.id, 'checkbox19No',  'checkbox19');" id="checkbox19No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox19"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox19Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox19Yes',  'checkbox19');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerInjuryDetails')};" id="checkbox19Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox19_0"></h:outputLabel>


                                   <h:commandButton id="page5CancerInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerInjuryDetails}"  
								onclick="launchDetailsPopUp('page5CancerInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox20No}" onclick="toggleCBGroup(this.form.id, 'checkbox20No',  'checkbox20');" id="checkbox20No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox20"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox20Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox20Yes',  'checkbox20');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerLimbDetails')};" id="checkbox20Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox20_0"></h:outputLabel>
	

                                   <h:commandButton id="page5CancerLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerLimbDetails}"  
								onclick="launchDetailsPopUp('page5CancerLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox21No}" onclick="toggleCBGroup(this.form.id, 'checkbox21No',  'checkbox21');" id="checkbox21No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox21"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox21Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox21Yes',  'checkbox21');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CancerFollowUpVisit')};" id="checkbox21Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox21_0"></h:outputLabel>


                                  <h:commandButton id="page5CancerFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCancerFollowupVisit}"   
								onclick="launchDetailsPopUp('page5CancerFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup>
                             <h:panelGroup id="chestpain">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Chestpainhighbl}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>




                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox22No}" onclick="toggleCBGroup(this.form.id, 'checkbox22No',  'checkbox22',true);valueChangeEvent();" id="checkbox22No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox22"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox22Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox22Yes',  'checkbox22',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPDetails')};valueChangeEvent();submit();" id="checkbox22Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseChestPainInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox22_0"></h:outputLabel>



                                      
                                     <h:commandButton id="page5CPDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpDetails}"  
								onclick="launchDetailsPopUp('page5CPDetails');" style="margin-right:10px;margin-left:40px;"/>                       
                             </h:panelGroup> 
                             <Help:Link contextId="P2D_H_Heart" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2D_W_Heart" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="HealthQest" rendered="#{pc_Page5HealthQuest.expandChestPainInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox23No}" onclick="toggleCBGroup(this.form.id, 'checkbox23No',  'checkbox23');" id="checkbox23No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox23"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox23Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox23Yes',  'checkbox23');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPSymptomsDetails')};" id="checkbox23Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox23_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5CPSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5CPSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox24No}" onclick="toggleCBGroup(this.form.id, 'checkbox24No',  'checkbox24');" id="checkbox24No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox24"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox24Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox24Yes',  'checkbox24');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPTreatment')};" id="checkbox24Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox24_0"></h:outputLabel>


                                    <h:commandButton id="page5CPTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpTreatment}"   
								onclick="launchDetailsPopUp('page5CPTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5CPDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpDoctor}"   
								onclick="launchDetailsPopUp('page5CPDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox25No}" onclick="toggleCBGroup(this.form.id, 'checkbox25No',  'checkbox25');" id="checkbox25No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox25"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox25Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox25Yes',  'checkbox25');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPInjuryDetails')};" id="checkbox25Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox25_0"></h:outputLabel>


                                   <h:commandButton id="page5CPInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpInjuryDetails}"   
								onclick="launchDetailsPopUp('page5CPInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox26No}" onclick="toggleCBGroup(this.form.id, 'checkbox26No',  'checkbox26');" id="checkbox26No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox26"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox26Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox26Yes',  'checkbox26');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPLimbDetails')};" id="checkbox26Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox26_0"></h:outputLabel>
	

                                   <h:commandButton id="page5CPLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpLimbDetails}"  
								onclick="launchDetailsPopUp('page5CPLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox27No}" onclick="toggleCBGroup(this.form.id, 'checkbox27No',  'checkbox27');" id="checkbox27No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox27"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox27Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox27Yes',  'checkbox27');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5CPFollowUpVisit')};" id="checkbox27Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox27_0"></h:outputLabel>



                                  <h:commandButton id="page5CPFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasCpFollowupVisit}"  
								onclick="launchDetailsPopUp('page5CPFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup> 
                              <h:panelGroup id="kidneydisorder">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Kidneyurinaryor}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>




                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox28No}" onclick="toggleCBGroup(this.form.id, 'checkbox28No',  'checkbox28',true);valueChangeEvent();" id="checkbox28No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox28"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox28Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox28Yes',  'checkbox28',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDDetails')};valueChangeEvent();submit();" id="checkbox28Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseKidneyDisorderInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox28_0"></h:outputLabel>



                                      
                                     <h:commandButton id="page5KDDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdDetails}"  
								onclick="launchDetailsPopUp('page5KDDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P2E_H_Kidney" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                              <h:panelGroup id="kidneydisorderdetails" rendered="#{pc_Page5HealthQuest.expandKidneyDisorderInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox29No}" onclick="toggleCBGroup(this.form.id, 'checkbox29No',  'checkbox29');" id="checkbox29No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox29"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox29Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox29Yes',  'checkbox29');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDSymptomsDetails')};" id="checkbox29Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox29_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5KDSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5KDSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox30No}" onclick="toggleCBGroup(this.form.id, 'checkbox30No',  'checkbox30');" id="checkbox30No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox30"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox30Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox30Yes',  'checkbox30');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDTreatment')};" id="checkbox30Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox30_0"></h:outputLabel>



                                    <h:commandButton id="page5KDTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdTreatment}"  
								onclick="launchDetailsPopUp('page5KDTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5KDDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdDoctor}"  
								onclick="launchDetailsPopUp('page5KDDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox31No}" onclick="toggleCBGroup(this.form.id, 'checkbox31No',  'checkbox31');" id="checkbox31No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox31"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox31Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox31Yes',  'checkbox31');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDInjuryDetails')};" id="checkbox31Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox31_0"></h:outputLabel>


                                   <h:commandButton id="page5KDInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdInjuryDetails}" onclick="launchDetailsPopUp('page5KDInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;"></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;"></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox32No}" onclick="toggleCBGroup(this.form.id, 'checkbox32No',  'checkbox32');" id="checkbox32No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox32"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox32Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox32Yes',  'checkbox32');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDLimbDetails')};" id="checkbox32Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox32_0"></h:outputLabel>

	

                                   <h:commandButton id="page5KDLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdLimbDetails}"  
								onclick="launchDetailsPopUp('page5KDLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox33No}" onclick="toggleCBGroup(this.form.id, 'checkbox33No',  'checkbox33');" id="checkbox33No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox33"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox33Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox33Yes',  'checkbox33');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5KDFollowUpVisit')};" id="checkbox33Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox33_0"></h:outputLabel>


                                  <h:commandButton id="page5KDFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasKdFollowupVisit}"  
								onclick="launchDetailsPopUp('page5KDFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup>
                              <h:panelGroup id="liverdisorder">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Liverorgastroin}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>





                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                    <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox34No}" onclick="toggleCBGroup(this.form.id, 'checkbox34No',  'checkbox34',true);valueChangeEvent();" id="checkbox34No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox34"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox34Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox34Yes',  'checkbox34',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDDetails')};valueChangeEvent();submit();" id="checkbox34Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseLiverDisorderInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox34_0"></h:outputLabel>
	

                                      
                                     <h:commandButton id="page5LDDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdDetails}"  
								onclick="launchDetailsPopUp('page5LDDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P2F_H_Liver" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2F_W_Liver" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="liverdisorderdetails" rendered="#{pc_Page5HealthQuest.expandLiverDisorderInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox35No}" onclick="toggleCBGroup(this.form.id, 'checkbox35No',  'checkbox35');" id="checkbox35No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox35"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox35Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox35Yes',  'checkbox35');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDSymptomsDetails')};" id="checkbox35Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox35_0"></h:outputLabel>



                                      
                                     <h:commandButton id="page5LDSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5LDSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox36No}" onclick="toggleCBGroup(this.form.id, 'checkbox36No',  'checkbox36');" id="checkbox36No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox36"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox36Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox36Yes',  'checkbox36');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDTreatment')};" id="checkbox36Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox36_0"></h:outputLabel>


                                    <h:commandButton id="page5LDTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdTreatment}"   
								onclick="launchDetailsPopUp('page5LDTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5LDDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdDoctor}"   
								onclick="launchDetailsPopUp('page5LDDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox37No}" onclick="toggleCBGroup(this.form.id, 'checkbox37No',  'checkbox37');" id="checkbox37No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox37"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox37Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox37Yes',  'checkbox37');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDInjuryDetails')};" id="checkbox37Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox37_0"></h:outputLabel>


                                   <h:commandButton id="page5LDInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdInjuryDetails}"   
								onclick="launchDetailsPopUp('page5LDInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox38No}" onclick="toggleCBGroup(this.form.id, 'checkbox38No',  'checkbox38');" id="checkbox38No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox38"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox38Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox38Yes',  'checkbox38');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDLimbDetails')};" id="checkbox38Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox38_0"></h:outputLabel>

	

                                   <h:commandButton id="page5LDLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdLimbDetails}"  
								onclick="launchDetailsPopUp('page5LDLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                              <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox39No}" onclick="toggleCBGroup(this.form.id, 'checkbox39No',  'checkbox39');" id="checkbox39No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox39"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox39Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox39Yes',  'checkbox39');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5LDFollowUpVisit')};" id="checkbox39Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox39_0"></h:outputLabel>

                                  <h:commandButton id="page5LDFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasLdFollowupVisit}"   
								onclick="launchDetailsPopUp('page5LDFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup> 
                             
                              <h:panelGroup id="asthamadisorder">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Asthmasleepapna}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;" ></h:outputLabel>






                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox40No}" onclick="toggleCBGroup(this.form.id, 'checkbox40No',  'checkbox40',true);valueChangeEvent();" id="checkbox40No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox40"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox40Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox40Yes',  'checkbox40',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADDetails')};valueChangeEvent();submit();" id="checkbox40Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseAsthamaDisorderInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox40_0"></h:outputLabel>
	

                                      
                                     <h:commandButton id="page5ADDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdDetails}"  
								onclick="launchDetailsPopUp('page5ADDetails');" style="margin-right:10px;margin-left:40px;"/>                
                             </h:panelGroup> 
                             <Help:Link contextId="P2G_H_Asthma" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2G_W_Asthma" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="asthamadisorderdetails" rendered="#{pc_Page5HealthQuest.expandAsthamaDisorderInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox41No}" onclick="toggleCBGroup(this.form.id, 'checkbox41No',  'checkbox41');" id="checkbox41No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox41"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox41Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox41Yes',  'checkbox41');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADSymptomsDetails')};" id="checkbox41Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox41_0"></h:outputLabel>

                                      
                                     <h:commandButton id="page5ADSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5ADSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox42No}" onclick="toggleCBGroup(this.form.id, 'checkbox42No',  'checkbox42');" id="checkbox42No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox42"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox42Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox42Yes',  'checkbox42');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADTreatment')};" id="checkbox42Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox42_0"></h:outputLabel>



                                    <h:commandButton id="page5ADTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdTreatment}"  
								onclick="launchDetailsPopUp('page5ADTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5ADDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdDoctor}"  
								onclick="launchDetailsPopUp('page5ADDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox43No}" onclick="toggleCBGroup(this.form.id, 'checkbox43No',  'checkbox43');" id="checkbox43No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox43"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox43Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox43Yes',  'checkbox43');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADInjuryDetails')};" id="checkbox43Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox43_0"></h:outputLabel>



                                   <h:commandButton id="page5ADInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdInjuryDetails}"   
								onclick="launchDetailsPopUp('page5ADInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox44No}" onclick="toggleCBGroup(this.form.id, 'checkbox44No',  'checkbox44');" id="checkbox44No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox44"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox44Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox44Yes',  'checkbox44');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADLimbDetails')};" id="checkbox44Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox44_0"></h:outputLabel>
	
	

                                   <h:commandButton id="page5ADLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdLimbDetails}"   
								onclick="launchDetailsPopUp('page5ADLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox45No}" onclick="toggleCBGroup(this.form.id, 'checkbox45No',  'checkbox45');" id="checkbox45No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox45"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox45Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox45Yes',  'checkbox45');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ADFollowUpVisit')};" id="checkbox45Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox45_0"></h:outputLabel>


                                  <h:commandButton id="page5ADFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAdFollowupVisit}"  
								onclick="launchDetailsPopUp('page5ADFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup> 
                             <h:panelGroup id="paralysis">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Lossofvisionorh}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>







                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox46No}" onclick="toggleCBGroup(this.form.id, 'checkbox46No',  'checkbox46',true);valueChangeEvent();" id="checkbox46No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox46"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox46Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox46Yes',  'checkbox46',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisDetails')};valueChangeEvent();submit();" id="checkbox46Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseParalysisInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox46_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5ParalysisDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisDetails}"  
								onclick="launchDetailsPopUp('page5ParalysisDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup> 
                             <Help:Link contextId="P2H_H_VisionLoss" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2H_W_VisionLoss" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="paralysisdetails" rendered="#{pc_Page5HealthQuest.expandParalysisDisorderInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox47No}" onclick="toggleCBGroup(this.form.id, 'checkbox47No',  'checkbox47');" id="checkbox47No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox47"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox47Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox47Yes',  'checkbox47');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisSymptomsDetails')};" id="checkbox47Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox47_0"></h:outputLabel>


                                      
                                     <h:commandButton id="page5ParalysisSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisSymptomsDetails}"   
								onclick="launchDetailsPopUp('page5ParalysisSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox48No}" onclick="toggleCBGroup(this.form.id, 'checkbox48No',  'checkbox48');" id="checkbox48No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox48"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox48Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox48Yes',  'checkbox48');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisTreatment')};" id="checkbox48Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox48_0"></h:outputLabel>




                                    <h:commandButton id="page5ParalysisTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisTreatment}"  
								onclick="launchDetailsPopUp('page5ParalysisTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5ParalysisDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisDoctor}"  
								onclick="launchDetailsPopUp('page5ParalysisDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox49No}" onclick="toggleCBGroup(this.form.id, 'checkbox49No',  'checkbox49');" id="checkbox49No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox49"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox49Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox49Yes',  'checkbox49');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisInjuryDetails')};" id="checkbox49Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox49_0"></h:outputLabel>



                                   <h:commandButton id="page5ParalysisInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisInjuryDetails}"  
								onclick="launchDetailsPopUp('page5ParalysisInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox50No}" onclick="toggleCBGroup(this.form.id, 'checkbox50No',  'checkbox50');" id="checkbox50No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox50"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox50Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox50Yes',  'checkbox50');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisLimbDetails')};" id="checkbox50Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox50_0"></h:outputLabel>

	

                                   <h:commandButton id="page5ParalysisLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisLimbDetails}"  
								onclick="launchDetailsPopUp('page5ParalysisLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox51No}" onclick="toggleCBGroup(this.form.id, 'checkbox51No',  'checkbox51');" id="checkbox51No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox51"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox51Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox51Yes',  'checkbox51');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ParalysisFollowUpVisit')};" id="checkbox51Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox51_0"></h:outputLabel>


                                  <h:commandButton id="page5ParalysisFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasParalysisFollowupVisit}"  
								onclick="launchDetailsPopUp('page5ParalysisFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup>
                             <h:panelGroup id="arthritis">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Arthritisorback}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>








                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox52No}" onclick="toggleCBGroup(this.form.id, 'checkbox52No',  'checkbox52',true);valueChangeEvent();" id="checkbox52No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox52"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox52Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox52Yes',  'checkbox52',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisDetails')};valueChangeEvent();submit();" id="checkbox52Yes" valueChangeListener="#{pc_Page5HealthQuest.collapseArthritisInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox52_0"></h:outputLabel>
	
                                      
                                     <h:commandButton id="page5ArthritisDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisDetails}"  
								onclick="launchDetailsPopUp('page5ArthritisDetails');" style="margin-right:10px;margin-left:40px;"/>                                                  
                             </h:panelGroup> 
                             <Help:Link contextId="P2I_H_Arthritis" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2I_W_Arthritis" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                              <h:panelGroup id="arthritisdetails" rendered="#{pc_Page5HealthQuest.expandArthritisDisorderInd}">
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                	<h:outputLabel value="#{componentBundle.Wereyouexperien}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>

                                    
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox53No}" onclick="toggleCBGroup(this.form.id, 'checkbox53No',  'checkbox53');" id="checkbox53No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox53"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox53Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox53Yes',  'checkbox53');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisSymptomsDetails')};" id="checkbox53Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox53_0"></h:outputLabel>



                                      
                                     <h:commandButton id="page5ArthritisSymptomsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisSymptomsDetails}"  
								onclick="launchDetailsPopUp('page5ArthritisSymptomsDetails');" style="margin-right:10px;margin-left:40px;"/>
                                      
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                     <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox54No}" onclick="toggleCBGroup(this.form.id, 'checkbox54No',  'checkbox54');" id="checkbox54No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox54"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox54Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox54Yes',  'checkbox54');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisTreatment')};" id="checkbox54Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox54_0"></h:outputLabel>





                                    <h:commandButton id="page5ArthritisTreatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisTreatment}"   
								onclick="launchDetailsPopUp('page5ArthritisTreatment');" style="margin-right:10px;margin-left:40px;"/>
                                     <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>     
                                    <h:outputLabel value="#{componentBundle.Whatdoctorsawyo}" styleClass="formLabel" style="width: 450px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel> 
                                    <h:commandButton id="page5ArthritisDoctor" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisDoctor}"   
								onclick="launchDetailsPopUp('page5ArthritisDoctor');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox55No}" onclick="toggleCBGroup(this.form.id, 'checkbox55No',  'checkbox55');" id="checkbox55No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox55"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox55Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox55Yes',  'checkbox55');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisInjuryDetails')};" id="checkbox55Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox55_0"></h:outputLabel>


                                   <h:commandButton id="page5ArthritisInjuryDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisInjuryDetails}"  
								onclick="launchDetailsPopUp('page5ArthritisInjuryDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                    <h:outputLabel value="#{componentBundle.Wasalimbinvolve}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;"></h:outputLabel>
                                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel><h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox56No}" onclick="toggleCBGroup(this.form.id, 'checkbox56No',  'checkbox56');" id="checkbox56No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox56"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox56Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox56Yes',  'checkbox56');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisLimbDetails')};" id="checkbox56Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox56_0"></h:outputLabel>

	

                                   <h:commandButton id="page5ArthritisLimbDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisLimbDetails}"  
								onclick="launchDetailsPopUp('page5ArthritisLimbDetails');" style="margin-right:10px;margin-left:40px;"/>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 430px;text-align:left;margin-left:45px;" ></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox57No}" onclick="toggleCBGroup(this.form.id, 'checkbox57No',  'checkbox57');" id="checkbox57No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox57"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox57Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox57Yes',  'checkbox57');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5ArthritisFollowUpVisit')};" id="checkbox57Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox57_0"></h:outputLabel>
	


                                  <h:commandButton id="page5ArthritisFollowUpVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasArthritisFollowupVisit}"  
								onclick="launchDetailsPopUp('page5ArthritisFollowUpVisit');" style="margin-right:10px;margin-left:40px;"/> 

                                                                      

                             </h:panelGroup>
                             <h:panelGroup id="alcoholcounselling"> 
                               <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                 <h:outputLabel value="#{componentBundle.Counselingforal}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>

                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox58No}" onclick="toggleCBGroup(this.form.id, 'checkbox58No',  'checkbox58');" id="checkbox58No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox58"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox58Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox58Yes',  'checkbox58');if(document.getElementById(this.id).checked){launchDetailsPopUp('page5AlcoholCounselling')};" id="checkbox58Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox58_0"></h:outputLabel>


                                  <h:commandButton id="page5AlcoholCounselling" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasAlcoholCounselling}"   
								onclick="launchDetailsPopUp('page5AlcoholCounselling');" style="margin-right:10px;margin-left:40px;"/> 
                             </h:panelGroup> 
                             <Help:Link contextId="P2J_H_Drugs" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="P2J_W_Drugs" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                             <h:panelGroup id="pregnant"> 
                               <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                <h:outputLabel value="#{componentBundle.Ifapplicableaco}" styleClass="formLabel" style="width: 459px;text-align:left;margin-left:15px;"></h:outputLabel>


                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox59No}" onclick="toggleCBGroup(this.form.id, 'checkbox59No',  'checkbox59',true);valueChangeEvent();" id="checkbox59No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="checkbox59"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Page5HealthQuest.checkbox59Yes}" onclick="toggleCBGroup(this.form.id, 'checkbox59Yes',  'checkbox59',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page5PregnancyDetails')};valueChangeEvent();submit();" id="checkbox59Yes" valueChangeListener="#{pc_Page5HealthQuest.collapsePregnancyInd}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="checkbox59_0"></h:outputLabel>


                                  <h:commandButton id="page5PregnancyDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasPregnancyDetails}"  
								onclick="launchDetailsPopUp('page5PregnancyDetails');" style="margin-right:10px;margin-left:40px;"/> 

                             </h:panelGroup>
                             <h:panelGroup id="duedate" rendered="#{pc_Page5HealthQuest.expandPregnancyInd}"> 
                               <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                <h:outputLabel value="#{componentBundle.Ifcurrentlypreg}" styleClass="formLabel" style="width: 456px;text-align:left;margin-left:30px;"></h:outputLabel>



                                  <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                 <h:inputText styleClass="formEntryTextHalf" style="margin-right:10px; width: 103px" value="#{pc_Page5HealthQuest.duedatetext}" id="duedateid">
                                      <f:convertDateTime pattern="#{bundle.datePattern}" />
 
                                </h:inputText>

                     
                                  <Help:Link contextId="P2K_H_Pregnancy" location="PHI" imageName="images/help.gif" tooltipText="Help"/>	
                                  <h:commandButton id="page5DueDateDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasDueDateDetails}"    
								onclick="launchDetailsPopUp('page5DueDateDetails');" style="margin-right:10px;margin-left:40px;"/> 
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>

                                 <h:outputLabel value="#{componentBundle.Ifcomplicatedpr}" styleClass="formLabel" style="width: 564px;text-align:left;margin-left:30px;"></h:outputLabel>
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 330px;" ></h:outputLabel>

                                   <h:commandButton id="page5ComplicationsDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page5HealthQuest.hasComplicationsDetails}"  
								onclick="launchDetailsPopUp('page5ComplicationsDetails');" style="margin-right:10px;margin-left:40px;"/>
                             </h:panelGroup>  
                             			
                           <f:subview id="tabHeader">
				       <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			        </f:subview>

				</div>
				 <h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page5HealthQuest.commitAction}"></h:commandButton>
			</h:form>
			
		</f:view>
</body>
</html>
