<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Diabetic</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_DIABETIC" value="#{pc_Diabetic}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Diabetic.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Diabetic.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0">
						<col width="325">
							<col width="200">
								<col width="105">
									<tr style="padding-top: 5px">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Diabetic}" style="width: 220px" id="dia" rendered="#{pc_Diabetic.showDia}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td>
											<h:outputText style="width:350px;text-align:left;margin-left:5px" value="#{componentBundle.Atwhatagewereyo}" id="atdiabetic" styleClass="formLabel" rendered="#{pc_Diabetic.showAtdiabetic}"></h:outputText>
										</td>
										<td>
											<h:inputText style="width: 72px" id="aged" value="#{pc_Diabetic.age}" rendered="#{pc_Diabetic.showAged}" disabled="#{pc_Diabetic.disableAged}"></h:inputText>
										</td>
										<td>
										     <h:commandButton id="diagnosed" image="images/circle_i.gif"  onclick="launchDetailsPopUp('diagnosed');" style="margin-right:10px;" rendered="#{pc_Diabetic.show1}" styleClass="ovitalic#{pc_Diabetic.hasdiagnosed}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td colspan="2">
											<h:outputText style="width: 561px;text-align:left;margin-left:5px" value="#{componentBundle.Pleaselistthena}" id="pleasediabetes" styleClass="formLabel" rendered="#{pc_Diabetic.showPleasediabetes}"></h:outputText>
										</td>
										<td>
										    <h:commandButton id="doctorslist" image="images/circle_i.gif"  onclick="launchDetailsPopUp('doctorslist');" style="margin-right:10px;" rendered="#{pc_Diabetic.show2}" styleClass="ovitalic#{pc_Diabetic.hasdoctorslist}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td>
											<h:outputText style="width:350px;text-align:left;margin-left:5px" value="#{componentBundle.Howisyourdiabet}" id="hownow" styleClass="formLabel" rendered="#{pc_Diabetic.showHownow}"></h:outputText>
											<h:outputText value="#{componentBundle.iedietoralmedic}" styleClass="formLabel" id="ie" style="width:350px;text-align:left;margin-left:5px" rendered="#{pc_Diabetic.showIe}"></h:outputText>
										</td>
										<td></td>
										<td>
										    <h:commandButton id="treatmentinfo" image="images/circle_i.gif"  onclick="launchDetailsPopUp('treatmentinfo');" style="margin-right:10px;" rendered="#{pc_Diabetic.show3}" styleClass="ovitalic#{pc_Diabetic.hastreatmentinfo}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td colspan="2">
											<h:outputText value="#{componentBundle.Ifoninsulintrea}" id="ifadministration" styleClass="formLabel" style="width: 560px;text-align:left;margin-left:15px" rendered="#{pc_Diabetic.showIfadministration}"></h:outputText>
										</td>
										<td>
										    <h:commandButton id="description" image="images/circle_i.gif"  onclick="launchDetailsPopUp('description');" style="margin-right:10px;" rendered="#{pc_Diabetic.show4}" styleClass="ovitalic#{pc_Diabetic.hasdescription}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td>
											<h:outputText value="#{componentBundle.Hasyourtreatmen}" styleClass="formLabel" style="width: 302px;text-align:left;margin-left:15px" id="hasyears" rendered="#{pc_Diabetic.showHasyears}"></h:outputText>
										</td>
										<td colspan="1">
											<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo}" id="prefix_0No" onclick="toggleCBGroup(this.form.id, 'prefix_0No',  'prefix_0');"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes}" id="prefix_0Yes" onclick="toggleCBGroup(this.form.id, 'prefix_0Yes',  'prefix_0');if(document.getElementById(this.id).checked){launchDetailsPopUp('modify')};"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td style="margin-left: 124px"></td>
						<tr>
							<td>
								<h:outputText value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" style="width: 335px;text-align:left;margin-left:30px" id="ifdetails" rendered="#{pc_Diabetic.showIfdetails}"></h:outputText>
							</td>
							<td></td>
							<td>
							    <h:commandButton id="modify" image="images/circle_i.gif"  onclick="launchDetailsPopUp('modify');" style="margin-right:10px;" rendered="#{pc_Diabetic.show5}" styleClass="ovitalic#{pc_Diabetic.hasmodify}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="2">
								<h:outputText style="width: 476px;text-align:left;margin-left:5px" value="#{componentBundle.HaveyoueverhadIndiana}" styleClass="formLabel" id="haveseen" rendered="#{pc_Diabetic.showHaveseen}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.Elevatedbloodpr}" styleClass="formLabel" style="width:300px;" id="elevatedpressure" rendered="#{pc_Diabetic.showElevatedpressure}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_1}" id="prefix_1No" onclick="toggleCBGroup(this.form.id, 'prefix_1No',  'prefix_1');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_1}" id="prefix_1Yes" onclick="toggleCBGroup(this.form.id, 'prefix_1Yes',  'prefix_1');if(document.getElementById(this.id).checked){launchDetailsPopUp('blood')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="blood" image="images/circle_i.gif"  onclick="launchDetailsPopUp('blood');" style="margin-right:10px;" rendered="#{pc_Diabetic.show6}" styleClass="ovitalic#{pc_Diabetic.hasblood}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.Hearttrouble}" styleClass="formLabel" style="width:300px;" id="hearttrouble" rendered="#{pc_Diabetic.showHearttrouble}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_2}" id="prefix_2No" onclick="toggleCBGroup(this.form.id, 'prefix_2No',  'prefix_2');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_2}" id="prefix_2Yes"  onclick="toggleCBGroup(this.form.id, 'prefix_2Yes',  'prefix_2');if(document.getElementById(this.id).checked){launchDetailsPopUp('trouble')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="trouble" image="images/circle_i.gif"  onclick="launchDetailsPopUp('trouble');" style="margin-right:10px;" rendered="#{pc_Diabetic.show7}" styleClass="ovitalic#{pc_Diabetic.hastrouble}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.EyeTrouble}" styleClass="formLabel" style="width:300px;" id="eyetrouble" rendered="#{pc_Diabetic.showEyetrouble}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_3}" id="prefix_3No" onclick="toggleCBGroup(this.form.id, 'prefix_3No',  'prefix_3');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_3}" id="prefix_3Yes" onclick="toggleCBGroup(this.form.id, 'prefix_3Yes',  'prefix_3');if(document.getElementById(this.id).checked){launchDetailsPopUp('eytrouble')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="eytrouble" image="images/circle_i.gif"  onclick="launchDetailsPopUp('eytrouble');" style="margin-right:10px;" rendered="#{pc_Diabetic.show8}" styleClass="ovitalic#{pc_Diabetic.haseytrouble}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.Kidneytrouble}" styleClass="formLabel" style="width:300px;" id="kidneytrouble" rendered="#{pc_Diabetic.showKidneytrouble}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_4}" id="prefix_4No" onclick="toggleCBGroup(this.form.id, 'prefix_4No',  'prefix_4');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_4}" id="prefix_4Yes" onclick="toggleCBGroup(this.form.id, 'prefix_4Yes',  'prefix_4');if(document.getElementById(this.id).checked){launchDetailsPopUp('kidtrouble')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="kidtrouble" image="images/circle_i.gif"  onclick="launchDetailsPopUp('kidtrouble');" style="margin-right:10px;" rendered="#{pc_Diabetic.show9}" styleClass="ovitalic#{pc_Diabetic.haskidtrouble}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.Recurrentinfect}" styleClass="formLabel" style="width:300px;" id="recurrentinfections" rendered="#{pc_Diabetic.showRecurrentinfections}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_5}" id="prefix_5No" onclick="toggleCBGroup(this.form.id, 'prefix_5No',  'prefix_5');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_5}" id="prefix_5Yes" onclick="toggleCBGroup(this.form.id, 'prefix_5Yes',  'prefix_5');if(document.getElementById(this.id).checked){launchDetailsPopUp('infection')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="infection" image="images/circle_i.gif"  onclick="launchDetailsPopUp('infection');" style="margin-right:10px;" rendered="#{pc_Diabetic.show10}" styleClass="ovitalic#{pc_Diabetic.hasinfection}"/>

							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right">
								<h:outputText value="#{componentBundle.Otherprolongedi}" styleClass="formLabel" style="width:300px;" id="otherillness" rendered="#{pc_Diabetic.showOtherillness}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_6}" id="prefix_6No" onclick="toggleCBGroup(this.form.id, 'prefix_6No',  'prefix_6');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_6}" id="prefix_6Yes" onclick="toggleCBGroup(this.form.id, 'prefix_6Yes',  'prefix_6');if(document.getElementById(this.id).checked){launchDetailsPopUp('illness')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="illness" image="images/circle_i.gif"  onclick="launchDetailsPopUp('illness');" style="margin-right:10px;" rendered="#{pc_Diabetic.show11}" styleClass="ovitalic#{pc_Diabetic.hasillness}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="3">
								<h:outputText style="width: 600px;text-align:left;margin-left:5px" value="#{componentBundle.Haveyoueverbeendiabetic}" styleClass="formLabel" id="havehospitalization" ></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixNo_7}" id="prefix_7No" onclick="toggleCBGroup(this.form.id, 'prefix_7No',  'prefix_7');"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Diabetic.prefixYes_7}" id="prefix_7Yes" onclick="toggleCBGroup(this.form.id, 'prefix_7Yes',  'prefix_7');if(document.getElementById(this.id).checked){launchDetailsPopUp('severecondition')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Ifyespleaseprov}" style="width: 335px;text-align:left;margin-left:15px" id="ifdetails_0" styleClass="formLabel" rendered="#{pc_Diabetic.showIfdetails_0}"></h:outputLabel>
							</td>
							<td></td>
							<td>
							    <h:commandButton id="severecondition" image="images/circle_i.gif"  onclick="launchDetailsPopUp('severecondition');" style="margin-right:10px;" rendered="#{pc_Diabetic.show12}" styleClass="ovitalic#{pc_Diabetic.hasseverecondition}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="2">
								<h:outputText style="width: 526px;text-align:left;margin-left:5px" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="pleaseanswers" rendered="#{pc_Diabetic.showPleaseanswers}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="addtional" image="images/circle_i.gif"  onclick="launchDetailsPopUp('addtional');" style="margin-right:10px;" rendered="#{pc_Diabetic.show13}" styleClass="ovitalic#{pc_Diabetic.hasaddtional}"/>
							</td>
						</tr>
					
						
						<tr style="padding-top: 5px">
							<td colspan="2" style="height: 32px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Diabetic.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_0" rendered="#{pc_Diabetic.show_0}" disabled="#{pc_Diabetic.disable_0}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_Diabetic.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_Diabetic.show_1}" disabled="#{pc_Diabetic.disable_1}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Diabetic.okAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{!pc_Links.showIdDiabetic}" disabled="#{pc_Diabetic.disable_2}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Diabetic.okAction}" onclick="resetTargetFrame(this.form.id);" id="updateButton" rendered="#{pc_Links.showIdDiabetic}" disabled="#{pc_Diabetic.disable_2}"></h:commandButton>
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 19px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
