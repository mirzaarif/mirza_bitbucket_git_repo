<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>respiratory</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
                getScrollXY('page');			
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_RESPIRATORY" value="#{pc_Respiratory}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_LINKS" value="#{pc_Links}"></PopulateBean:Load>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentIDBundle" basename="properties.nbaApplicationID"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Respiratory.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Respiratory.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="350">
							<col width="170">
								<col width="110">
									<tr style="padding-top:5px;">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Respiratory}" style="width: 220px" id="respiratoryid" rendered="#{pc_Respiratory.showRespiratoryid}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="2" style="padding-top: 15px;">
											<h:outputLabel value="#{componentBundle.Whatrespiratorypastmar}" style="width:500px;text-align:left;margin-left:5px;" styleClass="formLabel" id="respiratorycondition" rendered="#{pc_Respiratory.showRespiratorycondition}"></h:outputLabel>
											<h:outputLabel value="#{componentBundle.ieasthmabronchi}" style="width:500px;text-align:left;margin-left:15px;" styleClass="formLabel" id="asthmaid" rendered="#{pc_Respiratory.showAsthmaid}"></h:outputLabel>
										</td>
										<td>
											<h:commandButton id="respiratoryrespiratoryconditioninfo" image="images/circle_i.gif" 
											onclick="launchDetailsPopUp('respiratoryrespiratoryconditioninfo');" style="margin-right:10px;"  rendered="#{pc_Respiratory.showRespiratoryconditioninfo}" disabled="#{pc_Respiratory.disableRespiratoryconditioninfo}" styleClass="ovitalic#{pc_Respiratory.hasRespiratoryConditionDetails}"></h:commandButton>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<tr style="padding-top:5px;">
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputLabel value="#{componentBundle.Howoftendoyouex}" style="width:500px;text-align:left;margin-left:5px;" styleClass="formLabel" id="breathingdifficulty" rendered="#{pc_Respiratory.showBreathingdifficulty}"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratorybreathingdifficultyinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorybreathingdifficultyinfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showBreathingdifficultyinfo}" disabled="#{pc_Respiratory.disableBreathingdifficultyinfo}" styleClass="ovitalic#{pc_Respiratory.hasBreathingDifficultyDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputLabel value="#{componentBundle.Whenwasthelaste}" style="width:340px;text-align:left;margin-left:15px;" styleClass="formLabel" id="lastepisode" rendered="#{pc_Respiratory.showLastepisode}"></h:outputLabel>
							</td>
							<td></td>
							<td>
								<h:commandButton id="respiratorylastepisodeinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorylastepisodeinfo');" style="margin-right:10px;"  rendered="#{pc_Respiratory.showLastepisodeinfo}" disabled="#{pc_Respiratory.disableLastepisodeinfo}" styleClass="ovitalic#{pc_Respiratory.hasLastEpisodeDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputLabel value="#{componentBundle.Wasanemergencyr}" style="width:340px;text-align:left;margin-left:15px;" styleClass="formLabel" id="emergencyrequired" rendered="#{pc_Respiratory.showEmergencyrequired}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo}" id="prefix_1No" onclick="toggleCBGroup(this.form.id, 'prefix_1No',  'prefix_1')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes}" id="prefix_1Yes" onclick="toggleCBGroup(this.form.id, 'prefix_1Yes',  'prefix_1');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratorydatedurationlocationinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputLabel value="#{componentBundle.Ifsopleasestate}" style="width:340px;text-align:left;margin-left:30px;" styleClass="formLabel" id="datedurationlocation" rendered="#{pc_Respiratory.showDatedurationlocation}"></h:outputLabel>
							</td>
							<td align="left"></td>
							<td>
								<h:commandButton id="respiratorydatedurationlocationinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorydatedurationlocationinfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showDatedurationlocationinfo}" disabled="#{pc_Respiratory.disableDatedurationlocationinfo}" styleClass="ovitalic#{pc_Respiratory.hasDateDurationDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="3"></td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputLabel value="#{componentBundle.Doyougetpaininy}" style="width: 363px;text-align:left;margin-left:5px;" styleClass="formLabel" id="paininchest" rendered="#{pc_Respiratory.showPaininchest}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_1}" id="prefix1No" onclick="toggleCBGroup(this.form.id, 'prefix1No',  'prefix1')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_1}" id="prefix1Yes" onclick="toggleCBGroup(this.form.id, 'prefix1Yes',  'prefix1');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratorypaininchestinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="2">
								<h:outputLabel value="#{componentBundle.Ifsounderwhatcondition}" style="width:340px;text-align:left;margin-left:15px;" styleClass="formLabel" id="circumstances" rendered="#{pc_Respiratory.showCircumstances}"></h:outputLabel>
								<h:outputLabel value="#{componentBundle.ieexertioncoldw}" style="width:340px;text-align:left;margin-left:30px;" styleClass="formLabel" id="excertion" rendered="#{pc_Respiratory.showExcertion}"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratorypaininchestinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorypaininchestinfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showCircumstancesinfo}" disabled="#{pc_Respiratory.disableCircumstancesinfo}" styleClass="ovitalic#{pc_Respiratory.hasPainInChestDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputLabel value="#{componentBundle.Doyougetshortof}" style="width:500px;text-align:left;margin-left:5px;" styleClass="formLabel" id="shortofbreath" rendered="#{pc_Respiratory.showShortofbreath}"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="right">
								<h:outputLabel value="#{componentBundle.Whenclimbingafl}" style="width:300px;" styleClass="formLabel" id="climbingid" rendered="#{pc_Respiratory.showClimbingid}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_2}" id="prefix2No" onclick="toggleCBGroup(this.form.id, 'prefix2No',  'prefix2')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_2}" id="prefix2Yes" onclick="toggleCBGroup(this.form.id, 'prefix2Yes',  'prefix2');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratoryclimbinginfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratoryclimbinginfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratoryclimbinginfo');" style="margin-right:10px;"  rendered="#{pc_Respiratory.showClimbinginfo}" disabled="#{pc_Respiratory.disableClimbinginfo}" styleClass="ovitalic#{pc_Respiratory.hasClimbingStairDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="right">
								<h:outputLabel value="#{componentBundle.Whenwalkingmore}" style="width:300px;" styleClass="formLabel" id="walkingid" rendered="#{pc_Respiratory.showWalkingid}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_3}" id="prefix3No" onclick="toggleCBGroup(this.form.id, 'prefix3No',  'prefix3')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_3}" id="prefix3Yes" onclick="toggleCBGroup(this.form.id, 'prefix3Yes',  'prefix3');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratorywalkinginfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratorywalkinginfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorywalkinginfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showWalkinginfo}" disabled="#{pc_Respiratory.disableWalkinginfo}" styleClass="ovitalic#{pc_Respiratory.hasWalkingFewBlockDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="right">
								<h:outputLabel value="#{componentBundle.Whenatrest}" style="width:300px;" styleClass="formLabel" id="atrest" rendered="#{pc_Respiratory.showAtrest}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_4}" id="prefix4No" onclick="toggleCBGroup(this.form.id, 'prefix4No',  'prefix4')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_4}" id="prefix4Yes" onclick="toggleCBGroup(this.form.id, 'prefix4Yes',  'prefix4');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratoryatrestinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratoryatrestinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratoryatrestinfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showAtrestinfo}" disabled="#{pc_Respiratory.disableAtrestinfo}" styleClass="ovitalic#{pc_Respiratory.hasAtRestDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputLabel value="#{componentBundle.Whatphysicianshpastmar}" style="width:520px;text-align:left;margin-left:5px;" styleClass="formLabel" id="physiciansseenforcondition" rendered="#{pc_Respiratory.showPhysiciansseenforcondition}"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratoryphysiciansseenforconditioninfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratoryphysiciansseenforconditioninfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showPhysiciansseenforconditioninfo}" disabled="#{pc_Respiratory.disablePhysiciansseenforconditioninfo}" styleClass="ovitalic#{pc_Respiratory.hasPhsycbDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputLabel value="#{componentBundle.Inthepast7years}" style="width:500px;text-align:left;margin-left:5px;" styleClass="formLabel" id="medicationforrespiratorycondition" rendered="#{pc_Respiratory.showMedicationforrespiratorycondition}"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_5}" id="prefix5No" onclick="toggleCBGroup(this.form.id, 'prefix5No',  'prefix5')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_5}" id="prefix5Yes" onclick="toggleCBGroup(this.form.id, 'prefix5Yes',  'prefix5');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratorymedicationlistinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputLabel value="#{componentBundle.Ifsolistmedicat}" style="width: 392px;text-align:left;margin-left:15px;" styleClass="formLabel" id="medicationlist" rendered="#{pc_Respiratory.showMedicationlist}"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratorymedicationlistinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorymedicationlistinfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showMedicationlistinfo}" disabled="#{pc_Respiratory.disableMedicationlistinfo}" styleClass="ovitalic#{pc_Respiratory.hasMedicationForRespiratoryDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputLabel value="#{componentBundle.Haveyoueverdone}" style="width:600px;text-align:left;margin-left:5px;" styleClass="formLabel" id="miningid" rendered="#{pc_Respiratory.showMiningid}"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixNo_6}" id="prefix6No" onclick="toggleCBGroup(this.form.id, 'prefix6No',  'prefix6')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Respiratory.prefixYes_6}" id="prefix6Yes" onclick="toggleCBGroup(this.form.id, 'prefix6Yes',  'prefix6');if(document.getElementById(this.id).checked){launchDetailsPopUp('respiratorymininghowlonginfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputLabel value="#{componentBundle.Ifsowhichhaveyo}" style="width: 392px;text-align:left;margin-left:15px;" styleClass="formLabel" id="mininghowlong" rendered="#{pc_Respiratory.showMininghowlong}"></h:outputLabel>
							</td>
							<td></td>
							<td>
								<h:commandButton id="respiratorymininghowlonginfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratorymininghowlonginfo');" style="margin-right:10px;" rendered="#{pc_Respiratory.showMininghowlonginfo}" disabled="#{pc_Respiratory.disableMininghowlonginfo}" styleClass="ovitalic#{pc_Respiratory.hasMiningDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputLabel value="#{componentBundle.Pleaseaddanyadd}" style="width:500px;text-align:left;margin-left:15px;" styleClass="formLabel" id="additionalinformation" rendered="#{pc_Respiratory.showAdditionalinformation}"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="respiratoryadditionalinformationinfo" image="images/circle_i.gif" 
								onclick="launchDetailsPopUp('respiratoryadditionalinformationinfo');" style="margin-right:10px;"  rendered="#{pc_Respiratory.showAdditionalinformationinfo}" disabled="#{pc_Respiratory.disableAdditionalinformationinfo}" styleClass="ovitalic#{pc_Respiratory.hasAdditionalInformationDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Respiratory.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="cancel" rendered="#{pc_Respiratory.showCancel}" disabled="#{pc_Respiratory.disableCancel}"></h:commandButton>
								<h:commandButton value="clear" styleClass="buttonLeft-1" action="#{pc_Respiratory.clearAction}" onclick="resetTargetFrame(this.form.id);" id="clear" rendered="#{pc_Respiratory.showClear}" disabled="#{pc_Respiratory.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Respiratory.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok" rendered="#{!pc_Links.showIdRespiratory}" disabled="#{pc_Respiratory.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Respiratory.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok2" rendered="#{pc_Links.showIdRespiratory}" disabled="#{pc_Respiratory.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
