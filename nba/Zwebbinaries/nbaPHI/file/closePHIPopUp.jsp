<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- NBA213            7      Unified user Interface -->

<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Close Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="theme/accelerator.css">
<script type="text/javascript">	
        if(top.window.opener.document.forms['phiinterview']){
           top.window.opener.document.forms['phiinterview'].submit();
         } else if(top.window.opener.document.forms['page']){             
            top.window.opener.document.forms['page'].submit();
         }
		top.window.close(); 
	</script>
</head>
<body>
</body>
</html>
