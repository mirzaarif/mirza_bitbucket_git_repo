<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Military Occupation</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_MILITARYOCCUPATION" value="#{pc_MilitaryOccupation}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_MilitaryOccupation.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_MilitaryOccupation.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="325">
							<col width="200">
								<col width="105">
									<tr style="padding-top: 5px">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.MilitaryStatus}" style="width: 220px" id="_0" rendered="#{pc_MilitaryOccupation.show_0}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td colspan="3" align="left">
											<h:outputText style="width: 576px;text-align:left;margin-left:5px;" value="#{componentBundle.Areyouonactived}" styleClass="formLabel" id="armyActiveDutyLabel" rendered="#{pc_MilitaryOccupation.showArmyActiveDutyLabel}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td align="left" colspan="1"></td>
										<td colspan="1" align="left">
											<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.areYouOnActiveDutyNo}" id="areYouOnActiveDutyNo" onclick="toggleCBGroup(this.form.id, 'areYouOnActiveDutyNo',  'areYouOnActiveDuty',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.areYouOnActiveDutyYes}" id="areYouOnActiveDutyYes" valueChangeListener="#{pc_MilitaryOccupation.collapseActiveDuty}" onclick="toggleCBGroup(this.form.id, 'areYouOnActiveDutyYes',  'areYouOnActiveDuty',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryArmyAffiDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_MilitaryOccupation.expandActiveDutyInd}">
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="armyActiveDutyLabel_0" rendered="#{pc_MilitaryOccupation.showArmyActiveDutyLabel_0}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="2">
								<h:outputText value="#{componentBundle.Whatisyourmilit}" styleClass="formLabel" style="width: 360px;text-align:left;margin-left:30px;" id="armyAffilLabel" rendered="#{pc_MilitaryOccupation.showArmyAffilLabel}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="millitaryArmyAffiDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('millitaryArmyAffiDetails');" rendered="#{pc_MilitaryOccupation.showArmyAffiDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyAffiliationDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.BranchieArtille}" styleClass="formLabel" style="width: 275px;text-align:left;margin-left:30px;" id="armyBranchLabel" rendered="#{pc_MilitaryOccupation.showArmyBranchLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryArmyBranchDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('millitaryArmyBranchDetails');" rendered="#{pc_MilitaryOccupation.showArmyBranchDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyBranchDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="3" align="left">
								<h:outputText value="#{componentBundle.AreyoucurrentlyAssigned}" styleClass="formLabel" style="width: 539px;text-align:left;margin-left:30px;" id="armyCurrentAssignedLabel" rendered="#{pc_MilitaryOccupation.showArmyCurrentAssignedLabel}"></h:outputText>
								<h:outputText value="#{componentBundle.ieMedicalassign}" styleClass="formLabel" style="width: 360px;text-align:left;margin-left:30px;" id="armyMedicalAssignedLabel" rendered="#{pc_MilitaryOccupation.showArmyMedicalAssignedLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.armyMedicalAssignedYesNoNo}" id="armyMedicalAssignedYesNoNo" onclick="toggleCBGroup(this.form.id, 'armyMedicalAssignedYesNoNo',  'armyMedicalAssignedYesNo')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.armyMedicalAssignedYesNoYes}" id="armyMedicalAssignedYesNoYes" onclick="toggleCBGroup(this.form.id, 'armyMedicalAssignedYesNoYes',  'armyMedicalAssignedYesNo');if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryArmyCurrentAssignDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Ifsospecifycurr}" styleClass="formLabel" style="width: 275px;text-align:left;margin-left:30px;" id="armyCurrentAssignLabel" rendered="#{pc_MilitaryOccupation.showArmyCurrentAssignLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryArmyCurrentAssignDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryArmyCurrentAssignDetails');" rendered="#{pc_MilitaryOccupation.showArmyCurrentAssignDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyCurrentAssignedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Duties}" styleClass="formLabel" style="width: 275px;text-align:left;margin-left:30px;" id="armyDutiesLabel" rendered="#{pc_MilitaryOccupation.showArmyDutiesLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryArmyDutiesDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryArmyDutiesDetails');" rendered="#{pc_MilitaryOccupation.showArmyDutiesDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyDutiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="3" align="left">
								<h:outputText value="#{componentBundle.Ifdutiesinvolve}" styleClass="formLabel" style="width: 480px;text-align:left;padding-bottom:10px;margin-left:30px;" id="armyDutiesInvolveLabel" rendered="#{pc_MilitaryOccupation.showArmyDutiesInvolveLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.PaygradeieE5}" styleClass="formLabel" style="width: 275px;text-align:left;margin-left:30px;" id="armyPayGradeLabel" rendered="#{pc_MilitaryOccupation.showArmyPayGradeLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryArmyPayGradeDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryArmyPayGradeDetails');" rendered="#{pc_MilitaryOccupation.showArmyPayGradeDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyPayGradedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="3" align="left">
								<h:outputText value="#{componentBundle.Haveyoubeenaler}" styleClass="formLabel" style="width: 539px;text-align:left;margin-left:30px;" id="armyAlertedActiveDutyLabel" rendered="#{pc_MilitaryOccupation.showArmyAlertedActiveDutyLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.alertedForActiveDutyOutsideUSNo}" id="alertedForActiveDutyOutsideUSNo" onclick="toggleCBGroup(this.form.id, 'alertedForActiveDutyOutsideUSNo',  'alertedForActiveDutyOutsideUS')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.alertedForActiveDutyOutsideUSYes}" id="alertedForActiveDutyOutsideUSYes" onclick="toggleCBGroup(this.form.id, 'alertedForActiveDutyOutsideUSYes',  'alertedForActiveDutyOutsideUS');if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryArmyAlertedActiveDutyIfYesDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputText value="#{componentBundle.Ifsogivedetails}" styleClass="formLabel" style="width: 275px;text-align:left;margin-left:30px;" id="armyAlertedActiveDutyIfYesLabel" rendered="#{pc_MilitaryOccupation.showArmyAlertedActiveDutyIfYesLabel}"></h:outputText>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="millitaryArmyAlertedActiveDutyIfYesDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryArmyAlertedActiveDutyIfYesDetails');" rendered="#{pc_MilitaryOccupation.showArmyAlertedActiveDutyIfYesDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyAlertedActiveDutyDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px">
							<td colspan="3" align="left">
								<h:outputText style="width: 576px;text-align:left;margin-left:5px;" value="#{componentBundle.Ifnotonactivedu}" styleClass="formLabel" id="natinalGuardActiveDutyLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardActiveDutyLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.notOnActiveDutyNo}" id="notOnActiveDutyNo" onclick="toggleCBGroup(this.form.id, 'notOnActiveDutyNo',  'notOnActiveDuty',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.notOnActiveDutyYes}" id="notOnActiveDutyYes" valueChangeListener="#{pc_MilitaryOccupation.collapseNotOnActiveDuty}" onclick="toggleCBGroup(this.form.id, 'notOnActiveDutyYes',  'notOnActiveDuty',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryNatinalGuardMilllitaryAffDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="IfyesNoonActiveDuty" rendered="#{pc_MilitaryOccupation.expandNotOnActiveDutyInd}">
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Ifyes_0}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="natinalGuardActiveDutyIfYesLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardActiveDutyIfYesLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="3"></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="2">
								<h:outputText value="#{componentBundle.Whatisyourmilit_0}" styleClass="formLabel" style="width: 527px;padding-bottom:10px;text-align:left;margin-left:30px;" id="natinalGuardMilllitaryAffLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardMilllitaryAffLabel}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="millitaryNatinalGuardMilllitaryAffDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardMilllitaryAffDetails');" rendered="#{pc_MilitaryOccupation.showNatinalGuardMilllitaryAffDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNGMillitaryAffiliationDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.IsyoudutyActive}" styleClass="formLabel" style="width: 275px;text-align:left;padding-bottom:10px;margin-left:30px;" id="natinalGuardActiveInactiveStandByLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardActiveInactiveStandByLabel}"></h:outputText>
							</td>
							<td align="left" colspan="2">
								<h:selectBooleanCheckbox id="active" value="#{pc_MilitaryOccupation.active}" rendered="#{pc_MilitaryOccupation.showActive}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:inactive').checked=false;document.getElementById('page:standby').checked=false;};" disabled="#{pc_MilitaryOccupation.disableActive}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Active}" styleClass="formLabelRight" style="margin-right:5px;" id="_1" rendered="#{pc_MilitaryOccupation.show_1}"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.inactive}" id="inactive" rendered="#{pc_MilitaryOccupation.showInactive}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:active').checked=false;document.getElementById('page:standby').checked=false;};" disabled="#{pc_MilitaryOccupation.disableInactive}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Inactive}" styleClass="formLabelRight" style="margin-right:5px;" id="_2" rendered="#{pc_MilitaryOccupation.show_2}"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.standby}" id="standby" rendered="#{pc_MilitaryOccupation.showStandby}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:active').checked=false;document.getElementById('page:inactive').checked=false;};" disabled="#{pc_MilitaryOccupation.disableStandby}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Standby}" styleClass="formLabelRight" style="margin-right:5px;" id="_3" rendered="#{pc_MilitaryOccupation.show_3}"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.BranchieArtille}" styleClass="formLabel" style="width: 275px;text-align:left;padding-bottom:10px;margin-left:30px;" id="natinalGuardBranchlabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardBranchlabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>							 
								<h:commandButton id="millitaryNatinalGuardBranchDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardBranchDetails');" rendered="#{pc_MilitaryOccupation.showNatinalGuardBranchDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNBBranchedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Duties_0}" styleClass="formLabel" style="width: 275px;text-align:left;padding-bottom:10px;margin-left:30px;" id="natinalGuardDutiesLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardDutiesLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryNatinalGuardDutiesDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardDutiesDetails');" rendered="#{pc_MilitaryOccupation.showNatinalGuardDutiesDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNGDutiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="2">
								<h:outputText value="#{componentBundle.Ifdutiesinvolve}" styleClass="formLabel" style="width: 473px;padding-bottom:10px;margin-left:15px;" id="natinalGuardDutiesInvolveAviationLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardDutiesInvolveAviationLabel}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.PaygradeieE5}" styleClass="formLabel" style="width: 275px;text-align:left;padding-bottom:10px;margin-left:30px;" id="natinalGuardPayGradeLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardPayGradeLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryNatinalGuardPayGradeDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardPayGradeDetails');" rendered="#{pc_MilitaryOccupation.showNatinalGuardPayGradeDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNGPayGradedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Areyouenrolledi}" styleClass="formLabel" style="width: 527px;padding-bottom:10px;text-align:left;margin-left:30px;" id="natinalGuardROTCLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardROTCLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.enrolledROTCNo}" id="enrolledROTCNo" onclick="toggleCBGroup(this.form.id, 'enrolledROTCNo',  'enrolledROTC')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.enrolledROTCYes}" id="enrolledROTCYes" onclick="toggleCBGroup(this.form.id, 'enrolledROTCYes',  'enrolledROTC');if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryNatinalGuardClassDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="2">
								<h:outputLabel value="#{componentBundle.Ifsogivedetails_0}" style="width: 422px;text-align:left;padding-bottom:10px;margin-left:45px;" styleClass="formLabel" id="natinalGuardClassLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardClassLabel}"></h:outputLabel>
							</td>
							<td>								
								<h:commandButton id="millitaryNatinalGuardClassDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardClassDetails');" rendered="#{pc_MilitaryOccupation.showNatinalGuardClassDetailsImg}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNGClassDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Haveyoubeenaler_0}" styleClass="formLabel" style="width: 527px;padding-bottom:10px;text-align:left;margin-left:30px;" id="natinalGuardHaveYouBeenLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardHaveYouBeenLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.alertedActiveDutyNo}" id="alertedActiveDutyNo" onclick="toggleCBGroup(this.form.id, 'alertedActiveDutyNo',  'alertedActiveDuty')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_MilitaryOccupation.alertedActiveDutyYes}" id="alertedActiveDutyYes" onclick="toggleCBGroup(this.form.id, 'alertedActiveDutyYes',  'alertedActiveDuty');if(document.getElementById(this.id).checked){launchDetailsPopUp('millitaryNatinalGuardHaveYouBeenIfYesDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1"></td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="left" colspan="1">
								<h:outputText value="#{componentBundle.Ifsogivedetails_0_1}" styleClass="formLabel" style="width: 275px;text-align:left;padding-bottom:10px;margin-left:45px;" id="natinalGuardHaveYouBeenIfYesLabel" rendered="#{pc_MilitaryOccupation.showNatinalGuardHaveYouBeenIfYesLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="millitaryNatinalGuardHaveYouBeenIfYesDetails" image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('millitaryNatinalGuardHaveYouBeenIfYesDetails');" rendered="#{pc_MilitaryOccupation.show_4}" styleClass="ovitalic#{pc_MilitaryOccupation.hasArmyNGHaveYouBeenDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="_5" rendered="#{pc_MilitaryOccupation.show_5}" disabled="#{pc_MilitaryOccupation.disable_5}" action="#{pc_MilitaryOccupation.cancelAction}" onclick="resetTargetFrame(this.form.id);"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_MilitaryOccupation.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_6" rendered="#{pc_MilitaryOccupation.show_6}" disabled="#{pc_MilitaryOccupation.disable_6}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_MilitaryOccupation.okAction}" onclick="resetTargetFrame(this.form.id);" id="_7" rendered="#{!pc_Links.showIdMilitary}" disabled="#{pc_MilitaryOccupation.disable_7}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_MilitaryOccupation.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdMilitary}" disabled="#{pc_MilitaryOccupation.disable_7}"></h:commandButton>
							</td>							
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" align="left"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
