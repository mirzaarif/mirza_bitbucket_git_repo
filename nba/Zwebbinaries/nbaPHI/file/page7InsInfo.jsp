<!-- CHANGE LOG -->
<!-- Audit Number   	Version   Change Description -->
<!-- FNB004         	NB-1101	PHI-->
<!-- FNB004 - VEPL1206  NB-1101	Look into why "java.lang.IllegalArgumentException" is appearing onbottom of interview screen.-->
<!-- FNB004 - VEPL1372	NB-1101	On PHI Interview Page-7 for in check box for Replace(Yes or No), both 'Yes' and 'No' can be checked at the same time. -->
<!-- NBA324         	NB-1301	nbAFull Personal History Interview -->
<!-- SPRNBA-798         NB-1401   Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Page 7 Ins Info</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<SCRIPT language="JavaScript" src="include/currency.js" ></SCRIPT><!-- FNB004 - VEPL1206  -->
<SCRIPT type="text/javascript" src="javascript/formFunctions.js"></SCRIPT><!-- FNB004 - VEPL1206  -->
<script type="text/javascript">

			<!--
			function setTargetFrame() {
				document.forms['phiinterview'].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
				getScrollXY('phiinterview');	
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}	
			function hiddenClick(){		    
				window.parent.document.getElementById("page1TeleInterview:links_x").click();	  
		     }
			function toggleCBGroup(formName,id,prefix) {
			   var tempPrefix=prefix;  	//FNB004 - VEPL1372
			   var tempId=id;	      	//FNB004 - VEPL1372		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form alert(inputs[i].name);			
							if(inputs[i].name.indexOf(tempId)=='-1'){	  //FNB004 - VEPL1372     	
								if(inputs[i].name.indexOf(tempPrefix)!='-1'){  	//FNB004 - VEPL1372
									inputs[i].checked = false ;
								}
							}//FNB004 - VEPL1372 	
					} 
				}   
			}
			//Method to validate year field
			function year_Validate(field)
			{
				var strInput = field.value;	
				if(strInput.length == 0)
					return true;
				
				if (strInput.length != 4)
				{
					raiseError("Invalid date format, enter YYYY.", field);
					return false;
				}
				if (isNaN(strInput)) {					
					raiseError("Year must be an integer in format yyyy.", field);
					return false;
				}
				var year = strInput - 0;
				
				if (year > 2100)
				{
					raiseError("Year is too far in the future.", field);
					return false;	
				}
				if (year < 1900)
				{
					raiseError("Year is too far in the past.", field);
					return false;	
				}

				field.valid = strInput ;
				return true;
			}
			//  Displays a message box and sets focus to a control specified in the field parameter.
			function raiseError(sError, field, type)
			{
				if (!type){
					var ret = window.confirm(sError + "\nPress OK to make the correction or Cancel to revert back to the old value."); 	
					if(field != null && !field.disabled && ret)	{				
						document.getElementById (field.id).focus();	  	  			
					}else{
						field.value = ( (!field.valid) ? "" : field.valid);			
					}
				}
			}
			function resetCounters()
			{
			  document.forms['phiinterview']['phiinterview:lifecounter'].value = 0;
			  document.forms['phiinterview']['phiinterview:dicounter'].value = 0;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="resetCounters();filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE7INSINFO" value="#{pc_Page7InsInfo}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>			
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				<h:inputHidden id="scrollx" value="#{pc_Page7InsInfo.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page7InsInfo.scrollYC}"></h:inputHidden>
				<h:inputHidden id="lifecounter" value="#{pc_Page7InsInfo.currentLifeCounter}"></h:inputHidden>
				<h:inputHidden id="dicounter" value="#{pc_Page7InsInfo.currentDICounter}"></h:inputHidden>
				<div class="inputForm">
                               <h:outputLabel value="#{componentBundle.InsuranceInform}" style="width:508px" styleClass="sectionSubheader"> <!-- NBA324 -->
                               </h:outputLabel>
                               <h:panelGroup id="healthinsurancepg">
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                   <h:outputLabel style="width: 470px; text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseprovidein}" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page7InsInfo.healthinsuranceNo}" onclick="toggleCBGroup(this.form.id, 'healthinsuranceNo',  'healthinsurance')" id="healthinsuranceNo"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="healthinsurance"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page7InsInfo.healthinsuranceYes}" onclick="toggleCBGroup(this.form.id, 'healthinsuranceYes',  'healthinsurance');if(document.getElementById(this.id).checked){launchDetailsPopUp('page7InsuranceApplicationDetails')};" id="healthinsuranceYes"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="healthinsurance_0"></h:outputLabel>
                                     <h:commandButton id="page7InsuranceApplicationDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasInsAppDetails}" 
								onclick="launchDetailsPopUp('page7InsuranceApplicationDetails');" style="margin-right:10px;margin-left:40px;"/>
                                    


                               </h:panelGroup>
                               <Help:Link contextId="Q1_H_OtherInsuranceApps" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  				<Help:Link contextId="Q1_W_OtherInsuranceApps" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                               <h:panelGroup id="lifeinsurancePG">
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel style="width: 470px; text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveanylif}" styleClass="formLabel" ></h:outputLabel>
					    <h:outputLabel style="width: 470px; text-align:left;margin-left:15px;" value="#{componentBundle.Ifyescompleteth}" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page7InsInfo.lifeinsuranceNo}" onclick="toggleCBGroup(this.form.id, 'doYouHaveLIinForceNo',  'doYouHaveLIinForce')" id="doYouHaveLIinForceNo"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="lifeinsurance"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page7InsInfo.lifeinsuranceYes}" onclick="toggleCBGroup(this.form.id, 'doYouHaveLIinForceYes',  'doYouHaveLIinForce')" id="doYouHaveLIinForceYes"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px;margin-right:70px;" styleClass="formLabelRight" id="lifeinsurance_0"></h:outputLabel>

                                  


                               </h:panelGroup>
                               <Help:Link contextId="Q2_H_LifeInsurance" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				 				 <Help:Link contextId="Q2_W_LifeInsurance" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
                               <h:panelGrid id="lifeInsurancePGrid" columns="2" styleClass="formDataEntryLine" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0" style="width: 575px;">
			             <h:column>
				        <h:dataTable id="lifeinsurancetable" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_Page7InsInfo.lifeInsuranceList}" var="lifeinsurance"
						style="width: 510px;margin-top: -4px;">						
					    <h:column id="liCol1">
						<h:panelGroup id="lifeInsuranceDetails" styleClass="formDataEntryLine">
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.CompanyFullName}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="companyname" styleClass="formEntryTextHalf" style="margin-right:60px;width: 100px;" value="#{lifeinsurance.companyFullName}"></h:inputText>
                                       <h:commandButton id="page7LifeInsuranceCompany" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasLifeInsuranceCompany}" 
								onclick="launchDetailsPopUp('page7LifeInsuranceCompany'+#{lifeinsurance.counter});" style="margin-right:10px;margin-left:40px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.YearIssued}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="yearissued" styleClass="formEntryTextHalf" style="margin-right:60px;width: 50px;" value="#{lifeinsurance.yearIssued}" onblur="year_Validate(this);">                                      
                                       </h:inputText>
                                       <h:commandButton id="page7LifeInsuranceYearIssued" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasLifeInsuranceYearIssued}" 
								onclick="launchDetailsPopUp('page7LifeInsuranceYearIssued'+#{lifeinsurance.counter});" style="margin-right:10px;margin-left:90px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.Amount}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="lifeInsuranceAmount" styleClass="formEntryTextHalf" style="margin-right:60px;width: 100px;" value="#{lifeinsurance.amount}" onkeypress="checkPositiveMoney(this, event)"><f:convertNumber type="currency" currencySymbol="#{componentBundle.currencySymbol}" maxFractionDigits="2"/></h:inputText><!-- FNB004 - VEPL1206  -->
                                       <h:commandButton id="page7LifeInsuranceAmountDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasLifeInsuranceAmountDetails}" 
								onclick="launchDetailsPopUp('page7LifeInsuranceAmountDetails'+#{lifeinsurance.counter});" style="margin-right:40px;margin-left:40px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.PurposeofInsura}" styleClass="formLabel"></h:outputLabel>
                                        <h:selectOneMenu id="purposeofinsurance" styleClass="formEntryTextHalf" style="width: 200px;" value="#{lifeinsurance.purposeofInsurance}">
									          <f:selectItems value="#{pc_Page7InsInfo.liPurposeList}"></f:selectItems>
						                 </h:selectOneMenu>

                                        
                                       <h:commandButton id="page7LifeInsurancePurpose" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasLifeInsurancePurpose}" 
								onclick="launchDetailsPopUp('page7LifeInsurancePurpose'+#{lifeinsurance.counter});" style="margin-left:0px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.Replaceyesorno}" styleClass="formLabel"></h:outputLabel>
                                       <h:selectBooleanCheckbox value="#{lifeinsurance.lifeinsurancereplace1No}" onclick="toggleCBGroup(this.form.id, 'lifeinsurancereplace1No',  'lifeinsurancereplace1')" id="lifeinsurancereplace1No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="lifeinsurancereplace1"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{lifeinsurance.lifeinsurancereplace1Yes}" onclick="toggleCBGroup(this.form.id, 'lifeinsurancereplace1Yes',  'lifeinsurancereplace1');if(document.getElementById(this.id).checked){launchDetailsPopUp('page7LifeInsuranceReplaceDetails'+#{lifeinsurance.counter})};" id="lifeinsurancereplace1Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px;margin-right:50px;" styleClass="formLabelRight" id="lifeinsurancereplace1_0"></h:outputLabel>
                                        <h:commandButton id="page7LifeInsuranceReplaceDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasLifeInsuranceReplaceDetails}" 
								onclick="launchDetailsPopUp('page7LifeInsuranceReplaceDetails'+#{lifeinsurance.counter});" style="margin-right:10px;margin-left:40px;"/>
                                  
						</h:panelGroup>
						
						
								
					</h:column>
				     </h:dataTable>
				         <h:commandLink id="addAnother1" value="#{componentBundle.buttonAddAnother}" styleClass="formButtonInterface" action="#{pc_Page7InsInfo.addAnotherLifeInsurance}" style="margin-bottom: 2px;margin-left: 380px; width: 65px;"/>              
			          </h:column>
			        </h:panelGrid>
			         <h:panelGroup id="diinsurancePG" >
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
                                  <h:outputLabel style="width: 332px; text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveanydis}" styleClass="formLabel" ></h:outputLabel>
					    <h:outputLabel style="width: 470px; text-align:left;margin-left:15px;" value="#{componentBundle.Ifyescompleteth}" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
                                   <h:selectBooleanCheckbox value="#{pc_Page7InsInfo.diinsuranceNo}" onclick="toggleCBGroup(this.form.id, 'doYouHaveDIinForceNo',  'doYouHaveDIinForce')" id="doYouHaveDIinForceNo"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="diinsurance"></h:outputLabel>
						<h:selectBooleanCheckbox value="#{pc_Page7InsInfo.diinsuranceYes}" onclick="toggleCBGroup(this.form.id, 'doYouHaveDIinForceYes',  'doYouHaveDIinForce')" id="doYouHaveDIinForceYes"></h:selectBooleanCheckbox>
						<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px;margin-right:70px;" styleClass="formLabelRight" id="diinsurance_0"></h:outputLabel>

                                
                               </h:panelGroup>
                               <Help:Link contextId="Q2_H_DisabilityIncome" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
                         <h:panelGrid id="diInsurancePGrid" columns="2" styleClass="formDataEntryLine" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0" style="width: 575px;">
			             <h:column>
				        <h:dataTable id="diinsurancetable" styleClass="formTableData" cellspacing="0" cellpadding="0" rows="0" value="#{pc_Page7InsInfo.diInsuranceList}" var="diinsurance"
						style="width: 510px;margin-top: -4px;">						
					    <h:column id="diCol1">
						   <h:panelGroup id="diInsuranceDetails" styleClass="formDataEntryLine">
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.CompanyFullName}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="companyname_1" styleClass="formEntryTextHalf" style="margin-right:60px;width: 100px;" value="#{diinsurance.companyFullName}"></h:inputText>
                                       <h:commandButton id="page7DIInsuranceCompany" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceCompany}" 
								onclick="launchDetailsPopUp('page7DIInsuranceCompany'+#{diinsurance.counter});" style="margin-right:10px;margin-left:40px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.YearIssued}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="yearissued_1" styleClass="formEntryTextHalf" style="margin-right:60px;width: 50px;" value="#{diinsurance.yearIssued}" onblur="year_Validate(this);">                                       
                                       </h:inputText>
                                       <h:commandButton id="page7DIInsuranceYearIssued" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceYearIssued}" 
								onclick="launchDetailsPopUp('page7DIInsuranceYearIssued'+#{diinsurance.counter});" style="margin-right:10px;margin-left:90px;"/>
                                       <h:outputLabel style="width: 170px;" value="#{componentBundle.Amount}" styleClass="formLabel"></h:outputLabel>
                                       <h:inputText id="disabilityIncomeAmount" styleClass="formEntryTextHalf" style="margin-right:60px;width: 100px;" value="#{diinsurance.amount}" onkeypress="checkPositiveMoney(this, event)" ><f:convertNumber type="currency" currencySymbol="#{componentBundle.currencySymbol}" maxFractionDigits="2"/></h:inputText><!-- FNB004 - VEPL1206  -->
                                       <h:commandButton id="page7DIInsuranceAmountDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceAmountDetails}" 
								onclick="launchDetailsPopUp('page7DIInsuranceAmountDetails'+#{diinsurance.counter});" style="margin-right:80px;margin-left:40px;"/>
                                   <h:outputLabel style="width: 170px;margin-right:50px;" value="#{componentBundle.BenefitPeriod}" styleClass="formLabel"></h:outputLabel>
                                        <h:commandButton id="page7DIInsuranceBenefitPeriod" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceBenefitPeriod}" 
								onclick="launchDetailsPopUp('page7DIInsuranceBenefitPeriod'+#{diinsurance.counter});" style="margin-right:60px;margin-left:149px;"/>

                                       
                                       <h:outputLabel style="width: 170px;margin-right:50px;" value="#{componentBundle.WaitingPeriod}" styleClass="formLabel"></h:outputLabel>
                                       <h:commandButton id="page7DIInsuranceWaitingPeriod" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceWaitingPeriod}" 
								onclick="launchDetailsPopUp('page7DIInsuranceWaitingPeriod'+#{diinsurance.counter});" style="margin-right:40px;margin-left:149px;"/>
                                 <h:outputLabel style="width: 170px;" value="#{componentBundle.Replaceyesorno}" styleClass="formLabel"></h:outputLabel>
                                 <h:selectBooleanCheckbox value="#{diinsurance.diinsurancereplaceNo}" onclick="toggleCBGroup(this.form.id, 'diinsurancereplaceNo',  'diinsurancereplace')"  id="diinsurancereplaceNo"></h:selectBooleanCheckbox>
								 <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="diinsurancereplace"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{diinsurance.diinsurancereplaceYes}" onclick="toggleCBGroup(this.form.id, 'diinsurancereplaceYes',  'diinsurancereplace');if(document.getElementById(this.id).checked){launchDetailsPopUp('page7DIInsuranceReplaceDetails'+#{diinsurance.counter})};" id="diinsurancereplaceYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px;margin-right:50px;" styleClass="formLabelRight" id="diinsurancereplace_0"></h:outputLabel>
								 <h:commandButton id="page7DIInsuranceReplaceDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page7InsInfo.hasDIInsuranceReplaceDetails}" 
								onclick="launchDetailsPopUp('page7DIInsuranceReplaceDetails'+#{diinsurance.counter});" style="margin-right:10px;margin-left:35px;"/>
								 
                                      
								
                                      
						</h:panelGroup>
						   
						
					</h:column>
				     </h:dataTable>
				        <h:commandLink id="addAnother2" value="#{componentBundle.buttonAddAnother}" styleClass="formButtonInterface" action="#{pc_Page7InsInfo.addAnotherDIInsurance}" style="margin-bottom: 2px;margin-left: 380px; width: 65px;"/>              
			          </h:column>
			         
		          </h:panelGrid>
			        
                       
                                
                                <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;margin-right:60px;" ></h:outputLabel>
                               <h:commandButton style="margin-left:380px;" styleClass="formButton" value="Final Review" action="#{pc_Page7InsInfo.finalReviewAction}" onclick="hiddenClick();resetTargetFrame(this.form.id);"></h:commandButton> <!-- NBA324 -->




                         
					
                           <f:subview id="tabHeader">
				       <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			        </f:subview>


				</div>
                       <h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page7InsInfo.commitAction}"></h:commandButton>

			</h:form>
			<div id="Messages" style="display:none"><h:messages /></div> <!-- FNB004 - VEPL1206  -->
		</f:view>
</body>
</html>
