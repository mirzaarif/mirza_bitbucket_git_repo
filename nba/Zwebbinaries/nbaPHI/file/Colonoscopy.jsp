<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>colonoscopy</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
		function valueChangeEvent() {	
			    getScrollXY('page');
				}
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_COLONOSCOPY" value="#{pc_Colonoscopy}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Colonoscopy.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Colonoscopy.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px;" cellpadding="0" cellspacing="0" style="cellpadding: 0px; cellspacing: 0px;table-layout:fixed">
						<col width="410">
							<col width="135">
								<col width="65">
									<tr style="padding-top: 5px">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Colonoscopy}" style="width: 220px" id="colonoscopyLabel" rendered="#{pc_Colonoscopy.showColonoscopyLabel}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td >
											<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouhadacolo}" styleClass="formLabel" id="colonoscopyDoneIn3YrsLabel" rendered="#{pc_Colonoscopy.showColonoscopyDoneIn3YrsLabel}"></h:outputText>
										</td>
										<td colspan="2" >
											<h:selectBooleanCheckbox value="#{pc_Colonoscopy.colonoscopyDoneIn3YrsNo}" onclick="toggleCBGroup(this.form.id, 'colonoscopyDoneIn3YrsNo',  'colonoscopyDoneIn3Yrs',true);valueChangeEvent();" id="colonoscopyDoneIn3YrsNo"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="colonoscopyDoneIn3Yrs"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Colonoscopy.colonoscopyDoneIn3YrsYes}" onclick="toggleCBGroup(this.form.id, 'colonoscopyDoneIn3YrsYes',  'colonoscopyDoneIn3Yrs',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('colonoscopyDetails')};valueChangeEvent();submit();" id="colonoscopyDoneIn3YrsYes" valueChangeListener="#{pc_Colonoscopy.colonoscopyDetails}"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="colonoscopyDoneIn3Yrs_0"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Colonoscopy.expandColonoscopyDetails}">
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="yesLabel" rendered="#{pc_Colonoscopy.showYesLabel}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText value="#{componentBundle.Whenandwherewas}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="colonoscopyDetailsLbl" rendered="#{pc_Colonoscopy.showColonoscopyDetailsLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="colonoscopyDetails" onclick="launchDetailsPopUp('colonoscopyDetails');" rendered="#{pc_Colonoscopy.showColonoscopyDetails}" styleClass="ovitalic#{pc_Colonoscopy.hascolonoscopyDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Whywasitdone}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="colonoscopyReasonLbl" rendered="#{pc_Colonoscopy.showColonoscopyReasonLbl}"></h:outputText>
								<h:outputText value="#{componentBundle.Whatweresymptom}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="colonoscopySymptomsLbl" rendered="#{pc_Colonoscopy.showColonoscopySymptomsLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="colonoscopyReason" onclick="launchDetailsPopUp('colonoscopyReason');" rendered="#{pc_Colonoscopy.showColonoscopyReason}" styleClass="ovitalic#{pc_Colonoscopy.hascolonoscopyReason}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Werethereanypol}" styleClass="formLabel" id="anyPolypsFoundLbl" rendered="#{pc_Colonoscopy.showAnyPolypsFoundLbl}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.anyPolypsFoundNo}" onclick="toggleCBGroup(this.form.id, 'anyPolypsFoundNo',  'anyPolypsFound',true);valueChangeEvent();" id="anyPolypsFoundNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="anyPolypsFound"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.anyPolypsFoundYes}" onclick="toggleCBGroup(this.form.id, 'anyPolypsFoundYes',  'anyPolypsFound',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('polypsRemovedDetails')};valueChangeEvent();submit();" id="anyPolypsFoundYes" valueChangeListener="#{pc_Colonoscopy.ifAnyPolypsFound}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="anyPolypsFound_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Colonoscopy.expandIfAnyPolypsFound}">
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="yesPolypsFoundLbl" rendered="#{pc_Colonoscopy.showYesPolypsFoundLbl}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Howmanywerether}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="polypsRemovedDetailsLbl" rendered="#{pc_Colonoscopy.showPolypsRemovedDetailsLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="polypsRemovedDetails" onclick="launchDetailsPopUp('polypsRemovedDetails');" rendered="#{pc_Colonoscopy.showPolypsRemovedDetails}" styleClass="ovitalic#{pc_Colonoscopy.haspolypsRemovedDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText value="#{componentBundle.Whenweretheyall}" styleClass="formLabel" style="width: 378px;text-align:left;margin-left:30px" id="removedMalignantLbl" rendered="#{pc_Colonoscopy.showRemovedMalignantLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="removedMalignantDetails" onclick="launchDetailsPopUp('removedMalignantDetails');" rendered="#{pc_Colonoscopy.showRemovedMalignantDetails}" styleClass="ovitalic#{pc_Colonoscopy.hasremovedMalignantDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px">
							<td colspan="2"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveaprior}" styleClass="formLabel" id="historyOfPolypsLbl" rendered="#{pc_Colonoscopy.showHistoryOfPolypsLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.historyOfPolypsNo}" onclick="toggleCBGroup(this.form.id, 'historyOfPolypsNo',  'historyOfPolyps',true);valueChangeEvent();" id="historyOfPolypsNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="historyOfPolyps"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.historyOfPolypsYes}" onclick="toggleCBGroup(this.form.id, 'historyOfPolypsYes',  'historyOfPolyps',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('historyTimesForPolypsDetails')};valueChangeEvent();submit();" id="historyOfPolypsYes" valueChangeListener="#{pc_Colonoscopy.historyOfPolys}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="historyOfPolyps_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Colonoscopy.expandHistoryOfPolys}">
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="historyOfPolypsYesLbl" rendered="#{pc_Colonoscopy.showHistoryOfPolypsYesLbl}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Whenandhowmanyw}" styleClass="formLabel" style="width: 378px;text-align:left;margin-left:30px" id="historyTimesForPolypsLbl" rendered="#{pc_Colonoscopy.showHistoryTimesForPolypsLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="historyTimesForPolypsDetails" onclick="launchDetailsPopUp('historyTimesForPolypsDetails');" rendered="#{pc_Colonoscopy.showHistoryTimesForPolypsDetails}" styleClass="ovitalic#{pc_Colonoscopy.hashistoryTimesForPolypsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Weretheyremoved}" styleClass="formLabel" style="width: 378px;text-align:left;margin-left:30px" id="removedPolypsLbl" rendered="#{pc_Colonoscopy.showRemovedPolypsLbl}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="removedPolypsDetails" onclick="launchDetailsPopUp('removedPolypsDetails');" rendered="#{pc_Colonoscopy.showRemovedPolypsDetails}" styleClass="ovitalic#{pc_Colonoscopy.hasremovedPolypsDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Anycancerfound}" styleClass="formLabel" id="cancerFoundLbl" rendered="#{pc_Colonoscopy.showCancerFoundLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.cancerFoundNo}" onclick="toggleCBGroup(this.form.id, 'cancerFoundNo',  'cancerFound')" id="cancerFoundNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="cancerFound"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.cancerFoundYes}" onclick="toggleCBGroup(this.form.id, 'cancerFoundYes',  'cancerFound')" id="cancerFoundYes" ></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="cancerFound_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.ColonoscopyHaveyoueverbeen}" styleClass="formLabel" id="youHadColitisLbl" rendered="#{pc_Colonoscopy.showYouHadColitisLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.youHadColitisNo}" onclick="toggleCBGroup(this.form.id, 'youHadColitisNo',  'youHadColitis')" id="youHadColitisNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="youHadColitis"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.youHadColitisYes}" onclick="toggleCBGroup(this.form.id, 'youHadColitisYes',  'youHadColitis')" id="youHadColitisYes" ></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="youHadColitis_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.ColonoscopyHaveyoueverbeenTold}" styleClass="formLabel" id="chrohnDiseaseLbl" rendered="#{pc_Colonoscopy.showChrohnDiseaseLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.chrohnDiseaseNo}" onclick="toggleCBGroup(this.form.id, 'chrohnDiseaseNo',  'chrohnDisease')" id="chrohnDiseaseNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="chrohnDisease"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.chrohnDiseaseYes}" onclick="toggleCBGroup(this.form.id, 'chrohnDiseaseYes',  'chrohnDisease')" id="chrohnDiseaseYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="chrohnDisease_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Whendidtheysugg}" styleClass="formLabel" id="suggestNextColonoscopyLbl" rendered="#{pc_Colonoscopy.showSuggestNextColonoscopyLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="suggestNextColonoscopyDetails" onclick="launchDetailsPopUp('suggestNextColonoscopyDetails');" rendered="#{pc_Colonoscopy.showSuggestNextColonoscopyDetails}" styleClass="ovitalic#{pc_Colonoscopy.hassuggestNextColonoscopyDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td >
								<h:outputText style="width: 370px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveafamil}" styleClass="formLabel" id="familyHistoryOfColonLbl" rendered="#{pc_Colonoscopy.showFamilyHistoryOfColonLbl}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.familyHistoryOfColonNo}" onclick="toggleCBGroup(this.form.id, 'familyHistoryOfColonNo',  'familyHistoryOfColon')" id="familyHistoryOfColonNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="familyHistoryOfColon"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Colonoscopy.familyHistoryOfColonYes}" onclick="toggleCBGroup(this.form.id, 'familyHistoryOfColonYes',  'familyHistoryOfColon');if(document.getElementById(this.id).checked){launchDetailsPopUp('kindOfColonDetails')};" id="familyHistoryOfColonYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="familyHistoryOfColon_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="yesFamilyHistory" rendered="#{pc_Colonoscopy.showYesFamilyHistory}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.WhatkindCancerp}" styleClass="formLabel" style="width:300px;text-align:left;padding-bottom:10px;;margin-left: 15px" id="kindOfColonLbl" rendered="#{pc_Colonoscopy.showKindOfColonLbl}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="kindOfColonDetails" onclick="launchDetailsPopUp('kindOfColonDetails');" rendered="#{pc_Colonoscopy.showKindOfColonDetails}" styleClass="ovitalic#{pc_Colonoscopy.haskindOfColonDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Whoandwhatagesw}" styleClass="formLabel" style="width: 475px;text-align:left;margin-left:30px" id="familyMemberDetailsLbl" rendered="#{pc_Colonoscopy.showFamilyMemberDetailsLbl}"></h:outputText>
								<h:outputText value="#{componentBundle.MotherFatherBro}" styleClass="formLabel" style="width: 475px;text-align:left;margin-left:30px" id="familyMember" rendered="#{pc_Colonoscopy.showFamilyMember}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="familyMemberDetails" onclick="launchDetailsPopUp('familyMemberDetails');" rendered="#{pc_Colonoscopy.showFamilyMemberDetails}" styleClass="ovitalic#{pc_Colonoscopy.hasfamilyMemberDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="4"><hr/></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" style="height: 51px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancelButton" action="#{pc_Colonoscopy.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Colonoscopy.showCancelButton}" disabled="#{pc_Colonoscopy.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clearButton" action="#{pc_Colonoscopy.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Colonoscopy.showClearButton}" disabled="#{pc_Colonoscopy.disableClearButton}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="okButton" action="#{pc_Colonoscopy.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdColonoscopy}" disabled="#{pc_Colonoscopy.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_Colonoscopy.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdColonoscopy}" disabled="#{pc_Colonoscopy.disableOkButton}"></h:commandButton>
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 20px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
