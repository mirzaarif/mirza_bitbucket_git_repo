<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Foreign Travel</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_FOREIGNTRAVEL" value="#{pc_ForeignTravel}"></PopulateBean:Load>
			<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentIDBundle" basename="properties.nbaApplicationID"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_ForeignTravel.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_ForeignTravel.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="380">
							<col width="130">
								<col width="60">
									<col width="60"></col>
									<tr style="padding-top: 5px">
										<td class="sectionSubheader" colspan="4"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.ForeignTravel}" style="width: 220px" id="_0" rendered="#{pc_ForeignTravel.show_0}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 15px;">
										<td colspan="2"></td>
										<td colspan="2"></td>
									</tr>
									<tr style="padding-top: 5px">
										<td>
											<h:outputText style="width: 525px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatisyourcount}" styleClass="formLabel" id="labelOriginCountry" rendered="#{pc_ForeignTravel.showLabelOriginCountry}"></h:outputText>
										</td>
										<td align="left" colspan="3">
											<h:selectOneMenu style="width: 180px;" styleClass="formEntryText" value="#{pc_ForeignTravel.whatisyourcountryoforigin}" id="whatisyourcountryoforigin" rendered="#{pc_ForeignTravel.showWhatisyourcountryoforigin}" disabled="#{pc_ForeignTravel.disableWhatisyourcountryoforigin}">
												<f:selectItems value="#{pc_ForeignTravel.originCountryList}"></f:selectItems>
											</h:selectOneMenu>
										</td>
									</tr>
									<tr style="padding-top: 15px;">
										<td colspan="2"></td>
										<td></td>
										<td></td>
									</tr>
									<tr style="padding-top: 15px;">
										<td>
											<h:outputText style="width: 525px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatcountryarey}" styleClass="formLabel" id="labelCurrentCitizen" rendered="#{pc_ForeignTravel.showLabelCurrentCitizen}"></h:outputText>
										</td>
										<td align="left" colspan="2">
											<h:selectOneMenu style="width: 180px;" styleClass="formEntryText" value="#{pc_ForeignTravel.whatcountryareyouacurrentcitizenof}" id="whatcountryareyouacurrentcitizenof" rendered="#{pc_ForeignTravel.showWhatcountryareyouacurrentcitizenof}" disabled="#{pc_ForeignTravel.disableWhatcountryareyouacurrentcitizenof}">
												<f:selectItems value="#{pc_ForeignTravel.currentCountryList}"></f:selectItems>
											</h:selectOneMenu>
										</td>
										<td>											
											<h:commandButton id="currentcitizenDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('currentcitizenDetails');" style="margin-right:10px" rendered="#{pc_ForeignTravel.showCurrentcitizenDetails}" styleClass="ovitalic#{pc_ForeignTravel.hascurrentcitizenDetails}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td>
											<h:outputText value="#{componentBundle.IfnonUScitizend}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:15px;" id="labelUSVisaGreenCard" rendered="#{pc_ForeignTravel.showLabelUSVisaGreenCard}"></h:outputText>
										</td>
										<td style="width: 138px" align="left">
											<h:selectBooleanCheckbox value="#{pc_ForeignTravel.usVisaGreenCardNo}" id="usVisaGreenCardNo" onclick="toggleCBGroup(this.form.id, 'usVisaGreenCardNo',  'usVisaGreenCard')"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_ForeignTravel.usVisaGreenCardYes}" id="usVisaGreenCardYes" onclick="toggleCBGroup(this.form.id, 'usVisaGreenCardYes',  'usVisaGreenCard')"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<td></td>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Ifyeswhatisthet}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:30px;" id="labelVisaExpDate" rendered="#{pc_ForeignTravel.showLabelVisaExpDate}"></h:outputText>
							</td>
							<td align="left">								
								<h:selectOneMenu style="width: 120px;" styleClass="formEntryText" value="#{pc_ForeignTravel.usVisaDetail}" id="usVisaDetail" rendered="#{pc_ForeignTravel.showUSVisaDetail}" disabled="#{pc_ForeignTravel.disableUSVisaDetail}">
												<f:selectItems value="#{pc_ForeignTravel.visaTypeList}"></f:selectItems>
								</h:selectOneMenu>
							</td>
							<td colspan="2">
								<h:inputText style="width: 95px;" styleClass="formEntryText" id="usVisaExpDate" value="#{pc_ForeignTravel.usVisaExpDate}" rendered="#{pc_ForeignTravel.showUSVisaExpDate}" disabled="#{pc_ForeignTravel.disableUSVisaExpDate}">
								<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText style="width: 420px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatwasyourdate}" styleClass="formLabel" id="labelUSEntryDate" rendered="#{pc_ForeignTravel.showLabelUSEntryDate}"></h:outputText>
							</td>
							<td align="left">
								<h:inputText style="width: 95px;" styleClass="formEntryText" id="usEntryDate" value="#{pc_ForeignTravel.usEntryDate}" rendered="#{pc_ForeignTravel.showUSEntryDate}" disabled="#{pc_ForeignTravel.disableUSEntryDate}">
								<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="4" style="padding-top: 15px;">
								<h:outputText style="width: 602px;text-align:left;margin-left:5px;" value="#{componentBundle.Provideyourimme}" styleClass="formLabel" id="labelFamilyDetails" rendered="#{pc_ForeignTravel.showLabelFamilyDetails}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td align="left"></td>
							<td></td>
							<td>								
								<h:commandButton id="FamilyDetailsPopUp" image="images/circle_i.gif"  onclick="launchDetailsPopUp('FamilyDetailsPopUp');" style="margin-right:10px" rendered="#{pc_ForeignTravel.showFamilyDetailsPopUp}" styleClass="ovitalic#{pc_ForeignTravel.hasFamilyDetailsPopUp}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="4">
								<h:outputText style="width: 525px;text-align:left;margin-left:5px;" value="#{componentBundle.Inthepast2years}" styleClass="formLabel" id="labelTravelOutsideUS" rendered="#{pc_ForeignTravel.showLabelTravelOutsideUS}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td align="left">
								<h:selectBooleanCheckbox value="#{pc_ForeignTravel.travelOutsideUSYesNONo}" id="travelOutsideOfUsNo" onclick="toggleCBGroup(this.form.id, 'travelOutsideOfUsNo',  'travelOutsideOfUs')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ForeignTravel.travelOutsideUSYesNOYes}" id="travelOutsideOfUsYes" onclick="toggleCBGroup(this.form.id, 'travelOutsideOfUsYes',  'travelOutsideOfUs');if(document.getElementById(this.id).checked){launchDetailsPopUp('travelOutsideUsDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText styleClass="formLabel" style="width: 400px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyes}" id="labelIfYesTravelOutsideUS" rendered="#{pc_ForeignTravel.showLabelIfYesTravelOutsideUS}"></h:outputText>
								<h:outputText value="#{componentBundle.whatcountrywasv}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:30px;" id="labelCountryVisited" rendered="#{pc_ForeignTravel.showLabelCountryVisited}"></h:outputText>
							</td>
							<td align="left"></td>
							<td></td>
							<td>								
								<h:commandButton id="travelOutsideUsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('travelOutsideUsDetails');" style="margin-right:10px" rendered="#{pc_ForeignTravel.showTravelOutsideUsDetails}" styleClass="ovitalic#{pc_ForeignTravel.hastravelOutsideUsDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="4" style="padding-top: 15px;">
								<h:outputText style="width: 569px;text-align:left;margin-left:5px;" value="#{componentBundle.Inthenext12mont}" styleClass="formLabel" id="labelTravelOutsideUsInNext12months" rendered="#{pc_ForeignTravel.showLabelTravelOutsideUsInNext12months}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td align="left">
								<h:selectBooleanCheckbox value="#{pc_ForeignTravel.travelOutsideUsInNext12monthsYesNoNo}" id="travelOutsideUsInNext12monthsYesNoNo" onclick="toggleCBGroup(this.form.id, 'travelOutsideUsInNext12monthsYesNoNo',  'travelOutsideUsInNext12monthsYesNo')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ForeignTravel.travelOutsideUsInNext12monthsYesNoYes}" id="travelOutsideUsInNext12monthsYesNoYes" onclick="toggleCBGroup(this.form.id, 'travelOutsideUsInNext12monthsYesNoYes',  'travelOutsideUsInNext12monthsYesNo');if(document.getElementById(this.id).checked){launchDetailsPopUp('travelOutsideUsInNext12monthsDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:15px;" id="labelTravelOutsideUsIfYes" rendered="#{pc_ForeignTravel.showLabelTravelOutsideUsIfYes}"></h:outputText>
								<h:outputText value="#{componentBundle.whatcountryisto}" styleClass="formLabel" style="width: 425px;text-align:left;margin-left:30px;" id="labelTravelOutsideUsPlaceVisited" rendered="#{pc_ForeignTravel.showLabelTravelOutsideUsPlaceVisited}"></h:outputText>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="travelOutsideUsInNext12monthsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('travelOutsideUsInNext12monthsDetails');" style="margin-right:10px" rendered="#{pc_ForeignTravel.showTravelOutsideUsInNext12monthsDetails}" styleClass="ovitalic#{pc_ForeignTravel.hastravelOutsideUsInNext12monthsDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="2" style="height: 56px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancel" action="#{pc_ForeignTravel.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_ForeignTravel.showCancel}" disabled="#{pc_ForeignTravel.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clear" action="#{pc_ForeignTravel.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_ForeignTravel.showClear}" disabled="#{pc_ForeignTravel.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="ok" action="#{pc_ForeignTravel.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdForeignTravel}" disabled="#{pc_ForeignTravel.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="update" action="#{pc_ForeignTravel.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdForeignTravel}" disabled="#{pc_ForeignTravel.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
