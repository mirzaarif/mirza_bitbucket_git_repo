<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	
  <h:panelGroup id="paging" styleClass="ovDivPHIPagingData">
						    <h:commandLink id="paging_0" rendered="#{pc_PagingBottom.show_0}" action="#{pc_PagingBottom.actionPrevious}" >
								<h:outputText value="#{componentBundle.ltltPREV}" escape="false" styleClass="ovDivPHIPagingText" id="idPrevious" rendered="#{pc_PagingBottom.showIdPrevious}"></h:outputText>  <!-- NBA234 -->
							</h:commandLink>
							<h:commandLink id="paging_1" rendered="#{pc_PagingBottom.show_1}" action="#{pc_PagingBottom.actionPage1}" >
								<h:outputText value="#{componentBundle.Field1}" styleClass="ovDivPHIPagingText" id="idPage1"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field1}" rendered="#{!pc_PagingBottom.show_1}" styleClass="ovDivPHIPagingTextBold" id="idPage1_2"></h:outputText>	 <!-- NBA324 -->						
							<h:commandLink id="paging_2" rendered="#{pc_PagingBottom.show_2}" action="#{pc_PagingBottom.actionPage2}" >
								<h:outputText value="#{componentBundle.Field2}" styleClass="ovDivPHIPagingText" id="idPage2"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field2}" rendered="#{!pc_PagingBottom.show_2}" styleClass="ovDivPHIPagingTextBold" id="idPage2_2"></h:outputText> <!-- NBA324 -->
							
							<h:commandLink id="paging_3" rendered="#{pc_PagingBottom.show_3}" action="#{pc_PagingBottom.actionPage3}" >
								<h:outputText value="#{componentBundle.Field3}" styleClass="ovDivPHIPagingText" id="idPage3" ></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field3}" rendered="#{!pc_PagingBottom.show_3}" styleClass="ovDivPHIPagingTextBold" id="idPage3_2"></h:outputText> <!-- NBA324 -->
							
							<h:commandLink id="paging_4" rendered="#{pc_PagingBottom.show_4}" action="#{pc_PagingBottom.actionPage4}" >
								<h:outputText value="#{componentBundle.Field4}" styleClass="ovDivPHIPagingText" id="idPage4"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field4}" rendered="#{!pc_PagingBottom.show_4}" styleClass="ovDivPHIPagingTextBold" id="idPage4_2"></h:outputText>	 <!-- NBA324 -->						
							<h:commandLink id="paging_5" rendered="#{pc_PagingBottom.show_5}" action="#{pc_PagingBottom.actionPage5}" >
								<h:outputText value="#{componentBundle.Field5}" styleClass="ovDivPHIPagingText" id="idPage5" rendered="#{pc_PagingBottom.showIdPage5}"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field5}" rendered="#{!pc_PagingBottom.show_5}" styleClass="ovDivPHIPagingTextBold" id="idPage5_2"></h:outputText> <!-- NBA324 -->							
							<h:commandLink id="paging_6" rendered="#{pc_PagingBottom.show_6}" action="#{pc_PagingBottom.actionPage6}" >
								<h:outputText value="#{componentBundle.Field6}" styleClass="ovDivPHIPagingText" id="idPage6" rendered="#{pc_PagingBottom.showIdPage6}"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field6}" rendered="#{!pc_PagingBottom.show_6}" styleClass="ovDivPHIPagingTextBold" id="idPage6_2"></h:outputText> <!-- NBA324 -->							
							<h:commandLink id="paging_7" rendered="#{pc_PagingBottom.show_7}" action="#{pc_PagingBottom.actionPage7}">
								<h:outputText value="#{componentBundle.Field7}" styleClass="ovDivPHIPagingText" id="idPage7" rendered="#{pc_PagingBottom.showIdPage7}"></h:outputText> <!-- NBA324 -->
							</h:commandLink>
							<h:outputText value="#{componentBundle.Field7}" rendered="#{!pc_PagingBottom.show_7}" styleClass="ovDivPHIPagingTextBold" id="idPage7_2"></h:outputText> <!-- NBA324 -->							
							<h:commandLink id="paging_8" rendered="#{pc_PagingBottom.show_8}" action="#{pc_PagingBottom.actionNext}" >
								<h:outputText value="#{componentBundle.NEXTgtgt}" escape="false" styleClass="ovDivPHIPagingText" id="idNext" rendered="#{pc_PagingBottom.showIdNext}"></h:outputText> <!-- NBA234 -->
							</h:commandLink>
	</h:panelGroup>
</jsp:root>