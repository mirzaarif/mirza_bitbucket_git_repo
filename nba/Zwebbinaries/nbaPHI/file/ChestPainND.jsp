<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>chest Pain ND</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
		function valueChangeEvent() {	
			    getScrollXY('page');
				}
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_CHESTPAIN" value="#{pc_ChestPain}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_ChestPain.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_ChestPain.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="430">
							<col width="120">
								<col width="90">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.ChestPain}" style="width: 220px" id="chestPainQuestionnaire" rendered="#{pc_ChestPain.showChestPain}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="3">
											<h:outputText style="width:610px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverexpe}" styleClass="formLabel" id="experiencedChestPain" rendered="#{pc_ChestPain.showExperiencedChestPain}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td>
							<h:outputText></h:outputText>
							</td>
										<td align="left" colspan="2">
											<h:selectBooleanCheckbox value="#{pc_ChestPain.chestPainNo}" id="chestPainNo" onclick="toggleCBGroup(this.form.id, 'chestPainNo',  'chestPain',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" id="chestPain" style="width: 28px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_ChestPain.chestPainYes}" id="chestPainYes" onclick="toggleCBGroup(this.form.id, 'chestPainYes',  'chestPain',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('chestPainDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_ChestPain.collapseChestPainDetails}"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" id="chestPain_0" style="width: 28px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_ChestPain.expandChestPain}">
						<tr>
						<td>
							<h:outputText value="#{componentBundle.Ifso}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="chestPainLabel" rendered="#{pc_ChestPain.showChestPainLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Givedateduratio}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="chestPainDetailsLabel" rendered="#{pc_ChestPain.showChestPainDetailsLabel}"></h:outputText>
							</td>
							<td></td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" onclick="launchDetailsPopUp('chestPainDetails');" id="chestPainDetails" rendered="#{pc_ChestPain.showChestPainDetails}" styleClass="ovitalic#{pc_ChestPain.haschestPainDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Didthepainradia}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:30px" id="painDetailsLabel" rendered="#{pc_ChestPain.showPainDetailsLabel}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.painNo}" id="painNo" onclick="toggleCBGroup(this.form.id, 'painNo',  'pain')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="pain" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.painYes}" id="painYes" onclick="toggleCBGroup(this.form.id, 'painYes',  'pain');if(document.getElementById(this.id).checked){launchDetailsPopUp('painDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="pain_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="center" colspan="1">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="painDetails" onclick="launchDetailsPopUp('painDetails');" rendered="#{pc_ChestPain.showPainDetails}" styleClass="ovitalic#{pc_ChestPain.haspainDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 10px;">
							<td colspan="3">
								<h:outputText style="width:610px;text-align:left;margin-left:5px;" value="#{componentBundle.ChestPainHaveyouseenaphy}" styleClass="formLabel" id="hospitalizedForChestPainLabel" rendered="#{pc_ChestPain.showHospitalizedForChestPainLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left"> </td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.hospitalizedForChestPainNo}" id="hospitalizedForChestPainNo" onclick="toggleCBGroup(this.form.id, 'hospitalizedForChestPainNo',  'hospitalizedForChestPain',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="hospitalizedForChestPain" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.hospitalizedForChestPainYes}" id="hospitalizedForChestPainYes" onclick="toggleCBGroup(this.form.id, 'hospitalizedForChestPainYes',  'hospitalizedForChestPain',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('physicianDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_ChestPain.collapseHospitalizedDetails}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="hospitalizedForChestPain_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_ChestPain.expandHospitalizationDetails}">
						<tr>
							<td>
								<h:outputText value="#{componentBundle.Ifso}" styleClass="formLabel" style="width:300px;text-align:left;margin-left:15px;" id="hospitalizedIfYes" rendered="#{pc_ChestPain.showHospitalizedIfYes}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.ChestPainPleaseprovideth}" styleClass="formLabel" style="width: 590px;text-align:left;margin-left:30px" id="physicianDetailsLabel" rendered="#{pc_ChestPain.showPhysicianDetailsLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td></td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="physicianDetails" onclick="launchDetailsPopUp('physicianDetails');" rendered="#{pc_ChestPain.showPhysicianDetails}" styleClass="ovitalic#{pc_ChestPain.hasphysicianDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Wastestingrecom}" styleClass="formLabel" style="width: 590px;text-align:left;margin-left:30px" id="recommendedTestingLabel" rendered="#{pc_ChestPain.showRecommendedTestingLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.recommendedTestingNo}" id="recommendedTestingNo" onclick="toggleCBGroup(this.form.id, 'recommendedTestingNo',  'recommendedTesting')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="recommendedTesting" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.recommendedTestingYes}" id="recommendedTestingYes" onclick="toggleCBGroup(this.form.id, 'recommendedTestingYes',  'recommendedTesting');if(document.getElementById(this.id).checked){launchDetailsPopUp('recommendedTestingDetails')}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="recommendedTesting_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="recommendedTestingDetails" onclick="launchDetailsPopUp('recommendedTestingDetails');" rendered="#{pc_ChestPain.showRecommendedTestingDetails}" styleClass="ovitalic#{pc_ChestPain.hasrecommendedTestingDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Wasmedicationpr}" styleClass="formLabel" style="width: 590px;text-align:left;margin-left:30px" id="prescribedMedicationLabel" rendered="#{pc_ChestPain.showPrescribedMedicationLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.prescribedMedicationNo}" id="prescribedMedicationNo" onclick="toggleCBGroup(this.form.id, 'prescribedMedicationNo',  'prescribedMedication')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="prescribedMedication" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.prescribedMedicationYes}" id="prescribedMedicationYes" onclick="toggleCBGroup(this.form.id, 'prescribedMedicationYes',  'prescribedMedication');if(document.getElementById(this.id).checked){launchDetailsPopUp('prescribedMedicationDetails')}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="prescribedMedication_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="prescribedMedicationDetails" onclick="launchDetailsPopUp('prescribedMedicationDetails');" rendered="#{pc_ChestPain.showPrescribedMedicationDetails}" styleClass="ovitalic#{pc_ChestPain.hasprescribedMedicationDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputLabel value="#{componentBundle.Areyounowtaking}" style="width: 590px;text-align:left;margin-left:30px" styleClass="formLabel" id="takingMedicationLabel" rendered="#{pc_ChestPain.showTakingMedicationLabel}"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.takingMedicationNo}" id="takingMedicationNo" onclick="toggleCBGroup(this.form.id, 'takingMedicationNo',  'takingMedication')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="takingMedication" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.takingMedicationYes}"id="takingMedicationYes" onclick="toggleCBGroup(this.form.id, 'takingMedicationYes',  'takingMedication')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="takingMedication_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Wasrestrictiono}" styleClass="formLabel" style="width: 549px;text-align:left;margin-left:30px" id="restrictedActivitesLabel" rendered="#{pc_ChestPain.showRestrictedActivitesLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_ChestPain.restrictedActivitesNo}" id="restrictedActivitesNo" onclick="toggleCBGroup(this.form.id, 'restrictedActivitesNo',  'restrictedActivites')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 28px" id="restrictedActivites" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_ChestPain.restrictedActivitesYes}" id="restrictedActivitesYes" onclick="toggleCBGroup(this.form.id, 'restrictedActivitesYes',  'restrictedActivites');if(document.getElementById(this.id).checked){launchDetailsPopUp('restrictedActivitesDetails')}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 28px" id="restrictedActivites_0" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="restrictedActivitesDetails" onclick="launchDetailsPopUp('restrictedActivitesDetails');" rendered="#{pc_ChestPain.showRestrictedActivitesDetails}" styleClass="ovitalic#{pc_ChestPain.hasrestrictedActivitesDetails}"></h:commandButton>
							</td>
						</tr>
						</col>
							</col>
						</col>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 10px;">
							<td colspan="2">
								<h:outputText style="width:610px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="additionalDetailsLabel" rendered="#{pc_ChestPain.showAdditionalDetailsLabel}"></h:outputText>
							</td>
							<td align="center">
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="additionalDetails" rendered="#{pc_ChestPain.showAdditionalDetails}" onclick="launchDetailsPopUp('additionalDetails');" styleClass="ovitalic#{pc_ChestPain.hasadditionalDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancelButton" action="#{pc_ChestPain.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_ChestPain.showCancelButton}" disabled="#{pc_ChestPain.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clearButton" action="#{pc_ChestPain.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_ChestPain.showClearButton}" disabled="#{pc_ChestPain.disableClearButton}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="okButton" action="#{pc_ChestPain.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdChestPain}" disabled="#{pc_ChestPain.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_ChestPain.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdChestPain}" disabled="#{pc_ChestPain.disableOkButton}"></h:commandButton>
							</td>							
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" align="left"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
