<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>arthritis IN</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_ARTHRITIS" value="#{pc_Arthritis}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Arthritis.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Arthritis.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px;" cellpadding="0" cellspacing="0" style="cellpadding: 0px; cellspacing: 0px;table-layout:fixed">
						<col width="410">
							<col width="135">
								<col width="85">
									<tr style="padding-top:5px;">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Arthritis}" style="width: 220px" id="arthritis" rendered="#{pc_Arthritis.showArthritis}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="3" style="padding-top: 15px;">
											<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.ArthritisInthepast5years}" styleClass="formLabel" id="arthritisQ1Label" rendered="#{pc_Arthritis.showArthritisQ1Label}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td></td>
										<td>
											<h:commandButton image="images/circle_i.gif" id="arthritisQ1Detail" onclick="launchDetailsPopUp('arthritisQ1Detail');" rendered="#{pc_Arthritis.showArthritisQ1Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ1Detail}"></h:commandButton>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="2" style="padding-top: 15px;">
											<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.ArthritisHowoftenhaveyou}" styleClass="formLabel" id="arthritisQ2Label" rendered="#{pc_Arthritis.showArthritisQ2Label}"></h:outputText>
										</td>
										<td>
											<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ2Detail" onclick="launchDetailsPopUp('arthritisQ2Detail');" rendered="#{pc_Arthritis.showArthritisQ2Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ2Detail}"></h:commandButton>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="2" style="padding-top: 15px;">
											<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.Whichjointsareo}" styleClass="formLabel" id="arthritisQ3Label" rendered="#{pc_Arthritis.showArthritisQ3Label}"></h:outputText>
										</td>
										<td>
											<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ3Detail" onclick="launchDetailsPopUp('arthritisQ3Detail');" rendered="#{pc_Arthritis.showArthritisQ3Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ3Detail}"></h:commandButton>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="3" style="padding-top: 15px;">
											<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.Hasyourconditio}" styleClass="formLabel" id="arthritisQ4Label" rendered="#{pc_Arthritis.showArthritisQ4Label}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td></td>
										<td>
											<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ4Detail" onclick="launchDetailsPopUp('arthritisQ4Detail');" rendered="#{pc_Arthritis.showArthritisQ4Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ4Detail}"></h:commandButton>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="2" style="padding-top: 15px;">
											<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.ArthritisHaveyouseenaphyIN}" styleClass="formLabel" id="arthritisQ5Label" rendered="#{pc_Arthritis.showArthritisQ5Label}"></h:outputText>
										</td>
										<td></td>
									</tr>
									<tr style="padding-top:5px;">
										<td></td>
										<td>
											<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB1No}" onclick="toggleCBGroup(this.form.id, 'arthritisCB1No',  'arthritisCB1',true);valueChangeEvent();" id="arthritisCB1No"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB1"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB1Yes}" onclick="toggleCBGroup(this.form.id, 'arthritisCB1Yes',  'arthritisCB1',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('arthritisQ5ADetail')};valueChangeEvent();submit();" id="arthritisCB1Yes" valueChangeListener="#{pc_Arthritis.expandArthritisInfo}"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB1_0"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Arthritis.expandArthritisSeenPhysician}">
						<tr style="padding-top:5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 615px;text-align:left;margin-left:5px;" id="arthritisQ5ALabel" rendered="#{pc_Arthritis.showArthritisQ5ALabel}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.ArthritisPleaseprovideth}" styleClass="formLabel" style="width: 341px;text-align:left;margin-left:15px;" id="arthritisQ5A1Label" rendered="#{pc_Arthritis.showArthritisQ5A1Label}"></h:outputText>
								<h:outputText value="#{componentBundle.andthetimeofthe}" styleClass="formLabel" style="width: 341px;text-align:left;margin-left:15px;" id="arthritisQ5A2Label" rendered="#{pc_Arthritis.showArthritisQ5A2Label}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ5ADetail" onclick="launchDetailsPopUp('arthritisQ5ADetail');" rendered="#{pc_Arthritis.showArthritisQ5ADetail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ5ADetail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Whattestingwasr}" styleClass="formLabel" style="width: 341px;text-align:left;margin-left:15px;" id="arthritisQ5BLabel" rendered="#{pc_Arthritis.showArthritisQ5BLabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ5BDetail" onclick="launchDetailsPopUp('arthritisQ5BDetail');" rendered="#{pc_Arthritis.showArthritisQ5BDetail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ5BDetail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Wherewerethetes}" styleClass="formLabel" style="width: 369px;text-align:left;margin-left:15px;" id="arthritisQ5B1Label" rendered="#{pc_Arthritis.showArthritisQ5B1Label}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ5B2Detail" onclick="launchDetailsPopUp('arthritisQ5B2Detail');" rendered="#{pc_Arthritis.showArthritisQ5B2Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ5B2Detail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Whattreatementa}" styleClass="formLabel" style="width: 369px;text-align:left;margin-left:15px;" id="arthritisQ5CLabel" rendered="#{pc_Arthritis.showArthritisQ5CLabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ5CDetail" onclick="launchDetailsPopUp('arthritisQ5CDetail');" rendered="#{pc_Arthritis.showArthritisQ5CDetail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ5CDetail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Wereyouractivit}" styleClass="formLabel" style="width: 369px;text-align:left;margin-left:15px;" id="arthritisQ5DLabel" rendered="#{pc_Arthritis.showArthritisQ5DLabel}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB2No}" onclick="toggleCBGroup(this.form.id, 'arthritisCB2No',  'arthritisCB2')" id="arthritisCB2No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB2"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB2Yes}" onclick="toggleCBGroup(this.form.id, 'arthritisCB2Yes',  'arthritisCB2');if(document.getElementById(this.id).checked){launchDetailsPopUp('arthritisQ5DDetail')};" id="arthritisCB2Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB2_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ5DDetail" onclick="launchDetailsPopUp('arthritisQ5DDetail');" rendered="#{pc_Arthritis.showArthritisQ5DDetail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ5DDetail}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top:5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.Doestheconditio}" styleClass="formLabel" id="arthritisQ6Label" rendered="#{pc_Arthritis.showArthritisQ6Label}"></h:outputText>
							</td>
							<td></td>
						</tr>						
						<tr style="padding-top:5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB3No}" onclick="toggleCBGroup(this.form.id, 'arthritisCB3No',  'arthritisCB3')" id="arthritisCB3No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB3"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB3Yes}" onclick="toggleCBGroup(this.form.id, 'arthritisCB3Yes',  'arthritisCB3');if(document.getElementById(this.id).checked){launchDetailsPopUp('arthritisQ6Detail')};" id="arthritisCB3Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB3_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ6Detail" onclick="launchDetailsPopUp('arthritisQ6Detail');" rendered="#{pc_Arthritis.showArthritisQ6Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ6Detail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.ArthritisDoyounoworhavey}" styleClass="formLabel" id="arthritisQ7Label" rendered="#{pc_Arthritis.showArthritisQ7Label}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB4No}" onclick="toggleCBGroup(this.form.id, 'arthritisCB4No',  'arthritisCB4')" id="arthritisCB4No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB4"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Arthritis.arthritisCB4Yes}" onclick="toggleCBGroup(this.form.id, 'arthritisCB4Yes',  'arthritisCB4');if(document.getElementById(this.id).checked){launchDetailsPopUp('arthritisQ7Detail')};" id="arthritisCB4Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="arthritisCB4_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" style="width: 240px;text-align:left;padding-bottom:10px;;margin-left: 12px" id="arthritisQ7ALabel" rendered="#{pc_Arthritis.showArthritisQ7ALabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ7Detail"  onclick="launchDetailsPopUp('arthritisQ7Detail');" rendered="#{pc_Arthritis.showArthritisQ7Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ7Detail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 615px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="arthritisQ8Label" rendered="#{pc_Arthritis.showArthritisQ8Label}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="arthritisQ8Detail" onclick="launchDetailsPopUp('arthritisQ8Detail');" rendered="#{pc_Arthritis.showArthritisQ8Detail}" styleClass="ovitalic#{pc_Arthritis.hasarthritisQ8Detail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td  align="left" style="height: 48px">
								<h:commandButton value="Cancel" styleClass="formButton" action="#{pc_Arthritis.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="cancelButton" rendered="#{pc_Arthritis.showCancelButton}" disabled="#{pc_Arthritis.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="formButton" action="#{pc_Arthritis.clearAction}" onclick="resetTargetFrame(this.form.id);" id="clearButton" rendered="#{pc_Arthritis.showClearButton}" disabled="#{pc_Arthritis.disableClearButton}"></h:commandButton>
							</td>
							<td colspan="2">
								<h:commandButton value="OK" styleClass="formButton" action="#{pc_Arthritis.okAction}" onclick="resetTargetFrame(this.form.id);" id="okButton" rendered="#{!pc_Links.showIdArthritis}" disabled="#{pc_Arthritis.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="formButton" action="#{pc_Arthritis.okAction}" onclick="resetTargetFrame(this.form.id);" id="updateButton" rendered="#{pc_Links.showIdArthritis}" disabled="#{pc_Arthritis.disableOkButton}"></h:commandButton>
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 20px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
