<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>details</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/help.js"></script>
<script type="text/javascript" src="javascript/global/RoboHelp_CSH.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script type="text/javascript">
            var width=555;
            var height=350;
			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function showAccelHelp(helpContextId, helpPrefix) {
				if(helpContextId == null || helpContextId == ""){
					 // no valid context specified so launch default
					helpContextId = 0;
				}
				if(RH_ShowHelp != null){
					if(helpContextId == 0){
						RH_ShowHelp(0, helpPrefix + ">" + "", HH_DISPLAY_TOPIC, 0);
					} else {
						RH_ShowHelp(0, helpPrefix + ">" + "", HH_HELP_CONTEXT, helpContextId);		
					}
				} else {
					alert("nohelp");
				}
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="popupInit();scrollToCoordinates('details');">
		<f:view>	
		<PopulateBean:Load serviceName="RETRIEVE_DETAILS" value="#{pc_Details}"></PopulateBean:Load>		
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" style="height:450px" id="details">
				<h:inputHidden id="scrollx" value="#{pc_Details.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Details.scrollYC}"></h:inputHidden>
				<h:outputLabel value="#{componentBundle.Details}" style="width:608px" styleClass="sectionSubheader"></h:outputLabel> <!-- NBA324 -->
				<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
					<tbody>
						<tr >
							<td></td>
							<td align="right">
							<SCRIPT>defaultHelpContext = <c:out value="${helpContextId}"/>;
</SCRIPT><IMG src='images/help.gif' onclick="showAccelHelp(<c:out value="${helpContextId}"/>, '././nbaPHI/helpWhy/!SSL!/WebHelp/phiHelp.htm');" title='Help' style='cursor: hand ! IMPORTANT;border:none;' />
								<SCRIPT>defaultHelpContext = <c:out value="${whyContextId}"/>;
</SCRIPT><IMG src='images/why.gif' onclick="showAccelHelp(<c:out value="${whyContextId}"/>, '././nbaPHI/helpWhy/!SSL!/WebHelp/phiHelp.htm');" title='Why' style='cursor: hand ! IMPORTANT;border:none;' />								  
							</td>
						</tr>
						<tr>
							<td width="100%" colspan="2">
								<table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
									<tbody>
										<tr style="padding-top: 25px; padding-bottom: 15px;">
											<td>
												<h:inputTextarea style="width: 520px; height: 230px;margin-left:15px;" id="id_84ind9gv" value="#{pc_Details.detailstext}" rendered="#{pc_Details.showId_84ind9gv}" disabled="#{pc_Details.disableId_84ind9gv}">  <!-- NBA324 -->
												</h:inputTextarea>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							
						</tr>
						<tr valign="bottom">
							<td colspan="2" align="left">
								<h:commandButton styleClass="buttonLeft" value="Cancel" action="#{pc_Details.actionCancel}" ></h:commandButton>
								<h:commandButton styleClass="buttonLeft-1" value="Clear" action="#{pc_Details.actionClear}" ></h:commandButton>
								<h:commandButton styleClass="buttonRight" value="OK"  action="#{pc_Details.actionOK}" ></h:commandButton>
							</td>
						</tr>
					</tbody>
				</table>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
			<script type="text/javascript">
				document.getElementById('details:id_84ind9gv').focus();
			</script>
</body>
</html>
