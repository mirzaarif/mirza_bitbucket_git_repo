<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>SleepApnea</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">
			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
                getScrollXY('page');			
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			function valueChangeEvent() {	
			    getScrollXY('page');
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_SLEEPAPNEA" value="#{pc_SleepApnea}"></PopulateBean:Load>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentIDBundle" basename="properties.nbaApplicationID"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_SleepApnea.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_SleepApnea.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="330">
							<col width="200">
								<col width="100">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.SleepApnea}" style="width: 220px" id="sleepapnea" rendered="#{pc_SleepApnea.showSleepapnea}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="2" align="left"></td>
										<td></td>
									</tr>
									<tr style="padding-top: 5px;">
										<td align="left" colspan="1">
											<h:outputText style="width: 298px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverhadasa}" styleClass="formLabel" id="sleepstudy" rendered="#{pc_SleepApnea.showSleepstudy}"></h:outputText>
										</td>
										<td style="margin-left: 10px;">
											<h:selectBooleanCheckbox value="#{pc_SleepApnea.sleepStudyYNNo}" id="sleepStudyYNNo" onclick="toggleCBGroup(this.form.id, 'sleepStudyYNNo',  'sleepStudyYN',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_SleepApnea.sleepStudyYNYes}" id="sleepStudyYNYes" valueChangeListener="#{pc_SleepApnea.collapseSleepStudyDetail}" onclick="toggleCBGroup(this.form.id, 'sleepStudyYNYes',  'sleepStudyYN',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('sleepapneahowmanytimesinfo')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ify" rendered="#{pc_SleepApnea.expandSleepStudyDetailsInd}">
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Howmanytimes}" styleClass="formLabel" id="howmanytimes" rendered="#{pc_SleepApnea.showHowmanytimes}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneahowmanytimesinfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('sleepapneahowmanytimesinfo');" rendered="#{pc_SleepApnea.showHowmanytimesinfo}" disabled="#{pc_SleepApnea.disableHowmanytimesinfo}" styleClass="ovitalic#{pc_SleepApnea.hasHowManyTimesDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Whenwerethey}" styleClass="formLabel" id="whenwerethey" rendered="#{pc_SleepApnea.showWhenwerethey}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneawhenweretheyinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneawhenweretheyinfo');" rendered="#{pc_SleepApnea.showWhenweretheyinfo}" disabled="#{pc_SleepApnea.disableWhenweretheyinfo}" styleClass="ovitalic#{pc_SleepApnea.hasWhenWereTheyDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Whenwasyourmost}" styleClass="formLabel" id="mostrecent" rendered="#{pc_SleepApnea.showMostrecent}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneamostrecentinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneamostrecentinfo');" rendered="#{pc_SleepApnea.showMostrecentinfo}" disabled="#{pc_SleepApnea.disableMostrecentinfo}" styleClass="ovitalic#{pc_SleepApnea.hasMostRecentDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.WhatwastheDrnam}" styleClass="formLabel" id="doctorname" rendered="#{pc_SleepApnea.showDoctorname}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneadoctornameinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneadoctornameinfo');" rendered="#{pc_SleepApnea.showDoctornameinfo}" disabled="#{pc_SleepApnea.disableDoctornameinfo}" styleClass="ovitalic#{pc_SleepApnea.hasDoctorNameDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px;">
							<td colspan="2" align="left">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatwereyoursym}" styleClass="formLabel" id="symptomsid" rendered="#{pc_SleepApnea.showSymptomsid}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneasymptomsinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneasymptomsinfo');" rendered="#{pc_SleepApnea.showSymptomsinfo}" disabled="#{pc_SleepApnea.disableSymptomsinfo}" styleClass="ovitalic#{pc_SleepApnea.hasSymptomsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" align="left">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Snoringspousesa}" styleClass="formLabel" id="snoringid" rendered="#{pc_SleepApnea.showSnoringid}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3" align="left"></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td align="left" colspan="2">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatwasthediagn}" styleClass="formLabel" id="diagnosisid" rendered="#{pc_SleepApnea.showDiagnosisid}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneadiagnosisinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneadiagnosisinfo');" rendered="#{pc_SleepApnea.showDiagnosisinfo}" disabled="#{pc_SleepApnea.disableDiagnosisinfo}" styleClass="ovitalic#{pc_SleepApnea.hasDiagnosisDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3" align="left">
								<h:outputText style="width: 494px;text-align:left;margin-left:5px;" value="#{componentBundle.Didtheydescribe}" styleClass="formLabel" id="severesleepapnea" rendered="#{pc_SleepApnea.showSeveresleepapnea}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left"></td>
							<td>
								<h:selectOneMenu style="width: 160px" styleClass="formEntryTextHalf" id="severesleepapneavalues" value="#{pc_SleepApnea.severeSleepApneaValues}" rendered="#{pc_SleepApnea.showSeveresleepapneavalues}" disabled="#{pc_SleepApnea.disableSeveresleepapneavalues}">
									<f:selectItems value="#{pc_SleepApnea.severesleepapneaList}"></f:selectItems>
								</h:selectOneMenu>
							</td>
							<td>
								<h:commandButton id="sleepapneaseveresleepapneainfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneaseveresleepapneainfo');" rendered="#{pc_SleepApnea.showSeveresleepapneainfo}" disabled="#{pc_SleepApnea.disableSeveresleepapneainfo}" styleClass="ovitalic#{pc_SleepApnea.hasSeverSleepApneaDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2" align="left">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatwasthesugge}" styleClass="formLabel" id="suggestedtreatment" rendered="#{pc_SleepApnea.showSuggestedtreatment}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneasuggestedtreatmentinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneasuggestedtreatmentinfo');" rendered="#{pc_SleepApnea.showSuggestedtreatmentinfo}" disabled="#{pc_SleepApnea.disableSuggestedtreatmentinfo}" styleClass="ovitalic#{pc_SleepApnea.hasSuggestTreatmentDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" align="left">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.SurgeryCPAPweig}" styleClass="formLabel" id="surgeryid" rendered="#{pc_SleepApnea.showSurgeryid}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3" align="left"></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td align="left" colspan="2">
								<h:outputText style="width: 530px;text-align:left;margin-left:5px;" value="#{componentBundle.Ifrecommendtous}" styleClass="formLabel" id="cpaprecommended" rendered="#{pc_SleepApnea.showCpaprecommended}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneacpaprecommendedinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneacpaprecommendedinfo');" rendered="#{pc_SleepApnea.showCpaprecommendedinfo}" disabled="#{pc_SleepApnea.disableCpaprecommendedinfo}" styleClass="ovitalic#{pc_SleepApnea.hasRecommendedDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Doyouhaveanypro}" styleClass="formLabel" id="problemsid" rendered="#{pc_SleepApnea.showProblemsid}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneaproblemsinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneaproblemsinfo');" rendered="#{pc_SleepApnea.showProblemsinfo}" disabled="#{pc_SleepApnea.disableProblemsinfo}" styleClass="ovitalic#{pc_SleepApnea.hasProblemsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Diditclearupyou}" styleClass="formLabel" id="clearupsymptoms" rendered="#{pc_SleepApnea.showClearupsymptoms}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneaclearupsymptomsinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneaclearupsymptomsinfo');" rendered="#{pc_SleepApnea.showClearupsymptomsinfo}" disabled="#{pc_SleepApnea.disableClearupsymptomsinfo}" styleClass="ovitalic#{pc_SleepApnea.hasClearUpSymptomsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 275px;text-align:left;margin-left:30px;" value="#{componentBundle.Areyouplanningo}" styleClass="formLabel" id="planningtocontinue" rendered="#{pc_SleepApnea.showPlanningtocontinue}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="sleepapneaplanningtocontinueinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneaplanningtocontinueinfo');" rendered="#{pc_SleepApnea.showPlanningtocontinueinfo}" disabled="#{pc_SleepApnea.disablePlanningtocontinueinfo}" styleClass="ovitalic#{pc_SleepApnea.hasPlanningToContinueDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" align="left"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 277px;text-align:left;margin-left:5px;" value="#{componentBundle.Didtheyrecommen}" styleClass="formLabel" id="recommendsurgery" rendered="#{pc_SleepApnea.showRecommendsurgery}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.recommendsurgeryYNNo}" id="recommendsurgeryYNNo" onclick="toggleCBGroup(this.form.id, 'recommendsurgeryYNNo',  'recommendsurgeryYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.recommendsurgeryYNYes}" id="recommendsurgeryYNYes" onclick="toggleCBGroup(this.form.id, 'recommendsurgeryYNYes',  'recommendsurgeryYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText style="width: 574px;text-align:left;margin-left:30px;" value="#{componentBundle.Didyouhavethisd}" styleClass="formLabel" id="surgeryinfutureid" rendered="#{pc_SleepApnea.showSurgeryinfutureid}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.surgeryinfutureYNNo}" id="surgeryinfutureYNNo" onclick="toggleCBGroup(this.form.id, 'surgeryinfutureYNNo',  'surgeryinfutureYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.surgeryinfutureYNYes}" id="surgeryinfutureYNYes" onclick="toggleCBGroup(this.form.id, 'surgeryinfutureYNYes',  'surgeryinfutureYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('sleepapneasurgeryinfuturewheninfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left" colspan="2">
								<h:outputText style="width: 574px;text-align:left;margin-left:30px;" value="#{componentBundle.When}" styleClass="formLabel" id="surgeryinfuturewhen" rendered="#{pc_SleepApnea.showSurgeryinfuturewhen}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneasurgeryinfuturewheninfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneasurgeryinfuturewheninfo');" rendered="#{pc_SleepApnea.showSurgeryinfuturewheninfo}" disabled="#{pc_SleepApnea.disableSurgeryinfuturewheninfo}" styleClass="ovitalic#{pc_SleepApnea.hasSurgeryInFutureDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3" align="left"></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td align="left" colspan="2">
								<h:outputText style="width: 527px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveanyfol}" styleClass="formLabel" id="visitsappointmentssleepstudies" rendered="#{pc_SleepApnea.showVisitsappointmentssleepstudies}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.visitsappointmentssleepstudiesYNNo}" id="visitsappointmentssleepstudiesYNNo" onclick="toggleCBGroup(this.form.id, 'visitsappointmentssleepstudiesYNNo',  'visitsappointmentssleepstudiesYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_SleepApnea.visitsappointmentssleepstudiesYNYes}" id="visitsappointmentssleepstudiesYNYes" onclick="toggleCBGroup(this.form.id, 'visitsappointmentssleepstudiesYNYes',  'visitsappointmentssleepstudiesYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('sleepapneavisitsappointmentssleepstudieswheninfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputText style="width: 574px;text-align:left;margin-left:30px;" value="#{componentBundle.When}" styleClass="formLabel" id="visitsappointmentssleepstudieswhen" rendered="#{pc_SleepApnea.showVisitsappointmentssleepstudieswhen}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneavisitsappointmentssleepstudieswheninfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('sleepapneavisitsappointmentssleepstudieswheninfo');" rendered="#{pc_SleepApnea.showVisitsappointmentssleepstudieswheninfo}" disabled="#{pc_SleepApnea.disableVisitsappointmentssleepstudieswheninfo}" styleClass="ovitalic#{pc_SleepApnea.hasVisitAppoinmentSleepStudiesDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3" align="left"></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td align="left" colspan="2">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Arethereanyaddi}" styleClass="formLabel" id="additionalcomments" rendered="#{pc_SleepApnea.showAdditionalcomments}"></h:outputText>
							</td>
							<td>
								<h:commandButton id="sleepapneaadditionalcommentsinfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('sleepapneaadditionalcommentsinfo');" rendered="#{pc_SleepApnea.showAdditionalcommentsinfo}" disabled="#{pc_SleepApnea.disableAdditionalcommentsinfo}" styleClass="ovitalic#{pc_SleepApnea.hasAdditionalCommentsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_SleepApnea.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="cancel" rendered="#{pc_SleepApnea.showCancel}" disabled="#{pc_SleepApnea.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_SleepApnea.clearAction}" onclick="resetTargetFrame(this.form.id);" id="clear" rendered="#{pc_SleepApnea.showClear}" disabled="#{pc_SleepApnea.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_SleepApnea.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok" rendered="#{!pc_Links.showIdSleepApnea}" disabled="#{pc_SleepApnea.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_SleepApnea.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok222" rendered="#{pc_Links.showIdSleepApnea}" disabled="#{pc_SleepApnea.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 40px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
