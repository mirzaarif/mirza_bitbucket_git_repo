<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Driving</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}

			//-->
		
         


</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_DRIVING" value="#{pc_Driving}"></PopulateBean:Load>			
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Driving.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Driving.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="400">
							<col width="170">
								<col width="60">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Driving}" style="width: 220px" id="driving" rendered="#{pc_Driving.showDriving}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td style="padding-top: 15px;">
											<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Doyoucurrentlyh}" styleClass="formLabel" id="dolicense" rendered="#{pc_Driving.showDolicense}"></h:outputText>
										</td>
										<td style="padding-top: 15px;">
											<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo}" id="prefixNo" onclick="toggleCBGroup(this.form.id, 'prefixNo',  'prefix');"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes}" id="prefixYes" onclick="toggleCBGroup(this.form.id, 'prefixYes',  'prefix');if(document.getElementById(this.id).checked){launchDetailsPopUp('validlicense')};"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 387px;text-align:left;margin-left:30px" value="#{componentBundle.Ifnoexplain}" styleClass="formLabel" id="ifexplain" rendered="#{pc_Driving.showIfexplain}"></h:outputText>
							</td>
							<td></td>
							<td>
							    <h:commandButton id="validlicense" image="images/circle_i.gif"  onclick="launchDetailsPopUp('validlicense');" style="margin-right:10px;" rendered="#{pc_Driving.show1}" styleClass="ovitalic#{pc_Driving.hasvalidlicense}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;" valign="top;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Whatadditionale}" styleClass="formLabel" id="whatany" rendered="#{pc_Driving.showWhatany}"></h:outputText>
							</td>
							<td style="padding-top: 15px;" colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Driving.none}" id="none" rendered="#{pc_Driving.showNone}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:motorcycle').checked=false;document.getElementById('page:other').checked=false;};" disabled="#{pc_Driving.disableNone}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.None}" styleClass="formLabelRight" style="margin-right:100px;" id="_0" rendered="#{pc_Driving.show_0}"></h:outputLabel>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Driving.motorcycle}" id="motorcycle" rendered="#{pc_Driving.showMotorcycle}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:none').checked=false;document.getElementById('page:other').checked=false;};" disabled="#{pc_Driving.disableMotorcycle}"></h:selectBooleanCheckbox>
							    <h:outputLabel value="#{componentBundle.Motorcycle}" styleClass="formLabelRight" style="margin-right:20px;" id="_1" rendered="#{pc_Driving.show_1}"></h:outputLabel>
							
							</td>
						<td></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Driving.other}" id="other" rendered="#{pc_Driving.showOther}" onclick="if(document.getElementById(this.id).checked){document.getElementById('page:none').checked=false;document.getElementById('page:motorcycle').checked=false;};" disabled="#{pc_Driving.disableOther}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Other}" styleClass="formLabelRight" style="margin-right:100px;" id="_2" rendered="#{pc_Driving.show_2}"></h:outputLabel>
							</td>
							<td>
							     <h:commandButton id="license" image="images/circle_i.gif"  onclick="launchDetailsPopUp('license');" style="margin-right:10px;" rendered="#{pc_Driving.show2}" styleClass="ovitalic#{pc_Driving.haslicense}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText value="#{componentBundle.Whatpercentofti}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:5px" id="whatbelt" rendered="#{pc_Driving.showWhatbelt}"></h:outputText>
								<h:outputText value="#{componentBundle.askexactlyaswri}" styleClass="formLabelRight" style="width: 400px;text-align:left;margin-left:5px" id="askwritten" rendered="#{pc_Driving.showAskwritten}"></h:outputText>
							</td>
							<td style="padding-top: 15px;"></td>
							<td>
							     <h:commandButton id="wear" image="images/circle_i.gif"  onclick="launchDetailsPopUp('wear');" style="margin-right:10px;" rendered="#{pc_Driving.show3}" styleClass="ovitalic#{pc_Driving.haswear}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText value="#{componentBundle.Haveyoueverhada}" styleClass="formLabel" style="width: 400px;text-align:left;margin-left:5px" id="haveviolation" rendered="#{pc_Driving.showHaveviolation}"></h:outputText>
							</td>
							<td colspan="1" style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_1}" id="prefix_1No" onclick="toggleCBGroup(this.form.id, 'prefix_1No',  'prefix_1')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_1}" id="prefix_1Yes" onclick="toggleCBGroup(this.form.id, 'prefix_1Yes',  'prefix_1');if(document.getElementById(this.id).checked){launchDetailsPopUp('violation')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 387px;text-align:left;margin-left:30px" value="#{componentBundle.Ifyeswhen}" styleClass="formLabel" id="ifyes" rendered="#{pc_Driving.showIfyes}"></h:outputText>
							</td>
							<td></td>
							<td>
							     <h:commandButton id="violation" image="images/circle_i.gif"  onclick="launchDetailsPopUp('violation');" style="margin-right:10px;" rendered="#{pc_Driving.show4}" styleClass="ovitalic#{pc_Driving.hasviolation}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText style="width: 600px;text-align:left;margin-left:5px" value="#{componentBundle.Haveyoubeenchar}" styleClass="formLabel" id="haveyears" rendered="#{pc_Driving.showHaveyears}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left" style="padding-top: 5px;"></td>
							<td colspan="1" style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_2}" id="prefix_2No" onclick="toggleCBGroup(this.form.id, 'prefix_2No',  'prefix_2')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_2}" id="prefix_2Yes" onclick="toggleCBGroup(this.form.id, 'prefix_2Yes',  'prefix_2');if(document.getElementById(this.id).checked){launchDetailsPopUp('charged')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.ifyesexplaindriving}" styleClass="formLabel" style="width: 351px;text-align:left;margin-left:60px" id="ifexplain_0" rendered="#{pc_Driving.showIfexplain_0}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
							     <h:commandButton id="charged" image="images/circle_i.gif"  onclick="launchDetailsPopUp('charged');" style="margin-right:10px;" rendered="#{pc_Driving.show5}" styleClass="ovitalic#{pc_Driving.hascharged}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td align="left" style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Anyothermovingv}" styleClass="formLabel" id="anyyears" rendered="#{pc_Driving.showAnyyears}"></h:outputText>
							</td>
							<td colspan="1" style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_3}" id="prefix_3No" onclick="toggleCBGroup(this.form.id, 'prefix_3No',  'prefix_3')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_3}" id="prefix_3Yes" onclick="toggleCBGroup(this.form.id, 'prefix_3Yes',  'prefix_3');if(document.getElementById(this.id).checked){launchDetailsPopUp('moving')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifyeswhatwasthe}" styleClass="formLabel" id="ifwhen" style="width: 387px;text-align:left;margin-left:30px" rendered="#{pc_Driving.showIfwhen}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
							     <h:commandButton id="moving" image="images/circle_i.gif"  onclick="launchDetailsPopUp('moving');" style="margin-right:10px;" rendered="#{pc_Driving.show6}" styleClass="ovitalic#{pc_Driving.hasmoving}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Ifnomotorcyclee}" id="ifquestions" styleClass="formLabelRight" style="width: 602px;text-align:left;margin-left:5px" rendered="#{pc_Driving.showIfquestions}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Doyouownorfrequ}" styleClass="formLabel" id="domotorcycle" rendered="#{pc_Driving.showDomotorcycle}"></h:outputText>
							</td>
							<td style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_4}" id="prefix_4No" onclick="toggleCBGroup(this.form.id, 'prefix_4No',  'prefix_4')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_4}" id="prefix_4Yes" onclick="toggleCBGroup(this.form.id, 'prefix_4Yes',  'prefix_4')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifyeshowlonghav}" styleClass="formLabel" style="width: 387px;text-align:left;margin-left:30px" id="ifbike" rendered="#{pc_Driving.showIfbike}"></h:outputText>
							</td>
							<td>
								<h:inputText style="width: 81px" id="lon" value="#{pc_Driving.lon}" rendered="#{pc_Driving.showLon}" disabled="#{pc_Driving.disableLon}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Listmakeandnumb}" styleClass="formLabel" style="width: 351px;text-align:left;margin-left:60px" id="listcc" rendered="#{pc_Driving.showListcc}"></h:outputText>
							</td>
							<td></td>
							<td>
							     <h:commandButton id="list" image="images/circle_i.gif"  onclick="launchDetailsPopUp('list');" style="margin-right:10px;" rendered="#{pc_Driving.show7}" styleClass="ovitalic#{pc_Driving.haslist}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Howmanymilesdoy}" id="howyears" styleClass="formLabel" rendered="#{pc_Driving.showHowyears}"></h:outputText>
							</td>
							<td style="padding-top: 15px;">
								<h:inputText style="width: 81px" value="#{pc_Driving.average}" id="average" rendered="#{pc_Driving.showAverage}" disabled="#{pc_Driving.disableAverage}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputText value="#{componentBundle.Isitseasonalrid}" styleClass="formLabel" style="width: 387px;text-align:left;margin-left:30px" id="isriding" rendered="#{pc_Driving.showIsriding}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_5}" id="prefix_5No" onclick="toggleCBGroup(this.form.id, 'prefix_5No',  'prefix_5')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_5}" id="prefix_5Yes" onclick="toggleCBGroup(this.form.id, 'prefix_5Yes',  'prefix_5')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Anyprofessional}" styleClass="formLabel" id="anycourses" rendered="#{pc_Driving.showAnycourses}"></h:outputText>
							</td>
							<td style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_6}" id="prefix_6No" onclick="toggleCBGroup(this.form.id, 'prefix_6No',  'prefix_6')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_6}" id="prefix_6Yes" onclick="toggleCBGroup(this.form.id, 'prefix_6Yes',  'prefix_6');if(document.getElementById(this.id).checked){launchDetailsPopUp('traininginfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifyesexplain_0}" styleClass="formLabel" style="width: 387px;text-align:left;margin-left:30px" id="ifexplain_1" rendered="#{pc_Driving.showIfexplain_1}"></h:outputText>
							</td>
							<td></td>
							<td>
							     <h:commandButton id="traininginfo" image="images/circle_i.gif"  onclick="launchDetailsPopUp('traininginfo');" style="margin-right:10px;" rendered="#{pc_Driving.show8}" styleClass="ovitalic#{pc_Driving.hastraininginfo}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Whatpercentofti_0}" styleClass="formLabel" id="wearHelmet" rendered="#{pc_Driving.showWearHelmet}"></h:outputText>
							</td>
							<td style="padding-top: 15px;">
								<h:inputText style="width: 81px" id="helm" value="#{pc_Driving.helmet}" rendered="#{pc_Driving.showHelm}" disabled="#{pc_Driving.disableHelm}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Whatpercentofti_0_1}" styleClass="formLabel" id="whatriding2" rendered="#{pc_Driving.showWhatriding2}"></h:outputText>
							</td>
							<td style="padding-top: 15px;">
								<h:inputText style="width: 81px" id="protective" value="#{pc_Driving.protectiveEyewear}" rendered="#{pc_Driving.showProtective}" disabled="#{pc_Driving.disableProtective}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td style="padding-top: 15px;">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px" value="#{componentBundle.Haveyoueverbeendriving}" id="havebike" styleClass="formLabel" rendered="#{pc_Driving.showHavebike}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixNo_7}" id="prefix_7No" onclick="toggleCBGroup(this.form.id, 'prefix_7No',  'prefix_7')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Driving.prefixYes_7}" id="prefix_7Yes" onclick="toggleCBGroup(this.form.id, 'prefix_7Yes',  'prefix_7');if(document.getElementById(this.id).checked){launchDetailsPopUp('convicted')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifyeslistdatean}" styleClass="formLabel" style="width: 387px;text-align:left;margin-left:30px" id="ifviolation" rendered="#{pc_Driving.showIfviolation}"></h:outputText>
							</td>
							<td></td>
							<td>
							     <h:commandButton id="convicted" image="images/circle_i.gif"  onclick="launchDetailsPopUp('convicted');" style="margin-right:10px;" rendered="#{pc_Driving.show9}" styleClass="ovitalic#{pc_Driving.hasconvicted}"/>
							</td>
						</tr>

						<tr style="padding-top: 5px;">
							<td colspan="2" style="height: 32px; padding-bottom: 30px;" valign="top">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Driving.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_3" rendered="#{pc_Driving.show_3}" disabled="#{pc_Driving.disable_3}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_Driving.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_4" rendered="#{pc_Driving.show_4}" disabled="#{pc_Driving.disable_4}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Driving.okAction}" onclick="resetTargetFrame(this.form.id);" id="_5" rendered="#{!pc_Links.showIdDriving}" disabled="#{pc_Driving.disable_5}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Driving.okAction}" onclick="resetTargetFrame(this.form.id);" id="updateButton" rendered="#{pc_Links.showIdDriving}" disabled="#{pc_Driving.disable_5}"></h:commandButton>
							</td>
							<td></td>
						</tr>
					
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
