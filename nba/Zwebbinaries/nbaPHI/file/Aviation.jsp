<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>aviation</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
		function valueChangeEvent() {	
			    getScrollXY('page');
				}
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_AVIATION" value="#{pc_Aviation}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Aviation.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Aviation.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="360">
							<col width="90">
								<col width="90">
									<col width="90">
										<tr style="padding-top: 5px;">
											<td colspan="4" class="sectionSubheader"> <!-- NBA324 -->
												<h:outputLabel value="#{componentBundle.Aviation}" style="width: 220px" id="Aviation" rendered="#{pc_Aviation.showAviation}"></h:outputLabel>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="2">
												<h:outputText style="width: 260px;text-align:left;margin-left:5px" value="#{componentBundle.FLYINGTIME}" styleClass="formLabel" id="flyingTime" rendered="#{pc_Aviation.showFlyingTime}"></h:outputText>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="4">
												<h:outputText value="#{componentBundle.HoursflownasaPi}" styleClass="formLabel" style="width: 564px;text-align:left;margin-left:5px;" id="flyingTimeLabel" rendered="#{pc_Aviation.showFlyingTimeLabel}"></h:outputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="4"></td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="1">
												<h:outputText style="width: 260px;text-align:left;margin-left:15px" value="#{componentBundle.NOTFLYINGFORPAY}" styleClass="formLabel" id="notFlyingForPay" rendered="#{pc_Aviation.showNotFlyingForPay}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Next12Months}" styleClass="formLabelRight" style="width:60px;" id="a1next12Months" rendered="#{pc_Aviation.showA1next12Months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Past12Months}" styleClass="formLabelRight" style="width:60px;" id="a1past12months" rendered="#{pc_Aviation.showA1past12months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Field12YearsAgo}" styleClass="formLabelRight" style="width:60px;" id="a11to2yearsago" rendered="#{pc_Aviation.showA11to2yearsago}"></h:outputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Pleasure}" styleClass="formLabel" id="pleasureLabel" rendered="#{pc_Aviation.showPleasureLabel}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="pleasureNext12Months" value="#{pc_Aviation.pleasureNext12Months}" rendered="#{pc_Aviation.showPleasureNext12Months}" disabled="#{pc_Aviation.disablePleasureNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="pleasurePast12Months" value="#{pc_Aviation.pleasurePast12Months}" rendered="#{pc_Aviation.showPleasurePast12Months}" disabled="#{pc_Aviation.disablePleasurePast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="pleasure1to2YearsAgo" value="#{pc_Aviation.pleasure1to2YearsAgo}" rendered="#{pc_Aviation.showPleasure1to2YearsAgo}" disabled="#{pc_Aviation.disablePleasure1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Personalbusines}" styleClass="formLabel" id="personalBusinessTransportation" rendered="#{pc_Aviation.showPersonalBusinessTransportation}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" immediate="personalNext12months" value="#{pc_Aviation.personalNext12months}" id="personalNext12months" rendered="#{pc_Aviation.showPersonalNext12months}" disabled="#{pc_Aviation.disablePersonalNext12months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="personalPast12Months" value="#{pc_Aviation.personalPast12Months}" rendered="#{pc_Aviation.showPersonalPast12Months}" disabled="#{pc_Aviation.disablePersonalPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="personal1to2YearsAgo" value="#{pc_Aviation.personal1to2YearsAgo}" rendered="#{pc_Aviation.showPersonal1to2YearsAgo}" disabled="#{pc_Aviation.disablePersonal1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Instructionasas}" styleClass="formLabel" id="instruction" rendered="#{pc_Aviation.showInstruction}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="instructionNext12Month" value="#{pc_Aviation.instructionNext12Month}" rendered="#{pc_Aviation.showInstructionNext12Month}" disabled="#{pc_Aviation.disableInstructionNext12Month}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="instructionPast12Months" value="#{pc_Aviation.instructionPast12Months}" rendered="#{pc_Aviation.showInstructionPast12Months}" disabled="#{pc_Aviation.disableInstructionPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="instructionPast1to2YearsAgo" value="#{pc_Aviation.instructionasastudent}" rendered="#{pc_Aviation.showInstructionPast1to2YearsAgo}" disabled="#{pc_Aviation.disableInstructionPast1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 115px;text-align:left;margin-left:45px;" value="#{componentBundle.Otherdescribe}" styleClass="formLabel" id="others" rendered="#{pc_Aviation.showOthers}"></h:outputText>
												<h:commandButton image="images/circle_i.gif" style="margin-right:82px;" onclick="launchDetailsPopUp('notFlyingForPayOthDetails');" id="notFlyingForPayOthDetails" rendered="#{pc_Aviation.showNotFlyingForPayOthDetails}" styleClass="ovitalic#{pc_Aviation.hasnotFlyingForPayOthDetails}"></h:commandButton>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="otherNext12Months" value="#{pc_Aviation.otherNext12Months}" rendered="#{pc_Aviation.showOtherNext12Months}" disabled="#{pc_Aviation.disableOtherNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="otherPast12Months" value="#{pc_Aviation.otherPast12Months}" rendered="#{pc_Aviation.showOtherPast12Months}" disabled="#{pc_Aviation.disableOtherPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="other1to2YearsAgo" value="#{pc_Aviation.other1to2YearsAgo}" rendered="#{pc_Aviation.showOther1to2YearsAgo}" disabled="#{pc_Aviation.disableOther1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="left" colspan="1">
												<h:outputText style="width: 260px;text-align:left;margin-left:15px" value="#{componentBundle.FLYINGFORPAY}" styleClass="formLabel" id="flyingForPay" rendered="#{pc_Aviation.showFlyingForPay}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Next12Months}" styleClass="formLabelRight" style="width:60px;" id="a2next12Months" rendered="#{pc_Aviation.showA2next12Months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Past12Months}" styleClass="formLabelRight" style="width:60px;" id="a2past12Months" rendered="#{pc_Aviation.showA2past12Months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Field12YearsAgo}" styleClass="formLabelRight" style="width:60px;" id="a21to2yearsAgo" rendered="#{pc_Aviation.showA21to2yearsAgo}"></h:outputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Scheduledpassen}" styleClass="formLabel" id="scheduledPassengerAirline" rendered="#{pc_Aviation.showScheduledPassengerAirline}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="scheduledPassengerNext12Months" value="#{pc_Aviation.scheduledPassengerNext12Months}" rendered="#{pc_Aviation.showScheduledPassengerNext12Months}" disabled="#{pc_Aviation.disableScheduledPassengerNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="scheduledPassengerPast12Months" value="#{pc_Aviation.scheduledPassengerPast12Months}" rendered="#{pc_Aviation.showScheduledPassengerPast12Months}" disabled="#{pc_Aviation.disableScheduledPassengerPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="scheduledPassenger1to2YearsAgo" value="#{pc_Aviation.scheduledPassenger1to2YearsAgo}" rendered="#{pc_Aviation.showScheduledPassenger1to2YearsAgo}" disabled="#{pc_Aviation.disableScheduledPassenger1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Employerownedai}" styleClass="formLabel" id="field12YearsAgo" rendered="#{pc_Aviation.showField12YearsAgo}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="empOwnedNext12Months" value="#{pc_Aviation.empOwnedNext12Months}" rendered="#{pc_Aviation.showEmpOwnedNext12Months}" disabled="#{pc_Aviation.disableEmpOwnedNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="empOwnedPast12Months" value="#{pc_Aviation.empOwnedPast12Months}" rendered="#{pc_Aviation.showEmpOwnedPast12Months}" disabled="#{pc_Aviation.disableEmpOwnedPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="empOwned1to2YearsAgo" value="#{pc_Aviation.empOwned1to2YearsAgo}" rendered="#{pc_Aviation.showEmpOwned1to2YearsAgo}" disabled="#{pc_Aviation.disableEmpOwned1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Otherfreightorp}" styleClass="formLabel" id="otherfreightorpassengerservice" rendered="#{pc_Aviation.showOtherfreightorpassengerservice}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="otherFrieghtNext12Months" value="#{pc_Aviation.otherFrieghtNext12Months}" rendered="#{pc_Aviation.showOtherFrieghtNext12Months}" disabled="#{pc_Aviation.disableOtherFrieghtNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="otherFrieghtPast12Months" value="#{pc_Aviation.otherFrieghtPast12Months}" rendered="#{pc_Aviation.showOtherFrieghtPast12Months}" disabled="#{pc_Aviation.disableOtherFrieghtPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="otherFrieght1to2YearsAgo" value="#{pc_Aviation.otherFrieght1to2YearsAgo}" rendered="#{pc_Aviation.showOtherFrieght1to2YearsAgo}" disabled="#{pc_Aviation.disableOtherFrieght1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Cropdustingorae}" styleClass="formLabel" id="cropDustingOrAerialSpraying" rendered="#{pc_Aviation.showCropDustingOrAerialSpraying}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="cropNext12Months" value="#{pc_Aviation.cropNext12Months}" rendered="#{pc_Aviation.showCropNext12Months}" disabled="#{pc_Aviation.disableCropNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="cropPast12Months" value="#{pc_Aviation.cropPast12Months}" rendered="#{pc_Aviation.showCropPast12Months}" disabled="#{pc_Aviation.disableCropPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="crop1to2YearsAgo" value="#{pc_Aviation.crop1to2YearsAgo}" rendered="#{pc_Aviation.showCrop1to2YearsAgo}" disabled="#{pc_Aviation.disableCrop1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td align="right" colspan="1">
												<h:outputText style="width: 220px;text-align:left;margin-left:45px;" value="#{componentBundle.Studentinstruct}" styleClass="formLabel" id="studentInstruction" rendered="#{pc_Aviation.showStudentInstruction}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="studentNext12Months" value="#{pc_Aviation.studentNext12Months}" rendered="#{pc_Aviation.showStudentNext12Months}" disabled="#{pc_Aviation.disableStudentNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="studentPast12Months" value="#{pc_Aviation.studentPast12Months}" rendered="#{pc_Aviation.showStudentPast12Months}" disabled="#{pc_Aviation.disableStudentPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="student1to2Years" value="#{pc_Aviation.student1to2Years}" rendered="#{pc_Aviation.showStudent1to2Years}" disabled="#{pc_Aviation.disableStudent1to2Years}"></h:inputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="1"></td>
											<td align="left" colspan="1">
												<h:outputText value="#{componentBundle.Next12Months}" styleClass="formLabelRight" style="width:60px;" id="a3Next12Months" rendered="#{pc_Aviation.showA3Next12Months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Past12Months}" styleClass="formLabelRight" style="width:60px;" id="a3Past12Months" rendered="#{pc_Aviation.showA3Past12Months}"></h:outputText>
											</td>
											<td>
												<h:outputText value="#{componentBundle.Field12YearsAgo}" styleClass="formLabelRight" style="width:60px;" id="a31to2Years" rendered="#{pc_Aviation.showA31to2Years}"></h:outputText>
											</td>
										</tr>
										<tr style="padding-top: 5px;">
											<td colspan="1">
												<h:outputText style="width: 260px;text-align:left;margin-left:15px" value="#{componentBundle.MILITARY}" styleClass="formLabel" id="military" rendered="#{pc_Aviation.showMilitary}"></h:outputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="militaryNext12Months" value="#{pc_Aviation.militaryNext12Months}" rendered="#{pc_Aviation.showMilitaryNext12Months}" disabled="#{pc_Aviation.disableMilitaryNext12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="militaryPast12Months" value="#{pc_Aviation.militaryPast12Months}" rendered="#{pc_Aviation.showMilitaryPast12Months}" disabled="#{pc_Aviation.disableMilitaryPast12Months}"></h:inputText>
											</td>
											<td>
												<h:inputText style="width: 60px;"  styleClass="formEntryText" id="military1to2YearsAgo" value="#{pc_Aviation.military1to2YearsAgo}" rendered="#{pc_Aviation.showMilitary1to2YearsAgo}" disabled="#{pc_Aviation.disableMilitary1to2YearsAgo}"></h:inputText>
											</td>
										</tr>
									</col>
								</col>
							</col>
						</col>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputText style="width: 260px;text-align:left;margin-left:5px" value="#{componentBundle.FLYINGEXPERIENC}" styleClass="formLabel" id="flyingExperience" rendered="#{pc_Aviation.showFlyingExperience}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 5px;">
								<h:outputText style="width: 425px;text-align:left;margin-left:15px;" value="#{componentBundle.Totalnumberofho}" styleClass="formLabel" id="ttlNumberOfHrsLabel" rendered="#{pc_Aviation.showTtlNumberOfHrsLabel}"></h:outputText>
							</td>
							<td>
								<h:inputText style="width: 60px;"  styleClass="formEntryText" id="ttlNumberOfHrs" value="#{pc_Aviation.ttlNumberOfHrs}" rendered="#{pc_Aviation.showTtlNumberOfHrs}" disabled="#{pc_Aviation.disableTtlNumberOfHrs}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Numberofthoseho}" styleClass="formLabel" style="width: 411px;text-align:left;margin-left:30px;" id="noOfHrsLabel" rendered="#{pc_Aviation.showNoOfHrsLabel}"></h:outputText>
							</td>
							<td>
								<h:inputText style="width: 60px;"  styleClass="formEntryText" id="noOfHrs" value="#{pc_Aviation.noOfHrs}" rendered="#{pc_Aviation.showNoOfHrs}" disabled="#{pc_Aviation.disableNoOfHrs}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputText style="width: 427px;text-align:left;margin-left:15px;" value="#{componentBundle.Dateofmostrecen}" styleClass="formLabel" id="dtMostRecentFlightLablel" rendered="#{pc_Aviation.showDtMostRecentFlightLablel}"></h:outputText>
							</td>
							<td colspan="2">
								<h:inputText style="width: 100px;"  styleClass="formEntryText" id="mostRecentFlightDate" value="#{pc_Aviation.dtMostRecentFlight}" rendered="#{pc_Aviation.showDtMostRecentFlight}" disabled="#{pc_Aviation.disableDtMostRecentFlight}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 369px;text-align:left;margin-left:5px;" value="#{componentBundle.LICENSESANDCERT}" styleClass="formLabel" id="licensesAndCertificates" rendered="#{pc_Aviation.showLicensesAndCertificates}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 516px;text-align:left;margin-left:15px" value="#{componentBundle.Whattypeofcerti}" styleClass="formLabel" id="certificateTypeLabel" rendered="#{pc_Aviation.showCertificateTypeLabel}"></h:outputText>
								<h:outputText style="width: 516px;text-align:left;margin-left:30px" value="#{componentBundle.Studentprivatec}" styleClass="formLabel" id="certificateDetailsLabel" rendered="#{pc_Aviation.showCertificateDetailsLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectOneMenu style="width: 173px" id="certificateType" value="#{pc_Aviation.fieldcertificateType}" rendered="#{pc_Aviation.showCertificateType}" disabled="#{pc_Aviation.disableCertificateType}">
									<f:selectItems value="#{pc_Aviation.olILUAVIATIONTYPEList}"></f:selectItems>
								</h:selectOneMenu>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="certificateDetails" onclick="launchDetailsPopUp('certificateDetails');" rendered="#{pc_Aviation.showCertificateDetails}" styleClass="ovitalic#{pc_Aviation.hascertificateDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width:600px;text-align:left;margin-left:15px" value="#{componentBundle.DoyouhaveanInst}" styleClass="formLabel" id="iFRLabel" rendered="#{pc_Aviation.showIFRLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.ifRNo}" onclick="toggleCBGroup(this.form.id, 'ifRNo',  'ifR')" id="ifRNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="iFR"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.ifRYes}" onclick="toggleCBGroup(this.form.id, 'ifRYes',  'ifR');if(document.getElementById(this.id).checked){launchDetailsPopUp('iFRDetails')};" id="ifRYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="iFR_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Ifyeshowmanyhou}" styleClass="formLabel" style="width: 516px;text-align:left;margin-left:30px" id="iFRLabelIfYes" rendered="#{pc_Aviation.showIFRLabelIfYes}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="iFRDetails" onclick="launchDetailsPopUp('iFRDetails');" rendered="#{pc_Aviation.showIFRDetails}" styleClass="ovitalic#{pc_Aviation.hasiFRDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 516px;text-align:left;margin-left:15px" value="#{componentBundle.AviationDoyouHaveAny}" styleClass="formLabel" id="otherFlightRatingLabel" rendered="#{pc_Aviation.showOtherFlightRatingLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.otherFlightRatingNo}" onclick="toggleCBGroup(this.form.id, 'otherFlightRatingNo',  'otherFlightRating')" id="otherFlightRatingNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="otherFlightRating"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.otherFlightRatingYes}" onclick="toggleCBGroup(this.form.id, 'otherFlightRatingYes',  'otherFlightRating');if(document.getElementById(this.id).checked){launchDetailsPopUp('otherFlightRatingDetails')};" id="otherFlightRatingYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="otherFlightRating_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" id="otherFlightRatingDetails" style="margin-right:10px;" onclick="launchDetailsPopUp('otherFlightRatingDetails');" rendered="#{pc_Aviation.showOtherFlightRatingDetails}" styleClass="ovitalic#{pc_Aviation.hasotherFlightRatingDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.MEDICAL}" styleClass="formLabel" style="width: 369px;text-align:left;margin-left:5px;" id="medical" rendered="#{pc_Aviation.showMedical}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputText value="#{componentBundle.WhatclassofFAAm}" styleClass="formLabel" style="width: 350px;text-align:left;margin-left:15px;" id="classOfFAALabel" rendered="#{pc_Aviation.showClassOfFAALabel}"></h:outputText>
								<h:outputText value="#{componentBundle.Class1ClassIIor}" styleClass="formLabel" style="width: 350px;text-align:left;margin-left:15px;" id="classDetailsLabel" rendered="#{pc_Aviation.showClassDetailsLabel}"></h:outputText>
							</td>
							<td colspan="3">
								<h:selectOneMenu style="width: 173px" id="classOfFAA" value="#{pc_Aviation.classOfFAA}" rendered="#{pc_Aviation.showClassOfFAA}" disabled="#{pc_Aviation.disableClassOfFAA}">
									<f:selectItems value="#{pc_Aviation.olILUMEDEXAMList}"></f:selectItems>
								</h:selectOneMenu>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputText style="width: 350px;text-align:left;margin-left:15px;" value="#{componentBundle.DateoflastFAAme}" styleClass="formLabel" id="lastFAADateLabel" rendered="#{pc_Aviation.showLastFAADateLabel}"></h:outputText>
							</td>
							<td colspan="3">
								<h:inputText style="width: 100px;"  styleClass="formEntryText" id="lastFAADate" value="#{pc_Aviation.lastFAADate}" rendered="#{pc_Aviation.showLastFAADate}" disabled="#{pc_Aviation.disableLastFAADate}"><f:convertDateTime pattern="#{componentBundle.datePattern}"/></h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputText style="width: 350px;text-align:left;margin-left:15px;" value="#{componentBundle.Listanyoperatio}" styleClass="formLabel" id="operationalLimitationsLabel" rendered="#{pc_Aviation.showOperationalLimitationsLabel}"></h:outputText>
							</td>
							<td colspan="2"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="operationalLimitationsDetails" onclick="launchDetailsPopUp('operationalLimitationsDetails');" rendered="#{pc_Aviation.showOperationalLimitationsDetails}" styleClass="ovitalic#{pc_Aviation.hasoperationalLimitationsDetails}"></h:commandButton>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 240px;text-align:left;margin-left:10px" value="#{componentBundle.FLYINGDETAILS}" styleClass="formLabel" id="flyingDetails" rendered="#{pc_Aviation.showFlyingDetails}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText style="width: 381px;text-align:left;margin-left:15px;" value="#{componentBundle.Howmuchofyourfl}" styleClass="formLabel" id="qualifiedFlyingTimeLabel" rendered="#{pc_Aviation.showQualifiedFlyingTimeLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="qualifiedFlyingTimeDetails" onclick="launchDetailsPopUp('qualifiedFlyingTimeDetails');" rendered="#{pc_Aviation.showQualifiedFlyingTimeDetails}" styleClass="ovitalic#{pc_Aviation.hasqualifiedFlyingTimeDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 590px;text-align:left;margin-left:15px;" value="#{componentBundle.AviationHaveyoueverhad}" styleClass="formLabel" id="aricraftAccidentLabel" rendered="#{pc_Aviation.showAricraftAccidentLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.aricraftAccidentNo}" onclick="toggleCBGroup(this.form.id, 'aricraftAccidentNo',  'aricraftAccident')" id="aricraftAccidentNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="aricraftAccident"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.aricraftAccidentYes}" onclick="toggleCBGroup(this.form.id, 'aricraftAccidentYes',  'aricraftAccident');if(document.getElementById(this.id).checked){launchDetailsPopUp('aircraftAccidentDetails')};" id="aricraftAccidentYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="aricraftAccident_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="aricraftAccidentDetails" onclick="launchDetailsPopUp('aircraftAccidentDetails');" rendered="#{pc_Aviation.showAricraftAccidentDetails}" styleClass="ovitalic#{pc_Aviation.hasaircraftAccidentDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.Doyoualwaysusep}" styleClass="formLabel" id="usePublicAirportsLabel" rendered="#{pc_Aviation.showUsePublicAirportsLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.usePublicAirportsNo}" onclick="toggleCBGroup(this.form.id, 'usePublicAirportsNo',  'usePublicAirports');if(document.getElementById(this.id).checked){launchDetailsPopUp('usePublicAirportsDetail')};" id="usePublicAirportsNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="usePublicAirports"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.usePublicAirportsYes}" onclick="toggleCBGroup(this.form.id, 'usePublicAirportsYes',  'usePublicAirports');" id="usePublicAirportsYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="usePublicAirports_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="usePublicAirportsDetail" onclick="launchDetailsPopUp('usePublicAirportsDetail');" rendered="#{pc_Aviation.showUsePublicAirportsDetail}" styleClass="ovitalic#{pc_Aviation.hasusePublicAirportsDetail}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.Haveyouflownord}" styleClass="formLabel" id="flownOutsideUSLabel" rendered="#{pc_Aviation.showFlownOutsideUSLabel}"></h:outputText>
								<h:outputText value="#{componentBundle.Ifyeswhereandho}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="flownOutsideUSIfYes" rendered="#{pc_Aviation.showFlownOutsideUSIfYes}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.flownOutsideUSNo}" onclick="toggleCBGroup(this.form.id, 'flownOutsideUSNo',  'flownOutsideUS')" id="flownOutsideUSNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="flownOutsideUS"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.flownOutsideUSYes}" onclick="toggleCBGroup(this.form.id, 'flownOutsideUSYes',  'flownOutsideUS');if(document.getElementById(this.id).checked){launchDetailsPopUp('flownOutsideUSDetails')};" id="flownOutsideUSYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="flownOutsideUS_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="flownOutsideUSDetails" onclick="launchDetailsPopUp('flownOutsideUSDetails');" rendered="#{pc_Aviation.showFlownOutsideUSDetails}" styleClass="ovitalic#{pc_Aviation.hasflownOutsideUSDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.Inwhattypesofci}" styleClass="formLabel" id="typeOfAircraft" rendered="#{pc_Aviation.showTypeOfAircraft}"></h:outputText>
								<h:outputText value="#{componentBundle.FixedWingorRoto}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="typeOfAircraftList" rendered="#{pc_Aviation.showTypeOfAircraftList}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="typeOfAircraftListDetails" onclick="launchDetailsPopUp('typeOfAircraftListDetails');" rendered="#{pc_Aviation.showTypeOfAircraftListDetails}" styleClass="ovitalic#{pc_Aviation.hastypeOfAircraftListDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Whatisthemakemo}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="aircraftConfiguration" rendered="#{pc_Aviation.showAircraftConfiguration}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="aircraftConfigurationDetails" onclick="launchDetailsPopUp('aircraftConfigurationDetails');" rendered="#{pc_Aviation.showAircraftConfigurationDetails}" styleClass="ovitalic#{pc_Aviation.hasaircraftConfigurationDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.Haveyouflownord_0}" styleClass="formLabel" id="flownAsembledAricraftLabel" rendered="#{pc_Aviation.showFlownAsembledAricraftLabel}"></h:outputText>
								<h:outputText value="#{componentBundle.Ifyesgivedetail}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="flownAsembledAricraftIfYes" rendered="#{pc_Aviation.showFlownAsembledAricraftIfYes}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.flownAsembledAricraftNo}" onclick="toggleCBGroup(this.form.id, 'flownAsembledAricraftNo',  'flownAsembledAricraft')" id="flownAsembledAricraftNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="flownAsembledAricraft"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.flownAsembledAricraftYes}" onclick="toggleCBGroup(this.form.id, 'flownAsembledAricraftYes',  'flownAsembledAricraft');if(document.getElementById(this.id).checked){launchDetailsPopUp('flownAsembledAricraftDetails')};" id="flownAsembledAricraftYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="flownAsembledAricraft_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="flownAsembledAricraftDetails" onclick="launchDetailsPopUp('flownAsembledAricraftDetails');" rendered="#{pc_Aviation.showFlownAsembledAricraftDetails}" styleClass="ovitalic#{pc_Aviation.hasflownAsembledAricraftDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.AviationHaveyoubeen}" styleClass="formLabel" id="involvedInSkyDivingLabel" rendered="#{pc_Aviation.showInvolvedInSkyDivingLabel}"></h:outputText>
								<h:outputText value="#{componentBundle.Ifyeswhereandho}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="involvedInSkyDivingIfYes" rendered="#{pc_Aviation.showInvolvedInSkyDivingIfYes}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.involvedInSkyDivingNo}" onclick="toggleCBGroup(this.form.id, 'involvedInSkyDivingNo',  'involvedInSkyDiving')" id="involvedInSkyDivingNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="involvedInSkyDiving"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.involvedInSkyDivingYes}" onclick="toggleCBGroup(this.form.id, 'involvedInSkyDivingYes',  'involvedInSkyDiving');if(document.getElementById(this.id).checked){launchDetailsPopUp('involvedInSkyDivingDetails')};" id="involvedInSkyDivingYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="involvedInSkyDiving_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="involvedInSkyDivingDeatils" onclick="launchDetailsPopUp('involvedInSkyDivingDetails');" rendered="#{pc_Aviation.showInvolvedInSkyDivingDeatils}" styleClass="ovitalic#{pc_Aviation.hasinvolvedInSkyDivingDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 240px;text-align:left;margin-left:5px;" value="#{componentBundle.MILITARYFLYING}" styleClass="formLabel" id="militaryFlying" rendered="#{pc_Aviation.showMilitaryFlying}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 512px;text-align:left;margin-left:15px;" value="#{componentBundle.AviationAreyouamember}" styleClass="formLabel" id="memberOfMilitaryOrgLabel" rendered="#{pc_Aviation.showMemberOfMilitaryOrgLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Aviation.memberOfMilitaryOrgNo}" onclick="toggleCBGroup(this.form.id, 'memberOfMilitaryOrgNo',  'memberOfMilitaryOrg',true);valueChangeEvent();" id="memberOfMilitaryOrgNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="memberOfMilitaryOrg"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Aviation.memberOfMilitaryOrgYes}" onclick="toggleCBGroup(this.form.id, 'memberOfMilitaryOrgYes',  'memberOfMilitaryOrg',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('memberOfMilitaryOrgDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_Aviation.expandMemberOfMilitaryDetails}" id="memberOfMilitaryOrgYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="memberOfMilitaryOrg_0"></h:outputLabel>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="memberOfMilitaryOrgDeatils" onclick="launchDetailsPopUp('memberOfMilitaryOrgDetails');" rendered="#{pc_Aviation.showMemberOfMilitaryOrgDeatils}" styleClass="ovitalic#{pc_Aviation.hasmemberOfMilitaryOrgDetails}"></h:commandButton>
							</td>
						</tr>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Aviation.expandMemberOfMilitary}">						
							<tr style="padding-top: 5px;">
								<td colspan="1">
									<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 225px;text-align:left;margin-left:30px;" id="memberOfMilitaryOrgIfYes" rendered="#{pc_Aviation.showMemberOfMilitaryOrgIfYes}"></h:outputText>
								</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr style="padding-top: 5px;">
								<td colspan="3">
									<h:outputText value="#{componentBundle.Towhatmilitaryo}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="militaryOrgLabel" rendered="#{pc_Aviation.showMilitaryOrgLabel}"></h:outputText>
								</td>
								<td>
									<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="militaryOrgDetails" onclick="launchDetailsPopUp('militaryOrgDetails');" rendered="#{pc_Aviation.showMilitaryOrgDetails}" styleClass="ovitalic#{pc_Aviation.hasmilitaryOrgDetails}"></h:commandButton>
								</td>
							</tr>
							<tr style="padding-top: 5px;">
								<td colspan="3">
									<h:outputText value="#{componentBundle.Inwhattypeofmil}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="typeOfMilitaryAircraftLabel" rendered="#{pc_Aviation.showTypeOfMilitaryAircraftLabel}"></h:outputText>
									<h:outputText styleClass="formLabel" style="width: 485px;text-align:left;margin-left:45px;" value="#{componentBundle.Givealphabetica}" id="militaryAircraftNumericeCode" rendered="#{pc_Aviation.showMilitaryAircraftNumericeCode}"></h:outputText>
								</td>
								<td>
									<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="typeOfMilitaryAircraftDetails" onclick="launchDetailsPopUp('typeOfMilitaryAircraftDetails');" rendered="#{pc_Aviation.showTypeOfMilitaryAircraftDetails}" styleClass="ovitalic#{pc_Aviation.hastypeOfMilitaryAircraftDetails}"></h:commandButton>
								</td>
							</tr>
							<tr style="padding-top: 5px;">
								<td colspan="3"></td>
								<td></td>
							</tr>
							<tr style="padding-top: 5px;">
								<td colspan="3">
									<h:outputText value="#{componentBundle.AviationHowlonghaveyou}" styleClass="formLabel" style="width: 512px;text-align:left;margin-left:30px;" id="howLongFlyingAircraft" rendered="#{pc_Aviation.showHowLongFlyingAircraft}"></h:outputText>
									<h:outputText styleClass="formLabel" style="width: 482px;text-align:left;margin-left:45px;" value="#{componentBundle.Iflessthan1year}" id="listAircraftPreviouslyFlown" rendered="#{pc_Aviation.showListAircraftPreviouslyFlown}"></h:outputText>
								</td>
								<td>
									<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="howLongFlyingAircraftDetails" onclick="launchDetailsPopUp('howLongFlyingAircraftDetails');" rendered="#{pc_Aviation.showHowLongFlyingAircraftDetails}" styleClass="ovitalic#{pc_Aviation.hashowLongFlyingAircraftDetails}"></h:commandButton>
								</td>
							</tr>
							<tr style="padding-top: 5px;">
								<td colspan="1">
									<h:outputText value="#{componentBundle.Doyoueverflyfro}" styleClass="formLabel" style="width: 256px;text-align:left;margin-left:30px;" id="aircraftCarrierLabel" rendered="#{pc_Aviation.showAircraftCarrierLabel}"></h:outputText>
								</td>
								<td colspan="2">
									<h:selectBooleanCheckbox value="#{pc_Aviation.aircraftCarrierNo}" onclick="toggleCBGroup(this.form.id, 'aircraftCarrierNo',  'aircraftCarrier')" id="aircraftCarrierNo"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="aircraftCarrier"></h:outputLabel>
									<h:selectBooleanCheckbox value="#{pc_Aviation.aircraftCarrierYes}" onclick="toggleCBGroup(this.form.id, 'aircraftCarrierYes',  'aircraftCarrier');if(document.getElementById(this.id).checked){launchDetailsPopUp('aircraftCarrierDeatils')};" id="aircraftCarrierYes"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="aircraftCarrier_0"></h:outputLabel>
								</td>
								<td>
									<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="aircraftCarrierDeatils" onclick="launchDetailsPopUp('aircraftCarrierDeatils');" rendered="#{pc_Aviation.showAircraftCarrierDeatils}" styleClass="ovitalic#{pc_Aviation.hasaircraftCarrierDetails}"></h:commandButton>
								</td>
							</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1">
								<h:outputText value="#{componentBundle.ADDITIONALDETAI}" styleClass="formLabel" style="width: 314px;text-align:left;margin-left:5px;" id="additionalDetailsLabel" rendered="#{pc_Aviation.showAdditionalDetailsLabel}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="additionalDetails" onclick="launchDetailsPopUp('additionalDetails');" rendered="#{pc_Aviation.showAdditionalDetails}" styleClass="ovitalic#{pc_Aviation.hasadditionalDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText value="#{componentBundle.Includingtypeof}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:15px;" id="aviationAdditionalDetailsLabel" rendered="#{pc_Aviation.showAviationAdditionalDetailsLabel}"></h:outputText>
							</td>
						</tr>
						<tr/>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="4" align="left" style="height: 60px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancelButton" action="#{pc_Aviation.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Aviation.showCancelButton}" disabled="#{pc_Aviation.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clearButton" action="#{pc_Aviation.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Aviation.showClearButton}" disabled="#{pc_Aviation.disableClearButton}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="okButton" action="#{pc_Aviation.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdAviation}" disabled="#{pc_Aviation.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_Aviation.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdAviation}" disabled="#{pc_Aviation.disableOkButton}"></h:commandButton>
							</td>							
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" align="left"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
