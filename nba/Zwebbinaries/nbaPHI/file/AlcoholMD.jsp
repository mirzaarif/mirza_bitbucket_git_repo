<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>alcohol MD</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_ALCOHOL" value="#{pc_Alcohol}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Alcohol.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Alcohol.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px;" cellpadding="0" cellspacing="0" style="cellpadding: 0px; cellspacing: 0px;table-layout:fixed">
						<col width="410">
							<col width="135">
								<col width="65">
									<tr style="padding-top:5px;">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.Alcohol}" style="width: 220px" id="Alcohol" rendered="#{pc_Alcohol.showAlcohol}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td colspan="3" align="left">
											<h:outputText style="width: 630px;text-align:left;margin-left:5px" value="#{componentBundle.Doyoupresentlyd}" styleClass="formLabel" id="AlcoholQ1Label" rendered="#{pc_Alcohol.showAlcoholQ1Label}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top:5px;">
										<td align="left"></td>
										<td>
											<h:selectBooleanCheckbox value="#{pc_Alcohol.alcoholicNo}" onclick="toggleCBGroup(this.form.id, 'alcoholicNo',  'alcoholic',true);valueChangeEvent();" id="alcoholicNo"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="alcoholic"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Alcohol.alcoholicYes}" onclick="toggleCBGroup(this.form.id, 'alcoholicYes',  'alcoholic',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('alcoholicAmount')};valueChangeEvent();submit();" id="alcoholicYes" valueChangeListener="#{pc_Alcohol.collapseAlcoholInfo}"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="alcoholic_0"></h:outputLabel>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Alcohol.expandAlcoholInd}">
						<tr style="padding-top:5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholicQ1yes" rendered="#{pc_Alcohol.showAlcoholicQ1yes}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Howmuchandhowof}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:30px;" id="alcoholQuantity" rendered="#{pc_Alcohol.showAlcoholQuantity}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('alcoholicAmount');" id="alcoholicAmount" rendered="#{pc_Alcohol.showAlcoholicAmount}" styleClass="ovitalic#{pc_Alcohol.hasalcoholicAmount}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Inthepasthaveyo}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:30px;" id="AlcoholQ1Blabel" rendered="#{pc_Alcohol.showAlcoholQ1Blabel}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.amountNo}" onclick="toggleCBGroup(this.form.id, 'amountNo',  'amount')" id="amountNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="amount"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.amountYes}" onclick="toggleCBGroup(this.form.id, 'amountYes',  'amount');if(document.getElementById(this.id).checked){launchDetailsPopUp('alcoholDetails')};" id="amountYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="amount_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:45px;" id="alcoholQ1Bdetails" rendered="#{pc_Alcohol.showAlcoholQ1Bdetails}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('alcoholDetails');" id="alcoholDetails" rendered="#{pc_Alcohol.showAlcoholDetails}" styleClass="ovitalic#{pc_Alcohol.hasalcoholDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="3">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Ifnolongerdrink}" styleClass="formLabel" id="alcoholQ2Label" rendered="#{pc_Alcohol.showAlcoholQ2Label}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Whendidyoudisco}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ2ALabel" rendered="#{pc_Alcohol.showAlcoholQ2ALabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('discontinuedAlcohol');" style="margin-right:10px;" id="discontinuedAlcohol" rendered="#{pc_Alcohol.showDiscontinuedAlcohol}" styleClass="ovitalic#{pc_Alcohol.hasdiscontinuedAlcohol}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Whatledtothecha}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ2BLabel" rendered="#{pc_Alcohol.showAlcoholQ2BLabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('alcoholChangedetails');" style="margin-right:10px;" id="alcoholChangedetails" rendered="#{pc_Alcohol.showAlcoholChangedetails}" styleClass="ovitalic#{pc_Alcohol.hasalcoholChangedetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="2">
								<h:outputText value="#{componentBundle.Pleasedescribey}" styleClass="formLabel" style="width: 500px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ2CLabel" rendered="#{pc_Alcohol.showAlcoholQ2CLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('drinkingPatternDetails');" style="margin-right:10px;" id="drinkingPatternDetails" rendered="#{pc_Alcohol.showDrinkingPatternDetails}" styleClass="ovitalic#{pc_Alcohol.hasdrinkingPatternDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="3">
								<h:outputText style="width: 590px;text-align:left;margin-left:5px;" value="#{componentBundle.AlcoholHaveyoueverbeenMD}" styleClass="formLabel" id="alcoholQ3Label" rendered="#{pc_Alcohol.showAlcoholQ3Label}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.alcoholQ3No}" onclick="toggleCBGroup(this.form.id, 'alcoholQ3No',  'alcoholQ3');" id="alcoholQ3No"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="alcoholQ3"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.alcoholQ3Yes}" onclick="toggleCBGroup(this.form.id, 'alcoholQ3Yes',  'alcoholQ3');if(document.getElementById(this.id).checked){launchDetailsPopUp('alcoholQ3DetailsIcon')};" id="alcoholQ3Yes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="alcoholQ3_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifsopleaseindic}" styleClass="formLabel" style="width: 300px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ3Details" rendered="#{pc_Alcohol.showAlcoholQ3Details}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('alcoholQ3DetailsIcon');" style="margin-right:10px;" id="alcoholQ3DetailsIcon" rendered="#{pc_Alcohol.showAlcoholQ3DetailsIcon}" styleClass="ovitalic#{pc_Alcohol.hasalcoholQ3DetailsIcon}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="3">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouhadprofe}" styleClass="formLabel" id="alcoholQ4Label" rendered="#{pc_Alcohol.showAlcoholQ4Label}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.professionaldetailsNo}" onclick="toggleCBGroup(this.form.id, 'professionaldetailsNo',  'professionaldetails',true);valueChangeEvent();" id="professionaldetailsNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="professionaldetails"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.professionaldetailsYes}" onclick="toggleCBGroup(this.form.id, 'professionaldetailsYes',  'professionaldetails',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('professionConsultationDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_Alcohol.collapseProfessionalDetails}" id="professionaldetailsYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="professionaldetails_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="IfprofessionalDetailsYes" rendered="#{pc_Alcohol.expandProfessionalDetailsInd}">
						<tr style="padding-top:5px;">
							<td align="left" colspan="3">
								<h:outputText value="#{componentBundle.Ifsopleasegived}" styleClass="formLabel" style="width: 578px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ4Details" rendered="#{pc_Alcohol.showAlcoholQ4Details}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif"  onclick="launchDetailsPopUp('professionConsultationDetails');" style="margin-right:10px;" id="professionConsultationDetails" rendered="#{pc_Alcohol.showProfessionConsultationDetails}" styleClass="ovitalic#{pc_Alcohol.hasprofessionConsultationDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="3">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.AlcoholAreyouapresento}" styleClass="formLabel" id="alcoholQ5Label" rendered="#{pc_Alcohol.showAlcoholQ5Label}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.memberOfGroupNo}" onclick="toggleCBGroup(this.form.id, 'memberOfGroupNo',  'memberOfGroup',true);valueChangeEvent();" id="memberOfGroupNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="memberOfGroup"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.memberOfGroupYes}" onclick="toggleCBGroup(this.form.id, 'memberOfGroupYes',  'memberOfGroup',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('memberDetails')};valueChangeEvent();submit();" valueChangeListener="#{pc_Alcohol.collapseGroupMemberDetails}" id="memberOfGroupYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="memberOfGroup_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="IfmemberOfGroupYes" rendered="#{pc_Alcohol.expandMemberOfGroupInd}">
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.IfyesWhendidyou}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ5ALabel" rendered="#{pc_Alcohol.showAlcoholQ5ALabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('memberDetails');" id="memberDetails" rendered="#{pc_Alcohol.showMemberDetails}" styleClass="ovitalic#{pc_Alcohol.hasmemberDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Areyounowanacti}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:30px;" id="alcoholQ5BLabel" rendered="#{pc_Alcohol.showAlcoholQ5BLabel}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.activeMemberNo}" onclick="toggleCBGroup(this.form.id, 'activeMemberNo',  'activeMember');if(document.getElementById(this.id).checked){launchDetailsPopUp('alcoholQ5DetailsIcon')};" id="activeMemberNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="activeMember"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.activeMemberYes}" onclick="toggleCBGroup(this.form.id, 'activeMemberYes',  'activeMember')" id="activeMemberYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="activeMember_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifnotwhatisthed}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ5BDetails" rendered="#{pc_Alcohol.showAlcoholQ5BDetails}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('alcoholQ5DetailsIcon');" style="margin-right:10px;" id="alcoholQ5DetailsIcon" rendered="#{pc_Alcohol.showAlcoholQ5DetailsIcon}" styleClass="ovitalic#{pc_Alcohol.hasalcoholQ5DetailsIcon}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Haveyouhadanyre}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:15px;" id="alcoholQ5CLabel" rendered="#{pc_Alcohol.showAlcoholQ5CLabel}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.relapseNo}" onclick="toggleCBGroup(this.form.id, 'relapseNo',  'relapse');" id="relapseNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="relapse"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Alcohol.relapseYes}" onclick="toggleCBGroup(this.form.id, 'relapseYes',  'relapse');if(document.getElementById(this.id).checked){launchDetailsPopUp('relapsedDetailsIcon')};" id="relapseYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="relapse_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left">
								<h:outputText value="#{componentBundle.Ifsopleasegiven}" styleClass="formLabel" style="width: 400px;text-align:left;padding-bottom:10px;margin-left:30px;" id="alcoholQ5CRelapseDetails" rendered="#{pc_Alcohol.showAlcoholQ5CRelapseDetails}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" onclick="launchDetailsPopUp('relapsedDetailsIcon');" style="margin-right:10px;" id="relapsedDetailsIcon" rendered="#{pc_Alcohol.showRelapsedDetailsIcon}" styleClass="ovitalic#{pc_Alcohol.hasrelapsedDetailsIcon}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="2">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.AlcoholPleaseaddanyadd}" styleClass="formLabel" id="alcoholQ6Label" rendered="#{pc_Alcohol.showAlcoholQ6Label}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif"  onclick="launchDetailsPopUp('additionalDetailsIcon');" style="margin-right:10px;" id="additionalDetailsIcon" rendered="#{pc_Alcohol.showAdditionalDetailsIcon}" styleClass="ovitalic#{pc_Alcohol.hasadditionalDetailsIcon}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" colspan="3"></td>
						</tr>
						<tr style="padding-top:5px;">
							<td align="left" style="height: 48px">
								<h:commandButton value="Cancel" styleClass="formButton" id="cancelButton" action="#{pc_Alcohol.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Alcohol.showCancelButton}" disabled="#{pc_Alcohol.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="formButton" id="clearButton" action="#{pc_Alcohol.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Alcohol.showClearButton}" disabled="#{pc_Alcohol.disableClearButton}"></h:commandButton>
							</td>
							<td colspan="2">
								<h:commandButton value="OK" styleClass="formButton" id="okButton" action="#{pc_Alcohol.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdAlcohol}" disabled="#{pc_Alcohol.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="formButton" id="updateButton" action="#{pc_Alcohol.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdAlcohol}" disabled="#{pc_Alcohol.disableOkButton}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2" align="left"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
