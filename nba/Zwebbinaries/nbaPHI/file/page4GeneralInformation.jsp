<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	    PHI-->
<!-- SPRNBA-493 	NB-1101   	Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA324         NB-1301	    nbAFull Personal History Interview -->
<!-- SPRNBA-617 	NB-1301     Doctor Last Name field allowing too many characters -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 4 General Info</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame() {
				getScrollXY('phiinterview');
				document.forms['phiinterview'].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('phiinterview');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}			
			function toggleCBGroup(formName, prefix, id,indicator) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					   alert(inputs[i].name);					  
						if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
							if(inputs[i].checked==true && indicator==true){
									inputs[i].checked = false ;
									form.submit();
								}else{
									inputs[i].checked = false ;
								}
						}
					}   
				}   
			}			
			function valueChangeEvent() {	
			    getScrollXY('phiinterview');
				}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>	
		    <PopulateBean:Load serviceName="RETRIEVE_PAGE4GENERALINFORMATION" value="#{pc_Page4GeneralInformation}"></PopulateBean:Load>		
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<div id="Messages" style="display: none"><h:messages></h:messages></div>
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				<h:inputHidden id="scrollx" value="#{pc_Page4GeneralInformation.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page4GeneralInformation.scrollYC}"></h:inputHidden>
				<div class="inputForm">
				     
				        <h:outputLabel value="#{componentBundle.GeneralInf}" styleClass="sectionSubheader" style="width: 508px" id="_0" > <!-- NBA324 -->
				        </h:outputLabel>
				        <h:panelGroup id="limitalcoholuse">
				          <h:outputLabel style="width: 470px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverbeen_3}" styleClass="formLabel" id="_1" ></h:outputLabel>
				          <h:outputLabel style="width: 490px;text-align:left;margin-left:15px;" value="#{componentBundle.FYITheremaybean}" styleClass="formLabel" id="_2"></h:outputLabel>
				          <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo}" onclick="toggleCBGroup(this.form.id, 'checkbox1','checkbox1_No');" id="checkbox1_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="prefix"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes}" id="checkbox1_Yes" onclick="toggleCBGroup(this.form.id, 'checkbox1','checkbox1_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4limitAlcoholUse')};" ></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="prefix_0"></h:outputLabel> 
                           <h:commandButton id="page4limitAlcoholUse" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasLimitAlcoholUse}" 
								onclick="launchDetailsPopUp('page4limitAlcoholUse');" style="margin-right:10px;margin-left:40px;"/>		
				        </h:panelGroup> 
				        <Help:Link contextId="N4_H_AlcoholAdvisory" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="N4_W_AlcoholAdvisory" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				        <h:panelGroup id="convictedoffelony">
				         <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel style="width: 470px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverbeen_0}" styleClass="formLabel" id="_3" ></h:outputLabel>
				          <h:outputLabel style="width: 490px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyesexplain}" styleClass="formLabel" id="_4"></h:outputLabel>
				          <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_1}" onclick="toggleCBGroup(this.form.id, 'checkbox2','checkbox2_No');" id="checkbox2_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_1}" onclick="toggleCBGroup(this.form.id, 'checkbox2','checkbox2_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4ConvictedFelony')};" id="checkbox2_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 
                           <h:commandButton id="page4ConvictedFelony" image="images/circle_i.gif"  styleClass="ovitalic#{pc_Page4GeneralInformation.hasConvictedFelony}"
								onclick="launchDetailsPopUp('page4ConvictedFelony');" style="margin-right:10px;margin-left:40px;"/>		
				        </h:panelGroup> 
				        <Help:Link contextId="N5_H_Convictions" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="N5_W_Convictions" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				        <h:panelGroup id="internationnaltravel">
				         <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel style="width: 470px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouintendtotr}" styleClass="formLabel"  ></h:outputLabel>				          
				          <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_2}" onclick="toggleCBGroup(this.form.id, 'checkbox3','checkbox3_No');" id="checkbox3_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_2}" onclick="toggleCBGroup(this.form.id, 'checkbox3','checkbox3_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4PurposeOfTravel')};" id="checkbox3_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 100px;" styleClass="formLabelRight" ></h:outputLabel> 
				        </h:panelGroup> 
				        <Help:Link contextId="N6_H_Travel" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="N6_W_Travel" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				         <h:panelGroup id="purposeoftravel">
				         <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel style="width: 490px;text-align:left;margin-left:5px;" value="#{componentBundle.WhatcountryWhat}" styleClass="formLabel" ></h:outputLabel>				          
				          <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 5px;" ></h:outputLabel> 
                           <h:commandButton id="page4PurposeOfTravel" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasPurposeofTravel}" 
								onclick="launchDetailsPopUp('page4PurposeOfTravel');" style="margin-right:10px;margin-left:45px;"/>	
				        </h:panelGroup>
				         <h:panelGroup id="militaryreserve">
				         <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel style="width: 470px;text-align:left;margin-left:5px;" value="#{componentBundle.Areyouamemberor}" styleClass="formLabel"  ></h:outputLabel>
				          <h:outputLabel style="width: 490px;text-align:left;margin-left:15px;" value="#{componentBundle.Completeappropr}" styleClass="formLabel" ></h:outputLabel>
				          <h:outputLabel style="width: 490px;text-align:left;margin-left:15px;" value="#{componentBundle.FYINoneedtoaski}" styleClass="formLabel" ></h:outputLabel>
				          <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				          <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_3}" onclick="toggleCBGroup(this.form.id, 'checkbox4','checkbox4_No');" id="checkbox4_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_3}" onclick="toggleCBGroup(this.form.id, 'checkbox4','checkbox4_Yes');;if(document.getElementById(this.id).checked){launchDetailsPopUp('page4MilitaryReserve')}" id="checkbox4_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 
                           <h:commandButton id="page4MilitaryReserve" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasMilitaryReserve}" 
								onclick="launchDetailsPopUp('page4MilitaryReserve');" style="margin-right:10px;margin-left:40px;"/>		
				        </h:panelGroup>
				        <Help:Link contextId="N62_H_ArmedForces" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="N62_W_ArmedForces" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				        <h:outputLabel value="#{componentBundle.HealthInformati}" styleClass="sectionSubheader" style="width: 508px" > <!-- NBA324 -->
				        </h:outputLabel>
				        <h:panelGroup id="personalphysician">
				           <h:outputLabel style="width: 240px;text-align:left;" value="#{componentBundle.Personalphysici}" styleClass="formLabel"></h:outputLabel>
				           <h:commandButton id="page4PersonalPhysician" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasPersonalPhysician}" 
								onclick="launchDetailsPopUp('page4PersonalPhysician');" style="margin-right:10px;margin-left:370px;"/>		
				        </h:panelGroup> 
				        <Help:Link contextId="O1A_H_Physician" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		
				        <h:panelGroup id="physicianname">
				              <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				              <h:outputLabel value="#{componentBundle.Name}" style="width: 115px" styleClass="formLabel"/>				              
				              <h:inputText id="idFirstName" styleClass="formEntryText" value="#{pc_Page4GeneralInformation.firstname}" ></h:inputText>
							  <h:inputText id="idMI" styleClass="formEntryText" style="width: 26px" value="#{pc_Page4GeneralInformation.middlename}" ></h:inputText>
							  <h:inputText id="idLastName" styleClass="formEntryText" value="#{pc_Page4GeneralInformation.lastname}" maxlength="#{pc_Page4GeneralInformation.maxDrLastNameLength}" ></h:inputText> <!-- SPRNBA-617 -->
				              
				        </h:panelGroup>
				         <h:panelGroup id="clinicname">
				             <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				              <h:outputLabel value="#{componentBundle.Nameofclinic}" styleClass="formLabel" style="width: 115px" ></h:outputLabel>							  
				               <h:inputText id="idNameOfClinic" styleClass="formEntryText" style="width: 304px" value="#{pc_Page4GeneralInformation.clinicName}"></h:inputText>			               
				              
				        </h:panelGroup>
				        <Help:Link contextId="O1B_H_Clinic" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				        <h:panelGroup>
				          <h:outputLabel value="#{componentBundle.ifapplicable}" styleClass="formLabel" style="width: 115px"></h:outputLabel>
				          <h:outputLabel value="" styleClass="formLabel" style="width: 304px"></h:outputLabel>
				        </h:panelGroup>
				        
				         <h:panelGroup id="clinicaddress">				             
				              <h:outputLabel value="#{componentBundle.Addess}" styleClass="formLabel" style="width: 115px" ></h:outputLabel>
							  <h:inputText id="idAddrLine1" styleClass="formEntryText" style="width: 304px" value="#{pc_Page4GeneralInformation.addessline1}"></h:inputText>

				        </h:panelGroup>
				        <Help:Link contextId="O1C_H_Address" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				        <h:panelGroup id="clinicaddress1">				             
				              <h:outputLabel value="" styleClass="formLabel" style="width: 115px" ></h:outputLabel>
							  <h:inputText id="idAddrLine2" styleClass="formEntryText" style="width: 304px" value="#{pc_Page4GeneralInformation.addessline2}"></h:inputText>

				        </h:panelGroup>				       
				        <h:panelGroup id="cliniccity">
				             <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 5px;" ></h:outputLabel>
				              <h:outputLabel value="#{componentBundle.City}" styleClass="formLabel" style="width: 115px" ></h:outputLabel>							  
				               <h:inputText id="idcity" styleClass="formEntryText" style="width: 304px" value="#{pc_Page4GeneralInformation.city}"></h:inputText>			               
				              
				        </h:panelGroup>
				         <h:panelGroup id="clinicstate">
				             <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 5px;" ></h:outputLabel>
				              <h:outputLabel value="#{componentBundle.State}" styleClass="formLabel" style="width: 115px" ></h:outputLabel>							  
				            <h:selectOneMenu id="idState" styleClass="formEntryText" style="width: 304px" value="#{pc_Page4GeneralInformation.state}">
									<f:selectItems value="#{pc_Page4GeneralInformation.stateList}"></f:selectItems>
								</h:selectOneMenu>
		               
				              
				        </h:panelGroup>
				         <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>				        
				          <h:panelGrid id="zipcode" columns="3" columnClasses="labelAppEntry,colPHIData,colPHIData" cellpadding="0" cellspacing="0">
		                  <h:column>
		                       
			                  <h:outputLabel value="#{componentBundle.Code}" styleClass="formLabel" style="width: 115px;padding-top: 10px;" ></h:outputLabel>
		                  </h:column>
		                   <h:column>
			                     <h:selectOneRadio layout="pageDirection" styleClass="formEntryText" style="width: 75px" value="#{pc_Page4GeneralInformation.code}" id="code" >
									<f:selectItem itemLabel="#{componentBundle.Zip}" itemValue="0"></f:selectItem>
									<f:selectItem itemLabel="#{componentBundle.Postal}" itemValue="1"></f:selectItem>
								</h:selectOneRadio>				
		                    </h:column>
		                     <h:column>
			                     <h:inputText id="idZipCode" styleClass="formEntryText" style="width: 75px" value="#{pc_Page4GeneralInformation.zipcode}" >
			                     	<f:convertNumber type="zip" integerOnly="true" pattern="#{componentBundle.zipPattern}"/> <!-- SPRNBA-493 -->
			                     </h:inputText>
						   <h:inputText id="idPostalCode" styleClass="formEntryText" style="width: 75px" value="#{pc_Page4GeneralInformation.postalcode}" ></h:inputText>
			                     
		                    </h:column>
		                    </h:panelGrid>
		                     <h:panelGroup id="dateofvisit">                                  
				          <h:outputLabel value="#{componentBundle.Dateandreasonfo}" styleClass="formLabel" style="width: 220px"></h:outputLabel>
				          <h:inputText id="iddate" style="margin-right:10px; width: 120px" value="#{pc_Page4GeneralInformation.dateandreasonforlastvisit}" styleClass="formEntryTextHalf">
                              <f:convertDateTime pattern="#{bundle.datePattern}" />
                           </h:inputText>   
				          <h:commandButton id="page4DateOfVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasDateofVisit}" 
								onclick="launchDetailsPopUp('page4DateOfVisit');" style="margin-right:10px;margin-left:20px;"/>		
						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>  
                                   <h:selectOneMenu styleClass="formEntryTextHalf" style="margin-right:10px; width: 192px" id="id_oi0M3qTS" value="#{pc_Page4GeneralInformation.fieldidoi0M3qTS}">
									<f:selectItems value="#{pc_Page4GeneralInformation.field_100List}"></f:selectItems>
						</h:selectOneMenu>

				        </h:panelGroup>
				        <Help:Link contextId="O1D_H_DateLastVisit" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				        <h:panelGroup id="medicalattention">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.IftheProposedIn}" style="width: 480px;text-align:left;margin-left:15px;" styleClass="formLabel"></h:outputLabel>
						<h:outputLabel value="#{componentBundle.Whenwasthelastt}" style="width: 470px;text-align:left;margin-left:30px;" styleClass="formLabel"></h:outputLabel>
                                 <h:outputLabel value="" styleClass="formLabel" style="width: 330px;"></h:outputLabel>

				          <h:commandButton id="page4MedicalAttention" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasMedicalAttention}" 
								onclick="launchDetailsPopUp('page4MedicalAttention');" style="margin-right:10px;margin-left:40px;"/>		
                                   
				        </h:panelGroup> 
                                <h:panelGroup id="othercareprovider" >
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 20px;" ></h:outputLabel>
				          <h:outputLabel style="width: 411px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouseenanyo}" styleClass="formLabel"></h:outputLabel>

						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_4}" onclick="toggleCBGroup(this.form.id, 'checkbox5','checkbox5_No',true);valueChangeEvent();" id="checkbox5_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_4}" onclick="toggleCBGroup(this.form.id, 'checkbox5','checkbox5_Yes',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page4OtherCareProvider')};valueChangeEvent();submit();" valueChangeListener="#{pc_Page4GeneralInformation.collapseOtherCareProviderInd}" id="checkbox5_Yes"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4OtherCareProvider" image="images/circle_i.gif"  styleClass="ovitalic#{pc_Page4GeneralInformation.hasOtherCareProvider}"
								onclick="launchDetailsPopUp('page4OtherCareProvider');" style="margin-right:10px;margin-left:40px;"/>	
				        </h:panelGroup> 
				        <Help:Link contextId="O2A_H_OtherProvider" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		<Help:Link contextId="O2A_W_OtherProvider" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				        <h:panelGroup id="reasonofvisit" rendered="#{pc_Page4GeneralInformation.expandOtherCareProviderInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel style="width: 447px;text-align:left;margin-left:15px;" value="#{componentBundle.Whenandwhatwast}" styleClass="formLabel"></h:outputLabel>


						<h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 10px;" ></h:outputLabel>
                                      <h:commandButton id="page4ReasonOfVisit" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasReasonofVisit}" 
								onclick="launchDetailsPopUp('page4ReasonOfVisit');" style="margin-right:10px;margin-left:40px;"/>	
                                   
	
                                   
				        </h:panelGroup>  
                                 <h:panelGroup id="treatment" rendered="#{pc_Page4GeneralInformation.expandOtherCareProviderInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Wasthereanytest}" styleClass="formLabel" style="width: 447px;text-align:left;margin-left:15px;"></h:outputLabel>


						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_5}" onclick="toggleCBGroup(this.form.id, 'checkbox6','checkbox6_No');" id="checkbox6_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_5}" onclick="toggleCBGroup(this.form.id, 'checkbox6','checkbox6_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4Treatment')};" id="checkbox6_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4Treatment" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasTreatment}" 
								onclick="launchDetailsPopUp('page4Treatment');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup> 
				         <h:panelGroup id="injury" rendered="#{pc_Page4GeneralInformation.expandOtherCareProviderInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Wasthereanyinju}" styleClass="formLabel" style="width: 447px;text-align:left;margin-left:15px;"></h:outputLabel>



						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_6}" onclick="toggleCBGroup(this.form.id, 'checkbox7','checkbox7_No');" id="checkbox7_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_6}" onclick="toggleCBGroup(this.form.id, 'checkbox7','checkbox7_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4AnyInjury')};" id="checkbox7_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4AnyInjury" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasAnyInjury}" 
								onclick="launchDetailsPopUp('page4AnyInjury');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup> 
                                <h:panelGroup id="limbinvolved" rendered="#{pc_Page4GeneralInformation.expandOtherCareProviderInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Wastherealimbin}" styleClass="formLabel" style="width: 447px;text-align:left;margin-left:15px;"></h:outputLabel>




						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_7}" onclick="toggleCBGroup(this.form.id, 'checkbox8','checkbox8_No');" id="checkbox8_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_7}" onclick="toggleCBGroup(this.form.id, 'checkbox8','checkbox8_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4LimbInvolved')};" id="checkbox8_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4LimbInvolved" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasLimbInvolved}" 
								onclick="launchDetailsPopUp('page4LimbInvolved');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup> 
                                <h:panelGroup id="followupvisit" rendered="#{pc_Page4GeneralInformation.expandOtherCareProviderInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Isthereanyfollo}" styleClass="formLabel" style="width: 447px;text-align:left;margin-left:15px;"></h:outputLabel>



						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_8}" onclick="toggleCBGroup(this.form.id, 'checkbox9','checkbox9_No');" id="checkbox9_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_8}" onclick="toggleCBGroup(this.form.id, 'checkbox9','checkbox9_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4TestingRecommended')};" id="checkbox9_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4TestingRecommended" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasTestingRecommended}" 
								onclick="launchDetailsPopUp('page4TestingRecommended');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup>
				         <h:panelGroup id="medications">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Doyoutakeanypre}" styleClass="formLabel" style="width: 411px;text-align:left;margin-left:5px;"></h:outputLabel>




						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_9}" onclick="toggleCBGroup(this.form.id, 'checkbox10','checkbox10_No',true);valueChangeEvent();" id="checkbox10_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_9}" onclick="toggleCBGroup(this.form.id, 'checkbox10','checkbox10_Yes',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page4Prescriptions')};valueChangeEvent();submit();" id="checkbox10_Yes" valueChangeListener="#{pc_Page4GeneralInformation.collapseMedicationsInd}"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4Prescriptions" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasPrescriptions}" 
								onclick="launchDetailsPopUp('page4Prescriptions');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup>
				       <h:panelGroup id="dosage" rendered="#{pc_Page4GeneralInformation.expandMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Whatisthemedica}" styleClass="formLabel" style="width: 322px;text-align:left;margin-left:15px;"></h:outputLabel>

       			          <h:commandButton id="page4Medication" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasMedication}" 
								onclick="launchDetailsPopUp('page4Medication');" style="margin-right:10px;margin-left:35px;"/>	
                                                                     
				        </h:panelGroup>				        
                         <h:panelGroup id="condition" rendered="#{pc_Page4GeneralInformation.expandMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				         <h:outputLabel value="#{componentBundle.Whatisthecondit}" styleClass="formLabel" style="width: 352px;text-align:left;margin-left:15px;"></h:outputLabel>

       			          <h:commandButton id="page4ConditionOfMedication" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasConditionofMedication}" 
								onclick="launchDetailsPopUp('page4ConditionOfMedication');" style="margin-right:10px;margin-left:5px;"/>	
                                                                     
				        </h:panelGroup>  
				        <h:panelGroup id="howlong" rendered="#{pc_Page4GeneralInformation.expandMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				        <h:outputLabel value="#{componentBundle.Howlonghaveyout}" styleClass="formLabel" style="width: 352px;text-align:left;margin-left:15px;" id="_52" rendered="#{pc_Page4GeneralInfo.show_52}"></h:outputLabel>

       			          <h:commandButton id="page4HowLong" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasHowlong}" 
								onclick="launchDetailsPopUp('page4HowLong');" style="margin-right:10px;margin-left:5px;"/>	
                                                                     
				        </h:panelGroup>
				         <h:panelGroup id="alternativemedications">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 20px;" ></h:outputLabel>
				          <h:outputLabel value="#{componentBundle.Iftakingalterna}" styleClass="formLabel" style="width: 411px;text-align:left;margin-left:5px;"></h:outputLabel>
					<h:outputLabel value="#{componentBundle.Whyforanyspecif}" styleClass="formLabel" style="width: 411px;text-align:left;margin-left:30px;"></h:outputLabel>





						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_10}" onclick="toggleCBGroup(this.form.id, 'checkbox11','checkbox11_No',true);valueChangeEvent();" id="checkbox11_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_10}" onclick="toggleCBGroup(this.form.id, 'checkbox11','checkbox11_Yes',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page4AlternativeMedication')};valueChangeEvent();submit();" valueChangeListener="#{pc_Page4GeneralInformation.collapseAlternativeMedicationsInd}" id="checkbox11_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4AlternativeMedication" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasAlternativeMedication}" 
								onclick="launchDetailsPopUp('page4AlternativeMedication');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup> 
				         <h:panelGroup id="dosage2" rendered="#{pc_Page4GeneralInformation.expandAlternativeMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				        <h:outputLabel value="#{componentBundle.Whatisthemedica_0}" styleClass="formLabel" style="width: 318px;text-align:left;margin-left:15px;"></h:outputLabel>


       			          <h:commandButton id="page4Dosage" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasDosage}" 
								onclick="launchDetailsPopUp('page4Dosage');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup>
                                <h:panelGroup id="condition2" rendered="#{pc_Page4GeneralInformation.expandAlternativeMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				        <h:outputLabel value="#{componentBundle.Whatisthecondit}" styleClass="formLabel" style="width: 318px;text-align:left;margin-left:15px;"></h:outputLabel>



       			          <h:commandButton id="page4CondOfAlternativeMed" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasCondofAlternativemed}" 
								onclick="launchDetailsPopUp('page4CondOfAlternativeMed');" style="margin-right:10px;margin-left:40px;"/>	
                                                                     
				        </h:panelGroup>
                                 <h:panelGroup id="howlong2" rendered="#{pc_Page4GeneralInformation.expandAlternativeMedicationsInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				        <h:outputLabel value="#{componentBundle.Howlonghaveyout}" styleClass="formLabel" style="width: 348px;text-align:left;margin-left:15px;"></h:outputLabel>




       			          <h:commandButton id="page4HowLongAlternative" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasHowlongAlternative}" 
								onclick="launchDetailsPopUp('page4HowLongAlternative');" style="margin-right:10px;margin-left:10px;"/>	
                                                                     
				        </h:panelGroup>
				        <h:panelGroup id="fyi" rendered="#{pc_Page4GeneralInformation.expandAlternativeMedicationsInd}">
                                    <h:outputLabel value="#{componentBundle.FYIieChondroiti}" styleClass="formLabel" style="width:470px;text-align:left;margin-left:30px;"></h:outputLabel>
	                              <h:outputLabel value="#{componentBundle.FYIB12Injection}" styleClass="formLabel" style="width:470px;text-align:left;margin-left:30px;" id="_64"></h:outputLabel>

                                                                      
				        </h:panelGroup>
				       <h:panelGroup id="proposedinsured">
				                    <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 20px;" ></h:outputLabel>
                                   <h:outputLabel style="width: 508px;text-align:left;margin-left:5px;" value="#{componentBundle.Proposedinsured}" styleClass="formLabel"></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.HeightClothedWe}" styleClass="formLabel" style="width: 240px" ></h:outputLabel>
                                   <h:outputLabel value="#{componentBundle.Feet}" style="width: 75px;text-align:right;" styleClass="formLabel"></h:outputLabel>
								   <h:inputText style="width: 40px;margin-right:10px;" id="idfeet" value="#{pc_Page4GeneralInformation.feet}"></h:inputText>
								  
								   	<h:outputLabel value="#{componentBundle.Inches}" style="width: 20px" styleClass="formLabel"></h:outputLabel>
								<h:inputText style="width: 40px" id="idinches" value="#{pc_Page4GeneralInformation.inches}"></h:inputText>
								<h:outputLabel value="" styleClass="formLabel" style="width: 240px;height: 20px;" ></h:outputLabel>
								<h:outputLabel value="#{componentBundle.Weight}" style="width: 75px;text-align:right;" styleClass="formLabel" ></h:outputLabel>
								<h:inputText style="width: 40px" id="idweight" value="#{pc_Page4GeneralInformation.weight}" ></h:inputText>
								
								
                                   
                                   

                                                                      
				        </h:panelGroup>
				        <h:panelGroup id="weightchanged">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 20px;" ></h:outputLabel>
				                <h:outputLabel value="#{componentBundle.Hasyourweightch}" styleClass="formLabel" style="width: 490px;text-align:left;margin-left:15px;"></h:outputLabel>			                




						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_11}" onclick="toggleCBGroup(this.form.id, 'checkbox12','checkbox12_No',true);valueChangeEvent();" id="checkbox12_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_11}" onclick="toggleCBGroup(this.form.id, 'checkbox12','checkbox12_Yes',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('page4ReasonOfChange')};valueChangeEvent();submit();" valueChangeListener="#{pc_Page4GeneralInformation.collapseWeightChangedInd}" id="checkbox12_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px;margin-right:70px;" styleClass="formLabelRight" ></h:outputLabel> 

				        </h:panelGroup> 
				        <Help:Link contextId="O3B_H_Weight" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		
				         
                                 <h:panelGroup id="reasonforchange" rendered="#{pc_Page4GeneralInformation.expandWeightChangedInd}">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 10px;" ></h:outputLabel>
				        <h:outputLabel value="#{componentBundle.Ifyesindicatega}" styleClass="formLabel" style="width:340px;text-align:left;margin-left:30px;"></h:outputLabel>





       			          <h:commandButton id="page4ReasonOfChange" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasReasonofChange}" 
								onclick="launchDetailsPopUp('page4ReasonOfChange');" style="margin-right:10px;margin-left:3px;"/>	
                                                                     
				        </h:panelGroup>
				        <h:panelGroup id="AIDS">
                                  <h:outputLabel value="" styleClass="formLabel" style="width: 508px;height: 20px;" ></h:outputLabel>
				         <h:outputLabel style="width: 500px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverbeen_AIDS}" styleClass="formLabel"></h:outputLabel>
				         




						<h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
                                  <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixNo_12}" onclick="toggleCBGroup(this.form.id, 'checkbox13','checkbox13_No');" id="checkbox13_No"></h:selectBooleanCheckbox>
						  <h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
						   <h:selectBooleanCheckbox value="#{pc_Page4GeneralInformation.prefixYes_12}" onclick="toggleCBGroup(this.form.id, 'checkbox13','checkbox13_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page4DiagnosedForAids')};" id="checkbox13_Yes"></h:selectBooleanCheckbox>
					      <h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" ></h:outputLabel> 


				          <h:commandButton id="page4DiagnosedForAids" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page4GeneralInformation.hasDiagnosedForaids}" 
								onclick="launchDetailsPopUp('page4DiagnosedForAids');" style="margin-right:10px;margin-left:40px;"/>	
             
				        </h:panelGroup>
				        <Help:Link contextId="O4_H_Aids" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   		
         
				      
					 <f:subview id="tabHeader">
				       <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			        </f:subview>
				</div>
				<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page4GeneralInformation.commitAction}"></h:commandButton>
			</h:form>			
		</f:view>
</body>
</html>
