<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Racing</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			function valueChangeEvent() {	
			    getScrollXY('page');
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_RACING" value="#{pc_Racing}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Racing.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Racing.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="330">
							<col width="75">
								<col width="75">
									<col width="75">
										<col width="75">
											<tr style="padding-top: 5px;">
												<td colspan="5" class="sectionSubheader" align="left"> <!-- NBA324 -->
													<h:outputLabel value="#{componentBundle.Racing}" style="width: 220px" id="labelRacing" rendered="#{pc_Racing.showLabelRacing}"></h:outputLabel>
												</td>
											</tr>
											<tr style="padding-top: 5px;">
												<td colspan="2">
													<h:outputText style="width: 400px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyounoworhavey}" styleClass="formLabel" id="labelDoYouNow" rendered="#{pc_Racing.showLabelDoYouNow}"></h:outputText>
												</td>
												<td colspan="3">
													<h:selectBooleanCheckbox value="#{pc_Racing.everParticipatedInMotorizedRacingNo}" id="everParticipatedInMotorizedRacingNo" onclick="toggleCBGroup(this.form.id, 'everParticipatedInMotorizedRacingNo',  'everParticipatedInMotorizedRacing',true);valueChangeEvent();"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
													<h:selectBooleanCheckbox value="#{pc_Racing.everParticipatedInMotorizedRacingYes}" id="everParticipatedInMotorizedRacingYes" valueChangeListener="#{pc_Racing.collapseMotorizedRacingSection}" onclick="toggleCBGroup(this.form.id, 'everParticipatedInMotorizedRacingYes',  'everParticipatedInMotorizedRacing',false);valueChangeEvent();submit();"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
												</td>
											</tr>
										</col>
									</col>
								</col>
							</col>
						</col>
						<DynaDiv:DynamicDiv id="motorizedracingSection" rendered="#{pc_Racing.expandMotorizedRacingInd}">
						<tr style="padding-top: 5px;">
							<td align="left" colspan="5">															
								<h:outputText style="width: 600px;text-align:left;margin-left:15px;padding-bottom:5px;" value="#{componentBundle.Ifsospecifytype}" styleClass="formLabel" id="labelIfSoSpecifyType" rendered="#{pc_Racing.showLabelIfSoSpecifyType}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left" colspan="1"></td>
							<td colspan="3">
								<h:inputText styleClass="formEntryText" style="width: 200px" id="typeOfVehicle" value="#{pc_Racing.typeOfVehicle}" rendered="#{pc_Racing.showTypeOfVehicle}" disabled="#{pc_Racing.disableTypeOfVehicle}"></h:inputText>
							</td>
							<td>								
								<h:commandButton id="racingTypeOfVehicleDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingTypeOfVehicleDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showTypeOfVehicleDetailsImg}" styleClass="ovitalic#{pc_Racing.hasTypeOfVehicleDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width: 330px;text-align:left;margin-left:15px" value="#{componentBundle.Pleaseprovidede}" styleClass="formLabel" id="labelPleaseProvide" rendered="#{pc_Racing.showLabelPleaseProvide}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Last12months}" styleClass="formLabelRight" style="width:45px;" id="labelLast12Months" rendered="#{pc_Racing.showLabelLast12Months}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Field12YearsAgo}" styleClass="formLabelRight" style="width:45px;" id="label12YearAgo" rendered="#{pc_Racing.showLabel12YearAgo}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Priorto2YearsAg}" styleClass="formLabelRight" style="width:45px;" id="labelPriorto2YearsAgo" rendered="#{pc_Racing.showLabelPriorto2YearsAgo}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Next12Months}" styleClass="formLabelRight" style="width:45px;" id="labelNext12Months" rendered="#{pc_Racing.showLabelNext12Months}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width:310px;padding-bottom:5px;" value="#{componentBundle.Numberofraces}" styleClass="formLabel" id="labelNumberOfRaces" rendered="#{pc_Racing.showLabelNumberOfRaces}"></h:outputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="numberOfRacesLast12Months" value="#{pc_Racing.numberOfRacesLast12Months}" rendered="#{pc_Racing.showNumberOfRacesLast12Months}" disabled="#{pc_Racing.disableNumberOfRacesLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="numberOfRaces12yearsAgo" value="#{pc_Racing.numberOfRaces12yearsAgo}" rendered="#{pc_Racing.showNumberOfRaces12yearsAgo}" disabled="#{pc_Racing.disableNumberOfRaces12yearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="numberOfRacesPrior2YearsAgo" value="#{pc_Racing.numberOfRacesPrior2YearsAgo}" rendered="#{pc_Racing.showNumberOfRacesPrior2YearsAgo}" disabled="#{pc_Racing.disableNumberOfRacesPrior2YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="numberOfRacesNext12Months" value="#{pc_Racing.numberOfRacesNext12Months}" rendered="#{pc_Racing.showNumberOfRacesNext12Months}" disabled="#{pc_Racing.disableNumberOfRacesNext12Months}"></h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width:310px;padding-bottom:5px;" value="#{componentBundle.Totalmilesincom}" styleClass="formLabel"  id="labelTotalMilesInCompetition" rendered="#{pc_Racing.showLabelTotalMilesInCompetition}"></h:outputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="totalMilesinLast12Months" value="#{pc_Racing.totalMilesinLast12Months}" rendered="#{pc_Racing.showTotalMilesinLast12Months}" disabled="#{pc_Racing.disableTotalMilesinLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="totalMilesin12YearsAgo" value="#{pc_Racing.totalMilesin12YearsAgo}" rendered="#{pc_Racing.showTotalMilesin12YearsAgo}" disabled="#{pc_Racing.disableTotalMilesin12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="totalMilesinPrior2YearsAgo" value="#{pc_Racing.totalMilesinPrior2YearsAgo}" rendered="#{pc_Racing.showTotalMilesinPrior2YearsAgo}" disabled="#{pc_Racing.disableTotalMilesinPrior2YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="totalMilesinNext12Months" value="#{pc_Racing.totalMilesinNext12Months}" rendered="#{pc_Racing.showTotalMilesinNext12Months}" disabled="#{pc_Racing.disableTotalMilesinNext12Months}"></h:inputText>
							</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="4">
								<hr/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width:310px;padding-bottom:5px;" value="#{componentBundle.Maximumspeedatt}" styleClass="formLabel" id="labelMaximumSpeedAttained" rendered="#{pc_Racing.showLabelMaximumSpeedAttained}"></h:outputText>
							</td>
							<td colspan="2">
								<h:inputText style="width: 90px;" styleClass="formEntryText" id="maximumSpeedAttained" value="#{pc_Racing.maximumSpeedAttained}" rendered="#{pc_Racing.showMaximumSpeedAttained}" disabled="#{pc_Racing.disableMaximumSpeedAttained}"></h:inputText>
							</td>
							<td colspan="2"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td align="left">
								<h:outputText style="width:310px;padding-bottom:5px;" value="#{componentBundle.Typeofraceandtr}" styleClass="formLabel" id="labelTypeOfRace" rendered="#{pc_Racing.showLabelTypeOfRace}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="racingTypeOfRaceDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingTypeOfRaceDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showTypeOfRaceDetailsImg}" styleClass="ovitalic#{pc_Racing.hasTypeOfRaceDetails}"/>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px;">
							<td colspan="4">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px;" value="#{componentBundle.Howlonghaveyoup}" styleClass="formLabel" id="labelHowLongYouhave" rendered="#{pc_Racing.showLabelHowLongYouhave}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="racingHowLongYouHaveDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingHowLongYouHaveDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showHowLongYouHaveDetailsImg}" styleClass="ovitalic#{pc_Racing.hasHowLongDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouownorhavea}" styleClass="formLabel" id="labelDoyouOwnOrHaveAccess" rendered="#{pc_Racing.showLabelDoyouOwnOrHaveAccess}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Racing.getAccessToCompetitionVehicleNo}" id="getAccessToCompetitionVehicleNo" onclick="toggleCBGroup(this.form.id, 'getAccessToCompetitionVehicleNo',  'getAccessToCompetitionVehicle')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Racing.getAccessToCompetitionVehicleYes}" id="getAccessToCompetitionVehicleYes" onclick="toggleCBGroup(this.form.id, 'getAccessToCompetitionVehicleYes',  'getAccessToCompetitionVehicle');if(document.getElementById(this.id).checked){launchDetailsPopUp('racingIfSoPleaseProvideEngineDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="4">
								<h:outputText style="width: 583px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaceprovi}" styleClass="formLabel" id="labelIfSoPleaseProvideEngine" rendered="#{pc_Racing.showLabelIfSoPleaseProvideEngine}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="racingIfSoPleaseProvideEngineDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingIfSoPleaseProvideEngineDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showIfSoPleaseProvideEngineDetailsImg}" styleClass="ovitalic#{pc_Racing.hasProvideEngineDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText style="width: 400px;text-align:left;margin-left:5px;" value="#{componentBundle.Howfardoyoutypi}" styleClass="formLabel" id="labelHowFarDoYouTypically" rendered="#{pc_Racing.showLabelHowFarDoYouTypically}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td>								
								<h:commandButton id="racingHowFarDoYouTypicallyDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingHowFarDoYouTypicallyDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showHowFarDoYouTypicallyDetailsImg}" styleClass="ovitalic#{pc_Racing.hasHowFarDoYouTypicallyDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="5">
								<h:outputText style="width: 590px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouracedfor}" styleClass="formLabel" id="labelHaveYouRacedForCashprices" rendered="#{pc_Racing.showLabelHaveYouRacedForCashprices}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Racing.racedForCashPricesNo}" id="racedForCashPricesNo" onclick="toggleCBGroup(this.form.id, 'racedForCashPricesNo',  'racedForCashPrices')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Racing.racedForCashPricesYes}" id="racedForCashPricesYes" onclick="toggleCBGroup(this.form.id, 'racedForCashPricesYes',  'racedForCashPrices');if(document.getElementById(this.id).checked){launchDetailsPopUp('racingHaveYouRacedForCashpricesifSoDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText style="width: 187px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" id="labelHaveYouRacedForCashpricesifSo" rendered="#{pc_Racing.showLabelHaveYouRacedForCashpricesifSo}"></h:outputText>
							</td>
							<td colspan="2"></td>
							<td></td>
							<td>								
								<h:commandButton id="racingHaveYouRacedForCashpricesifSoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingHaveYouRacedForCashpricesifSoDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showHaveYouRacedForCashpricesifSoDetailsImg}" styleClass="ovitalic#{pc_Racing.hasHaveYouRacedForACashPriceDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText style="width: 419px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoubeeninvo}" styleClass="formLabel" id="labelHaveYouBeenInvolvedinAnAccident" rendered="#{pc_Racing.showLabelHaveYouBeenInvolvedinAnAccident}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Racing.involvedInAccidentNo}" id="involvedInAccidentNo" onclick="toggleCBGroup(this.form.id, 'involvedInAccidentNo',  'involvedInAccident')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Racing.involvedInAccidentYes}" id="involvedInAccidentYes" onclick="toggleCBGroup(this.form.id, 'involvedInAccidentYes',  'involvedInAccident');if(document.getElementById(this.id).checked){launchDetailsPopUp('racingHaveYouBeenInvolvedinAnAccidentIfSoDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td colspan="2"></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText style="width: 207px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" id="labelHaveYouBeenInvolvedinAnAccidentIfSo" rendered="#{pc_Racing.showLabelHaveYouBeenInvolvedinAnAccidentIfSo}"></h:outputText>
							</td>
							<td colspan="2"></td>
							<td></td>
							<td>								
								<h:commandButton id="racingHaveYouBeenInvolvedinAnAccidentIfSoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingHaveYouBeenInvolvedinAnAccidentIfSoDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showHaveYouBeenInvolvedinAnAccidentIfSoDetailsImg}" styleClass="ovitalic#{pc_Racing.hasHaveYouBeenInvolvedInAccidentDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="5">
								<h:outputText style="width: 599px;text-align:left;margin-left:5px;" value="#{componentBundle.Areyousponsored}" styleClass="formLabel" id="labelAreYouSponsored" rendered="#{pc_Racing.showLabelAreYouSponsored}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Racing.sponsoredByManufacturerNo}" id="sponsoredByManufacturerNo" onclick="toggleCBGroup(this.form.id, 'sponsoredByManufacturerNo',  'sponsoredByManufacturer')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Racing.sponsoredByManufacturerYes}" id="sponsoredByManufacturerYes" onclick="toggleCBGroup(this.form.id, 'sponsoredByManufacturerYes',  'sponsoredByManufacturer');if(document.getElementById(this.id).checked){launchDetailsPopUp('racingAreYouSponsoredIfSoDetail')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputText style="width: 207px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" id="labelAreYouSponsoredIfSo" rendered="#{pc_Racing.showLabelAreYouSponsoredIfSo}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td>								 
								<h:commandButton id="racingAreYouSponsoredIfSoDetail" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingAreYouSponsoredIfSoDetail');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showLabelAreYouSponsoredIfSoDetailImg}" styleClass="ovitalic#{pc_Racing.hasAreYouSponseredDetails}"/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText style="width: 445px;text-align:left;margin-left:5px;" value="#{componentBundle.HaveyoureceivedAnyTraining}" styleClass="formLabel" id="labelHaveYouReceivedAnyTraining" rendered="#{pc_Racing.showLabelHaveYouReceivedAnyTraining}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Racing.receivedAnyTrainingNo}" id="receivedAnyTrainingNo" onclick="toggleCBGroup(this.form.id, 'receivedAnyTrainingNo',  'receivedAnyTraining')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Racing.receivedAnyTrainingYes}" id="receivedAnyTrainingYes" onclick="toggleCBGroup(this.form.id, 'receivedAnyTrainingYes',  'receivedAnyTraining');if(document.getElementById(this.id).checked){launchDetailsPopUp('racingHaveYouReceivedAnyTrainingIfSoDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText style="width: 207px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" id="labelHaveYouReceivedAnyTrainingIfSo" rendered="#{pc_Racing.showLabelHaveYouReceivedAnyTrainingIfSo}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td>								
								<h:commandButton id="racingHaveYouReceivedAnyTrainingIfSoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingHaveYouReceivedAnyTrainingIfSoDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.showLabelHaveYouReceivedAnyTrainingIfSoDetailsImg}" styleClass="ovitalic#{pc_Racing.hasHaveYouReceivedAnyThingDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="4">
								<h:outputText style="width: 599px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="labelPleaseProvideAnyAdditionalInfo" rendered="#{pc_Racing.showLabelPleaseProvideAnyAdditionalInfo}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="racingAdditionalInfoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('racingAdditionalInfoDetails');" style="margin-right:10px;margin-left:10px;" rendered="#{pc_Racing.show_0}" styleClass="ovitalic#{pc_Racing.hasAdditionalInfoDetails}"/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left" style="height: 27px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Racing.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_Racing.show_1}" disabled="#{pc_Racing.disable_1}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_Racing.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{pc_Racing.show_2}" disabled="#{pc_Racing.disable_2}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Racing.okAction}" onclick="resetTargetFrame(this.form.id);" id="_3" rendered="#{!pc_Links.showIdRacing}" disabled="#{pc_Racing.disable_3}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Racing.okAction}" onclick="resetTargetFrame(this.form.id);" id="_333" rendered="#{pc_Links.showIdRacing}" disabled="#{pc_Racing.disable_3}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 44px"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
