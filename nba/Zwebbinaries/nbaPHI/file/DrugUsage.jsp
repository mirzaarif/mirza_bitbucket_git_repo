<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Drugusage</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_DRUGUSAGE" value="#{pc_DrugUsage}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_DrugUsage.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_DrugUsage.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="310">
							<col width="170">
								<col width="50">
									<tr style="padding-top: 5px">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.DrugUsage}" style="width: 220px" id="drug" rendered="#{pc_DrugUsage.showDrug}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td colspan="3">
											<h:outputText value="#{componentBundle.Doyoucurrentlyusedrug}" styleClass="formLabel" style="width: 620px;text-align:left;margin-left:5px;" id="doregularly" rendered="#{pc_DrugUsage.showDoregularly}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px">
										<td></td>
										<td colspan="1" style="width: 225px">
											<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo}" id="prefix_0No" onclick="toggleCBGroup(this.form.id, 'prefix_0No',  'prefix_0',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes}" id="prefix_0Yes" valueChangeListener="#{pc_DrugUsage.collapseDrugsList}" onclick="toggleCBGroup(this.form.id, 'prefix_0Yes',  'prefix_0',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('druginfo')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
										<td style="margin-left: 20px">
						    <h:commandButton id="druginfo" image="images/circle_i.gif"  onclick="launchDetailsPopUp('druginfo');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show1}" styleClass="ovitalic#{pc_DrugUsage.hasdruginfo}"/>
						</td>
									</tr>
								</col>
							</col>
						</col>
						<DynaDiv:DynamicDiv id="valuechan" rendered="#{pc_DrugUsage.expandDrugList}">
						
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Ifyespleaselist}" styleClass="formLabel" style="width: 497px;text-align:left;margin-left:15px;" id="ifeach" rendered="#{pc_DrugUsage.showIfeach}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right" colspan="2">
								<h:outputText value="#{componentBundle.Howoften}" styleClass="formLabel" style="width: 189px;margin-right:80px;" id="howoften" rendered="#{pc_DrugUsage.showHowoften}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="druglist" image="images/circle_i.gif"  onclick="launchDetailsPopUp('druglist');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show2}" styleClass="ovitalic#{pc_DrugUsage.hasdruglist}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right" colspan="2">
								<h:outputText value="#{componentBundle.Datesofuse}" styleClass="formLabel" style="width: 189px;margin-right:80px;" id="datesuse" rendered="#{pc_DrugUsage.showDatesuse}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="datelist" image="images/circle_i.gif"  onclick="launchDetailsPopUp('datelist');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show3}" styleClass="ovitalic#{pc_DrugUsage.hasdatelist}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td align="right" colspan="2">
								<h:outputText value="#{componentBundle.Methodofadminis}" styleClass="formLabel" style="width: 189px;margin-right:80px;" id="methodadministration" rendered="#{pc_DrugUsage.showMethodadministration}"></h:outputText>
								<h:outputText value="#{componentBundle.inhalationinjec}" styleClass="formLabel" id="inhalationetc" style="width: 255px;margin-right:80px;" rendered="#{pc_DrugUsage.showInhalationetc}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="adminmethod" image="images/circle_i.gif"  onclick="launchDetailsPopUp('adminmethod');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show4}" styleClass="ovitalic#{pc_DrugUsage.hasadminmethod}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px">
							<td colspan="3">
								<h:outputText style="width: 620px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverhadp}" styleClass="formLabel" id="havealcohol" rendered="#{pc_DrugUsage.showHavealcohol}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td colspan="1" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo_1}" id="prefix_1No" onclick="toggleCBGroup(this.form.id, 'prefix_1No',  'prefix_1')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes_1}" id="prefix_1Yes" onclick="toggleCBGroup(this.form.id, 'prefix_1Yes',  'prefix_1');if(document.getElementById(this.id).checked){launchDetailsPopUp('detailinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText style="width: 497px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" id="ifdetails" rendered="#{pc_DrugUsage.showIfdetails}"></h:outputText>
								<h:outputText style="width: 482px;text-align:left;margin-left:30px;" value="#{componentBundle.iedatenameandad}" id="ietreatment" styleClass="formLabel" rendered="#{pc_DrugUsage.showIetreatment}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="detailinfo" image="images/circle_i.gif"  onclick="launchDetailsPopUp('detailinfo');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show5}" styleClass="ovitalic#{pc_DrugUsage.hasdetailinfo}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="3" class="formLabel"></td>
						</tr>
						<tr style="padding-top: 15px">
							<td>
								<h:outputText style="width: 333px;text-align:left;margin-left:15px;" value="#{componentBundle.Areyouapresento}" styleClass="formLabel" id="aregroup" rendered="#{pc_DrugUsage.showAregroup}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo_2}" id="prefix_2No" onclick="toggleCBGroup(this.form.id, 'prefix_2No',  'prefix_2',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes_2}" id="prefix_2Yes" valueChangeListener="#{pc_DrugUsage.collapsePresentPastMember}" onclick="toggleCBGroup(this.form.id, 'prefix_2Yes',  'prefix_2',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('groupmember')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="groupmember" image="images/circle_i.gif"  onclick="launchDetailsPopUp('groupmember');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show6}" styleClass="ovitalic#{pc_DrugUsage.hasgroupmember}"/>
							</td>
						</tr>
						<DynaDiv:DynamicDiv id="valuechansecond" rendered="#{pc_DrugUsage.expandPresentPastMember}">
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 256px;text-align:left;margin-left:45px;" id="ifyes" rendered="#{pc_DrugUsage.showIfyes}"></h:outputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Whendidyoubecom}" styleClass="formLabel" style="width: 256px;text-align:left;margin-left:45px;" id="whenmember" rendered="#{pc_DrugUsage.showWhenmember}"></h:outputText>
							</td>
							<td colspan="2">
								<h:inputText style="width: 180px;" readonly="false" styleClass="formEntryText" id="MembershipDate" value="#{pc_DrugUsage.member}" rendered="#{pc_DrugUsage.showMemb}" disabled="#{pc_DrugUsage.disableMemb}"><f:convertDateTime pattern="#{property.datePattern}"/></h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Areyounowanacti}" styleClass="formLabel" style="width: 256px;text-align:left;margin-left:45px;" id="aremember" rendered="#{pc_DrugUsage.showAremember}"></h:outputText>
							</td>
							<td colspan="1" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo_3}" id="prefix_3No" onclick="toggleCBGroup(this.form.id, 'prefix_3No',  'prefix_3');if(document.getElementById(this.id).checked){launchDetailsPopUp('discontinuedreason')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes_3}" id="prefix_3Yes" onclick="toggleCBGroup(this.form.id, 'prefix_3Yes',  'prefix_3')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Ifnotwhatisthed}" styleClass="formLabel" style="width: 358px;text-align:left;margin-left:60px;" id="ifdiscontinued" rendered="#{pc_DrugUsage.showIfdiscontinued}"></h:outputText>
							</td>
							<td>
							   <h:commandButton id="discontinuedreason" image="images/circle_i.gif"  onclick="launchDetailsPopUp('discontinuedreason');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show7}" styleClass="ovitalic#{pc_DrugUsage.hasdiscontinuedreason}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Haveyouhadanyre}" styleClass="formLabel" style="width: 256px;text-align:left;margin-left:45px;" id="haverelapses" rendered="#{pc_DrugUsage.showHaverelapses}"></h:outputText>
							</td>
							<td colspan="1" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo_4}" id="prefix_4No" onclick="toggleCBGroup(this.form.id, 'prefix_4No',  'prefix_4')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes_4}" id="prefix_4Yes" onclick="toggleCBGroup(this.form.id, 'prefix_4Yes',  'prefix_4');if(document.getElementById(this.id).checked){launchDetailsPopUp('approx')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Ifsopleasegiven}" styleClass="formLabel" style="width: 358px;text-align:left;margin-left:60px;" id="ifdates" rendered="#{pc_DrugUsage.showIfdates}"></h:outputText>
							</td>
							<td>
							     <h:commandButton id="approx" image="images/circle_i.gif"  onclick="launchDetailsPopUp('approx');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show8}" styleClass="ovitalic#{pc_DrugUsage.hasapprox}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px">
							<td colspan="3">
								<h:outputText style="width: 620px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyoueverdrugusage}" styleClass="formLabel" id="haveuse" rendered="#{pc_DrugUsage.showHaveuse}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td colspan="1" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixNo_5}" id="prefix_5No" onclick="toggleCBGroup(this.form.id, 'prefix_5No',  'prefix_5')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_DrugUsage.prefixYes_5}" id="prefix_5Yes" onclick="toggleCBGroup(this.form.id, 'prefix_5Yes',  'prefix_5');if(document.getElementById(this.id).checked){launchDetailsPopUp('druguse')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2">
								<h:outputText style="width: 457px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifsopleaseprovi_0}" styleClass="formLabel" id="ifdetails2" rendered="#{pc_DrugUsage.showIfdetails2}"></h:outputText>
							</td>
							<td>
							     <h:commandButton id="druguse" image="images/circle_i.gif"  onclick="launchDetailsPopUp('druguse');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show9}" styleClass="ovitalic#{pc_DrugUsage.hasdruguse}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="2">
								<h:outputText style="width: 555px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseprovidean}" styleClass="formLabel" id="pleaseanswers" rendered="#{pc_DrugUsage.showPleaseanswers}"></h:outputText>
							</td>
							<td>
							   <h:commandButton id="reasoning" image="images/circle_i.gif"  onclick="launchDetailsPopUp('reasoning');" style="margin-right:10px;" rendered="#{pc_DrugUsage.show10}" styleClass="ovitalic#{pc_DrugUsage.hasreasoning}"/>
							</td>
						</tr>
						<tr style="padding-top:0px">
							<td style="height: 56px">
								<h:commandButton value="Cancel" styleClass="buttonLeft"  id="_0" rendered="#{pc_DrugUsage.show_0}" disabled="#{pc_DrugUsage.disable_0}" action="#{pc_DrugUsage.cancelAction}" onclick="resetTargetFrame(this.form.id);"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_DrugUsage.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_DrugUsage.show_1}" disabled="#{pc_DrugUsage.disable_1}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_DrugUsage.okAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{!pc_Links.showIdDrugUsage}" disabled="#{pc_DrugUsage.disable_2}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_DrugUsage.okAction}" onclick="resetTargetFrame(this.form.id);" id="updateButton" rendered="#{pc_Links.showIdDrugUsage}" disabled="#{pc_DrugUsage.disable_2}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
