<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 3 General Information</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame() {
				getScrollXY('phiinterview');
				document.forms['phiinterview'].target='controlFrame';				
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('phiinterview');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}		
			function toggleCBGroup(formName, prefix, id,indicator) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					   alert(inputs[i].name);					  
						if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
							if(inputs[i].checked==true && indicator==true){
									inputs[i].checked = false ;
									form.submit();
								}else{
									inputs[i].checked = false ;
								}
						}
					}   
				}   
			}	
			function valueChangeEvent() {	
			    getScrollXY('phiinterview');
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE3GENERALINFORMATION" value="#{pc_Page3GeneralInformation}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_PAGINGBOTTOM" value="#{pc_PagingBottom}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>			
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<div id="Messages" style="display: none"><h:messages></h:messages></div>
				<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				   <h:inputHidden id="scrollx" value="#{pc_Page1Identification.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page1Identification.scrollYC}"></h:inputHidden>
				<div class="inputForm">
				    <h:outputLabel value="#{componentBundle.GeneralInformat}" style="width: 508px" id="generalinformation" styleClass="sectionSubheader"  rendered="#{pc_Page3GeneralInformation.showGeneralinformation}"> <!-- NBA324 -->
				    </h:outputLabel>
				     <h:panelGroup>
			                <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 10px;" ></h:outputLabel>
			             </h:panelGroup>
				    <h:panelGroup id="last3yearsid">				     
				       <h:outputLabel value="#{componentBundle.Withinthelastth}" style="width: 450px;text-align:left;margin-left=5px;" styleClass="formLabel" id="flyingtype" rendered="#{pc_Page3GeneralInformation.showFlyingtype}"></h:outputLabel>	
				    <h:outputLabel value="#{componentBundle.Flownasapilotst}" style="width: 450px;text-align:left;margin-left=10px;" styleClass="formLabel" id="flyingtype2" rendered="#{pc_Page3GeneralInformation.showFlyingtype}"></h:outputLabel>
				                    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				                    <h:selectBooleanCheckbox id="Checkbox1_No" value="#{pc_Page3GeneralInformation.prefixNo}" onclick="toggleCBGroup(this.form.id, 'Checkbox1','Checkbox1_No')"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox1_Yes" value="#{pc_Page3GeneralInformation.prefixYes}" onclick="toggleCBGroup(this.form.id, 'Checkbox1','Checkbox1_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page3pilotStudentpilotDetails')};"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
				     <h:commandButton id="page3pilotStudentpilotDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasPilotStudentDetails}" 
								onclick="launchDetailsPopUp('page3pilotStudentpilotDetails');" style="margin-right:10px;margin-left:40px;"/>
				    </h:panelGroup>
				    <Help:Link contextId="N1A_H_Pilot" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="N1A_W_Pilot" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    <h:panelGroup id="Engagedid">
				     <h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 15px;" ></h:outputLabel>
				    <h:outputLabel style="width: 450px;text-align:left;margin-left=5px;" value="#{componentBundle.Engagedinanykin}" styleClass="formLabel" id="activitytype" rendered="#{pc_Page3GeneralInformation.showActivitytype}"></h:outputLabel>
				    <h:outputLabel style="width: 450px;text-align:left;margin-left=10px;" value="#{componentBundle.Ifyeslistactivi}" styleClass="formLabel" id="activitytype2" rendered="#{pc_Page3GeneralInformation.showActivitytype}"></h:outputLabel>
				    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;height: 10px;" ></h:outputLabel>
				    <h:selectBooleanCheckbox id="Checkbox2_No" value="#{pc_Page3GeneralInformation.prefixNo_1}" onclick="toggleCBGroup(this.form.id, 'Checkbox2','Checkbox2_No')"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox2_Yes" value="#{pc_Page3GeneralInformation.prefixYes_1}" onclick="toggleCBGroup(this.form.id, 'Checkbox2','Checkbox2_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page3racingscubadetails')};"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>								
								<h:commandButton id="page3racingscubadetails" image="images/circle_i.gif"  styleClass="ovitalic#{pc_Page3GeneralInformation.hasRacingScubaDetails}" onclick="launchDetailsPopUp('page3racingscubadetails');" style="margin-right:10px;margin-left:40px;" rendered="#{pc_Page3GeneralInformation.showActivitytypeinfo}"/>
								
				    </h:panelGroup>
				    <Help:Link contextId="N1B_H_Racing" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="N1B_W_Racing" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    <h:panelGroup id="Engagedinrockid">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 15px;" ></h:outputLabel>
				    <h:outputLabel style="width: 450px;text-align:left;margin-left=5px;" value="#{componentBundle.Engagedinrockor}" styleClass="formLabel" id="sporttype" rendered="#{pc_Page3GeneralInformation.showSporttype}"></h:outputLabel>
				    <h:outputLabel style="width: 450px;text-align:left;margin-left=5px;" value="#{componentBundle.Ifyeslistactivi_0}" styleClass="formLabel" id="sporttype2" rendered="#{pc_Page3GeneralInformation.showSporttype}"></h:outputLabel>
				    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
				    <h:selectBooleanCheckbox id="Checkbox3_No" value="#{pc_Page3GeneralInformation.prefixNo_2}" onclick="toggleCBGroup(this.form.id, 'Checkbox3','Checkbox3_No')"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox3_Yes" value="#{pc_Page3GeneralInformation.prefixYes_2}" onclick="toggleCBGroup(this.form.id, 'Checkbox3','Checkbox3_Yes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page3MountainClimbingDetails')};"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>								
								<h:commandButton id="page3MountainClimbingDetails" image="images/circle_i.gif"  styleClass="ovitalic#{pc_Page3GeneralInformation.hasMountainClimbingDetails}" onclick="launchDetailsPopUp('page3MountainClimbingDetails');" style="margin-right:10px;margin-left:40px;" rendered="#{pc_Page3GeneralInformation.showSporttypeinfo}"/>
									
								 
				    </h:panelGroup>
				     <Help:Link contextId="N1C_H_Climbing" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  <Help:Link contextId="N1C_W_Climbing" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    <h:panelGroup id="Beenid">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 15px;" ></h:outputLabel>
				    <h:outputLabel style="width: 400px;text-align:left;padding-bottom:10px;margin-left=5px;" value="#{componentBundle.Beenconvictedof}" styleClass="formLabel" id="convictedtype" rendered="#{pc_Page3GeneralInformation.showConvictedtype}"></h:outputLabel>
				    <h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
				    <h:selectBooleanCheckbox id="Checkbox4_No" value="#{pc_Page3GeneralInformation.prefixNo_3}" onclick="toggleCBGroup(this.form.id, 'Checkbox4','Checkbox4_No',true);valueChangeEvent();"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox4_Yes" value="#{pc_Page3GeneralInformation.prefixYes_3}" onclick="toggleCBGroup(this.form.id, 'Checkbox4','Checkbox4_Yes',false);valueChangeEvent();submit();" valueChangeListener="#{pc_Page3GeneralInformation.collapseDrivingLicesenceViolations}"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>								
				    
				    </h:panelGroup>
				    <h:panelGroup id="Restrictedid" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">				    
				    <h:outputLabel style="width: 200px;text-align:left;padding-bottom:10px;margin-left=5px;" value="#{componentBundle.RestrictedDates}" styleClass="formLabel" id="restricteddate" rendered="#{pc_Page3GeneralInformation.showRestricteddate}"></h:outputLabel>
				    
				    <h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.restrictedDates}" id="restrictedDates" rendered="#{pc_Page3GeneralInformation.showRestrictedDates}" disabled="#{pc_Page3GeneralInformation.disableRestrictedDates}">
				       <f:convertDateTime pattern="#{property.datePattern}" />
				    </h:inputText>				    
				    <h:commandButton id="page3RestrictedDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasRestrictedDetails}"  onclick="launchDetailsPopUp('page3RestrictedDetails');" style="margin-right:10px;margin-left:40px;" rendered="#{pc_Page3GeneralInformation.showRestricteddateinfo}"/>
							</h:panelGroup>
				    <DynaDiv:DynamicDiv id="resDate" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}" style="position:absolute;overflow:visible;">	
				        <Help:Link contextId="N1D_H_Convictions" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  		<Help:Link contextId="N1D_W_Convictions" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    </DynaDiv:DynamicDiv>
				    <h:panelGroup rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
							<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 5px;" ></h:outputLabel>	
						  <h:outputLabel value="" styleClass="formLabel" style="width: 205px;height: 10px;" ></h:outputLabel>
						<h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.restrictedDates_0}" id="restrictedDates_0" rendered="#{pc_Page3GeneralInformation.showRestrictedDates_0}" disabled="#{pc_Page3GeneralInformation.disableRestrictedDates_0}">
						   <f:convertDateTime pattern="#{property.datePattern}" />
						</h:inputText>
							
				     </h:panelGroup>
				    <h:panelGroup id="Revokedid" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>	
				    <h:outputLabel style="width: 200px;text-align:left;padding-bottom:10px;margin-left=5px;" value="#{componentBundle.RevokedDates}" styleClass="formLabel" id="revokeddate" rendered="#{pc_Page3GeneralInformation.showRevokeddate}"></h:outputLabel>
				    <h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.revokedDates}" id="revokedDates" rendered="#{pc_Page3GeneralInformation.showRevokedDates}" disabled="#{pc_Page3GeneralInformation.disableRevokedDates}">
				      <f:convertDateTime pattern="#{property.datePattern}" />
				    </h:inputText>
				    <h:commandButton id="page3RevokedDatesDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasRevokedDatesDetails}" onclick="launchDetailsPopUp('page3RevokedDatesDetails');" style="margin-right:10px;margin-left:40px;" rendered="#{pc_Page3GeneralInformation.showRevokeddateinfo}"/>				    
								</h:panelGroup>
				    <DynaDiv:DynamicDiv id="resDate" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}" style="position:absolute;overflow:visible;">	
				        <Help:Link contextId="N1D_H_Convictions" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  		<Help:Link contextId="N1D_W_Convictions" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    </DynaDiv:DynamicDiv>
				    <h:panelGroup rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
				    <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 5px;" ></h:outputLabel>	
				    <h:outputLabel value="" styleClass="formLabel" style="width: 205px;height: 10px;" ></h:outputLabel>
				    <h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.revokedDates_0}" id="revokedDates_0" rendered="#{pc_Page3GeneralInformation.showRevokedDates_0}" disabled="#{pc_Page3GeneralInformation.disableRevokedDates_0}">
				      <f:convertDateTime pattern="#{property.datePattern}" />
				    </h:inputText>
				    
				    </h:panelGroup>

			<h:panelGroup id="otherid" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
			<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>	
           <h:outputLabel style="width: 200px;text-align:left;padding-bottom:10px;margin-left=5px;" value="#{componentBundle.Otherlisttypean}" styleClass="formLabel" id="otherdate" rendered="#{pc_Page3GeneralInformation.showOtherdate}"></h:outputLabel>
                  
         
          <h:commandButton id="page3OtherDatesDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasOtherDatesDetails}" onclick="launchDetailsPopUp('page3OtherDatesDetails');" style="margin-right:10px;margin-left:370px;" rendered="#{pc_Page3GeneralInformation.showOtherdateinfo}"/>
								</h:panelGroup>
				    <DynaDiv:DynamicDiv id="resDate" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}" style="position:absolute;overflow:visible;">	
				        <Help:Link contextId="N1D_H_Convictions" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  		<Help:Link contextId="N1D_W_Convictions" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    </DynaDiv:DynamicDiv>
				    <h:panelGroup id="otherid3" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
           <h:selectOneMenu style="width: 420px;margin-left:25px;" id="otherdatedrop1" value="#{pc_Page3GeneralInformation.otherDate1}" rendered="#{pc_Page3GeneralInformation.showOtherdatedrop1}" disabled="#{pc_Page3GeneralInformation.disableOtherdatedrop1}">
										<f:selectItems value="#{pc_Page3GeneralInformation.otherDateDrop1List}"></f:selectItems>
									</h:selectOneMenu>
          				 </h:panelGroup>
				    <h:panelGroup rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
            <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 5px;" ></h:outputLabel>	
           <h:outputLabel value="" styleClass="formLabel" style="width: 205px;height: 10px;" ></h:outputLabel>
          <h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.otherlisttypeanddates}" id="otherlisttypeanddates" rendered="#{pc_Page3GeneralInformation.showOtherlisttypeanddates}" disabled="#{pc_Page3GeneralInformation.disableOtherlisttypeanddates}">
              <f:convertDateTime pattern="#{property.datePattern}" />
           </h:inputText>
           <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 5px;" ></h:outputLabel>
           <h:outputLabel value="" styleClass="formLabel" style="width: 205px;height: 10px;" ></h:outputLabel>
                
          <h:commandButton id="page3OtherDatesDetails1" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasOtherDates1Details}"  onclick="launchDetailsPopUp('page3OtherDatesDetails1');" style="margin-right:10px;margin-left:370px;" rendered="#{pc_Page3GeneralInformation.show_1}"/>
									</h:panelGroup>
         <h:panelGroup  rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
				    <DynaDiv:DynamicDiv id="resDate" rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}" style="position:absolute;overflow:visible;">	
				        <Help:Link contextId="N1D_H_Convictions" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  		<Help:Link contextId="N1D_W_Convictions" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    </DynaDiv:DynamicDiv>
          </h:panelGroup>
          <h:panelGroup  rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
          <h:selectOneMenu style="width: 420px;margin-left:25px;" id="otherdatedrop2" value="#{pc_Page3GeneralInformation.otherDate2}" rendered="#{pc_Page3GeneralInformation.showOtherdatedrop2}" disabled="#{pc_Page3GeneralInformation.disableOtherdatedrop2}">
										<f:selectItems value="#{pc_Page3GeneralInformation.otherDateDrop2List}"></f:selectItems>
									</h:selectOneMenu>          
      
									</h:panelGroup>
				    <h:panelGroup rendered="#{pc_Page3GeneralInformation.expandDrivingLicesenceViolationsInd}">
								<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 5px;" ></h:outputLabel>
								 <h:outputLabel value="" styleClass="formLabel" style="width: 205px;height: 10px;" ></h:outputLabel>
								<h:inputText styleClass="textInput" style="height: 22px; width: 130px" value="#{pc_Page3GeneralInformation.otherlisttypeanddates_0}" id="otherlisttypeanddates_0" rendered="#{pc_Page3GeneralInformation.showOtherlisttypeanddates_0}" disabled="#{pc_Page3GeneralInformation.disableOtherlisttypeanddates_0}">
								   <f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
								
          
			</h:panelGroup>
			<h:panelGroup id="Inthelastid">
			        <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 20px;" ></h:outputLabel>
			<h:outputLabel value="#{componentBundle.Inthelast10year}" style="width: 400px;text-align:left;margin-left=5px;" styleClass="formLabel" id="tobaccoform" rendered="#{pc_Page3GeneralInformation.showTobaccoform}"></h:outputLabel>
			<h:outputLabel value="" styleClass="formLabel" style="width: 210px;height: 10px;" ></h:outputLabel>
			<h:selectBooleanCheckbox id="Checkbox5_No" value="#{pc_Page3GeneralInformation.prefixNo_4}" onclick="toggleCBGroup(this.form.id, 'Checkbox5','Checkbox5_No',true);valueChangeEvent();"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox5_Yes" value="#{pc_Page3GeneralInformation.prefixYes_4}" onclick="toggleCBGroup(this.form.id, 'Checkbox5','Checkbox5_Yes',false);valueChangeEvent();submit();" valueChangeListener="#{pc_Page3GeneralInformation.collapseTobaccoUsage}"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 113px" styleClass="formLabelRight"></h:outputLabel>
								
			</h:panelGroup>
			<Help:Link contextId="N2_H_Tobacco" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
			<Help:Link contextId="N2_W_Tobacco" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
			<h:panelGroup id="Currentid" rendered="#{pc_Page3GeneralInformation.expandTobaccoUsageInd}">
			<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
			<h:outputLabel style="width: 330px;text-align:left;padding-bottom:10px;margin-left=10px;" value="#{componentBundle.CurrentuseWhatt}" styleClass="formLabel" id="quantitypacks" rendered="#{pc_Page3GeneralInformation.showQuantitypacks}"></h:outputLabel>			
			 <h:commandButton id="page3TobaccoUsageDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasTobaccoUsageDetails}" onclick="launchDetailsPopUp('page3TobaccoUsageDetails');" style="margin-left:34px;" rendered="#{pc_Page3GeneralInformation.showQuantitypacksimg}"/>
			</h:panelGroup>
			<h:panelGroup id="Howid" rendered="#{pc_Page3GeneralInformation.expandTobaccoUsageInd}">
			<h:outputLabel style="width: 400px;text-align:left;margin-left=10px;" value="#{componentBundle.Howmanyyearshav}" styleClass="formLabel" id="smokingtime" rendered="#{pc_Page3GeneralInformation.showSmokingtime}"></h:outputLabel>
			<h:outputLabel value="" styleClass="formLabel" style="width: 215px;height: 10px;" ></h:outputLabel>
			<h:inputText styleClass="textInput" style="height: 22px; width: 60px" value="#{pc_Page3GeneralInformation.noofyears}" id="howmanyyears" ></h:inputText>
									<h:selectBooleanCheckbox value="#{pc_Page3GeneralInformation.prefixQuit}" onclick="toggleCBGroup(this.form.name, 'prefixQuit',  'prefix')"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Quit}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>							
						 <h:commandButton id="page3SmokingDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasSmokingDetails}"  onclick="launchDetailsPopUp('page3SmokingDetails');" style="margin-left:43px;" rendered="#{pc_Page3GeneralInformation.showSmokingtimeinfo}"/>		
	</h:panelGroup>
	<h:panelGroup id="Ifid" rendered="#{pc_Page3GeneralInformation.expandTobaccoUsageInd}">
	<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
	<h:outputLabel style="width: 400px;text-align:left;padding-bottom:10px;margin-left=10px;" value="#{componentBundle.Ifquithowlongag}" styleClass="formLabel" id="quittime" rendered="#{pc_Page3GeneralInformation.showQuittime}"></h:outputLabel>
	<h:outputLabel value="" styleClass="formLabel" style="width: 330px;height: 10px;" ></h:outputLabel>	
	<h:commandButton id="page3SmokingQuitDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasSmokingQuitDetails}"  onclick="launchDetailsPopUp('page3SmokingQuitDetails');" style="margin-left:43px;" rendered="#{pc_Page3GeneralInformation.showSmokingtimeinfo}"/>
	</h:panelGroup>
	<h:panelGroup id="Intheid" >
	<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 20px;" ></h:outputLabel>
	<h:outputLabel value="#{componentBundle.Inthelast10year_past}" style="width: 450px;text-align:left;margin-left=5px;" styleClass="formLabel" id="consumedalcohol" rendered="#{pc_Page3GeneralInformation.showConsumedalcohol}"></h:outputLabel>
	<h:outputLabel value="" styleClass="formLabel" style="width: 210px;height: 10px;" ></h:outputLabel>
	<h:selectBooleanCheckbox id="Checkbox6_No" value="#{pc_Page3GeneralInformation.prefixNo_5}" onclick="toggleCBGroup(this.form.id, 'Checkbox6','Checkbox6_No',true);valueChangeEvent();"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
									<h:selectBooleanCheckbox id="Checkbox6_Yes" value="#{pc_Page3GeneralInformation.prefixYes_5}" onclick="toggleCBGroup(this.form.id, 'Checkbox6','Checkbox6_Yes',false);valueChangeEvent();submit();" valueChangeListener="#{pc_Page3GeneralInformation.collapseAlcoholUsage}"></h:selectBooleanCheckbox>
									<h:outputLabel value="#{componentBundle.Yes}" style="width: 113px" styleClass="formLabelRight"></h:outputLabel>							
					
							</h:panelGroup>
							<Help:Link contextId="N3_H_AlcoholUse" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
							<Help:Link contextId="N3_W_AlcoholUse" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
							<h:panelGroup id="Ifcurrentid" rendered="#{pc_Page3GeneralInformation.expandAlcoholUsageInd}">
								<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
							<h:outputLabel style="width: 400px;text-align:left;margin-left=10px;" value="#{componentBundle.Ifyesdatelastus}" styleClass="formLabel" id="currentuse" rendered="#{pc_Page3GeneralInformation.showCurrentuse}"></h:outputLabel>
							<h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 10px;" ></h:outputLabel>
							<h:inputText styleClass="textInput" style="height: 18px; width: 130px" value="#{pc_Page3GeneralInformation.datelastused}" id="ifcurrentusequantityperdayweekmonth" rendered="#{pc_Page3GeneralInformation.showIfcurrentusequantityperdayweekmonth}" disabled="#{pc_Page3GeneralInformation.disableIfcurrentusequantityperdayweekmonth}"><f:convertDateTime pattern="#{property.datePattern}" /></h:inputText>
							<h:outputLabel value="" styleClass="formLabel" style="width: 20px;height: 10px;" ></h:outputLabel>
						<h:commandButton id="page3AlcoholUsageDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasAlcoholUsageDetails}" onclick="launchDetailsPopUp('page3AlcoholUsageDetails');" style="margin-left:30px;" rendered="#{pc_Page3GeneralInformation.showConsumedamtinfo}"/>
								
							</h:panelGroup>
							<h:panelGroup id="Whatid" rendered="#{pc_Page3GeneralInformation.expandAlcoholUsageInd}">
							<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
							<h:outputLabel style="width: 400px;text-align:left;margin-left=10px;" value="#{componentBundle.Whatisthemostyo}" styleClass="formLabel" id="consumedmost" rendered="#{pc_Page3GeneralInformation.showConsumedmost}"></h:outputLabel>
							<h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 10px;" ></h:outputLabel>
							<h:inputText styleClass="textInput" style="height: 18px; width: 130px" value="#{pc_Page3GeneralInformation.alcoholType}" id="whatisthemostyouwouldconsumeinonesitting" ></h:inputText>							
							<h:commandButton id="page3AlcoholMostConsumeDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasAlcoholMostConsumingDetails}" onclick="launchDetailsPopUp('page3AlcoholMostConsumeDetails');" style="margin-left:42px;" rendered="#{pc_Page3GeneralInformation.showConsumedmostinfo}"/>
							</h:panelGroup>
							<h:panelGroup id="howid" rendered="#{pc_Page3GeneralInformation.expandAlcoholUsageInd}">
							<h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
							<h:outputLabel style="width: 400px;text-align:left;margin-left=10px;" value="#{componentBundle.Howoftendoyouco}" styleClass="formLabel" id="consumedamt" rendered="#{pc_Page3GeneralInformation.showConsumedamt}"></h:outputLabel>
							<h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 10px;" ></h:outputLabel>
							<h:selectOneMenu style="width: 130px" id="consumedamtdrop" value="#{pc_Page3GeneralInformation.consumedamt1}" rendered="#{pc_Page3GeneralInformation.showConsumedamtdrop}" disabled="#{pc_Page3GeneralInformation.disableConsumedamtdrop}">
										<f:selectItems value="#{pc_Page3GeneralInformation.consumedAmtDropList}"></f:selectItems>
									</h:selectOneMenu>							
							<h:commandButton id="page3AlcoholConsumptionDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasAlcoholConsumptionDetails}" onclick="launchDetailsPopUp('page3AlcoholConsumptionDetails');" style="margin-left:42px;" rendered="#{pc_Page3GeneralInformation.showConsumedamtinfo}"/>
							<h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 10px;" ></h:outputLabel>
							<h:selectBooleanCheckbox value="#{pc_Page3GeneralInformation.prefixQuit_1}" onclick="toggleCBGroup(this.form.name, 'prefixQuit_1',  'prefix')"></h:selectBooleanCheckbox>
										<h:outputLabel value="#{componentBundle.Quit}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								</h:panelGroup>
							<h:panelGroup id="Ifheid" rendered="#{pc_Page3GeneralInformation.expandAlcoholUsageInd}">
							<h:outputLabel style="width: 400px;text-align:left;margin-left=10px;" value="#{componentBundle.Ifheshehasquito}" styleClass="formLabel" id="quitreason" rendered="#{pc_Page3GeneralInformation.showQuitreason}"></h:outputLabel>
							<h:outputLabel style="width: 400px;text-align:left;margin-left=15px;" value="#{componentBundle.WhendidyouquitW}" styleClass="formLabel" id="quitreason1" ></h:outputLabel>
							<h:outputLabel style="width: 400px;text-align:left;margin-left=15px;" value="#{componentBundle.AbstainThereaso}" styleClass="formLabel" id="quitreason2" ></h:outputLabel>
							<h:outputLabel value="" styleClass="formLabel" style="width: 200px;height: 10px;" ></h:outputLabel>
							<h:inputText styleClass="textInput" style="height: 22px; width: 130px;" value="#{pc_Page3GeneralInformation.whenQuit}" id="quitid" >
							    <f:convertDateTime pattern="#{property.datePattern}" />
							</h:inputText>
							<h:commandButton id="page3AlcoholQuitDetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page3GeneralInformation.hasAlcoholQuitDetails}"  onclick="launchDetailsPopUp('page3AlcoholQuitDetails');" style="margin-left:42px;" rendered="#{pc_Page3GeneralInformation.showQuitreasoninfo}"/>
						    <h:outputLabel value="" styleClass="formLabel" style="width: 450px;height: 10px;" ></h:outputLabel>
								</h:panelGroup>
				
			
	
			
				    
			
						 <f:subview id="tabHeader">
				  <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			   </f:subview>
			
					</div>
					<h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page3GeneralInformation.commitAction}"></h:commandButton>
				
			</h:form>
		</f:view>
		
</body>
</html>
