<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Extremesportss</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
			//-->
		

			
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_EXTREMESPORTSS" value="#{pc_Extremesportss}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_LINKS" value="#{pc_Links}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="property" basename="properties.nbaApplicationData" />
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Extremesportss.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Extremesportss.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="330">
							<col width="180">
								<col width="90">
									<tr style="padding-top: 5px">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.ExtremeSports}" style="width: 220px" id="Extremesports" rendered="#{pc_Extremesportss.showExtremesports}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 15px">
										<td colspan="1">
											<h:outputText style="width: 500px;text-align:left;margin-left:5px" value="#{componentBundle.Haveyoueverpart}" styleClass="formLabel" id="Havesport" rendered="#{pc_Extremesportss.showHavesport}"></h:outputText>
											
										</td>
										<td colspan="2">
										    <h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixNo}" id="prefix_0No" onclick="toggleCBGroup(this.form.id, 'prefix_0No',  'prefix_0',true);valueChangeEvent();"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixYes}" id="prefix_0Yes" valueChangeListener="#{pc_Extremesportss.collapseParticipationDetail}" onclick="toggleCBGroup(this.form.id, 'prefix_0Yes',  'prefix_0',false);valueChangeEvent();submit();" ></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>	
										</td>
									</tr>
								</col>
							</col>
						</col>
						<DynaDiv:DynamicDiv id="Ify" rendered="#{pc_Extremesportss.expandParticipationDetailsInd}">
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText styleClass="formLabel" style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyes}" id="Ifyes" rendered="#{pc_Extremesportss.showIfyes}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Whatistheactivi}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="Whatsport" rendered="#{pc_Extremesportss.showWhatsport}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 180px" id="Whatin" value="#{pc_Extremesportss.whatActivityIn}" rendered="#{pc_Extremesportss.showWhatin}" disabled="#{pc_Extremesportss.disableWhatin}"></h:inputText>							   
							</td>
							<td>
							 <h:commandButton id="Sport" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Sport');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showSport}" styleClass="ovitalic#{pc_Extremesportss.hasSport}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Howlongandoften}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="Howparticipating" rendered="#{pc_Extremesportss.showHowparticipating}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
							    <h:commandButton id="Participating" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Participating');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showParticipating}" styleClass="ovitalic#{pc_Extremesportss.hasParticipating}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Whatisthelocati}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="Whatactivity" rendered="#{pc_Extremesportss.showWhatactivity}"></h:outputText>
										
							</td>
							
							<td colspan="1"></td>
							<td>
							    <h:commandButton id="Activity" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Activity');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showActivity}" styleClass="ovitalic#{pc_Extremesportss.hasActivity}"/>
							</td>
						</tr>
						<tr style="padding-top:5px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Isyourstatusint}" styleClass="formLabel" style="width: 469px;text-align:left;margin-left:30px;" id="Isprofessional" rendered="#{pc_Extremesportss.showIsprofessional}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1"></td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.amateur}" id="amateurNo" onclick="toggleCBGroup(this.form.id, 'amateurNo',  'amateur')" rendered="#{pc_Extremesportss.showAmateur}" disabled="#{pc_Extremesportss.disableAmateur}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Amateur}" styleClass="formLabelRight" style="margin-right:10px;" id="Amateurr" rendered="#{pc_Extremesportss.showAmateurr}"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.professional}" id="amateurYes" onclick="toggleCBGroup(this.form.id, 'amateurYes',  'amateur')" rendered="#{pc_Extremesportss.showProfessional}" disabled="#{pc_Extremesportss.disableProfessional}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Professional}" styleClass="formLabelRight" id="Professionall" rendered="#{pc_Extremesportss.showProfessionall}"></h:outputLabel>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						
						<tr style="padding-top: 15px">
							<td colspan="1" style="padding-top: 15px;">
								<h:outputText style="width: 500px;text-align:left;margin-left:5px" value="#{componentBundle.Haveyouhadanytr}" id="Haveactivity" styleClass="formLabel" rendered="#{pc_Extremesportss.showHaveactivity}"></h:outputText>
							</td>
							<td colspan="2" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixNo_1}" id="prefix_1No" onclick="toggleCBGroup(this.form.id, 'prefix_1No',  'prefix_1')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixYes_1}" id="prefix_1Yes" onclick="toggleCBGroup(this.form.id, 'prefix_1Yes',  'prefix_1');if(document.getElementById(this.id).checked){launchDetailsPopUp('Training')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText styleClass="formLabel" style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyes}" id="Ifyes_0" rendered="#{pc_Extremesportss.showIfyes_0}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td>
								<h:outputText value="#{componentBundle.Whenandwhattype}" styleClass="formLabel" style="width: 300px;text-align:left;margin-left:30px;" id="Whentraining" rendered="#{pc_Extremesportss.showWhentraining}"></h:outputText>
							</td>
							<td></td>
							<td>
							    <h:commandButton id="Training" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Training');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showTraining}" styleClass="ovitalic#{pc_Extremesportss.hasTraining}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width: 600px;text-align:left;margin-left:5px" value="#{componentBundle.Areyouamemberof}" id="Arerules" styleClass="formLabel" rendered="#{pc_Extremesportss.showArerules}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1"></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixNo_2}" id="prefix_2No" onclick="toggleCBGroup(this.form.id, 'prefix_2No',  'prefix_2')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixYes_2}" id="prefix_2Yes" onclick="toggleCBGroup(this.form.id, 'prefix_2Yes',  'prefix_2');if(document.getElementById(this.id).checked){launchDetailsPopUp('What')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText styleClass="formLabel" style="width: 300px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyes}" id="Ifyes_1" rendered="#{pc_Extremesportss.showIfyes_1}"></h:outputText>
							</td>
							<td colspan="1" style="width: 225px"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.WhatisthenameofExtreme}" id="Whatmember" styleClass="formLabel" style="width: 470px;text-align:left;margin-left:30px;" rendered="#{pc_Extremesportss.showWhatmember}"></h:outputText>
							</td>
							<td>
							    <h:commandButton id="What" image="images/circle_i.gif"  onclick="launchDetailsPopUp('What');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showWhat}" styleClass="ovitalic#{pc_Extremesportss.hasWhat}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width: 600px;text-align:left;margin-left:5px" value="#{componentBundle.HaveyoueverbeenExtreme}" id="Haveactivity2" styleClass="formLabel" rendered="#{pc_Extremesportss.showHaveactivity2}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1">
								<h:outputText id="Have" rendered="#{pc_Extremesportss.showHave}"></h:outputText>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixNo_3}" id="prefix_3No" onclick="toggleCBGroup(this.form.id, 'prefix_3No',  'prefix_3')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixYes_3}" id="prefix_3Yes" onclick="toggleCBGroup(this.form.id, 'prefix_3Yes',  'prefix_3');if(document.getElementById(this.id).checked){launchDetailsPopUp('Activity2')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="Activity2" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Activity2');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showActivity2}" styleClass="ovitalic#{pc_Extremesportss.hasActivity2}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" style="padding-top: 15px;"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px">
							<td colspan="1">
								<h:outputText style="width: 360px;text-align:left;margin-left:5px;" value="#{componentBundle.Wouldyouliketoa}" styleClass="formLabel" id="Whatclarification" rendered="#{pc_Extremesportss.showWhatclarification}"></h:outputText>
							</td>
							<td colspan="1" style="width: 225px">
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixNo_4}" id="prefix_4No" onclick="toggleCBGroup(this.form.id, 'prefix_4No',  'prefix_4')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Extremesportss.prefixYes_4}" id="prefix_4Yes" onclick="toggleCBGroup(this.form.id, 'prefix_4Yes',  'prefix_4');if(document.getElementById(this.id).checked){launchDetailsPopUp('Clarification')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
							    <h:commandButton id="Clarification" image="images/circle_i.gif"  onclick="launchDetailsPopUp('Clarification');" style="margin-right:10px;" rendered="#{pc_Extremesportss.showClarification}" styleClass="ovitalic#{pc_Extremesportss.hasClarification}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="1"></td>
							<td colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="2" style="height: 82px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_Extremesportss.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_0" rendered="#{pc_Extremesportss.show_0}" disabled="#{pc_Extremesportss.disable_0}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_Extremesportss.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_Extremesportss.show_1}" disabled="#{pc_Extremesportss.disable_1}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_Extremesportss.okAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{!pc_Links.showIdExtremeSports}" disabled="#{pc_Extremesportss.disable_2}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_Extremesportss.okAction}" onclick="resetTargetFrame(this.form.id);" id="updateButton" rendered="#{pc_Links.showIdExtremeSports}" disabled="#{pc_Extremesportss.disable_2}"></h:commandButton>
							</td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
