<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Underwater Diving</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_UNDERWATERDIVING" value="#{pc_UnderwaterDiving}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_UnderwaterDiving.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_UnderwaterDiving.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="310">
							<col width="80">
								<col width="80">
									<col width="80">
										<col width="80">
											<tr style="padding-top: 5px;">
												<td colspan="5" class="sectionSubheader"> <!-- NBA324 -->
													<h:outputLabel value="#{componentBundle.UnderwaterDivin}" style="width: 220px" id="labelUnderWaterDiving" rendered="#{pc_UnderwaterDiving.showLabelUnderWaterDiving}"></h:outputLabel>
												</td>
											</tr>
											<tr style="padding-top: 5px;">
												<td style="padding-top: 5px;">
													<h:outputText value="#{componentBundle.Howlonghaveyoubeendiving}" styleClass="formLabel" style="width: 330px;text-align:left;margin-left:5px;" id="labelHowLongHaveYouBeenDiving" rendered="#{pc_UnderwaterDiving.showLabelHowLongHaveYouBeenDiving}"></h:outputText>
												</td>
												<td style="padding-top: 5px;"></td>
												<td></td>
												<td></td>
												<td>													
													<h:commandButton id="underWaterHowLongHaveYouBeenDivingDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterHowLongHaveYouBeenDivingDetails');" rendered="#{pc_UnderwaterDiving.showHowLongHaveYouBeenDivingDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdHowLongHaveYouBeenDivingDetails}"/>
												</td>
											</tr>
											<tr style="padding-top: 15px;">
												<td style="padding-top: 5px;">
													<h:outputText value="#{componentBundle.Whatdivingcerti}" styleClass="formLabel" style="width: 330px;text-align:left;margin-left:5px;" id="labelWhatDivingCertifications" rendered="#{pc_UnderwaterDiving.showLabelWhatDivingCertifications}"></h:outputText>
												</td>
												<td style="padding-top: 5px;"></td>
												<td></td>
												<td></td>
												<td>													
													<h:commandButton id="underWaterWhatDivingCertificationsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterWhatDivingCertificationsDetails');" rendered="#{pc_UnderwaterDiving.showWhatDivingCertificationsDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdWhatDivingCertificationsDetails}"/>
												</td>
											</tr>
											<tr style="padding-top: 15px;">
												<td style="padding-top: 5px;">
													<h:outputText value="#{componentBundle.Doyouownorrenty}" styleClass="formLabel" style="width: 330px;text-align:left;margin-left:5px;" id="labelDoYouOwn" rendered="#{pc_UnderwaterDiving.showLabelDoYouOwn}"></h:outputText>
												</td>
												<td style="padding-top: 5px;" colspan="3">
													<h:selectBooleanCheckbox id="ownOrRentYes" value="#{pc_UnderwaterDiving.own}" rendered="#{pc_UnderwaterDiving.showOwn}" disabled="#{pc_UnderwaterDiving.disableOwn}" onclick="toggleCBGroup(this.form.id, 'ownOrRentYes',  'ownOrRent')"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Own}" styleClass="formLabelRight" style="margin-right:5px;" id="_0" rendered="#{pc_UnderwaterDiving.show_0}"></h:outputLabel>
													<h:selectBooleanCheckbox id="ownOrRentNo" value="#{pc_UnderwaterDiving.rent}" rendered="#{pc_UnderwaterDiving.showRent}" disabled="#{pc_UnderwaterDiving.disableRent}" onclick="toggleCBGroup(this.form.id, 'ownOrRentNo',  'ownOrRent')"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Rent}" styleClass="formLabelRight" id="_1" rendered="#{pc_UnderwaterDiving.show_1}"></h:outputLabel>
												</td>
												<td></td>
											</tr>
											<tr>
												<td>
													<h:outputText value="#{componentBundle.Whatarethelocat}" styleClass="formLabel" style="width: 330px;text-align:left;margin-left:5px;" id="labelWhatAreTheLocations" rendered="#{pc_UnderwaterDiving.showLabelWhatAreTheLocations}"></h:outputText>
												</td>
												<td></td>
												<td></td>
												<td></td>
												<td>													
													<h:commandButton id="underWaterWhatAreTheLocationsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterWhatAreTheLocationsDetails');" rendered="#{pc_UnderwaterDiving.showWhatAreTheLocationsDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdWhatAreTheLocationsDetails}"/>
												</td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr style="padding-top: 15px;">
												<td style="padding-top: 5px;">
													<h:outputText value="#{componentBundle.Doyoueverdiveal}" styleClass="formLabel" style="width: 330px;text-align:left;margin-left:5px;" id="labelDoYouEverDiveAlone" rendered="#{pc_UnderwaterDiving.showLabelDoYouEverDiveAlone}"></h:outputText>
												</td>
												<td style="padding-top: 5px;" colspan="2">
													<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.doYouEverDiveAloneNo}" id="doYouEverDiveAloneNo" onclick="toggleCBGroup(this.form.id, 'doYouEverDiveAloneNo',  'doYouEverDiveAlone')"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
													<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.doYouEverDiveAloneYes}" id="doYouEverDiveAloneYes" onclick="toggleCBGroup(this.form.id, 'doYouEverDiveAloneYes',  'doYouEverDiveAlone');if(document.getElementById(this.id).checked){launchDetailsPopUp('underWaterDoYouEverDiveAloneDetails')};"></h:selectBooleanCheckbox>
													<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
												</td>
												<td></td>
												<td align="left" >													
													<h:commandButton id="underWaterDoYouEverDiveAloneDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterDoYouEverDiveAloneDetails');" style="margin-right:15px;" rendered="#{pc_UnderwaterDiving.showDoYouEverDiveAloneDetails}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdDoYouEverDiveAloneDetails}"/>
												</td>
											</tr>
										</col>
									</col>
								</col>
							</col>
						</col>
						<td></td>
						<td></td>
						<tr style="padding-top: 15px;">
							<td colspan="5" style="padding-top: 5px;">
								<h:outputText value="#{componentBundle.Haveyoueverordo}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px;" id="labelHaveYouEver" rendered="#{pc_UnderwaterDiving.showLabelHaveYouEver}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="labelParticipateInCaseDiving" rendered="#{pc_UnderwaterDiving.showLabelParticipateInCaseDiving}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.participateInCaseDivingNo}" id="participateInCaseDivingNo" onclick="toggleCBGroup(this.form.id, 'participateInCaseDivingNo',  'participateInCaseDiving')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.participateInCaseDivingYes}" id="participateInCaseDivingYes" onclick="toggleCBGroup(this.form.id, 'participateInCaseDivingYes',  'participateInCaseDiving');if(document.getElementById(this.id).checked){launchDetailsPopUp('underWaterParticipateInCaseDivingDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="underWaterParticipateInCaseDivingDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterParticipateInCaseDivingDetails');" rendered="#{pc_UnderwaterDiving.showParticipateInCaseDivingDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasParticipateInCaseDivingDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifwreckdivingdo}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="labelIfWreckDiving" rendered="#{pc_UnderwaterDiving.showLabelIfWreckDiving}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.doYouEnterTheWreckageNo}" id="doYouEnterTheWreckageNo" onclick="toggleCBGroup(this.form.id, 'doYouEnterTheWreckageNo',  'doYouEnterTheWreckage')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.doYouEnterTheWreckageYes}" id="doYouEnterTheWreckageYes" onclick="toggleCBGroup(this.form.id, 'doYouEnterTheWreckageYes',  'doYouEnterTheWreckage');if(document.getElementById(this.id).checked){launchDetailsPopUp('underWaterIfWreckDivingDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="underWaterIfWreckDivingDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterIfWreckDivingDetails');" rendered="#{pc_UnderwaterDiving.showIfWreckDivingDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdIfWreckDivingDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="5" style="padding-top: 5px;">
								<h:outputText value="#{componentBundle.HaveyoueverbeenAdvised}" styleClass="formLabel" style="width: 588px;text-align:left;margin-left:5px;" id="labelHaveYouEverBeenAdvised" rendered="#{pc_UnderwaterDiving.showLabelHaveYouEverBeenAdvised}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;">
								<h:outputText value="#{componentBundle.Ifsopleaseprovi}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="labelHaveYouEverBeenAdvisedIfSo" rendered="#{pc_UnderwaterDiving.showLabelHaveYouEverBeenAdvisedIfSo}"></h:outputText>
							</td>
							<td colspan="2">
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.haveYouEverBeenAdvisedIfSoNo}" id="haveYouEverBeenAdvisedIfSoNo" onclick="toggleCBGroup(this.form.id, 'haveYouEverBeenAdvisedIfSoNo',  'haveYouEverBeenAdvisedIfSo')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_UnderwaterDiving.haveYouEverBeenAdvisedIfSoYes}" id="haveYouEverBeenAdvisedIfSoYes" onclick="toggleCBGroup(this.form.id, 'haveYouEverBeenAdvisedIfSoYes',  'haveYouEverBeenAdvisedIfSo');if(document.getElementById(this.id).checked){launchDetailsPopUp('underWaterHaveYouEverBeenAdvisedIfSoDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
							<td>								
								<h:commandButton id="underWaterHaveYouEverBeenAdvisedIfSoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterHaveYouEverBeenAdvisedIfSoDetails');" rendered="#{pc_UnderwaterDiving.showHaveYouEverBeenAdvisedIfSoDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdHaveYouEverBeenAdvicedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2" style="padding-top: 5px;">
								<h:outputText value="#{componentBundle.Pleaseindicatet}" styleClass="formLabel" style="width: 553px;text-align:left;margin-left:5px;" id="labelPleaseIndicateTheTotalNumber" rendered="#{pc_UnderwaterDiving.showLabelPleaseIndicateTheTotalNumber}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="1">
								<h:outputText value="#{componentBundle.Last12Months}" styleClass="formLabelRight" style="width:65px;" id="labelLast12Months" rendered="#{pc_UnderwaterDiving.showLabelLast12Months}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Field12YearsAgo}" styleClass="formLabelRight" style="width:65px;" id="label12YearsAgo" rendered="#{pc_UnderwaterDiving.showLabel12YearsAgo}"></h:outputText>
							</td>
							<td>
								<h:outputText value="#{componentBundle.Next12months}" styleClass="formLabelRight" style="width:65px;" id="labelNext12Months" rendered="#{pc_UnderwaterDiving.showLabelNext12Months}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Depthsto75feet}" styleClass="formLabel" style="width: 300px;" id="LabelDepthTo75Feet" rendered="#{pc_UnderwaterDiving.showLabelDepthTo75Feet}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="depthTo75FeetLast12Months" value="#{pc_UnderwaterDiving.depthTo75FeetLast12Months}" rendered="#{pc_UnderwaterDiving.showDepthTo75FeetLast12Months}" disabled="#{pc_UnderwaterDiving.disableDepthTo75FeetLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="depthTo75Feet12YearsAgo" value="#{pc_UnderwaterDiving.depthTo75Feet12YearsAgo}" rendered="#{pc_UnderwaterDiving.showDepthTo75Feet12YearsAgo}" disabled="#{pc_UnderwaterDiving.disableDepthTo75Feet12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="depthTo75FeetNext12Months" value="#{pc_UnderwaterDiving.depthTo75FeetNext12Months}" rendered="#{pc_UnderwaterDiving.showDepthTo75FeetNext12Months}" disabled="#{pc_UnderwaterDiving.disableDepthTo75FeetNext12Months}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.From75to100feet}" styleClass="formLabel" style="width: 300px;" id="labelFrom75To100Feet" rendered="#{pc_UnderwaterDiving.showLabelFrom75To100Feet}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from75To100FeetLast12Months" value="#{pc_UnderwaterDiving.from75To100FeetLast12Months}" rendered="#{pc_UnderwaterDiving.showFrom75To100FeetLast12Months}" disabled="#{pc_UnderwaterDiving.disableFrom75To100FeetLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from75To100Feet12YearsAgo" value="#{pc_UnderwaterDiving.from75To100Feet12YearsAgo}" rendered="#{pc_UnderwaterDiving.showFrom75To100Feet12YearsAgo}" disabled="#{pc_UnderwaterDiving.disableFrom75To100Feet12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from75To100FeetNext12Months" value="#{pc_UnderwaterDiving.from75To100FeetNext12Months}" rendered="#{pc_UnderwaterDiving.showFrom75To100FeetNext12Months}" disabled="#{pc_UnderwaterDiving.disableFrom75To100FeetNext12Months}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.From100to130fee}" styleClass="formLabel" style="width: 300px;" id="labelFrom100To130Feet" rendered="#{pc_UnderwaterDiving.showLabelFrom100To130Feet}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from100To130FeetLast12Months" value="#{pc_UnderwaterDiving.from100To130FeetLast12Months}" rendered="#{pc_UnderwaterDiving.showFrom100To130FeetLast12Months}" disabled="#{pc_UnderwaterDiving.disableFrom100To130FeetLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from100To130Feet12YearsAgo" value="#{pc_UnderwaterDiving.from100To130Feet12YearsAgo}" rendered="#{pc_UnderwaterDiving.showFrom100To130Feet12YearsAgo}" disabled="#{pc_UnderwaterDiving.disableFrom100To130Feet12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from100To130FeetNext12Months" value="#{pc_UnderwaterDiving.from100To130FeetNext12Months}" rendered="#{pc_UnderwaterDiving.showFrom100To130FeetNext12Months}" disabled="#{pc_UnderwaterDiving.disableFrom100To130FeetNext12Months}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.From130to150fee}" styleClass="formLabel" style="width: 300px;" id="labelFrom130To150Feet" rendered="#{pc_UnderwaterDiving.showLabelFrom130To150Feet}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from130To150FeetLast12Months" value="#{pc_UnderwaterDiving.from130To150FeetLast12Months}" rendered="#{pc_UnderwaterDiving.showFrom130To150FeetLast12Months}" disabled="#{pc_UnderwaterDiving.disableFrom130To150FeetLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from130To150Feet12YearsAgo" value="#{pc_UnderwaterDiving.from130To150Feet12YearsAgo}" rendered="#{pc_UnderwaterDiving.showFrom130To150Feet12YearsAgo}" disabled="#{pc_UnderwaterDiving.disableFrom130To150Feet12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="from130To150FeetNext12Months" value="#{pc_UnderwaterDiving.from130To150FeetNext12Months}" rendered="#{pc_UnderwaterDiving.showFrom130To150FeetNext12Months}" disabled="#{pc_UnderwaterDiving.disableFrom130To150FeetNext12Months}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Over150feet}" styleClass="formLabel" style="width: 300px;" id="labelOver150Feet" rendered="#{pc_UnderwaterDiving.showLabelOver150Feet}"></h:outputText>
							</td>
							<td colspan="1">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="Over150FeetLast12Months" value="#{pc_UnderwaterDiving.over150FeetLast12Months}" rendered="#{pc_UnderwaterDiving.showOver150FeetLast12Months}" disabled="#{pc_UnderwaterDiving.disableOver150FeetLast12Months}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="Over150Feet12YearsAgo" value="#{pc_UnderwaterDiving.over150Feet12YearsAgo}" rendered="#{pc_UnderwaterDiving.showOver150Feet12YearsAgo}" disabled="#{pc_UnderwaterDiving.disableOver150Feet12YearsAgo}"></h:inputText>
							</td>
							<td>
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="Over150Feetnext12Months" value="#{pc_UnderwaterDiving.over150Feetnext12Months}" rendered="#{pc_UnderwaterDiving.showOver150Feetnext12Months}" disabled="#{pc_UnderwaterDiving.disableOver150Feetnext12Months}"></h:inputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td colspan="3">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Maximumdepthobt}" styleClass="formLabel" style="width: 300px;" id="labelMaximumDepthObtained" rendered="#{pc_UnderwaterDiving.showLabelMaximumDepthObtained}"></h:outputText>
							</td>
							<td colspan="2">
								<h:inputText style="width: 80px;" styleClass="formEntryText" id="maximumDepthObtained" value="#{pc_UnderwaterDiving.maximumDepthObtained}" rendered="#{pc_UnderwaterDiving.showMaximumDepthObtained}" disabled="#{pc_UnderwaterDiving.disableMaximumDepthObtained}"></h:inputText>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;">
								<h:outputText value="#{componentBundle.Whenandhowoften}" styleClass="formLabel" style="width: 300px;" id="labelWhenAndHowOftenDoYou" rendered="#{pc_UnderwaterDiving.showLabelWhenAndHowOftenDoYou}"></h:outputText>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td>								
								<h:commandButton id="underWaterWhenAndHowOftenDoYouDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterWhenAndHowOftenDoYouDetails');" rendered="#{pc_UnderwaterDiving.showWhenAndHowOftenDoYouDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasdWhenAndHowOftenDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="4" style="padding-top: 5px;">
								<h:outputText value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" style="width: 588px;text-align:left;margin-left:5px;" id="labelPleaseAddAnyAdditionalInfo" rendered="#{pc_UnderwaterDiving.showLabelPleaseAddAnyAdditionalInfo}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="underWaterPleaseAddAnyAdditionalInfoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('underWaterPleaseAddAnyAdditionalInfoDetails');" rendered="#{pc_UnderwaterDiving.showPleaseAddAnyAdditionalInfoDetailsImg}" styleClass="ovitalic#{pc_UnderwaterDiving.hasAddAnyAditionalInfoDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px; height: 60px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancel" action="#{pc_UnderwaterDiving.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_UnderwaterDiving.showCancel}" disabled="#{pc_UnderwaterDiving.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clear" action="#{pc_UnderwaterDiving.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_UnderwaterDiving.showClear}" disabled="#{pc_UnderwaterDiving.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="ok" action="#{pc_UnderwaterDiving.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdUnderwaterDiving}" disabled="#{pc_UnderwaterDiving.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="ok2" action="#{pc_UnderwaterDiving.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdUnderwaterDiving}" disabled="#{pc_UnderwaterDiving.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
