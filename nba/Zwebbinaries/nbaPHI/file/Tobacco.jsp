<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Tobacco</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_TOBACCO" value="#{pc_Tobacco}"></PopulateBean:Load>
			<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_Tobacco.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Tobacco.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table width="100%" align="center" border="0">
						<tr>
							<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
								<h:outputLabel value="#{componentBundle.Tobacco}" style="width: 220px" id="labelTobacco" rendered="#{pc_Tobacco.showLabelTobacco}"></h:outputLabel>
							</td>
						</tr>
						<tr>
							<td style="padding-top: 15px;">
								<h:outputLabel value="#{componentBundle.Haveyoueverused}" styleClass="formLabel" style="width:400px;text-align:left;" id="labelHaveYouEverUsed" rendered="#{pc_Tobacco.showLabelHaveYouEverUsed}"></h:outputLabel>
							</td>
							<td style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouEverUsedAnyFormOfTobaccoNo}" id="haveYouEverUsedAnyFormOfTobaccoNo" onclick="toggleCBGroup(this.form.id, 'haveYouEverUsedAnyFormOfTobaccoNo',  'haveYouEverUsedAnyFormOfTobacco')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouEverUsedAnyFormOfTobaccoYes}" id="haveYouEverUsedAnyFormOfTobaccoYes" onclick="toggleCBGroup(this.form.id, 'haveYouEverUsedAnyFormOfTobaccoYes',  'haveYouEverUsedAnyFormOfTobacco')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Areyoucurrently}" styleClass="formLabel" style="width:400px;text-align:left;" id="labelAreyouCurrentlyCigrateSmoker" rendered="#{pc_Tobacco.showLabelAreyouCurrentlyCigrateSmoker}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.areYouCurrentlyCigrateSmokerNo}" id="areYouCurrentlyCigrateSmokerNo" onclick="toggleCBGroup(this.form.id, 'areYouCurrentlyCigrateSmokerNo',  'areYouCurrentlyCigrateSmoker')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.areYouCurrentlyCigrateSmokerYes}" id="areYouCurrentlyCigrateSmokerYes" onclick="toggleCBGroup(this.form.id, 'areYouCurrentlyCigrateSmokerYes',  'areYouCurrentlyCigrateSmoker')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Ifsohowmanypack}" style="width:385px;text-align:left;margin-left:15px;padding-bottom:5px;" styleClass="formLabel" id="labelHowManyPackages" rendered="#{pc_Tobacco.showLabelHowManyPackages}"></h:outputLabel>
							</td>
							<td colspan="2">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="howManyPackages" value="#{pc_Tobacco.howManyPackages}" rendered="#{pc_Tobacco.showHowManyPackages}" disabled="#{pc_Tobacco.disableHowManyPackages}"></h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td>
								<h:outputLabel value="#{componentBundle.Haveyoubeenacig}" style="width:400px;text-align:left;" styleClass="formLabel" id="labelHaveYouBeenCigrateSmoker" rendered="#{pc_Tobacco.showLabelHaveYouBeenCigrateSmoker}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouBeenCigrateSmokerNo}" id="haveYouBeenCigrateSmokerNo" onclick="toggleCBGroup(this.form.id, 'haveYouBeenCigrateSmokerNo',  'haveYouBeenCigrateSmoker')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouBeenCigrateSmokerYes}" id="haveYouBeenCigrateSmokerYes" onclick="toggleCBGroup(this.form.id, 'haveYouBeenCigrateSmokerYes',  'haveYouBeenCigrateSmoker')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Ifsowhendidyouq}" style="width:385px;text-align:left;margin-left:15px;" styleClass="formLabel" id="labelIfSoWhenDidQuit" rendered="#{pc_Tobacco.showLabelIfSoWhenDidQuit}"></h:outputLabel>
							</td>
							<td colspan="2">
								<h:inputText style="width: 95px;" styleClass="formEntryText" id="ifSoWhenDidQuit" value="#{pc_Tobacco.ifSoWhenDidQuit}" rendered="#{pc_Tobacco.showIfSoWhenDidQuit}" disabled="#{pc_Tobacco.disableIfSoWhenDidQuit}">
								<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
							</td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Howmanypackages}" style="width:385px;text-align:left;margin-left:15px;padding-bottom:5px;" styleClass="formLabel" id="labelHowManyPackagesPerDayBeforeQuit" rendered="#{pc_Tobacco.showLabelHowManyPackagesPerDayBeforeQuit}"></h:outputLabel>
							</td>
							<td colspan="2">
								<h:inputText style="width: 60px;" styleClass="formEntryText" id="howManyPackagesPerDayBeforeQuit" value="#{pc_Tobacco.howManyPackagesPerDayBeforeQuit}" rendered="#{pc_Tobacco.showHowManyPackagesPerDayBeforeQuit}" disabled="#{pc_Tobacco.disableHowManyPackagesPerDayBeforeQuit}"></h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 20px;">
							<td>
								<h:outputLabel value="#{componentBundle.Doyouuseanyform}" style="width:400px;text-align:left;padding-bottom:5px;" styleClass="formLabel" id="labelDoYouUseAnyFormOfTobacco" rendered="#{pc_Tobacco.showLabelDoYouUseAnyFormOfTobacco}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.doYouUseAnyFormOfTobaccoNo}" id="doYouUseAnyFormOfTobaccoNo" onclick="toggleCBGroup(this.form.id, 'doYouUseAnyFormOfTobaccoNo',  'doYouUseAnyFormOfTobacco')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.doYouUseAnyFormOfTobaccoYes}" id="doYouUseAnyFormOfTobaccoYes" onclick="toggleCBGroup(this.form.id, 'doYouUseAnyFormOfTobaccoYes',  'doYouUseAnyFormOfTobacco');if(document.getElementById(this.id).checked){launchDetailsPopUp('tobaccoDoYouUseAnyFormOfTobaccoDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>								
								<h:commandButton id="tobaccoDoYouUseAnyFormOfTobaccoDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('tobaccoDoYouUseAnyFormOfTobaccoDetails');" style="margin-right:15px;" rendered="#{pc_Tobacco.showDoYouUseAnyFormOfTobaccoDetailsImg}" styleClass="ovitalic#{pc_Tobacco.hasDoYouSeeAnyFormOfTobaccoDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td>
								<h:outputLabel value="#{componentBundle.Haveyouusedanyf}" style="width:400px;text-align:left;" styleClass="formLabel" id="labelHaveYouUsedAnyFormOfTobbaccoOtherThan" rendered="#{pc_Tobacco.showLabelHaveYouUsedAnyFormOfTobbaccoOtherThan}"></h:outputLabel>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouUsedAnyFormOfTobbaccoOtherThanNo}" id="haveYouUsedAnyFormOfTobbaccoOtherThanNo" onclick="toggleCBGroup(this.form.id, 'haveYouUsedAnyFormOfTobbaccoOtherThanNo',  'haveYouUsedAnyFormOfTobbaccoOtherThan')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_Tobacco.haveYouUsedAnyFormOfTobbaccoOtherThanYes}" id="haveYouUsedAnyFormOfTobbaccoOtherThanYes" onclick="toggleCBGroup(this.form.id, 'haveYouUsedAnyFormOfTobbaccoOtherThanYes',  'haveYouUsedAnyFormOfTobbaccoOtherThan')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<h:outputLabel value="#{componentBundle.Ifsowhendidyouq_0}" style="width:385px;text-align:left;margin-left:15px;" styleClass="formLabel" id="labelIfSoWhenDidYouQuit" rendered="#{pc_Tobacco.showLabelIfSoWhenDidYouQuit}"></h:outputLabel>
							</td>
							<td colspan="2">
								<h:inputText style="width: 95px;" styleClass="formEntryText" id="ifSoWhenDidYouQuitTobacco" value="#{pc_Tobacco.ifSoWhenDidYouQuitTobacco}" rendered="#{pc_Tobacco.showIfSoWhenDidYouQuitTobacco}" disabled="#{pc_Tobacco.disableIfSoWhenDidYouQuitTobacco}">
								<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left" style="height: 27px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancel" action="#{pc_Tobacco.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Tobacco.showCancel}" disabled="#{pc_Tobacco.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="Clear" action="#{pc_Tobacco.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Tobacco.showClear}" disabled="#{pc_Tobacco.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="ok" action="#{pc_Tobacco.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdTobacco}" disabled="#{pc_Tobacco.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="okk" action="#{pc_Tobacco.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdTobacco}" disabled="#{pc_Tobacco.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 14px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
