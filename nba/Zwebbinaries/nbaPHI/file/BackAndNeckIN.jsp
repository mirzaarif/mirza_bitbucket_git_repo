<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>back And Neck IN</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			//-->
		  function launchDetailsPopUp(componentID) {
                getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {
                getScrollXY('page');
				}
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_BACKANDNECK" value="#{pc_BackAndNeck}"></PopulateBean:Load>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_BackAndNeck.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_BackAndNeck.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="430">
							<col width="150">
								<col width="50">
									<tr style="padding-top:5px">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.BackandNeck}" style="width: 220px" id="backAndNeckLabel" rendered="#{pc_BackAndNeck.showBackAndNeckLabel}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top:5px">
										<td colspan="3" style="padding-top: 15px;">
											<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.BackAndNeckDoyounoworhaveIN}" styleClass="formLabel" id="backOrNeckTroubleLabel" ></h:outputText>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<tr style="padding-top:5px">
							<td colspan="1"></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.backOrNeckTroubleNo}" onclick="toggleCBGroup(this.form.id, 'backOrNeckTroubleNo',  'backOrNeckTrouble')" id="backOrNeckTroubleNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="backOrNeckTrouble"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.backOrNeckTroubleYes}" onclick="toggleCBGroup(this.form.id, 'backOrNeckTroubleYes',  'backOrNeckTrouble');if(document.getElementById(this.id).checked){launchDetailsPopUp('ifYesTroubleDetails')};" id="backOrNeckTroubleYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="backOrNeckTrouble_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="3">
								<h:outputText style="width: 560px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyespleasegive}" styleClass="formLabel" id="ifYesTroubleDetailsLabel" rendered="#{pc_BackAndNeck.showIfYesTroubleDetailsLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1"></td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="ifYesTroubleDetails" onclick="launchDetailsPopUp('ifYesTroubleDetails');" rendered="#{pc_BackAndNeck.showIfYesTroubleDetails}" styleClass="ovitalic#{pc_BackAndNeck.hasifYesTroubleDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.BackAndNeckDoyounoworhavevisitedIN}" styleClass="formLabel" id="visitedChiropractorLabel" rendered="#{pc_BackAndNeck.showVisitedChiropractorLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1">
								<h:outputLabel value="#{componentBundle.Ifyes}" style="width: 74px;text-align:left;margin-left:15px;" styleClass="formLabel" id="ifYesVisitedChiropractorLabel" rendered="#{pc_BackAndNeck.showIfYesVisitedChiropractorLabel}"></h:outputLabel>
							</td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.visitedChiropractorNo}" onclick="toggleCBGroup(this.form.id, 'visitedChiropractorNo',  'visitedChiropractor',true);valueChangeEvent();" id="visitedChiropractorNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="visitedChiropractor"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.visitedChiropractorYes}" onclick="toggleCBGroup(this.form.id, 'visitedChiropractorYes',  'visitedChiropractor',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('lastVisitDetails')};valueChangeEvent();submit();"  id="visitedChiropractorYes" valueChangeListener="#{pc_BackAndNeck.expandVisitDetails}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="visitedChiropractor_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_BackAndNeck.expandVisitDetails}">
						<tr style="padding-top:5px">
							<td colspan="2">
								<h:outputText style="width: 470px;text-align:left;margin-left:30px;" value="#{componentBundle.Whenwasyourlast}" styleClass="formLabel" id="lastVisitDetailsLabel" rendered="#{pc_BackAndNeck.showLastVisitDetailsLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="lastVisitDetails" onclick="launchDetailsPopUp('lastVisitDetails');" rendered="#{pc_BackAndNeck.showLastVisitDetails}" styleClass="ovitalic#{pc_BackAndNeck.haslastVisitDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Whatdiagnostict}" styleClass="formLabel" style="width: 370px;text-align:left;margin-left:30px;" id="diagnosticTestDetailsLabel" rendered="#{pc_BackAndNeck.showDiagnosticTestDetailsLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="diagnosticTestDetails" onclick="launchDetailsPopUp('diagnosticTestDetails');" rendered="#{pc_BackAndNeck.showDiagnosticTestDetails}" styleClass="ovitalic#{pc_BackAndNeck.hasdiagnosticTestDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="2">
								<h:outputText value="#{componentBundle.BackAndNeckWhattreatmentwa}" styleClass="formLabel" style="width: 470px;text-align:left;margin-left:30px;" id="treatmentPrescribedLabel" rendered="#{pc_BackAndNeck.showTreatmentPrescribedLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="treatmentPrescribedDetails" onclick="launchDetailsPopUp('treatmentPrescribedDetails');" rendered="#{pc_BackAndNeck.showTreatmentPrescribedDetails}" styleClass="ovitalic#{pc_BackAndNeck.hastreatmentPrescribedDetails}"></h:commandButton>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top:5px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.BackAndNeckWithinthepastIN}" styleClass="formLabel" id="hospitalizedForBackAndNeckLabel" rendered="#{pc_BackAndNeck.showHospitalizedForBackAndNeckLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1"></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.hospitalizedForBackAndNeckNo}" onclick="toggleCBGroup(this.form.id, 'hospitalizedForBackAndNeckNo',  'hospitalizedForBackAndNeck')" id="hospitalizedForBackAndNeckNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="hospitalizedForBackAndNeck"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.hospitalizedForBackAndNeckYes}" onclick="toggleCBGroup(this.form.id, 'hospitalizedForBackAndNeckYes',  'hospitalizedForBackAndNeck');if(document.getElementById(this.id).checked){launchDetailsPopUp('hospitalizedForBackAndNeckDetails')};" id="hospitalizedForBackAndNeckYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="hospitalizedForBackAndNeck_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px">
							<td>
								<h:outputText style="width: 402px;text-align:left;margin-left:15px;" value="#{componentBundle.Ifyespleasegive_0}" styleClass="formLabel" id="hospitalizedForBackAndNeckDetailsLabel" rendered="#{pc_BackAndNeck.showHospitalizedForBackAndNeckDetailsLabel}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="hospitalizedForBackAndNeckDetails" onclick="launchDetailsPopUp('hospitalizedForBackAndNeckDetails');" rendered="#{pc_BackAndNeck.showHospitalizedForBackAndNeckDetails}" styleClass="ovitalic#{pc_BackAndNeck.hashospitalizedForBackAndNeckDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.BackAndNeckWithinthepast5IN}" styleClass="formLabel" id="missedWorkLabel" rendered="#{pc_BackAndNeck.showMissedWorkLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1"></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.missedWorkNo}" onclick="toggleCBGroup(this.form.id, 'missedWorkNo',  'missedWork')" id="missedWorkNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="missedWork"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.missedWorkYes}" onclick="toggleCBGroup(this.form.id, 'missedWorkYes',  'missedWork');if(document.getElementById(this.id).checked){launchDetailsPopUp('missedWorkDetails')};" id="missedWorkYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="missedWork_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Ifsowhenandhowm}" styleClass="formLabel" style="width: 402px;text-align:left;margin-left:15px;" id="missedWorkDetailsLabel" rendered="#{pc_BackAndNeck.showMissedWorkDetailsLabel}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="missedWorkDetails" onclick="launchDetailsPopUp('missedWorkDetails');" rendered="#{pc_BackAndNeck.showMissedWorkDetails}" styleClass="ovitalic#{pc_BackAndNeck.hasmissedWorkDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="3" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhavetolimi}" styleClass="formLabel" id="limitActivitesLabel" rendered="#{pc_BackAndNeck.showLimitActivitesLabel}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1"></td>
							<td colspan="1">
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.limitActivitesNo}" onclick="toggleCBGroup(this.form.id, 'limitActivitesNo',  'limitActivites')" id="limitActivitesNo"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight" id="limitActivites"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_BackAndNeck.limitActivitesYes}" onclick="toggleCBGroup(this.form.id, 'limitActivitesYes',  'limitActivites');if(document.getElementById(this.id).checked){launchDetailsPopUp('limitActivitesDetails')};" id="limitActivitesYes"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight" id="limitActivites_0"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="1">
								<h:outputText value="#{componentBundle.Ifyeswhatlimita}" styleClass="formLabel" style="width: 402px;text-align:left;margin-left:15px;" id="limitActivitesDetailsLabel" rendered="#{pc_BackAndNeck.showLimitActivitesDetailsLabel}"></h:outputText>
							</td>
							<td colspan="1"></td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="limitActivitesDetails" onclick="launchDetailsPopUp('limitActivitesDetails');" rendered="#{pc_BackAndNeck.showLimitActivitesDetails}" styleClass="ovitalic#{pc_BackAndNeck.haslimitActivitesDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top:5px">
							<td colspan="2" style="padding-top: 15px;">
								<h:outputText style="width: 534px;text-align:left;margin-left:5px;" value="#{componentBundle.Pleaseaddanyadd}" styleClass="formLabel" id="additionalDetailsLabel" rendered="#{pc_BackAndNeck.showAdditionalDetailsLabel}"></h:outputText>
							</td>
							<td>
								<h:commandButton image="images/circle_i.gif" style="margin-right:10px;" id="additionalDetails" onclick="launchDetailsPopUp('additionalDetails');" rendered="#{pc_BackAndNeck.showAdditionalDetails}" styleClass="ovitalic#{pc_BackAndNeck.hasadditionalDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px">
							<td colspan="5" align="left" style="height: 56px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" id="cancelButton" action="#{pc_BackAndNeck.cancelAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_BackAndNeck.showCancelButton}" disabled="#{pc_BackAndNeck.disableCancelButton}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" id="clearButton" action="#{pc_BackAndNeck.clearAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_BackAndNeck.showClearButton}" disabled="#{pc_BackAndNeck.disableClearButton}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" id="okButton" action="#{pc_BackAndNeck.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{!pc_Links.showIdBackAndNeck}" disabled="#{pc_BackAndNeck.disableOkButton}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_BackAndNeck.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdBackAndNeck}" disabled="#{pc_BackAndNeck.disableOkButton}"></h:commandButton>
							</td>							
						</tr>
						<tr>
							<td style="height: 20px"></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
