<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>links</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
		    getScrollXY('page');
			}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_LINKS" value="#{pc_Links}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<div class="inputForm">
					<table align="left" border="0" width="150" cellpadding="0" cellspacing="0">
						<tr>
							<td align="right" style="width:6px">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idAerialSports" rendered="#{pc_Links.showIdAerialSports}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px; padding-top: 10px">
								<h:commandLink id="_0" action="#{pc_Links.actionAerialSports}" rendered="#{pc_Links.show_0}">
									<h:outputText value="#{componentBundle.AerialSports}" styleClass="formLabel" style="width: 140px;text-align:left;padding-bottom:5px;" id="idAerialSportsText" rendered="#{pc_Links.showIdAerialSportsText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idalcohol" rendered="#{pc_Links.showIdalcohol} }"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_1" rendered="#{pc_Links.show_1}">
									<h:outputText value="#{componentBundle.Alcohol}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idAlcoholText" rendered="#{pc_Links.showIdAlcoholText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idArthritis" rendered="#{pc_Links.showIdArthritis}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_2" rendered="#{pc_Links.show_2}">
									<h:outputText value="#{componentBundle.Arthritis}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idArthritisText" rendered="#{pc_Links.showIdArthritisText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idAviation" rendered="#{pc_Links.showIdAviation}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_3" rendered="#{pc_Links.show_3}">
									<h:outputText value="#{componentBundle.Aviation}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idAviationText" rendered="#{pc_Links.showIdAviationText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idBackAndNeck" rendered="#{pc_Links.showIdBackAndNeck}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_4" rendered="#{pc_Links.show_4}">
									<h:outputText value="#{componentBundle.BackandNeck}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idBackAndNeckText" rendered="#{pc_Links.showIdBackAndNeckText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idChestPain" rendered="#{pc_Links.showIdChestPain}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_5" rendered="#{pc_Links.show_5}">
									<h:outputText value="#{componentBundle.ChestPain}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idChestPainText" rendered="#{pc_Links.showIdChestPainText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idColonoscopy" rendered="#{pc_Links.showIdColonoscopy}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_6" rendered="#{pc_Links.show_6}">
									<h:outputText value="#{componentBundle.Colonoscopy}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idColonoscopyText" rendered="#{pc_Links.showIdColonoscopyText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDiabetic" rendered="#{pc_Links.showIdDiabetic}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_7" rendered="#{pc_Links.show_7}">
									<h:outputText value="#{componentBundle.Diabetic}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="id_QYDF90Gk" rendered="#{pc_Links.showId_QYDF90Gk}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDriving" rendered="#{pc_Links.showIdDriving}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_8" rendered="#{pc_Links.show_8}">
									<h:outputText value="#{componentBundle.Driving}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idDrivingText" rendered="#{pc_Links.showIdDrivingText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idDrugUsage" rendered="#{pc_Links.showIdDrugUsage}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_9" rendered="#{pc_Links.show_9}">
									<h:outputText value="#{componentBundle.DrugUsage}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idDrugUsageText" rendered="#{pc_Links.showIdDrugUsageText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idExtremeSports" rendered="#{pc_Links.showIdExtremeSports}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_10" rendered="#{pc_Links.show_10}" >
									<h:outputText value="#{componentBundle.ExtremeSports}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idExtremeSportsText" rendered="#{pc_Links.showIdExtremeSportsText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idForeignTravel" rendered="#{pc_Links.showIdForeignTravel}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_11" rendered="#{pc_Links.show_11}">
									<h:outputText value="#{componentBundle.ForeignTravel}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idForeignTravelText" rendered="#{pc_Links.showIdForeignTravelText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idGastrointestinal" rendered="#{pc_Links.showIdGastrointestinal}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink style="" id="_12" rendered="#{pc_Links.show_12}">
									<h:outputText value="#{componentBundle.Gastrointestina}" styleClass="formLabel" style="width: 140px;text-align:left;padding-bottom:5px;" id="idGastrointestinalText" rendered="#{pc_Links.showIdGastrointestinalText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idMilitary" rendered="#{pc_Links.showIdMilitary}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_13" rendered="#{pc_Links.show_13}">
									<h:outputText value="#{componentBundle.Military}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idMilitaryText" rendered="#{pc_Links.showIdMilitaryText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idOAA" rendered="#{pc_Links.showIdOAA}"></h:outputText>
							</td>
							<td align="left" style="padding-bottom: 5px;">
								<h:commandLink id="_14" rendered="#{pc_Links.show_14}"></h:commandLink>
								<h:commandLink id="_15" rendered="#{pc_Links.show_15}">
									<h:outputText value="#{componentBundle.OlderAgeApplica}" styleClass="formLabel" style="width: 99px;text-align:left;padding-bottom:5px;" id="idOAAText" rendered="#{pc_Links.showIdOAAText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idPsych" rendered="#{pc_Links.showIdPsych}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_16" rendered="#{pc_Links.show_16}">
									<h:outputText value="#{componentBundle.Psych}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idPsychText" rendered="#{pc_Links.showIdPsychText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idRacing" rendered="#{pc_Links.showIdRacing}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_17" rendered="#{pc_Links.show_17}">
									<h:outputText value="#{componentBundle.Racing}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idRacingText" rendered="#{pc_Links.showIdRacingText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idRespiratory" rendered="#{pc_Links.showIdRespiratory}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_18" rendered="#{pc_Links.show_18}" >
									<h:outputText value="#{componentBundle.Respiratory}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idRespiratoryText" rendered="#{pc_Links.showIdRespiratoryText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idSleepApnea" rendered="#{pc_Links.showIdSleepApnea}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_19" rendered="#{pc_Links.show_19}" >
									<h:outputText value="#{componentBundle.SleepApnea}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idSleepApneaText" rendered="#{pc_Links.showIdSleepApneaText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idStoli" rendered="#{pc_Links.showIdStoli}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_20" rendered="#{pc_Links.show_20}" >
									<h:outputText value="#{componentBundle.STOLI}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idStoliText" rendered="#{pc_Links.showIdStoliText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idTobacco" rendered="#{pc_Links.showIdTobacco}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_21" rendered="#{pc_Links.show_21}" >
									<h:outputText value="#{componentBundle.Tobacco}" styleClass="formLabel" style="width: 130px;text-align:left;padding-bottom:5px;" id="idTobaccoText" rendered="#{pc_Links.showIdTobaccoText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h:outputText value="#{componentBundle.Field}" styleClass="formLabel" style="width: 3px;margin-left:5px;" id="idUnderwaterDiving" rendered="#{pc_Links.showIdUnderwaterDiving}"></h:outputText>
							</td>
							<td style="padding-bottom: 5px;">
								<h:commandLink id="_22" rendered="#{pc_Links.show_22}" >
									<h:outputText value="#{componentBundle.UnderwaterDivin}" styleClass="formLabel" style="width: 117px;text-align:left;padding-bottom:5px;" id="idUnderwaterDivingText" rendered="#{pc_Links.showIdUnderwaterDivingText}"></h:outputText>
								</h:commandLink>
							</td>
						</tr>
                                  </table>
                          </div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
