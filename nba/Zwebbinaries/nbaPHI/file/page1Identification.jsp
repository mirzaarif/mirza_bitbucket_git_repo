<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB011         NB-1101	       PHI-->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>page 1 Identification</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
        <script type="text/javascript" src="javascript/global/file.js"></script>
        <script type="text/javascript" src="javascript/global/scroll.js"></script>
        <script type="text/javascript" src="javascript/global/scroll.js"></script>
        <script type="text/javascript" src="javascript/nbapopup.js"></script>
        <script type="text/javascript" src="javascript/global/desktopComponent.js"></script>        
        <script type="text/javascript">

			<!--			
			function setTargetFrame() {
				getScrollXY('phiinterview');
				document.forms['phiinterview'].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {
                getScrollXY('phiinterview');			
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}			
			function toggleCBGroup(formName, prefix, id,indicator) {		    	
			   id= formName + ':' +id;			   
			   var form = document.getElementById (formName);				
			   prefix = formName + ':' + prefix;			  
			   var inputs = form.elements; 			  
			   for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].type == "checkbox" && inputs[i].name != id) {	//Iterate over all checkboxes on the form					   alert(inputs[i].name);					  
						if(inputs[i].name.substr(0,prefix.length) == prefix){	//If the cb belongs to the group, identified by prefix, then uncheck it
							if(inputs[i].checked==true && indicator==true){
									inputs[i].checked = false ;
									form.submit();
								}else{
									inputs[i].checked = false ;
								}
						}
					}   
				}   
			}			
			//Method to validate ssn field
			function ssn_Validate(field)
			{
				var strInput = field.value;	
				if(strInput.length == 0)
					return true;
				if (isNaN(strInput)) {					
					raiseError("Social Security Number must be an integer.", field);
					return false;
				}
				if (strInput.length != 9)
				{
					raiseError("9 digits must be entered for the Social Security Number.", field);
					return false;
				}
				
				field.valid = strInput ;
				return true;
			}
			//  Displays a message box and sets focus to a control specified in the field parameter.
			function raiseError(sError, field, type)
			{
				if (!type){
					var ret = window.confirm(sError + "\nPress OK to make the correction or Cancel to revert back to the old value."); 	
					if(field != null && !field.disabled && ret)	{				
						document.getElementById (field.id).focus();	  	  			
					}else{
						field.value = ( (!field.valid) ? "" : field.valid);			
					}
				}
			}
			
			function valueChangeEvent() {
                getScrollXY('phiinterview');
                }		
			//-->
		
       </script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('phiinterview') ">		
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_PAGE1IDENTIFICATION" value="#{pc_Page1Identification}"></PopulateBean:Load>
			<PopulateBean:Load serviceName="RETRIEVE_PAGINGBOTTOM" value="#{pc_PagingBottom}"></PopulateBean:Load>
			<f:loadBundle basename="properties.nbaApplicationData" var="property" />
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentIDBundle" basename="properties.nbaApplicationID"></f:loadBundle>
			<div id="Messages" style="display: none"><h:messages></h:messages></div>
			<h:form styleClass="inputFormMat" id="phiinterview" styleClass="ovDivPHIData">
				<h:inputHidden id="scrollx" value="#{pc_Page1Identification.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_Page1Identification.scrollYC}"></h:inputHidden>				
				<div class="inputForm">						       
				   <h:outputLabel value="#{componentBundle.InsuredInformat}" styleClass="sectionSubheader" style="width: 508px" id="_0" rendered="#{pc_Page1Identification.show_0}"> <!-- NBA324 -->
				   </h:outputLabel>
				   <h:panelGroup id="interviewdateid">
				        <h:outputLabel style="width: 220px;padding-top: 10px;"  value="#{componentBundle.InterviewDate}" styleClass="formLabel" id="interviewdate" rendered="#{pc_Page1Identification.showInterviewdate}"></h:outputLabel>
				        <h:inputText value="#{pc_Page1Identification.interviewDate}" styleClass="formEntryTextHalf" id="textinterviewdate" style="width: 120px;" rendered="#{pc_Page1Identification.showTextinterviewdate}" disabled="#{pc_Page1Identification.disableTextinterviewdate}">
				             <f:convertDateTime pattern="#{property.datePattern}" />
				        </h:inputText>				        	
				   </h:panelGroup>
				   <Help:Link contextId="L1_H_InterviewDate" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				    <h:panelGroup id="insurednameid" style="padding-top: 10px;">
				         <h:outputLabel style="width: 220px;padding-top: 10px;" value="#{componentBundle.ProposedInsured}" styleClass="formLabel" id="legalname" rendered="#{pc_Page1Identification.showLegalname}"></h:outputLabel>
				         <h:inputText value="#{pc_Page1Identification.proposedInsuredsFirstName}" styleClass="formEntryTextHalf" id="textfirstname" style="width: 120px;margin-right:5px;" rendered="#{pc_Page1Identification.showTextfirstname}" disabled="#{pc_Page1Identification.disableTextfirstname}"></h:inputText>
						<h:inputText value="#{pc_Page1Identification.proposedInsuredsMiddleName}" styleClass="formEntryTextHalf" id="textmiddlename" style="width: 100px;" rendered="#{pc_Page1Identification.showTextmiddlename}" disabled="#{pc_Page1Identification.disableTextmiddlename}"></h:inputText>
				   </h:panelGroup>
				    <Help:Link contextId="L2_H_LegalName" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				    <h:panelGroup id="insuredmiddlenameid">
				         <h:outputLabel style="width: 220px;" value="#{componentBundle.FirstMiddleLast}" styleClass="formLabel" id="legalnamedesc" rendered="#{pc_Page1Identification.showLegalnamedesc}"></h:outputLabel>
				         <h:inputText value="#{pc_Page1Identification.proposedInsuredsLastName}" id="textinsuredlastname" styleClass="formEntryTextHalf" style="width: 120px;margin-right:5px;" rendered="#{pc_Page1Identification.showTextinsuredlastname}" disabled="#{pc_Page1Identification.disableTextinsuredlastname}"></h:inputText>
						 <h:inputText id="textinsuredsuffix" value="#{pc_Page1Identification.proposedInsuredsSuffix}" styleClass="formEntryTextHalf" style="width: 100px;" rendered="#{pc_Page1Identification.showTextinsuredsuffix}" disabled="#{pc_Page1Identification.disableTextinsuredsuffix}"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="insuredaddendumid">
				         <h:outputLabel value="#{componentBundle.Addendum}" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="addendum" rendered="#{pc_Page1Identification.showAddendum}"></h:outputLabel>
				         <h:inputText styleClass="formEntryText" style="width: 220px" id="textaddendum" value="#{pc_Page1Identification.addendum}" rendered="#{pc_Page1Identification.showTextaddendum}" disabled="#{pc_Page1Identification.disableTextaddendum}"></h:inputText>
				   </h:panelGroup>
				    <Help:Link contextId="L2A_H_Addendum" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				    <h:panelGroup id="insuredaddressid">
				         <h:outputLabel value="#{componentBundle.MailingAddress}" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="mailingaddress" rendered="#{pc_Page1Identification.showMailingaddress}"></h:outputLabel>
				        <h:inputText value="#{pc_Page1Identification.mailingAddress}" styleClass="formEntryText" style="width: 240px" id="textmailingaddress" rendered="#{pc_Page1Identification.showTextmailingaddress}" disabled="#{pc_Page1Identification.disableTextmailingaddress}"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup  id="insuredaddress2id">
				         <h:outputLabel value="" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="mailingaddress_1" ></h:outputLabel>
				         <h:inputText style="width: 240px;" styleClass="formEntryText" id="textmailingaddress_1" value="#{pc_Page1Identification.mailingAddress_1}" rendered="#{pc_Page1Identification.showTextmailingaddress_1}" disabled="#{pc_Page1Identification.disableTextmailingaddress_1}"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="cityid">
				         <h:outputLabel value="#{componentBundle.City}" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="city" rendered="#{pc_Page1Identification.showCity}"></h:outputLabel>
				       <h:inputText style="width: 240px" id="textcity" value="#{pc_Page1Identification.city}" styleClass="formEntryText" rendered="#{pc_Page1Identification.showTextcity}" disabled="#{pc_Page1Identification.disableTextcity}"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="stateid">
				         <h:outputLabel value="#{componentBundle.StateZip}" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="_1" rendered="#{pc_Page1Identification.show_1}"></h:outputLabel>
				       <h:selectOneMenu style="width: 160px" id="state" value="#{pc_Page1Identification.state}" rendered="#{pc_Page1Identification.showState}" disabled="#{pc_Page1Identification.disableState}">
									<f:selectItems value="#{pc_Page1Identification.statecodeList}"></f:selectItems>
										</h:selectOneMenu>
								<h:inputText style="width: 80px" value="#{pc_Page1Identification.stateZip}" styleClass="formEntryTextHalf" id="statezip" rendered="#{pc_Page1Identification.showStatezip}" disabled="#{pc_Page1Identification.disableStatezip}">
									<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> <!-- SPRNBA-493 -->
								</h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="birthdateid">
				       <h:outputLabel style="width: 220px;padding-top: 10px;" value="#{componentBundle.ProposedInsured_0}" styleClass="formLabel" id="_2" rendered="#{pc_Page1Identification.show_2}"></h:outputLabel>
				       <h:inputText id="birthdate" value="#{pc_Page1Identification.proposedInsuredsBirthDate}" styleClass="formEntryTextHalf" style="width:160;" rendered="#{pc_Page1Identification.showBirthdate}" disabled="#{pc_Page1Identification.disableBirthdate}">
				           <f:convertDateTime pattern="#{property.datePattern}" />
				       </h:inputText>
				   </h:panelGroup>	
			       <h:panelGroup id="ssnid">
				       <h:outputLabel value="#{componentBundle.SocialSecurityN}" styleClass="formLabel" style="width: 220px;padding-top: 10px;" id="_3" rendered="#{pc_Page1Identification.show_3}"></h:outputLabel>
				       <h:inputText value="#{pc_Page1Identification.socialSecurityNumber}" styleClass="formEntryTextHalf" style="width:160;" id="ssn" rendered="#{pc_Page1Identification.showSsn}" disabled="#{pc_Page1Identification.disableSsn}" onblur="ssn_Validate(this);"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="driverslicenseid">
				       <h:outputLabel value="#{componentBundle.DriversLicense}" styleClass="formLabel" style="width: 220px;padding-top: 10px" id="_4" rendered="#{pc_Page1Identification.show_4}"></h:outputLabel>
				       <h:inputText value="#{pc_Page1Identification.driversLicense}" styleClass="formEntryTextHalf" style="width:160;" id="driverslicense" rendered="#{pc_Page1Identification.showDriverslicense}" disabled="#{pc_Page1Identification.disableDriverslicense}"></h:inputText>
				   </h:panelGroup>
				    <h:panelGroup id="driverslicenseSid">
				       	<h:outputLabel value="#{componentBundle.DriversLicenseS}" styleClass="formLabel" style="width: 220px;padding-top: 10px" id="_5" rendered="#{pc_Page1Identification.show_5}"></h:outputLabel>
				       <h:selectOneMenu id="driverslicensestate" value="#{pc_Page1Identification.driversLicenseState}" styleClass="formEntryTextHalf" style="width:160;" rendered="#{pc_Page1Identification.showDriverslicensestate}" disabled="#{pc_Page1Identification.disableDriverslicensestate}">
									<f:selectItems value="#{pc_Page1Identification.driverslicensestateList}"></f:selectItems>
								</h:selectOneMenu>
				   </h:panelGroup>
				   
				   <h:panelGroup id="doyouhaveid">
				       <h:outputLabel style="width: 430px;text-align:left;margin-left:15px;padding-top: 10px" styleClass="formLabel" value="#{componentBundle.Doyouhaveanyadd}" id="_6" rendered="#{pc_Page1Identification.show_6}"></h:outputLabel>
				   </h:panelGroup>
				   
				    <h:panelGroup id="yesnoid">
				       	<h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
				       <h:selectBooleanCheckbox id="prefixNo" value="#{pc_Page1Identification.prefixNo}" onclick="toggleCBGroup(this.form.id, 'prefix','prefixNo')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 23px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox id="prefixYes" value="#{pc_Page1Identification.prefixYes}" onclick="toggleCBGroup(this.form.id, 'prefix','prefixYes');if(document.getElementById(this.id).checked){launchDetailsPopUp('page1additionalendorsementdetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 23px" styleClass="formLabelRight"></h:outputLabel>
								<h:commandButton id="page1additionalendorsementdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasAdditionalEndorsementDetails}" onclick="launchDetailsPopUp('page1additionalendorsementdetails');" style="margin-right:10px;margin-left:50px;"/>
				   </h:panelGroup>
				   
				   <h:panelGroup id="areyouid">
				       	<h:outputLabel style="width: 430px;text-align:left;margin-left:15px;padding-top: 10px;" styleClass="formLabel" value="#{componentBundle.Page1Areyouapermanen}" id="_7" rendered="#{pc_Page1Identification.show_7}"></h:outputLabel>
				       	<h:outputLabel style="width: 420px;text-align:left;margin-left:15px;" value="#{componentBundle.FYIIfeitheraUSC}" styleClass="formLabel" id="_8" rendered="#{pc_Page1Identification.show_8}"></h:outputLabel>
				   </h:panelGroup>
				   
				    <h:panelGroup id="yesno2id">
				       	<h:outputLabel value="" styleClass="formLabel" style="width: 220px;" ></h:outputLabel>
				       	<h:selectBooleanCheckbox id="checkbox2_No" value="#{pc_Page1Identification.prefixNo_1}" onclick="toggleCBGroup(this.form.id,'checkbox2','checkbox2_No');if(document.getElementById(this.id).checked){launchDetailsPopUp('page1residencedetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 23px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox id="checkbox2_Yes" value="#{pc_Page1Identification.prefixYes_1}" onclick="toggleCBGroup(this.form.id,'checkbox2','checkbox2_Yes')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 23px" styleClass="formLabelRight"></h:outputLabel>								
								<h:commandButton id="page1residencedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasResidenceDetails}" onclick="launchDetailsPopUp('page1residencedetails');" style="margin-right:10px;margin-left:50px;"/>		   		
							</h:panelGroup>
				   <Help:Link contextId="L4_H_Resident" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				  
				   <h:panelGroup id="employeeinfoid"  style="padding-top: 10px;">
				   <h:outputLabel value="#{componentBundle.EmploymentInfor}"  styleClass="sectionSubheader"  style="width: 508px;" id="_11" rendered="#{pc_Page1Identification.show_11}"> <!-- NBA324 -->
				   </h:outputLabel> 
				   </h:panelGroup> 
				   <h:panelGroup id="employernameid">
				       	<h:outputLabel style="width: 220px;padding-top: 20px;" value="#{componentBundle.EmployersName}" styleClass="formLabel" id="_12" rendered="#{pc_Page1Identification.show_12}"></h:outputLabel>
				       	<h:inputText value="#{pc_Page1Identification.employersName}" styleClass="formEntryTextHalf" style="width: 110px;" id="employeersname" rendered="#{pc_Page1Identification.showEmployeersname}" disabled="#{pc_Page1Identification.disableEmploymentInfo}" immediate="true"></h:inputText>	
				       	      <h:commandButton id="page1employerdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasEmployerDetails}" onclick="launchDetailsPopUp('page1employerdetails');" style="margin-right:10px;margin-left:33px;" disabled="#{pc_Page1Identification.disableEmploymentInfo}"/>			   						       	      
				   </h:panelGroup>  
				   <Help:Link contextId="M1_H_EmployersName" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   <h:panelGroup id="whattypeofid">
				       	<h:outputLabel style="width: 350px;text-align:left;margin-left:5px;padding-top: 10px;" value="#{componentBundle.Whattypeofprodu}" styleClass="formLabel" id="typeofproducts" rendered="#{pc_Page1Identification.showTypeofproducts}"></h:outputLabel>
								<h:commandButton id="page1servicesdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasServicesDetails}" onclick="launchDetailsPopUp('page1servicesdetails');" style="margin-right:10px;margin-left:7px;" disabled="#{pc_Page1Identification.disableEmploymentInfo}"/>										
				  </h:panelGroup> 
				   <Help:Link contextId="M2_H_ProductsServices" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   <Help:Link contextId="M2_W_ProductsServices" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    <h:panelGroup id="noofemployeesid">
				       	<h:outputLabel value="#{componentBundle.NumberofEmploye}" styleClass="formLabel" style="width: 220px;padding-top: 20px;" id="_16" rendered="#{pc_Page1Identification.show_16}"></h:outputLabel>
								<h:inputText style="width: 45px;margin-bottom:3px;" styleClass="formEntryText" id="noofemployees" value="#{pc_Page1Identification.numberofEmployees}" rendered="#{pc_Page1Identification.showNoofemployees}" disabled="#{pc_Page1Identification.disableEmploymentInfo}"></h:inputText>
								<h:commandButton id="page1employeedetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasEmployeeDetails}" onclick="launchDetailsPopUp('page1employeedetails');" style="margin-left:10px;margin-left: 97px;" disabled="#{pc_Page1Identification.disableEmploymentInfo}"/>										
				   </h:panelGroup> 
				    
				    <h:panelGroup id="douhaveanyid">
				       	<h:outputLabel style="width: 275px;text-align:left;margin-left:5px;padding-top: 10px;" value="#{componentBundle.Doyouhaveanyown}" styleClass="formLabel" id="_18" rendered="#{pc_Page1Identification.show_18}"></h:outputLabel>							
							<h:selectBooleanCheckbox id="checkbox3_No" value="#{pc_Page1Identification.prefixNo_2}" onclick="toggleCBGroup(this.form.id, 'checkbox3','checkbox3_No',true);valueChangeEvent();" valueChangeListener="#{pc_Page1Identification.disablePage2Questions}"   disabled="#{pc_Page1Identification.disableEmploymentInfo}"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 23px" styleClass="formLabelRight" ></h:outputLabel>
								<h:selectBooleanCheckbox id="checkbox3_Yes" value="#{pc_Page1Identification.prefixYes_2}" onclick="toggleCBGroup(this.form.id,'checkbox3','checkbox3_Yes',false);valueChangeEvent();submit();" valueChangeListener="#{pc_Page1Identification.collapseOwnershipInfo}" disabled="#{pc_Page1Identification.disableEmploymentInfo}"  immediate="true"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 23px" styleClass="formLabelRight"></h:outputLabel>
				   </h:panelGroup> 
				   
				   <h:panelGroup id="ifsoid" rendered="#{pc_Page1Identification.expandOwnershipInd}">
				       	<h:outputLabel value="#{componentBundle.Ifsowhatpercent}" styleClass="formLabel" style="width: 230px;padding-top: 10px;" id="_19" rendered="#{pc_Page1Identification.show_19}"></h:outputLabel>
								<h:commandButton id="page1ownershipdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasOwnershipDetails}" onclick="launchDetailsPopUp('page1ownershipdetails');" style="margin-right:27px;margin-left:128px;" disabled="#{pc_Page1Identification.disableEmploymentInfo}"/>								
								
				   </h:panelGroup>
				   
				   <DynaDiv:DynamicDiv id="Ifyes" rendered="#{pc_Page1Identification.expandOwnershipInd}" style="position:absolute;overflow:visible;">
				    <Help:Link contextId="M3_W_Ownership" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
				    </DynaDiv:DynamicDiv>
				  
				    <h:panelGroup id="jobtitleid">
				       	<h:outputLabel style="width: 220px;padding-top: 10px;padding-down: 10px;" value="#{componentBundle.Jobtitleandlist}" styleClass="formLabel" id="_22" rendered="#{pc_Page1Identification.show_22}"></h:outputLabel>
				       	        <h:commandButton id="page1jobdetails" image="images/circle_i.gif" styleClass="ovitalic#{pc_Page1Identification.hasJobDetails}" onclick="launchDetailsPopUp('page1jobdetails');" style="margin-right:10px;margin-left: 139px"/>								
							
				   </h:panelGroup>
				   <Help:Link contextId="M4_H_JobTitlesDuties" location="PHI" imageName="images/help.gif" tooltipText="Help"/>
				   <Help:Link contextId="M4_W_JobTitlesDuties" location="PHI" imageName="images/why.gif" tooltipText="Why"/>
			 
			   <f:subview id="tabHeader">
				  <c:import url="/nbaPHI/file/pagingBottom.jsp" />
			   </f:subview>
			   </div>
			   <h:commandButton style="display:none" id="hiddenSaveButton"	type="submit"  onclick="resetTargetFrame(this.form.id);" action="#{pc_Page1Identification.commitAction}"></h:commandButton>
				
			</h:form>
		</f:view>
</body>
</html>
