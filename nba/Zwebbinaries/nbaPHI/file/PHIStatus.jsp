<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-633     NB-1301	       PHI Correspondence icon not enabling correctly -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<html>
<head>
<base href="<%=basePath%>">
<title>PHI Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
<!-- NBA324 deleted -->
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>	
<script type="text/javascript">
			var contextpath = '<%=path%>';
			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			
			
			//-->
		
</script>


</head>
<body style="overflow-x: hidden; overflow-y: scroll"
	onload="filePageInit();scrollToCoordinates('page') ">
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_PHISTATUS"
		value="#{pc_PHIStatus}"></PopulateBean:Load>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
	<f:loadBundle var="componentBundle"
		basename="properties.nbaApplicationData"></f:loadBundle>
	<h:form styleClass="inputFormMat" id="page">
		<h:inputHidden id="scrollx" value="#{pc_PHIStatus.scrollXC}"></h:inputHidden>
		<h:inputHidden id="scrolly" value="#{pc_PHIStatus.scrollYC}"></h:inputHidden>
		<div class="inputForm">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td><h:selectOneMenu styleClass="textInput" style="width: 269px" id="selectInsured" value="#{pc_PHIStatus.insuredName}" rendered="#{pc_PHIStatus.phiBriefCase}"  disabled="#{pc_PHIStatus.disableSelectInsured}"
					   valueChangeListener="#{pc_PHIStatus.changeInsured}"><f:selectItems value="#{pc_PHIStatus.insuredList}"></f:selectItems>
				    </h:selectOneMenu>
				</td>
				<td align="right">
					 <h:commandButton id="correspondenceimage" image="images/correspondence/correspondence.gif" onclick="resetTargetFrame(this.form.id);" 
								action="#{pc_PHIStatus.launchCorrespondence}" rendered="#{pc_PHIStatus.showCorrespondenceimage}" style="margin-right:70px;"/> <!-- SPRNBA-633 -->		
				</td>
			</tr>
			<tr style="padding-top: 5px;">
			</tr>
			<tr style="padding-top: 10px;">
				<td colspan="2" align="left" class="formTitleBar"> <!-- NBA324 -->
				   <h:outputLabel value="#{componentBundle.ClientInformati}"  id="clientinfo" rendered="#{pc_PHIStatus.showClientinfo}"></h:outputLabel> <!-- NBA324 -->
				</td>
			</tr>
			<tr style="padding-top: 5px;">
				<td align="left" colspan="1">
				   <h:outputLabel value="#{componentBundle.ClientName}" style="width: 130px" styleClass="formLabel" id="clientname" rendered="#{pc_PHIStatus.showClientname}"></h:outputLabel>
				   <h:outputText style="width: 160px" styleClass="formEntryTextHalf" value="#{pc_PHIStatus.clientName}" id="textclientname" 	rendered="#{pc_PHIStatus.showTextclientname}"></h:outputText>
				</td>
				 <td align="left">
				   <h:outputLabel value="#{componentBundle.Nickname}" style="width: 130px" styleClass="formLabel" id="nickname" rendered="#{pc_PHIStatus.showNickname}"></h:outputLabel>
				   <h:outputText style="width: 160px" styleClass="formEntryTextHalf" value="#{pc_PHIStatus.nickName}" id="textnickname" rendered="#{pc_PHIStatus.showTextnickname}"></h:outputText></td>
			</tr>
			<tr style="padding-top: 5px; padding-bottom: 15px;">
				<td align="left" colspan="2">
				  <h:outputLabel id="ContactInfo" value="#{componentBundle.ContactInformat}" style="width: 140px" styleClass="formLabel" rendered="#{pc_PHIStatus.showContactInfo}"></h:outputLabel>
				  <h:outputText style="width: 450px" styleClass="formEntryTextHalf" value="#{pc_PHIStatus.contactInfo}" id="textcontactinfo" rendered="#{pc_PHIStatus.showTextcontactinfo}"></h:outputText></td>
			</tr>
			<tr style="padding-top: 5px;">
				<td colspan="2" align="left" class="sectionSubheader" style=""> <!-- NBA324 -->
				  <h:outputLabel 	id="callinfo" value="#{componentBundle.CallInformation}" rendered="#{pc_PHIStatus.showCallinfo}"></h:outputLabel>
				</td>
			</tr>
			<tr style="padding-top: 10px;">
				<td align="left" colspan="1">
				      <h:outputLabel value="#{componentBundle.InterviewCallba}" style="width: 140px" styleClass="formLabel" id="interviewdate" 	rendered="#{pc_PHIStatus.showInterviewdate}"></h:outputLabel> 
				      <h:inputText styleClass="formEntryTextHalf" style="width: 160px" 	value="#{pc_PHIStatus.interviewdate}" id="textinterviewdate" rendered="#{pc_PHIStatus.showTextinterviewdate}" disabled="#{pc_PHIStatus.disableTextinterviewdate}">
					    <f:convertDateTime pattern="#{property.datePattern}" />
				      </h:inputText>
				   </td>
				<td align="left" valign="bottom"><h:panelGrid border="0"
					columns="3" cellpadding="0" cellspacing="0" style="" id="_0"
					rendered="#{pc_PHIStatus.show_0}">
					<h:outputLabel value="#{componentBundle.InterviewTime}"
						style="width: 130px" styleClass="formLabel" id="interviewtime"
						rendered="#{pc_PHIStatus.showInterviewtime}"></h:outputLabel>
					<h:inputText id="interviewtime_0"
						value="#{pc_PHIStatus.interviewtime}" style="width: 50px"
						styleClass="formEntryDate"
						rendered="#{pc_PHIStatus.showInterviewtime_0}"
						disabled="#{pc_PHIStatus.disableInterviewtime_0}">
						<f:convertDateTime pattern="#{property.shorttimePattern}" />
					</h:inputText>
					<h:selectOneRadio layout="lineDirection"
						enabledClass="formDisplayText"
						style="width: 100px; margin-right: 30px"
						value="#{pc_PHIStatus.interviewtimeAMorPM}"
						id="selectinterviewtime"
						rendered="#{pc_PHIStatus.showSelectinterviewtime}"
						disabled="#{pc_PHIStatus.disableSelectinterviewtime}">
						<f:selectItem itemLabel="#{componentBundle.am}" itemValue="1"></f:selectItem>
						<f:selectItem itemLabel="#{componentBundle.pm}" itemValue="2"></f:selectItem>
					</h:selectOneRadio>
				</h:panelGrid></td>
			</tr>
			<tr style="padding-top: 10px;">
				<td align="left" colspan="1"><h:outputLabel
					value="#{componentBundle.BestPlacetoCall}" style="width: 140px"
					styleClass="formLabel" id="bestplacetocall"
					rendered="#{pc_PHIStatus.showBestplacetocall}"></h:outputLabel> <h:selectOneMenu
					style="width: 160px" styleClass="formEntryTextHalf"
					value="#{pc_PHIStatus.bestplacetocall}" id="bestplacetocall_0"
					rendered="#{pc_PHIStatus.showBestplacetocall_0}"
					disabled="#{pc_PHIStatus.disableBestplacetocall_0}">
					<f:selectItems value="#{pc_PHIStatus.bestPlaceList}"></f:selectItems>
				</h:selectOneMenu></td>
				<td align="left"><h:outputLabel id="homephone"
					value="#{componentBundle.HomePhone}" style="width: 130px"
					styleClass="formLabel" rendered="#{pc_PHIStatus.showHomephone}"></h:outputLabel>
				<h:inputText id="texthomephone" style="width: 140px"
					styleClass="formEntryTextHalf" value="#{pc_PHIStatus.homePhone}"
					rendered="#{pc_PHIStatus.showTexthomephone}"
					disabled="#{pc_PHIStatus.disableTexthomephone}">
					<f:convertNumber type="phone" integerOnly="true"
						pattern="#{property.phonePattern}" />
				</h:inputText></td>
			</tr>
			<tr style="padding-top: 10px;">
				<td align="left" colspan="1"><h:outputLabel id="bestdaytocall"
					value="#{componentBundle.BestDayToCall}" style="width: 140px"
					styleClass="formLabel" rendered="#{pc_PHIStatus.showBestdaytocall}"></h:outputLabel>
				<h:selectOneMenu style="width: 160px" styleClass="formEntryTextHalf"
					value="#{pc_PHIStatus.bestDayToCall}" id="selectbestdaytocall"
					rendered="#{pc_PHIStatus.showSelectbestdaytocall}"
					disabled="#{pc_PHIStatus.disableSelectbestdaytocall}">
					<f:selectItems value="#{pc_PHIStatus.olILUDAYList}"></f:selectItems>
				</h:selectOneMenu></td>
				<td align="left"><h:outputLabel id="workphone"
					value="#{componentBundle.WorkPhone}" style="width: 130px"
					styleClass="formLabel" rendered="#{pc_PHIStatus.showWorkphone}"></h:outputLabel>
				<h:inputText id="selectworkphone" styleClass="formEntryTextHalf"
					style="width: 140px" value="#{pc_PHIStatus.workphone}"
					rendered="#{pc_PHIStatus.showSelectworkphone}"
					disabled="#{pc_PHIStatus.disableSelectworkphone}">
					<f:convertNumber type="phone" integerOnly="true"
						pattern="#{property.phonePattern}" />
				</h:inputText></td>
			</tr>
			<tr style="padding-top: 10px;">
				<td align="left" colspan="1"><h:panelGrid border="0"
					columns="3" cellpadding="0" cellspacing="0"
					style="padding-bottom:15px;" id="_1"
					rendered="#{pc_PHIStatus.show_1}">
					<h:outputLabel id="besttimefrom"
						value="#{componentBundle.BestTimeFrom}" style="width: 140px"
						styleClass="formLabel" rendered="#{pc_PHIStatus.showBesttimefrom}"></h:outputLabel>
					<h:inputText id="textbesttimefrom"
						value="#{pc_PHIStatus.bestTimeToCallFrom}" style="width: 55px"
						styleClass="formEntryTextHalf"
						rendered="#{pc_PHIStatus.showTextbesttimefrom}"
						disabled="#{pc_PHIStatus.disableTextbesttimefrom}">
						<f:convertDateTime pattern="#{property.shorttimePattern}" />
					</h:inputText>
					<h:selectOneRadio layout="lineDirection"
						enabledClass="formDisplayText"
						style="width: 100px; margin-right: 50px"
						value="#{pc_PHIStatus.bestTimeFrom}" id="selectbesttimefrom"
						rendered="#{pc_PHIStatus.showSelectbesttimefrom}"
						disabled="#{pc_PHIStatus.disableSelectbesttimefrom}">
						<f:selectItem itemLabel="#{componentBundle.am}" itemValue="1"></f:selectItem>
						<f:selectItem itemLabel="#{componentBundle.pm}" itemValue="2"></f:selectItem>
					</h:selectOneRadio>
					<h:outputLabel id="besttimeto"
						value="#{componentBundle.BestTimeTo}" style="width: 140px"
						styleClass="formLabel" rendered="#{pc_PHIStatus.showBesttimeto}"></h:outputLabel>
					<h:inputText style="width: 55px"
						value="#{pc_PHIStatus.bestTimeToCallTo}" id="textbesttimeto"
						styleClass="formEntryTextHalf"
						rendered="#{pc_PHIStatus.showTextbesttimeto}"
						disabled="#{pc_PHIStatus.disableTextbesttimeto}">
						<f:convertDateTime pattern="#{property.shorttimePattern}" />
					</h:inputText>
					<h:selectOneRadio id="selectbestTimeto" layout="lineDirection"
						enabledClass="formDisplayText"
						style="width: 100px; margin-right: 50px"
						value="#{pc_PHIStatus.bestTimeTo}" immediate="selectbesttimeto"
						rendered="#{pc_PHIStatus.showSelectbestTimeto}"
						disabled="#{pc_PHIStatus.disableSelectbestTimeto}">
						<f:selectItem itemLabel="#{componentBundle.am}" itemValue="1"></f:selectItem>
						<f:selectItem itemLabel="#{componentBundle.pm}" itemValue="2"></f:selectItem>
					</h:selectOneRadio>
				</h:panelGrid></td>
				<td valign="top"><h:outputLabel id="mobilephone"
					value="#{componentBundle.MobilePhone}" style="width: 130px"
					styleClass="formLabel" rendered="#{pc_PHIStatus.showMobilephone}"></h:outputLabel>
				<h:inputText id="textmobilephone" styleClass="formEntryTextHalf"
					style="width: 140px" value="#{pc_PHIStatus.mobilephone}"
					rendered="#{pc_PHIStatus.showTextmobilephone}"
					disabled="#{pc_PHIStatus.disableTextmobilephone}">
					<f:convertNumber type="phone" integerOnly="true"
						pattern="#{property.phonePattern}" />
				</h:inputText></td>
			</tr>
			<tr style="padding-top: 5px;">
				<td colspan="2"></td>
			</tr>
			<tr style="padding-top: 5px;">
				<td class="sectionSubheaderBar" colspan="2"></td> <!-- NBA324 -->
			</tr>
			<tr style="padding-top: 10px;">
			<td colspan="2">
				<h:panelGroup styleClass="ovDivTableHeader"
					id="requirementspanelgroup"
					rendered="#{pc_PHIStatus.showRequirementspanelgroup}">
					<h:panelGrid columns="2"
						columnClasses="ovColHdrText210,ovColHdrText190"  
						 styleClass="ovTableHeader" cellspacing="0"
						id="requirementspanelgrid"
						rendered="#{pc_PHIStatus.showRequirementspanelgrid}"> <!-- NBA324 -->
						<h:outputText id="requirementsheader"
							value="#{componentBundle.Requirements}"
							rendered="#{pc_PHIStatus.showRequirementsheader}"></h:outputText>
						<h:outputText id="remarksheader"
							value="#{componentBundle.Remarks}"
							rendered="#{pc_PHIStatus.showRemarksheader}"></h:outputText>
					</h:panelGrid>
				</h:panelGroup> 
				<h:panelGrid columns="0" cellspacing="0" cellpadding="0"></h:panelGrid> <!-- NBA324 -->
				<h:panelGroup id="compData1" styleClass="ovDivTableData3">
					<h:dataTable width="100%" id="reqinfo" var="currentRow"
						styleClass="ovTableData" cellspacing="0"
						rowClasses="#{pc_ReqinfoTable.reqinfoRowStyles}"
						value="#{pc_ReqinfoTable.reqinfoList}"
						binding="#{pc_ReqinfoTable.reqinfoTable}"
						columnClasses="ovColText210,ovColText190">
						<h:column id="reqcodec">

							<h:outputText value="#{currentRow.reqcode}"
								styleClass="ovFullCellSelect"></h:outputText>

						</h:column>
						<h:column id="reqdetailsc">

							<h:outputText value="#{currentRow.reqdetails}"
								styleClass="ovFullCellSelect"></h:outputText>

						</h:column>
					</h:dataTable>
				</h:panelGroup>				
			</td>
			</tr>
			<tr style="padding-top: 5px;">
				<td></td>
				<td style="height: 10px"></td>
			</tr>
			<tr style="padding-top: 10px;">
				<td colspan="2" align="left"><h:panelGroup
					id="companioncasespanelgroup" styleClass="ovDivTableHeader"
					rendered="#{pc_PHIStatus.showCompanioncasespanelgroup}">
					<h:panelGrid id="companioncasespanelgrid" columns="4" 
						columnClasses="ovColHdrIcon,ovColHdrText140,ovColHdrText210,ovColHdrText150"
						styleClass="ovTableHeader" cellspacing="0"
						rendered="#{pc_PHIStatus.showCompanioncasespanelgrid}">
						<h:outputText id="overrodeiconheader"
							rendered="#{pc_PHIStatus.showOverrodeiconheader}"></h:outputText>
						<h:outputText id="companioncontractheader"
							value="#{componentBundle.CompanionContra}"
							rendered="#{pc_PHIStatus.showCompanioncontractheader}"></h:outputText>
						<h:outputText id="nameheader" value="#{componentBundle.Name}"
							rendered="#{pc_PHIStatus.showNameheader}"></h:outputText>
						<h:outputText id="overrideheader"
							value="#{componentBundle.Override}"
							rendered="#{pc_PHIStatus.showOverrideheader}"></h:outputText>
					</h:panelGrid>
				</h:panelGroup> 
				<h:panelGrid columns="0" cellspacing="0" cellpadding="0"></h:panelGrid>
				<h:panelGroup id="compData" styleClass="ovDivTableData3">
					<h:dataTable width="100%" id="CompanionCase" var="currentRow"
						styleClass="ovTableData" cellspacing="0"
						rowClasses="#{pc_CompanionCaseTable.companionCaseRowStyles}"
						value="#{pc_CompanionCaseTable.companionCaseList}"
						binding="#{pc_CompanionCaseTable.companionCaseTable}"
						columnClasses="ovColIcon,ovColText140,ovColText210,ovColText150">	<!-- NBA324 -->
						<h:column id="overridesign">

							<h:commandButton id="reqCom"
								image="images/needs_attention/yield.gif"
								rendered="#{currentRow.displayCautionIcon}"
								styleClass="ovViewIconTrue" />




						</h:column>
						<h:column id="contrctnocol">

							<h:outputText value="#{currentRow.contractnumber}"
								styleClass="ovFullCellSelect" style="width:140px;"></h:outputText>

						</h:column>
						<h:column id="primarynameh">

							<h:outputText value="#{currentRow.primaryName}"
								styleClass="ovFullCellSelect" style="width:210px;"></h:outputText>

						</h:column>
						<h:column id="overridedateh">

							<h:outputText value="#{currentRow.overridecolumn}" styleClass="ovFullCellSelect"
								style="width:150px;"></h:outputText>

						</h:column>
					</h:dataTable>
				</h:panelGroup></td>
			</tr>
		
			<tr style="padding-top: 10px;">
				<td colspan="2" align="left"><h:selectBooleanCheckbox
					id="selectcompleted" value="#{pc_PHIStatus.completed}"
					rendered="#{pc_PHIStatus.showSelectcompleted}"
					disabled="#{pc_PHIStatus.disableSelectcompleted}" onclick="submit();" valueChangeListener="#{pc_PHIStatus.phiCompletionAction}" immediate="true"></h:selectBooleanCheckbox>
				<h:outputLabel id="completed" value="#{componentBundle.Completed}"
					style="width:100px" styleClass="formLabelRight"
					rendered="#{pc_PHIStatus.showCompleted}"></h:outputLabel>
				<table align="left"
					style="width: 620; height: 80px; border-spacing: 3pt;">
					<tr></tr>
				</table>
				</td>
			</tr>
			<tr style="padding-top: 10px;">
				<td colspan="2" style="width: 650px" align="left">
					<f:subview id="nbaCommentBar" rendered="#{!pc_PHIStatus.phiBriefCase}">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />
					</f:subview>
				</td>
			</tr>
			<tr style="padding-bottom: 5px;">
				<td colspan="5" align="left" style="height: 50px;">
				<h:commandButton
					id="refreshbutton" value="Refresh" styleClass="buttonLeft"
					action="#{pc_PHIStatus.refreshAction}" 
					onclick="resetTargetFrame(this.form.id);"
					rendered="#{!pc_PHIStatus.phiBriefCase}"
					disabled="#{pc_PHIStatus.disableRefreshbutton}"></h:commandButton>
				<h:commandButton
					id="interviewbutton" immediate="true" value="Interview"
					styleClass="buttonRight-1"
					action="#{pc_PHIStatus.interviewAction}" 
					onclick="resetTargetFrame(this.form.id);"
					rendered="#{pc_PHIStatus.showInterviewbutton}"
					disabled="#{pc_PHIStatus.disableInterviewbutton}"></h:commandButton>
				<h:commandButton id="interviewcommit" value="Commit"
					styleClass="buttonRight"
					action="#{pc_PHIStatus.commitAction}"                      
					onclick="setTargetFrame(this.form.id);"
					rendered="#{!pc_PHIStatus.phiBriefCase}"
					disabled="#{pc_PHIStatus.disableInterviewcommit||pc_PHIStatus.notLocked||pc_PHIStatus.auth.enablement['Commit']||pc_PHIStatus.phiCompleteInd}"></h:commandButton>
				</td>
			</tr>
		</table>
		</div>
	</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages>
	</div>
</f:view>
</body>
</html>
