<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>stoli</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_STOLI" value="#{pc_STOLI}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_STOLI.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_STOLI.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="330">
							<col width="200">
								<col width="100">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.StrangerOrigina}" style="width: 444px" id="strangerid" rendered="#{pc_STOLI.showStrangerid}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="3">
											<h:outputText value="#{componentBundle.Haveyouhadaconv}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="conversationaboutselling" rendered="#{pc_STOLI.showConversationaboutselling}"></h:outputText>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td style="padding-bottom: 5px;"></td>
										<td>
											<h:selectBooleanCheckbox value="#{pc_STOLI.conversationaboutsellingYNNo}" id="conversationaboutsellingYNNo" onclick="toggleCBGroup(this.form.id, 'conversationaboutsellingYNNo',  'conversationaboutsellingYN')"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_STOLI.conversationaboutsellingYNYes}" id="conversationaboutsellingYNYes" onclick="toggleCBGroup(this.form.id, 'conversationaboutsellingYNYes',  'conversationaboutsellingYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stoliconversationaboutsellinginfo')};"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
										<td>
											<h:commandButton id="stoliconversationaboutsellinginfo" image="images/circle_i.gif" style="margin-right:10px;"
											onclick="launchDetailsPopUp('stoliconversationaboutsellinginfo');" rendered="#{pc_STOLI.showConversationaboutsellinginfo}" disabled="#{pc_STOLI.disableConversationaboutsellinginfo}" styleClass="ovitalic#{pc_STOLI.hasConversationAboutSellingDetails}"></h:commandButton>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Haveyoubeenoffe}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="offeredmoneywithapplication" rendered="#{pc_STOLI.showOfferedmoneywithapplication}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.offeredmoneywithapplicationYNNo}" id="offeredmoneywithapplicationYNNo" onclick="toggleCBGroup(this.form.id, 'offeredmoneywithapplicationYNNo',  'offeredmoneywithapplicationYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.offeredmoneywithapplicationYNYes}" id="offeredmoneywithapplicationYNYes" onclick="toggleCBGroup(this.form.id, 'offeredmoneywithapplicationYNYes',  'offeredmoneywithapplicationYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stoliofferedmoneywithapplicationinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="stoliofferedmoneywithapplicationinfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('stoliofferedmoneywithapplicationinfo');" rendered="#{pc_STOLI.showOfferedmoneywithapplicationinfo}" disabled="#{pc_STOLI.disableOfferedmoneywithapplicationinfo}" styleClass="ovitalic#{pc_STOLI.hasOfferMoneyWithApplicationDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Willtherebeanyo}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="anyoneprovidingmoneyforpremium" rendered="#{pc_STOLI.showAnyoneprovidingmoneyforpremium}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.anyoneprovidingmoneyforpremiumYNNo}" id="anyoneprovidingmoneyforpremiumYNNo" onclick="toggleCBGroup(this.form.id, 'anyoneprovidingmoneyforpremiumYNNo',  'anyoneprovidingmoneyforpremiumYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.anyoneprovidingmoneyforpremiumYNYes}" id="anyoneprovidingmoneyforpremiumYNYes" onclick="toggleCBGroup(this.form.id, 'anyoneprovidingmoneyforpremiumYNYes',  'anyoneprovidingmoneyforpremiumYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stolirelationshipinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;"></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifyeswhoandwhat}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="relationshipid" rendered="#{pc_STOLI.showRelationshipid}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="stolirelationshipinfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('stolirelationshipinfo');" rendered="#{pc_STOLI.showRelationshipinfo}" disabled="#{pc_STOLI.disableRelationshipinfo}" styleClass="ovitalic#{pc_STOLI.hasRelationshipDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Doyouintendtobo}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="borrowmoney" rendered="#{pc_STOLI.showBorrowmoney}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifyes}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="borrowmoneyyes" rendered="#{pc_STOLI.showBorrowmoneyyes}"></h:outputText>
							</td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.borrowmoneyyesYNNo}" id="borrowmoneyyesYNNo" onclick="toggleCBGroup(this.form.id, 'borrowmoneyyesYNNo',  'borrowmoneyyesYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.borrowmoneyyesYNYes}" id="borrowmoneyyesYNYes" onclick="toggleCBGroup(this.form.id, 'borrowmoneyyesYNYes',  'borrowmoneyyesYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stoliborrowingfrominfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Whoareyouborrow}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:30px;" id="borrowingfrom" rendered="#{pc_STOLI.showBorrowingfrom}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="stoliborrowingfrominfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('stoliborrowingfrominfo');" rendered="#{pc_STOLI.showBorrowingfrominfo}" disabled="#{pc_STOLI.disableBorrowingfrominfo}" styleClass="ovitalic#{pc_STOLI.hasBorrowingFromDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Whendoestheloan}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:30px;" id="whentorepayloan" rendered="#{pc_STOLI.showWhentorepayloan}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="stoliwhentorepayloaninfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('stoliwhentorepayloaninfo');" rendered="#{pc_STOLI.showWhentorepayloaninfo}" disabled="#{pc_STOLI.disableWhentorepayloaninfo}" styleClass="ovitalic#{pc_STOLI.hasWhenToRepayLoanDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2">
								<h:outputText value="#{componentBundle.Istheresecurity}" styleClass="formLabel" style="width: 419px;text-align:left;margin-left:30px;" id="securityorcollateralforloan" rendered="#{pc_STOLI.showSecurityorcollateralforloan}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.securityorcollateralforloanYNNo}" id="securityorcollateralforloanYNNo" onclick="toggleCBGroup(this.form.id, 'securityorcollateralforloanYNNo',  'securityorcollateralforloanYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.securityorcollateralforloanYNYes}" id="securityorcollateralforloanYNYes" onclick="toggleCBGroup(this.form.id, 'securityorcollateralforloanYNYes',  'securityorcollateralforloanYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stolisecurityorcollateralforloaninfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="stolisecurityorcollateralforloaninfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('stolisecurityorcollateralforloaninfo');" rendered="#{pc_STOLI.showSecurityorcollateralforloaninfo}" disabled="#{pc_STOLI.disableSecurityorcollateralforloaninfo}" styleClass="ovitalic#{pc_STOLI.hasSecurityCollateralForLoanDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Haveyoueversold}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="yousoldpolicy" rendered="#{pc_STOLI.showYousoldpolicy}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td style="padding-bottom: 5px;"></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.yousoldpolicyYNNo}" id="yousoldpolicyYNNo" onclick="toggleCBGroup(this.form.id, 'yousoldpolicyYNNo',  'yousoldpolicyYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.yousoldpolicyYNYes}" id="yousoldpolicyYNYes" onclick="toggleCBGroup(this.form.id, 'yousoldpolicyYNYes',  'yousoldpolicyYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stoliyousoldpolicyinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="stoliyousoldpolicyinfo" image="images/circle_i.gif" style="margin-right:10px;"
								onclick="launchDetailsPopUp('stoliyousoldpolicyinfo');" rendered="#{pc_STOLI.showYousoldpolicyinfo}" disabled="#{pc_STOLI.disableYousoldpolicyinfo}" styleClass="ovitalic#{pc_STOLI.hasYouSoldPolicyDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Haveanylifeinsu}" styleClass="formLabel" style="width: 534px;text-align:left;margin-left:15px;" id="anypolicysold" rendered="#{pc_STOLI.showAnypolicysold}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.anypolicysoldYNNo}" id="anypolicysoldYNNo" onclick="toggleCBGroup(this.form.id, 'anypolicysoldYNNo',  'anypolicysoldYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.anypolicysoldYNYes}" id="anypolicysoldYNYes" onclick="toggleCBGroup(this.form.id, 'anypolicysoldYNYes',  'anypolicysoldYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('stolianypolicysoldinfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td>
								<h:commandButton id="stolianypolicysoldinfo" image="images/circle_i.gif" style="margin-right:10px;"
							onclick="launchDetailsPopUp('stolianypolicysoldinfo');" rendered="#{pc_STOLI.showAnypolicysoldinfo}" disabled="#{pc_STOLI.disableAnypolicysoldinfo}" styleClass="ovitalic#{pc_STOLI.hasAnyPolicySoldDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3">
								<h:outputText value="#{componentBundle.Inthelasttwoyeany}" styleClass="formLabel" style="width: 600px;text-align:left;margin-left:5px" id="completedmedicalexam" rendered="#{pc_STOLI.showCompletedmedicalexam}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td></td>
							<td>
								<h:selectBooleanCheckbox value="#{pc_STOLI.completedmedicalexamYNNo}" id="completedmedicalexamYNNo" onclick="toggleCBGroup(this.form.id, 'completedmedicalexamYNNo',  'completedmedicalexamYN')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_STOLI.completedmedicalexamYNYes}" id="completedmedicalexamYNYes" onclick="toggleCBGroup(this.form.id, 'completedmedicalexamYNYes',  'completedmedicalexamYN');if(document.getElementById(this.id).checked){launchDetailsPopUp('Stoliwhorecommendedexaminfo')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td>
								<h:outputText value="#{componentBundle.Ifyeswhorecomme}" styleClass="formLabel" style="width: 310px;text-align:left;margin-left:15px;" id="whorecommendedexam" rendered="#{pc_STOLI.showWhorecommendedexam}"></h:outputText>
							</td>
							<td></td>
							<td>
								<h:commandButton id="Stoliwhorecommendedexaminfo" image="images/circle_i.gif" style="margin-right:10px;" 
								onclick="launchDetailsPopUp('Stoliwhorecommendedexaminfo');" rendered="#{pc_STOLI.showWhorecommendedexaminfo}" disabled="#{pc_STOLI.disableWhorecommendedexaminfo}" styleClass="ovitalic#{pc_STOLI.hasWhenRecommendedExamDetails}"></h:commandButton>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="height: 59px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_STOLI.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="cancel" rendered="#{pc_STOLI.showCancel}" disabled="#{pc_STOLI.disableCancel}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_STOLI.clearAction}" onclick="resetTargetFrame(this.form.id);" id="clear" rendered="#{pc_STOLI.showClear}" disabled="#{pc_STOLI.disableClear}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_STOLI.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok" rendered="#{!pc_Links.showIdSTOLI}" disabled="#{pc_STOLI.disableOk}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" action="#{pc_STOLI.okAction}" onclick="resetTargetFrame(this.form.id);" id="ok2" rendered="#{pc_Links.showIdSTOLI}" disabled="#{pc_STOLI.disableOk}"></h:commandButton>
							</td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
