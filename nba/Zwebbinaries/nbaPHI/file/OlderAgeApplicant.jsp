<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- FNB004         NB-1101	       PHI-->
<!-- NBA324         NB-1301	       nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<html>
<head>
		<base href="<%=basePath%>">
		<title>Older Age Applicant</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css">
		<link href="theme/accelerator.css" rel="stylesheet" type="text/css">
		<!-- NBA324 deleted -->
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="theme/nbapopup.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript">

			<!--
			function setTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='controlFrame';
				return false;
			}
			function resetTargetFrame(formName) {
				getScrollXY(formName);
				document.forms[formName].target='';
				return false;
			}
			function launchDetailsPopUp(componentID) {	
			    getScrollXY('page');
				detailspopup = launchPopup('detailspopup', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaPHI/file/details.faces?componentID='+componentID, 555, 350);
				detailspopup.focus();
				top.mainContentFrame.contextMenu.detailspopup = detailspopup;
				return true;
			}
			
			function valueChangeEvent() {	
			    getScrollXY('page');
				}
			//-->
		
</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body style="overflow-x: hidden; overflow-y: scroll" onload="filePageInit();scrollToCoordinates('page') ">
		<f:view>
			<PopulateBean:Load serviceName="RETRIEVE_OLDERAGEAPPLICANT" value="#{pc_OlderAgeApplicant}"></PopulateBean:Load>
			<f:loadBundle var="bundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<f:loadBundle var="componentBundle" basename="properties.nbaApplicationData"></f:loadBundle>
			<h:form styleClass="inputFormMat" id="page">
				<h:inputHidden id="scrollx" value="#{pc_OlderAgeApplicant.scrollXC}"></h:inputHidden>
				<h:inputHidden id="scrolly" value="#{pc_OlderAgeApplicant.scrollYC}"></h:inputHidden>
				<div class="inputForm">
					<table align="left" border="0" width="630px" style="cellpadding: 0px; cellspacing: 0px; table-layout: fixed" cellpadding="0" cellspacing="0">
						<col width="330">
							<col width="220">
								<col width="80">
									<tr style="padding-top: 5px;">
										<td colspan="3" class="sectionSubheader" align="left"> <!-- NBA324 -->
											<h:outputLabel value="#{componentBundle.OlderAgeApplica}" style="width: 220px" id="labelOlderAgeApplicant" rendered="#{pc_OlderAgeApplicant.showLabelOlderAgeApplicant}"></h:outputLabel>
										</td>
									</tr>
									<tr style="padding-top: 15px;">
										<td colspan="1" align="left" style="padding-top: 15px;">
											<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Withwhomdoyoure}" styleClass="formLabel" id="labelWithWhom" rendered="#{pc_OlderAgeApplicant.showLabelWithWhom}"></h:outputText>
										</td>
										<td align="left" colspan="1" style="padding-top: 15px;">
											<h:selectOneMenu style="width: 200px" styleClass="formEntryText" id="doYouResideIn" value="#{pc_OlderAgeApplicant.withWhomDoYouReside}" rendered="#{pc_OlderAgeApplicant.showDoYouResideIn}" disabled="#{pc_OlderAgeApplicant.disableDoYouResideIn}">
												<f:selectItems value="#{pc_OlderAgeApplicant.withWhomDoYouResideList}"></f:selectItems>
											</h:selectOneMenu>
										</td>
										<td></td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="1" align="left">
											<h:outputText value="#{componentBundle.Ifotherpleasepr}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelIfOtherPleaseProvide" rendered="#{pc_OlderAgeApplicant.showLabelIfOtherPleaseProvide}"></h:outputText>
										</td>
										<td align="right" colspan="1"></td>
										<td>											
											<h:commandButton id="olderifOtherPleaseProvideDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderifOtherPleaseProvideDetails');" rendered="#{pc_OlderAgeApplicant.showIfOtherPleaseProvideDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasPleaseProvideDetails}"/>
										</td>
									</tr>
									<tr style="padding-top: 5px;">
										<td colspan="1" align="left">
											<h:outputText value="#{componentBundle.Doyouownapet}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelDoYouOwn" rendered="#{pc_OlderAgeApplicant.showLabelDoYouOwn}"></h:outputText>
										</td>
										<td colspan="1" align="left">
											<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouOwnPetNo}" id="doYouOwnPetNo" onclick="toggleCBGroup(this.form.id, 'doYouOwnPetNo',  'doYouOwnPet')"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
											<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouOwnPetYes}" id="doYouOwnPetYes" onclick="toggleCBGroup(this.form.id, 'doYouOwnPetYes',  'doYouOwnPet');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderDoYouOwnPetDetails')};"></h:selectBooleanCheckbox>
											<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
										</td>
										<td>											
											<h:commandButton id="olderDoYouOwnPetDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderDoYouOwnPetDetails');" rendered="#{pc_OlderAgeApplicant.showDoYouOwnPetDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasDoYouOwnPetDetails}"/>
										</td>
									</tr>
								</col>
							</col>
						</col>
						<td></td>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouresidein}" styleClass="formLabel" id="labelDoYouReside" rendered="#{pc_OlderAgeApplicant.showLabelDoYouReside}"></h:outputText>
							</td>
							<td align="left" colspan="1">
								<h:selectOneMenu style="width: 200px" styleClass="formEntryText" id="doYouResideIn_0" value="#{pc_OlderAgeApplicant.doYouResideIn}" rendered="#{pc_OlderAgeApplicant.showDoYouResideIn_0}" disabled="#{pc_OlderAgeApplicant.disableDoYouResideIn_0}">
									<f:selectItems value="#{pc_OlderAgeApplicant.doYouResideInList}"></f:selectItems>
								</h:selectOneMenu>
							</td>
							<td>								
								<h:commandButton id="olderDoYouResideInDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderDoYouResideInDetails');" rendered="#{pc_OlderAgeApplicant.showDoYouResideInDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasDoYouResidentDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveanyrel}" styleClass="formLabel" id="labelDoYouHaveAnyRelatives" rendered="#{pc_OlderAgeApplicant.showLabelDoYouHaveAnyRelatives}"></h:outputText>
							</td>
							<td colspan="1" align="left" style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouHaveAnyRelativesNo}" id="doYouHaveAnyRelativesNo" onclick="toggleCBGroup(this.form.id, 'doYouHaveAnyRelativesNo',  'doYouHaveAnyRelatives')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouHaveAnyRelativesYes}" id="doYouHaveAnyRelativesYes" onclick="toggleCBGroup(this.form.id, 'doYouHaveAnyRelativesYes',  'doYouHaveAnyRelatives');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderRelationShipDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Relationship}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelRelationShip" rendered="#{pc_OlderAgeApplicant.showLabelRelationShip}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderRelationShipDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderRelationShipDetails');" style="margin-right:10px;" rendered="#{pc_OlderAgeApplicant.showRelationShipDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasRelationshipDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.AreyoucurrentlyEmployed}" styleClass="formLabel" id="labelAreYouCurrentlyEmployed" rendered="#{pc_OlderAgeApplicant.showLabelAreYouCurrentlyEmployed}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.areYouCurrentlyEmployedNo}" id="areYouCurrentlyEmployedNo" onclick="toggleCBGroup(this.form.id, 'areYouCurrentlyEmployedNo',  'areYouCurrentlyEmployed',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.areYouCurrentlyEmployedYes}" id="areYouCurrentlyEmployedYes" valueChangeListener="#{pc_OlderAgeApplicant.collapseEmployedDetails}" onclick="toggleCBGroup(this.form.id, 'areYouCurrentlyEmployedYes',  'areYouCurrentlyEmployed',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('olderIfYesFTPTDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="employedIfYes" rendered="#{pc_OlderAgeApplicant.expandEmployedDetailsInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.IfyesFulltimeor}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelIfYesFTPT" rendered="#{pc_OlderAgeApplicant.showLabelIfYesFTPT}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderIfYesFTPTDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderIfYesFTPTDetails');" style="margin-right:10px;" rendered="#{pc_OlderAgeApplicant.showIfYesFTPTDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasIfYesFtpDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.OccupationDutie}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelOccupationDuties" rendered="#{pc_OlderAgeApplicant.showLabelOccupationDuties}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderOccupationDutiesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderOccupationDutiesDetails');" style="margin-right:10px;" rendered="#{pc_OlderAgeApplicant.showOccupationDutiesDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasOccupationDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="3" align="left" style="padding-top: 15px;">
								<h:outputText style="width:600px;text-align:left;margin-left:5px;" value="#{componentBundle.Havetherebeenan}" styleClass="formLabel" id="labelHaveThereBeen" rendered="#{pc_OlderAgeApplicant.showLabelHaveThereBeen}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.haveThereBeenMajorChangesNo}" id="haveThereBeenMajorChangesNo" onclick="toggleCBGroup(this.form.id, 'haveThereBeenMajorChangesNo',  'haveThereBeenMajorChanges')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.haveThereBeenMajorChangesYes}" id="haveThereBeenMajorChangesYes" onclick="toggleCBGroup(this.form.id, 'haveThereBeenMajorChangesYes',  'haveThereBeenMajorChanges');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderHaveThereBeenMajorChangesInLifeDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Ifyespleaseprov}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelIfYesPleaseProvideDetails" rendered="#{pc_OlderAgeApplicant.showLabelIfYesPleaseProvideDetails}"></h:outputText>
							</td>
							<td align="left" colspan="1" class="formEntryText" style="width: 146px"></td>
							<td>
								<h:commandButton id="olderHaveThereBeenMajorChangesInLifeDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderHaveThereBeenMajorChangesInLifeDetails');" style="margin-right:10px;" rendered="#{pc_OlderAgeApplicant.showHaveThereBeenMajorChangesInLifeDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasHaveThereBeenJObDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="3" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 542px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouparticipat}" styleClass="formLabel" id="labelDoYouParticipate" rendered="#{pc_OlderAgeApplicant.showLabelDoYouParticipate}"></h:outputText>
								<h:outputText value="#{componentBundle.churchclubstrav}" styleClass="formLabel" style="width: 542px;text-align:left;margin-left:15px;" id="labelChurchClub" rendered="#{pc_OlderAgeApplicant.showLabelChurchClub}"></h:outputText>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouParticipateInSocialActivitiesNo}" id="doYouParticipateInSocialActivitiesNo" onclick="toggleCBGroup(this.form.id, 'doYouParticipateInSocialActivitiesNo',  'doYouParticipateInSocialActivities',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouParticipateInSocialActivitiesYes}" id="doYouParticipateInSocialActivitiesYes" valueChangeListener="#{pc_OlderAgeApplicant.collapseHobbiesDetails}" onclick="toggleCBGroup(this.form.id, 'doYouParticipateInSocialActivitiesYes',  'doYouParticipateInSocialActivities',false);valueChangeEvent();if(document.getElementById(this.id).checked){launchDetailsPopUp('olderSocialActivitiesDetails')};submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="hobbiesIfYes" rendered="#{pc_OlderAgeApplicant.expandHobbiesDetailsInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Providedetailsa}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelDetailsFrequency" rendered="#{pc_OlderAgeApplicant.showLabelDetailsFrequency}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderSocialActivitiesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderSocialActivitiesDetails');"  rendered="#{pc_OlderAgeApplicant.showSocialActivitiesDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasSocialActivitiesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Doyoudoanyvolun}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelDoYouVolunteer" rendered="#{pc_OlderAgeApplicant.showLabelDoYouVolunteer}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouVolunteerWorkNo}" id="doYouVolunteerWorkNo" onclick="toggleCBGroup(this.form.id, 'doYouVolunteerWorkNo',  'doYouVolunteerWork')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouVolunteerWorkYes}" id="doYouVolunteerWorkYes" onclick="toggleCBGroup(this.form.id, 'doYouVolunteerWorkYes',  'doYouVolunteerWork');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderDoYouVolunteerWorkDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Providedetailsa}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labeldoYouVolunteerWorkDetails" rendered="#{pc_OlderAgeApplicant.showLabeldoYouVolunteerWorkDetails}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderDoYouVolunteerWorkDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderDoYouVolunteerWorkDetails');"  rendered="#{pc_OlderAgeApplicant.showDoYouVolunteerWorkDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasDOYouVolunteerDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouexercisere}" styleClass="formLabel" id="labelDoYouExcerciseRegularly" rendered="#{pc_OlderAgeApplicant.showLabelDoYouExcerciseRegularly}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouExcerciseRegularlyNo}" id="doYouExcerciseRegularlyNo" onclick="toggleCBGroup(this.form.id, 'doYouExcerciseRegularlyNo',  'doYouExcerciseRegularly',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouExcerciseRegularlyYes}" id="doYouExcerciseRegularlyYes" valueChangeListener="#{pc_OlderAgeApplicant.collapseExcerciseDetails}" onclick="toggleCBGroup(this.form.id, 'doYouExcerciseRegularlyYes',  'doYouExcerciseRegularly',false);if(document.getElementById(this.id).checked){launchDetailsPopUp('olderTypeOfExcerciseDetails')};valueChangeEvent();submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="excerciseIfYes" rendered="#{pc_OlderAgeApplicant.expandExcerciseDetailsInd}">
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Typeofexercise}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelTypeOfExcercise" rendered="#{pc_OlderAgeApplicant.showLabelTypeOfExcercise}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderTypeOfExcerciseDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderTypeOfExcerciseDetails');"  rendered="#{pc_OlderAgeApplicant.showTypeOfExcerciseDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasTypeOfExcerciseDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Frequencyandlen}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelFrequencyAndLength" rendered="#{pc_OlderAgeApplicant.showLabelFrequencyAndLength}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderFrequencyAndLengthDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderFrequencyAndLengthDetails');"  rendered="#{pc_OlderAgeApplicant.showFrequencyAndLengthDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasFrequencyAndLengthDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyouhaveavalid}" styleClass="formLabel" id="labelValidDriverLicense" rendered="#{pc_OlderAgeApplicant.showLabelValidDriverLicense}"></h:outputText>
							</td>
							<td colspan="1" align="left" style="padding-top: 15px;">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouhaveValidDriverLicenseNo}" id="doYouhaveValidDriverLicenseNo" onclick="toggleCBGroup(this.form.id, 'doYouhaveValidDriverLicenseNo',  'doYouhaveValidDriverLicense',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouhaveValidDriverLicenseYes}" id="doYouhaveValidDriverLicenseYes" valueChangeListener="#{pc_OlderAgeApplicant.collapseDriverLiceenseDetails}" onclick="toggleCBGroup(this.form.id, 'doYouhaveValidDriverLicenseYes',  'doYouhaveValidDriverLicense',false);valueChangeEvent();if(document.getElementById(this.id).checked){launchDetailsPopUp('olderWhenLicesenceRenewedDetails')};submit();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="validDriverLicenseIfYes" rendered="#{pc_OlderAgeApplicant.expandDriverLiceenseDetailsInd}">
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Whenwasyourlice}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelWhenLicesenceRenewed" rendered="#{pc_OlderAgeApplicant.showLabelWhenLicesenceRenewed}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderWhenLicesenceRenewedDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderWhenLicesenceRenewedDetails');"  rendered="#{pc_OlderAgeApplicant.showWhenLicesenceRenewedDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasWhenLicenseDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Anyrestrictions}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelAnyRestrictions" rendered="#{pc_OlderAgeApplicant.showLabelAnyRestrictions}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderAnyRestrictionsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderAnyRestrictionsDetails');"  rendered="#{pc_OlderAgeApplicant.showAnyRestrictionsDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasAnyRestrictionDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Howoftendoyoudr}" styleClass="formLabel" style="width: 434px;text-align:left;margin-left:30px" id="labelHowOftenDoYouDrive" rendered="#{pc_OlderAgeApplicant.showLabelHowOftenDoYouDrive}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderHowOftenDoYouDriveDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderHowOftenDoYouDriveDetails');"  rendered="#{pc_OlderAgeApplicant.showHowOftenDoYouDriveDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasHowOftenDoYouDriveDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Ifyouarenolonge}" styleClass="formLabel" style="width: 377px;text-align:left;margin-left:30px" id="labelIfYouAreNoLongerDriving" rendered="#{pc_OlderAgeApplicant.showLabelIfYouAreNoLongerDriving}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderIfNoLongerDriveDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderIfNoLongerDriveDetails');"  rendered="#{pc_OlderAgeApplicant.showIfYouAreNoLongerDrivingDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasIfNoLongerDriveDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Haveyouhadanyfa}" styleClass="formLabel" id="labelHaveYouHadAnyFalls" rendered="#{pc_OlderAgeApplicant.showLabelHaveYouHadAnyFalls}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.haveYouAnyFallsNo}" id="haveYouAnyFallsNo" onclick="toggleCBGroup(this.form.id, 'haveYouAnyFallsNo',  'haveYouAnyFalls',true);valueChangeEvent();"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.haveYouAnyFallsYes}" id="haveYouAnyFallsYes" valueChangeListener="#{pc_OlderAgeApplicant.collapseAnyFallsDetails}" onclick="toggleCBGroup(this.form.id, 'haveYouAnyFallsYes',  'haveYouAnyFalls',false);valueChangeEvent();submit();if(document.getElementById(this.id).checked){launchDetailsPopUp('olderHaveYouOftenfallenLast2yearsDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<DynaDiv:DynamicDiv id="anyFallenIfYes" rendered="#{pc_OlderAgeApplicant.expandAnyFallsDetailsInd}">
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Howoftenhaveyou}" styleClass="formLabel" style="width: 372px;text-align:left;margin-left:30px" id="labelHaveYouOftenfallenLast2years" rendered="#{pc_OlderAgeApplicant.showLabelHaveYouOftenfallenLast2years}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderHaveYouOftenfallenLast2yearsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderHaveYouOftenfallenLast2yearsDetails');"  rendered="#{pc_OlderAgeApplicant.showHaveYouOftenfallenLast2yearsDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasHaveYouOftenFallenDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Detailshowdidth}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelDetailsHowFallHappen" rendered="#{pc_OlderAgeApplicant.showLabelDetailsHowFallHappen}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderHowFallsHappenDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderHowFallsHappenDetails');"  rendered="#{pc_OlderAgeApplicant.showHowFallsHappenDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasHowFallsHappenDetails}"/>
							</td>
						</tr>
						</DynaDiv:DynamicDiv>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;" align="left"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left">
								<h:outputText style="width: 365px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyourequireass}" styleClass="formLabel" id="labelDoYouRequireAssistance" rendered="#{pc_OlderAgeApplicant.showLabelDoYouRequireAssistance}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouRequireAssistanceNo}" id="doYouRequireAssistanceNo" onclick="toggleCBGroup(this.form.id, 'doYouRequireAssistanceNo',  'doYouRequireAssistance')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouRequireAssistanceYes}" id="doYouRequireAssistanceYes" onclick="toggleCBGroup(this.form.id, 'doYouRequireAssistanceYes',  'doYouRequireAssistance');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderDoYouRequireAssistanceDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left">
								<h:outputText value="#{componentBundle.Ifyesprovidedet}" styleClass="formLabel" style="width: 491px;text-align:left;margin-left:30px" id="labelIfYesUseoFCan" rendered="#{pc_OlderAgeApplicant.showLabelIfYesUseoFCan}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderDoYouRequireAssistanceDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderDoYouRequireAssistanceDetails');"  rendered="#{pc_OlderAgeApplicant.showDoYouRequireAssistanceDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasDoYouRequireAssistanceDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 395px;" value="#{componentBundle.Doyourequireany}" styleClass="formLabel" id="labelDoYouRequireAnyHelpPerforming" rendered="#{pc_OlderAgeApplicant.showLabelDoYouRequireAnyHelpPerforming}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Bathing}" styleClass="formLabel" style="width: 300px;" id="labelBathing" rendered="#{pc_OlderAgeApplicant.showLabelBathing}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.bathingNo}" id="bathingNo" onclick="toggleCBGroup(this.form.id, 'bathingNo',  'bathing')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.bathingYes}" id="bathingYes" onclick="toggleCBGroup(this.form.id, 'bathingYes',  'bathing')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Dressing}" styleClass="formLabel" style="width: 300px;" id="labelDressing" rendered="#{pc_OlderAgeApplicant.showLabelDressing}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.dressingNo}" id="dressingNo" onclick="toggleCBGroup(this.form.id, 'dressingNo',  'dressing')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.dressingYes}" id="dressingYes" onclick="toggleCBGroup(this.form.id, 'dressingYes',  'dressing')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Toileting}" styleClass="formLabel" style="width: 300px;" id="labelToileting" rendered="#{pc_OlderAgeApplicant.showLabelToileting}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.toiletingNo}" id="toiletingNo" onclick="toggleCBGroup(this.form.id, 'toiletingNo',  'toileting')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.toiletingYes}" id="toiletingYes" onclick="toggleCBGroup(this.form.id, 'toiletingYes',  'toileting')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Eating}" styleClass="formLabel" style="width: 300px;" id="labelEating" rendered="#{pc_OlderAgeApplicant.showLabelEating}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.eatingNo}" id="eatingNo" onclick="toggleCBGroup(this.form.id, 'eatingNo',  'eating')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.eatingYes}" id="eatingYes" onclick="toggleCBGroup(this.form.id, 'eatingYes',  'eating')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Transferringfro}" styleClass="formLabel" style="width: 300px;" id="labelTransferringFrombedToChair" rendered="#{pc_OlderAgeApplicant.showLabelTransferringFrombedToChair}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.transferingFromBedToChairNo}" id="transferingFromBedToChairNo" onclick="toggleCBGroup(this.form.id, 'transferingFromBedToChairNo',  'transferingFromBedToChair')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.transferingFromBedToChairYes}" id="transferingFromBedToChairYes" onclick="toggleCBGroup(this.form.id, 'transferingFromBedToChairYes',  'transferingFromBedToChair')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Gettingaroundin}" styleClass="formLabel" style="width: 300px;" id="labelGettingAround" rendered="#{pc_OlderAgeApplicant.showLabelGettingAround}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.gettingAroundInsideHomeNo}" id="gettingAroundInsideHomeNo" onclick="toggleCBGroup(this.form.id, 'gettingAroundInsideHomeNo',  'gettingAroundInsideHome')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.gettingAroundInsideHomeYes}" id="gettingAroundInsideHomeYes" onclick="toggleCBGroup(this.form.id, 'gettingAroundInsideHomeYes',  'gettingAroundInsideHome')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Shopping}" styleClass="formLabel" style="width: 300px;" id="labelShopping" rendered="#{pc_OlderAgeApplicant.showLabelShopping}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.shoppingNo}" id="shoppingNo" onclick="toggleCBGroup(this.form.id, 'shoppingNo',  'shopping')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.shoppingYes}" id="shoppingYes" onclick="toggleCBGroup(this.form.id, 'shoppingYes',  'shopping')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Takingmedicatio}" styleClass="formLabel" style="width: 300px;" id="labelTakingMedications" rendered="#{pc_OlderAgeApplicant.showLabelTakingMedications}"></h:outputText>
							</td>
							<td colspan="1" align="left" class="formLabelRight">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.takingMedicationsNo}" id="takingMedicationsNo" onclick="toggleCBGroup(this.form.id, 'takingMedicationsNo',  'takingMedications')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.takingMedicationsYes}" id="takingMedicationsYes" onclick="toggleCBGroup(this.form.id, 'takingMedicationsYes',  'takingMedications')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Preparingmeals}" styleClass="formLabel" style="width: 300px;" id="labelPreparingMeals" rendered="#{pc_OlderAgeApplicant.showLabelPreparingMeals}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.preparingMealsNo}" id="preparingMealsNo" onclick="toggleCBGroup(this.form.id, 'preparingMealsNo',  'preparingMeals')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.preparingMealsYes}" id="preparingMealsYes" onclick="toggleCBGroup(this.form.id, 'preparingMealsYes',  'preparingMeals')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Usingthetelepho}" styleClass="formLabel" style="width: 300px;" id="labelUsingTheTelephone" rendered="#{pc_OlderAgeApplicant.showLabelUsingTheTelephone}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.usingTelephoneNo}" id="usingTelephoneNo" onclick="toggleCBGroup(this.form.id, 'usingTelephoneNo',  'usingTelephone')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.usingTelephoneYes}" id="usingTelephoneYes" onclick="toggleCBGroup(this.form.id, 'usingTelephoneYes',  'usingTelephone')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Usingtransporta}" styleClass="formLabel" style="width: 300px;" id="labelUsingTransportation" rendered="#{pc_OlderAgeApplicant.showLabelUsingTransportation}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.usingTransportataionNo}" id="usingTransportataionNo" onclick="toggleCBGroup(this.form.id, 'usingTransportataionNo',  'usingTransportataion')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.usingTransportataionYes}" id="usingTransportataionYes" onclick="toggleCBGroup(this.form.id, 'usingTransportataionYes',  'usingTransportataion')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Housekeeping}" styleClass="formLabel" style="width: 300px;" id="labelHouseKeeping" rendered="#{pc_OlderAgeApplicant.showLabelHouseKeeping}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.houseKeepingNo}" id="houseKeepingNo" onclick="toggleCBGroup(this.form.id, 'houseKeepingNo',  'houseKeeping')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.houseKeepingYes}" id="houseKeepingYes" onclick="toggleCBGroup(this.form.id, 'houseKeepingYes',  'houseKeeping')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Managingmoney}" styleClass="formLabel" style="width: 300px;" id="labelManagingMoney" rendered="#{pc_OlderAgeApplicant.showLabelManagingMoney}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.managingMoneyNo}" id="managingMoneyNo" onclick="toggleCBGroup(this.form.id, 'managingMoneyNo',  'managingMoney')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.managingMoneyYes}" id="managingMoneyYes" onclick="toggleCBGroup(this.form.id, 'managingMoneyYes',  'managingMoney')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="right">
								<h:outputText value="#{componentBundle.Doinglaundry}" styleClass="formLabel" style="width: 300px;" id="labelDoingLaundry" rendered="#{pc_OlderAgeApplicant.showLabelDoingLaundry}"></h:outputText>
							</td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doingLaundryNo}" id="doingLaundryNo" onclick="toggleCBGroup(this.form.id, 'doingLaundryNo',  'doingLaundry')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doingLaundryYes}" id="doingLaundryYes" onclick="toggleCBGroup(this.form.id, 'doingLaundryYes',  'doingLaundry')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;" align="left">
								<h:outputText value="#{componentBundle.Ifyesdetails}" styleClass="formLabel" style="width: 470px;text-align:left;margin-left:15px;" id="labelIfYesDetails" rendered="#{pc_OlderAgeApplicant.showLabelIfYesDetails}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderIfYesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderIfYesDetails');"  rendered="#{pc_OlderAgeApplicant.showIfYesDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasIfYesDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" style="padding-top: 15px;" align="left">
								<h:outputText style="width: 489px;text-align:left;margin-left:5px;" value="#{componentBundle.Doyoufeeldepres}" styleClass="formLabel" id="labelDoYouFeelDepressed" rendered="#{pc_OlderAgeApplicant.showLabelDoYouFeelDepressed}"></h:outputText>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="1" align="left"></td>
							<td colspan="1" align="left">
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouFeelDepressedNo}" id="doYouFeelDepressedNo" onclick="toggleCBGroup(this.form.id, 'doYouFeelDepressedNo',  'doYouFeelDepressed')"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.No}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
								<h:selectBooleanCheckbox value="#{pc_OlderAgeApplicant.doYouFeelDepressedYes}" id="doYouFeelDepressedYes" onclick="toggleCBGroup(this.form.id, 'doYouFeelDepressedYes',  'doYouFeelDepressed');if(document.getElementById(this.id).checked){launchDetailsPopUp('olderDoYouFeelDepressedIfYesDetails')};"></h:selectBooleanCheckbox>
								<h:outputLabel value="#{componentBundle.Yes}" style="width: 31px" styleClass="formLabelRight"></h:outputLabel>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left">
								<h:outputText value="#{componentBundle.Ifyespleaseprov_0}" styleClass="formLabel" style="width: 290px;text-align:left;margin-left:30px" id="labelDoYouFeelDepressedIfYes" rendered="#{pc_OlderAgeApplicant.showLabelDoYouFeelDepressedIfYes}"></h:outputText>
							</td>
							<td align="left" colspan="1"></td>
							<td>								
								<h:commandButton id="olderDoYouFeelDepressedIfYesDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderDoYouFeelDepressedIfYesDetails');"  rendered="#{pc_OlderAgeApplicant.showDoYouFeelDepressedIfYesDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasDoYouFeelDepressedDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 15px;">
							<td colspan="2" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 403px;text-align:left;margin-left:5px;" value="#{componentBundle.Whatdistancecan}" styleClass="formLabel" id="labelWhatDistance" rendered="#{pc_OlderAgeApplicant.showLabelWhatDistance}"></h:outputText>
								<h:outputText value="#{componentBundle.lessthanablock1}" styleClass="formLabel" style="width: 392px;text-align:left;margin-left:15px" id="labelLessThan" rendered="#{pc_OlderAgeApplicant.showLabelLessThan}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderWhatDistanceDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderWhatDistanceDetails');"  rendered="#{pc_OlderAgeApplicant.showWhatDistanceDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasWhatDistanceDetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="1" align="left"></td>
							<td align="left" colspan="1"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left" style="padding-top: 15px;">
								<h:outputText style="width: 300px;text-align:left;margin-left:5px;" value="#{componentBundle.Anyadditionalco}" styleClass="formLabel" id="labelAnyAdditionalComments" rendered="#{pc_OlderAgeApplicant.showLabelAnyAdditionalComments}"></h:outputText>
							</td>
							<td>								
								<h:commandButton id="olderAnyAdditionalCommentsDetails" image="images/circle_i.gif"  onclick="launchDetailsPopUp('olderAnyAdditionalCommentsDetails');"  rendered="#{pc_OlderAgeApplicant.showAnyAdditionalCommentsDetailsImg}" styleClass="ovitalic#{pc_OlderAgeApplicant.hasAnyAdditionalCommentsetails}"/>
							</td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="5" align="left">
								<hr/>
							</td>
							<td></td>
						</tr>
						<tr style="padding-top: -25px">
							<td colspan="5" align="left" style="height: 52px">
								<h:commandButton value="Cancel" styleClass="buttonLeft" action="#{pc_OlderAgeApplicant.cancelAction}" onclick="resetTargetFrame(this.form.id);" id="_0" rendered="#{pc_OlderAgeApplicant.show_0}" disabled="#{pc_OlderAgeApplicant.disable_0}"></h:commandButton>
								<h:commandButton value="Clear" styleClass="buttonLeft-1" action="#{pc_OlderAgeApplicant.clearAction}" onclick="resetTargetFrame(this.form.id);" id="_1" rendered="#{pc_OlderAgeApplicant.show_1}" disabled="#{pc_OlderAgeApplicant.disable_1}"></h:commandButton>
								<h:commandButton value="OK" styleClass="buttonRight" action="#{pc_OlderAgeApplicant.okAction}" onclick="resetTargetFrame(this.form.id);" id="_2" rendered="#{!pc_Links.showIdOlderAgeApplicant}" disabled="#{pc_OlderAgeApplicant.disable_2}"></h:commandButton>
								<h:commandButton value="Update" styleClass="buttonRight" id="updateButton" action="#{pc_OlderAgeApplicant.okAction}" onclick="resetTargetFrame(this.form.id);" rendered="#{pc_Links.showIdOlderAgeApplicant}" disabled="#{pc_OlderAgeApplicant.disable_2}"></h:commandButton>
							</td>
							<td align="left" colspan="1" style="height: 26px"></td>
							<td></td>
						</tr>
						<tr style="padding-top: 5px;">
							<td colspan="2" align="left" style="height: 18px"></td>
							<td></td>
						</tr>
					</table>
				</div>
			</h:form>
			<div id="Messages" style="display: none">
				<h:messages></h:messages>
			</div>
		</f:view>
</body>
</html>
