<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB004         NB-1101   PHI Processing -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Personal History Interview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var context1 = '<%=basePath%>';
			var topOffset = -22;
			var defaultindex = 1;
			var draftChanges = false;
			var commentBarRefresh = false;			
			function mainTabSize() {
				winHeight = window.screen.availHeight;
				// 175 = 37(browser title) + 33(title) + 23(menu) + 50(status) + 32(tabs)
				tabHeight = winHeight - 285;
				document.getElementById('phi').height = tabHeight;
			}			
			
		//-->
	</script>
</head>
<body class="desktopBody" style="overflow-x: hidden; overflow-y: hidden;" > 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>		
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:31px">
					<td><FileLoader:Files location="nbaPHI/file/" numTabsPerRow="2"/></td>
				</tr>
				<tr style="height:*; vertical-align: top;">
					<td><iframe id="phi" name="file" src="" width="100%" frameborder="0" onload="mainTabSize();"></iframe>
					</td>
				</tr>
			</tbody>
		</table>
	</f:view>
	
</body>
</html>