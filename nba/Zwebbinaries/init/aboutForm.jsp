<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA349		    NB-1401	  nbA IP Compliance -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=	basePath%>">
<title>Accelerator About</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<!-- NBA349 Code Deleted -->
</HEAD>
<BODY CLASS="desktopBody" style="background: #DDDDDD" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0"><!-- NBA349 -->
<f:view>
	<h:form>
		<f:loadBundle var="details"
			basename="com.csc.fs.accel.ui.config.nba.ApplicationDetails" /> <!-- FNB016 -->
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ><!-- NBA349 -->
			<TR>
				<TD colspan="2" style="padding-left: 15px;height:33px;"><!-- NBA349 -->
					<h:outputText rendered="true" escape="false" styleClass="productTitle" style="color:black;" value="#{details.Application_Name}"></h:outputText><!-- NBA349 -->
				</TD>
			</TR>
			<TR>
				<TD align="center" colspan="2" class="textInput" style="padding-left: 3px;">
					<div class="divTable" style="padding-left: 5px;
					 		text-align: left; background: white;"><!-- NBA349 -->
						<!-- NBA349 Code Deleted -->
					 	<PRE class="textInput"><h:outputText escape="false" rendered="true" value="#{details.About_Text}"></h:outputText></PRE><!-- NBA349 -->
						<!--Begin NBA349-->
						<table class="divTable" style="padding-left: 5px; width: 800px; height: 35px;
					 		text-align: left; background: white;border-collapse:collapse;" border="1">
					 	<tr height="35">
							<th class="textInput" style="font-weight:bold;text-align:center;border-right:1px solid;width:163;"><h:outputText rendered="true" value="#{details.Oss}"></h:outputText></th>
							<th class="textInput" style="font-weight:bold;text-align:center;center;border-right:1px solid;width:407;"><h:outputText rendered="true" value="#{details.Url}"></h:outputText></th>
							<th class="textInput" style="font-weight:bold;text-align:center;"><h:outputText rendered="true" value="#{details.License}"></h:outputText></th>
						</tr>
						<tr>
						<jsp:include page="nbaOssInfo.htm"></jsp:include>
						</tr>
					</table>
					<!--End NBA349-->
					</div>
				</TD>
			</TR>
		</TABLE>
		</h:form>
	</f:view>
	</BODY>
</HTML>
