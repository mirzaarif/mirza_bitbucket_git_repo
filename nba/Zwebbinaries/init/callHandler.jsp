<%@ page language="java" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
ResourceBundle configFile = ResourceBundle.getBundle("com.csc.fs.accel.ui.config.CTI");
ResourceBundle configFileChat = ResourceBundle.getBundle("com.csc.fs.accel.ui.config.Chat");
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Call Handler</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/notification.js"></script>
<script type="text/javascript">
<!--
		window.opener = parent;
		var path = '<%= path%>';
		var chatEnabled = "false";
		var ctiEnabled = "false";
		
		function initializeFrame(){
		    try{
		    }catch(err){
		    }
		}
//-->
</script>
</head>
<body onload="initializeFrame();">
	<f:view>
	<div id="callData" style="display:none;">
	</div>
	<div id="chatData" style="display:none;">
	</div>
	<div id="Messages" style="display:none;">
	</div>
	</f:view>
</body>
</html>
