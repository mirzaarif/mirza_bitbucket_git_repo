<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- ACN005           4      Aviation -->
<!-- NBA118           5      Work Item Identification Project-->

<%@ page language="java" errorPage="errorpage.jsp" %> 
<html>
<head>
<TITLE>Aviation Medical Conditions</TITLE>
<!-- NBA118 code deleted -->
<%@ include file="include/copyright.html" %>
</head>
<!-- This page was generated on <%= (new java.util.Date()).toString() %> -->
<%@ include file="include/connect.js" %>
<%session.removeAttribute(com.csc.fsg.nba.foundation.NbaConstants.S_KEY_INITIAL_LOAD);%> <!-- SPR1851 -->
<body scroll="yes" style="OVERFLOW: auto" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" onload="load()" onresize="positionAviationView()" onunload="logoff()">

<SCRIPT language="JavaScript" src="include/common.js" ></SCRIPT>
<SCRIPT language="JavaScript" src="include/tablepane.js" ></SCRIPT>
<SCRIPT language="JavaScript" src="include/date.js" ></SCRIPT>
<SCRIPT language="JavaScript" src="include/time.js" ></SCRIPT>
<SCRIPT language="JavaScript" src="include/alphanumeric.js" ></SCRIPT>
<SCRIPT language="JavaScript" src="include/treeview.js" ></SCRIPT>
<FORM name="nba_frm" method="post" target="fData">
	<SCRIPT>
	//Every view must implement this function
		function positionView(){
	
			// resize the browser to the correct height and width depending on available real-estate on screen
			prefWidth=650;
			prefHieght=600;
			
			winHeight=window.screen.availHeight;
			winWidth=window.screen.availWidth;	
			
			if(winHeight>prefHieght)
				winHeight=prefHieght;
			
			if(winWidth>prefWidth)
				winWidth=prefWidth;
			
	  		window.resizeTo(winWidth,winHeight);
		  	window.moveTo(0,0);   	 
		  	
	    }
	</SCRIPT>
	
	<DIV ID="Content1" Style="POSITION:absolute; LEFT: 0px; VISIBILITY: inherit; OVERFLOW: auto; WIDTH: 620px; TOP: 80px; HEIGHT: 280px">
	  	<SPAN STYLE ="LEFT: 8px; WIDTH: 130px; POSITION: absolute; TOP: 20px; HEIGHT: 14px" >Angina With Catheter:</SPAN>
	  	<SELECT ID="questionNumber_1_EF" NAME="questionNumber_1_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 18px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

	  	<SPAN STYLE ="LEFT: 8px; WIDTH: 145px; POSITION: absolute; TOP: 48px; HEIGHT: 14px" >Angina Without Catheter:</SPAN>
		<SELECT ID="questionNumber_2_EF" NAME="questionNumber_2_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 46px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 130px; POSITION: absolute; TOP: 76px; HEIGHT: 14px">CABG:</SPAN>
		<SELECT ID="questionNumber_3_EF" NAME="questionNumber_3_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 74px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 130px; POSITION: absolute; TOP: 104px; HEIGHT: 14px">M.I. with Catheter:</SPAN>
		<SELECT ID="questionNumber_4_EF" NAME="questionNumber_4_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 102px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 130px; POSITION: absolute; TOP: 132px; HEIGHT: 14px">M.I. without Catheter:</SPAN>
		<SELECT ID="questionNumber_5_EF" NAME="questionNumber_5_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 130px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 150px; POSITION: absolute; TOP: 160px; HEIGHT: 14px">Congestive Heart Failure:</SPAN>
		<SELECT ID="questionNumber_6_EF" NAME="questionNumber_6_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 158px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 130px; POSITION: absolute; TOP: 188px; HEIGHT: 14px">Cardiomyopathy:</SPAN>
		<SELECT ID="questionNumber_7_EF" NAME="questionNumber_7_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 186px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 8px; WIDTH: 150px; POSITION: absolute; TOP: 216px; HEIGHT: 14px">Heart Valve Replacement:</SPAN>
		<SELECT ID="questionNumber_8_EF" NAME="questionNumber_8_EF" STYLE ="LEFT: 160px; WIDTH: 120px; POSITION: absolute; TOP: 214px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 20px; HEIGHT: 14px">Affective Disorders:</SPAN>
		<SELECT ID="questionNumber_9_EF" NAME="questionNumber_9_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 18px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 48px; HEIGHT: 14px">Suicide Attempt:</SPAN>
		<SELECT ID="questionNumber_10_EF" NAME="questionNumber_10_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 46px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 76px; HEIGHT: 14px">Epilepsy:</SPAN>
		<SELECT ID="questionNumber_11_EF" NAME="questionNumber_11_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 74px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 104px; HEIGHT: 14px">Diabetes Mellitus:</SPAN>
		<SELECT ID="questionNumber_12_EF" NAME="questionNumber_12_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 102px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 132px; HEIGHT: 14px">Alcohol Abuse:</SPAN>
		<SELECT ID="questionNumber_13_EF" NAME="questionNumber_13_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 130px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 130px; POSITION: absolute; TOP: 160px; HEIGHT: 14px">Drug Abuse:</SPAN>
		<SELECT ID="questionNumber_14_EF" NAME="questionNumber_14_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 158px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 188px; POSITION: absolute; TOP: 188px; HEIGHT: 14px">Syncope:</SPAN>
		<SELECT ID="questionNumber_15_EF" NAME="questionNumber_15_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 186px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<SPAN STYLE ="LEFT: 300px; WIDTH: 190px; POSITION: absolute; TOP: 216px; HEIGHT: 14px">Treatment of Chronic Condition:</SPAN>
		<SELECT ID="questionNumber_16_EF" NAME="questionNumber_16_EF" STYLE ="LEFT: 487px; WIDTH: 120px; POSITION: absolute; TOP: 214px; HEIGHT: 21px" >
			<OPTION value="" selected ></OPTION>
		</SELECT>

		<BUTTON Name="OK" STYLE="LEFT: 5px; WIDTH: 81px; POSITION: absolute; TOP: 250px; HEIGHT: 25px" onclick="performOK(this)">OK</BUTTON>
		<BUTTON Name="Cancel" STYLE="LEFT: 527px; WIDTH: 85px; POSITION: absolute; TOP: 250px; HEIGHT: 25px" onclick="performCancel(this)">Cancel</BUTTON>
	</DIV>
	<DIV class="tab" ID="WaitScreen" Style="Z-INDEX: 900; LEFT: 0px; VISIBILITY: visible; WIDTH: 620px; POSITION: absolute; TOP: 80px; HEIGHT: 280px">
	</DIV>
	<IFRAME id="fData" name="fData" style="VISIBILITY: hidden; WIDTH: 0px; POSITION: absolute; TOP: 0px; HEIGHT: 0px" border=0></IFRAME>	
	<SCRIPT>
		addSecureField("ACTION");
		addSecureField("MAX_SUSPEND_DAYS");			
	</SCRIPT>

    <DIV id="WaitScreen" class="tab" STYLE="LEFT: 0px; VISIBILITY: hidden; WIDTH: 1260px; TOP: 80px; HEIGHT: 800px"></DIV>
	<IFRAME id="fData" name="fData" style="VISIBILITY: hidden; WIDTH: 0px; POSITION: absolute; TOP: 0px; HEIGHT: 0px" border=0></IFRAME>

	<INPUT TYPE="hidden" NAME="ActionIndicator" value="<%=request.getParameter("ActionIndicator")%>">
	<INPUT TYPE="hidden" NAME="Action">	
	<INPUT TYPE="hidden" NAME="description">	
	<INPUT TYPE="hidden" NAME="nbARequirementStatus">	
	<INPUT TYPE="hidden" NAME="View" value="<%=request.getParameter("View")%>">		
</FORM>	
<script>
</script>	
<%@ include file="include/popupframework.html" %>

</body>
<script>
//    window.onload = load;

function load(){
	positionAviationView();
	getField("Action").value = "LOAD";
	invokeServer("NbaAviationMedicalConditionServlet");
}


function handle_callBack(e){	
	if(document.getElementById("Content1")){
		document.getElementById("Content1").style.visibility = "visible";			
	}
}

function performCancel() {
	window.opener.focus();
	window.close();
}

function performOK(field) {	
	getField("Action").value = "OK";
	invokeServer("NbaAviationMedicalConditionServlet");	
}

function positionAviationView()
{
	var X,Y;	
	Y = window.screen.availHeight;
  	X = window.screen.availWidth;	 	
  	window.resizeTo(X,Y);
  	window.moveTo(0,0); 
}

function closeScreen(){
	if (window.opener && !window.opener.closed){
		opener.transfer();
	}else {
		alert("The parent window has been closed. Please login again.");
		window.close();
	}
}

function logoff(){
}

</script>

</HTML>
