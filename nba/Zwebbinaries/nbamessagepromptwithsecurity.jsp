<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA077           4       Reissue and Complex Change -->
<!-- NBA118			  5	      Work Item Identification Project-->

<%@ page language="java" import="com.csc.fsg.nba.foundation.*" errorPage="errorpage.jsp" %>
<HTML>
	<HEAD>
		<TITLE>nbA message</TITLE>
		<!-- NBA118 code deleted -->
	</HEAD>	
	<LINK rel="stylesheet" type="text/css" name="stylesheet" href="include/styles.css"> <!--NBA118-->
	<BODY style="background-color: silver" scroll="yes" style="overflow:auto" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">		
		<SCRIPT language="JavaScript" src="include/common.js"></SCRIPT>
		<FORM name="nba_frm" method="post" target="fData">
			<table border="0" align="center">
			  <tr valign="top" align="left">
			    <td ><SPAN id="message" Style="WIDTH: 375px; HEIGHT: 30px"> </SPAN></td>
			  </tr>
			  <tr >
			    <td align="center">
			    	<% if("false".equals(request.getParameter("READONLY"))){ %>
						<BUTTON name="yes" Style="WIDTH: 85px; HEIGHT: 25px" onclick= "getField('result').value=true;top.close()" >Yes</BUTTON>	    	
					<% }else{ %>
						<BUTTON name="yes" Style="WIDTH: 85px; HEIGHT: 25px" onclick= "getField('result').value=true;top.close()" disabled="disabled">Yes</BUTTON>	    	
					<% }%>
					&nbsp;
					<BUTTON name="no" Style="WIDTH: 85px; HEIGHT: 25px" onclick= "getField('result').value=false;top.close()" >No</BUTTON>
					
			    </td>
			  </tr>
			</table>	
			<SCRIPT>
				//Add hidden fields here.				
				addSecureField("result");				
			    getField("result").value = false;
			</SCRIPT>    
		</FORM>
		<%@ include file="include/dialog.js" %>
	</BODY>	
</HTML>