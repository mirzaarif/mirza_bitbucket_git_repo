<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<%@ page language="java" import="com.csc.fsg.nba.foundation.*" errorPage="errorpage.jsp" %>
<HTML>
	<HEAD>
		<TITLE>nbA message</TITLE>
	</HEAD>	
	<LINK rel="stylesheet" type="text/css" name="stylesheet" href="include/styles.css"> <!--NBA118-->
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<BODY style="background-color: white" scroll="yes" style="overflow:auto" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">		
		<SCRIPT language="JavaScript" src="include/common.js"></SCRIPT>
		<FORM name="nba_frm">
			<table border="0" align="center">
			  <tr valign="top" align="left">
			    <td ><SPAN id="message" Style="WIDTH: 375px; HEIGHT: 30px"> </SPAN></td>
			  </tr>
			  <tr >
			    <td align="center">
					<BUTTON name="yes" styleClass="buttonLeft" onclick= "getField('result').value=true;top.close()" >Yes</BUTTON>	    	
					&nbsp;
					<BUTTON name="no" styleClass="buttonRight" onclick= "getField('result').value=false;top.close()" >No</BUTTON>
			    </td>
			  </tr>
			</table>	
			<SCRIPT>
				//Add hidden fields here.
				addSecureField("result");
			    getField("result").value = false;
			</SCRIPT>    
		</FORM>
		<%@ include file="include/dialog.js" %>
	</BODY>	
</HTML>