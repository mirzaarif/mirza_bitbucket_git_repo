<!-- CHANGE LOG -->
<!-- Audit Number	Version 	Change Description -->
<!-- NBA178            7        Relate Rewrite -->
<!-- NBA212            7        Content Services -->
<!-- NBA213            7        Unified User Interface -->
<!-- NBA271            8        Implementation Of Business Entities in nbA -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->
<!-- NBA340         NB-1501     Mask Government ID -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %> 
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Relate</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_relate'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_relate'].target='';
			return false;
		}
		//NBA213 code deleted
		function enableSearchButton() {
			toDisable = true; //NBA271
			if(document.getElementById('form_relate:nbaRelateSearch:nbaRelateCriteria_businessarea').value!="-1" ){ //NBA271
				if(document.getElementById('form_relate:nbaRelateSearch:iptcontractNumber').value!= "" || 
				document.getElementById('form_relate:nbaRelateSearch:govtIdInput').value!= "" ||   //NBA340
				document.getElementById('form_relate:nbaRelateSearch:iptLastName').value!= "" ) {
					toDisable = false;//NBA271		
				} else if(document.getElementById('form_relate:nbaRelateSearch:nbaRelateCriteria_worktype').value!="-1" &&
				document.getElementById('form_relate:nbaRelateSearch:From_Date').value!="" && 
				document.getElementById('form_relate:nbaRelateSearch:From_Date').value!="MM/dd/yyyy") {
				  	toDisable = false;//NBA271
				}
			}
			document.getElementById('form_relate:nbaRelateSearch:btnSearch1').disabled = toDisable;	//NBA271
			document.getElementById('form_relate:nbaRelateSearch:btnSearch2').disabled = toDisable;	//NBA271				
		}
		function submitRelateForm() {
			document.forms['form_relate'].submit();
		}		
		//NBA212 code deleted
		
		//NBA340 new Method
		$(document).ready(function() {			
	        $("input[name$='govtIdInput']").govtidValue();
		})		
	</script>
</head>
<body onload="filePageInit();enableSearchButton();" style="overflow-x: hidden; overflow-y: scroll">	<!-- NBA213 NBA271-->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_RELATE" value="#{pc_relate}" />
	<h:form id="form_relate">
		<f:subview id="nbaRelateSearch">
			<c:import url="/nbaRelate/subviews/nbaRelateCriteria.jsp" />
		</f:subview>

		<f:subview id="nbaRelateTable1">
			<c:import url="/nbaRelate/subviews/nbaRelateTable1.jsp" />
		</f:subview>

		<h:panelGroup id="buttonPanelGroup" styleClass="ovButtonBar">
			<h:commandButton id="btnBreak" value="#{property.buttonBreak}" styleClass="ovButtonLeft" disabled="#{pc_relate.breakDisabled}"
				action="#{pc_relate.breakWorkItem}" onclick="resetTargetFrame();" />
			<h:commandButton id="btnDown" value="#{property.buttonDown}" styleClass="ovButtonLeft" style="left: 210px" disabled="#{pc_relate.moveDisabled}" 
				action="#{pc_relate.moveDown}" onclick="resetTargetFrame();" />
			<h:commandButton id="btnUp" value="#{property.buttonUp}" styleClass="ovButtonLeft" style="left: 305px" disabled="#{pc_relate.moveDisabled}" 
				action="#{pc_relate.moveUp}" onclick="resetTargetFrame();" />
			<h:commandButton id="btnExpand" value="#{property.buttonExpand}" styleClass="ovButtonRight-1" disabled="#{pc_relate.expandDisabled}"
				action="#{pc_relate.expand}" onclick="resetTargetFrame();" />
			<h:commandButton id="btnOpen" value="#{property.buttonOpen}" styleClass="ovButtonRight" disabled="#{pc_relate.openDisabled}"
				action="#{pc_relate.open}" onclick="setTargetFrame();" /> <!-- NBA213 -->
		</h:panelGroup>

		<f:subview id="nbaRelateTable2">
			<c:import url="/nbaRelate/subviews/nbaRelateTable2.jsp" />
		</f:subview>

		<f:subview id="nbaCommentBar" rendered="#{pc_relate.workItemOpen}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>

		<h:panelGroup id="commitButtonPanelGroup" styleClass="tabButtonBar" style="height: 35px">
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" 
				disabled="#{pc_relate.auth.enablement['Commit'] || 
							pc_relate.notLocked}" action="#{pc_relate.commit}" 
				onclick="resetTargetFrame()" /> <!-- NBA213 -->
		</h:panelGroup>
		<!-- NBA213 code deleted -->
	</h:form>
	<!-- NBA213 code deleted -->
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
