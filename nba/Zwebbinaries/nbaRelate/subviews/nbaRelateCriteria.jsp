<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA178           7         Relate Rewrite -->
<!-- NBA271           8         Implementation Of Business Entities in nbA -->
<!-- FNB011             NB-1101 Work Tracking -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:panelGroup id="relateFormat" styleClass="inputFormMat">
	<h:panelGroup id="relateForm" styleClass="inputForm" style="height: 180px">
		<h:panelGroup id="titleBarPanelGroup" styleClass="formTitleBar">
			<h:outputText value="#{property.relateTitle}" styleClass="shTextLarge" />
		</h:panelGroup>

		<h:panelGroup id="businessAreaPanelGroup" styleClass="formDataEntryTopLine">
			<h:outputLabel value="#{property.relateBusinessArea}" styleClass="formLabel" />
			<h:selectOneMenu id="nbaRelateCriteria_businessarea" value="#{pc_relateCriteria.businessArea}" styleClass="formEntryText" style="width: 290px" 
				onchange="submit()">
				<f:selectItems id="nbaRelateCriteria_businessarea_selectItems" value="#{pc_relateCriteria.businessAreas}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="queuePanelGroup" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.relateQueue}" styleClass="formLabel" />
			<h:selectOneMenu id="nbaRelateCriteria_queue" value="#{pc_relateCriteria.queue}" styleClass="formEntryText" style="width: 290px">
				<f:selectItems id="nbaRelateCriteria_queue_selectItems" value="#{pc_relateCriteria.queues}" />
			</h:selectOneMenu>
		</h:panelGroup>
		
		<h:panelGroup id="workTypePanelGroup" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.relateWorkType}" styleClass="formLabel" />
			<h:selectOneMenu id="nbaRelateCriteria_worktype" value="#{pc_relateCriteria.workType}" styleClass="formEntryText" style="width: 290px" 
				onchange="resetTargetFrame();submit()"> 
				<f:selectItems id="nbaRelateCriteria_worktype_selectItems" value="#{pc_relateCriteria.workTypes}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="statusPanelGroup" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.relateStatus}" styleClass="formLabel" />
			<h:selectOneMenu id="nbaRelateCriteria_status" value="#{pc_relateCriteria.status}" styleClass="formEntryTextFull">
				<f:selectItems id="nbaRelateCriteria_status_selectItems" value="#{pc_relateCriteria.statuses}" />
			</h:selectOneMenu>
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGroup id="dataFieldsPanelGroup1" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.relateContractNumber}" styleClass="formLabel" />
			<h:inputText id = "iptContractNumber" value="#{pc_relateCriteria.contractNumber}" styleClass="formEntryText" onkeypress="toUpperCase(event);" style="width: 150px" maxlength="15"
				onkeyup="enableSearchButton();"/>
			<h:outputLabel value="#{property.relateGovtID}" styleClass="formLabel" />
			<h:inputText id= "govtIdInput" value="#{pc_relateCriteria.govtID}" styleClass="formEntryText" style="width: 100px" maxlength="9" 
				onkeyup="enableSearchButton();" /><!-- NBA340 -->
			<h:inputHidden id="govtIdKey" value="#{pc_relateCriteria.govtIdKey}"></h:inputHidden><!-- NBA340 -->	
		</h:panelGroup>
		<h:panelGroup id="dataFieldsPanelGroup2" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.relateLastName}" styleClass="formLabel" />
			<h:inputText id = "iptLastName" value="#{pc_relateCriteria.lastName}" styleClass="formEntryText" onkeypress="toUpperCase(event);" style="width: 150px"
				onkeyup="enableSearchButton();"/>
			<h:outputLabel value="#{property.relateFirstName}" styleClass="formLabel" />
			<h:inputText value="#{pc_relateCriteria.firstName}" onkeypress="toUpperCase(event);" styleClass="formEntryText" style="width: 100px"/>
			<h:outputLabel value="#{property.relateMiddleName}" styleClass="formLabel" style="width: 80px" />
			<h:inputText value="#{pc_relateCriteria.middleName}" onkeypress="toUpperCase(event);" styleClass="formEntryText"  style="width: 100px;" />	<!-- FNB011 -->
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGrid id="datesPanelGrid" columns="2" cellspacing="0" cellpadding="0">
			<h:column>
				<h:outputLabel value="#{property.relateFromDate}" styleClass="formLabel" />
				<h:inputText id="From_Date" value="#{pc_relateCriteria.fromDate}" styleClass="formEntryDate" style="width: 100px" 
					onkeyup="enableSearchButton();" immediate="true">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<h:outputLabel value="#{property.relateFromTime}" styleClass="formLabel" style="width: 100px" />
				<h:inputText id="From_Time" value="#{pc_relateCriteria.fromTime}" styleClass="formEntryDate" style="width: 80px">
					<f:convertDateTime pattern="#{property.shorttimePattern}" />
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio value="#{pc_relateCriteria.fromTimePeriod}" layout="lineDirection" enabledClass="formDisplayText" style="width: 100px; margin-right: 70px">
					<f:selectItems value="#{pc_relateCriteria.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputLabel value="#{property.relateToDate}" styleClass="formLabel" />
				<h:inputText id="To_Date" value="#{pc_relateCriteria.toDate}" styleClass="formEntryDate" style="width: 100px" 
					immediate="true">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<h:outputLabel value="#{property.relateToTime}" styleClass="formLabel" style="width: 100px" />
				<h:inputText id="To_Time" value="#{pc_relateCriteria.toTime}" styleClass="formEntryDate" style="width: 80px">
					<f:convertDateTime pattern="#{property.shorttimePattern}" />
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio value="#{pc_relateCriteria.toTimePeriod}" layout="lineDirection" enabledClass="formDisplayText" style="width: 100px; margin-right: 70px">
					<f:selectItems value="#{pc_relateCriteria.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
		</h:panelGrid>
		<h:panelGroup id="searchButtonsPanelGroup" styleClass="formButtonBar">
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft" action="#{pc_relateCriteria.clear}" onclick="resetTargetFrame();" />
			<h:commandButton id="btnSearch1" value="#{property.buttonSearchOne}" styleClass="formButtonRight-1" 
					action="#{pc_relateCriteria.searchOne}" onclick="resetTargetFrame();submitRelateForm();" /><!-- NBA271 -->
			<h:commandButton id="btnSearch2" value="#{property.buttonSearchTwo}" styleClass="formButtonRight" 
					action="#{pc_relateCriteria.searchTwo}" onclick="resetTargetFrame();submitRelateForm();" /><!-- NBA271 -->
		</h:panelGroup>
	</h:panelGroup>
</h:panelGroup>		