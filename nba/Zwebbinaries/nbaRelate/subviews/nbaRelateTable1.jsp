<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA178           7         Relate Rewrite -->
<!-- NBA212           7        Content Services -->
<!-- SPRNBA-921	    NB-1501     Medical Source Rows Not Displayed in Hierarchy and Results Page Navigation After Image Opened-->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup id="relateHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" cellspacing="0" columnClasses="ovColHdrIcon,ovColHdrText170,ovColHdrText100,ovColHdrText125,ovColHdrText100,ovColHdrText85">
			<h:outputText id="relateTable1HdrCol1" value="#{property.relateTableCol1}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol1}" />
			<h:outputText id="relateTable1HdrCol2" value="#{property.relateTableCol2}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol2}" />
			<h:outputText id="relateTable1HdrCol3" value="#{property.relateTableCol3}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol3}" />
			<h:outputText id="relateTable1HdrCol4" value="#{property.relateTableCol4}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol4}" />
			<h:outputText id="relateTable1HdrCol5" value="#{property.relateTableCol5}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol5}" />
			<h:outputText id="relateTable1HdrCol6" value="#{property.relateTableCol6}" styleClass="ovColSorted#{pc_relate.relateTable1.sortedByCol6}" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="relateData1" styleClass="ovDivTableData" style="height:150px">
		<h:dataTable id="relateTable1" styleClass="ovTableData" cellspacing="0" rows="0" border="0" binding="#{pc_relate.relateTable1.dataTable}" value="#{pc_relate.relateTable1.results}" 
		var="relate" rowClasses="#{pc_relate.relateTable1.rowStyles}" columnClasses="ovColIconTop,ovColText170,ovColText100,ovColText125,ovColText100,ovColText85" style="min-height: 19px;">
			<h:column>
				<h:panelGroup>
					<h:commandButton id="relateTable1Col1" image="#{relate.imageUrl}" styleClass="ovViewIcon#{relate.imagePresent}" onclick="setTargetFrame()" action="#{relate.showSelectedImage}"
						immediate="true" onmouseup="submitRelateForm()" disabled="#{relate.medicalRequirement}" title="#{relate.lockedBy}"/> <!-- NBA212 SPRNBA-921-->
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:commandButton id="relateIcon1" image="images/hierarchies/#{relate.icon1}" rendered="#{relate.icon1Rendered}" styleClass="#{relate.icon1StyleClass}" style="margin-left: 5px; vertical-align: top" />
					<h:commandButton id="relateIcon2" image="images/hierarchies/#{relate.icon2}" rendered="#{relate.icon2Rendered}" styleClass="#{relate.icon2StyleClass}" style="margin-left: 10px; vertical-align: top" />
					<h:commandButton id="relateIcon3" image="images/hierarchies/#{relate.icon3}" rendered="#{relate.icon3Rendered}" styleClass="#{relate.icon3StyleClass}" style="margin-left: 15px; vertical-align: top" />
					<h:commandLink id="relateTable1Col2" action="#{pc_relate.relateTable1.selectRow}" immediate="true"> 
						<h:inputTextarea readonly="true" value="#{relate.workType}" title="#{relate.workIdentification}" styleClass="ovMultiLine#{relate.multiLineRowStyle}" style="margin-left: 5px;width: 105px;" />
					</h:commandLink>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:commandLink id="relateTable1Col3" styleClass="ovMultiLine#{relate.multiLineRowStyle}" action="#{pc_relate.relateTable1.selectRow}" immediate="true">
					<h:outputText value="#{relate.contractNumber}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="relateTable1Col4" action="#{pc_relate.relateTable1.selectRow}" immediate="true"> 
					<h:inputTextarea readonly="true" value="#{relate.name}" styleClass="ovMultiLine#{relate.multiLineRowStyle}" style="width: 125px" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="relateTable1Col5" styleClass="ovMultiLine#{relate.multiLineRowStyle}" action="#{pc_relate.relateTable1.selectRow}" immediate="true">
					<h:outputText value="#{relate.govtID}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="relateTable1Col6" value="#{relate.suspended}" title="#{relate.suspendOriginator}" styleClass="ovMultiLine#{relate.multiLineRowStyle}" action="#{pc_relate.relateTable1.selectRow}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_relate.relateTable1.showPrevious}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.previousPageAbsolute}" immediate="true" />
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_relate.relateTable1.showPrevious}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.previousPage}" immediate="true" />
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_relate.relateTable1.showPreviousSet}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.previousPageSet}" immediate="true" />
		<h:commandLink value="#{pc_relate.relateTable1.page1Number}" rendered="#{pc_relate.relateTable1.showPage1}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page1}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page1Number}" rendered="#{pc_relate.relateTable1.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page2Number}" rendered="#{pc_relate.relateTable1.showPage2}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page2}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page2Number}" rendered="#{pc_relate.relateTable1.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page3Number}" rendered="#{pc_relate.relateTable1.showPage3}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page3}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page3Number}" rendered="#{pc_relate.relateTable1.currentPage3}" styleClass="ovStatusBarTextBold" /> 
		<h:commandLink value="#{pc_relate.relateTable1.page4Number}" rendered="#{pc_relate.relateTable1.showPage4}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page4}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page4Number}" rendered="#{pc_relate.relateTable1.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page5Number}" rendered="#{pc_relate.relateTable1.showPage5}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page5}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page5Number}" rendered="#{pc_relate.relateTable1.currentPage5}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page6Number}" rendered="#{pc_relate.relateTable1.showPage6}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page6}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page6Number}" rendered="#{pc_relate.relateTable1.currentPage6}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page7Number}" rendered="#{pc_relate.relateTable1.showPage7}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page7}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page7Number}" rendered="#{pc_relate.relateTable1.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_relate.relateTable1.page8Number}" rendered="#{pc_relate.relateTable1.showPage8}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.page8}" immediate="true" />
		<h:outputText value="#{pc_relate.relateTable1.page8Number}" rendered="#{pc_relate.relateTable1.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_relate.relateTable1.showNextSet}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.nextPageSet}" immediate="true" />
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_relate.relateTable1.showNext}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.nextPage}" immediate="true" />
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_relate.relateTable1.showNext}" styleClass="ovStatusBarText" action="#{pc_relate.relateTable1.nextPageAbsolute}" immediate="true" />
	</h:panelGroup>
</jsp:root>
