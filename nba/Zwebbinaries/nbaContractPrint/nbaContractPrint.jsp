<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA183           7         Contract Print UI Rewrite -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_contractprint'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractprint'].target='';
			return false;
		}
		
	</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="form_contractprint">
		<div id="contractprintFormat" class="inputFormMat">
			<div id="suspendForm" class="inputForm">
				<h:panelGroup styleClass="formTitleBar">
					<h:outputLabel value="#{property.contractPrintTitle}" styleClass="shTextLarge" />
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryTopLine" style="margin-top: 10px">
					<h:outputText id="documentLabel" value="#{property.contractPrint}" styleClass="formLabel" style="vertical-align: top" />
					<h:selectManyListbox id="documentList" styleClass="formEntryTextHalf"
										value="#{pc_contractPrint.selectedDocuments}" style="height: 120px; width: 300px">
						<f:selectItems value="#{pc_contractPrint.documentList}" />
					</h:selectManyListbox>
				</h:panelGroup>
			</div>
		</div>

		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>

		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" 
				disabled="#{pc_contractPrint.auth.enablement['Commit'] || 
							pc_contractPrint.notLocked}"
				action="#{pc_contractPrint.commit}" styleClass="tabButtonRight" />	<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
