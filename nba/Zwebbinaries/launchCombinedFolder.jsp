<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

String uiProcessName = "";
uiProcessName = request.getQueryString();
int i = uiProcessName.indexOf("processName=");
if(i >= 0){
	uiProcessName = uiProcessName.substring(i + 12);
} else {
	uiProcessName = "";
}

boolean closeWindow = false;
if (request.getQueryString() != null) {
	closeWindow = (request.getQueryString().indexOf("close=true") >= 0);
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		if ('<%=closeWindow%>' == 'true'){
			parent.closeWindow();
		}
		top.loadPolicyAndClientOnRight('<%=basePath%>');
		top.refreshLeftFile();

		function run(){
			document.all["launchProcessForm:launchProcess"].click();
		}

	</script>
</head>
<body onload="run();">
<f:view>
	<h:form id="launchProcessForm">
		<div id="launchProcessSection" style="display: none">
			<h:commandButton id="launchProcess" action="#{pc_ProcessLauncher.launchProcess}"/>
	</h:form>
	<div id="Messages"><h:messages /></div>
</f:view>
</body>
</html>
