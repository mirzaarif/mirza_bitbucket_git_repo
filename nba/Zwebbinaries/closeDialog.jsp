
<!-- CHANGE LOG -->
 <!-- Audit Number   Version   Change Description -->
 <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->
 <!-- ACEL3255                 Login: If the user logs in, then cancels when prompted for a role, the user is unable to log in again without exiting. -->
 <!-- ACEL3241                 Parties: Change Insured function removes original insured from the claim entirely. -->
 <!-- NBA341         NB-1401   Internet Explorer 11 Certification -->
<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
		//ACEL3241 Starts
		String queryString = request.getQueryString();
		boolean refreshLeftSide = false;
		boolean refreshRightSide = false;
		
		if(queryString != null) {
			refreshLeftSide = queryString.indexOf("refreshLeftSide=true") >= 0;
			refreshRightSide = queryString.indexOf("refreshRightSide=true") >= 0;
		}		
		//ACEL3241 Ends
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Close Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		parent.closeWindow();
		parent.hideWait();

		//ACEL3255: If logon page then refresh it when dialog closes		
		if(parent.mainContentFrame.location.href.indexOf('faces/authorization/logon.jsp') != -1) {
			parent.mainContentFrame.location.href = parent.mainContentFrame.location.href;
		} else {
			if(!parent.windowsOpen()){
				parent.refreshDT();
			}
			
			//ACEL3241 Starts
			if ('<%= refreshLeftSide%>' == 'true'){
				parent.refreshLeftSide();
			}
			
			if ('<%= refreshRightSide%>' == 'true'){
				parent.refreshRightSide();
			}
			//ACEL3241 Ends
		}
		
		//NBA341 new function
		function focusParent() { 
			top.mainContentFrame.focus();
		}

	</script>
</head>
<body onload="focusParent();"> <!--NBA341-->
	CLOSE DIALOG
<f:view>
	<div id="Messages"><h:messages /></div>
</f:view>
<div id="MessageTitle">Information</div>
<script type="text/javascript">
		try{
		    //Begin SPR3356	
   		    var innerText=document.all["Messages"].innerHTML;
		    innerText=top.getInnerHTML(innerText);		    
		    document.all["Messages"].innerHTML= innerText;  
	        //End SPR3356  
			if(document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != ""){
				parent.showWindow('<%=path%>','faces/error.jsp', this);
			}
		} catch(err){
		}
	</script>
</body>
</html>
