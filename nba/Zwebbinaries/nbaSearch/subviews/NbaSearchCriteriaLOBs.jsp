<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- SPR3254           8      Search and Save Favorite Push Buttons Not Disabled Until Minimum Data Entry Requirements are Met -->
<!-- FNB011           NB-1101  Work Tracking -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGrid id="searchGroup11" columns="3" styleClass="" style="margin-top: -5px" cellspacing="0" cellpadding="0"
					columnClasses="formLabelTable,colSearchLobs,colMoreCodes"><!-- SPR3254 -->
		<h:column>
			<h:outputLabel id="addtnlLOBLbl" value="#{property.searchAddlLOB}" styleClass="formLabel" style="width: 150px; margin-top: 10px" /><!-- SPR3254 -->
		</h:column>
		<h:column>
			<h:dataTable id="srchLobs" styleClass="formTableData" style="width: 825px; table-layout: fixed; background: transparent" 
						cellspacing="0" cellpadding="0" rows="0"
						value="#{pc_searchCriteria.workValuesList}" var="lob" > <!-- NBA213 FNB011 -->
				<h:column>
					<h:panelGroup id="searchGroup12" styleClass="formDataDisplayLine" style="margin-left: -5px"><!-- SPR3254 -->
						<h:selectOneMenu id="lobNameDD" value="#{lob.name}" styleClass="formEntryTextFull" style="width: 290px"
									valueChangeListener="#{lob.changeLob}" onchange="resetTargetFrame();submit()"><!-- SPR3254 -->
							<f:selectItems id="lobNameList" value="#{pc_searchCriteria.allowableLobs}" /><!-- SPR3254 -->
						</h:selectOneMenu>
						<!-- begin FNB011 -->
						<h:inputText id="lobValueTxt" value="#{lob.textValue}" styleClass="formEntryText" style="width: 400px; margin-left: 85px;"
									rendered="#{lob.typeDefault}" />
						<h:inputText id="toDateTxt" value="#{lob.dateValue}" styleClass="formEntryText" style="width: 400px; margin-left: 85px;" 
									rendered="#{lob.typeDate}">
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
						<!-- end FNB011 -->
						<h:selectOneMenu id="lobValueDD" value="#{lob.textValue}" styleClass="formEntryTextFull" style="width: 400px; margin-left: 85px"
									rendered="#{lob.hasAllowableValues}"><!-- SPR3254, FNB011 -->
							<f:selectItems id="lobValueList" value="#{lob.allowableValues}" /><!-- SPR3254 -->
						</h:selectOneMenu>
					</h:panelGroup>
				</h:column>
			</h:dataTable>
		</h:column>
		<h:column>
			<h:commandLink id="btnAnotherLob" value="#{property.buttonAnotherLOB}" styleClass="formButtonInterface" style="margin-left: 5px;"
							action="#{pc_searchCriteria.addAnotherWorkValue}" /><!-- SPR3254 FNB011-->
		</h:column>
	</h:panelGrid>
