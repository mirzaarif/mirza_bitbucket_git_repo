<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3254           8      Search and Save Favorite Push Buttons Not Disabled Until Minimum Data Entry Requirements are Met -->
<!-- FNB004           NB-1101 PHI -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<!-- begin FNB004 -->
<h:panelGrid id="searchGroup8_1" columns="2" styleClass="formDataEntryLine" cellspacing="0" cellpadding="0">
	<h:column>
		<h:selectOneRadio id="dateRangeType" value="#{pc_searchCriteria.dateRangeType}" layout="pageDirection" enabledClass="formDisplayText"
			immediate="true" onclick="disableTimeFields(this.value)" style="width: 165px; margin-left: 5px">
			<f:selectItems id="dateRangeTypes" value="#{pc_searchCriteria.dateRangeTypes}" /><!-- SPR3254 -->
		</h:selectOneRadio>
	</h:column>
	<h:column>
<!-- end FNB004 -->
	<h:panelGrid id="searchGroup8" columns="2" styleClass="formDataEntryLine" cellspacing="0" cellpadding="0"><!-- SPR3254 -->
		<h:column>
			<h:outputLabel id="fromDateLbl" value="#{property.searchFromDate}" styleClass="formLabel" style="width: 90px" /><!-- SPR3254 FNB004-->
			<h:inputText id="fromDateTxt" value="#{pc_searchCriteria.fromDate}" styleClass="formEntryDate" style="width: 100px" 
				onkeyup="enableSearchButton();" onmouseout="enableSearchButton();"><!-- SPR3254 -->
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputLabel id="fromTimeLbl" value="#{property.searchFromTime}" styleClass="formLabel" style="width: 70px" /><!-- SPR3254 FNB004-->
			<h:inputText id="fromTimeTxt" value="#{pc_searchCriteria.fromTime}" styleClass="formEntryDate" style="width: 50px" ><!-- SPR3254 FNB004-->
				<f:convertDateTime pattern="#{property.shorttimePattern}"/>
			</h:inputText>
		</h:column>
		<h:column>
			<h:selectOneRadio id="fromTimePeriodRB" value="#{pc_searchCriteria.fromTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
							style="width: 100px; margin-right: 30px"><!-- SPR3254 FNB004-->
				<f:selectItems id="fromTimePeriodOpt" value="#{pc_searchCriteria.timePeriods}" /><!-- SPR3254 -->
			</h:selectOneRadio>
		</h:column>
		<h:column>
			<h:outputLabel id="toDateLbl" value="#{property.searchToDate}" styleClass="formLabel" style="width: 90px" /><!-- SPR3254 FNB004-->
			<h:inputText id="toDateTxt" value="#{pc_searchCriteria.toDate}" styleClass="formEntryDate" style="width: 100px" ><!-- SPR3254 -->
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
			<h:outputLabel id="toTimeLbl" value="#{property.searchToTime}" styleClass="formLabel" style="width: 70px" /><!-- SPR3254 FNB004 -->
			<h:inputText id="toTimeTxt" value="#{pc_searchCriteria.toTime}" styleClass="formEntryDate" style="width: 50px"><!-- SPR3254 FNB004 -->
				<f:convertDateTime pattern="#{property.shorttimePattern}"/>
			</h:inputText>
		</h:column>
		<h:column>
			<h:selectOneRadio id="toTimePeriodRB" value="#{pc_searchCriteria.toTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
							style="width: 100px; margin-right: 30px"><!-- SPR3254 FNB004 -->
				<f:selectItems id="toTimePeriodOpt" value="#{pc_searchCriteria.timePeriods}" /><!-- SPR3254 -->
			</h:selectOneRadio>
		</h:column>
	</h:panelGrid>
</h:column><!-- FNB004 -->
</h:panelGrid><!-- FNB004 -->
