<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3254           8      Search and Save Favorite Push Buttons Not Disabled Until Minimum Data Entry Requirements are Met -->
<!-- FNB004          NB-1101  PHI -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGroup id="searchGroup10" styleClass="formDataEntryLine"><!-- SPR3254 FNB004 -->
		<h:outputLabel id="searchFavLbl" value="#{property.searchFavorite}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
		<h:selectOneMenu id="searchFavDD" styleClass="formEntryText" value="#{pc_searchCriteria.favoriteSelected}" style="width: 425px"><!-- SPR3254 -->
				<f:selectItems id="searchFavList" value="#{pc_searchCriteria.favoritesListing}" /><!-- SPR3254 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="searchGroup11" styleClass="formButtonBar"><!-- SPR3254 FNB004 -->
		<h:commandButton id="btnDeleteFav" value="#{property.buttonDelete}" styleClass="formButton" style="margin-left: 150px"
					action="#{pc_searchCriteria.deleteFavorite}" onclick="setTargetFrame()"
					disabled="#{pc_searchCriteria.deleteFavoriteDisabled}" />
		<h:commandButton id="btnRetrieveFav" value="#{property.buttonRetrieve}" styleClass="formButton" style="margin-left: 250px"
					action="#{pc_searchCriteria.retrieveFavorite}" onclick="resetTargetFrame()"
					disabled="#{pc_searchCriteria.retrieveFavoriteDisabled}" />
	</h:panelGroup>
