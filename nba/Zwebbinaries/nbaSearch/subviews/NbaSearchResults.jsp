<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3266           7      Table Pane Focus Should Not Reset to Top When History Event is Selected -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA229            8      nbA Work List and Search Results Enhancement -->
<!-- FNB004         NB-1101   PHI -->
<!-- FNB012         NB-1101   Pooling -->
<!-- NBA305         NB-1301   Standardized CSC Product UI Colors -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<!-- begin NBA213 -->
<h:panelGroup id="collapseImage">  <!-- NBA305 -->
	<h:commandButton id="restoreImg"
		style="position: absolute;left:1232px;margin-top: 6px;" image="images/arrow_up.gif"
		onclick="expandTableClick();top.hideWait();return false;" />
	<h:commandButton id="collapseImg"
		style="position: absolute;left:1232px;margin-top: 6px;" image="images/arrow_down.gif"
		onclick="collapseTableClick();top.hideWait();return false;" />
</h:panelGroup>
<!-- end NBA213  -->
<h:panelGroup id="searchHeader" styleClass="ovDivTableHeader"
	style="width: 1250px">
	<h:panelGrid columns="15" styleClass="formTableHeader" cellspacing="0"
		columnClasses="ovColHdrIcon,ovColHdrText60,ovColHdrText100,ovColHdrText120,ovColHdrText140,ovColHdrDate,ovColHdrText100,ovColHdrText125,ovColHdrText125,ovColHdrText75,ovColHdrText75,ovColHdrText85,ovColHdrIconBig,ovColHdrIcon,ovColHdrDate"> <!-- NBA229 FNB004-->
		<h:commandLink id="searchHdrCol1" value="#{property.searchCol1}" style="width: 100%; height: 100%" 
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol1}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol2" value="#{property.searchCol2}" style="width: 100%; height: 100%" 
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol2}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol3" value="#{property.searchCol3}" style="width: 100%; height: 100%" 
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol3}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol4" value="#{property.searchCol4}" style="width: 100%; height: 100%" 
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol4}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol5" value="#{property.searchCol5}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol5}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol5a" value="#{property.searchCol13}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol13}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- NBA229-->
		<h:commandLink id="searchHdrCol6" value="#{property.searchCol6}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol6}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol7" value="#{property.searchCol7}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol7}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol8" value="#{property.searchCol8}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol8}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol9" value="#{property.searchCol9}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol9}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<h:commandLink id="searchHdrCol10" value="#{property.searchCol10}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol10}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
		<!-- begin FNB004 -->
		<h:commandLink id="searchHdrCol10a" value="#{property.searchCol10a}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol10a}" actionListener="#{pc_searchResultTable.sortColumn}" /> 
		<h:commandLink id="searchHdrCol10b" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol10b}"  style="width: 20px;text-align:left" actionListener="#{pc_searchResultTable.sortColumn}">
				<h:graphicImage url="images/link_icons/states.gif" style="margin-left: -5px;border:none; text-align:left;"></h:graphicImage>
		</h:commandLink>
		<!-- end FNB004 -->
		<!--Begin NBA229 -->
		<h:commandLink id="searchHdrCol11" 
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol11}"  style="width: 20px;text-align:left" actionListener="#{pc_searchResultTable.sortColumn}">
				<h:graphicImage url="images/workflow/significantReq.gif" style="margin-left: -5px;border:none; text-align:left;"></h:graphicImage>
		</h:commandLink> 
		<!-- End NBA229 -->
		<h:commandLink id="searchHdrCol12" value="#{property.searchCol12}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_searchResultTable.sortedByCol12}" actionListener="#{pc_searchResultTable.sortColumn}" /> <!-- SPR3266 -->
	</h:panelGrid>
</h:panelGroup>
<h:panelGroup id="searchData" styleClass="ovDivTableData12"
	style="width: 1250px; background-color: white">
	<h:dataTable id="searchTable" styleClass="ovTableData" cellspacing="0"
		binding="#{pc_searchResultTable.dataTable}" value="#{pc_searchResultTable.results}" var="result"
		rowClasses="#{pc_searchResultTable.rowStyles}"
		columnClasses="ovColIconTop,ovColText60,ovColText100,ovColText120,ovColText140,ovColDate,ovColText100,ovColText125,ovColText125,ovColText75,ovColText75,ovColText85,ovColText30,ovColIcon,ovColDate"> <!-- NBA229 FNB004-->
		<h:column>
				<h:commandLink id="searchCol1" styleClass="ovFullCellSelect" style="width: 100%; height: 100%;" onmousedown="saveTableScrollPosition();"
								action="#{pc_searchResultTable.selectRow}" immediate="true" >  <!-- SPR3266 -->
					<h:graphicImage url="images/comments/lock-closed-forlist.gif" title="#{result.lockedBy}"
									 styleClass="ovViewIcon#{result.locked}" style="border-width: 0px; margin-top: -3px;" />
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol2" onmousedown="saveTableScrollPosition();" action="#{pc_searchResultTable.selectRow}" immediate="true">  <!-- SPR3266 -->
					<h:inputTextarea readonly="true" value="#{result.businessArea}" styleClass="ovMultiLine#{result.draftText}" style="width: 60px" /><!-- FNB004 -->
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol3" onmousedown="saveTableScrollPosition();" action="#{pc_searchResultTable.selectRow}" immediate="true">  <!-- SPR3266 -->
					<h:inputTextarea readonly="true" value="#{result.queue}" styleClass="ovMultiLine#{result.draftText}" style="width: 100px" /> <!-- NBA229 FNB004 -->
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol4" value="#{result.contractNumber}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  <!-- SPR3266 -->
		</h:column>
		<h:column>
				<h:commandLink id="searchCol5" onmousedown="saveTableScrollPosition();" action="#{pc_searchResultTable.selectRow}" immediate="true" >  <!-- SPR3266, NBA229 -->
					<h:inputTextarea readonly="true" value="#{result.name}" styleClass="ovMultiLine#{result.draftText}" style="width: 140px" /> <!-- NBA229 -->
				</h:commandLink> <!-- NBA229 -->
		</h:column>
		<!-- Begin NBA229 -->
		<h:column>
				<h:commandLink id="searchCol5a" value="#{result.birthDate}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  
		</h:column>
		<!-- End NBA229 -->
		<h:column>
				<h:commandLink id="searchCol6" value="#{result.govtID}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  <!-- SPR3266 -->
		</h:column>
		<h:column>
				<h:commandLink id="searchCol7" onmousedown="saveTableScrollPosition();" action="#{pc_searchResultTable.selectRow}" immediate="true">  <!-- SPR3266 -->
					<h:inputTextarea readonly="true" value="#{result.workType}" styleClass="ovMultiLine#{result.draftText}" style="width: 125px" />
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol8" onmousedown="saveTableScrollPosition();" action="#{pc_searchResultTable.selectRow}" immediate="true">  <!-- SPR3266 -->
					<h:inputTextarea readonly="true" value="#{result.status}" styleClass="ovMultiLine#{result.draftText}" style="width: 125px" cols="35" /><!-- FNB004 -->
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol9" value="#{result.faceAmt}" style="width: 100%; text-align: right;" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  <!-- SPR3266 -->
		</h:column>
		<h:column>
				<h:commandLink id="searchCol10" value="#{result.agentID}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  <!-- SPR3266 -->
		</h:column>
		<h:column>
		<!-- begin FNB004 -->
				<h:commandLink id="searchCol10a" value="#{result.interviewDateTime}" onmousedown="saveTableScrollPosition();"
							styleClass="ovMultiLine#{result.draftText}" action="#{pc_searchResultTable.selectRow}" immediate="true" />
		</h:column>
		<h:column>
				<h:commandLink id="searchCol10b" value="#{result.residenceState}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />
		</h:column>
		<h:column>
		<!-- end FNB004 -->
				<h:commandLink id="searchCol11"	styleClass="ovFullCellSelect" onmousedown="saveTableScrollPosition();"
							action="#{pc_searchResultTable.selectRow}" immediate="true">  <!-- SPR3266 -->
					<h:selectBooleanCheckbox id="searchCol11a" value="#{result.significantRequirement}" rendered="#{result.significantRequirement}"
					styleClass="ovFullCellSelectCheckBox" style="margin-left: -15px; text-align:left;" readonly="true" /> <!-- NBA229-->
			</h:commandLink>
		</h:column>
		<h:column>
				<h:commandLink id="searchCol12" value="#{result.suspended}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" action="#{pc_searchResultTable.selectRow}" immediate="true" />  <!-- SPR3266 -->
		</h:column>
	</h:dataTable>
</h:panelGroup>
<!-- begin NBA213 -->
<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_searchResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.previousPageAbsolute}" />  <!-- SPR3266 -->
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_searchResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.previousPage}" />  <!-- SPR3266 -->
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_searchResultTable.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.previousPageSet}" />  <!-- SPR3266 -->
		<h:commandLink value="#{pc_searchResultTable.page1Number}" rendered="#{pc_searchResultTable.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page1}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page1Number}" rendered="#{pc_searchResultTable.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_searchResultTable.page2Number}" rendered="#{pc_searchResultTable.showPage2}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page2}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page2Number}" rendered="#{pc_searchResultTable.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_searchResultTable.page3Number}" rendered="#{pc_searchResultTable.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page3}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page3Number}" rendered="#{pc_searchResultTable.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_searchResultTable.page4Number}" rendered="#{pc_searchResultTable.showPage4}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page4}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page4Number}" rendered="#{pc_searchResultTable.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_searchResultTable.page5Number}" rendered="#{pc_searchResultTable.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page5}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page5Number}" rendered="#{pc_searchResultTable.currentPage5}" styleClass="ovStatusBarTextBold" />		
		<h:commandLink value="#{pc_searchResultTable.page6Number}" rendered="#{pc_searchResultTable.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page6}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page6Number}" rendered="#{pc_searchResultTable.currentPage6}" styleClass="ovStatusBarTextBold" />		
		<h:commandLink value="#{pc_searchResultTable.page7Number}" rendered="#{pc_searchResultTable.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page7}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page7Number}" rendered="#{pc_searchResultTable.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_searchResultTable.page8Number}" rendered="#{pc_searchResultTable.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.page8}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.page8Number}" rendered="#{pc_searchResultTable.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_searchResultTable.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.nextPageSet}" />  <!-- SPR3266 -->
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_searchResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.nextPage}" />  <!-- SPR3266 -->
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_searchResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_searchResultTable.nextPageAbsolute}" />  <!-- SPR3266 -->
		<h:outputText value="#{pc_searchResultTable.resultItemRange}" rendered="#{pc_searchResultTable.showResultItemRange}" styleClass="ovStatusBarTextBold" />  <!-- FNB012 -->
</h:panelGroup>
<!-- end NBA213 -->
<h:panelGroup styleClass="formDataDisplayLine"> <!-- NBA213, NBA305 -->
	<h:outputLabel value="#{property.searchSuspOrig}" styleClass="formLabel" style="width: 140px" />
	<h:outputText value="#{pc_searchResultTable.selectedSuspendOriginator}" styleClass="formDisplayText" style="width: 80px" />
	<h:outputLabel value="#{property.searchReason}" styleClass="formLabel" style="width: 80px" />
	<h:inputTextarea value="#{pc_searchResultTable.selectedSuspendReason}" readonly="true" styleClass="formDisplayTextMultiLine" 
			style="width: 300px" />
	<h:outputLabel value="#{property.searchActivation}" styleClass="formLabel" style="width: 100px" />
	<h:outputText value="#{pc_searchResultTable.selectedActivationDate}" styleClass="formDisplayDate">
		<f:convertDateTime pattern="#{property.datePattern}" />
	</h:outputText>
	<h:outputLabel value="#{property.searchActStatus}" styleClass="formLabel" style="width: 140px" />
	<h:inputTextarea value="#{pc_searchResultTable.selectedActivationStatus}" readonly="true" styleClass="formDisplayTextMultiLine"
		style="width: 325px" />
</h:panelGroup>
<h:panelGroup styleClass="ovButtonBar">
	<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="tabButtonLeft" 
		action="#{pc_searchResultTable.cancel}" onclick="setTargetFrame();" /> <!-- NBA213 -->
	<h:commandButton id="btnActivate" value="#{property.buttonActivate}" styleClass="tabButtonRight-2" style="left: 970px"
			action="#{pc_searchResultTable.activate}" immediate="true" disabled="#{pc_searchResultTable.activateDisabled}" />
	<h:commandButton id="btnUnsuspend" value="#{property.buttonUnsuspend}" styleClass="tabButtonRight-1" style="left: 1064px"
			action="#{pc_searchResultTable.unsuspend}" immediate="true" disabled="#{pc_searchResultTable.unsuspendDisabled}" />
	<h:commandButton id="btnOpen" value="#{property.buttonOpen}" styleClass="tabButtonRight" style="left: 1158px" action="#{pc_searchResultTable.open}"
			disabled="#{pc_searchResultTable.openDisabled}" onclick="setTargetFrame();" /> <!-- NBA213 -->
</h:panelGroup>
	<h:inputHidden id="searchTableVScroll" value="#{pc_searchResultTable.VScrollPosition}" />  <!-- SPR3266 -->
