<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3254           8      Search and Save Favorite Push Buttons Not Disabled Until Minimum Data Entry Requirements are Met -->
<!-- FNB004          NB-1101  PHI -->
<!--  FNB011			NB-1101 Work Tracking -->
<!-- NBA340          NB-1501   Mask Government ID -->

<script language="JavaScript" type="text/javascript">
	function noPercent(e)
	{
		var key;
		var keyChar;
		var percentCheck;

		if (window.event) {  // IE
			key = e.keyCode;
		} else if (e.which) { // Netscape/Firefox/Opera
			key = e.which;
		}
		keyChar = String.fromCharCode(key);
		percentCheck = /%/;
		return !percentCheck.test(keyChar);
	}
</script>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

		<h:panelGroup id="searchGroup3" styleClass="formDataEntryTopLine"><!-- SPR3254 -->
			<h:outputLabel id="businessAreaLbl" value="#{property.searchBusinessArea}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
			<h:selectOneMenu id="businessAreaDD" value="#{pc_searchCriteria.businessArea}" styleClass="formEntryText" style="width: 290px"
						valueChangeListener="#{pc_searchCriteria.changeBusinessArea}" onchange="submit()"><!-- SPR3254 -->
				<f:selectItems id="businessAreaList" value="#{pc_searchCriteria.businessAreas}" /><!-- SPR3254 -->
			</h:selectOneMenu>
			<h:outputLabel id="queueLbl" value="#{property.searchQueue}" styleClass="formLabel" /><!-- SPR3254 -->
			<h:selectOneMenu id="queueDD" value="#{pc_searchCriteria.queue}" styleClass="formEntryText" style="width: 290px"><!-- SPR3254 -->
				<f:selectItems id="queueList" value="#{pc_searchCriteria.queues}" /><!-- SPR3254 -->
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup  id="searchGroup4" styleClass="formDataEntryLine"><!-- SPR3254 -->
			<h:outputLabel id="workTypeLbl" value="#{property.searchWorkType}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
			<h:selectOneMenu id="workTypeDD" value="#{pc_searchCriteria.workType}" styleClass="formEntryText" style="width: 290px"
						valueChangeListener="#{pc_searchCriteria.changeWorkType}" onchange="resetTargetFrame();submit()"><!-- SPR3254 -->
				<f:selectItems id="workTypeList" value="#{pc_searchCriteria.workTypes}" /><!-- SPR3254 -->
			</h:selectOneMenu>
			<h:outputLabel id="statusLbl" value="#{property.searchStatus}" styleClass="formLabel" /><!-- SPR3254 -->
			<h:selectOneMenu id="statusDD" value="#{pc_searchCriteria.status}" styleClass="formEntryText" style="width: 550px"><!-- SPR3254 -->
				<f:selectItems id="statusList" value="#{pc_searchCriteria.statuses}" /><!-- SPR3254 -->
			</h:selectOneMenu>
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGroup id="searchGroup5" styleClass="formDataEntryLine"><!-- SPR3254 -->
			<h:outputLabel id="contractNumLbl" value="#{property.searchContractNumber}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
			<h:inputText id="contractNumTxt" value="#{pc_searchCriteria.contractNumber}" styleClass="formEntryText"
						style="width: 250px" maxlength="15" onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();"
						onmouseout="enableSearchButton();"/><!-- SPR3254 -->
			<h:outputLabel id="govtIdLbl" value="#{property.searchGovtID}" styleClass="formLabel" /><!-- SPR3254 -->
			<h:inputText id="govtIdInput" value="#{pc_searchCriteria.govtID}" styleClass="formEntryText" maxlength="9" style="width: 250px" 
				onkeyup="enableSearchButton();" onmouseout="enableSearchButton();" /><!-- SPR3254 NBA340-->
			<h:inputHidden id="govtIdKey" value="#{pc_searchCriteria.govtIdKey}"></h:inputHidden><!-- NBA340 -->
			<h:outputLabel id="agentIdLbl" value="#{property.searchAgentID}" styleClass="formLabel"/><!-- SPR3254 -->
			<h:inputText id="agentIdTxt" value="#{pc_searchCriteria.agentID}" styleClass="formEntryText" style="width: 250px"
						onkeypress="toUpperCase(event);" /><!-- SPR3254 -->
		</h:panelGroup>
		<h:panelGroup id="searchGroup6" styleClass="formDataEntryLine"><!-- SPR3254 -->
			<h:outputLabel id="lastNameLbl" value="#{property.searchLastName}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
			<h:inputText id="lastNameTxt" value="#{pc_searchCriteria.lastName}" styleClass="formEntryText" style="width: 250px"
							onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();" onmouseout="enableSearchButton();"/><!-- SPR3254 -->
			<h:outputLabel id="firstNameLbl" value="#{property.searchFirstName}" styleClass="formLabel" /><!-- SPR3254 -->
			<h:inputText id="firstNameTxt" value="#{pc_searchCriteria.firstName}" styleClass="formEntryText" style="width: 250px"
							onkeypress="toUpperCase(event);" /><!-- SPR3254 -->
			<h:outputLabel id="middleNameLbl" value="#{property.searchMiddleName}" styleClass="formLabel" /><!-- SPR3254 -->
			<h:inputText id="middleNameTxt" value="#{pc_searchCriteria.middleName}" styleClass="formEntryText" 
							onkeypress="toUpperCase(event);" style="width: 250px;" /><!-- SPR3254 FNB011 -->
		</h:panelGroup>
		<h:panelGroup id="searchGroup7" styleClass="formDataEntryLine"><!-- SPR3254 -->
			<h:outputLabel id="uwQueueLbl" value="#{property.searchUWQueue}" styleClass="formLabel" style="width: 150px" /><!-- SPR3254 -->
			<h:selectOneMenu id="uwQueueDD" styleClass="formEntryText" value="#{pc_searchCriteria.underwriterQueue}" style="width: 250px" 
				onchange="enableSearchButton();"><!-- SPR3254 -->
					<f:selectItems id="uwQueueList" value="#{pc_searchCriteria.underwriterQueues}" /><!-- SPR3254 -->
			</h:selectOneMenu>
			<h:selectBooleanCheckbox id="lstNonRevRecRB" value="#{pc_searchCriteria.lastNonReviewedReqReceived}" style="margin-left: 30px" /><!-- SPR3254 -->
			<h:outputLabel id="lastReqLbl" value="#{property.searchLastReq}" styleClass="formLabelRight" /><!-- SPR3254 -->
		</h:panelGroup>
