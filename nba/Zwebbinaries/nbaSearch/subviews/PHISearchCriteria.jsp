<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB004           NB-1101      PHI -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGrid id="searchGroup8" columns="3" styleClass="formDataEntryLine" cellspacing="0" cellpadding="0"><!-- FNB004-->
		<h:column>
			<h:outputLabel id="interviewFromDate" value="#{property.interviewFromDate}" styleClass="formLabel" style="width: 150px" />
		</h:column>
		<h:column>
			<h:inputText id="interviewFromDateTxt" value="#{pc_searchCriteria.interviewFromDate}" styleClass="formEntryDate" style="width: 100px" 
				onkeyup="enableSearchButton();" onmouseout="enableSearchButton();">
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
		</h:column>
		<h:column>
			<h:selectBooleanCheckbox id="confidential" value="#{pc_searchCriteria.confidential}" style="margin-left: 30px" />
			<h:outputLabel id="confidentialLbl" value="#{property.confidential}" styleClass="formLabelRight" />
		</h:column>
		<h:column>
			<h:outputLabel id="interviewToDate" value="#{property.searchToDate}" styleClass="formLabel" style="width: 150px" />
		</h:column>
		<h:column>
			<h:inputText id="interviewToDateTxt" value="#{pc_searchCriteria.interviewToDate}" styleClass="formEntryDate" style="width: 100px" >
				<f:convertDateTime pattern="#{property.datePattern}"/>
			</h:inputText>
		</h:column>
		<h:column>
			<h:selectBooleanCheckbox id="rush" value="#{pc_searchCriteria.rush}" style="margin-left: 30px" />
			<h:outputLabel id="rushlbl" value="#{property.rush}" styleClass="formLabelRight" />
		</h:column>
	</h:panelGrid>
