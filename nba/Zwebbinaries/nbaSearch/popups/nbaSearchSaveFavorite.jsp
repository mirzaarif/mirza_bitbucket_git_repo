<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3535           8      Doctor Pop Up View has a White Stripe Behind Push Buttons -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.searchSaveFavoriteTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=175;
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_searchSave'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_searchSave'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
			<h:form id="form_searchSave" >
				<h:panelGroup styleClass="entryLinePadded" style="margin-top: 50px">
					<h:outputLabel value="#{property.searchFavorite}" styleClass="entryLabel" />
					<h:inputText value="#{pc_searchCriteria.favoriteSelected}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:selectBooleanCheckbox value="#{pc_searchCriteria.favoriteSaveAsPrimary}" style="margin-left: 122px" />
					<h:outputLabel value="#{property.searchFavoritePrimary}" styleClass="formLabelRight" />
				</h:panelGroup>
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton value="#{property.buttonCancel}"
									action="#{pc_searchCriteria.cancel}" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton value="#{property.buttonSave}"
									action="#{pc_searchCriteria.save}" onclick="setTargetFrame();" styleClass="buttonRight"/>
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div><!-- SPR3535 -->
		</body>
	</f:view>
</html>
	