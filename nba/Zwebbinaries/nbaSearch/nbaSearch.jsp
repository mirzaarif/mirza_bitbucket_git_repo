<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA136            6      In Tray and Search Rewrite -->
<!-- SPR3266           7      Table Pane Focus Should Not Reset to Top When History Event is Selected -->
<!-- NBA213            7      Unified User Interface -->
<!-- SPR3254           8      Search and Save Favorite Push Buttons Not Disabled Until Minimum Data Entry Requirements are Met -->
<!-- NBA271            8      Implementation Of Business Entities in nbA -->
<!-- FNB012         NB-1101   Pool Processing -->
<!-- SPRNBA-436     NB-1101   Page scrolling with screen resolution other than 1280 x 1024 -->
<!-- NBA305         NB-1301   Standardized CSC Product UI Colors -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<!-- begin NBA213 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.searchTitle}"/></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script type="text/javascript" src="javascript/global/scroll.js"></script>
            <script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
	        <script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
			<script language="JavaScript" type="text/javascript">
				var width=1250; 
				var height=750;
				var contextpath = '<%=path%>'; //NBA340
				//begin SPRNBA-436
				if (window.screen.width < 1280) {
					width = window.screen.width - 30;
				}
				if (window.screen.height < 900) {
					height = window.screen.height - 110;
				}
				//end SPRNBA-436
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_searchIntray'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_searchIntray'].target='';
					return false;
				}
				function openBF(){
					//begin NBA213
					var isExpanded = document.getElementById('expand').value;
					var isOverridden = document.getElementById('form_searchIntray:isOverridden').value;
					//if we have overriden the table expansion, do the opposite of what is returned 
					if (isOverridden != null && isOverridden == 'true' && isExpanded != null) {
						isExpanded = toggle(isExpanded);
					} 
					if (isExpanded != null && isExpanded == 'true'){
						expandTable();
					} else {
						
						collapseTable();
					}
					//end NBA213
				}
				
				//SPR3266 New Method
				function saveTableScrollPosition() {
					saveScrollPosition('form_searchIntray:searchResults:searchData', 'form_searchIntray:searchResults:searchTableVScroll');
					return false;
				}
				//SPR3266 New Method
				function scrollTablePosition() {
					scrollToPosition('form_searchIntray:searchResults:searchData', 'form_searchIntray:searchResults:searchTableVScroll');
					return false;
				}
				/*
				 *	Expand a table to full screen and override the default setting on expand
				 *  arrow click
				 */
				//NBA213 New Function
				function expandTableClick() {
					expandTable();
					overrideTable();
				}
				/*
				 *	Collapse a table to half screen and override the default setting on collapse
				 *  arrow click
				 */
				//NBA213 New Function
				function collapseTableClick() {
					collapseTable();
					overrideTable();
				}
				/*
				 *	Expand a table to full screen 
				 */
				function expandTable(){
					document.getElementById('searchCriteria').style.display = "none";
					//begin FNB004
					if (height <= 750) {
						document.getElementById('searchResult').style.height = height;
					} else {
						document.getElementById('searchResult').style.height = '790px';
					}
					//end FNB004
					document.getElementById('form_searchIntray:searchResults:restoreImg').style.visibility = "hidden";
					document.getElementById('form_searchIntray:searchResults:collapseImg').style.visibility = "visible";
					document.getElementById('expand').value ="true"; //set for an onclick expansion
					//begin FNB004
					if (height <= 750) {
						document.getElementById('form_searchIntray:searchResults:searchData').style.height = height - 100;
					} else {
						document.getElementById('form_searchIntray:searchResults:searchData').style.height = '690px';
					}
					//end FNB004
				}
				//FNB004 New Method
				function resizeForPHI(){
					if(document.getElementById('form_searchIntray:searchCriteria:workTypeDD').value=="NBPHI"){
						document.getElementById('searchCriteria').style.height="460px";
						if (window.screen.height >= 900) {
							height = 790;
						}
					} else {
						document.getElementById('searchCriteria').style.height="415px";
						if (window.screen.height >= 900) {
							height = 750;
						}
					}
				}
				//NBA213
				/*
				 *	Collapse a table to 1/2 view
				 */
				function collapseTable(){
					document.getElementById('searchCriteria').style.display = "";
					document.getElementById('searchResult').style.height = '330px';
					//document.getElementById('expand').value ="false"; //set for an onclick collapse
					document.getElementById('form_searchIntray:searchResults:restoreImg').style.visibility = "visible";
					document.getElementById('form_searchIntray:searchResults:collapseImg').style.visibility = "hidden";
					//show the buttons when the table collapses
					document.getElementById('form_searchIntray:searchResults:searchData').style.height = null;
				}
				/*
				 *	Toggle two string values that represent true and false.
				 *  Used for hidden fields returning strings not booleans 
				 */
				 //NBA213 New Function
				function toggle(value){
					if (value == 'true') {
						return 'false';
					} else {
						return 'true';
					}
				}
				
				/*
				 *	Set the override indicator on a click
				 */
				 //NBA213 New Function
				 function overrideTable(){
				 	var isOverridden = document.getElementById('form_searchIntray:isOverridden').value;
					if (isOverridden == null) {
			        	isOverridden = 'true';
					} else {
						isOverridden = toggle(isOverridden);
					}	
					document.getElementById('form_searchIntray:isOverridden').value = isOverridden;
				 }
				 /*
				 *	Reset the override indicator to false
				 */
				 //NBA213 New Function
				 function resetOverride(){
				 	document.getElementById('form_searchIntray:isOverridden').value = 'false';
				 }
				 /*
				 *	Disable the Search and Save favourite buttons until minimum search criteria entered
				 */
				  //SPR3254 New Function
				 function enableSearchButton() {
					toDisable = true;
					if(document.getElementById('form_searchIntray:searchCriteria:businessAreaDD').value!= "-1"){ //NBA271
						if(document.getElementById('form_searchIntray:searchCriteria:contractNumTxt').value!= "" || 
						document.getElementById('form_searchIntray:searchCriteria:govtIdInput').value!= "" || //NBA340
						document.getElementById('form_searchIntray:searchCriteria:lastNameTxt').value!= "" ||
						document.getElementById('form_searchIntray:searchCriteria:uwQueueDD').value!= "-1"){
							toDisable = false;				
						} else if(document.getElementById('form_searchIntray:searchCriteria:workTypeDD').value !="-1") {	//FNB012
			  				toDisable = false;	//FNB012
						}	//FNB012
					}//NBA271
					document.getElementById('form_searchIntray:btnSaveFav').disabled = toDisable;	
					document.getElementById('form_searchIntray:btnSearch').disabled = toDisable;					
				}
				
				//FNB004 New Method
				function disableTimeFields(value){
					if(value == '2'){ //for timestamp
						document.getElementById('form_searchIntray:searchDates:fromTimeTxt').disabled = true;
						document.getElementById('form_searchIntray:searchDates:toTimeTxt').disabled = true;
						document.getElementById('form_searchIntray:searchDates:fromTimePeriodRB').disabled = true;
						document.getElementById('form_searchIntray:searchDates:toTimePeriodRB').disabled = true;
						
					} else {
						document.getElementById('form_searchIntray:searchDates:fromTimePeriodRB').disabled = false;
						document.getElementById('form_searchIntray:searchDates:toTimePeriodRB').disabled = false;
						document.getElementById('form_searchIntray:searchDates:fromTimeTxt').disabled = false;
						document.getElementById('form_searchIntray:searchDates:toTimeTxt').disabled = false;
					}
				}
				/*
				 * Method to zoom in if resolution is 1024*1280
				 */
				 //SPRNBA-436 new method
				function resize() {
					if (window.screen.width < 1280) {
						document.getElementById('searchScroll').style.width = width;
						document.getElementById('searchScroll').style.overflowX = "scroll";
					}
					document.getElementById('searchScroll').style.height = height;
					if (window.screen.height < 900) {
						document.getElementById('searchScroll').style.overflowX = "scroll";
						document.getElementById('searchScroll').style.overflowY = "scroll";
					}
				}
								
				//NBA340 new Method
				$(document).ready(function() {			
	              $("input[name$='govtIdInput']").govtidValue();
		        })
		        
			</script>
		</head>
		<body  id="nbASearch" onload="resizeForPHI();popupInit();openBF();scrollTablePosition();enableSearchButton();resize();">  <!-- SPR3266 NBA213 SPR3254 SPRNBA-436 -->
		<PopulateBean:Load serviceName="RETRIEVE_SEARCH_INTRAY" value="#{pc_searchIntray}" />
			<h:form id="form_searchIntray" onsubmit="saveTableScrollPosition();" style="overflow: auto">  <!-- SPR3266 -->
				<!--  NBA213 code deleted -->
				<div id="searchScroll" style="background-color:Transparent; width: 1250px; overflow-x: hidden; height: 750px; overflow-y: hidden" >    <!-- SPRNBA-436 -->
				<div id="searchCriteria" style="width: 1250px; height: auto;background-color:Transparent; overflow-x: hidden; overflow-y: hidden;  z-index=0"> <!-- NBA213 -->
					<f:subview id="searchCriteria">
						<c:import url="/nbaSearch/subviews/NbaSearchCriteria.jsp" />
					</f:subview>
					<!-- begin FNB004 -->
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td class="formSplitLeft">
								<f:subview id="PHISearchCriteria" rendered="#{pc_searchCriteria.PHISearch}">
									<c:import url="/nbaSearch/subviews/PHISearchCriteria.jsp" />
								</f:subview>
							</td>
							<td class="formSplitRight">
							</td>
						</tr>
					</table>
					<!-- end FNB004 -->
		
		
					<hr class="formSeparator" />

					<h:panelGroup id="searchGroup1" styleClass="formDataEntryLine" style="overflow-x: hidden; overflow-y: auto; height:30px"><!-- SPR3254 -->
						<f:subview id="searchAddlLobs">
							<c:import url="/nbaSearch/subviews/NbaSearchCriteriaLOBs.jsp" />
						</f:subview>
					</h:panelGroup>
		
					<hr class="formSeparator" />
		
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td class="formSplitLeftBorder">
								<f:subview id="searchDates">
									<c:import url="/nbaSearch/subviews/NbaSearchCriteriaDates.jsp" />
								</f:subview>
							</td>
							<td class="formSplitRight">
								<f:subview id="searchFavorites">
									<c:import url="/nbaSearch/subviews/NbaSearchCriteriaFavorites.jsp" />
								</f:subview>
							</td>
						</tr>
					</table>
		
					<hr class="formSeparator" />
					<!-- begin NBA213 -->
					<h:panelGroup id="searchGroup2" styleClass="formButtonBar"> <!-- SPR3254 SPRNBA-436-->
						<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft"
										action="#{pc_searchCriteria.clear}" onclick="resetTargetFrame();" />		 <!-- SPRNBA-436-->
						<h:commandButton id="btnSaveFav" value="#{property.buttonSaveFav}" styleClass="formButtonRight-1" style="left: 1039px; width: 97px;"
										action="#{pc_searchCriteria.saveFavorite}" onclick="setTargetFrame();" />		 <!-- SPRNBA-436-->
						<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight" style="left: 1143px;"
										action="#{pc_searchCriteria.search}" onclick="resetOverride();resetTargetFrame();" />		 <!-- SPRNBA-436-->
					<!-- end NBA213 -->
					</h:panelGroup>
				</div>
				<!-- NBA213 code deleted -->
				<div id="searchResult" style="background-color:Transparent; width: 1250px; overflow-x: hidden; overflow-y: hidden; z-index=9999"> <!-- NBA213, NBA305 -->
					<f:subview id="searchResults">
						<c:import url="/nbaSearch/subviews/NbaSearchResults.jsp" />
					</f:subview>
				</div>
				</div>  <!-- SPRNBA-436 -->
				<h:inputHidden id="isOverridden" value="false" />
			</h:form>
			<div id="Messages" style="display:none">
				<h:messages />
			</div>
		</body> <!-- NBA213 -->
	</f:view>
</html>
	