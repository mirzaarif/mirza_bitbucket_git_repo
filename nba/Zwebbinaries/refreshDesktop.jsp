<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if(request.getServerPort() == 80){
	basePath = request.getScheme()+"://"+request.getServerName() + path+"/";
} else {
	basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path+"/";
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Refresh Desktop</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		parent.refreshDT();
	</script>
</head>

<body>
Refresh Desktop
<f:view>
	<div id="Messages">	
		<h:messages />
	</div>
</f:view>
<script type="text/javascript">
	try {
   		var innerText=document.all["Messages"].innerHTML;
		innerText=top.getInnerHTML(innerText);		    
		document.all["Messages"].innerHTML= innerText; 

		if(document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != ""){
			parent.showWindow('<%=path%>','faces/error.jsp', this);
		}
	} catch (err) {
	}
</script>
</body>
</html>
