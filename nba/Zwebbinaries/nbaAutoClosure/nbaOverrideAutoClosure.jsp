<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA254          NB-1101    Automatic Closure and Refund of CWA -->
<!-- FNB016			 NB-1101  	Configuration Changes -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
     <head>
			<base href="<%=basePath%>">
			<title>Override Auto Closure</title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
				var width=380;
				var height=150;  
				function setTargetFrame() {
					document.forms['form_overrideAutoClosure'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_overrideAutoClosure'].target='';
					return false;
				}
			</script>
	</head>
	<body onload="popupInit();">
	<f:view>	    
		   <f:loadBundle basename="properties.nbaApplicationData" var="property"/>		
		   <PopulateBean:Load serviceName="RETRIEVE_CLOSURE_DETAILS" value="#{pc_overrideAutoClosure}" />
	   	   <h:form id="form_overrideAutoClosure" >
			  <h:panelGroup id="closureDate_PGroup" styleClass="formDataEntryLine">
				  <h:outputText id="closureDate_OT" value="#{property.pendingClosureDate}" styleClass="formLabel" style="width: 150px;"/>
				  <h:inputText id="closureDate_EF" value="#{pc_overrideAutoClosure.closureDate}" styleClass="formEntryDate" style="width: 100px">
				     <f:convertDateTime pattern="#{property.datePattern}"/>
			      </h:inputText>
				  <h:outputText id="overridden_OT" value="#{property.overridden}" styleClass="formLabel" style="width: 100px;" rendered="#{pc_overrideAutoClosure.override}"/>				
			  </h:panelGroup>

			  <h:panelGroup id="closureType_PGroup" styleClass="formDataEntryLine">
				  <h:outputText id="closureType_OT" value="#{property.pendingClosureType}" styleClass="formLabel" style="width: 150px;"/>
				  <h:outputText id="closureType_OV" value="#{pc_overrideAutoClosure.closureType}" styleClass="formEntryText" style="width: 150px;" />
			  </h:panelGroup>
		
			  <h:panelGroup styleClass="buttonBar">
				  <h:commandButton id="overrideCancel_CButton" value="#{property.buttonCancel}" action="#{pc_overrideAutoClosure.cancel}" onclick="setTargetFrame();" styleClass="buttonLeft" />
				  <h:commandButton id="overrideCommit_CButton" value="#{property.buttonCommit}" action="#{pc_overrideAutoClosure.commit}" onclick="setTargetFrame();" styleClass="buttonRight" disabled="#{pc_overrideAutoClosure.disableCommit || pc_overrideAutoClosure.auth.enablement['CommitButton']}"/> <!-- FNB016  -->
			  </h:panelGroup>
			  <div id="Messages" style="display: none"><h:messages /></div></div>
		  </h:form>
	</f:view>
	</body>
</html>
		