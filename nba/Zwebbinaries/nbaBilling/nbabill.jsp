<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA303       	NB-1401     Billing User Interface Rewrite  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
    String path = request.getContextPath();
    String basePath = "";
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
		var contextpath = '<%=path%>';
		var fileLocationHRef  = '<%=basePath%>' + 'nbaBilling/nbabill.faces';
		function setTargetFrame() {
			document.forms['form_billing'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_billing'].target='';
			return false;
		}
	</script>
</head>
<body onload="filePageInit()" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_BILLINGINFO" value="#{pc_nbaBill}" />
	<h:form id="form_billing" styleClass="inputFormMat">
		<h:panelGroup id="PGroup1" styleClass="inputForm">
			<f:subview id="billInfo">
				<c:import url="/nbaBilling/subviews/billingInfo.jsp" />
			</f:subview>
			<f:subview id="billCntrlNavigation">
				<c:import url="/nbaBilling/subviews/billingControlTable.jsp" />
			</f:subview>
			<h:panelGroup id="billButtonBar" styleClass="ovButtonBar" style="width:595px; margin-left:10px; margin-bottom:10px;">
				<h:commandButton id="btnBillDelete" value="#{property.buttonDelete}" disabled="#{pc_controlTable.disableDelete}"
					styleClass="ovButtonLeft" style="margin-left: 20px;" onclick="setTargetFrame();"
					action="#{pc_controlTable.delete}" />
				<h:commandButton id="btnBillView" value="#{property.buttonViewUpdate}" disabled="#{pc_controlTable.disableDelete}"
					styleClass="ovButtonRight-1" action="#{pc_controlTable.view}" style="left: 416px;"/>
				<h:commandButton id="btnBillCreate" value="#{property.buttonCreate}" styleClass="ovButtonRight"
					action="#{pc_controlTable.create}" style="left: 510px;"/>
			</h:panelGroup>
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup id="billingCntrlButtonBar" styleClass="tabButtonBar">
			<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_nbaBill.refresh}" onclick="resetTargetFrame()"
				styleClass="tabButtonLeft" />
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_nbaBill.commit}" styleClass="tabButtonRight" 
				disabled="#{pc_nbaBill.auth.enablement['Commit'] || pc_nbaBill.notLocked}"/>
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
