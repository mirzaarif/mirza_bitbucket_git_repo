<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA303       	NB-1401     Billing User Interface Rewrite  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="controlTitle" value="#{property.control}"
		styleClass="sectionSubheader" style="margin-top:10px; width:595px; margin-left:10px;" />
	<h:panelGroup id="controlHeader" styleClass="formDivTableHeader">
		<h:panelGrid columns="4" styleClass="formTableHeader" cellspacing="0" style="text-align: center;"
			columnClasses="ovColHdrText190,ovColHdrText190,ovColHdrText195">
			<h:commandLink id="cntrlHdrCol1" value="#{property.uwCovContractCol4}" styleClass="ovColSorted#{pc_controlTable.sortedByCol1}" actionListener="#{pc_controlTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
			<h:commandLink id="cntrlHdrCol2" value="#{property.contractChgIdentificationMsg}" styleClass="ovColSorted#{pc_controlTable.sortedByCol2}" actionListener="#{pc_controlTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
			<h:commandLink id="cntrlHdrCol3" value="#{property.cntrlCol3}" styleClass="ovColSorted#{pc_controlTable.sortedByCol3}" actionListener="#{pc_controlTable.sortColumn}"
			onmouseover="resetTargetFrame()" immediate="true"/>
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="controlTableData" styleClass="formDivTableData6">
		<h:dataTable id="controlTableDataTable" styleClass="formTableData"
			cellspacing="0" rows="0" border="0"
			binding="#{pc_controlTable.dataTable}"
			value="#{pc_controlTable.controlList}" var="controlItem"
			rowClasses="#{pc_controlTable.rowStyles}"
			columnClasses="ovColText190,ovColText190,ovColText190">
			<h:column>
				<h:commandLink id="effective" styleClass="ovFullCellSelect"
					style="width: 100%;" action="#{pc_controlTable.selectControlRow}" immediate="true">
					<h:outputText value="#{controlItem.effective}">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="identification" value="#{controlItem.identification}" styleClass="ovFullCellSelect" 
				style="width:100%" action="#{pc_controlTable.selectControlRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="account" value="#{controlItem.bankAcc}"
					styleClass="ovFullCellSelect" style="width:100%" 
					action="#{pc_controlTable.selectControlRow}" immediate="true" rendered="#{!controlItem.eftCreditcard}" />
				<h:commandLink id="accountCredit"
					value="#{controlItem.ccNumberFormatted}" styleClass="ovFullCellSelect"
					style="width:100%" action="#{pc_controlTable.selectControlRow}"
					immediate="true" rendered="#{controlItem.eftCreditcard}" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>