<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA303       	NB-1401     Billing User Interface Rewrite  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="controlTitleReadOnly" value="#{property.control}"
		styleClass="formSectionBar" style="margin-top:10px; width:628px; margin-left:0px;" />
	<h:panelGroup id="controlHeaderReadOnly" styleClass="ovDivTableHeader">
		<h:panelGrid columns="4" styleClass="ovTableHeader" cellspacing="0"
			style="text-align: center;"
			columnClasses="ovColHdrText190,ovColHdrText190,ovColHdrText195">
			<h:outputText id="cntrlHdrCol01" value="#{property.uwCovContractCol4}" />
			<h:outputText id="cntrlHdrCol02" value="#{property.contractChgIdentificationMsg}" />
			<h:outputText id="cntrlHdrCol03" value="#{property.cntrlCol3}" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="controlTableDataReadOnly" styleClass="ovDivTableData6">
		<h:dataTable id="controlTableDataTableReadOnly" styleClass="ovTableData"
			cellspacing="0" rows="0" border="0"
			binding="#{pc_controlTable.dataTable}"
			value="#{pc_controlTable.controlList}" var="controlItem"
			rowClasses="#{pc_controlTable.rowStyles}"
			columnClasses="ovColText190,ovColText190,ovColText190">
			<h:column>
				<h:outputText id="effectiveReadOnly" styleClass="ovFullCellSelect"
					style="width: 100%;" value="#{controlItem.effective}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:column>
			<h:column>
				<h:outputText id="identificationReadOnly" value="#{controlItem.identification}" 
				styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
			<h:column>
				<h:outputText id="accountReadOnly" value="#{controlItem.bankAcc}"
					styleClass="ovFullCellSelect" style="width:100%" rendered="#{!controlItem.eftCreditcard}"/>
					<h:outputText id="accountCredit" value="#{controlItem.ccNumberFormatted}"
					styleClass="ovFullCellSelect" style="width:100%" rendered="#{controlItem.eftCreditcard}"/>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>