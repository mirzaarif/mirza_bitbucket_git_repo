<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA303       	NB-1401     Billing User Interface Rewrite  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="billingGroup1" styleClass="formDataEntryLine">
		<h:outputText id="form" value="#{property.csForm}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="formDD" value="#{pc_billInfo.formTypeSelected}" styleClass="formEntryText" 
			style="width: 160px" immediate="true" onchange="submit()">
			<f:selectItems id="formList" value="#{pc_billInfo.formTypeList}" />
		</h:selectOneMenu>
		<h:outputText id="timing" value="#{property.timing}" styleClass="formLabel" style="width: 135px"
			rendered="#{!pc_billInfo.directBill}" />
		<h:selectOneMenu id="timingDD" value="#{pc_billInfo.timingSelected}" styleClass="formEntryText" style="width: 160px" immediate="true"
			onchange="submit()" rendered="#{!pc_billInfo.directBill}">
			<f:selectItems id="timingList" value="#{pc_billInfo.timingList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="billingGroup2" styleClass="formDataEntryLine">
		<h:outputText id="mode" value="#{property.csMode}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="modeDD" value="#{pc_billInfo.modeTypeSelected}" styleClass="formEntryText" style="width: 160px"
			valueChangeListener="#{pc_billInfo.specialFreqSelected}" immediate="true" onchange="submit()">
			<f:selectItems id="modeList" value="#{pc_billInfo.modeTypeList}" />
		</h:selectOneMenu>
		<h:outputText id="draftDay" value="#{property.draftDay}" styleClass="formLabel" style="width: 135px"
			rendered="#{!pc_billInfo.directBill}" />
		<h:inputText id="draftDayEF" value="#{pc_billInfo.draftDay}" styleClass="formEntryText" style="width: 160px"
			rendered="#{!pc_billInfo.directBill}">
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="billingGroup3" styleClass="formDataEntryLine">
		<h:outputText id="premium" value="#{property.csPremium}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="premiumEF" value="#{pc_billInfo.premium}" styleClass="formEntryText" style="width: 160px"
			disabled="#{pc_billInfo.premiumDisabled}">
			<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
		</h:inputText>
		<h:outputText id="billNo" value="#{property.billNo}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="billNoEF" value="#{pc_billInfo.billNo}" styleClass="formEntryText" style="width: 160px"
			disabled="#{pc_billInfo.cyberlifeBESSystem}">
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="billingGroup4" styleClass="formDataEntryLine">
		<h:outputText id="premiumtype" value="#{property.premiumType}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="premiumtypeDD" value="#{pc_billInfo.premTypeSelected}" styleClass="formEntryText"
			style="width: 160px" immediate="true" onchange="submit()" disabled="#{pc_billInfo.cyberlifeBESSystem}">
			<f:selectItems id="premiumtypeList" value="#{pc_billInfo.premTypeList}" />
		</h:selectOneMenu>
		<h:outputText id="loanInterestBill" value="#{property.loanInterestBill}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="loanInterestBillDD" value="#{pc_billInfo.loanInterestBillSelected}"
			styleClass="formEntryText" style="width: 160px" immediate="true" onchange="submit()" disabled="#{pc_billInfo.cyberlifeBESSystem}">
			<f:selectItems id="tloanInterestBillList" value="#{pc_billInfo.loanInterestBillList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="billingGroup5" styleClass="formDataEntryLine">
		<h:outputText id="proxytype" value="#{property.proxytype}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="proxytypeDD" value="#{pc_billInfo.proxyTypeSelected}" styleClass="formEntryText"
			style="width: 160px" immediate="true" onchange="submit()" disabled="#{pc_billInfo.cyberlifeBESSystem}">
			<f:selectItems id="proxytypeList"
				value="#{pc_billInfo.proxyTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<f:verbatim>
		<hr id="billingSeparator1" class="formSeparator" />
	</f:verbatim>
	<h:panelGroup id="billingGroup6" styleClass="formDataEntryTopLine">
		<h:outputText id="billing" value="#{property.billing}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="billingDD" value="#{pc_billInfo.billingSelected}"
			styleClass="formEntryText" style="width: 160px" disabled="#{!pc_billInfo.billingDDEnabled}" 
			immediate="true" onchange="submit()">
			<f:selectItems id="billingList" value="#{pc_billInfo.billingList}" />
		</h:selectOneMenu>
		<h:outputText id="specialHandling" value="#{property.specialHandling}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="specialHandlingDD" value="#{pc_billInfo.specialHandlingSelected}"
			styleClass="formEntryText" style="width: 160px" immediate="true" onchange="submit()">
			<f:selectItems id="specialHandlingList" value="#{pc_billInfo.specialHandlingList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="billingGroup7" styleClass="formDataEntryLine">
		<h:outputText id="paidToDate" value="#{property.paidToDate}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="paidToDateEF" value="#{pc_billInfo.paidToDate}" styleClass="formEntryText" style="width: 160px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:outputText id="additionalLevel" value="#{property.additionalLevel}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="additionalLevelEF" value="#{pc_billInfo.additionalLevel}" styleClass="formEntryText"
			style="width: 160px" disabled="#{!pc_billInfo.additionalLevelEnabled}">
			<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="billingGroup8" styleClass="formDataEntryLine">
		<h:outputText id="billedToDate" value="#{property.billedToDate}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="billedToDateEF" value="#{pc_billInfo.billedToDate}" styleClass="formEntryText" style="width: 160px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:outputText id="lastBillDate" value="#{property.lastBillDate}" styleClass="formLabel" style="width: 135px" />
		<h:inputText id="lastBillDateEF" value="#{pc_billInfo.lastBillDate}" styleClass="formEntryText" style="width: 160px">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="billingGroup9" styleClass="formDataEntryLine">
		<h:outputText id="lastBillKind" value="#{property.lastBillKind}" styleClass="formLabel" style="width: 135px" />
		<h:selectOneMenu id="lastBillKindDD" value="#{pc_billInfo.lastBillKindSelected}"
			styleClass="formEntryText" style="width: 160px" immediate="true" onchange="submit()">
			<f:selectItems id="lastBillKindList" value="#{pc_billInfo.lastBillKindList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="specialFreq" rendered="#{pc_billInfo.specialFreq}">
		<f:verbatim>
			<hr id="billingSeparator2" class="formSeparator" />
		</f:verbatim>
		<h:panelGroup id="billingGroup10" styleClass="formDataEntryTopLine">
			<h:outputText id="quotedPremiumBasis" value="#{property.quotedPremiumBasis}" 
				styleClass="formLabel" style="width: 165px" />
			<h:inputText id="quotedPremiumBasisEF" value="#{pc_billInfo.quotedPremiumBasis}" styleClass="formEntryText"
				style="width: 130px" disabled="#{!pc_billInfo.billAmountDisabled}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
			</h:inputText>
			<h:selectOneRadio id="radioQuotedBilling" value="#{pc_billInfo.radioQuotedBillingSelected}"
				layout="lineDirection" styleClass="radioLabel radioGrpBilling" disabled="#{!pc_billInfo.billAmountDisabled}">
				<f:selectItem id="weekly" itemLabel="#{property.weekly}" itemValue="1" />
				<f:selectItem id="annually" itemLabel="#{property.annually}" itemValue="2" />
			</h:selectOneRadio>
		</h:panelGroup>
		<h:panelGroup id="billingGroup11" styleClass="formDataEntryLine">
			<h:outputText id="initialBilledToDate" value="#{property.initialBilledToDate}" 
				styleClass="formLabel" style="width: 165px" />
			<h:inputText id="initialBilledToDateEF" value="#{pc_billInfo.initialBilledToDate}"
				styleClass="formEntryText" style="width: 130px">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
			<h:outputText id="initialDeductionDate" value="#{property.initialDeductionDate}" 
				styleClass="formLabel" style="width: 165px" />
			<h:inputText id="initialDeductionDateEF" value="#{pc_billInfo.initialDeductionDate}"
				styleClass="formEntryText" style="width: 130px">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="billingGroup12" styleClass="formDataEntryLine">
			<h:outputText id="billAmount" value="#{property.billAmount}" styleClass="formLabel" style="width: 165px" />
			<h:inputText id="billAmountEF" value="#{pc_billInfo.billAmount}" styleClass="formEntryText" style="width: 130px" 
				disabled="true" rendered="#{!pc_billInfo.billAmountDisabled}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
			</h:inputText>
			<h:outputText id="billAmountDisplay" value="#{pc_billInfo.billAmount}" styleClass="formEntryText"
				style="width: 130px" rendered="#{pc_billInfo.billAmountDisabled}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
			</h:outputText>
			<h:outputText id="firstMonthlyDate" value="#{property.firstMonthlyDate}" styleClass="formLabel" style="width: 165px" />
			<h:inputText id="firstMonthlyDateEF" value="#{pc_billInfo.firstMonthlyDate}" styleClass="formEntryText" style="width: 130px">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="billingGroup13" styleClass="formDataEntryLine">
			<h:outputText id="excessPremium" value="#{property.excessPremium}" styleClass="formLabel" style="width: 165px" />
			<h:inputText id="excessPremiumEF" value="#{pc_billInfo.excessPremium}" styleClass="formEntryText"
				style="width: 130px" disabled="#{pc_billInfo.excessPremiumLevelDisabled}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency"/>
			</h:inputText>
			<h:outputText id="paidToDate2" value="#{property.paidToDate}" styleClass="formLabel" style="width: 165px" />
			<h:inputText id="paidToDateEF2" value="#{pc_billInfo.paidToDate2}" styleClass="formEntryText" style="width: 130px">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="billingGroup14" styleClass="formDataEntryLine">
			<h:outputText id="payrollFrequency" value="#{property.payrollFrequency}" styleClass="formLabel" style="width: 165px" />
			<h:selectOneMenu id="payrollFrequencyDD" value="#{pc_billInfo.payrollFrequencySelected}"
				styleClass="formEntryText" style="width: 130px" immediate="true" onchange="submit()">
				<f:selectItems id="payrollFrequencyList" value="#{pc_billInfo.payrollFrequencyList}" />
			</h:selectOneMenu>
			<h:outputText id="firstSkipMonth" value="#{property.firstSkpMonth}" styleClass="formLabel" style="width: 165px" />
			<h:inputText id="firstSkipMonthEF" value="#{pc_billInfo.firstSkipMonth}" styleClass="formEntryText" style="width: 130px">
			</h:inputText>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>