<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA303       	NB-1401     Billing User Interface Rewrite  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
    String path = request.getContextPath();
    String basePath = "";
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Create New Billing Control</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script type="text/javascript" src="javascript/global/jquery.js"></script>
<script type="text/javascript" src="include/creditCard.js"></script>
<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaBilling/nbaBillingControl.faces';
		var formID = 'form_billingControl';
		var ccNumberTokenized = 'cardNumberTokenized';
		function setTargetFrame() {
			document.forms['form_billingControl'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_billingControl'].target='';
			return false;
		}
		function formatCreditCardNumber(field,hiddenField) {
			return formatCardNumber(formID,'',field,hiddenField);
		}
		//send the credit card details for performing credit card tokenization
		function processCreditCardInformation(isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID){
			var subviewID = '';
			var errorMessageDiv = '';
			var skipCreditCardValidation = top.ccTokenizationFrame.isSkipCCValidation();
			var tokenizationServiceURL = top.ccTokenizationFrame.getTokenizationURL();
			var tokenizationServiceRequest = top.ccTokenizationFrame.getTokenizationRequest();
			processCreditCardTokenization(formID,subviewID,skipCreditCardValidation,tokenizationServiceURL,tokenizationServiceRequest,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv);				
		}
		//if the credit card tokenization request is not complete, disables the add, addNew or Update button.
		function authorizeCommit(formID,subviewID,tokenized){
		 	var ccInfoTokenized = true;
		 	var subviewID = '';
		 	ccInfoTokenized = tokenized;
		 	setValue(formID,subviewID,ccNumberTokenized,tokenized);
		 	ccInfoTokenized = getValue(formID,subviewID,ccNumberTokenized);
		 	btnAdd = document.getElementById('form_billingControl:add');
		 	btnAddNew = document.getElementById('form_billingControl:addNew');
		 	btnUpdate = document.getElementById('form_billingControl:update');
			if(btnUpdate != null){
				btnUpdate.disabled = !ccInfoTokenized;
			}
			if(btnAdd != null){
				btnAdd.disabled = !ccInfoTokenized;
				btnAddNew.disabled = !ccInfoTokenized;
			}
		}
	</script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body onload="filePageInit()" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_BILLINGCONTROL" value="#{pc_billingControl}" />
	<h:form id="form_billingControl">
		<f:subview id="billCntrltable">
			<c:import url="/nbaBilling/subviews/billingControlReadOnly.jsp" />
		</f:subview>
		<div id="createNew" class="inputFormMat">
			<div class="inputForm" style="height: 320px;">
				<h:outputLabel id="cnTitle1" value="#{property.createBillingCntrl}" styleClass="formTitleBar" rendered="#{!pc_billingControl.update}"/> 
				<h:outputLabel id="cnTitle2" value="#{property.viewBillingCntrl}" styleClass="formTitleBar" rendered="#{pc_billingControl.update}"/>
				<h:panelGroup id="cntrlGrp1" styleClass="formDataEntryLine" rendered="#{!pc_billingControl.directorPayrollBill}">
					<h:outputText id="sign1" value="#{property.sign1}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="sign1EF" value="#{pc_billingControl.sign1}" styleClass="formEntryText" style="width: 450px">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp2" styleClass="formDataEntryLine" rendered="#{!pc_billingControl.directorPayrollBill}">
					<h:outputText id="sign2" value="#{property.sign2}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="sign2EF" value="#{pc_billingControl.sign2}" styleClass="formEntryText" style="width: 450px">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp3" styleClass="formDataEntryLine" rendered="#{!pc_billingControl.directorPayrollBill}">
					<h:outputText id="sign3" value="#{property.sign3}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="sign3EF" value="#{pc_billingControl.sign3}" styleClass="formEntryText" style="width: 450px">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp4" styleClass="formDataEntryLine">
					<h:outputText id="effectiveDate" value="#{property.uwEffective}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="effectiveEF" value="#{pc_billingControl.effective}" styleClass="formEntryText" style="width: 155px">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>
					<h:outputText id="bankAccount" value="#{property.bankAccount}" styleClass="formLabel" style="width: 140px"
						rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}" />
					<h:inputText id="bankAccountEF" value="#{pc_billingControl.bankAcc}" styleClass="formEntryText" style="width: 155px"
						rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp5" styleClass="formDataEntryLine">
					<h:outputText id="identificationControl" value="#{property.appEntIdentification}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="identificationEF" value="#{pc_billingControl.identification}"
						styleClass="formEntryText" style="width: 155px">
					</h:inputText>
					<h:outputText id="transitNo" value="#{property.transitNo}" styleClass="formLabel" style="width: 140px"
						rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}" />
					<h:inputText id="transitNoEF" value="#{pc_billingControl.transitNo}" styleClass="formEntryText" style="width: 155px"
						rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp6" styleClass="formDataEntryTopLine">
					<h:outputText id="accType" value="#{property.accType}" styleClass="formLabel" style="width: 140px"
						rendered="#{!pc_billingControl.directorPayrollBill}" />
					<h:selectOneMenu id="accTypeDD" value="#{pc_billingControl.accType}" styleClass="formEntryText" style="width: 155px"
						rendered="#{!pc_billingControl.directorPayrollBill}" valueChangeListener="#{pc_billingControl.eftTypeSelected}" onchange="submit()" immediate="true">
						<f:selectItems id="accTypeList" value="#{pc_billingControl.accTypeList}" />
					</h:selectOneMenu>
					<h:outputText id="branchNo" value="#{property.branchNo}" styleClass="formLabel" style="width: 140px"
						rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}" />
					<h:inputText id="branchNoEF" value="#{pc_billingControl.branchNo}"
						styleClass="formEntryText" style="width: 155px" rendered="#{!pc_billingControl.directorPayrollBill && !pc_billingControl.eftCreditcard}">
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp7" styleClass="formDataEntryLine" rendered="#{!pc_billingControl.directorPayrollBill && pc_billingControl.eftCreditcard}">
					<h:outputText id="ccType" value="#{property.appEntCCType}" styleClass="formLabel" style="width: 140px" />
					<h:selectOneMenu id="ccTypeDD" value="#{pc_billingControl.ccType}" styleClass="formEntryText" style="width: 155px"
						onchange="formatCreditCardNumber('ccNoEF','cardNumberEFHidden');processCreditCardInformation(false,'ccTypeDD','ccLabel','cardNumberEFHidden','','ccNoEF','');">
						<f:selectItems id="ccTypeList" value="#{pc_billingControl.ccTypeList}" />
					</h:selectOneMenu>
					<h:inputHidden id="ccLabel" value="#{pc_billingControl.selectedCardLabel}"></h:inputHidden>
					<h:outputText id="ccNo" value="#{property.appEntCCNumber}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="ccNoEF" value="#{pc_billingControl.ccNumberFormatted}" styleClass="formEntryText" style="width: 155px"
						onchange="formatCreditCardNumber('ccNoEF','cardNumberEFHidden');processCreditCardInformation(false,'ccTypeDD','ccLabel','cardNumberEFHidden','','ccNoEF','');">
					</h:inputText>
					<h:inputHidden id="cardNumberEFHidden" value="#{pc_billingControl.ccNo}"></h:inputHidden>
					<h:inputHidden id="cardNumberTokenized" value="#{pc_billingControl.ccNumberTokenized}"></h:inputHidden>
				</h:panelGroup> 
				<h:panelGroup id="cntrlGrp8" styleClass="formDataEntryLine" rendered="#{!pc_billingControl.directorPayrollBill && pc_billingControl.eftCreditcard}">
					<h:outputText id="expiryDate" value="#{property.expiryDate}" styleClass="formLabel" style="width: 140px" />
					<h:inputText id="expiryDateEF" value="#{pc_billingControl.expirationDate}"
						styleClass="formEntryText" style="width: 155px" immediate="true">
						<f:convertDateTime pattern="#{property.creditCardDatePattern}" dateStyle="short"/>
					</h:inputText>
				</h:panelGroup> 
				<h:panelGroup styleClass="formButtonBar">
					<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" action="#{pc_billingControl.cancel}" immediate="true" />
					<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
						action="#{pc_billingControl.clear}" />
					<h:commandButton id="update" value="#{property.buttonUpdate}" styleClass="formButtonRight" onclick="resetTargetFrame();"
						action="#{pc_billingControl.update}" rendered="#{pc_billingControl.update}"/>
					<h:commandButton id="addNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" onclick="resetTargetFrame();"
						action="#{pc_billingControl.addNew}" rendered="#{!pc_billingControl.update}"/>
					<h:commandButton id="add" value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="resetTargetFrame();"
						action="#{pc_billingControl.add}" rendered="#{!pc_billingControl.update}"/>
				</h:panelGroup> 
				<h:inputHidden id="guid" value="#{pc_billingControl.guid}"></h:inputHidden>
				<h:inputHidden id="txnDate" value="#{pc_billingControl.txnDate}"></h:inputHidden>
				<h:inputHidden id="txnTime" value="#{pc_billingControl.txnTime}"></h:inputHidden>
			</div>
		</div>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
