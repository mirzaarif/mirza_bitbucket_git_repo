<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description --> 
<!-- NBA225			   8	  nbA Comments -->
<!-- SPRNBA-436          NB-1101  Page scrolling with screen resolution other than 1280 x 1024 -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}

pagecode.authorization.NbaAuthorization bean = new pagecode.authorization.NbaAuthorization();
java.util.Map enablement = bean.getAuth().getEnablement();
Boolean isSecureCommentAuthorized = (Boolean) enablement.get("SecureComment");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>" target="control">
<title>Workflow File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
	var context = '<%=path%>';
	var topOffset = 8;

	function getArgs() {
		try {
			var args = new Object();
			var query = location.search.substring(1);
			var pairs = query.split("&");
			for (var i = 0; i < pairs.length; i++) {
				var pos = pairs[i].indexOf('=');
				if (pos == -1) {
					continue;
				}
				var argname = pairs[i].substring(0, pos);
				var value = pairs[i].substring(pos + 1);
				args[argname] = value;
			}
			return args;
		}catch(er){
			reportException(er, "common - getArgs");
		}
	}
	
	function initialize(){
		var args = getArgs();
		if(args['embedded'] != 'true'){
			document.body.className = "desktopBody";
			 
			element = document.getElementById("TitleBar");
			if(element != null){
				element.style.display = "inline";
				element.style.visibility = "visible";
			}
			element = document.getElementById("mainTitle");
			if(element != null){
				element.className = "textMainTitleBar";
			}
			element = document.getElementById("mainTitleHelp");
			if(element != null){
				element.className = "textMainTitleBar";
			}
		} else {
			document.body.className = "updatePanel";
		}
<%
	if (isSecureCommentAuthorized != null && isSecureCommentAuthorized.booleanValue()) {
 %>
		selectWorkView('nbaComments/nbaCommentsOverview.faces');
<% } else { %>
		selectWorkView('uw/file/nbaCommentsOverview.faces');
<% } %>
	}	
	
	var lastSelection = null;
	
	function selectWorkView(page){ 
		loadPage('<%=basePath%>' + page);
		return false;
	}

	function initFileSize() {
		tabHeight = window.screen.availHeight - 250;	<!--SPRNBA-436-->
		document.getElementById('file').height = tabHeight;		
	}

	//-->
 	</script>
	<script type="text/javascript" src="javascript/loader.js"></script>
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="initialize();">
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<h:form id="commentSelectForm"> 

							<iframe name="file" src="" class="commentsFile"  height="100%" width="100%" frameborder="0" scrolling="NO" onload="initFileSize();"></iframe>
	</h:form>
</f:view>

</body>
</html>
