<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA154            6      Requirement view -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7      Unified User Interface -->
<!-- SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implementation -->
<!-- FNB011 		NB-1101   Work Tracking -->
<!-- FNB016 		NB-1101   Configuration Changes -->
<!-- SPRNBA-751     NB-1401   Not Prompted When Leave Requirements Business Function with Uncommitted Draft Rows -->
<!-- SPRNBA-747     NB-1401   General Code Clean Up -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Requirements and Impairments Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/nbapopup.js"></script>	
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaRequirements/nbaReqOverview.faces';//NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_reqimpoverview'].target='controlFrame';
			document.getElementById("form_reqimpoverview:parentView").value = 'requirementBF';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_reqimpoverview'].target='';
			return false;
		}
		
		//SPRNBA-751 New Method
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_reqimpoverview']['form_reqimpoverview:draftChanges'].value;  //NBA213
		}
		
		//SPRNBA-751 New Method
		function initialize() {
			setDraftChanges();
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit(); initialize();" style="overflow-x: hidden; overflow-y: scroll">  <!-- SPRNBA-751 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="INITIALIZE_REQUIREMENTS_BF" value="#{pc_reqBF}" />  <!-- FNB016 -->
		<PopulateBean:Load serviceName="RETRIEVE_REQUIREMENTS" value="#{pc_reqimpNav}" />
		
		<h:form id="form_reqimpoverview">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
			</f:subview>
			<h:outputLabel id="reqTableTitle" value="#{property.requirementTitle}" styleClass="sectionSubheader" /> 
			<f:subview id="reqOverviewTbl">
				<c:import url="/uw/subviews/NbaRequirementsTable.jsp" />
			</f:subview>
			<h:panelGrid id="reqTableButtonBar" columns="1" styleClass="ovButtonBar" cellpadding="0" cellspacing="0" ><!-- SPR3399 -->
				<h:panelGroup id="requirementButtonBar" styleClass="ovButtonBar"><!-- SPR3399 -->
					<h:commandButton id="btnReqDelete" value="#{property.buttonDelete}" styleClass="ovButtonLeft"
							disabled="#{pc_reqimpNav.selectedInsuredsRequirements.deleteDisabled}" immediate="true"
							onclick="setTargetFrame();" action="#{pc_reqimpNav.selectedInsuredsRequirements.deleteRequirement}" /> 
					<h:commandButton id="btnReqView" value="#{property.buttonViewUpdate}" disabled="#{pc_reqimpNav.selectedInsuredsRequirements.updateDisabled}" styleClass="ovButtonRight-1"
							 onclick="resetTargetFrame()" action="#{pc_reqimpNav.selectedInsuredsRequirements.updateRequirement}" immediate="true" /> <!-- NBA212, SPRNBA-747 -->
					<h:commandButton id="btnReqCreate" value="#{property.buttonCreate}" styleClass="ovButtonRight" action="#{pc_reqimpNav.selectedInsuredsRequirements.createRequirement}" 
							disabled="#{pc_reqimpNav.createDisabled}"
							immediate="true" onclick="resetTargetFrame()"/> <!-- NBA212, NBA329 -->
				</h:panelGroup>
			</h:panelGrid>
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />
			</f:subview>

			<h:panelGroup id="reqtabButtonBar" styleClass="tabButtonBar"><!-- SPR3399 -->
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_reqimpNav.refresh}" immediate="true"
					onclick="resetTargetFrame()" styleClass="tabButtonLeft" /> <!-- NBA212 -->
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_reqimpNav.commit}"
					onclick="resetTargetFrame()"
					disabled="#{pc_reqimpNav.auth.enablement['Commit'] || 
								pc_reqimpNav.notLocked}" 
					styleClass="tabButtonRight" />	<!-- NBA212, NBA213 -->
			</h:panelGroup>
			<h:inputHidden id="draftChanges" value="#{pc_reqimpNav.draftChanges}" /> <!-- SPRNBA-751 -->
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	