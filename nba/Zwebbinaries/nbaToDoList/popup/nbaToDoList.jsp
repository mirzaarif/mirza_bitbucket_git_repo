<%-- CHANGE LOG 
     Audit Number   Version   Change Description
     NBA251           8       nbA Case Manager and Companion Case Assignment
     FNB011         NB-1101   Work Tracking
     SPRNBA-798     NB-1401   Change JSTL Specification Level
     SPRNBA-976     NB-1601   Null session variables result from overlapping transactions initiated on the To-Do List user interface
--%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>	
		<PopulateBean:Load serviceName="RETRIEVE_TODO_LIST" value="#{pc_toDoList}" />
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.toDoList}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<%-- SPRNBA-976 deleted --%>
			<script type="text/javascript" src="javascript/global/file.js"></script>
			<%-- SPRNBA-976 deleted --%>
			<script language="JavaScript" type="text/javascript">
							
				var contextpath = '<%=path%>';	//FNB011		
				
				function setTargetFrame() {		
					document.forms['form_todo'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_todo'].target='';
					return false;
				}
				//SPRNBA-976 code deleted
				function refreshParentWindow(){
					if (document.getElementById('refreshParentWindow').value == 'true'){
						top.myOpener.top.refreshLeftFile();  //SPRNBA-976
						top.focus;
					}
					document.getElementById('refreshParentWindow').value = 'false';
				}
				//SPRNBA-976 new function
				function close() {
					document.forms['form_todo']['form_todo:closeButton'].click();
					return true;
				}
				
			</script>
		</head> 
		<body onload="filePageInit();refreshParentWindow();">
			<div id="contentArea" style="height:100%;background-color:Transparent; overflow-x: hidden; overflow-y: hidden">
				<h:form id="form_todo" >
					<f:subview id="tabHeader">
						<c:import url="/nbaToDoList/subviews/nbaToDoListDetails.jsp" />
					</f:subview>
					<h:panelGroup id="pGroup5" styleClass="buttonBar">
						<%-- begin SPRNBA-976 --%>
						<h:commandButton id="cancelButton" value="#{property.buttonCancel}" action="#{pc_toDoList.cancel}" styleClass="buttonLeft"
							onclick="if (#{pc_toDoList.workItemLocked}) { setTargetFrame(); } else {resetTargetFrame();}" />
						<h:commandButton id="refreshButton" value="#{property.buttonRefresh}" action="#{pc_toDoList.refresh}" styleClass="buttonLeft-1" 
							onclick="if (#{pc_toDoList.workItemLocked}) { setTargetFrame(); } else {resetTargetFrame();}" />
						<h:commandButton id="closeButton" action="#{pc_toDoList.close}" style="visibility: hidden" />
						<%-- end SPRNBA-976 --%>
					</h:panelGroup>
				</h:form>
			</div>
			<div id="Messages" style="display:none">
				<h:messages />
			</div>
		</body>
		<h:inputHidden id="refreshParentWindow" value="#{pc_toDoList.refreshParentWindow}" /> 
	</f:view>
</html>