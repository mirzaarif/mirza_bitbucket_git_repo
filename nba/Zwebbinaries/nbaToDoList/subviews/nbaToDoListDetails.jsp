<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA251           8       nbA Case Manager and Companion Case Assignment -->
<!-- SPRNBA-976        NB-1601   Null session variables result from overlapping transactions initiated on the To-Do List user interface -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />

	<!-- Table Column Headers -->
	<h:panelGroup id="pGroup1" styleClass="popupDivTableHeader"
		style="margin-left: 0px;">
		<h:panelGrid id="pGrid1" columns="3" styleClass="popupTableHeader"
			style="width: 599px;"
			columnClasses="ovColHdrText50,ovColHdrText270,ovColHdrText270"
			cellspacing="0">
			<h:outputLabel id="listHdrCol1" value="" style="text-align: left;"
				styleClass="ovColSortedFalse" />
			<h:outputLabel id="listHdrCol2" value="#{property.colHdrType}"
				style="text-align: left;" styleClass="ovColSortedFalse" />
			<h:outputLabel id="listHdrCol3" value="#{property.colHdrReason}"
				style="text-align: left;" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="pGroup2" styleClass="popupDivTableData6" rendered="#{pc_toDoList.anyWorkToDisplay}"
		style="height: 260px; width: 600px;margin-left: 0px;overflow-x: hidden;overflow-y: auto">
		<h:dataTable id="toDoTable" styleClass="formTableDataRightButtons" cellspacing="0"
			rows="0" binding="#{pc_toDoList.resultsTable}"
			value="#{pc_toDoList.workItemList}" var="workItem"
			rowClasses="#{pc_toDoList.rowStyles}"  
			columnClasses="ovColText50,ovColText270,ovColText270">
			<h:column>
				<h:panelGroup id="pGroup3">
					<h:commandButton id="closeFolder" image="./images/rqa/folder.gif"
						rendered="#{!workItem.locked}" onclick="resetTargetFrame();"
						disabled="#{pc_toDoList.workItemLocked}"
						action="#{pc_toDoList.lockWork}" style="margin-left: 5px;"  /> <!-- SPRNBA-976 -->
					<h:commandButton id="openFolder"
						image="./images/rqa/folderopen.gif"
						rendered="#{workItem.locked}" onclick="resetTargetFrame();"
						action="#{pc_toDoList.completeWork}" style="margin-left: 5px;"  />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:commandLink id="workTypeId1" action="" style="margin-left: 10px;">
					<h:inputTextarea id="workTypeId2" styleClass="ovMultiLine" readonly="true" 
						value="#{workItem.workType}" style="width: 100%"/>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="reasonId1" action="" style="margin-left: 10px;">
					<h:inputTextarea id="reasonId2" styleClass="ovMultiLine" readonly="true" 
						value="#{workItem.reasonWorkRecieved}" style="width: 100%"/>
				</h:commandLink>
			</h:column>
		</h:dataTable>
		
	</h:panelGroup>
	<h:panelGroup id="pGroup4" style="height: 260px;padding-top: 130px;margin-left: 150px;" styleClass="shTextLarge" rendered="#{!pc_toDoList.anyWorkToDisplay}">
		<h:outputText value="#{property.noWorkToDisplay}"  />
	</h:panelGroup>
</jsp:root>
