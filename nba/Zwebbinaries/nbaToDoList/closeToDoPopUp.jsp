<%-- CHANGE LOG
     Audit Number   Version   Change Description
     NBA251           8       nbA Case Manager and Companion Case Assignment
     SPRNBA-976     NB-1601   Null session variables result from overlapping transactions initiated on the To-Do List user interface
--%>

<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

	//begin SPRNBA-976
	boolean refreshMain = false;
	String queryString = request.getQueryString();
	if (queryString != null) {
		refreshMain = queryString.indexOf("refreshMain=true") >= 0;
	}
	//end SPRNBA-976

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%-- begin SPRNBA-976 --%>
	<head>
		<base href="<%=basePath%>">
		<title>Close ToDo Dialog</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="theme/accelerator.css">
		<script type="text/javascript">
			if (<%= refreshMain %> == true) {
				top.myOpener.top.refreshLeftFile();
			}
			top.window.close();
		</script>
	</head>
	<body>
	</body>
<%-- end SPRNBA-976 --%>
</html>
