<%--
     Audit Number   Version   Change Description
     SPRNBA-976     NB-1601   Null session variables result from overlapping transactions initiated on the To-Do List user interface
--%>

<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Refresh TO-DO</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		function refresh() {
			try {
				//close the confirmation popup
				parent.closeWindow();

				//check for any errors
	   		    var innerText=document.all["Messages"].innerHTML;
			    innerText=top.getInnerHTML(innerText);		    
			    document.all["Messages"].innerHTML= innerText;  
				if (document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != "") {
					parent.showWindow('<%=path%>','faces/error.jsp', this);
				} else {
					//refresh nbA desktop
					if (top.myOpener) {
						top.myOpener.top.refreshLeftFile();
					}
					top.refreshDT();
				}
			} catch(err) {
				top.reportException(err, "refresh To-Do");
			}
		}
	</script>
</head>
<body onload="refresh()">
	REFRESH TO-DO
	<f:view>
		<div id="Messages"><h:messages /></div>
	</f:view>
</body>
</html>
