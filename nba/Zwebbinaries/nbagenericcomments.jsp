<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA024            2      Conversion of applet based views to HTML -->
<!-- NBA118            5      Work Item Identification Project-->

<%@ page language="java" import="com.csc.fsg.nba.foundation.*" errorPage="errorpage.jsp" %>
<HTML>
	<HEAD>
		<TITLE>Comments</TITLE>
		<!-- NBA118 code deleted -->
	
		<%@ include file="include/copyright.html" %>		
	</HEAD>	
	<LINK rel="stylesheet" type="text/css" name="stylesheet" href="include/styles.css"> <!--NBA118-->
	<BODY scroll="yes" style="overflow:auto" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">		
		<SCRIPT language="JavaScript" src="include/common.js"></SCRIPT>
		<FORM name="nba_frm" method="post" target="fData">		
			<TEXTAREA name="comment" Style="LEFT: 10px; WIDTH: 375px; POSITION: absolute; TOP: 10px; HEIGHT: 250px" class="readonlyTextArea" readonly></TEXTAREA>
			<BUTTON name="close" Style="LEFT: 300px; WIDTH: 85px; POSITION: absolute; TOP: 265px; HEIGHT: 25px" onclick= "top.close()" accesskey="C"><U>C</U>lose</U> </BUTTON>
			<SCRIPT>
				//Add hidden fields here.
		        addSecureField("ACTION");
		    </SCRIPT>    
		</FORM>
		<%@ include file="include/dialog.js" %>
	</BODY>	
</HTML>