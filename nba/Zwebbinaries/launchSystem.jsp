<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch External System</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		function process(){
			if(top.lastButtonClicked != null){
				try{
					top.lastButtonClicked.isClicked="false";
				}catch(err){
				}
			}
			var data = document.all.WorkDefinition.innerHTML;
			data = data.replace(/(&lt;)/g,'<');
			data = data.replace(/(&gt;)/g,'>');
			parent.awd.launchSystem(data);
			parent.refreshDT();			
		}
	</script>
</head>
<body onload="process();">
<f:view>
	<h:outputText id="WorkDefinition" value="#{pc_Desktop.ids['WORK_XML']}"></h:outputText>

</f:view>
</body>
</html>
