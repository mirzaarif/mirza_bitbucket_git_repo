<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA243 		NB-1401   Replacement User Interface Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:outputText id="RTTableTitle" value="Replaced Contracts" styleClass="sectionSubheader" style="margin-left: -10px; margin-top:10px; width:628px;" />
	<!-- Table Column Headers -->
	<h:panelGroup id="covClient1TabHeader1" styleClass="ovDivTableHeader">
		<h:panelGrid columns="5" styleClass="ovTableHeader" columnClasses="ovColHdrText120,ovColHdrDate,ovColHdrAmt100,ovColHdrText150,ovColHdrText150"
			cellspacing="0">
			<h:commandLink id="RPLHdrCol00" value="#{property.replacementCol1}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol0}"
				actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="RPLHdrCol11" value="#{property.replacementCol2}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol1}"
				actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="RPLHdrCol22" value="#{property.replacementCol3}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol2}"
				actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="RPLHdrCol33" value="#{property.replacementCol4}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol3}"
				actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="RPLHdrCol44" value="#{property.replacementCol5}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol4}"
				actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="covClient1TabData2" styleClass="ovDivTableData" style="height:250px;">
		<h:dataTable id="coverageTable2" styleClass="ovTableData" cellspacing="0" cellpadding="0" rows="0" binding="#{pc_replacementTable.dataTable}"
			value="#{pc_replacementTable.replacementList}" var="replacement" rowClasses="#{pc_replacementTable.rowStyles}"
			columnClasses="ovColText120,ovColDate,ovColAmt100,ovColText150,ovColText150">

			<h:column id="covCol1">
				<h:outputText value="#{replacement.contractNumber}" styleClass="ovFullCellSelect">
				</h:outputText>
			</h:column>
			<h:column id="covCol2">
				<h:outputText value="#{replacement.issueDate}" styleClass="ovFullCellSelect">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:column>
			<h:column id="covCol3">
					<h:outputText value="#{replacement.faceAmount}" styleClass="ovFullCellSelectPrf">
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
				</h:outputText>				
			</h:column>
			<h:column id="covCol4">
				<h:outputText value="#{replacement.replacedCompany}" styleClass="ovFullCellSelect" style="width:140px;">
				</h:outputText>
			</h:column>
			<h:column id="covCol5">
				<h:outputText value="#{replacement.lineofBColumn}" styleClass="ovFullCellSelect">
				</h:outputText>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>