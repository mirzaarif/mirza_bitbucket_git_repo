<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA243 		NB-1401   Replacement User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA298         NB-1501   MEC Processing Enhancement -->
<!-- NBA300         NB-1501   nbA Foundation for Term Conversion -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Add Replacement contract</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>	
<script type="text/javascript" src="javascript/global/file.js"></script>

<script language="JavaScript" type="text/javascript">				
		var contextpath = '<%=path%>';	
		var fileLocationHRef  = '<%=basePath%>' + 'nbaReplacement/addReplacementContract.faces';		
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['formAddReplacement'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['formAddReplacement'].target='';
			return false;
		}	
		//function enable/dissable add button based on wether replacedComapny, lineOfBusiness and Amount are filled or not.
		function checkMinimunData(){
			var lineOfBusinessSelection = document.getElementById("formAddReplacement:LineOfBusinessSelection");
			var lineOfBusinessValue = lineOfBusinessSelection.options[lineOfBusinessSelection.selectedIndex].value;
			
			var replacedCompanySelection = document.getElementById("formAddReplacement:ReplacedCompanySelection");
			var replacedCompanyValue = replacedCompanySelection.options[replacedCompanySelection.selectedIndex].value;
			<!-- begin NBA300 -->
			var convertedAmountText = document.getElementById("formAddReplacement:ConvertedAmount");
			var convAmt=false;
			if( convertedAmountText!= null ) {
				var convertedAmountValue = convertedAmountText.value;
				convertedAmountValue=convertedAmountValue.replace(/,/g,"");
				if(convertedAmountValue.indexOf("$") > -1){
					var convertedAmount = convertedAmountValue.substr(1); // To remove the $ symbol
					if(convertedAmount > 0){
						convAmt= true;
					}
				} else if(convertedAmountValue > 0){
					convAmt= true;	
				}
			} else {
				convAmt= true;
			}
			if ( replacedCompanyValue != "-1" && lineOfBusinessValue != "-1" && convAmt){
			<!-- end NBA300 -->
				document.getElementById("formAddReplacement:buttonAddNew").disabled = false;
				document.getElementById("formAddReplacement:buttonAdd").disabled = false;
			}else{	
				document.getElementById("formAddReplacement:buttonAddNew").disabled = true;
				document.getElementById("formAddReplacement:buttonAdd").disabled = true;
			}
		}
</script>
</head>
<body onload="filePageInit();checkMinimunData();" style="overflow-x: hidden; overflow-y: scroll; margin-left: 3px;">
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_NEW_REPLACEMENT" value="#{pc_replacement}" />
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="formAddReplacement">
		<f:subview id="nbaReplacementTable3">
			<c:import url="subviews/NbaReplacementTable.jsp" />
		</f:subview>
		<h:panelGroup id="createNew1" styleClass="inputFormMat" style="width:628px;">
			<h:panelGroup styleClass="inputForm" style="height:250px;">
				<h:outputLabel id="CreateNewReplacedContracts1" value="#{property.createNewReplacedContract}" styleClass="formTitleBar" />
				<h:panelGrid columns="2">
					<h:column>
						<!--LEFT column-->
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="ContractNumber1" value="#{property.rplContractNumber}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.contractNumber}" id="Contract_Number1" styleClass="formEntryText" style="width:150px;" />
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="IssueDate1" value="#{property.rplIssueDate}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.issueDate}" id="Issue_Date1" styleClass="formEntryText" style="width:150px;">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:inputText>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="SurrenderValue1" value="#{property.rplSurrenderValue}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.surrenderValue}" id="Surrender_Value1" styleClass="formEntryText" style="width:150px;">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
							</h:inputText>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="PreTefraAmount1" value="#{property.rplPreTefraAmount}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.preTefraAmount}" id="Pre_TefraAmount1" styleClass="formEntryText" style="width:150px;">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
							</h:inputText>
						</h:panelGroup>
						<!-- begin NBA298 -->
						<h:panelGroup styleClass="formDataEntryLine" style= "padding-top:10px;" >
							<h:outputText id="MECStatus" value="#{property.rplMECStatus}" styleClass="formLabel" style="width:135px;" />
							<h:selectOneMenu id="MECStatusDD" value ="#{pc_replacement.mecStatus}" styleClass="formEntryText" style="width:150px;"> 
				              <f:selectItems value="#{pc_replacement.mecStatusList}" />
			                </h:selectOneMenu>
						</h:panelGroup>
						<!-- end NBA298 -->
					</h:column>
					<h:column>
						<!--RIGHT column-->
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="LineOfBusiness1" value="#{property.rplLineOfBusiness}" styleClass="formLabel" style="width:135px;" />
							<h:selectOneMenu value="#{pc_replacement.lineOfBusiness}" id="LineOfBusinessSelection" onchange="checkMinimunData()"
									styleClass="formEntryText" style="width:150px;">
								<f:selectItems value="#{pc_replacement.lineOfBusinessList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="FaceAmount1" value="#{property.rplFaceAmount}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.faceAmount}" id="Face_Amount1" styleClass="formEntryText" style="width:150px;">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
							</h:inputText>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="IncontestabilityDate1" value="#{property.rplIncontestabilityDate}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.incontestabilityDate}" id="incontestability_Date" styleClass="formEntryText" style="width:150px;">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:inputText>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="PostTefraAmount1" value="#{property.rplPostTefraAmount}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.postTefraAmount}" id="Post_TefraAmount1" styleClass="formEntryText" style="width:150px;">
								<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
							</h:inputText>
						</h:panelGroup>
						<!-- begin NBA298 -->
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="SevenPayStartDate" value="#{property.rplSevenPayStartDate}" styleClass="formLabel" style="width:135px;" />
							<h:inputText value="#{pc_replacement.sevenPayStartDate}" id="Seven_PayStartDate" styleClass="formEntryText" style="width:150px;">
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>
						</h:panelGroup>
						<!-- end NBA298 -->
					</h:column>
				</h:panelGrid>
				<h:panelGrid columns="1">
					<h:column>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="ReplacedCompany1" value="#{property.rplReplacedCompany}" styleClass="formLabel" style="width:135px;" />
							<h:selectOneMenu value="#{pc_replacement.replacedCompany}" id="ReplacedCompanySelection" onchange="checkMinimunData()"
									styleClass="formEntryText" style="width:457px;"> <!-- NBA298 -->
								<f:selectItems value="#{pc_replacement.replacedCompanyNameList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputText id="plan1" value="#{property.rplPlan}" styleClass="formLabel" style="width:135px;" />
							<h:selectOneMenu value="#{pc_replacement.plan}" styleClass="formEntryText" style="width:457px;"> <!-- NBA298 -->
								<f:selectItems value="#{pc_replacement.planList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<!-- begin NBA300 -->
						<h:panelGroup styleClass="formDataEntryLine">
							<h:selectBooleanCheckbox value="#{pc_replacement.termConversion}" styleClass="formEntryCheckbox" style="position: relative; left: 131px" disabled="#{pc_replacement.termConversionDisabled}" immediate="true" onclick="submit()" /> 
							<h:outputLabel value="#{property.rplTermConversion}" style="position: relative; left: 130px; margin-top: 3px" styleClass="formLabelRight"/> 
						</h:panelGroup>
						<!-- end NBA300 -->
					</h:column>
				</h:panelGrid>
				<!-- begin NBA300 -->
				<h:panelGroup style="height:250px;" rendered="#{pc_replacement.termConversion}">
				<f:verbatim>
					<hr class="formSeparator" style="text-align:center" />
				</f:verbatim>
				<h:panelGrid columns="2">
					<h:column>
						<!--LEFT column-->
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="ConversionType1" value="#{property.rplConversionType}" styleClass="formLabel" style="width:125px;" />
								<h:selectOneMenu value="#{pc_replacement.conversionType}" id="ConversionTypeSelection" onchange="checkMinimunData()"
										styleClass="formEntryText" style="width:160px;">
									<f:selectItems value="#{pc_replacement.conversionTypeList}" />
								</h:selectOneMenu>
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="Status1" value="#{property.rplStatus}" styleClass="formLabel" style="width:125px;" />
								<h:selectOneMenu value="#{pc_replacement.status}" id="StatusSelection" onchange="checkMinimunData()"
										styleClass="formEntryText" style="width:160px;">
									<f:selectItems value="#{pc_replacement.statusList}" />
								</h:selectOneMenu>
							</h:panelGroup>
					</h:column>
					<h:column>
							<h:panelGroup styleClass="formDataEntryLine" style="vertical-align:top;">
								<h:outputText id="ConvertedAmount1" value="#{property.rplConvAmount}" styleClass="formLabel" style="width:125px;" />
								<h:inputText value="#{pc_replacement.convAmount}" id="ConvertedAmount" styleClass="formEntryText" style="width:150px;" onkeyup="checkMinimunData();" onmouseout="checkMinimunData();">
									<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
								</h:inputText>
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="PlanSeries1" value="#{property.rplPlanSeries}" styleClass="formLabel" style="width:125px;" />
								<h:selectOneMenu value="#{pc_replacement.planSeries}" id="PlanSeriesSelection" onchange="checkMinimunData()"
										styleClass="formEntryText" style="width:150px;">
									<f:selectItems value="#{pc_replacement.planSeriesList}" />
								</h:selectOneMenu>
							</h:panelGroup>
					</h:column>
				</h:panelGrid>
				<h:panelGrid columns="1">
					<h:column>
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="RateClass1" value="#{property.rplRateClass}" styleClass="formLabel" style="width:125px;" />
								<h:selectOneMenu value="#{pc_replacement.rateClass}" id="RateClassSelection" onchange="checkMinimunData()"
										styleClass="formEntryText" style="width:160px;">
									<f:selectItems value="#{pc_replacement.rateClassList}" />
								</h:selectOneMenu>
							</h:panelGroup>
					</h:column>
				</h:panelGrid>
				<h:panelGrid columns="2">
					<h:column>
							<h:panelGroup styleClass="formDataEntryLine" >
								<h:outputText id="Riders1" value="#{property.rplRiders}" styleClass="sectionSubheader"  style="width:270px; margin-left:3px;" />
							</h:panelGroup>
							<h:panelGroup id="ridersTable_PGroup" styleClass="ovDivTableData6" style=" margin-left:13px; margin-top:-2px;  width: 270px; height: 95px;">
								<h:dataTable id="riders_DataTable" styleClass="ovTableData" cellspacing="0" binding="#{pc_ridersTable.dataTable}"
									value="#{pc_ridersTable.ridersList}" style="table-layout: auto; min-height: 5px; width: 267px;"
									var="ridersRow" columnClasses="ovColText250" rowClasses="#{pc_ridersTable.rowStyles}">
									<h:column>
										<h:commandLink id="ridersCol1_Link"  action="#{pc_ridersTable.selectForMultipleRows}" value="#{ridersRow.riderTrans}" styleClass="ovFullCellSelect">
										</h:commandLink>		
									</h:column>										
								</h:dataTable>
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px; margin-top:10px">
								<h:selectBooleanCheckbox value="#{pc_replacement.tableRating}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplTableRating}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.reqReduceRating}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel  styleClass="formLabelRight" value="#{property.rplReqReduceRating}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.incReqUnderwriting}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplIncReqUnderwriting}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.benAddReqUnderwriting}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplBenAddReqUnderwriting}" /> 
							</h:panelGroup>
					</h:column>
					<h:column>						
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="Benefits" value="#{property.rplBenefits}" styleClass="sectionSubheader" style="width:270px; margin-left:3px;" />
							</h:panelGroup>
							<h:panelGroup id="benefitsTable_PGroup" styleClass="ovDivTableData6" style=" margin-top:-2px; margin-left:13px; width: 270px; height: 95px;">
								<h:dataTable id="benefits_DataTable" styleClass="ovTableData" cellspacing="0" binding="#{pc_benefitsTable.dataTable}"
									value="#{pc_benefitsTable.benefitsList}" style="table-layout: auto; min-height: 5px; width: 250px;"
									var="benefitsRow" columnClasses="ovColText250" rowClasses="#{pc_benefitsTable.rowStyles}">
									<h:column>				
										<h:commandLink id="benefitsCol1_Link" styleClass="ovFullCellSelect"  action="#{pc_benefitsTable.selectForMultipleRows}" value="#{benefitsRow.benefitTrans}">
										</h:commandLink>		
									</h:column>										
								</h:dataTable>
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px; margin-top:10px">
								<h:selectBooleanCheckbox value="#{pc_replacement.flatExtra}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplFlatExtra}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.reinsured}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel  styleClass="formLabelRight" value="#{property.rplReinsured}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.ridAddReqUnderwriting}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplRidAddReqUnderwriting}" /> 
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine" style="margin-left:9px;">
								<h:selectBooleanCheckbox value="#{pc_replacement.remExclRider}" styleClass="formEntryCheckbox"/> 
								<h:outputLabel styleClass="formLabelRight" value="#{property.rplRemExclRider}" /> 
							</h:panelGroup>
					</h:column>
				</h:panelGrid>
				</h:panelGroup>
				<!-- end NBA300 -->
				<h:panelGroup styleClass="formButtonBar" style="margin-top:30px;">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft"
							action="#{pc_replacement.cancel}" onclick="resetTargetFrame();" immediate="true" />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1"
							action="#{pc_replacement.clear}" onclick="resetTargetFrame()" />
					<h:commandButton id="buttonAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1"
							action="#{pc_replacement.addAndNew}" onclick="resetTargetFrame()" />
					<h:commandButton id="buttonAdd" value="#{property.buttonAdd}" styleClass="formButtonRight"
							action="#{pc_replacement.add}" onclick="resetTargetFrame()" />
				</h:panelGroup>
				<f:subview id="nbaCommentBar1">
					<c:import url="/common/subviews/NbaCommentBar.jsp" />
				</f:subview>
			</h:panelGroup>
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>