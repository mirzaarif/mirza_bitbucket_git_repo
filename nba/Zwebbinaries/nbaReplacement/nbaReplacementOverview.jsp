<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA243			NB-1401   Replacement User Interface Rewrite-->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

	  
<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>nba replacement overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<link type="text/css" rel="stylesheet" href="theme/accelerator.css" />
<link type="text/css" rel="stylesheet" href="theme/nbaStyle.css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>	
<script type="text/javascript" src="javascript/global/file.js"></script>

<script language="JavaScript" type="text/javascript">	

		var contextpath = '<%=path%>'; 
		var fileLocationHRef  = '<%=basePath%>' + 'nbaReplacement/nbaReplacementOverview.faces';
		function setTargetFrame() {
				//alert('Setting Target Frame');
			document.forms['replacementForm'].target='controlFrame';		
			return false;
		}
		function resetTargetFrame() {
				//alert('Resetting Target Frame');
			document.forms['replacementForm'].target='';
			return false;
		}	
		function commitReplacement(){			
		var selectedValue = document.getElementById("replacementForm:rplType").value;
		var rowCount = document.getElementById('replacementForm:replacementTable').getElementsByTagName('tr').length;						
			//if replacementType is empty 
			if(rowCount > 0 && selectedValue == -1){
				setTargetFrame();				
			}else{
				resetTargetFrame();
			}
		}
	</script>
</head>

<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll; margin-left: 3px;">
<div id="blankDiv" style="height: 20px; overflow: auto;"></div>
<f:view>
	<PopulateBean:Load serviceName="RETRIEVE_ALL_REPLACEMENTS" value="#{pc_replacementTable}" />
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="replacementForm">

	<h:outputText styleClass="formLabel" value="#{property.replacementType}" style="width:130px; " />
	<h:selectOneMenu id="rplType" value="#{pc_replacementTable.replacementType}" style="width:493px;">
		<f:selectItems value="#{pc_replacementTable.replacementTypeList}" />
	</h:selectOneMenu>
	<h:outputText id="RTTableTitle" value="#{property.replacedContracts}" styleClass="sectionSubheader" style="margin-top:10px; width:628px;" />
		<!-- Table Column Headers -->
		<h:panelGroup id="covClient1TabHeader" styleClass="ovDivTableHeader">
			<h:panelGrid columns="5" styleClass="ovTableHeader" columnClasses="ovColHdrText120,ovColHdrDate,ovColHdrAmt100,ovColHdrText150,ovColHdrText150"
				cellspacing="0">
				<h:commandLink id="RPLHdrCol0" value="#{property.replacementCol1}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol0}"
					actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
				<h:commandLink id="RPLHdrCol1" value="#{property.replacementCol2}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol1}"
					actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
				<h:commandLink id="RPLHdrCol2" value="#{property.replacementCol3}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol2}"
					actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
				<h:commandLink id="RPLHdrCol3" value="#{property.replacementCol4}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol3}"
					actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
				<h:commandLink id="RPLHdrCol4" value="#{property.replacementCol5}" styleClass="ovColSorted#{pc_replacementTable.sortedByCol4}"
					actionListener="#{pc_replacementTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			</h:panelGrid>
		</h:panelGroup>
		<!-- Table Columns -->
		<h:panelGroup id="covClient1TabData" styleClass="ovDivTableData" style="height: 300px;">
			<h:dataTable id="replacementTable" styleClass="ovTableData" cellspacing="0" cellpadding="0" rows="0" binding="#{pc_replacementTable.dataTable}"
				value="#{pc_replacementTable.replacementList}" var="replacement" rowClasses="#{pc_replacementTable.rowStyles}"
				columnClasses="ovColText120,ovColDate,ovColAmt100,ovColText150,ovColText150">
				<!--NBA245 -->
				<h:column id="covCol1">
					<h:commandLink id="covCol1LINK" action="#{pc_replacementTable.selectSingleRow}" immediate="true" styleClass="ovFullCellSelect">
						<h:outputText id="covCol1LINKText" value="#{replacement.contractNumber}">
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column id="covCol2">
					<h:commandLink id="covCol2LINK" action="#{pc_replacementTable.selectSingleRow}" immediate="true" styleClass="ovFullCellSelectPrf">
						<h:outputText id="covCol2LINKText" value="#{replacement.issueDate}">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column id="covCol3">
					<h:commandLink id="covCol3LINK" action="#{pc_replacementTable.selectSingleRow}" immediate="true" styleClass="ovFullCellSelectPrf">
						<h:outputText id="covCol3LINKText" value="#{replacement.faceAmount}" >
							<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2" />
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column id="covCol4">
					<h:commandLink id="covCol4LINK" action="#{pc_replacementTable.selectSingleRow}" immediate="true" styleClass="ovFullCellSelectPrf">
						<h:outputText id="covCol4LINKText" value="#{replacement.replacedCompany}">
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column id="covCol5">
					<h:commandLink id="covCol5LINK" action="#{pc_replacementTable.selectSingleRow}" immediate="true" styleClass="ovFullCellSelectPrf">
						<h:outputText id="covCol5LINKText" value="#{replacement.lineofBColumn}">
						</h:outputText>
					</h:commandLink>
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		<h:panelGroup styleClass="ovButtonBar" style="width: 628px">
			<h:commandButton value="#{property.buttonDelete}" styleClass="ovButtonLeft" action="#{pc_replacementTable.deleteReplacement}"
				disabled="#{pc_replacementTable.updateDisabled}" onclick="setTargetFrame();"></h:commandButton>
			<h:commandButton value="#{property.buttonViewUpdate}" styleClass="ovButtonRight-1" action="View_Update_replacement_contract"
				disabled="#{pc_replacementTable.updateDisabled}"></h:commandButton>
			<h:commandButton value="#{property.buttonCreate}" styleClass="ovButtonRight" action="Create_replacement_contract"></h:commandButton>
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton value="#{property.buttonRefresh}" styleClass="tabButtonLeft" action="#{pc_replacementTable.refreshReplacement}"></h:commandButton>
			<h:commandButton value="#{property.buttonCommit}" styleClass="tabButtonRight" action="#{pc_replacementTable.commitReplacement}"
				disabled="#{pc_replacementTable.auth.enablement['Commit'] || pc_replacementTable.notLocked}" onclick="commitReplacement();"></h:commandButton>
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>