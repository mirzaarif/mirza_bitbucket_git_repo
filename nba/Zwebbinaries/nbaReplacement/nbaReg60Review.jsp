<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA231            	NB-1101	Replacement Processing -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%><!-- NBA171 -->
<%
    String path = request.getContextPath();
			String basePath = "";
			if (request.getServerPort() == 80) {
				basePath = request.getScheme() + "://"
						+ request.getServerName() + path + "/";
			} else {
				basePath = request.getScheme() + "://"
						+ request.getServerName() + ":"
						+ request.getServerPort() + path + "/";
			}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.reg60Title}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	<script language="JavaScript" type="text/javascript">
			<!--
				var width=420;
				var height=140; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_repl'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_repl'].target='';
					return false;
				}
			//-->
			</script>
	</head>
	<body onload="popupInit();">
	<PopulateBean:Load serviceName="RETRIEVE_REG60REVIEW" value="#{pc_reg60Review}" />
	<h:form id="form_repl">
		<h:panelGroup styleClass="formDataEntryLine">
			<h:outputLabel id="olreg60Review" value="#{property.reg60Review}" styleClass="formLabel" style="width: 180px;" />
			<h:selectOneMenu id="itreg60Review" value="#{pc_reg60Review.reg60Review}" styleClass="formEntryTextHalf" style="width: 210px;">
				<f:selectItems value="#{pc_reg60Review.reg60ReviewList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup styleClass="formDataEntryLine" rendered="#{pc_reg60Review.renderReg60PreSale}">
			<h:outputLabel id="olreg60PSDecision" value="#{property.reg60PSDecision}" styleClass="formLabel" style="width: 180px;" />
			<h:selectOneMenu id="itreg60PSDecision" value="#{pc_reg60Review.reg60PSDecision}" styleClass="formEntryTextHalf" style="width: 210px;">
				<f:selectItems value="#{pc_reg60Review.reg60PSDecisionList}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup styleClass="buttonBar">
			<h:commandButton id="rvCancel" value="#{property.buttonCancel}" action="#{pc_reg60Review.cancel}" onclick="setTargetFrame();" styleClass="buttonLeft" />
			<h:commandButton id="rvCommit" value="#{property.buttonCommit}" disabled="#{pc_reg60Review.disableCommit || pc_reg60Review.notLocked }" action="#{pc_reg60Review.commit}" onclick="setTargetFrame();" styleClass="buttonRight" />
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
	</body>
</f:view>
</html>
