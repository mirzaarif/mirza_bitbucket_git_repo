<!-- CHANGE LOG -->
<!-- Audit Number     Version         Change Description -->
<!--   NBA317         NB-1301         PCI Compliance For Credit Card Numbers Using Web Service -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = "";
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script language="JavaScript" type="text/javascript">
	function getTokenizationURL(){
		return document.getElementById('form_ccTokenizationInfo:tokenizationURL').value;
	}
	
	function getTokenizationRequest(){
		return document.getElementById('form_ccTokenizationInfo:tokenizationRequest').value;
	}
	
	function isSkipCCValidation(){
		return document.getElementById('form_ccTokenizationInfo:skipCCValidation').value;
	}	

</script>
</head>
<body>
<f:view>
	<h:form id="form_ccTokenizationInfo">
		<h:inputHidden id="tokenizationURL" value="#{pc_ccTokenizationInfo.tokenizationServiceURL}"></h:inputHidden>
		<h:inputHidden id="tokenizationRequest" value="#{pc_ccTokenizationInfo.tokenizationServiceRequest}"></h:inputHidden>
		<h:inputHidden id="skipCCValidation" value="#{pc_ccTokenizationInfo.skipCreditCardValidation}"></h:inputHidden>
	</h:form>
</f:view>
</body>
</html>
