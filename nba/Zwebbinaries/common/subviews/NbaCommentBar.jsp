<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA152            6      Comments Rewrite -->
<!-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213            7      Unified User Interface  -->
<!-- NBA225            8      nbA Comments -->
<!-- SPR3558            8      Blank Indexing View Returned  -->
<!-- FNB004       NB1101      PHI -->
<!-- NBA356       NB1501      Comments Floating View -->
<!-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View -->
<!-- SPRNBA-1005    NB-1601   May Receive Erroneous Confirmation For Uncommitted Comments -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> 
	<!--  NBA158 code deleted -->
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:panelGrid id="commentBarPanelGroup1" styleClass="commentBar" cellpadding="0" cellspacing="0" > <!-- NBA152 SPR3558-->
			<h:column id="commentBarColumn"> <!--  NBA152 SPR3558 -->
			<h:panelGroup id="commentBarPanelGroup2" styleClass="commentBar">	<!-- SPR3558 -->
				<h:commandButton id="launchComments" value="#{property.launchComment}" styleClass="commentBarLabelButton"
								style="width: 120px;position: absolute; left: 1%"  onclick="launchCommentView();top.hideWait();return false;" rendered="#{pc_commentsPopUpData.launchCommentsRendered}" /> <!-- NBA356 -->
				<!-- begin SPR3073  -->
				<h:commandButton id="lockClosed" image="images/comments/lock-closed-forbar.gif" title="#{property.addSecureComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('SECURE');top.hideWait();return false;" style="position: absolute; left: 27%"
								rendered="#{pc_commentsPopUpData.secureAddRendered}"/> <!-- NBA152, NBA225, FNB004 -->
				<h:commandButton id="lockOpen" image="images/comments/lock-open-forbar.gif" title="#{property.addGeneralComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('GENERAL');top.hideWait();return false;" style="position: absolute; left: 35%" /><!-- FNB004 -->
				<h:commandButton id="exclamation" image="images/comments/exclamation-forbar.gif" title="#{property.addSpecialInstruction}" styleClass="commentIcon"
								onclick="launchCommentPopUp('INSTRUCTION');top.hideWait();return false;" style="position: absolute; left: 43%" /><!-- FNB004 -->
				<h:commandButton id="PHI" image="images/comments/PHI-forbar.gif" title="#{property.addPHIComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('PHI');top.hideWait();return false;" style="position: absolute; left: 50%"  rendered="#{pc_commentsPopUpData.phiRendered}"/><!-- FNB004 -->
				<!-- begin FNB004 -->				
				<h:commandButton id="phone" image="images/comments/phone-forbar.gif" title="#{property.addPhoneRecord}" styleClass="commentIcon"
									onclick="launchCommentPopUp('Phone');top.hideWait();return false;" style="position: absolute; left: #{pc_commentsPopUpData.posPhoneIcon}%" />
				<h:commandButton id="envelope" image="images/comments/envelope-forbar.gif" title="#{property.sendAnEmail}" styleClass="commentIcon"
								onclick="launchCommentPopUp('Email');top.hideWait();return false;" style="position: absolute; left: #{pc_commentsPopUpData.posEmailIcon}%" />
 				<!-- end FNB004 -->
				<h:commandButton id="commit" value="#{property.buttonBarCommit}" title="#{property.commitComment}" action="#{pc_commentsPopUpData.commitComments}" 
								onclick="setTargetFrame();resetDraftCommentStatus();" styleClass="commentBarLabelButton" style="right: 129px;" 
								disabled="#{pc_commentsPopUpData.auth.enablement['Commit'] || 
											pc_commentsPopUpData.notLocked}"
								rendered="#{pc_commentsPopUpData.commitRendered}"/> <!-- NBA152, NBA213, SPRNBA-306 SPRNBA-1005 -->
				<h:commandButton id="defaultLabel" value="#{property.addComment}" rendered="#{pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								style="width: 100px;"  onclick="launchCommentPopUp('Secure'); top.hideWait();return false;" /> <!-- NBA152 -->
				<h:commandButton id="defaultLabe2" value="#{property.addComment}" rendered="#{!pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								style="width: 100px;"  onclick="launchCommentPopUp('General'); top.hideWait();return false;" /> <!-- NBA152 -->
				<!--  end SPR3073 -->
		</h:panelGroup>
		</h:column> <!--NBA152 -->
	</h:panelGrid> <!-- NBA152 -->
</jsp:root>
