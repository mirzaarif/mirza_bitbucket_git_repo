<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA180 		   7	  Contract Copy Rewrite -->


<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

		<h:panelGroup id="valMsgHeader" styleClass="formDivTableHeader">
			<h:panelGrid columns="4" styleClass="formTableHeader" cellspacing="0" columnClasses="ovColHdrIcon,ovColHdrText125,ovColHdrText300,ovColHdrText120">
				<h:commandLink id="messageHdrCol1" actionListener="#{pc_contractMessages.sortColumn}" styleClass="ovColSorted#{pc_contractMessages.sortedByCol1}" />
				<h:commandLink id="messageHdrCol2" value="#{property.valMsgsCol2}" actionListener="#{pc_contractMessages.sortColumn}" styleClass="ovColSorted#{pc_contractMessages.sortedByCol2}" />
				<h:commandLink id="messageHdrCol3" value="#{property.valMsgsCol3}" actionListener="#{pc_contractMessages.sortColumn}" styleClass="ovColSorted#{pc_contractMessages.sortedByCol3}" />
				<h:commandLink id="messageHdrCol4" value="#{property.valMsgsCol4}" actionListener="#{pc_contractMessages.sortColumn}" styleClass="ovColSorted#{pc_contractMessages.sortedByCol4}" />
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup id="contractData" styleClass="formDivTableData12" style="height: 450px" >
			<h:dataTable id="contractTable" styleClass="formTableData" cellspacing="0" binding="#{pc_contractMessages.dataTable}" value="#{pc_contractMessages.msgList}" var="message"
					rowClasses="#{pc_contractMessages.rowStyles}" columnClasses="ovColIconTop,ovColText125,ovColText300,ovColText120">
				<h:column>
					<h:commandButton id="messageCol1a" image="images/needs_attention/flag-onwhite.gif" rendered="#{message.severeInd}" styleClass="ovViewIconTrue" action="#{pc_contractMessages.selectRow}" immediate="true" />
					<h:commandButton id="messageCol1b" image="images/needs_attention/filledcircle-onwhite.gif" rendered="#{message.overriddenInd}" styleClass="ovViewIconTrue" action="#{pc_contractMessages.selectRow}" immediate="true" />
					<h:commandButton id="messageCol1d" image="images/needs_attention/yield.gif" rendered="#{message.overridableInd}" styleClass="ovViewIconTrue" action="#{pc_contractMessages.selectRow}" immediate="true" />
					<h:commandButton id="messageCol1c" image="images/needs_attention/clear.gif" styleClass="ovViewIconFalse" action="#{pc_contractMessages.selectRow}" immediate="true" />
				</h:column>
				<h:column>
					<h:commandLink value="#{message.valType}" styleClass="ovFullCellSelectPrf" action="#{pc_contractMessages.selectRow}" immediate="true" />
				</h:column>
				<h:column>
					<h:commandLink value="#{message.message}" style= "color: #000000;" action="#{pc_contractMessages.selectRow}" immediate="true" />
				</h:column>
				<h:column>
					<h:selectBooleanCheckbox value="#{message.autoDelete}" rendered="#{message.autoDelete}" disabled="true"
									styleClass="ovFullCellSelectCheckBox" immediate="true" />
					<h:commandButton id="messageCol4b" image="images/needs_attention/clear.gif" styleClass="ovViewIconFalse"
									action="#{pc_contractMessages.selectRow}" immediate="true" />
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		<h:panelGroup styleClass="ovStatusBar" style="margin-left: 10px; width: 595px; margin-bottom: 15px">
			<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_contractMessages.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.previousPageAbsolute}" immediate="true" />
			<h:commandLink value="#{property.previousPage}" rendered="#{pc_contractMessages.showPrevious}" styleClass="ovStatusBarText" 
						action="#{pc_contractMessages.previousPage}" immediate="true" />
			<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_contractMessages.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.previousPageSet}" immediate="true" />
			<h:commandLink value="#{pc_contractMessages.page1Number}" rendered="#{pc_contractMessages.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page1}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page1Number}" rendered="#{pc_contractMessages.currentPage1}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page2Number}" rendered="#{pc_contractMessages.showPage2}" styleClass="ovStatusBarText" 
						action="#{pc_contractMessages.page2}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page2Number}" rendered="#{pc_contractMessages.currentPage2}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page3Number}" rendered="#{pc_contractMessages.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page3}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page3Number}" rendered="#{pc_contractMessages.currentPage3}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page4Number}" rendered="#{pc_contractMessages.showPage4}" styleClass="ovStatusBarText"
			 			action="#{pc_contractMessages.page4}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page4Number}" rendered="#{pc_contractMessages.currentPage4}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page5Number}" rendered="#{pc_contractMessages.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page5}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page5Number}" rendered="#{pc_contractMessages.currentPage5}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page6Number}" rendered="#{pc_contractMessages.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page6}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page6Number}" rendered="#{pc_contractMessages.currentPage6}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page7Number}" rendered="#{pc_contractMessages.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page7}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page7Number}" rendered="#{pc_contractMessages.currentPage7}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{pc_contractMessages.page8Number}" rendered="#{pc_contractMessages.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.page8}" immediate="true" />
			<h:outputText value="#{pc_contractMessages.page8Number}" rendered="#{pc_contractMessages.currentPage8}" styleClass="ovStatusBarTextBold" />
			<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_contractMessages.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.nextPageSet}" immediate="true" />
			<h:commandLink value="#{property.nextPage}" rendered="#{pc_contractMessages.showNext}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.nextPage}" immediate="true" />
			<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_contractMessages.showNext}" styleClass="ovStatusBarText"
						action="#{pc_contractMessages.nextPageAbsolute}" immediate="true" />
		</h:panelGroup>
</jsp:root>
