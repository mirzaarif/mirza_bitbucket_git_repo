<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-446		NB-1101   Companion Case Refresh Issue After Commit -->
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if(request.getServerPort() == 80){
	basePath = request.getScheme()+"://"+request.getServerName() + path+"/";
} else {
	basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path+"/";
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Refresh Business Function</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		top.mainContentFrame.contentRightFrame.file.location.href = top.mainContentFrame.contentRightFrame.file.location.href;
	</script>
</head>

<body>
Refresh Business Function
	<f:view>
		<div id="Messages">	
			<h:messages />
		</div>
	</f:view>
	<script type="text/javascript">
			try{
	   		    var innerText=document.all["Messages"].innerHTML;
			    var message=top.getInnerHTML(innerText);	
			    document.all["Messages"].innerHTML= message;    
				if(document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != ""){
					parent.showWindow('<%=path%>','faces/error.jsp', this);
				}
			} catch(err){
			}
	</script>
</body>
</html>
