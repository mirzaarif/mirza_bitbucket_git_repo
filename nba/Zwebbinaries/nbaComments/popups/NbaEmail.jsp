<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- NBA171            6      nbA Linux re-certification project --%>
<%-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA --%>
<%-- FNB011 				NB-1101	Work Tracking --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- NBA337         NB-1401   Email Enhancement & support email attachment --%>
<%-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View --%>
<%-- NBA406         NB-1601   Rich Text Ad-Hoc Underwriter Emails Business Requirement --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %> <%-- NBA171 --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbaimages.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<%-- SPRNBA-306 code deleted --%>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script language="JavaScript" type="text/javascript">
	
				
	var contextpath = '<%=path%>'; //FNB011
	var pathref = top.opener; //NBA337 To save current top.opener value. 
	var width = 650;
	var height = 350;
	var DELETE_KEY = 46; //NBA337 keycode for Delete key

	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['form_commentsEmail'].target = 'controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['form_commentsEmail'].target = '';
		return false;
	}
	// NBA337 Reset the top.opener value
	function resetopener() {
		top.opener = pathref; //NBA337
	}

	// NBA337 called from attachmentUploader.jsp after upload process
	function onFileUploaderComplete() {
		document.getElementById('form_commentsEmail:refreshAttachment_Button').value = "true";
	}

	// NBA337 Refreshes the Attachment data table
	function onUploadComplete() {
		if (document
				.getElementById('form_commentsEmail:refreshAttachment_Button').value == "true") {
			document.getElementById(
					"form_commentsEmail:refreshAttachment_Button").click();
		}
	}

	// NBA337 To resize the window and set the focus to Attachments data table.
	function resizeWindow() {
		if (document.getElementById('form_commentsEmail:resizePopup').value == "true") {
			top.resizeTo(650, 950); //Resizes to render the attachemnt table pane.//NBA406
		} else {
			top.resizeTo(650, 800); //NBA406

		}
		if (document.getElementById('form_commentsEmail:attachments_DataTable') != null) {
			document.getElementById('form_commentsEmail:attachments_DataTable')
					.focus();
		}
	}

	// NBA337 Removes the selected row on press of Delete key
	function removeSelected() {
		if (event.keyCode == DELETE_KEY) {
			document.getElementById("form_commentsEmail:removeSelected_Button")
					.click();
		}
	}

	//NBA337 calling the onload functions
	function initialize() {
		filePageInit();
		if (document.getElementById('refreshParent').value == 'true') { //SPRNBA-306
			top.refreshParent(); //SPRNBA-306
		} //SPRNBA-306
		resizeWindow();
		if (document.getElementById("form_commentsEmail:richTextEditor") !== null) { //NBA406
            createIframe(); //NBA406
        } //NBA406
	}

	//NBA406 doesn't block the load event
	function createIframe() {
		var i = document.createElement("iframe");
		i.src = "faces/nbaEditor/emailEditor.jsp";
		i.scrolling = "NO";
		i.frameborder = "0";
		i.width = "100%";
		i.height = "415px";
		document.getElementById("form_commentsEmail:richTextEditor").appendChild(i);
	};
</script>

</head>
<body onload="initialize();" onbeforeunload="resetopener();">
	<%-- SPR3073 NBA337 --%>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_commentsEmail">
			<%-- begin NBA337 --%>
			<h:panelGroup
				style="background-color: white;left: 400; width: 100%; height: 175px;">
				<h:panelGroup styleClass="formDataEntryLine" style="margin-top: 10px;">
					<h:outputLabel value="#{property.emailTo}" styleClass="entryLabelTop" style="width: 70px;height: 25px" />
					<h:inputText value="#{pc_commentsPopUpData.to}" style="width: 490;" />
					<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle"
						disabled="#{pc_commentsPopUpData.iconDisabled}" action="#{pc_commentsPopUpData.toList}" onclick="setTargetFrame(); " />
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.emailFrom}" styleClass="entryLabelTop" style="width: 70px;height: 25px" />
					<h:inputText value="#{pc_commentsPopUpData.from}" style="width: 490;" />
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.emailCC}" styleClass="entryLabelTop" style="width: 70px;height: 25px" />
					<h:inputText id="targetCC" value="#{pc_commentsPopUpData.CC}" style="width: 490;" />
					<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle"
						disabled="#{pc_commentsPopUpData.iconDisabled}" action="#{pc_commentsPopUpData.cCList}" onclick="setTargetFrame();" />
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.emailBCC}" styleClass="entryLabelTop" style="width: 70px;height: 25px" />
					<h:inputText value="#{pc_commentsPopUpData.BCC}" style="width: 490;" />
					<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle"
						disabled="#{pc_commentsPopUpData.iconDisabled}" action="#{pc_commentsPopUpData.bCCList}" onclick="setTargetFrame();" />
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.emailSubject}" styleClass="entryLabelTop" style="width: 70px;height: 25px" />
					<h:inputText value="#{pc_commentsPopUpData.subject}" style="width: 490;" />
				</h:panelGroup>
			</h:panelGroup>
			<h:panelGroup id="richTextEditor" rendered="#{pc_commentsPopUpData.enableRichEditor}">
				<%--		iframe would be dynamically loaded    --%>
				<h:inputHidden id="editorValue" value="#{pc_commentsPopUpData.textEmail}" />
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="background-color: white;left: 400; width: 100%; height: 275px;" rendered="#{!pc_commentsPopUpData.enableRichEditor}">
				<h:outputLabel value="#{property.impMessage}" styleClass="entryLabelTop" style="margin-top:10px;width:70px" />
				<h:inputTextarea value="#{pc_commentsPopUpData.textEmail}" rows="10" styleClass="formEntryTextMultiLineFull"
				style="background-image: url('images/comments/envelope-watermark.gif');background-repeat: no-repeat;background-position: center;height:240px;" />
			</h:panelGroup>
			<iframe id="emailAttachFile" value="Attach" src="faces/nbaComments/popups/attachmentUploader.jsp" style="margin-top: -4px" name="uploaderFrame"
				onload="onUploadComplete();" width="100%" frameborder="0" marginheight="0" marginwidth="0" scrolling="NO" height="35 px"></iframe>
			<h:panelGroup style="vertical-align: top;background-color: white;left: 400; width: 100%; height: 55px;" rendered="#{pc_commentsPopUpData.attachmentsRendered}">
				<f:verbatim>
					<hr class="formSeparator" />
				</f:verbatim>
				<h:panelGroup styleClass="formDataEntryLine" style="margin-top:-8px">
					<h:outputLabel value="#{property.attachments}" styleClass="entryLabelTop" />
					<h:panelGroup id="attachmentsTable_PGroup" styleClass="formDivTableDataRightButtons3"
						style="margin-left:0px; margin-top:5px; width: 280px; height: 95px; background-color: #FFFFFF"> <%-- NBA406 --%>
						<h:dataTable id="attachments_DataTable" onkeydown="removeSelected();"
							styleClass="formTableDataRightButtons" cellspacing="0" binding="#{pc_attachmentsTable.dataTable}"
							value="#{pc_attachmentsTable.attachmentsList}" style="table-layout: auto; min-height: 5px; width: 283px;"
							var="attachmentsRow" columnClasses="ovColText350" rowClasses="#{pc_attachmentsTable.rowStyles}">
							<h:column>
								<h:commandLink id="attachmentsCol1_Link" action="#{pc_attachmentsTable.selectForMultipleRows}" value="#{attachmentsRow.fileName}">
								</h:commandLink>
							</h:column>
						</h:dataTable>
					</h:panelGroup>
					<h:commandButton id="removeAll_Button" value="#{property.buttonRemoveAll}" action="#{pc_commentsPopUpData.removeAll}" onclick="setTargetFrame();" styleClass="buttonRight-1" style="position:relative;left:10px;"/> <%-- NBA406 --%>
				</h:panelGroup>
				<h:panelGroup style="margin-left:385px;">
					<h:outputLabel value="#{property.totalSize}" styleClass="entryLabelTop" style="width:80px; margin-left: 25px; margin-top: -20px" />
					<h:outputLabel value="#{pc_commentsPopUpData.totalSize}" styleClass="entryLabelTop" style="width:80px; margin-left: -25px; margin-top: -20px" />
				</h:panelGroup>
			</h:panelGroup>
			<h:panelGroup styleClass="buttonBar" style="width: 100%px; height: 85px; background-color: white;">
				<%-- NBA406 --%>
				<h:commandButton id="cb1" action="#{pc_commentsPopUpData.cancel}" value="#{property.buttonCancel}" styleClass="buttonLeft" />
				<h:commandButton id="cb2" value="#{property.buttonClear}" action="#{pc_commentsPopUpData.clear}" styleClass="buttonLeft-1" />
				<h:commandButton id="cb3" value="#{property.buttonAddNew}" action="#{pc_commentsPopUpData.addAndNewEmailComment}" rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight-1" style="position:relative;left:400px;"/> <%-- NBA406 --%>
				<h:commandButton id="cb4" value="#{property.buttonAdd}" action="#{pc_commentsPopUpData.addAndCloseEmailComment}" rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight" style="position:relative;left:408px;" onclick="setTargetFrame();" /> <%-- NBA406 --%>
				<%-- SPRNBA-306 --%>
				<h:commandButton id="cb5" value="#{property.buttonUpdate}" action="#{pc_commentsPopUpData.updateEmailComment}" rendered="#{pc_commentsPopUpData.updateMode}" styleClass="buttonRight" style="position:relative;left:490px;"/> <%-- NBA406 --%>
			</h:panelGroup>
			<div style="display: none">
				<h:commandButton id="refreshAttachment_Button" action="#{pc_attachmentsTable.processUpload}" value="#{pc_commentsPopUpData.refreshAttachment}" />
				<h:inputHidden id="resizePopup" value="#{pc_commentsPopUpData.attachmentsRendered}" />
				<h:commandButton id="removeSelected_Button" action="#{pc_attachmentsTable.removeSelected}" disabled="#{pc_attachmentsTable.removeDisabled}" />
			</div>
		</h:form>
		<div id="Messages" style="display: none">
			<h:messages />
		</div>
		<h:inputHidden id="refreshParent" value="#{pc_commentsPopUpData.refreshParent}" />
		<%-- SPR3073--%>
	</f:view>
</body>
</html>
