<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- NBA152			   6 	  Comments Rewrite--%>
<%-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA --%>
<%-- SPR3456 		   8	  Error Message Popup for Missing or Invalid is Not Visible When Special Instruction Type is Not Selected --%>
<%-- SPR3674 		   8	  Date of Birth Format Incorrect on Comment Pop Up --%>
<%-- NBA337         NB-1401   Email Enhancement & support email attachment --%>
<%-- NBA406        NB-1601   Rich Text Ad-Hoc Underwriter Emails Business Requirement --%>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>COMMENTS / PHONE RECORD / EMAIL </title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<%-- SPR3456 code deleted --%>
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>		
	<%-- SPR3456 code deleted --%>
	<script type="text/javascript">
		var width=650;
		var height=350;
		<!--
			var context = '<%=path%>';
			var topOffset = -22;  //[TODO] need audit number, change required due to iteration_1a merge
			var parentWindow = window.opener; //SPR3073
		//-->
		basePath = '<%=basePath%>'; //SPR3073
		
		//NBA406 
		function initFileSize() {
			tabHeight = window.screen.availHeight - 100;  // To resize tab height based on available screen size
			document.getElementById('file').height = tabHeight;
		}
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body><%-- SPR3073, SPR3456 --%>
	<f:view>
		<DIV id="contentArea"> <%--SPR3456 --%>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>		
		<PopulateBean:Load serviceName="RETRIEVE_COMMENTS_POPUP" value="#{pc_commentsPopUpData}" /> <%--SPR3073 --%>
		<h:panelGroup styleClass="summaryHeader">
			<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
			<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
			<h:outputText value="#{property.birthDate}" styleClass="shText" style="position: absolute; left: 422px" />
			<h:outputText value="#{pc_contractStatus.insuredBirthDate}" styleClass="shText" style="position: absolute; left: 462px" > <%--NBA152 SPR3674 --%>
				<f:convertDateTime pattern="#{property.datePattern}"/> <%-- SPR3674--%>
			</h:outputText> <%--SPR3674 --%>
			<h:outputFormat value="{0} {1}" styleClass="shText" style="position: absolute; right: 15px">
				<f:param value="#{property.age}" />
				<f:param value="#{pc_contractStatus.insuredAge}" /> <%--NBA152 --%>
			</h:outputFormat>

		</h:panelGroup>
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:16px; top: 50px; position: absolute;left: 10px;">
					<td><FileLoader:Files location="/nbaComments/popups/" configFileName="fileConfigCommentsTabs.properties" numTabsPerRow="6" defaultIndex="#{pc_commentsPopUpData.defaultIndex}"/> <%-- SPR3456 --%>
						<FileLoader:DisableTab disableIndex="1" disableValue="#{pc_commentsPopUpData.disableTabNotes}"/>
						<FileLoader:DisableTab disableIndex="2" disableValue="#{pc_commentsPopUpData.disableTabPhone}"/>
						<FileLoader:DisableTab disableIndex="3" disableValue="#{pc_commentsPopUpData.disableTabEmail}"/>						
					</td>
				</tr>
				<tr style="height:*; top: 81px; position: absolute;left: 10px;"> <%--NBA337 NBA406 --%>
					<td><iframe id="file" src="" height="750px" width="100%" frameborder="0" scrolling="auto" onload="initFileSize();"></iframe></td> <%-- NBA406 --%>					
				</tr>
			</tbody>
		</table>
		</DIV> <%-- SPR3456 --%>
	</f:view>
</body>
</html>