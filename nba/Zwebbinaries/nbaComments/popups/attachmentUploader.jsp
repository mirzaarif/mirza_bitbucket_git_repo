<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA337         NB-1401   Email Enhancement & support email attachment --%>
<%-- NBA406         NB-1601   Rich Text Ad-Hoc Underwriter Emails Business Requirement --%>

<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Attachment Uploader</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function setTargetFrame() {
		document.forms['FileUploader'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		document.forms['FileUploader'].target='';
		return false;
	}
	function onComplete() {
		if(typeof(parent.onFileUploaderComplete) === "function") {
			parent.onFileUploaderComplete();
		}	
	}
	function setFileMessage(msg1) {
		document.getElementById('processing_message').innerHTML = msg1;
		}
	
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" leftmargin="0" rightmargin="0" topmargin="0" >
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<form name="FileUploader" action="attachmentUploader" enctype="multipart/form-data" method="post">			
			<table cellpadding="0" align="left" cellspacing="0" style="vertical-align: top; background-color:white; " width="100%" height="35px" border="0">
				<tr>
			    	<td width="15%" align="left">
						<h:outputText id="lblEmailAttachFile" styleClass="entryLabelTop" style="margin-top:10px;width:85px;" value="#{property.attachFile}"></h:outputText> <%-- NBA406 --%>
					</td>
					<td width="10%" align="left" valign="top">
						<input type="file" multiple name="source" style="width: 0px; margin-top:13px; margin-left:-4px;" class="fileUploaderClass" 
						onchange="top.showTimedWait('',3000); setFileMessage('processing request');resetTargetFrame(); this.form.submit(); onComplete();" multiple />
					</td>
					<td align="left">
						<span id="processing_message" style="margin-left:10px; font: bold 10px 'Verdana'; color: navy; text-decoration: none;"></span>
					</td>
				</tr>
			</table>
		</form>
	</f:view>
</body>
</html>