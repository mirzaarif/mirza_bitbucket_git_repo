<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- NBA152            6      Comments Rewrite --%>
<%-- NBA171            6      nbA Linux re-certification project --%>
<%-- SPR3073           6      The add comments dialog under the upgraded (JSF) look & feel should not cover nbA --%>
<%-- NBA225            8      nbA Comments --%>
<%-- FNB011 	 NB-1101	  Work Tracking --%>
<%-- FNB004 	 NB-1101 	  PHI --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- NBA341         NB-1401   Internet Explorer 11 Certification --%>
<%-- NBA337         NB-1401   Email Enhancement & support email attachment --%>
<%-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View --%>
<%-- SPRNBA-1023    NB-1601   Comment Pop Up Height Not Resized Once Expanded for Attachments on Email Tab --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><%-- NBA171 --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<%-- SPRNBA-306 code deleted --%>	
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script> 
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- SPRNBA-1023 --%>			
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var width=650;
		var height=350;		
		function setTargetFrame() {
			//alert('calling set target frame form_commentsInstructions');
			document.forms['form_commentsInstructions'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('calling reset target frame');
			document.forms['form_commentsInstructions'].target='';
			return false;
		}
		
		//NBA337 calling the onload functions
		function initialize() {
			filePageInit();
			if (document.getElementById('refreshParent').value == 'true') {  //SPRNBA-306
				top.refreshParent();  //SPRNBA-306
			}  //SPRNBA-306
			resizeCommentPopUp();
		}
	</script>
</head>
<body onload="initialize();"> <%-- SPR3073 NBA337 --%>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:form id="form_commentsInstructions">
			<h:panelGroup id="PGroup1" style="background-color: white;left: 400; width: 100%; height: 255" >
				<h:panelGroup id="PGroup2" styleClass="formDataEntryLine" >
				<%-- begin FNB004 --%>
					<h:panelGrid id="pgrid1" columns="3" style="top: 200px;height: 45px;">

						<h:column>
							<h:outputLabel id="OL1" value="#{property.instructionType}" styleClass="entryLabelTop" style="width: 120px;" />
						</h:column>

						<h:column>
							<h:selectOneMenu id="commentTypeSelect" value="#{pc_commentsPopUpData.commentType}" styleClass="formEntryTextFull" style="width: 150px;"
								valueChangeListener="#{pc_commentsPopUpData.valueChangeRadio}">
								<f:selectItems value="#{pc_commentsPopUpData.commentTypes}" />					
							</h:selectOneMenu>
						</h:column>
						<h:column>
							<h:selectOneMenu id="partyID" value="#{pc_commentsPopUpData.partyId}" styleClass="formEntryTextHalf" style="width: 190px;" rendered="#{!pc_commentsPopUpData.disableSecureDD}"> <%-- NBA341 --%>
								<f:selectItems value="#{pc_commentsPopUpData.insuredClientsForCommentsPopup}" />
							</h:selectOneMenu>
							<h:selectOneMenu id="LBox1" value="#{pc_commentsPopUpData.specialInstructionType}" styleClass="formEntryTextHalf" style="width: 190px;" rendered="#{!pc_commentsPopUpData.disableSpecialDD}"> <%-- NBA341 --%>
								<f:selectItems value="#{pc_commentsPopUpData.instructionTypes}" />
							</h:selectOneMenu>
						</h:column>
					</h:panelGrid>
					<h:panelGrid id="pgrid2" columns="2" border="0" width="500px" style="top: 250px">
						<h:column>
							<h:outputLabel id="OL6" value="#{property.impMessage}" styleClass="entryLabelTop" />
						</h:column>
						
						<h:column>
							<h:inputTextarea id="ITArea1" value="#{pc_commentsPopUpData.textGeneral}" rows="10" styleClass="formEntryTextMultiLineFull" style="background-image: url('images/comments/CommentsInstructions#{pc_commentsPopUpData.commInstrBackgroundName}-watermark.gif');background-repeat: no-repeat;background-position: center;width: 465px;"/> <%-- SPRNBA-1023 --%>
						</h:column>
					</h:panelGrid>
				<%-- end FNB004 --%>	
				</h:panelGroup>
				<h:panelGroup id="PGroup5" styleClass="buttonBar" style="position:absolute; top: 200px;left: 0px;"> <%-- SPRNBA-1023 --%>
					<%-- begin SPR3073 --%>
					<h:commandButton id="cb1" value="#{property.buttonCancel}" action="#{pc_commentsPopUpData.cancel}" styleClass="buttonLeft" />
					<h:commandButton id="cb2" value="#{property.buttonClear}" action="#{pc_commentsPopUpData.clear}"  styleClass="buttonLeft-1" />
					<h:commandButton id="cb3" value="#{property.buttonAddNew}"  action="#{pc_commentsPopUpData.addAndNewGeneralComment}"  rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight-1" />
					<h:commandButton id="cb4" value="#{property.buttonAdd}"  action="#{pc_commentsPopUpData.addAndCloseGeneralComment}"   rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight" onclick="setTargetFrame();"/>  <%-- SPRNBA-306 --%>
					<h:commandButton id="cb5" value="#{property.buttonUpdate}" action="#{pc_commentsPopUpData.updateGeneralComment}" rendered="#{pc_commentsPopUpData.updateMode}" styleClass="buttonRight" />
					<%-- end SPR3073 --%>
				</h:panelGroup>
			</h:panelGroup>
		</h:form>	
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
		<h:inputHidden id="refreshParent" value="#{pc_commentsPopUpData.refreshParent}" />  <%-- SPR3073--%>
	</f:view>
</body>
</html>
