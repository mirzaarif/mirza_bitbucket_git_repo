<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- NBA171            6      nbA Linux re-certification project --%>
<%-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA --%>
<%-- FNB011 				NB-1101	Work Tracking --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- NBA337         NB-1401   Email Enhancement & support email attachment --%>
<%-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View --%>
<%-- SPRNBA-1023    NB-1601   Comment Pop Up Height Not Resized Once Expanded for Attachments on Email Tab --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><%-- NBA171 --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<%-- SPRNBA-306 code deleted --%>	
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- SPRNBA-1023 --%>	
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var width=650;
		var height=350;
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_PhoneComments'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_PhoneComments'].target='';
			return false;
		}
		
		//NBA337 calling the onload functions
		function initialize() {
			filePageInit();
			if (document.getElementById('refreshParent').value == 'true') {  //SPRNBA-306
				top.refreshParent();  //SPRNBA-306
			}  //SPRNBA-306
			resizeCommentPopUp();
		}
	</script>
</head>
<body onload="initialize();"> <%-- SPR3073 NBA337 --%>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:form id="form_PhoneComments">
			<h:panelGroup id="pgroup1" style="background-color: white;left: 400; width: 100%; height: 255" >
				<h:panelGrid id="pgrid1" columns="4" border="0" width="500px" style="top: 200px;height: 45px;">
					<h:column>
						<h:outputLabel value="#{property.phoneContact}" styleClass="entryLabelTop" style="width: 114px;" />
					</h:column>
					<h:column>
						<h:inputText value="#{pc_commentsPopUpData.contact}" style="width: 160;" />
					</h:column>
					<h:column>
						<h:outputLabel value="#{property.phoneRelationship}" styleClass="entryLabelTop" style="width: 108px;" />
					</h:column>
					<h:column>
						<h:selectOneMenu id="onemenu1" value="#{pc_commentsPopUpData.relationship}" styleClass="formEntryTextFull" style="width: 175px">
							<f:selectItems value="#{pc_commentsPopUpData.phoneRelationships}" />
						</h:selectOneMenu>
					</h:column>
				</h:panelGrid>
				
				<h:panelGroup id="pgroup2" styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.impMessage}" styleClass="entryLabelTop" style="padding-top:60px;"/> <%-- SPRNBA-1023 --%>
					<h:inputTextarea value="#{pc_commentsPopUpData.textPhone}" rows="10" styleClass="formEntryTextMultiLineFull" style="background-image: url('images/comments/phone-watermark.gif');background-repeat: no-repeat;background-position: center;"/>
				</h:panelGroup>
		
				<h:panelGroup id="pgroup3" styleClass="buttonBar" >
					<%-- begin SPR3073 --%>
					<h:commandButton id="cb1" value="#{property.buttonCancel}" action="#{pc_commentsPopUpData.cancel}" styleClass="buttonLeft" />
					<h:commandButton id="cb2" value="#{property.buttonClear}" action="#{pc_commentsPopUpData.clear}" styleClass="buttonLeft-1" />
					<h:commandButton id="cb3" value="#{property.buttonAddNew}" action="#{pc_commentsPopUpData.addAndNewPhoneComment}" onclick="resetTargetFrame();" rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight-1" />
					<h:commandButton id="cb4" value="#{property.buttonAdd}" action="#{pc_commentsPopUpData.addAndClosePhoneComment}" rendered="#{!pc_commentsPopUpData.updateMode}" styleClass="buttonRight"  onclick="setTargetFrame();"/>  <%-- SPRNBA-306 --%>
					<h:commandButton id="cb5" value="#{property.buttonUpdate}" action="#{pc_commentsPopUpData.updatePhoneComment}"  rendered="#{pc_commentsPopUpData.updateMode}" styleClass="buttonRight" />
					<%--  end SPR3073 --%>
				</h:panelGroup>
			</h:panelGroup>
		</h:form>	
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
		<h:inputHidden id="refreshParent" value="#{pc_commentsPopUpData.refreshParent}" />  <%-- SPR3073--%>
	</f:view>
</body>
</html>
