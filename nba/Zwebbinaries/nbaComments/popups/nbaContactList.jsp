<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA337         NB-1401   Email Enhancement & support email attachment-->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<html>
<head>
<base href="<%=basePath%>">
<title>Contacts</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	<!--
		var titleName = null;
		var width=380;
		var height=180;
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_contactList'].target='controlFrame';
			return false;
		}
		
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_contactList'].target='';
			return false;
		}
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" onload="popupInit(); resetTargetFrame();">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:form id="form_contactList">
			<h:panelGroup id="contactsTable_PGroup" styleClass="formDivTableDataRightButtons3" style="margin-left:17px; margin-top:15px; width: 345px; height: 95px; background-color: #FFFFFF">
				<h:dataTable id="contacts_DataTable" styleClass="formTableDataRightButtons" cellspacing="0" binding="#{pc_contactsTable.dataTable}"
					value="#{pc_contactsTable.contactsList}" style="table-layout: auto; min-height: 5px; width: 343px;"
					var="contactsRow" columnClasses="ovColText350" rowClasses="#{pc_contactsTable.rowStyles}">
						<h:column>
							<h:commandLink id="contactsCol1_Link"  action="#{pc_contactsTable.selectForMultipleRows}" value="#{contactsRow.address}" title="#{contactsRow.title}">
							</h:commandLink>		
						</h:column>										
				</h:dataTable>
			</h:panelGroup>						
		    <h:panelGroup styleClass="buttonBar" style="width: 580px; margin-top:10px">
				<h:commandButton id="cb1" value="#{property.buttonCancel}" action="#{pc_commentsPopUpData.cancelContactList}" immediate="true" styleClass="buttonLeft" onclick="setTargetFrame();"/>
				<h:commandButton id="cb2" value="#{property.buttonClear}" onclick="resetTargetFrame();" immediate="true" action="#{pc_commentsPopUpData.clearContactList}" styleClass="buttonLeft-1"  />
				<h:commandButton id="cb5" value="#{property.buttonUpdate}" action="#{pc_commentsPopUpData.updateContactList}" styleClass="buttonRight" onclick="setTargetFrame();"/>
			</h:panelGroup>			
		</h:form>
	<div id="Messages" style="display: none"><h:messages></h:messages></div>
</f:view>
</body>
</html>
