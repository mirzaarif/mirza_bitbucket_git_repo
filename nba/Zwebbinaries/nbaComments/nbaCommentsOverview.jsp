<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA152            6      Comments Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- NBA213			   7	  Unified User Interface  -->
<!-- NBA225            8      Comments -->
<!-- FNB011 	 NB-1101  	  Work Tracking -->
<!-- FNB004 	 NB-1101 	  PHI -->
<!-- FNB004 - VEPL1274 	 NB-1101 	  Commit button not available -->
<!-- SPRNBA-583  NB-1301      General Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Comments Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>	<!-- SPR3073 -->
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		parent.fileLocationHRef = '<%=basePath%>' + 'nbaComments/nbaCommentsOverview.faces';		
		function setTargetFrame() {
			//alert('calling set targeet frame');
			document.forms['form_commentsOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_commentsOverview'].target='';
			return false;
		}
		//NBA213 New Function
		function initPage(){
			filePageInit();
			if (document.getElementById('form_commentsOverview:commentsTable:commentData') != null) {
				<!-- FNB004 - VEPL1274 Code deleted -->
				tabHeight = window.screen.availHeight - 525; <!-- FNB004 - VEPL1274 -->
				document.getElementById('form_commentsOverview:commentsTable:commentData').style.height = tabHeight; <!-- FNB004 - VEPL1274 -->
			}
		}
		
	</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965, NBA213 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_COMMENTS_BF" value="#{pc_commentsBFNavigation}" />
		<h:form id="form_commentsOverview">
			<!-- begin NBA213 -->
			<h:panelGroup id="commentsTitle" styleClass="sectionHeader"> <!-- NBA225 -->
				<h:outputText id="commentsSectionTitle" value="#{property.commentsTitle}" styleClass="shTextLarge" style="position: absolute; left: 10px"/>	
			</h:panelGroup>
			<h:panelGroup id="checkBoxes" styleClass="sectionSubheader" style="padding-top: 0px"> <!-- NBA225 -->
				<h:selectBooleanCheckbox id="commCB2" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.general}" style="margin-left: 12px" />
				<h:graphicImage value="images/comments/lock-open-forsectionhead.gif" styleClass="commentIcon"  />
				<h:outputText value="#{property.generalLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="commCB3" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.special}" style="margin-left: 12px; width: 20px" />
				<h:graphicImage value="images/comments/exclamation-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.specialInstructionLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
				<!-- begin FNB004, NBA324 -->
				<h:selectBooleanCheckbox id="commCB3a" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.PHI}" 
				 rendered ="#{pc_commentsBFNavigation.auth.visibility['PHICommentShowSelect']}" style="margin-left: 12px; width: 20px;"/>
				<h:graphicImage id="phiImage" value="images/comments/PHI-forsectionhead.gif" styleClass="commentIcon" rendered ="#{pc_commentsBFNavigation.auth.visibility['PHICommentShowSelect']}"/>
				<h:outputText id="phiCheckBox" value="#{property.phiLabel}" styleClass="shText" rendered ="#{pc_commentsBFNavigation.auth.visibility['PHICommentShowSelect']}"/><!-- SPRNBA-583	 -->
				<!-- end FNB004, NBA324 -->
				<h:selectBooleanCheckbox id="commCB4" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.automated}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/zigzag-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.automatedLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="ccommCB5" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.phone}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/phone-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.phoneLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="commCB6" styleClass="formEntryCheckbox" value="#{pc_commentsBFNavigation.email}" style="margin-left: 12px; width: 20px;" />
				<h:graphicImage value="images/comments/envelope-forsectionsubhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.emailLabel}" styleClass="shText" />	<!-- SPRNBA-583	 -->
			<!-- FNB004 code deleted -->		
			</h:panelGroup>
			<!--  end NBA213 -->
			<!-- NBA225 -->
			<h:panelGroup id="voidOption" styleClass="sectionSubheader" style="padding-top: 0px">
				<h:outputText id="voidedLabel" value="#{property.voidedLabel}" styleClass="formLabel" style="width: 90px; margin-left: -5px;"/><!-- FNB004 -->
				<h:selectOneMenu id="voidedList" styleClass="shMenu" style="width: 300px" value="#{pc_commentsBFNavigation.voidedOption}" 									 
								 disabled="#{pc_commentsBFNavigation.voidOptionDisabled}"><!-- FNB004, NBA324 -->
					<f:selectItems id="voidedListValue" value="#{pc_commentsBFNavigation.voidedList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<!-- Begin FNB004 -->
			<h:panelGroup id="clientRelation" styleClass="sectionSubheader" style="padding-top: 0px">
				<h:outputText value="#{property.commentRelates}" styleClass="formLabel" style="width: 90px; margin-left: -5px;"/>		
				<h:selectOneMenu value="#{pc_commentsBFNavigation.selectedInsured}" styleClass="shMenu" style="width: 300px">
					<f:selectItems value="#{pc_commentsBFNavigation.insuredClients}"/>
				</h:selectOneMenu>
				<h:commandButton id="goButton" value="Go" styleClass="formButtonRight" style="margin-top: 0px" action="#{pc_commentsTable.retrieveSelectedRecords}"/>
			</h:panelGroup>
			<!-- End FNB004 -->
			<f:subview id="commentsTable">
				<c:import url="/uw/subviews/NbaCommentsTable.jsp" />
			</f:subview>
			<f:subview id="nbaCommentBar">
				<c:import url="/nbaComments/subviews/NbaCommentBar.jsp" />
			</f:subview>

			<h:panelGroup id="commandButtons" styleClass="tabButtonBar"> <!-- NBA225 -->
				<!-- begin NBA213 -->
				<h:commandButton id="btnCommRefresh" value="#{property.buttonRefresh}" styleClass="ovButtonLeft" action="#{pc_commentsBFNavigation.refresh}" immediate="true"/>
				<h:commandButton id="btnCommDelete" action="#{pc_commentsBFNavigation.allComments.deleteComment}" onclick="setTargetFrame();" 
					value="#{property.buttonDelete}" disabled="#{pc_commentsBFNavigation.allComments.deleteDisabled}" styleClass="ovButtonLeft-1" immediate="true"/>
				<!-- NBA225 -->
				<h:commandButton id="btnCommVoid" action="#{pc_commentsBFNavigation.allComments.voidComment}" onclick="setTargetFrame();" 
					value="#{property.buttonVoid}" rendered="#{pc_commentsBFNavigation.allComments.voidRendered}" disabled="#{pc_commentsBFNavigation.allComments.voidDisabled}"
					styleClass="ovButtonLeft-1" immediate="true"/>
				<h:commandButton id="btnCommCommit" action="#{pc_commentsBFNavigation.allComments.commitComment}" value="#{property.buttonCommit}"
					disabled="#{pc_commentsBFNavigation.auth.enablement['Commit'] || 
								pc_commentsBFNavigation.notLocked}" 
					styleClass="ovButtonRight-1" /> <!-- NBA213 -->
				<h:commandButton id="btnCommUpdate"  onclick="launchCommentPopUp('Update');" value="#{property.buttonUpdate}" disabled="#{pc_commentsBFNavigation.allComments.updateDisabled}" styleClass="ovButtonRight" /> <!--SPR3073 -->
				<!-- end NBA213 -->
			</h:panelGroup>
		</h:form>
		<!-- NBA213 code deleted -->
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	