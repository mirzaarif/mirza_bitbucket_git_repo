<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View --%>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if(request.getServerPort() == 80){
	basePath = request.getScheme()+"://"+request.getServerName() + path+"/";
} else {
	basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path+"/";
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Refresh Comment Bar Upon Commit</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		function refreshBF() {
			if (top.mainContentFrame != null) {
				if (top.mainContentFrame.contentRightFrame != null) {
					if (top.mainContentFrame.contentRightFrame.file != null) {
						//if commmentBar exists in its own iframe on the business function
						if (top.mainContentFrame.contentRightFrame.file.commentBar != null) {
							top.mainContentFrame.contentRightFrame.file.commentBar.location.href = top.mainContentFrame.contentRightFrame.file.commentBar.location.href;
						} else if (top.mainContentFrame.contentRightFrame.file.file != null) {
						    if (top.mainContentFrame.contentRightFrame.file.file.interviewpage == null) {
						    	hideCommitButton(top.mainContentFrame.contentRightFrame.file.file);
						    }
						} else {
							hideCommitButton(top.mainContentFrame.contentRightFrame.file);
						}
					}
				}
				if (top.mainContentFrame.contentLeftFrame != null) {
					if (top.mainContentFrame.contentLeftFrame.file != null) {
						if (top.mainContentFrame.contentLeftFrame.file.file != null) {
							if (top.mainContentFrame.contentLeftFrame.file.file.location.href.indexOf("inbox") < 0) {
								hideCommitButton(top.mainContentFrame.contentLeftFrame.file.file);
							}
						}
					}
				}
			}
		}

		//Find and hide the commit button on the nbA comment bar
		function hideCommitButton(frame) {
			var elementList = frame.document.getElementsByTagName("*");
			var listSize = elementList.length;
			for (i=0; i<listSize; i++) {
				if (elementList[i].id.indexOf("nbaCommentBar:commit") >= 0) {
					elementList[i].style.visibility = "hidden";
					return;
				}
			}
		}
	</script>
</head>

<body>
Refresh Comment Bar Upon Commit of Comments
	<f:view>
		<div id="Messages">
			<h:messages />
		</div>
	</f:view>
	<script type="text/javascript">
			try {
				var innerText=document.all["Messages"].innerHTML;
				var message=top.getInnerHTML(innerText);
				document.all["Messages"].innerHTML= message;
				if (document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != "") {
					parent.showWindow('<%=path%>','faces/error.jsp', this);
				} else {
					//only refresh the business function's comment bar if there was not an error message
					refreshBF();
				}
				top.hideWait();
			} catch(err) {
//				alert(err);
			}
	</script>
</body>
</html>
