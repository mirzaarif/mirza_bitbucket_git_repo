<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA152            6      Comments Rewrite -->
<!-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA225 		   8	  nbA Comments -->
<!-- FNB004 	 NB-1101 	  PHI -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<!--  NBA158 code deleted -->	
	<f:loadBundle basename="properties.nbaApplicationData" var="properties" />
	<h:panelGrid columns="1" styleClass="commentBar" cellpadding="0" cellspacing="0" > <!-- NBA152 -->
		<h:column> <!-- NBA152 -->
			<h:panelGroup styleClass="commentBar"> 
				<h:commandLink value="#{properties.previousAbsolute}" rendered="#{pc_commentsTable.showPrevious}" styleClass="commentPaging" style="text-indent: 0px;"
								action="#{pc_commentsTable.previousPageAbsolute}" immediate="true" />
				<h:commandLink value="#{properties.previousPage}" rendered="#{pc_commentsTable.showPrevious}" styleClass="commentPaging"
								action="#{pc_commentsTable.previousPage}" immediate="true" />
				<h:commandLink value="#{properties.previousPageSet}" rendered="#{pc_commentsTable.showPreviousSet}" styleClass="commentPaging"
								action="#{pc_commentsTable.previousPageSet}" immediate="true" />
				<h:commandLink value="#{pc_commentsTable.page1Number}" rendered="#{pc_commentsTable.showPage1}" styleClass="commentPaging"
								action="#{pc_commentsTable.page1}" immediate="true" />
				<h:outputText value="#{pc_commentsTable.page1Number}" rendered="#{pc_commentsTable.currentPage1}" styleClass="commentPagingBold" />
				<h:commandLink value="#{pc_commentsTable.page2Number}" rendered="#{pc_commentsTable.showPage2}" styleClass="commentPaging"
								action="#{pc_commentsTable.page2}" immediate="true" />
				<h:outputText value="#{pc_commentsTable.page2Number}" rendered="#{pc_commentsTable.currentPage2}" styleClass="commentPagingBold" />
				<h:commandLink value="#{pc_commentsTable.page3Number}" rendered="#{pc_commentsTable.showPage3}" styleClass="commentPaging"
								action="#{pc_commentsTable.page3}" immediate="true" />
				<h:outputText value="#{pc_commentsTable.page3Number}" rendered="#{pc_commentsTable.currentPage3}" styleClass="commentPagingBold" />
				<h:commandLink value="#{pc_commentsTable.page4Number}" rendered="#{pc_commentsTable.showPage4}" styleClass="commentPaging"
								action="#{pc_commentsTable.page4}" immediate="true" />
				<h:outputText value="#{pc_commentsTable.page4Number}" rendered="#{pc_commentsTable.currentPage4}" styleClass="commentPagingBold" />
				<h:commandLink value="#{pc_commentsTable.page5Number}" rendered="#{pc_commentsTable.showPage5}" styleClass="commentPaging"
								action="#{pc_commentsTable.page5}" immediate="true" />
				<h:outputText value="#{pc_commentsTable.page5Number}" rendered="#{pc_commentsTable.currentPage5}" styleClass="commentPagingBold" />		
				<h:commandLink value="#{properties.nextPageSet}" rendered="#{pc_commentsTable.showNextSet}" styleClass="commentPaging"
								action="#{pc_commentsTable.nextPageSet}" immediate="true" />
				<h:commandLink value="#{properties.nextPage}" rendered="#{pc_commentsTable.showNext}" styleClass="commentPaging"
								action="#{pc_commentsTable.nextPage}" immediate="true" />
				<h:commandLink value="#{properties.nextAbsolute}" rendered="#{pc_commentsTable.showNext}" styleClass="commentPaging"
								action="#{pc_commentsTable.nextPageAbsolute}" immediate="true" />

				<!-- begin SPR3073  -->
				<h:commandButton id="lockClosed" image="images/comments/lock-closed-forbar.gif" title="#{property.addSecureComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('SECURE');" style="position: absolute; left: #{pc_commentsTable.posSecureIcon}%"
								rendered="#{pc_commentsPopUpData.secureAddRendered}"/> <!-- NBA152, NBA225, FNB004 -->
				<h:commandButton id="lockOpen" image="images/comments/lock-open-forbar.gif" title="#{property.addGeneralComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('GENERAL');" style="position: absolute; left: #{pc_commentsTable.posGeneralIcon}%" /><!-- FNB004 -->
				<h:commandButton id="exclamation" image="images/comments/exclamation-forbar.gif" title="#{property.addSpecialInstruction}" styleClass="commentIcon"
								onclick="launchCommentPopUp('INSTRUCTION');" style="position: absolute; left: #{pc_commentsTable.posSpecialIcon}%" /><!-- FNB004 -->
				<h:commandButton id="PHI" image="images/comments/PHI-forbar.gif" title="#{property.addPHIComment}" styleClass="commentIcon"
								onclick="launchCommentPopUp('PHI');" style="position: absolute; left: #{pc_commentsTable.posPHIIcon}%"
								rendered="#{pc_commentsPopUpData.phiRendered}"/><!-- FNB004 NBA324 -->
				<h:commandButton id="phone" image="images/comments/phone-forbar.gif" title="#{property.addPhoneRecord}" styleClass="commentIcon"
								onclick="launchCommentPopUp('Phone');" style="position: absolute; left: #{pc_commentsTable.posPhoneIcon}%" />
				<h:commandButton id="envelope" image="images/comments/envelope-forbar.gif" title="#{property.sendAnEmail}" styleClass="commentIcon"
								onclick="launchCommentPopUp('Email');" style="position: absolute; left: #{pc_commentsTable.posEmailIcon}%" />
		
				<h:commandButton id="defaultLabel" value="#{property.addComment}" rendered="#{pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								onclick="launchCommentPopUp('Secure');" /> <!-- NBA152 -->
				<h:commandButton id="defaultLabe2" value="#{property.addComment}" rendered="#{!pc_commentsPopUpData.securedRendered}" styleClass="commentBarLabelButton"
								onclick="launchCommentPopUp('General');" /> <!-- NBA152 -->
				<!--  end SPR3073 -->
			</h:panelGroup>		
		</h:column> <!--NBA152 -->
	</h:panelGrid> <!-- NBA152 -->
</jsp:root>