<%@ page language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<html>
<head>
<base href="<%=basePath%>">
<title>Internal Authentication for J2EE</title>
<SCRIPT type="text/javascript" src="javascript/error.js"></SCRIPT>
<script type="text/javascript">
<!--
		function invokeAuthenticate(){
			try{
				var logonDocument = top.mainContentFrame.window.document;
				if(logonDocument != null){
					var mainUser = logonDocument.getElementById("logonForm:usernamefld");
					var mainPassword = logonDocument.getElementById("logonForm:passwordfld");
					if(mainUser != null){
						document.getElementById("j_username").value = mainUser.value;
					}
					if(mainPassword != null){
						document.getElementById("j_password").value = mainPassword.value;
					}
					document.getElementById("submit").click();
				}
			}catch(err){
				reportException(err,"Exception");
			}
		}
//--></script>
</head>
<body onload="invokeAuthenticate();">
	<form method="POST" action='<%=response.encodeURL("j_security_check") %>' id="J2EEForm">
		<p>
			Secure Access:
		</p>
		<TABLE>
			<tr>
				<td>
					User:
				</td>
				<td>
					<input type="text" name="j_username" id="j_username"></input>
				</td>
			</tr>
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input type="password" name="j_password" id="j_password"></input>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="2">
					<input type="submit" id="submit"></input>
				</td>
			</tr>
		</TABLE>

	</form>
</body>
</html>
