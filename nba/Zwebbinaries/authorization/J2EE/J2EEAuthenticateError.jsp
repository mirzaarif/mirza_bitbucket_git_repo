<%@ page language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<html>
<head>
<base href="<%=basePath%>">
<title>Internal Authentication for J2EE</title>
<script type="text/javascript">
<!--
		window.opener = parent;
		parent.showWindow('<%=path%>','/error.jsp', parent);
//--></script>
</head>
<body>
<div id="Messages">
<p>Failed to authenticate user with provided credentials</p>
</div>
</body>
</html>
