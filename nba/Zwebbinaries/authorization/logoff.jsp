
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>Logoff Page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
	<!--
		function doLogoff(){
			document.getElementById("logoffForm:ok").click();	//NBA213 	
			document.getElementById("logoffForm:ok").click();	//faces occasionally ignore the first request	//NBA213 			
		}
	//-->
	</script>
</head>
<body class="darkBlueBody">
	<f:view>
		<h:form id="logoffForm">
			<h:commandButton id="ok" type="submit" action="#{pc_Logoff.logoff}" value=""></h:commandButton>
		</h:form>
	</f:view>
</body>
</html>