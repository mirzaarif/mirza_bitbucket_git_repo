<%@ page language="java" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>" target="controlFrame">
	<title>Logon Page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
	<!--
		try{
			top.mainContentFrame.authConfirmed = true;
			top.mainContentFrame.completeLogon();
		} catch(err){
			top.reportException(err, "setting authenticated flag!");
		}
	//-->
	</script>
</head>
<body>
	<p>Confirmed</p>
</body>
</html>
