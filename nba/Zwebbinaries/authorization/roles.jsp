<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath =
	request.getScheme()
		+ "://"
		+ request.getServerName()
		+ ":"
		+ request.getServerPort()
		+ path
		+ "/";
%>

<head>
<base href="<%=basePath%>" >
<title>Roles</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=280;
		var height=180;
		
	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['rolesForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['rolesForm'].target='';
		return false;
	}
		
	function highlightRow(){
		top.scrollHighlight(document.getElementById('rolesTableData'), 
					document.getElementById('rolesForm:selectedRow').innerHTML);
	}		
		
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="popupInit();highlightRow();">
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="rolesForm">
		<h:outputText style="display:none;" value="#{pc_Roles.refresh}"></h:outputText>
		<table width="98%" align="center" cellpadding="2" cellspacing="0" border="0">
			<tbody>
				<tr>
					<td>
					<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr>
								<td align="right"><Help:Link contextId="Accel_Roles"/></td>
							</tr>
							<tr>
								<td class="headerRow"><h:dataTable styleClass="header" headerClass="headerTable"  cellpadding="0"
									width="100%" cellspacing="0" border="0">
									<h:column>
										<f:facet name="header">
											<h:outputText style="width:88px" value="#{bundle.roles_type}"></h:outputText>
										</f:facet>
									</h:column>
								</h:dataTable></td>
							</tr>
							<tr>
								<td>
								<div class="divTable" style="height: 100px" id="rolesTableData"><h:dataTable
									id="rowTable" value="#{pc_Roles.roles}"
									binding="#{pc_Roles.rolesTable}" var="currentRow"
									columnClasses="column" rowClasses="#{pc_Roles.rowStyles}"
									headerClass="header" styleClass="complexTable" cellpadding="2"
									cellspacing="0" width="100%" border="1">

									<h:column>
										<h:commandLink action="#{pc_Roles.selectRow}" onmouseover="resetTargetFrame();">
											<h:outputText style="width:76px;" id="typeValue"
												value="#{currentRow}"></h:outputText>
										</h:commandLink>
									</h:column>
								</h:dataTable></div>
								<h:outputText style="display:none;" id="selectedRow"
									value="#{pc_Roles.selectedRowIndex}"></h:outputText>
								</td>
							</tr>
							<tr>
								<td height="2px"></td>
							</tr>
							<tr>
								<td>
								<table width="100%" align="right" cellpadding="0"
									cellspacing="10" border="0">
									<tbody>
										<tr>
										<td align="right" colspan="2"><h:commandButton id="OK"
												styleClass="button" value="#{bundle.ok}"
												action="#{pc_Roles.submit}" onclick="setTargetFrame();">
											</h:commandButton>
											
											<h:commandButton id="Cancel"
												styleClass="buttonCancel" value="#{bundle.cancel}" onclick="setTargetFrame();"
												immediate="true" action="#{pc_Roles.cancel}" />											
											</td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div id="Messages" style="display: none"><h:messages /></div>
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
</f:view>
</body>
</html>

