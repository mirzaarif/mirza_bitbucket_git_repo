<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

boolean closeWindow = false;
if (request.getQueryString() != null) {
	closeWindow = (request.getQueryString().indexOf("close=true") >= 0);
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		if ('<%=closeWindow%>' == 'true'){
			parent.closeAllWindows();
		}
		top.loadPolicyOnRight('<%=basePath%>');
		//top.refreshLeftFile();

	</script>
</head>
<body>
<f:view>
	<div id="Messages"><h:messages /></div>
</f:view>
</body>
</html>
