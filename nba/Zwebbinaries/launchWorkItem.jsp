
 <!-- CHANGE LOG -->
 <!-- Audit Number   Version   Change Description -->
 <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->
 <!-- SPRNBA-947     NB-1601   Null Pointer Exception May Occur in Inbox if User Selects Different Work -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Work Item</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	window.opener = parent;
	function execute(){
		parent.closeAllWindows();
		if (document.getElementById("Messages") != null){
		        //Begin SPR3356	
		         var innerText=document.getElementById("Messages").innerHTML;
		         innerText=top.getInnerHTML(innerText);		    
		         document.getElementById("Messages").innerHTML= innerText;  		   
		         //End SPR3356 
				if (document.getElementById("Messages").innerHTML != ""){
					top.showWindow(top.basePath,'faces/error.jsp', this);
				}
			}
		//Begin SPRNBA-947
		//Refresh the policySelection href to trigger the "newbusinessHasChanged" event in workSelection.jsp
		if (top.mainContentFrame.contentLeftFrame.policySelection) {
			top.mainContentFrame.contentLeftFrame.policySelection.location.href = top.mainContentFrame.contentLeftFrame.policySelection.location.href;
		}
		//End SPRNBA-947
	}
	</script>
</head>
<body onload="execute();">
<f:view>
	<h:form id="selectWorkForm">
		<div id="workRelaunch" style="display:none">
			<h:outputText value="#{pc_WorkflowFile.workRelaunch}"></h:outputText>
		</div>
	</h:form>
	<div id="Messages"><h:messages /></div>
</f:view>
<div id="MessageTitle">Information</div>
</body>
</html>
