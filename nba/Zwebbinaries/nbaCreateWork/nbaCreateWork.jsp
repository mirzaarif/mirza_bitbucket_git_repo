<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA174           7         Create Work UI Rewrite -->
<!-- NBA213			  7			Unified User Interface -->
<!-- FNB011			  	NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<!--  begin NBA213  -->
	<f:view>	
		<head>
			<base href="<%=basePath%>">
			<f:loadBundle basename="properties.nbaApplicationData" var="property" />
			<title><h:outputText value="#{property.createWrkTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/file.js"></script>
			<script type="text/javascript" src="javascript/global/dialogManagement.js"></script>
			<script type="text/javascript" src="javascript/nbapopup.js"></script> 
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
						
				var contextpath = '<%=path%>';	//FNB011		
			
				var width=620;
				var height=300; 
				<!--  end begin NBA213  -->
				function setTargetFrame() {
					document.forms['form_creatework'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_creatework'].target='';
					return false;
				}
			</script>
		
		</head>
		<!--  begin NBA213  -->
		<body onload="popupInit();"> 
			<PopulateBean:Load serviceName="RETRIEVE_CREATEWORK" value="#{pc_createWork}" />
			<h:form id="form_creatework">
				<h:panelGroup id="createworkForm" styleClass="updatePanel" style="height: 240px"> 
					<h:panelGroup styleClass="formDataEntryTopLine">
						<h:outputText value="#{property.createWrkBussArea}" styleClass="formLabel" />
						<h:selectOneMenu styleClass="formEntryTextFull" immediate="true" value="#{pc_createWork.businessArea}"
							valueChangeListener="#{pc_createWork.changeBusinessArea}" onchange="submit()">
							<f:selectItems value="#{pc_createWork.businessAreas}" />
						</h:selectOneMenu>
					</h:panelGroup> 
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText value="#{property.createWrkType}" styleClass="formLabel" />
						<h:selectOneMenu styleClass="formEntryTextFull" immediate="true" value="#{pc_createWork.workType}"
							valueChangeListener="#{pc_createWork.changeWorkType}" onchange="resetTargetFrame();submit()">
							<f:selectItems value="#{pc_createWork.workTypes}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText value="#{property.createWrkInitialSts}" styleClass="formLabel" />
						<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_createWork.status}" immediate="true"
							valueChangeListener="#{pc_createWork.changeStatus}" onchange="resetTargetFrame();submit()">
							<f:selectItems value="#{pc_createWork.initialStatuses}" />
						</h:selectOneMenu>
					</h:panelGroup> 
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText value="#{property.createWrkQueue}" styleClass="formLabel" />
						<h:outputText value="#{pc_createWork.queue}" styleClass="formEntryTextFull" />
					</h:panelGroup> 
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText value="#{property.createWrkReason}" styleClass="formLabel" />
						<h:inputText id="reasonText" maxlength="75" binding="#{pc_createWork.reasonText}" value="#{pc_createWork.reason}" styleClass="formEntryTextFull" />
					</h:panelGroup> 
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputText value="#{property.createWrkPrity}" styleClass="formLabel" />
						<h:selectOneMenu styleClass="formEntryTextFull" binding="#{pc_createWork.priSelect}" value="#{pc_createWork.priority}">
							<f:selectItems value="#{pc_createWork.priorities}" />
						</h:selectOneMenu>
					</h:panelGroup>
				</h:panelGroup> <!-- NBA213 -->
				<!-- NBA213 code deleted -->
				<h:panelGroup styleClass="buttonBar" style="padding-top: 0px;">
					<h:commandButton id="suspendCancel" value="#{property.buttonCancel}" styleClass="buttonLeft" 
						action="#{pc_createWork.cancel}" onclick="setTargetFrame();" /> 
					<h:commandButton id="btnCommit" value="#{property.buttonCommit}" 
						action="#{pc_createWork.commit}" 
						disabled="#{pc_createWork.auth.enablement['Commit'] || 
									pc_createWork.notLocked || 
									!pc_createWork.workEntityOrTran}"
						styleClass="buttonRight" onclick="setTargetFrame();" />  <!-- NBA213 FNB011 -->
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display:none"><h:messages /></div>
		</body> 
	</f:view>
	<!--  end NBA213  --> 
</html>
