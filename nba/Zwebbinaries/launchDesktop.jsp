<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
String desktopName = "";
desktopName = request.getQueryString();
int i = desktopName.indexOf("desktopView=");
if(i >= 0){
	desktopName = desktopName.substring(i + 12);
}
path += "/";

boolean closeWindow = (request.getQueryString().indexOf("close=true") >= 0);

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Launch Desktop</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		window.opener = parent;
		if ('<%= closeWindow%>' == 'true'){
			parent.closeWindow();
		}
		
		parent.closeAllWindows();
		
		parent.showWait("Initializing Desktop");
		var waitTimer = null;
		checkLoaded(10); //NBA208-19
		parent.launchDesktop('<%= path%>','<%= desktopName%>');
		
		function isLoaded(){
			if(top.mainContentFrame.location.href.indexOf('<%= path%>') > -1){
				parent.hideWait();
			} else {
				checkLoaded(100); //NBA208-19
			}
		}
		
		function checkLoaded(waitTime){ //NBA208-19
			try {
				waitTimer = setTimeout('isLoaded()', waitTime); //NBA208-19
			}catch(er){
				reportException(er, "checkLoaded");
			}
		}
	</script>
</head>
<body>
	<f:view>
	</f:view>
</body>
</html>
