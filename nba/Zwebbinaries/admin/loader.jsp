<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.Initializer"%>
<%@ page import="java.util.*"%>

<% 
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<SCRIPT type="text/javascript" src="javascript/error.js"></SCRIPT>

<script>
	function execute(url){
		results.location.href = url;
	}
</script>
</head>
<body class="darkBlueBody" topmargin="0" leftmargin="0" rightmargin="0"
	bottommargin="0" marginwidth="0" marginheight="0">
<table width="98%" align="center" cellpadding="0" cellspacing="0"
	border="0">
	<tbody>
		<tr class="windowBar">
			<td class="windowBarLeft"></td>
			<td class="windowBarCenter">
			<div class="textWindowBar" onmousedown="return false;"
				onmouseover="return false;">Accelerator Data Loader Console</div>
			</td>
			<td class="windowBarRight"></td>
		</tr>
		<tr class="windowBarSeparator">
			<td colspan="3"></td>
		</tr>
		<tr valign="top" class="mainBody" style="height: 500">
			<td colspan="3" align="center">
			<table width="100%" height="100%">
				<tbody>
					<tr>
						<td align="center"><iframe name="content" id="content" width="98%"
							height="460" frameborder="0" src="admin/loader/view.jsp" ></iframe></td>
					</tr>
					<tr>
						<td class="subTextTitle">Results</td>
					</tr>
					<tr>
						<td><iframe name="results" id="results" width="98%"
							height="140" frameborder="0" ></iframe></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
