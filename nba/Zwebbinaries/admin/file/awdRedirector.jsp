<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.awd.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";

	function execute(url){
		top.results.location.href = url;
	}
	
	function killSession(){
		if(lastKey != null && lastKey != ""){
			if(confirm("Are you sure you wish to terminate the AWD Sessions for user[" + lastKey + "]")){
				execute("awdRedirectorKillUser.jsp?key=" + lastKey);
			}
		} else {
			alert("Please select a user to terminate");
		}
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "awdRedirectorLog.jsp?key=" + keyName;
			document.all["killSession"].disabled=false;
			userLog.location.href = url;
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
	function refresh(){
		top.content.file.location.href = top.content.file.location.href;
	}
	
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" colspan="2"></td>
	</tr>
	<%
		List currentUsers = AWDAccess.getUsersTable();
	%>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Current Users</td>
				<td align="right">
				<button class="button"
					onclick="refresh();">Refresh</button>
				<button class="button" id="killSession"
					onclick="killSession();" disabled="disabled">Kill Session</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" class="section1" style="text-align: center">
			<table width="98%" cellpadding="0" cellspacing="0">
				<tr class="header">
					<th width="300">AWD User Name</th>
					<th width="*"># Active Sessions</th>
				</tr>
			</table>
			<div class="divTable" style="height: 80px; width:98%;" >
				<table width="100%" cellpadding="0" cellspacing="0">
				<%
					Iterator iter = currentUsers.iterator();
					boolean isOdd = true;
					while(iter.hasNext()){
						String[] detail = (String[])iter.next();
						String keyName = "";
						String sessions = "";						
						if(detail != null && detail.length == 2){
							keyName = detail[0];
							sessions = detail[1];
						}
						String currentStyle = (isOdd ? "rowOdd" : "rowEven");
						isOdd = !isOdd;
				%>
					<tr id="<%=keyName%>" class="<%=currentStyle %>" onclick="setSelection('<%=keyName%>');" style="text-align:left">
						<td id="<%=keyName%>Text" class="textLabel" style="text-align:left" width="300" onclick="setSelection('<%=keyName%>');"><%=keyName %></td>
						<td id="<%=keyName%>SessText" class="textLabel" style="text-align:left" width="*" onclick="setSelection('<%=keyName%>');"><%=sessions %></td>
					</tr>
				<%
					}
				%>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" ></td>
	</tr>
	<tr>
		<td height="15" colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe id="userLog" name="userLog"  width="101%" height="220" frameborder="0" src=""></iframe>
		</td>
	</tr>
</table>
</div>
</body>
</html>
