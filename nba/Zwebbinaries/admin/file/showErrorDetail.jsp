<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.Initializer"%>
<%@ page import="com.csc.fs.accel.ui.ServiceDelegator"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if(i >= 0){
	keyName = keyName.substring(i + 4);
}

%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="900px">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Service Action Execution Errors Log - <%=keyName %></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="100%" class="section1"
			style="text-align: center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<th width="150">Date Time Stamp</th>			
				<th width="150">UserName</th>
				<th>USK</th>
			</tr>
		</table>
		<div class="divTable" style="height: 550px; width:98%" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<%
				ServiceDelegator.ServiceActionLogEntry logEntry = (ServiceDelegator.ServiceActionLogEntry)ServiceDelegator.serviceActionLog.get(keyName);
				Iterator iter = logEntry.errors.iterator();
				boolean isOdd = true;
				while(iter.hasNext()){
					ServiceDelegator.ServiceActionErrorLog errorEntry = (ServiceDelegator.ServiceActionErrorLog)iter.next();
					String currentStyle = (isOdd ? "rowOdd" : "rowEven");
					isOdd = !isOdd;
			%>
				<tr class="<%=currentStyle %>" style="text-align:left">
					<td class="textLabel" style="text-align:left; font-weight: bold;" width="150" ><%=errorEntry.dateTime.toString() %></td>
					<td class="textLabel" style="text-align:left; font-weight: bold;" width="150" ><%=errorEntry.userName %></td>
					<td class="textLabel" style="text-align:left; font-weight: bold;" );"><%=errorEntry.userSessionKey %></td>
				</tr>
			<%
					currentStyle = (isOdd ? "rowOdd" : "rowEven");
					isOdd = !isOdd;
			%>
				<tr class="<%=currentStyle %>" style="text-align:left">
					<td class="textLabel" style="text-align:right; font-weight: bold;"> -- Process Stack:</td>
					<td class="textLabel" style="text-align:left;" colspan="2">
					<div class="divTable" style="height: 150px; width:710px; border: none;" >
						<%
							String[] procs = errorEntry.processStack.split("\\$\\$");
							for(int elem = 0; elem < procs.length; elem ++){
						%>
							<%=procs[elem] %><br>
						<%
							}
						%>
					</div>
					</td>
				</tr>
			<%
					currentStyle = (isOdd ? "rowOdd" : "rowEven");
					isOdd = !isOdd;
			%>
				<tr class="<%=currentStyle %>" style="text-align:left">
					<td class="textLabel" style="text-align:right; font-weight: bold;"  > -- Current IDs:</td>
					<td class="textLabel" style="text-align:left;" colspan="2">
					<div class="divTable" style="height: 150px; width:710px; border: none;" >
						<%
							String[] lines = errorEntry.currentIDs.split(",");
							for(int elem = 0; elem < lines.length; elem ++){
						%>
							<%=lines[elem] %><br>
						<%
							}
						%>
					</div>
					</td>
					
					
				</tr>
			<%
					Iterator msgIter = errorEntry.errors.iterator();
					while(msgIter.hasNext()){
						String errorText = (String)msgIter.next();
						currentStyle = (isOdd ? "rowOdd" : "rowEven");
						isOdd = !isOdd;
			%>
				<tr class="<%=currentStyle %>" style="text-align:left">
					<td class="textLabel" style="text-align:right; font-weight: bold;"> -- Error:</td>
					<td class="textLabel" style="text-align:left;" colspan="2">
					<div class="divTable" style="height: 150px; width:710px; border: none;" >
						<%=errorText %>
					</div>
					</td>
				</tr>
			<%
					}
			
			%>		
				<tr>
					<td bgcolor="#000000" height="1" colspan="3"></td>
				</tr>
			
			<%
				}
			%>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
</table>
</div>
</body>
</html>
