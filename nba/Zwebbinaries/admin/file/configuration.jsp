<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Configuration Parameters</td>
				<td align="right">
				<button class="button"
					onclick="execute('../../servlet/Initializer?mode=REFRESH');">Reload</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="100%" class="section1"
			style="text-align: center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<th width="250">Configuration Name</th>
				<th align="left">Value</th>
				<th align="left">Loaded</th>
			</tr>
			<tr class="rowOdd">
				<td width="250" class="textLabel">ServiceAccessConfigLocation</td>
				<td class="text" align="left">
				<% 
					Iterator iter = Initializer.serviceAccessConfigLocations.iterator();
					while(iter.hasNext()){
						String location = (String)iter.next();
				%>
					<a href=".<%=location%>" target="_new"><%=location%></a><br>
				<%
					}
				%>
				</td>
				<td class="text" align="left"><%=Initializer.serviceAccessConfigLoaded%></td>
			</tr>
			<tr class="rowEven">
				<td width="250" class="textLabel">BPMConfigLocation</td>
				<td class="text" align="left">
				<% 
					iter = Initializer.bpmAccessConfigurationLocations.iterator();
					while(iter.hasNext()){
						String location = (String)iter.next();
				%>
					<a href="./<%=location%>" target="_new"><%=location%></a><br>
				<%
					}
				%>
				</td>
				<td class="text" align="left"><%=Initializer.bpmAccessConfigurationsLoaded%></td>
			</tr>
			<tr class="rowOdd">
				<td width="250" class="textLabel">WorkflowConfigLocation</td>
				<td class="text" align="left">
				<% 
					iter = Initializer.workflowAccessConfigLocations.iterator();
					while(iter.hasNext()){
						String location = (String)iter.next();
				%>
					<a href="./<%=location%>" target="_new"><%=location%></a><br>
				<%
					}
				%>
				</td>
				<td class="text" align="left"><%=Initializer.workflowAccessConfigLoaded%></td>
			</tr>
			<tr class="rowEven">
				<td width="250" class="textLabel">AuthorizationConfigLocation</td>
				<td class="text" align="left">
				<% 
					iter = Initializer.authorizationAccessConfigLocations.iterator();
					while(iter.hasNext()){
						String location = (String)iter.next();
				%>
					<a href="./<%=location%>" target="_new"><%=location%></a><br>
				<%
					}
				%>
				</td>
				<td class="text" align="left"><%=Initializer.authorizationAccessConfigLoaded%></td>
			</tr></table>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
</table>
</div>
</body>
</html>
