<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";
	
	function execute(url){
		top.results.location.href = url;
		location.href = location.href;
	}
	
	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}

	function setErrorSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "showErrorDetail.jsp?key=" + keyName;
			window.open(url,"","location=no,fullscreen=no, menubar=no,resizable=yes,scrollbars=no,status=no,toolbar=no,top=0,left=0,width=930,height=640");
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}

</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">UI Service Action Execution Log</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="100%" class="section1"
			style="text-align: center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<th width="220">Service Action Name</th>
				<th style="text-align:left; width: 350; overflow: hidden;">Implementation Class</th>
				<th width="60" align="center">Success</th>
				<th width="60" align="center">Fail</th>
				<th width="60" align="center">Fastest</th>
				<th width="60" align="center">Slowest</th>
				<th width="60" align="center">Avg</th>
			</tr>
		</table>
		<div class="divTable" style="height: 300px; width:98%;" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<%
				Iterator iter = ServiceDelegator.serviceActionLog.keySet().iterator();
				boolean isOdd = true;
				while(iter.hasNext()){
					String keyName = (String)iter.next();
					ServiceDelegator.ServiceActionLogEntry logEntry = (ServiceDelegator.ServiceActionLogEntry)ServiceDelegator.serviceActionLog.get(keyName);
					String currentStyle = (isOdd ? "rowOdd" : "rowEven");
					isOdd = !isOdd;
			%>
				<tr id="<%=keyName%>" class="<%=currentStyle %>" onclick="setSelection('<%=keyName%>');" style="text-align:left">
					<td class="textLabel" style="text-align:left" width="220" onclick="setSelection('<%=keyName%>');"><div style="text-align:left; width: 220; overflow: hidden;"><%=logEntry.serviceActionName %></div></td>
					<td class="textLabel" onclick="setSelection('<%=keyName%>');"><div style="text-align:left; width: 350; overflow: hidden;"><%=logEntry.implementationClassName %><div></td>
					<td class="textLabel" style="text-align:center;" width="60" onclick="setSelection('<%=keyName%>');"><%=logEntry.numSuccessfulExecutions %></td>
					<td class="textLabel" style="text-align:center" width="60" onclick="setErrorSelection('<%=keyName%>');"><%=logEntry.numFailedExecutions %></td>
					<td class="textLabel" style="text-align:center" width="60" onclick="setSelection('<%=keyName%>');"><%=logEntry.fastestTime %></td>
					<td class="textLabel" style="text-align:center" width="60" onclick="setSelection('<%=keyName%>');"><%=logEntry.slowestTime %></td>
					<td class="textLabel" style="text-align:center" width="60" onclick="setSelection('<%=keyName%>');"><%=logEntry.getAvg() %></td>
				</tr>
			<%
				}
			%>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
</table>
</div>
</body>
</html>
