<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.awd.*"%>
<%@ page import="java.util.*"%>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if(i >= 0){
	keyName = keyName.substring(i + 4);
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
</head>
<body class="desktopBody" onload="top.content.file.location.href = top.content.file.location.href;">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<% if(AWDAccess.killSession(keyName)){
				%>
					<td align="left" class="subTextTitle">AWD sessions for User [<%=keyName %>] have been terminated.</td>
				<%
					} else {
				%>
					<td align="left" class="subTextTitle">Failed to terminate AWD sessions for User [<%=keyName %>].</td>
				<%
					}
				%>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" ></td>
	</tr>
	<tr>
		<td height="15" colspan="2"></td>
	</tr>
</table>
</div>
</body>
</html>
