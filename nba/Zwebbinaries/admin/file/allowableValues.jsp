<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";
	
	function execute(url){
		top.results.location.href = url;
		location.href = location.href;
	}
	
	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "allowableValueDetail.jsp?key=" + keyName;
			valuesDetail.location.href = url;
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
	function clearSelected(){
		execute("../../servlet/Initializer?mode=CLEAR_DOMAIN&domain=" + lastKey);
	}
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<%
		List currentItems = DomainDataAccess.Factory.getInstance().getLoadedLists();
	%>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Allowable Values Lists</td>
				<td align="right" class="textLabel">
				Number Loaded Lists: <%=currentItems.size() %>
				<button class="button" style="width:120px"
					onclick="execute('../../servlet/Initializer?mode=CLEAR_DOMAINS');">Clear Domain Data</button>
				<button class="button" style="width:120px"
					onclick="clearSelected();">Clear Selected</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="100%" class="section1"
			style="text-align: center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<th>Loaded Domain Identifier</th>
			</tr>
		</table>
		<div class="divTable" style="height: 80px; width:98%;" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<%
				Iterator iter = currentItems.iterator();
				boolean isOdd = true;
				while(iter.hasNext()){
					String keyName = (String)iter.next();
					String currentStyle = (isOdd ? "rowOdd" : "rowEven");
					isOdd = !isOdd;
			%>
				<tr id="<%=keyName%>" class="<%=currentStyle %>" onclick="setSelection('<%=keyName%>');" style="text-align:left">
					<td id="<%=keyName%>Text" class="textLabel" style="text-align:left" onclick="setSelection('<%=keyName%>');"><%=keyName %></td>
				</tr>
			<%
				}
			%>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td>
			<iframe id="valuesDetail" name="valuesDetail" width="100%" height="220" frameborder="0" src=""></iframe>
		</td>
	</tr>
</table>
</div>
</body>
</html>
