<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.awd.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if(i >= 0){
	keyName = keyName.substring(i + 4);
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}
</script>
</head>
<body class="desktopBody" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
			<div class="subTextTitle">Log for User <%=keyName %></div>
		</td>
	</tr>
	<tr>
		<td valign="top" class="section1" style="text-align: center">
			<table width="98%" cellpadding="0" cellspacing="0">
				<tr class="header">
					<th width="80">Date/Time</th>
					<th width="340">Request</th>
					<th width="340">Response</th>
					<th width="150">Source IP</th>
					<th width="80">Time</th>
				</tr>
			</table>
			<div class="divTable" style="height: 155px; width:98%;" >
				<table width="100%" cellpadding="0" cellspacing="0">
					<%
						List currentItems = AWDAccess.getViewLogTable(keyName);
						Iterator iter = currentItems.iterator();
						boolean isOdd = true;
						while(iter.hasNext()){
							String[] newItem = (String[])iter.next();
							String currentStyle = (isOdd ? "rowOdd" : "rowEven");
							isOdd = !isOdd;
					%>
						<tr class="<%=currentStyle %>" style="text-align:left">
							<td width="150" class="textLabel"><%=newItem[0] %></td>
							<td class="textLabel" style="text-align:left" style="height: 150px; width:300px;">
							<div class="divTable" style="height: 150px; width:300px;" >
								<%=newItem[1] %>
							</div>
							</td>
							<td class="textLabel" style="text-align:left" style="height: 150px; width:300px;">
							<div class="divTable" style="height: 150px; width:300px;" >
								<%=newItem[2] %>
							</div>
							</td>
							<td width="150" class="textLabel" style="text-align:left"><%=newItem[3] %></td>
							<td width="130" class="textLabel" style="text-align:left"><%=newItem[4] %></td>
						</tr>
					<%
						}
					%>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
</table>
</body>
</html>
