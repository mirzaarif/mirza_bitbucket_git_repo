<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.Initializer"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top">
<table cellpadding="0" cellspacing="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Logging Details</td>
				<td align="right">
				<button class="button"
					onclick="window.open('../../servlet/Initializer?mode=GET_LOG');">View
				Log</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="section1">
		<table>
			<tr>
				<td class="textLabel">Current Logging Level:</td>
				<td><SELECT id="logLevel" class="textInput">
					<OPTION value="0x00"
						<%if (LogHandler.Factory.getLogLevel().equals("0x00")) {%>
						selected <%}%>>No Logging</OPTION>
					<OPTION value="0x01"
						<%if (LogHandler.Factory.getLogLevel().equals("0x01")) {%>
						selected <%}%>>Level 1:Errors and Times</OPTION>
					<OPTION value="0x02"
						<%if (LogHandler.Factory.getLogLevel().equals("0x02")) {%>
						selected <%}%>>Level 2:Level 1 + Warning</OPTION>
					<OPTION value="0x03"
						<%if (LogHandler.Factory.getLogLevel().equals("0x03")) {%>
						selected <%}%>>Level 3:Level 2 + Information</OPTION>
					<OPTION value="0x04"
						<%if (LogHandler.Factory.getLogLevel().equals("0x04")) {%>
						selected <%}%>>Level 4:Level 3 + Procedure/Method entry/Exit</OPTION>
					<OPTION value="0x05"
						<%if (LogHandler.Factory.getLogLevel().equals("0x05")) {%>
						selected <%}%>>Level 5:Level 4 + XML</OPTION>
					<OPTION value="0x06"
						<%if (LogHandler.Factory.getLogLevel().equals("0x06")) {%>
						selected <%}%>>Level 6:Level 5 + Debug</OPTION>
					<OPTION value="0x07"
						<%if (LogHandler.Factory.getLogLevel().equals("0x07")) {%>
						selected <%}%>>Level 7:Level 6 + Low Level Debug</OPTION>
					<OPTION value="0x99"
						<%if (LogHandler.Factory.getLogLevel().equals("0x99")) {%>
						selected <%}%>>Full Logging</OPTION>
				</SELECT></td>
				<td>
				<button class="button"
					onclick="execute('../../servlet/Initializer?mode=SET_LOG&level=' + logLevel.value);">Set
				Level</Button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Log Elements</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="section1">
		<table>
			<tr>
			<td width="50%">
			<table>
				<tr>
					<td class="textLabel" width="220">Debug</td>
					<td align="left" class="text" ><%=LogHandler.Factory.isLogging(LogHandler.LOG_DEBUG)%></td>
				</tr>
				<tr>
					<td align="left" class="textLabel">Error</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_ERROR)%></td>
				</tr>
				<tr>
					<td align="left" class="textLabel">Info</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_INFO)%></td>
				</tr>
				<tr>
					<td align="left" class="textLabel">Transaction</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_TXN)%></td>
				</tr>
				<tr>
					<td align="left" class="textLabel">XML</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_XML)%></td>
				</tr>
			</table>
			</td>
			<td width="50%">
			<table>
				<tr>
					<td class="textLabel" align="left">Transaction Performance</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_TXN_PERF)%></td>
				</tr>
				<tr>
					<td class="textLabel" align="left">Low Level Debug</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_LOW_LEVEL_DEBUG)%></td>
				</tr>
				<tr>
					<td class="textLabel" align="left">Warning</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_WARNING)%></td>
				</tr>
				<tr>
					<td class="textLabel" align="left">Process Stack Logging</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_PROCESS)%></td>
				</tr>
				<tr>
					<td class="textLabel" align="left">Entitlement
					(Enablement/Visibility) Logging</td>
					<td align="left" class="text"><%=LogHandler.Factory.isLogging(LogHandler.LOG_ENTITLEMENT)%></td>
				</tr></table>
			</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
</table>
</div>
</body>
</html>
