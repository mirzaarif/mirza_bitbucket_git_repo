<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Workflow File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var context = '<%=path%>';
		var topOffset=10;
	//-->
	</script>
</head>
<body class="desktopBody">
	<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="1">
		<tbody>
			<tr style="height:100px;" class="textMainTitleBar">
				<td><FileLoader:Files location="admin/file/" numTabsPerRow="10" /></td>
			</tr>
			<tr>
				<td><iframe class="configurationFile" name="file" src="" style="width:100%;height:800px" scrolling="no" frameborder="0" ></iframe></td>
			</tr>
		</tbody>
	</table>
</f:view>
</body>
</html>
