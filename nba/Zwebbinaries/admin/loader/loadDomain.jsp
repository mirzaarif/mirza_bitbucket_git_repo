<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.loader.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String keyName = request.getQueryString();
int j = keyName.indexOf("key=");
if(j >= 0){
	keyName = keyName.substring(j + 4);
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
</head>
<body class="desktopBody" onload="top.content.file.location.href = top.content.file.location.href;">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<% 
					StringBuffer sb = new StringBuffer();
					if(keyName.equals("ALL")){
						URL dataDir = Thread.currentThread().getContextClassLoader().getResource("/rf/core");
						File target = new File(dataDir.getFile().substring(1));
						boolean isOdd = true;
						if(target != null){
							if(target.exists() && target.isDirectory()){
								String[] children = target.list();
								for(int i = 0; i < children.length; i++){
									if(children[i].toUpperCase().endsWith("XML")){
										if(DataLoader.Factory.process(children[i].substring(0, children[i].length()-4), null)){
											sb.append(children[i]).append(" has been successfully imported").append(".<br>");
										} else {
											sb.append(children[i]).append(" failed to import [").append(DataLoader.Factory.getLastError()).append("].<br>");
										}
									}
								}
							}
						}
					} else {
						if(DataLoader.Factory.process(keyName.substring(0, keyName.length()-4), null)){
							sb.append(keyName).append(" has been successfully imported").append(".<br>");
						} else {
							sb.append(keyName).append(" failed to import [").append(DataLoader.Factory.getLastError()).append("].<br>");
						}
					}
				%>
					<td align="left" class="subTextTitle">Import of [<%=keyName %>] completed.</td>
			</tr>
			<tr>
					<td align="left" class="text"><%=sb.toString()%></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" ></td>
	</tr>
	<tr>
		<td height="15" colspan="2"></td>
	</tr>
</table>
</div>
</body>
</html>
