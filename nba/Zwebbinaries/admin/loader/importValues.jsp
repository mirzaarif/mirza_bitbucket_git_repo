<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";
	
	function execute(url){
  		top.results.document.write(
			'<HTML><HEAD>' +
			'<LINK REL="stylesheet" TYPE="text/css" HREF="<%=basePath%>css/accelerator.css">' +
			'</HEAD>' +
			'<BODY class="mainBody" leftmargin="0" topmargin="0" >' +
				'<table width="100%"><tr><td align="center" class="textWait">' +
						'<div align="center">' +
							'<br ID="imgLocation"/>' +
							'<IMG src="<%=basePath%>images/wait.gif" />' +
							'<br/>' +
							'<P/>' +
							'Please wait loading file...' +
						'</div>' +
				'</td></tr></table>' +
			'</BODY></HTML>');
		top.results.location.href = url;
		location.href = location.href;
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
	function loadDomain(domainName){
		execute('loadDomain.jsp?key=' + domainName);
	}
	
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<%
		List currentItems = DomainDataAccess.Factory.getInstance().getLoadedLists();
	%>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Allowable Values Lists</td>
				<td align="right" class="textLabel">
				<button class="button" style="width:150px"
					onclick="loadDomain('ALL');">Import ALL Domain Data</button>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="100%" class="section1"
			style="text-align: center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<th>Domain File Name</th>
			</tr>
		</table>
		<div class="divTable" style="height: 310px; width:98%;" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<%
				URL dataDir = Thread.currentThread().getContextClassLoader().getResource("/rf/core");
				File target = new File(dataDir.getFile().substring(1));
				boolean isOdd = true;
				if(target != null){
					if(target.exists() && target.isDirectory()){
						String[] children = target.list();
						for(int i = 0; i < children.length; i++){
							if(children[i].toUpperCase().endsWith("XML")){
								String keyName = children[i];
								String currentStyle = (isOdd ? "rowOdd" : "rowEven");
								isOdd = !isOdd;
			%>
				<tr id="<%=keyName%>" class="<%=currentStyle %>" onclick="setSelection('<%=keyName%>');" style="text-align:left">
					<td id="<%=keyName%>Text" class="textLabel" style="text-align:left"><%=keyName %></td><td align="right"><button class="buttonTiny" onclick="loadDomain('<%=keyName%>');">load</button></td>
				</tr>
			<%
							}
						}
					}
				}
			%>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
</table>
</div>
</body>
</html>
