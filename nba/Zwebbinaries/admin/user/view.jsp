<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Workflow File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var context = '<%=path%>';
	//-->
	</script>
</head>
<body class="desktopBody">
	<table height="100%" width="100%">
		<tbody>
			<tr>
				<td><FileLoader:Files location="admin/user/file/" numTabsPerRow="10"/></td>
			</tr>
			<tr>
				<td><iframe class="configurationFile" name="file" src="" style="width:100%;height:800" scrolling="no" frameborder="0"></iframe></td>
			</tr>
		</tbody>
	</table>
</f:view>
</body>
</html>
