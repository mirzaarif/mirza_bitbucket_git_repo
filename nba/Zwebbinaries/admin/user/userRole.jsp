<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if (i >= 0) {
	keyName = keyName.substring(i + 4);
}

String description = "";

%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";

	function execute(url){
		top.results.location.href = url;
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "userRole.jsp?key=" + keyName;
			roleDetail.location.href = url;
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
</script>
</head>
<body class="desktopBody">
<FORM action="<%=basePath%>servlet/UserAdmin" method="post" target="results">
<table width="101%" cellpadding="0" cellspacing="0" class="section1">
	<tr>
		<td bgcolor="#000000" height="1" colspan="3"></td>
	</tr>
	<tr>
		<td align="left" class="subTextTitle" >
		<% if(keyName != null && !keyName.equals("")){
			Attributes atts = (Attributes)UserAccess.allRoles.get(keyName);
			if(atts != null){
				Attribute roleDescAtt = atts.get("description");
				description = (String)roleDescAtt.get();
			}
		%>
			Update Role [<%=keyName %>]
			<INPUT type="hidden" name="operation" value="UPDATE_ROLE"></INPUT>		
		<% } else { %>
			Add New Role
			<INPUT type="hidden" name="operation" value="ADD_ROLE"></INPUT>		
		<% } %>
		<INPUT type="hidden" name="RoleName" value="<%=keyName%>"></INPUT>		
		</td>
	</tr>
	<%
	%>

	<tr>
		<td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLabel">Role Name:</td>
				<td class="textInput">
				<INPUT type="text" name="newRoleName" value="<%=keyName%>" class="textInput"></INPUT>
				</td>
			</tr>
			<tr>
				<td class="textLabel">Description:</td>
				<td class="textInput">
				<INPUT type="text" name="newRoleDescription" value="<%=description%>" class="textInput"></INPUT>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<BUTTON class="button" type="submit">OK</BUTTON>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
</table>
</FORM>
</body>
</html>
