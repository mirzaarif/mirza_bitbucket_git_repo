<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";

	function execute(url){
		top.results.location.href = url;
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "userDetail.jsp?key=" + keyName;
			userDetail.location.href = url;
			updateUserDetail.location.href="blank.html";
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
	function updateUser(){
		updateUserDetail.location.href = "updateUserDetail.jsp?key=" + lastKey;
	}

	function deleteUser(){
		updateUserDetail.location.href = "deleteUserDetail.jsp?key=" + lastKey;
	}

	
	function addUser(){
		updateUserDetail.location.href = "addUserDetail.jsp";
	}
	
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" colspan="2"></td>
	</tr>
	<%
		UserAccess profileLoader = new UserAccess();
		List errors = new ArrayList();
		List currentItems = profileLoader.getProfiles(null, null, errors);
		Collections.sort(currentItems);
	%>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Current Users</td>
				<td align="right">
				<button class="button"
					onclick="deleteUser();">Remove</button>
				</td>
			</tr>
		</table>
		</td>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Detail</td>
				<td align="right">
				<button class="button"
					onclick="addUser();">Add</button>
				<button class="button"
					onclick="updateUser();">Update</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" width="250" class="section1" style="text-align: center">
			<table width="98%" cellpadding="0" cellspacing="0">
				<tr>
					<th class="headerRow"><span class="header">Users</span></th>
				</tr>
			</table>
			<div class="divTable" style="height: 260px; width:98%;" >
				<table class="complexTable" width="100%" cellpadding="0" cellspacing="0" border="1">
				<%
					Iterator iter = currentItems.iterator();
					while(iter.hasNext()){
						String keyName = (String)iter.next().toString();
				%>
					<tr id="<%=keyName%>" class="row" onclick="setSelection('<%=keyName%>');" style="text-align:left">
						<td id="<%=keyName%>Text" class="column" style="text-align:left" onclick="setSelection('<%=keyName%>');"><%=keyName %></td>
					</tr>
				<%
					}
				%>
				</table>
			</div>
		</td>
		<td class="section1" style="text-align: center" width="*">
			<iframe id="userDetail" name="userDetail" width="100%" height="280" frameborder="0" src=""></iframe>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1" colspan="2"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" colspan="2"></td>
	</tr>
	<tr>
		<td height="15" colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe id="updateUserDetail" name="updateUserDetail" width="100%" height="320" frameborder="0" src=""></iframe>
		</td>
	</tr>
</table>
</div>
</body>
</html>
