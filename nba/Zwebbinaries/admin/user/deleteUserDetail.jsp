<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if (i >= 0) {
	keyName = keyName.substring(i + 4);
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}

</script>
</head>
<body topmargin="0" leftmargin="0">
<%
	Attributes values = (Attributes) UserAccess.currentUserDetails.get(keyName);
	Attribute value = null;
%>
<FORM action="<%=basePath%>servlet/UserAdmin" method="post"
	target="results">
<table width="101%" cellpadding="0" cellspacing="0" class="section1">
	<tr>
		<td bgcolor="#000000" height="1" colspan="3"></td>
	</tr>
	<tr>
		<td align="left" class="subTextTitle" colspan="2">DELETE User <%=keyName %>
		<INPUT type="hidden" name="MainUserId" value="<%=keyName%>"></INPUT> <INPUT
			type="hidden" name="operation" value="DELETE_USER"></INPUT></td>
	</tr>
	<tr>
		<td colspan="2" align="right">
		<BUTTON class="button" type="submit">OK</BUTTON>
		</td>
	</tr>
</table>
</FORM>
</body>
</html>
