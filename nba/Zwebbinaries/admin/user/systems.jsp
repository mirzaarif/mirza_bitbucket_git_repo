<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";

	function execute(url){
		top.results.location.href = url;
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			systemDetail.location.href = "systemDetail.jsp?key=" + keyName;
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}

	function addSystem(){
		try{
			systemDetail.location.href = "systemDetail.jsp?key=";
		} catch(err){
			top.reportException(err,"system");
		}
	}
	
</script>
</head>
<body class="desktopBody">
<div style="text-align: center; vertical-align: top;">
<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" colspan="2"></td>
	</tr>
	<%
		UserAccess profileLoader = new UserAccess();
		List errors = new ArrayList();
		Map currentItems = profileLoader.getAllSystems(errors);
	%>
	<tr>
		<td class="section1">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="subTextTitle">Current Systems</td>
				<td align="right">
					<button class="button" onclick="addSystem();">Add</button>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top" class="section1" style="text-align: center">
			<table width="98%" cellpadding="0" cellspacing="0">
				<tr class="header">
					<th width="*">System Name</th>
				</tr>
			</table>
			<div class="divTable" style="height: 80px; width:98%;" >
				<table width="100%" cellpadding="0" cellspacing="0">
				<%
					Iterator iter = currentItems.keySet().iterator();
					boolean isOdd = true;
					while(iter.hasNext()){
						String currentStyle = (isOdd ? "rowOdd" : "rowEven");
						isOdd = !isOdd;
						Attributes atts = (Attributes)currentItems.get(iter.next());
						Attribute roleNameAtt = atts.get("sn");
						String keyName = (String)roleNameAtt.get();
				%>
					<tr id="<%=keyName%>" class="<%=currentStyle %>" onclick="setSelection('<%=keyName%>');" style="text-align:left">
						<td id="<%=keyName%>Text" class="textLabel" style="text-align:left" onclick="setSelection('<%=keyName%>');" width="*"><%=keyName %></td>
					</tr>
				<%
					}
				%>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td height="5" class="section1" colspan="2"></td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1" colspan="2"></td>
	</tr>
	<tr>
		<td height="15" colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe id="systemDetail" name="systemDetail" width="100%" height="220" frameborder="0" src=""></iframe>
		</td>
	</tr>
</table>
</div>
</body>
</html>
