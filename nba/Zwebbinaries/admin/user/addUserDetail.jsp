<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}

	function setupSystem(object){
	    if(object != null){
	        var systemName = object.id;
	        systemName = systemName.substr(systemName.indexOf(":")+1);
	        if(object.checked){
	            var genCred = window.document.getElementById("GEN_CRED:" + systemName).checked;
	            try{
	                if(genCred){
	                    window.document.getElementById("GEN_CRED:" + systemName).disabled = false;
	                    window.document.getElementById("USERID:" + systemName).disabled = true;
	                    window.document.getElementById("PASSWORD:" + systemName).disabled = true;
	                    window.document.getElementById("LOGON_MECHANISM:" + systemName).disabled = true;
	                } else {
	                    window.document.getElementById("GEN_CRED:" + systemName).disabled = false;
	                    window.document.getElementById("USERID:" + systemName).disabled = false;
	                    window.document.getElementById("PASSWORD:" + systemName).disabled = false;
	                    window.document.getElementById("LOGON_MECHANISM:" + systemName).disabled = false;
	                }
	            }catch(err){
	            }
	        } else {
                window.document.getElementById("GEN_CRED:" + systemName).disabled = true;
                window.document.getElementById("USERID:" + systemName).disabled = true;
                window.document.getElementById("PASSWORD:" + systemName).disabled = true;
                window.document.getElementById("LOGON_MECHANISM:" + systemName).disabled = true;
	        }
	    }
	}
	
	function setupCredential(object){
	    if(object != null){
	        var systemName = object.id;
	        systemName = systemName.substr(systemName.indexOf(":")+1);
	        try{
		        if(object.checked){
					window.document.getElementById("GEN_CRED:" + systemName).disabled = false;
					window.document.getElementById("USERID:" + systemName).disabled = true;
					window.document.getElementById("PASSWORD:" + systemName).disabled = true;
					window.document.getElementById("LOGON_MECHANISM:" + systemName).disabled = true;
		        } else {
	                window.document.getElementById("GEN_CRED:" + systemName).disabled = false;
	                window.document.getElementById("USERID:" + systemName).disabled = false;
	                window.document.getElementById("PASSWORD:" + systemName).disabled = false;
	                window.document.getElementById("LOGON_MECHANISM:" + systemName).disabled = false;
		        }
	        }catch(err){
	        }
	    }
	}
	
</script>
</head>
<body topmargin="0" leftmargin="0">
<FORM action="<%=basePath%>servlet/UserAdmin" method="post"
	target="results">
<table width="101%" cellpadding="0" cellspacing="0" class="section1">
	<tr>
		<td bgcolor="#000000" height="1" colspan="3"></td>
	</tr>
	<tr>
		<td align="left" class="subTextTitle" colspan="2">Add User
		<INPUT type="hidden" name="operation" value="ADD_USER"></INPUT></td>
	</tr>

	<tr>
		<td width="250" valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLabel">User Name:</td>
				<td class="textInput">
				<INPUT type="text" name="cn" value=""
					class="textInput"></INPUT></td>
			</tr>
			<tr>
				<td class="textLabel">Password:</td>
				<td class="textInput"><INPUT type="password" name="MainPassword"
					value="" class="textInput"></INPUT></td>
			</tr>
			<tr>
				<td class="textLabel">Last Name:</td>
				<td class="textInput">
				<INPUT type="text" name="sn" value=""class="textInput"></INPUT></td>
			</tr>
		</table>
		<p>
		<div align="center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<td align="left" colspan="2" height="15">Roles/Groups</td>
			</tr>
			<tr>
				<td>
				<div class="divTable"
					style="height: 135px; width: 100%; border: none">
				<table width="100%" cellpadding="0" cellspacing="0">
					<%	Iterator rolesiter = UserAccess.allRoles.keySet().iterator();
						boolean isOdd = true;
						while (rolesiter.hasNext()) {
							String currentStyle = (isOdd ? "rowOdd" : "rowEven");
							isOdd = !isOdd;
							String roleName = (String)rolesiter.next();
					%>
					<tr class="<%=currentStyle %>">
						<td align="left" class="text" colspan="2"><INPUT
							name="ROLE:<%=roleName%>" type="checkbox"></td>
						<td align="left" class="text" colspan="2"><%=roleName%></td>
					</tr>
					<%	}%>
				</table>
				</div>
				</td>
			</tr>
		</table>
		</div>
		</td>
		<td>
		<%
			UserAccess profileLoader = new UserAccess();
			List errors = new ArrayList();
			Map currentItems = profileLoader.getAllSystems(errors);
		%>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<td align="left" colspan="2" height="15">Systems</td>
			</tr>
			<tr>
				<td>
				<div class="divTable"
					style="height: 240px; width: 100%; border: none">
				<table width="100%" cellpadding="0" cellspacing="0">
					<%
				
					Iterator iter = currentItems.keySet().iterator();
					while(iter.hasNext()){
						String currentStyle = (isOdd ? "rowOdd" : "rowEven");
						isOdd = !isOdd;
						String sysName = (String) iter.next();
					%>
					<tr class="<%=currentStyle %>">
						<td align="left" class="text"><INPUT id="SYSTEM:<%=sysName%>" name="SYSTEM:<%=sysName%>"
							type="checkbox" onclick="setupSystem(this);"> <%=sysName%></td>
						<td class="textLabel">
							Generic System Credential:
							<INPUT id="GEN_CRED:<%=sysName%>" name="GEN_CRED:<%=sysName%>"
							type="checkbox" onclick="setupCredential(this);" 
							disabled="disabled"></td>
						<td class="textLabel">User ID: <INPUT id="USERID:<%=sysName %>" name="USERID:<%=sysName %>" type="text"
							value="" disabled="disabled" >
						</td>
						<td class="textLabel">Password: <INPUT id="PASSWORD:<%=sysName %>" name="PASSWORD:<%=sysName %>" type="text"
							value="" disabled="disabled" >
						</td>
						<td class="textLabel">Mandatory Logon: <SELECT id="LOGON_MECHANISM:<%=sysName %>" name="LOGON_MECHANISM:<%=sysName %>" disabled="disabled">
							<OPTION label="Yes" value="Yes">Yes</OPTION>
							<OPTION label="No" value="No">No</OPTION>
						</SELECT>
						</td>
					</tr>
					<%
					}
				%>
				</table>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right">
		<BUTTON class="button" type="submit">OK</BUTTON>
		</td>
	</tr>
</table>
</FORM>
</body>
</html>
