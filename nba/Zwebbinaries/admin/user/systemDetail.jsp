<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if (i >= 0) {
	keyName = keyName.substring(i + 4);
}

String description = "";

%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	var lastKey = "";
	var lastKeyStyle = "";

	function execute(url){
		top.results.location.href = url;
	}

	function setSelection(keyName){
		try{
			if(lastKey != null && lastKey != ""){
				window.document.all[lastKey].className = lastKeyStyle;
			}
			lastKeyStyle = document.all[keyName].className;
			lastKey = keyName;
			document.all[keyName].className = "highlight";
			var url = "systemDetail.jsp?key=" + keyName;
			systemDetail.location.href = url;
		} catch(err){
			top.reportException(err,"setSelection");
		}
	}
	
</script>
</head>
<body class="desktopBody">
<FORM action="<%=basePath%>servlet/UserAdmin" method="post" target="results">
<table width="101%" cellpadding="0" cellspacing="0" class="section1">
	<tr>
		<td bgcolor="#000000" height="1" colspan="3"></td>
	</tr>
	<tr>
		<td align="left" class="subTextTitle" >
		<% 
		Attributes sysatts = null;
			if(keyName != null && !keyName.equals("")){
			Attributes atts = (Attributes)UserAccess.allSystems.get(keyName);
			UserAccess uac = new UserAccess();
			sysatts = uac.getGenericSystem(keyName);
		%>
			Update System [<%=keyName %>]
			<INPUT type="hidden" name="operation" value="UPDATE_SYSTEM"></INPUT>		
		<% } else { %>
			Add New System
			<INPUT type="hidden" name="operation" value="ADD_SYSTEM"></INPUT>		
		<% } %>
		<INPUT type="hidden" name="SystemName" value="<%=keyName%>"></INPUT>		
		</td>
	</tr>
	<%
	%>

	<tr>
		<td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLabel">System Name:</td>
				<td class="textInput">
				<INPUT type="text" name="newSystemName" value="<%=keyName%>" class="textInput"></INPUT>
				</td>
			</tr>
			<tr>
				<td class="textLabel">Generic Credential User ID:</td>
				<td class="textInput">
				<%
					String currValue = "";
					if(sysatts != null){
						Attribute itemValue = (Attribute)sysatts.get("userName");
						if(itemValue != null){
						    currValue = (itemValue != null ? (String)itemValue.get() : "");
						}
					}
				%>
				<INPUT type="text" name="User" value="<%=currValue%>" class="textInput"></INPUT>
				</td>
			</tr>
			<tr>
				<td class="textLabel">Generic Credential Password:</td>
				<td class="textInput">
				<%
					if(sysatts != null){
						Attribute itemValue = (Attribute)sysatts.get("userPassword");
						if(itemValue != null){
						    currValue = new String((itemValue != null ? (byte[])itemValue.get() : (new byte[0])));
						}
					}
				%>				
				<INPUT type="text" name="Password" value="<%=currValue%>" class="textInput"></INPUT>
				</td>
			</tr>			
			<tr>
				<td class="textLabel">Generic Credential Mandatory Logon:</td>
				<td class="textInput">
					<%
					if(sysatts != null){
						Attribute itemValue = (Attribute)sysatts.get("LogonMechanism");
					    if(itemValue == null){
					        currValue =(itemValue != null ? (itemValue.get().equals("M") ? "Yes" : "No") : "No");
					    }
					}
					%>
					<SELECT name="LogonMechanism">
						<OPTION label="Yes" <% if(currValue.equals("Yes")){ %>
							selected="selected" <%} %> value="Yes">Yes</OPTION>
						<OPTION label="No" <% if(currValue.equals("No")){ %>
							selected="selected" <%} %> value="No">No</OPTION>
					</SELECT> 
				</td>
			</tr>						
			<tr>
				<td colspan="2" align="right">
					<BUTTON class="button" type="submit">OK</BUTTON>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="#000000" height="1"></td>
	</tr>
</table>
</FORM>
</body>
</html>
