<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ page import="com.csc.fs.accel.ui.log.LogHandler"%>
<%@ page import="com.csc.fs.accel.ui.action.admin.user.*"%>
<%@ page import="javax.naming.directory.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
String keyName = request.getQueryString();
int i = keyName.indexOf("key=");
if (i >= 0) {
	keyName = keyName.substring(i + 4);
}
%>
<base href="<%=basePath%>">
<title>Accelerator Application Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function execute(url){
		top.results.location.href = url;
	}
</script>
</head>
<body class="section1" topmargin="0" leftmargin="0">
<%
	Attributes values = (Attributes) UserAccess.currentUserDetails.get(keyName);
	Attribute value = null;
%>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td width="250" valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="textLabel">User ID:</td>
				<td class="text">
				<%
					value = values.get("cn");
				%>
				<%=value.get()%>
				</td>
			</tr>
			<tr>
				<td class="textLabel">Name:</td>
				<td class="text">
				<%
					value = values.get("sn");
				%>
				<%=value.get()%>
				</td>
			</tr>
		</table>

		<div align="center">
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<td align="left" colspan="2" height="15">Roles/Groups</td>
			</tr>
			<tr>
				<td>
				<div class="divTable" style="height: 200px; width: 100%;">
				<table width="100%" cellpadding="0" cellspacing="0">
					<%	List roles = (List) UserAccess.currentUserRoles.get(keyName);
						Iterator rolesiter = roles.iterator();
						boolean isOdd = true;
						while (rolesiter.hasNext()) {
							String currentStyle = (isOdd ? "rowOdd" : "rowEven");
							isOdd = !isOdd;
							String roleName = (String)rolesiter.next();
					%>
					<tr class="<%=currentStyle %>">
						<td align="left" class="text" colspan="2"><%=roleName%></td>
					</tr>
					<%	}%>
				</table>
				</div>
				</td>
			</tr>
		</table>
		</div>
		
		<td>
		
		<td width="*" valign="top">
		
		<%
			UserAccess profileLoader = new UserAccess();
			List errors = new ArrayList();
			//Map currentItems = profileLoader.getAllSystems(errors);
			Map currentItems = (Map) UserAccess.currentUserSystems.get(keyName);
		%>		
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr class="header">
				<td align="left" colspan="2" height="15">Systems</td>
			</tr>
			<tr>
				<td>
				<div class="divTable" style="height: 250px; width: 100%;">
				<table width="100%" cellpadding="0" cellspacing="0">
				<%
				
					Iterator iter = currentItems.keySet().iterator();
					while(iter.hasNext()){
						String currentStyle = (isOdd ? "rowOdd" : "rowEven");
						isOdd = !isOdd;
						String sysName = (String) iter.next();
						Map sysvals = (Map) currentItems.get(sysName);
					%>
					<tr class="<%=currentStyle %>">
						<td align="left" class="text">
							<%=sysName%>
						</td>
						<%
						Attribute itemValue = (Attribute)sysvals.get("cn");
						if(itemValue != null && itemValue.get().equals(sysName)){
						    %>
								<td class="textLabel" colspan="3">Generic Credential</td>
						    <%
						} else if(itemValue != null){
						    itemValue = (Attribute)sysvals.get("userName");
						    String userID = (itemValue != null ? (String)itemValue.get() : "");
						    itemValue = (Attribute)sysvals.get("userPassword");
						    String password = new String((itemValue != null ? (byte[])itemValue.get() : (new byte[0])));
						    Attribute logonmechanism = (Attribute)sysvals.get("LogonMechanism");
						    if(logonmechanism == null){
						        logonmechanism = (Attribute)sysvals.get("logonmechanism");
						    }
						    String logon =(logonmechanism != null ? (logonmechanism.get().equals("M") ? "Yes" : "No") : "No");
						%>
								<td class="textLabel">User ID:<%=userID%></td>
								<td class="textLabel">Password:<%=password%></td>
								<td class="textLabel">Logon Required:<%=logon%></td>
						<%
						} else {
						%>
							<td class="textLabel" colspan="3"></td>
						<%
						}
						%>
					</tr>
				<%
				}
				%>					
				</table>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>
