<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA352         NB-1501   MIB Plan F Information Received Notification --%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGroup id="planFResultHeader" styleClass="ovDivTableHeader" style="width: 650px">
		<h:panelGrid id="planFTableHeader" columns="7" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrDate,ovColHdrText140,ovColHdrText120,ovColHdrText65,ovColHdrText85,ovColHdrText75,ovColHdrText65">
			<h:commandLink id="resultHdrCol1" value="#{property.mpfResultCol1}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol1}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol2" value="#{property.mpfResultCol2}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol2}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol3" value="#{property.mpfResultCol3}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol3}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol4" value="#{property.mpfResultCol4}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol4}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol5" value="#{property.mpfResultCol5}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol5}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol6" value="#{property.mpfResultCol6}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol6}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
			<h:commandLink id="resultHdrCol7" value="#{property.mpfResultCol7}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_mibPlanFInformationResultTable.sortedByCol7}" actionListener="#{pc_mibPlanFInformationResultTable.sortColumn}" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="planFResultData" styleClass="ovDivTableData" style="height: 380px; width: 650px">
		<h:dataTable id="planFResultTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_mibPlanFInformationResultTable.dataTable}" value="#{pc_mibPlanFInformationResultTable.results}" var="result"
					rowClasses="#{pc_mibPlanFInformationResultTable.rowStyles}" 
					columnClasses="ovColDate,ovColText140,ovColText120,ovColText65,ovColText85,ovColText75,ovColText65" >
			<h:column>
				<h:commandLink id="resultCol1" title="" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
								styleClass="ovFullCellSelect" style="height: 100%;" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true" >
					<h:outputText id="resultCreateDate" value="#{result.createDate}">
					  <f:convertDateTime pattern="#{property.datePattern}"/>
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol2" title="#{result.name}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
					<h:inputTextarea id="resultName" readonly="true" value="#{result.name}" styleClass="ovMultiLine#{result.draftText}" style="width: 140px" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol3" title="#{result.contractNumber}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
					<h:outputText id="resultContractNumber" value="#{result.contractNumber}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol4" title="#{result.nbAApproved}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
					<h:outputText id="resultnbAApproved" value="#{result.nbAApproved}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol5" title="#{result.matchStatus}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
					<h:outputText id="resultMatch" value="#{result.matchStatus}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol6" title="#{result.examinedBy}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
					<h:outputText id="resultExaminedBy" value="#{result.examinedBy}" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:selectBooleanCheckbox id="examinedCB" value="#{result.examined}" styleClass="ovFullCellSelectCheckBox" immediate="true" onclick="resetTargetFrame();submit();" valueChangeListener="#{result.examinedChanged}"/>
				<h:commandLink id="resultCol7" title="" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							action="#{pc_mibPlanFInformationResultTable.selectSingleRow}" immediate="true">
				</h:commandLink>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup id="buttnBar" styleClass="ovButtonBar">
		<h:commandButton id="btnResultViewDetails" value="#{property.mpfButtonViewDetails}" styleClass="ovButtonRight-1"
						onclick="setTargetFrame();" action="#{pc_mibPlanFInformationResultTable.viewMIBPlanFDetails}" disabled="#{pc_mibPlanFInformationResultTable.viewDetailsDisabled}" />
		<h:commandButton id="btnOpenContract" value="#{property.buttonOpen}" styleClass="ovButtonRight"
						onclick="setTargetFrame();" action="#{pc_mibPlanFInformationResultTable.openContract}" disabled="#{pc_mibPlanFInformationResultTable.openDisabled}" />
	</h:panelGroup>
	<h:panelGroup id="statusBarGrp" styleClass="ovStatusBar">
		<h:commandLink id="statusBarGrp1" value="#{property.previousAbsolute}" rendered="#{pc_mibPlanFInformationResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.previousPageAbsolute}" />
		<h:commandLink id="statusBarGrp2" value="#{property.previousPage}" rendered="#{pc_mibPlanFInformationResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.previousPage}" />
		<h:commandLink id="statusBarGrp3" value="#{property.previousPageSet}" rendered="#{pc_mibPlanFInformationResultTable.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.previousPageSet}" />
		<h:commandLink id="statusBarGrp4" value="#{pc_mibPlanFInformationResultTable.page1Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page1}" />
		<h:outputText id="resultPageNumTxt1" value="#{pc_mibPlanFInformationResultTable.page1Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp5" value="#{pc_mibPlanFInformationResultTable.page2Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage2}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page2}" />
		<h:outputText id="resultPageNumTxt2" value="#{pc_mibPlanFInformationResultTable.page2Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp6" value="#{pc_mibPlanFInformationResultTable.page3Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page3}" />
		<h:outputText id="resultPageNumTxt3" value="#{pc_mibPlanFInformationResultTable.page3Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp7" value="#{pc_mibPlanFInformationResultTable.page4Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage4}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page4}" />
		<h:outputText id="resultPageNumTxt4" value="#{pc_mibPlanFInformationResultTable.page4Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp8" value="#{pc_mibPlanFInformationResultTable.page5Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page5}" />
		<h:outputText id="resultPageNumTxt5" value="#{pc_mibPlanFInformationResultTable.page5Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage5}" styleClass="ovStatusBarTextBold" />	
		<h:commandLink id="statusBarGrp9" value="#{pc_mibPlanFInformationResultTable.page6Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page6}" />
		<h:outputText id="resultPageNumTxt6" value="#{pc_mibPlanFInformationResultTable.page6Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage6}" styleClass="ovStatusBarTextBold" />		
		<h:commandLink id="statusBarGrp10" value="#{pc_mibPlanFInformationResultTable.page7Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page7}" />
		<h:outputText id="resultPageNumTxt7" value="#{pc_mibPlanFInformationResultTable.page7Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp11" value="#{pc_mibPlanFInformationResultTable.page8Number}" rendered="#{pc_mibPlanFInformationResultTable.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.page8}" />
		<h:outputText id="resultPageNumTxt8" value="#{pc_mibPlanFInformationResultTable.page8Number}" rendered="#{pc_mibPlanFInformationResultTable.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink id="statusBarGrp12" value="#{property.nextPageSet}" rendered="#{pc_mibPlanFInformationResultTable.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.nextPageSet}" />
		<h:commandLink id="statusBarGrp13" value="#{property.nextPage}" rendered="#{pc_mibPlanFInformationResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.nextPage}" />
		<h:commandLink id="statusBarGrp14" value="#{property.nextAbsolute}" rendered="#{pc_mibPlanFInformationResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_mibPlanFInformationResultTable.nextPageAbsolute}" />
						
	</h:panelGroup>
	<h:inputHidden id="resultTableVScroll" value="#{pc_mibPlanFInformationResultTable.VScrollPosition}" />
	