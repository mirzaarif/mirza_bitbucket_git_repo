<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA352         NB-1501   MIB Plan F Information Received Notification --%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<h:panelGrid id="planFCriteriaTable1" columns="2" cellspacing="0" cellpadding="0">
	<h:panelGroup id="criteriaTable1Row1Column1" styleClass="formDataEntryTopLine">
		<h:outputLabel id="contractNumLbl"
			value="#{property.mibPlanFInformationContractNumber}" styleClass="formLabel" />
		<h:inputText id="contractNumTxt"
			value="#{pc_mibPlanFInformationCriteria.contractNumber}"
			styleClass="formEntryText" style="width: 150px" maxlength="15"
			onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();" onmouseout="enableSearchButton();" onkeydown="enableSearchButton();"/>
	</h:panelGroup>
	<h:panelGroup id="criteriaTable1Row1Column2" styleClass="formDataEntryTopLine">
		<h:outputLabel id="matchStatusLbl" value="#{property.mibPlanFInformationMatchStatus}"
			styleClass="formLabel" />
		<h:selectOneMenu id="matchStatusDD"
			value="#{pc_mibPlanFInformationCriteria.matchStatusCode}"
			styleClass="formEntryText" style="width: 150px"
			onchange="enableSearchButton();">
			<f:selectItems id="matchStatusCodeList"
				value="#{pc_mibPlanFInformationCriteria.matchStatusCodes}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="criteriaTable1Row2Column1" styleClass="formDataEntryLine">
		<h:outputLabel id="lastNameLbl" value="#{property.mibPlanFInformationLastName}"
			styleClass="formLabel" />
		<h:inputText id="lastNameTxt"
			value="#{pc_mibPlanFInformationCriteria.lastName}"
			styleClass="formEntryText" style="width: 150px"
			onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();" onmouseout="enableSearchButton();"/>
	</h:panelGroup>
	<h:panelGroup id="criteriaTable1Row2Column2" styleClass="formDataEntryLine">
		<h:outputLabel id="firstNameLbl" value="#{property.mibPlanFInformationFirstName}"
			styleClass="formLabel" />
		<h:inputText id="firstNameTxt"
			value="#{pc_mibPlanFInformationCriteria.firstName}"
			styleClass="formEntryText" style="width: 150px"
			onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();" onmouseout="enableSearchButton();"/>
	</h:panelGroup>
	<h:panelGroup id="criteriaTable1Row3Column1" styleClass="formDataEntryLine">
		<h:selectBooleanCheckbox id="nbAApprovedCB"
			value="#{pc_mibPlanFInformationCriteria.nbAApproved}"
			style="margin-left: 30px"
			onclick="enableSearchButton();" />
		<h:outputLabel id="nbAApprovedLbl" value="#{property.mibPlanFInformationNbaApproved}"
			styleClass="formLabelRight" />
	</h:panelGroup>
	<h:panelGroup id="criteriaTable1Row3Column2" styleClass="formDataEntryLine">
		<h:selectBooleanCheckbox id="notExaminedCB"
			value="#{pc_mibPlanFInformationCriteria.notExamined}"
			style="margin-left: 30px"
			onclick="enableSearchButton();" />
		<h:outputLabel id="notExaminedLbl" value="#{property.mibPlanFInformationNotExamined}"
			styleClass="formLabelRight" />
	</h:panelGroup>
</h:panelGrid>

<f:verbatim>
	<hr class="formSeparator" />
</f:verbatim>

<h:panelGrid id="planFCriteriaTable2" columns="2" cellspacing="0" cellpadding="0">
	<h:panelGroup id="criteriaTable2Row1Column1" styleClass="formDataEntryLine">
		<h:outputLabel id="fromDateLbl" value="#{property.mibPlanFInformationFromDate}"
			styleClass="formLabel" />
		<h:inputText id="fromDateTxt"
			value="#{pc_mibPlanFInformationCriteria.fromDate}"
			styleClass="formEntryDate" style="width: 150px"
			onkeyup="enableSearchButton();" onmouseout="enableSearchButton();">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="criteriaTable2Row1Column2" styleClass="formDataEntryLine">
		<h:outputLabel id="toDateLbl" value="#{property.mibPlanFInformationToDate}"
			styleClass="formLabel" />
		<h:inputText id="toDateTxt"
			value="#{pc_mibPlanFInformationCriteria.toDate}"
			styleClass="formEntryDate" style="width: 150px"
			onkeyup="enableSearchButton();" onmouseout="enableSearchButton();" onkeydown="enableSearchButton();">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
</h:panelGrid>
