<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA352         NB-1501   MIB Plan F Information Received Notification --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>nbA MIB PlanF Information Action</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<SCRIPT type="text/javascript" src="javascript/global/file.js"></SCRIPT>
	<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
	<script type="text/javascript" src="javascript/global/scroll.js"></script>
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_planFInformationAction'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_planFInformationAction'].target='';
			return false;
		}
		function saveTableScrollPosition() {
			saveScrollPosition('form_planFInformationAction:planFResults:planFResultData', 'form_planFInformationAction:planFResults:resultTableVScroll');
			return false;
		}
		function scrollTablePosition() {
			scrollToPosition('form_planFInformationAction:planFResults:planFResultData', 'form_planFInformationAction:planFResults:resultTableVScroll');
			return false;
		}
		function enableSearchButton() {
			/* alert('enableSearchButton called'); */
 			toDisable = true;
 			if(document.getElementById('form_planFInformationAction:mibPlanFCriteria:contractNumTxt').value!="" || 
				document.getElementById('form_planFInformationAction:mibPlanFCriteria:matchStatusDD').value!="-1" || 
				document.getElementById('form_planFInformationAction:mibPlanFCriteria:lastNameTxt').value!="" || 
				document.getElementById('form_planFInformationAction:mibPlanFCriteria:nbAApprovedCB').checked || 
				document.getElementById('form_planFInformationAction:mibPlanFCriteria:notExaminedCB').checked) {
					toDisable = false;	
				}
			if(document.getElementById('form_planFInformationAction:mibPlanFCriteria:fromDateTxt').value!="" && 
				document.getElementById('form_planFInformationAction:mibPlanFCriteria:fromDateTxt').value!="MM/dd/yyyy") {
				  	toDisable = false;
				}
			if(document.getElementById('form_planFInformationAction:mibPlanFCriteria:toDateTxt').value!="" && 
					document.getElementById('form_planFInformationAction:mibPlanFCriteria:toDateTxt').value!="MM/dd/yyyy") {
					  	toDisable = false;
					}
 			if(document.getElementById('form_planFInformationAction:mibPlanFCriteria:firstNameTxt').value!="" && 
					document.getElementById('form_planFInformationAction:mibPlanFCriteria:lastNameTxt').value=="") {
					  	toDisable = true;
					}
			document.getElementById('form_planFInformationAction:btnSearch').disabled = toDisable;
		}	
	</script>
</head>
<body onload="filePageInit();scrollTablePosition();enableSearchButton();" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<h:form id="form_planFInformationAction" onsubmit="saveTableScrollPosition();">
			<div class="inputFormMat">
				<div class="inputForm" style="height: 200px">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputLabel value="#{property.mibPlanFInformationTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
		
					<f:subview id="mibPlanFCriteria">
						<c:import url="/nbaMIBPlanFInformation/subviews/NbaMIBPlanFInformationCriteria.jsp" />
					</f:subview>
		
					<hr class="formSeparator" />
		
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft"
										action="#{pc_mibPlanFInformationCriteria.clear}" onclick="resetTargetFrame();" />
						<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight"
										action="#{pc_mibPlanFInformationCriteria.search}" onclick="resetTargetFrame();" />
					</h:panelGroup>
				</div>
			</div>
			<h:outputLabel id="mpfResultTableTitle" value="#{property.mibPlanFResultsTableTitle}" styleClass="sectionSubheader" style="margin-left: -10px;" />
			<f:subview id="planFResults">
				<c:import url="/nbaMIBPlanFInformation/subviews/NbaMIBPlanFInformationResults.jsp" />
			</f:subview>
			<h:panelGroup styleClass="tabButtonBar" style="height: 35px">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"
									action="#{pc_mibPlanFInformationCriteria.search}" onclick="resetTargetFrame();"/>
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" onclick="resetTargetFrame();"
									action="#{pc_mibPlanFInformationResultTable.commitExamination}" />
			</h:panelGroup>
		</h:form>

		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
