<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA352 		NB-1501	  MIB Plan F Information Received Notification -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.mpfDetailsTitle}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';
		var fileLocationHRef = '<%=basePath%>' + '/nbaMIBPlanFInformation/popups/NbaInteractiveMIBFollowUpDetails.faces';
		var width=630;
		var height=530;
		
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_viewMIBPlanFDetails'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_viewMIBPlanFDetails'].target='';
			return false;
		}
	</script>
	</head>
	<body onload="popupInit();"
		style="overflow-x: hidden; overflow-y: scroll">
		<PopulateBean:Load serviceName="RETRIEVE_MIB_INTERACTIVE_FOLLOW_UP"
			value="#{pc_interactiveMIBCheckAndFollowUpsBean}" />
		<h:form id="form_viewMIBPlanFDetails">
			<div id="mibPlanFDetails" class="inputFormMat">
				<div id="mibPlanFDetailsInputForm" class="inputForm">
					<h:panelGrid id="planFDetailsTable1" columns="3" cellspacing="0"
						cellpadding="0">
						<h:panelGroup id="detailsTable1Row1Column1"
							styleClass="formDataDisplayTopLine">
							<h:outputLabel id="mibPlanFInformationFirstNameLbl"
								value="#{property.mibPlanFInformationFirstName}"
								styleClass="formLabel" style="width: 100px" />
							<h:outputText id="mibPlanFInformationFirstNameTxt"
								value="#{pc_interactiveMIBFollowUp.firstName}"
								title="#{pc_interactiveMIBFollowUp.firstName}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row1Column2"
							styleClass="formDataDisplayTopLine">
							<h:outputLabel id="mibPlanFInformationMiddleNameLbl"
								value="#{property.mibPlanFInformationMiddleName}"
								styleClass="formLabel" style="width: 95px" />
							<h:outputText id="mibPlanFInformationMiddleNameTxt"
								value="#{pc_interactiveMIBFollowUp.middleName}"
								title="#{pc_interactiveMIBFollowUp.middleName}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row1Column3"
							styleClass="formDataDisplayTopLine">
							<h:outputLabel id="mibPlanFInformationLastNameLbl"
								value="#{property.mibPlanFInformationLastName}"
								styleClass="formLabel" style="width: 105px" />
							<h:outputText id="mibPlanFInformationLastNameTxt"
								value="#{pc_interactiveMIBFollowUp.lastName}"
								title="#{pc_interactiveMIBFollowUp.lastName}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row2Column1"
							styleClass="formDataDisplayLine">
							<h:outputLabel id="mibBirthDateLbl"
								value="#{property.mibBirthDate}" styleClass="formLabel"
								style="width: 100px" />
							<h:outputText value="#{pc_interactiveMIBFollowUp.dateOfBirth}"
								styleClass="formDisplayDate">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row2Column2"
							styleClass="formDataDisplayLine">
							<h:outputLabel id="mibBirthStateLbl"
								value="#{property.mibBirthState}" styleClass="formLabel"
								style="width: 95px" />
							<h:outputText id="mibBirthStateTxt"
								value="#{pc_interactiveMIBFollowUp.birthState}"
								title="#{pc_interactiveMIBFollowUp.birthState}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row2Column3"
							styleClass="formDataDisplayLine">
							<h:outputLabel id="mpfDetailsSSNLbl"
								value="#{property.mpfDetailsSSN}" styleClass="formLabel"
								style="width: 105px" />
							<h:outputText id="mpfDetailsSSNTxt"
								value="#{pc_interactiveMIBFollowUp.govtID}"
								title="#{pc_interactiveMIBFollowUp.govtID}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
						<h:panelGroup id="detailsTable1Row3Column1"
							styleClass="formDataDisplayLine">
							<h:outputLabel id="mibOccupationLbl"
								value="#{property.mibOccupation}" styleClass="formLabel"
								style="width: 100px" />
							<h:outputText id="mibOccupationTxt"
								value="#{pc_interactiveMIBFollowUp.occupation}"
								title="#{pc_interactiveMIBFollowUp.occupation}"
								styleClass="formDisplayText" style="width: 100px" />
						</h:panelGroup>
					</h:panelGrid>
					<h:panelGroup id="mpfDetailsMatchMessageRow"
						styleClass="formDataDisplayLine"
							rendered="#{pc_interactiveMIBFollowUp.notFoundStatus}" >
						<f:verbatim>
							<hr class="formSeparator" />
						</f:verbatim>
						<h:outputLabel id="mpfDetailsMatchMessageLbl"
							value="#{property.mpfDetailsMatchMessage}" styleClass="formLabel"
							style="width: 120px"/>
						<h:outputText id="mpfDetailsMatchMessageTxt"
							value="#{pc_interactiveMIBFollowUp.matchMessage}"
							title="#{pc_interactiveMIBFollowUp.matchMessage}"
							styleClass="formDisplayText" style="width: 430px"/>
					</h:panelGroup>
					<f:verbatim>
						<hr class="formSeparator" />
					</f:verbatim>
					<f:subview id="mibFollowUpOverview">
						<c:import
							url="/nbaMIBPlanFInformation/subviews/NbaMIBFollowUpOverview.jsp" />
					</f:subview>
					<h:panelGroup id="mpfDetailsButtonBar" styleClass="formButtonBar">
						<h:commandButton id="mpfDetailsButtonCancel"
							value="#{property.buttonCancel}" styleClass="formButtonLeft"
							onclick="setTargetFrame();"
							action="#{pc_interactiveMIBCheckAndFollowUpsBean.cancelInteractiveDetails}"
							immediate="true" />
						<h:commandButton id="mpfDetailsButtonExamined"
							value="#{property.mpfDetailsButtonExamined}"
							styleClass="formButtonRight" onclick="setTargetFrame();"
							action="#{pc_interactiveMIBCheckAndFollowUpsBean.markSelectedRowAsExamined}" />
					</h:panelGroup>
				</div>
			</div>
		</h:form>
		<div id="Messages" style="display: none">
			<h:messages />
		</div>
	</body>
</f:view>
</html>