<%-- CHANGE LOG 
     Audit Number   Version   Change Description
     NBA213 		   7	  Unified User Interface
     SPR3811		NB-1301	  Corrections to Companion Case View
     NBA350		    NB-1401   Applet Implementation of AWD Image Viewer
     SPRNBA-976 	NB-1601   Null session variables result from overlapping transactions initiated on the To-Do List user interface
 --%>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}

boolean closeWindow = false;
if (request.getQueryString() != null) {
	closeWindow = (request.getQueryString().indexOf("close=true") >= 0);
}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Launch Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		try {	//SPR3811
			parent.closeAllWindows();
			top.closeToDoList();  //SPRNBA-976
		}catch(err){	//SPR3811
			//contintue processing
	    }	//SPR3811
		top.loadRightFile('<%=basePath%>desktops/blank.html');
		top.refreshLeftFile();
		//begin NBA350
		try {
			top.ImgViewer.closeImageViewer(); //NBA213
		} catch(err){//Do nothing. No recovery
		}   	
		//end NBA350		
		if (top.currentOrientation != top.FULL_SCREEN) {  //FNB016
			top.showLeft();
		}  //FNB016
	</script>
</head>
<body>
<f:view>
	<div id="Messages"><h:messages /></div>
</f:view>
</body>
</html>
