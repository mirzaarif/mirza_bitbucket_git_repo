<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA179				7		Reallocate UI Rewrite-->
<!-- NBA213           	7		Unified User Interface -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->


<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reallocate</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">

		function setTargetFrame() {
			document.forms['form_reallocate'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_reallocate'].target='';
			return false;
		}

		function initialize() {
			top.defaultWindowOrientation = top.FULL_SCREEN;
			top.showFullScreen();
			if (window.screen.availWidth >= 1280) {
				this.document.body.style.overflowX = "hidden";
				if (window.screen.availHeight >= 980) {
					this.document.body.style.overflowY = "hidden";
				}
			}
		}
		
	</script>
</head>
<body onload="filePageInit();initialize();" style="overflow-x: scroll; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:form id="form_reallocate">
		<f:subview id="nbaReallocateSearch">
			<c:import url="/nbaReallocate/subviews/nbaReallocateCriteria.jsp" />
		</f:subview>
		<h:panelGroup id="reallocate" styleClass="sectionSubheader" style="width: 1235px; height: 35px; text-indent: 0px;">
			<h:outputText value="#{property.reallocateQueue}:" styleClass="formLabel" />
			<h:selectOneMenu id="reallocateToQueue" styleClass="formEntryText" value="#{pc_reallocateResultTable.queue}"
				valueChangeListener="#{pc_reallocateResultTable.changeReallocateQueue}" style="width: 275px; margin-top: 4px">
				<f:selectItems value="#{pc_reallocateResultTable.queues}" />
			</h:selectOneMenu>
			<h:outputText value="#{property.reallocateUwQueue}:" styleClass="formLabel" style="margin-left: 40px;width: 150px" />
			<h:selectOneMenu id="undwriterqueue" styleClass="formEntryText" value="#{pc_reallocateResultTable.undQueue}"
				style="width: 170px; margin-top: 4px">
				<f:selectItems value="#{pc_reallocateResultTable.undQueues}" />
			</h:selectOneMenu>
			<h:outputText value="#{property.reallocateStatus}:" styleClass="formLabel" style="margin-left: 35px; margin-top: -5px;" />
			<h:selectOneMenu id="reallocateToStatus" styleClass="formEntryText" value="#{pc_reallocateResultTable.status}"
				style="width: 300px; margin-top: 4px">
				<f:selectItems value="#{pc_reallocateResultTable.statuses}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<f:subview id="searchResults">
			<c:import url="/nbaReallocate/subviews/nbaReallocateTable.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar" style="height: 35px">
			<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" style="left: 1143px"
				disabled="#{pc_reallocateResultTable.auth.enablement['Commit'] || 
							!pc_reallocateResultTable.draftItems}"
				action="#{pc_reallocateResultTable.commitReallocate}" />	<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<!-- NBA213 code deleted -->
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
