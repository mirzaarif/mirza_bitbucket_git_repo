<%-- CHANGE LOG 
Audit Number   Version   Change Description
NBA179             7     Reallocate Rewrite
SPRNBA-747     NB-1401   General Code Clean Up
SPRNBA-1012    NB-1601   Suspended tool tip in the Reallocate search results table pane is not appearing when the user hovers over the suspend date.
--%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:panelGroup id="searchHeader" styleClass="ovDivTableHeader" style="width: 1235px">
	<h:panelGrid columns="9" styleClass="ovTableHeader" style="width: 1215px" cellspacing="0"
		columnClasses="ovColHdrIcon,ovColHdrText100,ovColHdrText105,ovColHdrText155,ovColHdrText190,ovColHdrText170,ovColHdrText190,ovColHdrText100,ovColHdrText190">
		<h:commandLink id="searchHdrCol1" value="" style="width: 100%; height: 100%" styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol1}"
			actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol2" value="#{property.reallocateCreateDate}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol2}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol3" value="#{property.reallocatePriority}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol3}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol4" value="#{property.reallocateContract}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol4}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol5" value="#{property.reallocateName}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol5}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol6" value="#{property.reallocateWorkType}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol6}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol7" value="#{property.reallocateQueue}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol7}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol8" value="#{property.reallocateSuspended}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol8}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
		<h:commandLink id="searchHdrCol9" value="#{property.reallocateNewQueue}" style="width: 100%; height: 100%"
			styleClass="ovColSorted#{pc_reallocateResultTable.sortedByCol9}" actionListener="#{pc_reallocateResultTable.sortColumn}" immediate="true" />
	</h:panelGrid>
</h:panelGroup>
<h:panelGroup id="searchData" styleClass="ovDivTableData" style="width: 1235px; background-color: white; height: 445px">
	<h:dataTable id="searchTable" styleClass="ovTableData" style="width: 1215px" cellspacing="0" binding="#{pc_reallocateResultTable.dataTable}"
		value="#{pc_reallocateResultTable.results}" var="result" rowClasses="#{pc_reallocateResultTable.rowStyles}"
		columnClasses="ovColIconTop,ovColText100,ovColText105,ovColText155,ovColText190,ovColText170,ovColText190,ovColText100,ovColText190">
		<h:column>
			<h:commandLink id="searchCol1" styleClass="ovFullCellSelect" style="width: 100%; height: 100%;" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true">
				<h:graphicImage url="images/comments/lock-closed-forlist.gif" title="#{result.lockedBy}" styleClass="ovViewIcon#{result.locked}"
					style="border-width: 0px; margin-top: -3px;" />
			</h:commandLink>
		</h:column>
		<h:column>
			<h:commandLink id="searchCol2" value="#{result.createdDate}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol3" value="#{result.priority}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol4" value="#{result.contractNumber}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol5" value="#{result.name}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}" immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol6" value="#{result.workTypeTranslation}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol7" value="#{result.queue}" styleClass="ovFullCellSelect" action="#{pc_reallocateResultTable.selectRow}"
				immediate="true" />
		</h:column>
		<h:column>
			<h:commandLink id="searchCol8" value="#{result.suspended}" style="width: 100%; text-align: right;" styleClass="ovFullCellSelect" title="#{result.suspendOriginator}"
				action="#{pc_reallocateResultTable.selectRow}" immediate="true"/> <%-- SPRNBA-1012 --%>
		</h:column>
		<h:column>
			<h:commandLink id="searchCol9" value="#{result.newQueue}" style="width: 100%; text-align: right;" styleClass="ovFullCellSelect"
				action="#{pc_reallocateResultTable.selectRow}" immediate="true" />
		</h:column>

	</h:dataTable>
</h:panelGroup>
<h:panelGroup styleClass="ovButtonBar" style="width: 1235px;">
	<h:commandButton id="btnReallocate" value="#{property.buttonReallocate}" styleClass="tabButtonRight" style="left: 1135px"
				disabled="#{pc_reallocateResultTable.reallocateButtonDisable}" action="#{pc_reallocateResultTable.reallocate}" />  <%-- SPRNBA-747 --%>
</h:panelGroup>
<h:panelGroup styleClass="ovStatusBar" style="width: 1235px;">
	<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_reallocateResultTable.showPrevious}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.previousPageAbsolute}" immediate="true" />
	<h:commandLink value="#{property.previousPage}" rendered="#{pc_reallocateResultTable.showPrevious}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.previousPage}" immediate="true" />
	<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_reallocateResultTable.showPreviousSet}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.previousPageSet}" immediate="true" />
	<h:commandLink value="#{pc_reallocateResultTable.page1Number}" rendered="#{pc_reallocateResultTable.showPage1}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page1}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page1Number}" rendered="#{pc_reallocateResultTable.currentPage1}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page2Number}" rendered="#{pc_reallocateResultTable.showPage2}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page2}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page2Number}" rendered="#{pc_reallocateResultTable.currentPage2}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page3Number}" rendered="#{pc_reallocateResultTable.showPage3}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page3}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page3Number}" rendered="#{pc_reallocateResultTable.currentPage3}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page4Number}" rendered="#{pc_reallocateResultTable.showPage4}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page4}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page4Number}" rendered="#{pc_reallocateResultTable.currentPage4}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page5Number}" rendered="#{pc_reallocateResultTable.showPage5}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page5}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page5Number}" rendered="#{pc_reallocateResultTable.currentPage5}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page6Number}" rendered="#{pc_reallocateResultTable.showPage6}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page6}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page6Number}" rendered="#{pc_reallocateResultTable.currentPage6}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page7Number}" rendered="#{pc_reallocateResultTable.showPage7}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page7}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page7Number}" rendered="#{pc_reallocateResultTable.currentPage7}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{pc_reallocateResultTable.page8Number}" rendered="#{pc_reallocateResultTable.showPage8}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.page8}" immediate="true" />
	<h:outputText value="#{pc_reallocateResultTable.page8Number}" rendered="#{pc_reallocateResultTable.currentPage8}" styleClass="ovStatusBarTextBold" />
	<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_reallocateResultTable.showNextSet}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.nextPageSet}" immediate="true" />
	<h:commandLink value="#{property.nextPage}" rendered="#{pc_reallocateResultTable.showNext}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.nextPage}" immediate="true" />
	<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_reallocateResultTable.showNext}" styleClass="ovStatusBarText"
		action="#{pc_reallocateResultTable.nextPageAbsolute}" immediate="true" />
</h:panelGroup>
