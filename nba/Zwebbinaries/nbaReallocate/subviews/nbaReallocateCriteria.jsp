<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA179           7         Reallocate Rewrite -->
<!-- SPR3753          8         Date Fields Incorrectly Require Slashes To Be Entered -->

<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:panelGroup id="reallocateFormat" styleClass="inputFormMat" style="width:1235px">
	<h:panelGroup id="reallocateForm" styleClass="inputForm" style="height: 160px">
		<h:panelGroup styleClass="formTitleBar">
			<h:outputLabel value="#{property.reallocateSection}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="businessAreaPanelGroup" styleClass="formDataEntryTopLine">
			<h:outputLabel value="#{property.reallocateBussArea}:" styleClass="formLabel" />
			<h:selectOneMenu id="nbaReallocateCriteria_businessarea" immediate="true" value="#{pc_reallocateCriteria.businessArea}" styleClass="formEntryText"
				style="width: 425px" valueChangeListener="#{pc_reallocateCriteria.changeBusinessArea}" onchange="submit()">
				<f:selectItems id="nbaReallocateCriteria_businessarea_selectItems" value="#{pc_reallocateCriteria.businessAreas}" />
			</h:selectOneMenu>
			<h:outputLabel value="#{property.reallocateQueue}:" styleClass="formLabel" style="margin-left: 25px"/>
			<h:selectOneMenu id="nbaReallocateCriteria_queue" immediate="true" valueChangeListener="#{pc_reallocateCriteria.changeQueue}"
				value="#{pc_reallocateCriteria.queue}" styleClass="formEntryText" style="width: 425px">
				<f:selectItems id="nbaReallocateCriteria_queue_selectItems" value="#{pc_reallocateCriteria.queues}" />
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="workTypePanelGroup" styleClass="formDataEntryLine" style="height: 25px;">
			<h:outputLabel value="#{property.reallocateWorkType}:" styleClass="formLabel" />
			<h:selectOneMenu id="nbaReallocateCriteria_worktype" immediate="true" value="#{pc_reallocateCriteria.workType}" styleClass="formEntryText"
				style="width: 425px" valueChangeListener="#{pc_reallocateCriteria.changeWorkType}" onchange="resetTargetFrame();submit()">
				<f:selectItems id="nbaReallocateCriteria_worktype_selectItems" value="#{pc_reallocateCriteria.workTypes}" />
			</h:selectOneMenu>
			<h:selectBooleanCheckbox id="suspended" value="#{pc_reallocateCriteria.excludeSuspended}" style="margin-left: 147px" />
			<h:outputLabel value="#{property.reallocateExSuspended}" styleClass="formLabelRight" />
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGrid id="datesPanelGrid" columns="4" cellspacing="0" cellpadding="0">
			<h:column>
				<h:outputLabel value="#{property.reallocateFromDate}:" styleClass="formLabel" />
				<h:inputText id="From_Date" value="#{pc_reallocateCriteria.fromDate}" styleClass="formEntryDate" style="width: 100px"
					immediate="true"><!-- SPR3753 -->
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<h:outputLabel value="#{property.reallocateTime}:" styleClass="formLabel" style="width: 100px" />
				<h:inputText id="From_Time" value="#{pc_reallocateCriteria.fromTime}" styleClass="formEntryDate" style="width: 80px">
					<f:convertDateTime pattern="#{property.shorttimePattern}" />
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio value="#{pc_reallocateCriteria.fromTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
					style="width: 100px; margin-right: 70px">
					<f:selectItems value="#{pc_reallocateCriteria.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputLabel value="#{property.reallocateToDate}:" styleClass="formLabel" />
				<h:inputText id="To_Date" value="#{pc_reallocateCriteria.toDate}" styleClass="formEntryDate" style="width: 100px"
					immediate="true"><!-- SPR3753 -->
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<h:outputLabel value="#{property.reallocateTime}:" styleClass="formLabel" style="width: 100px" />
				<h:inputText id="To_Time" value="#{pc_reallocateCriteria.toTime}" styleClass="formEntryDate" style="width: 80px">
					<f:convertDateTime pattern="#{property.shorttimePattern}" />
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio value="#{pc_reallocateCriteria.toTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
					style="width: 100px; margin-right: 70px">
					<f:selectItems value="#{pc_reallocateCriteria.timePeriods}" />
				</h:selectOneRadio>
			</h:column>
		</h:panelGrid>
		<h:panelGroup id="searchButtonsPanelGroup" styleClass="formButtonBar">
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft" action="#{pc_reallocateCriteria.clear}"
				onclick="resetTargetFrame();" />
			<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight-1" style="left: 1130px"
				action="#{pc_reallocateCriteria.searchReallocate}" onclick="resetTargetFrame();" disabled="#{pc_reallocateCriteria.searchDisabled}" />
		</h:panelGroup>
	</h:panelGroup>
</h:panelGroup>
