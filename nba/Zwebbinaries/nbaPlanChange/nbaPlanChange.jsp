<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA181           	7         	Plan Change Rewrite -->
<!-- NBA187           	7         	Trial Application -->
<!-- NBA139           	7         	Plan Code Determination -->
<!-- NBA213			  	7	  		Unified User Interface  -->
<!-- SPR1613			8	  		Some Business Functions should be disabled on an Issued Contract  --> 
<!-- FNB011 		NB-1101	        Work Tracking -->
<!-- FNB013 		NB-1101	        DI Support for nbA -->
<!-- SPRNBA-798     NB-1401         Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
	String basePath = "";
    if (request.getServerPort() == 80) {
    	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
 	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Plan Change</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_planChange'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_planChange'].target='';
			return false;
		}

		//Perform initialization of the page
		function initPage() {
			filePageInit();

			if (document.getElementById('form_planChange:nbaValidationMessages:contractData') != null) {
				if (document.getElementById('form_planChange:genericPlanFieldsMainPGroup') != null) {
					document.getElementById('form_planChange:nbaValidationMessages:contractData').style.height = 370;
				} else {
					document.getElementById('form_planChange:nbaValidationMessages:contractData').style.height = 500;
				}
			}
		}
</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden; overflow-y: scroll" >
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<PopulateBean:Load serviceName="RETRIEVE_PLAN_CHANGE" value="#{pc_contractPlanChange}" />
		<h:form id="form_planChange"> 
			<div class="inputFormMat">
				<div class="inputForm">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputText id="planChangeTitle" value="#{property.planChgTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryTopLine" style="margin-top: 10px; padding-bottom: 19px"><!-- NBA139 -->
						<h:outputText value="#{property.planChgPlan}" styleClass="formLabel" style="width: 130px;"/>
						<h:selectOneMenu id="planDD" value="#{pc_contractPlanChange.plan}" styleClass="formEntryTextFull" style="width: 275px;"
							valueChangeListener="#{pc_contractPlanChange.planValueChange}" onchange="submit()"><!-- NBA139 -->
							<f:selectItems value="#{pc_contractPlanChange.planList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<!-- begin NBA139 -->
					<h:panelGroup id="genericPlanFieldsMainPGroup" rendered="#{pc_contractPlanChange.genericPlanImplementation}" >
						<h:panelGroup id="genericPlanFieldsPGroup" styleClass="formDataEntryLine" rendered="#{!pc_contractPlanChange.di)">			
							<h:selectBooleanCheckbox id="overrideGenericPlanCB" styleClass="ovFullCellSelectCheckBox" style="margin-left: 127px;width: 20px;" 
								value="#{pc_contractPlanChange.genericPlanOverrideInd}" disabled="#{pc_contractPlanChange.genericPlanOverrideIndDisabled || pc_contractPlanChange.wrappered}"
								valueChangeListener="#{pc_contractPlanChange.genericPlanOverrideValueChange}" onclick="resetTargetFrame();submit();" />
							<h:outputText id="overrideGenericPlanLabel" value="#{property.overrideGenericPlan}" styleClass="formLabel" 
								style="margin-left: 3px;width: 150px;text-align: left;"/>
						</h:panelGroup>
						<!-- Begin FNB013-->
						<h:panelGroup id="benefitPeriodAccGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&pc_contractPlanChange.di}" styleClass="formDataEntryLine">
							<h:outputText id="AccidentBenefitPeriod" value="#{property.appEntBenefitPeriodAcc}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="benefitPeriodAcc" value ="#{pc_contractPlanChange.benefitPeriodAcc}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="benefitPeriodAccList" value="#{pc_contractPlanChange.benefitPeriodAccList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="benefitPeriodSickGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&pc_contractPlanChange.di}" styleClass="formDataEntryLine">
							<h:outputText id="benefitPeriodSickLabel" value="#{property.appEntBenefitPeriodSick}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="benefitPeriodSick" value ="#{pc_contractPlanChange.benefitPeriodSick}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="benefitPeriodSickList" value="#{pc_contractPlanChange.benefitPeriodSickList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="waitingPeriodAccGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&pc_contractPlanChange.di}" styleClass="formDataEntryLine">
							<h:outputText id="AccidentWaitingPeriod" value="#{property.appEntWaitingPeriodAcc}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="waitingPeriodAcc" value ="#{pc_contractPlanChange.waitingPeriodAcc}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="waitingPeriodAccList" value="#{pc_contractPlanChange.waitingPeriodAccList}" />
							</h:selectOneMenu>
					    </h:panelGroup>
						<h:panelGroup id="waitingPeriodSickGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&pc_contractPlanChange.di}" styleClass="formDataEntryLine">
							<h:outputText id="waitingPeriodSickLabel" value="#{property.appEntWaitingPeriodSick}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="waitingPeriodSick" value ="#{pc_contractPlanChange.waitingPeriodSick}" styleClass="formEntryTextFull" 
							     style="width: 275px;" >
								<f:selectItems id="waitingPeriodSickList" value="#{pc_contractPlanChange.waitingPeriodSickList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="LabelFieldsPGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&!pc_contractPlanChange.di}" styleClass="formDataEntryLine" >
							<h:outputText id="levelField" value="#{property.levelPeriod}" styleClass="formLabel" style="width: 130px;"/>
							<h:inputText id="levelFieldEF" value="#{pc_contractPlanChange.genericPlanLevelPeriod}" styleClass="formEntryText"
								 style="width: 275px;" disabled="#{pc_contractPlanChange.genericPlanLevelPeriodDisabled}" />
						</h:panelGroup>
						<h:panelGroup id="calculationMethodPGroup" rendered="#{!(pc_contractPlanChange.overrideIndFromTXLife)&&!pc_contractPlanChange.di}" styleClass="formDataEntryLine">
							<h:outputText id="calculationMethodLabel" value="#{property.calculationMethod}" styleClass="formLabel" style="width: 130px;"/>
							<h:selectOneMenu id="calculationMethod" value ="#{pc_contractPlanChange.calculationMethod}" styleClass="formEntryTextFull" 
							    binding="#{pc_contractPlanChange.uiCalculationMethod}" style="width: 275px;" disabled="#{pc_contractPlanChange.calculationMethodDisabled}">
								<f:selectItems id="calculationMethodList" value="#{pc_contractPlanChange.calculationMethodList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<!-- End FNB013-->
						<h:panelGroup id="newPlanPGroup" styleClass="formDataEntryLine" style="padding-bottom: 19px" rendered="#{pc_contractPlanChange.newPlanRendered && !(pc_contractPlanChange.genericPlanOverrideInd)}">
								<h:outputText id="newPlan" value="#{property.newPlan}" styleClass="formLabel" style="width: 130px;"/>
								<h:outputText id="newPlanValue" value="#{pc_contractPlanChange.newPlan}" styleClass="formEntryText" style="width: 275px;" />
						</h:panelGroup>
					</h:panelGroup>					
					<!-- end NBA139 -->
					<f:subview id="nbaValidationMessages" rendered="#{pc_contractPlanChange.showMessages}">
						<c:import url="/common/subviews/NbaValidationMessages.jsp" />
					</f:subview>
				</div>
			</div>
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />
			</f:subview>
			<h:panelGroup styleClass="tabButtonBar">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"
								action="#{pc_contractPlanChange.refresh}" />
				<h:commandButton id="btnValidate" value="#{property.buttonValidate}" styleClass="tabButtonRight-1"
								action="#{pc_contractPlanChange.validate}" disabled="#{pc_contractPlanChange.trialApplication}" />  <!-- NBA187 -->
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight"
								action="#{pc_contractPlanChange.commit}" 
								disabled="#{pc_contractPlanChange.auth.enablement['Commit'] ||
											pc_contractPlanChange.notLocked || 
											pc_contractPlanChange.trialApplication || 
											pc_contractPlanChange.wrappered || 
											pc_contractPlanChange.issued}" />   <!-- NBA187 NBA139 NBA213 SPR1613 -->
			</h:panelGroup>
		</h:form>
		<div id="Messages" style="display:none"><h:messages /></div>
	</f:view>
</body>
</html>
