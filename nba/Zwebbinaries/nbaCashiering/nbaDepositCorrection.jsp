<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182            7      Cashiering Rewrite -->
<!-- SPR3482           8      Search Push Button Enabled on View Open of Deposit Correction Tab -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Deposit Correction</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<SCRIPT type="text/javascript" src="javascript/global/file.js"></SCRIPT>	
	<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_depositCorrection'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_depositCorrection'].target='';
			return false;
		}
		function openBF() {
			var nextViewValue = document.forms['form_depositCorrection']['form_depositCorrection:nextView'].value;
			if (nextViewValue != null && nextViewValue.length > 0) {
				var appBasePath = "";
				var context = "/nba/";
				if (window.document.location.port != "") {
					appBasePath = window.document.location.protocol + "//" + window.document.location.hostname +":" + window.document.location.port + context;
				} else {
					appBasePath = window.document.location.protocol + "//" + window.document.location.hostname + context;
				}
				top.window.location.href = appBasePath + nextViewValue;
			}
		}
		
		function saveTableScrollPosition() {
			saveScrollPosition('form_depositCorrection:searchResults:searchData', 'form_depositCorrection:searchResults:searchTableVScroll');
			return false;
		}
		
		function scrollTablePosition() {
			scrollToPosition('form_depositCorrection:searchResults:searchData', 'form_depositCorrection:searchResults:searchTableVScroll');
			return false;
		}
		//Begin SPR3482
		function enableButton() {
			toDisable = false;
			if((document.getElementById('form_depositCorrection:depositSearch:cashdepositDateInput').value == 'MM/dd/yyyy' 
			|| document.getElementById('form_depositCorrection:depositSearch:cashdepositDateInput').value == '' )
			&& document.getElementById('form_depositCorrection:depositSearch:cashCheckNumberInput').value == ''
			&& document.getElementById('form_depositCorrection:depositSearch:cashCheckAmountInput').value == ''
			&& document.getElementById('form_depositCorrection:depositSearch:cashContractNumberInput').value == ''
			&& document.getElementById('form_depositCorrection:depositSearch:cashLastNameInput').value == ''){
				toDisable = true;
			}
			document.getElementById('form_depositCorrection:depositSearch:btnSearch').disabled = toDisable;
		}
		//End SPR3482
	</script>
</head>
<body onload="filePageInit(); enableButton(); openBF();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll"><!-- SPR3482 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_DEPOSITS" value="#{pc_depositTable}" />
		<h:form id="form_depositCorrection" onsubmit="saveTableScrollPosition();"> 
			<div class="inputFormMat" style="width: 1244px">
				<div class="inputForm" style="height: auto">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputLabel value="#{property.cashieringDepositCorrection}" styleClass="shTextLarge" />
					</h:panelGroup>
		
					<f:subview id="depositSearch">
						<c:import url="/nbaCashiering/subviews/nbaDepositSearch.jsp" />
					</f:subview>
		
					

				</div>
			</div>
			<div class="inputFormMat" style="width: 1250px">
				<f:subview id="depositTable">
					<c:import url="/nbaCashiering/subviews/nbaDepositTable.jsp" />
				</f:subview>
			</div>
		</h:form>
		<h:inputHidden id="titleBar" value="#{property.cashieringTitleBar}" />
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	