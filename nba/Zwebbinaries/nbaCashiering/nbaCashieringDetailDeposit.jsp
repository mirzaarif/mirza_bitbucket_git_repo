<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA182 		  7		  	Cashiering Workbench Rewrite -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Cashiering Detail</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_cashieringDetail'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cashieringDetail'].target='';
			return false;
		}
		
	</script>
	
</head>
<body onload="popupInit();" style="overflow-x: hidden; overflow-y: hidden"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>

		<h:form id="form_cashieringDetail">
			
			<h:panelGroup id="bundleDetail" styleClass="ovDivTableHeader" style="width: 100%" >
				<h:panelGrid columns="4" styleClass="ovTableHeader" columnClasses="ovColHdrText350,ovColHdrText350,ovColHdrText350,ovColHdrText350" cellspacing="0">
										
					
					<h:column>
						
							<h:outputLabel id="CheckSummary1" value="Totals:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue1" value="#{pc_depositTable.cashieringDetail.total}" styleClass="shTextLarge" />
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary2" value="Excluded:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue2" value="#{pc_depositTable.cashieringDetail.excludedTotal}" styleClass="shTextLarge" />
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary3" value="Unapplied:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue3" value="#{pc_depositTable.cashieringDetail.unappliedTotal}" styleClass="shTextLarge" />
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary4" value="Rejected:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue4" value="#{pc_depositTable.cashieringDetail.rejectedTotal}" styleClass="shTextLarge" />
					</h:column>
					
					
					
				</h:panelGrid>

			</h:panelGroup>
			
				
				

				<h:dataTable id="checkTable1" styleClass="ovTableData"  style="width: 100%" cellspacing="0" 
				binding="#{pc_depositTable.cashieringDetail.bundleTable}" value="#{pc_depositTable.cashieringDetail.results}" var="detail"
				columnClasses="ovColText295,ovColText295,ovColText295">
				<h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummaryTitle1" value="#{detail.companyCode}" styleClass="shTextLarge" />
						
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummaryTitle2" value="#{detail.bundleId}" styleClass="shTextLarge" />
						
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummaryTitle3" value="#{detail.totalAmount}" styleClass="shTextLarge" />
						
					</h:column>
					
					
					
					
					<h:panelGroup id="bundleDetailHeader" styleClass="ovDivTableHeader" style="width: 100%" >
						<h:panelGrid columns="9" styleClass="ovTableHeader" columnClasses="ovColHdrText105,ovColHdrText105,ovColHdrText105,ovColHdrText165,ovColHdrText165,ovColHdrText140,ovColHdrText140,ovColHdrText140,ovColHdrText165" cellspacing="0">
							<h:commandLink id="cashieringDetailCol1" value="#{property.cashieringDetailCol1}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol2" value="#{property.cashieringDetailCol2}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol3" value="#{property.cashieringDetailCol3}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol4" value="#{property.cashieringDetailCol4}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol5" value="#{property.cashieringDetailCol5}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol6" value="#{property.cashieringDetailCol6}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol7" value="#{property.cashieringDetailCol7}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol8" value="#{property.cashieringDetailCol8}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol9" value="#{property.cashieringDetailCol9}" styleClass="ovColSorted"/>
						</h:panelGrid>

					</h:panelGroup>
					
					<h:dataTable id="checkTable2" styleClass="ovTableData" style="width: 100%" cellspacing="0" 
					binding="#{pc_depositTable.cashieringDetail.checkTable}" value="#{detail.checks}" var="checks"
					columnClasses="ovColText105,ovColText105,ovColText105,ovColText165,ovColText165,ovColText140,ovColText140,ovColText140,ovColText165">
					

					<h:column>
						<h:selectBooleanCheckbox id="linkedCol1" value="#{checks.includedInd}" styleClass="ovFullCellSelectCheckBox"/>
					</h:column>
					<h:column>
						<h:selectBooleanCheckbox id="linkedCol2" value="#{checks.appliedInd}" styleClass="ovFullCellSelectCheckBox"/>
					</h:column>
					<h:column>
						<h:selectBooleanCheckbox id="linkedCol3" value="#{checks.rejectedInd}" styleClass="ovFullCellSelectCheckBox"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCol4" value="#{checks.primaryInsuredName}" styleClass="ovFullCellSelect"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCol5" value="#{checks.contractNumber}" styleClass="ovFullCellSelect"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCol6" value="#{checks.appliedAmount}" styleClass="ovFullCellSelect"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCol7" value="#{checks.checkAmount}" styleClass="ovFullCellSelect"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCol8" value="#{checks.checkNumber}" styleClass="ovFullCellSelect"/>
					</h:column>
					<h:column>
						<h:commandLink id="linkedCoL9" value="#{checks.company}" styleClass="ovFullCellSelect"/>
					</h:column>
					</h:dataTable>
	

				</h:column>					
				</h:dataTable>

			
			
			
			
			

			<h:panelGroup styleClass="ovButtonBar">
				<h:commandButton id="btnUpdate" value="#{property.buttonCancel}" styleClass="formButtonLeft" 
				action="#{pc_depositTable.cashieringDetail.cancel}" onclick="resetTargetFrame();"  immediate="true" />
				<h:commandButton id="btnClose" value="#{property.buttonRefresh}" styleClass="formButtonLeft" 
				action="#{pc_depositTable.cashieringDetail.refresh}"  style="left: 140px;"/>
				<h:commandButton id="btnCreate" value="#{property.buttonCommit}" styleClass="formButtonRight" 
					disabled="#{pc_depositTable.auth.enablement['Commit'] || 
								pc_depositTable.notLocked}"
					action="#{pc_depositTable.cashieringDetail.commit}"  style="left: 1120px;width: 92px;" />	<!-- NBA213 -->
			</h:panelGroup>
				
		</h:form>

	</f:view>
</body>
</html>
