<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182            7      Cashiering Rewrite -->
<!-- NBA213            7      Unified User Interface -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Cashiering File</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = -1;		
			var leftOffset = 5;
			var maxTabsPerRow = 8;

			function initFileSize() {
			   	winHeight = window.screen.availHeight;
				tabHeight = winHeight - 175;
				document.getElementById('file').height = tabHeight;
			}
		//-->
	</script>
</head>
<body class="desktopBody" onload="initialize();top.hideWait();" style="overflow-y: hidden">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<table height="100%" width="1245" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:20px;" class="desktopBody">
					<td></td>
				</tr>
				<tr style="height:30px;" class="textMainTitleBar">
					<td><FileLoader:Files location="nbaCashiering/file/" numTabsPerRow="2"/></td>						
						<FileLoader:DisableTab disableIndex="1" disableValue="#{pc_Coordinator.auth.enablement['cashieringSummary']}"/>   <!-- NBA213 -->						
						<FileLoader:DisableTab disableIndex="2" disableValue="#{pc_Coordinator.auth.enablement['cashieringCorrection']}"/>  <!-- NBA213 -->						
				</tr>
				
				<tr style="height:*; vertical-align: top;" class="textMainTitleBar">
					<td><iframe id="file" name="file" src="" height="100%" width="100%" frameborder="0"  onload="initFileSize();"></iframe></td>
				</tr>
			</tbody>
		</table>
	</f:view>
</body>
</html>
