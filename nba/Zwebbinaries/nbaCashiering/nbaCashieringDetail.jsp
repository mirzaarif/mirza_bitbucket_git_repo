<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA182 		  7		  	Cashiering Workbench Rewrite -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- SPR3483 		  8		  	Missing Scroll Bar on Detail View When Max Table Pane Height Reconfigured -->
<!-- NBA228         NB-1101   Cash Management Enhancement -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Cashiering Detail</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011

		function setTargetFrame() {
			document.forms['form_cashieringDetail'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cashieringDetail'].target='';
			return false;
		}
		
	</script>
	
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
	
		<h:form id="form_cashieringDetail">
			
			<h:panelGroup id="bundleDetail1" styleClass="ovDivTableHeader" style="width: 100%" rendered="#{pc_cashieringDetail.depositInclude}">
				<h:panelGrid columns="4" styleClass="sectionSubheader"  cellspacing="0">
										
					
					<h:column>
						
							<h:outputLabel id="CheckSummary11" value="Totals:" styleClass="shTextLarge" style="width:100px"/>
							
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary21" value="Excluded:"  styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue21" value="#{pc_cashieringDetail.excludedTotal}" styleClass="formLabel" style="text-align: left;">
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary31" value="Unapplied:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue31" value="#{pc_cashieringDetail.unappliedTotal}" styleClass="formLabel" style="text-align: left;">
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary41" value="Rejected:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue41" value="#{pc_cashieringDetail.rejectedTotal}" styleClass="formLabel" style="text-align: left;">
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
					</h:column>
					
					
					
				</h:panelGrid>

			</h:panelGroup>
			
			
			<h:panelGroup id="bundleDetail2" styleClass="ovDivTableHeader" style="width: 100%" rendered="#{pc_cashieringDetail.depositApply}">
				<h:panelGrid columns="4" styleClass="ovTableHeader" columnClasses="ovColHdrText455,ovColHdrText455,ovColHdrText455" cellspacing="0">
										
					
					<h:column>
						
							<h:outputLabel id="CheckSummary12" value="Totals:" styleClass="shTextLarge" />
							
					</h:column>
					
					<h:column>
						
							<h:outputLabel id="CheckSummary32" value="Unapplied:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue32" value="#{pc_cashieringDetail.unappliedTotal}" styleClass="formLabel" >
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
					</h:column>
					<h:column>
						
							<h:outputLabel id="CheckSummary42" value="Rejected:" styleClass="shTextLarge" />
							<h:outputText id="CheckSummaryValue42" value="#{pc_cashieringDetail.rejectedTotal}" styleClass="formLabel" >
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
					</h:column>
					
					
					
				</h:panelGrid>

			</h:panelGroup>
			
				
			<h:panelGroup id="bundleDetail3" styleClass="ovTableData" style="width: 100%;height: 15px;" >
			
			</h:panelGroup> 

			<h:dataTable id="checkTable11" styleClass="ovTableData"  rendered="#{pc_cashieringDetail.depositInclude}" style="width: 100%; " cellspacing="0" 
			binding="#{pc_cashieringDetail.bundleTable}" value="#{pc_cashieringDetail.results}" var="detail">
				<h:column>
					<h:panelGrid columns="2" styleClass="sectionSubheader" style="height:10px" columnClasses="ovColText375,ovColText630" cellspacing="0">
						<h:column>
							<h:outputLabel id="CheckSummaryTitle11" value="#{detail.companyName}" />
						</h:column>
						<h:column>
							<h:outputLabel id="CheckSummaryTitle210" value="Bundle:" styleClass="formLabel" style="font-weight: bold;width:50px;"/>
							<h:outputLabel id="CheckSummaryTitle211" value="#{detail.bundleId}" styleClass="ovColText165" style="border-bottom-width: 0px;"/>
							<h:outputLabel id="CheckSummaryTitle212" value="Total Amount:" styleClass="formLabel" style="font-weight: bold;width:125px;"/>
							<h:outputLabel id="CheckSummaryTitle213" value="#{pc_cashieringDetail.total}" styleClass="ovColText200" style="border-bottom-width: 0px;">
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputLabel>
							
						</h:column>
					</h:panelGrid>
						
					<h:panelGroup id="bundleDetailHeader1" styleClass="ovDivTableHeader" style="width: 100% " >
						<h:panelGrid columns="10" styleClass="ovTableHeader" columnClasses="ovColHdrText60,ovColHdrText60,ovColHdrText60,ovColHdrText170,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText250" cellspacing="0"> <!-- NBA228 -->
							<h:commandLink id="cashieringDetailCol11" value="#{property.cashieringDetailCol1}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol21" value="#{property.cashieringDetailCol2}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol31" value="#{property.cashieringDetailCol3}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol41" value="#{property.cashieringDetailCol4}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol51" value="#{property.cashieringDetailCol5}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol61" value="#{property.cashieringDetailCol6}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol71" value="#{property.cashieringDetailCol7}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol81" value="#{property.cashieringDetailCol8}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol91" value="#{property.cashieringDetailCol9}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol01" value="#{property.cashieringDetailCol10}" styleClass="ovColSorted"/>
						</h:panelGrid>
	
					</h:panelGroup>
					
					<h:panelGroup id="bundleDetail6" styleClass="ovTableData" style="width: 100%;height:#{pc_cashieringDetail.maxHeight}px; overflow: auto; " >	<!-- SPR3483 -->
						<h:dataTable id="checkTable21"  style="width: 100%; " cellspacing="0" 
						binding="#{pc_cashieringDetail.checkTable}" value="#{detail.checks}" var="checks" rowClasses="#{pc_cashieringDetail.rowStyles}"
						columnClasses="ovColText60,ovColText60,ovColText60,ovColText170,ovColText120,ovColText120,ovColText120,ovColText120,ovColText120,ovColText250"> <!-- NBA228 -->
							<h:column>
								<h:selectBooleanCheckbox  id="linkedCol11" value="#{checks.excludeInd}"  styleClass="ovFullCellSelectCheckBox" disabled="#{checks.disableExcludeInd || !pc_cashieringDetail.updateMode}" rendered="#{checks.primaryContractInd}"/> <!-- NBA228 -->
							</h:column>
							<h:column>
								<h:selectBooleanCheckbox  id="linkedCol21" value="#{checks.appliedInd}" styleClass="ovFullCellSelectCheckBox"  disabled="#{checks.disableAppliedInd || !pc_cashieringDetail.updateMode}"/> <!-- NBA228 -->
							</h:column>
							<h:column>
								<h:selectBooleanCheckbox id="linkedCol31" value="#{checks.rejectedInd}" styleClass="ovFullCellSelectCheckBox"  disabled="#{checks.disableRejectInd || !pc_cashieringDetail.updateMode}"/> <!-- NBA228 -->
							</h:column>
							<h:column>
								<h:commandLink id="linkedCol41" value="#{checks.primaryInsuredName}" styleClass="ovFullCellSelect"/>
							</h:column>
							<h:column>
								<h:commandLink id="linkedCol51" value="#{checks.contractNumber}" styleClass="ovFullCellSelect"/>
							</h:column>
							<h:column>
								<h:inputText id="linkedCol61" value="#{checks.appliedAmount}" styleClass="formLabel" style="width:100px" rendered="#{pc_cashieringDetail.fromDeposit && pc_cashieringDetail.updateMode}"> <!-- NBA228 -->
									<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
								</h:inputText>
								<h:outputText id="linkedCol616" value="#{checks.appliedAmount}" styleClass="formLabel" style="width:100px" rendered="#{pc_cashieringDetail.fromBundle || !pc_cashieringDetail.updateMode}"> <!-- NBA228 -->
									<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
								</h:outputText>
							</h:column>
							<h:column>
								<h:inputText id="linkedCol71" value="#{checks.checkAmount}" styleClass="formLabel" style="width:100px" rendered="#{pc_cashieringDetail.fromDeposit && checks.primaryContractInd && pc_cashieringDetail.updateMode}" > <!-- NBA228 -->
									<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
								</h:inputText>
								<h:outputText id="linkedCol717" value="#{checks.checkAmount}" styleClass="formLabel" style="width:100px" rendered="#{(pc_cashieringDetail.fromBundle && checks.primaryContractInd) || !pc_cashieringDetail.updateMode}" > <!-- NBA228 -->
									<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
								</h:outputText>
							</h:column>
							<h:column>
								<h:commandLink id="linkedCol81" value="#{checks.checkNumber}" styleClass="ovFullCellSelect" rendered="#{checks.primaryContractInd}"/>
							</h:column>
							<!-- Begin NBA228 -->
							<h:column>
								<h:commandLink id="linkedCol91" value="#{checks.checkName}" styleClass="ovFullCellSelect" rendered="#{checks.primaryContractInd}"/>
							</h:column>
							<!-- End NBA228 -->
							<h:column>
								<h:commandLink id="linkedCol01" value="#{checks.companyName}" styleClass="ovFullCellSelect"/> <!-- NBA228 -->
							</h:column>
						</h:dataTable>
					</h:panelGroup> <!-- SPR3483 -->
					
					<h:panelGroup id="bundleDetail4" styleClass="ovTableData" style="width: 100%;height: 30px;" >
			
					</h:panelGroup>
					
		
				</h:column>					
			</h:dataTable>
				
				

			
			<h:dataTable id="checkTable112" styleClass="ovTableData"  rendered="#{pc_cashieringDetail.depositApply}" style="width: 100%;" cellspacing="0" 
			binding="#{pc_cashieringDetail.bundleTable}" value="#{pc_cashieringDetail.results}" var="detail">
				<h:column>
					<h:column>
						<h:outputLabel id="CheckSummaryTitle112" value="#{detail.companyName}" styleClass="ovColText495" />
					</h:column>
					<h:column>
						<h:outputLabel id="CheckSummaryTitle220" value="Bundle:" styleClass="formLabel" style="font-weight: bold;width:50px;"/>
						<h:outputLabel id="CheckSummaryTitle22" value="#{detail.bundleId}" styleClass="ovColText190" />
						<h:outputLabel id="CheckSummaryTitle320" value="Total Amount:" styleClass="formLabel" style="font-weight: bold;width:125px;"/>
						<h:outputLabel id="CheckSummaryTitle32" value="#{pc_cashieringDetail.total}" styleClass="ovColText270" />
					</h:column>
					
					<h:panelGroup id="bundleDetailHeader2" styleClass="ovDivTableHeader" style="width: 100%" >
						<h:panelGrid columns="10" styleClass="ovTableHeader" columnClasses="ovColHdrText70,ovColHdrText70,ovColHdrText170,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText250" cellspacing="0"> <!-- NBA228 -->
							<h:commandLink id="cashieringDetailCol22" value="#{property.cashieringDetailCol2}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol32" value="#{property.cashieringDetailCol3}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol42" value="#{property.cashieringDetailCol4}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol52" value="#{property.cashieringDetailCol5}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol62" value="#{property.cashieringDetailCol6}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol72" value="#{property.cashieringDetailCol7}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol82" value="#{property.cashieringDetailCol8}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol92" value="#{property.cashieringDetailCol9}" styleClass="ovColSorted"/>
							<h:commandLink id="cashieringDetailCol02" value="#{property.cashieringDetailCol10}" styleClass="ovColSorted"/> <!-- NBA228 -->
						</h:panelGrid>
	
					</h:panelGroup>
					
					<h:dataTable id="checkTable22" styleClass="ovTableData" style="width: 100%; " cellspacing="0" 
					binding="#{pc_cashieringDetail.checkTable}" value="#{detail.checks}" var="checks" rowClasses="#{pc_cashieringDetail.rowStyles}"
					columnClasses="ovColText70,ovColText70,ovColText170,ovColText120,ovColText120,ovColText120,ovColText120,ovColText120,ovColText250">
						<h:column>
							<h:selectBooleanCheckbox  id="linkedCol22" value="#{checks.appliedInd}" styleClass="ovFullCellSelectCheckBox"  disabled="#{checks.disableAppliedInd || !pc_cashieringDetail.updateMode}"/> <!-- NBA228 -->
						</h:column>
						<h:column>
							<h:selectBooleanCheckbox id="linkedCol32" value="#{checks.rejectedInd}" styleClass="ovFullCellSelectCheckBox"  disabled="#{checks.disableRejectInd || !pc_cashieringDetail.updateMode}"/> <!-- NBA228 -->
						</h:column>
						<h:column>
							<h:commandLink id="linkedCol42" value="#{checks.primaryInsuredName}" styleClass="ovFullCellSelect"/>
						</h:column>
						<h:column>
							<h:commandLink id="linkedCol52" value="#{checks.contractNumber}" styleClass="ovFullCellSelect"/>
						</h:column>
						<h:column>
							<h:inputText id="linkedCol62" value="#{checks.appliedAmount}" styleClass="formLabel" style="width:120px" rendered="#{pc_cashieringDetail.fromDeposit && pc_cashieringDetail.updateMode}"><!-- NBA228 -->
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:inputText>
							<h:outputText id="linkedCol626" value="#{checks.appliedAmount}" styleClass="formLabel" style="width:120px" rendered="#{pc_cashieringDetail.fromBundle || !pc_cashieringDetail.updateMode}"><!-- NBA228 -->
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
						</h:column>
						<h:column>
							<h:inputText id="linkedCol72" value="#{checks.checkAmount}" styleClass="formLabel" style="width:120px" rendered="#{pc_cashieringDetail.fromDeposit && pc_cashieringDetail.updateMode}"><!-- NBA228 -->
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:inputText>
							<h:outputText id="linkedCol727" value="#{checks.checkAmount}" styleClass="formLabel" style="width:120px" rendered="#{pc_cashieringDetail.fromBundle || !pc_cashieringDetail.updateMode}"><!-- NBA228 -->
								<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
							</h:outputText>
						</h:column>
						<h:column>
							<h:commandLink id="linkedCol82" value="#{checks.checkNumber}" styleClass="ovFullCellSelect" rendered="#{checks.primaryContractInd}"/>
						</h:column>
						<!-- Begin NBA228 -->
						<h:column>
							<h:commandLink id="linkedCol92" value="#{checks.checkName}" styleClass="ovFullCellSelect" rendered="#{checks.primaryContractInd}"/>
						</h:column>
						<!-- End NBA228 -->	
						<h:column>
							<h:commandLink id="linkedCol02" value="#{checks.companyName}" styleClass="ovFullCellSelect"/>
						</h:column>
					</h:dataTable>
					<h:panelGroup id="bundleDetail5" styleClass="ovTableData" style="width: 100%;height: 30px;" >
			
					</h:panelGroup>

				</h:column>					
			</h:dataTable>
			
			
			<h:panelGroup styleClass="ovButtonBar">
				<h:commandButton id="btnCancelView" value="#{property.buttonCancel}" styleClass="formButtonLeft" rendered="#{!pc_cashieringDetail.updateMode}"
				action="#{pc_cashieringDetail.cancelView}" onclick="resetTargetFrame();" />
				<h:commandButton id="btnCancelUpdate" value="#{property.buttonCancel}" styleClass="formButtonLeft" rendered="#{pc_cashieringDetail.updateMode}"
				action="#{pc_cashieringDetail.cancelUpdate}" onclick="resetTargetFrame();" />
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="formButtonLeft" 
				action="#{pc_cashieringDetail.refresh}"  style="left: 140px;" />
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="formButtonRight" 
					disabled="#{pc_cashieringDetail.auth.enablement['Commit'] || !pc_cashieringDetail.updateMode}"
					action="#{pc_cashieringDetail.commit}"  style="left: 1120px;width: 92px;" /> <!-- NBA213 --> <!-- NBA228 -->
			</h:panelGroup>
				
		</h:form>
		
		<div id="Messages" style="display:none">
			<h:messages />
		</div>

	</f:view>
</body>
</html>
