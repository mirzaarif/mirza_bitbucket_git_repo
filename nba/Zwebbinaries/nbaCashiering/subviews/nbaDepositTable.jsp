<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA182            7      Cashiering Rewrite --%>
<%-- NBA228         NB-1101   Cash Management Enhancement --%>
<%-- SPRNBA-747     NB-1401   General Code Clean Up --%>
<%-- NBA416   	    NB-1601   Example Cashiering Auditor Role --%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGrid id="depositTitleHeader" styleClass="sectionSubheader" style="width: 100%" columns="1">
		<h:outputLabel id="depositSummaryTitle" value="#{property.depositTitle}"/>
	</h:panelGrid>
	
	<h:panelGroup id="depositTableHeader" styleClass="ovDivTableHeader" style="width: 100%">
		<h:panelGrid columns="6" styleClass="formTableHeader" cellspacing="0"
				columnClasses="ovColHdrText170,ovColHdrText170,ovColHdrText150,ovColHdrText120,ovColHdrText170,ovColHdrText455">
			<h:commandLink id="depositHdrCol2" value="#{property.depositCol2}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol2}" actionListener="#{pc_depositTable.sortColumn}" />  
			<h:commandLink id="depositHdrCol3" value="#{property.depositCol3}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol3}" actionListener="#{pc_depositTable.sortColumn}" />  
			<h:commandLink id="depositHdrCol4" value="#{property.depositCol4}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol4}" actionListener="#{pc_depositTable.sortColumn}" />  
			<h:commandLink id="depositHdrCol5" value="#{property.depositCol5}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol5}" actionListener="#{pc_depositTable.sortColumn}" />  
			<h:commandLink id="depositHdrCol6" value="#{property.depositCol6}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol6}" actionListener="#{pc_depositTable.sortColumn}" />
		    <h:commandLink id="depositHdrCol7" value="#{property.depositCol7}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_depositTable.sortedByCol7}" actionListener="#{pc_depositTable.sortColumn}" />  
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="depositData" styleClass="ovDivTableData12" style="width: 100%; background-color: white;height:#{pc_depositTable.maxHeight}px">
		<h:dataTable id="depositTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_depositTable.dataTable}" value="#{pc_depositTable.results}" var="depositTableItem"
					rowClasses="#{pc_depositTable.rowStyles}" 
					columnClasses="ovColText170,ovColText170,ovColText150,ovColText120,ovColText170,ovColText455">
			<h:column>
				<h:commandLink id="depositCol2" value="#{depositTableItem.depositDate}" onmousedown="saveTableScrollPosition();" 
							styleClass="ovFullCellSelect#{depositTableItem.eligibility}" style="margin-left: 5px;" action="#{pc_depositTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="depositCol3" value="#{depositTableItem.createBy}" onmousedown="saveTableScrollPosition();" 
							styleClass="ovFullCellSelect#{depositTableItem.eligibility}" action="#{pc_depositTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="depositCol4" value="#{depositTableItem.bundleId}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect#{depositTableItem.eligibility}" action="#{pc_depositTable.selectRow}" immediate="true" />  
			</h:column>
			<h:column>
				<h:commandLink id="depositCol5" value="#{depositTableItem.userId}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect#{depositTableItem.eligibility}" action="#{pc_depositTable.selectRow}" immediate="true" />  
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol50" styleClass="ovFullCellSelect#{depositTableItem.eligibility}" action="#{pc_depositTable.selectRow}">
					<h:outputText id="descriptionCol6" value="#{depositTableItem.totalAmount}" styleClass="formLabel" style="width: 170px">
							<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2"/>
					</h:outputText>
				</h:commandLink>
			</h:column>	
			
			<h:column>
				<h:commandLink id="depositCol7" value="#{depositTableItem.companyName}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect#{depositTableItem.eligibility}" action="#{pc_depositTable.selectRow}" immediate="true" />  
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	
	<h:panelGroup styleClass="ovButtonBar">
		<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="formButtonLeft" 
						action="#{pc_depositTable.refresh}" />
		<%-- NBA228 code deleted --%>
		<h:commandButton id="btnCreateDeposit" value="#{property.buttonCreateDeposit}" styleClass="formButtonRight" style="left: 1120px;width: 92px;"
						action="#{pc_depositTable.createDeposit}" disabled="#{pc_depositTable.disableCreateDeposit || !pc_depositTable.eligibleForLock || pc_depositTable.auth.enablement['Commit']}"/>  <%-- SPRNBA-747,NBA416 --%>
		<%-- Begin NBA228 --%>
		<h:commandButton id="btnView" value="#{property.buttonView}" styleClass="formButtonRight-1" style="left: 900px;"
						action="#{pc_depositTable.view}" disabled="#{pc_depositTable.disableView}" />
		<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight-1" style="left: 1010px;"
						action="#{pc_depositTable.update}" disabled="#{pc_depositTable.disableUpdate || !pc_depositTable.eligibleForLock}" />
		<%-- End NBA228 --%>
		
	</h:panelGroup>
	
	

	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_depositTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.previousPageAbsolute}" />  
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_depositTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.previousPage}" />  
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_depositTable.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.previousPageSet}" />  
		<h:commandLink value="#{pc_depositTable.page1Number}" rendered="#{pc_depositTable.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page1}" />  
		<h:outputText value="#{pc_depositTable.page1Number}" rendered="#{pc_depositTable.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_depositTable.page2Number}" rendered="#{pc_depositTable.showPage2}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page2}" />  
		<h:outputText value="#{pc_depositTable.page2Number}" rendered="#{pc_depositTable.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_depositTable.page3Number}" rendered="#{pc_depositTable.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page3}" />  
		<h:outputText value="#{pc_depositTable.page3Number}" rendered="#{pc_depositTable.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_depositTable.page4Number}" rendered="#{pc_depositTable.showPage4}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page4}" />  
		<h:outputText value="#{pc_depositTable.page4Number}" rendered="#{pc_depositTable.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_depositTable.page5Number}" rendered="#{pc_depositTable.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page5}" />  
		<h:outputText value="#{pc_depositTable.page5Number}" rendered="#{pc_depositTable.currentPage5}" styleClass="ovStatusBarTextBold" />		
		<h:commandLink value="#{pc_depositTable.page6Number}" rendered="#{pc_depositTable.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page6}" />  
		<h:outputText value="#{pc_depositTable.page6Number}" rendered="#{pc_depositTable.currentPage6}" styleClass="ovStatusBarTextBold" />		
		<h:commandLink value="#{pc_depositTable.page7Number}" rendered="#{pc_depositTable.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page7}" />  
		<h:outputText value="#{pc_depositTable.page7Number}" rendered="#{pc_depositTable.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_depositTable.page8Number}" rendered="#{pc_depositTable.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.page8}" />  
		<h:outputText value="#{pc_depositTable.page8Number}" rendered="#{pc_depositTable.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_depositTable.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.nextPageSet}" />  
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_depositTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.nextPage}" />  
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_depositTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_depositTable.nextPageAbsolute}" />  
	</h:panelGroup>
	<h:inputHidden id="searchTableVScroll" value="#{pc_depositTable.VScrollPosition}" />  
