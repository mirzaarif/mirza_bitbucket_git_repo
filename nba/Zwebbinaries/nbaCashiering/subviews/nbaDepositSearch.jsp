<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182            7      Cashiering Rewrite -->
<!-- SPR3482           8      Search Push Button Enabled on View Open of Deposit Correction Tab -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGroup>
		<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeft, formSplitRight" cellpadding="0" cellspacing="0"> 
			<h:column>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="cashdepositDateLabel" value="#{property.cashieringDepositdate}" styleClass="formLabel" style="width: 150px" />
					<h:inputText id="cashdepositDateInput" value="#{pc_depositSearch.depositDate}" onchange="enableButton();" styleClass="formEntryDate" style="width: 100px"><!-- SPR3482 -->
						<f:convertDateTime pattern="#{property.datePattern}"/>
					</h:inputText>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="cashCheckNumberLabel" value="#{property.cashieringCheckNumber}" styleClass="formLabel" style="width: 150px" />
					<h:inputText id="cashCheckNumberInput" value="#{pc_depositSearch.checkNumber}" onchange="enableButton();" styleClass="formEntryText" style="width: 180px"/><!-- SPR3482 -->
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="cashCheckAmountLabel" value="#{property.cashieringCheckAmount}" styleClass="formLabel" style="width: 150px" />
					<h:inputText id="cashCheckAmountInput" value="#{pc_depositSearch.checkAmount}" onchange="enableButton();" styleClass="formEntryText" ><!-- SPR3482 -->
						<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
					</h:inputText>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="cashContractNumberLabel" value="#{property.cashieringContractNumber}" styleClass="formLabel" style="width: 350px" />
					<h:inputText id="cashContractNumberInput" value="#{pc_depositSearch.contractNumber}" onchange="enableButton();" styleClass="formEntryText"
							style="width: 150px" maxlength="15" onkeypress="toUpperCase(event);" /><!-- SPR3482 -->
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="cashLastNameLabel" value="#{property.cashieringLastName}" styleClass="formLabel" style="width: 350px" />
					<h:inputText id="cashLastNameInput" value="#{pc_depositSearch.lastName}" onchange="enableButton();" styleClass="formEntryText" style="width: 150px" 
							onkeypress="toUpperCase(event);" /><!-- SPR3482 -->
				</h:panelGroup>
			</h:column>
			<h:panelGroup styleClass="formButtonBar">
				<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft" style="left: 70px"
					action="#{pc_depositSearch.clear}" onclick="enableButton();" /><!-- SPR3482 -->
				<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight" style="left: 1000px"
					action="#{pc_depositSearch.search}" onclick="resetTargetFrame();" /><!-- SPR3482 -->
			</h:panelGroup>		
		</h:panelGrid>
	</h:panelGroup>
