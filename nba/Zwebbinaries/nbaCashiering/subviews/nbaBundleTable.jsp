<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182           7       Cashiering Workbench Rewrite -->
<!-- NBA228         NB-1101   Cash Management Enhancement -->
<!-- NBA416   	    NB-1601   Example Cashiering Auditor Role -->
<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGrid id="bundleTitleHeader" styleClass="sectionSubheader" style="width: 100%" columns="1">
		<h:outputLabel id="BundleSummaryTitle" value="#{property.bundleTitle}"/>
	</h:panelGrid>
	
	<h:panelGroup id="bundleSummary" styleClass="ovDivTableHeader" style="width: 100%" >
		<!-- Begin NBA228 -->
		<h:panelGrid columns="7" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText170,ovColHdrText410,ovColHdrText210,ovColHdrText155,ovColHdrText70,ovColHdrText170" cellspacing="0">
			<h:outputText id="bundleSummaryCol1" value="" />
			<h:commandLink id="bundleSummaryCol2" value="#{property.bundleSummaryCol1}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol1}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
			<h:commandLink id="bundleSummaryCol3" value="#{property.bundleSummaryCol2}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol2}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
			<h:commandLink id="bundleSummaryCol4" value="#{property.bundleSummaryCol3}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol3}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
			<h:commandLink id="bundleSummaryCol5" value="#{property.bundleSummaryCol4}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol4}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
			<h:commandLink id="bundleSummaryCol6" value="#{property.bundleSummaryCol5}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol5}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
			<h:commandLink id="bundleSummaryCol7" value="#{property.bundleSummaryCol6}" styleClass="ovColSorted#{pc_cashieringSummary.bundleTable.sortedByCol6}" actionListener="#{pc_cashieringSummary.bundleTable.sortColumn}"/>
		</h:panelGrid>
		<!-- End NBA228 -->
	</h:panelGroup>
	
	<!-- Begin NBA228 -->
	<h:panelGroup id="bundleData" styleClass="ovDivTableData" style="width: 100%;height:#{pc_cashieringSummary.bundleTable.maxHeight}px" >
		<h:dataTable id="bundleTable" styleClass="ovTableData" cellspacing="0" 
			binding="#{pc_cashieringSummary.bundleTable.dataTable}" value="#{pc_cashieringSummary.bundleTable.results}" var="bundle"
			rowClasses="#{pc_cashieringSummary.bundleTable.rowStyles}" columnClasses="ovColIcon,ovColText170,ovColText410,ovColText210,ovColText155,ovColText70,ovColText170">
			<h:column>
					<h:commandButton id="descriptionCol1" image="images/needs_attention/close-to-new.gif" title="#{property.buttonCloseNew}" rendered="#{bundle.closeBundleInd}" styleClass="ovViewIconTrue"  /> 
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol2" value="#{bundle.userId}" styleClass="ovFullCellSelect#{bundle.eligibility}" action="#{pc_cashieringSummary.bundleTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol3" value="#{bundle.companyName}" styleClass="ovFullCellSelect#{bundle.eligibility}" action="#{pc_cashieringSummary.bundleTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol4" value="#{bundle.scanStation}" styleClass="ovFullCellSelect#{bundle.eligibility}" action="#{pc_cashieringSummary.bundleTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol5" value="#{bundle.bundleId}" styleClass="ovFullCellSelect#{bundle.eligibility}" action="#{pc_cashieringSummary.bundleTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol6" value="#{bundle.checkCount}" styleClass="ovFullCellSelect#{bundle.eligibility}" style="width: 98%;text-align: right;" action="#{pc_cashieringSummary.bundleTable.selectRow}" immediate="true"/>
			</h:column>
			<h:column>
				<h:commandLink id="descriptionCol50" styleClass="ovFullCellSelect#{bundle.eligibility}" action="#{pc_cashieringSummary.bundleTable.selectRow}">
					<h:outputText id="descriptionCol7" value="#{bundle.totalAmount}" styleClass="formLabel" style="width: 170px">
							<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2"/>
					</h:outputText>
				</h:commandLink>
			</h:column>
		</h:dataTable>

	</h:panelGroup>
	<!-- End NBA228 -->
	<h:panelGroup styleClass="ovButtonBar">
		<!-- NBA228 code deleted -->
		<h:commandButton id="btnClose" value="#{property.buttonCloseBundle}" styleClass="formButtonLeft"  
			action="#{pc_cashieringSummary.bundleTable.closeBundle}" disabled="#{pc_cashieringSummary.bundleTable.disableCloseBundle || !pc_cashieringSummary.bundleTable.eligibleForLock || pc_cashieringSummary.auth.enablement['CloseBundle']}"/> <!-- NBA416 -->
		<h:commandButton id="btnCreate" value="#{property.buttonCreateDeposit}" styleClass="formButtonRight" style="left: 1120px;width: 92px;" 
			action="#{pc_cashieringSummary.bundleTable.createDeposit}" disabled="#{pc_cashieringSummary.bundleTable.disableCreateDeposit || !pc_cashieringSummary.bundleTable.eligibleForLock || pc_cashieringSummary.auth.enablement['Commit']}"/> <!-- NBA416 -->
		<!-- Begin NBA228 -->
		<h:commandButton id="btnView" value="#{property.buttonView}" styleClass="formButtonRight-1" style="left: 815px;" 
			action="#{pc_cashieringSummary.bundleTable.view}" disabled="#{pc_cashieringSummary.bundleTable.disableView}"/>
		<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight-1" style="left: 915px;" 
			action="#{pc_cashieringSummary.bundleTable.update}" disabled="#{pc_cashieringSummary.bundleTable.disableUpdate || !pc_cashieringSummary.bundleTable.eligibleForLock}"/>	
		<h:commandButton id="btnCloseNew" value="#{property.buttonCloseNew}" styleClass="formButtonLeft-1" style="left: 115px;"
			action="#{pc_cashieringSummary.bundleTable.closeToNew}" disabled="#{pc_cashieringSummary.bundleTable.disableCloseNew || !pc_cashieringSummary.bundleTable.eligibleForLock || pc_cashieringSummary.auth.enablement['CloseBundle']}"/> <!-- NBA416 -->
		<h:commandButton id="btnReports" value="#{property.buttonCreateReports}" styleClass="formButtonRight-1" style="left: 1020px;" 
			action="#{pc_cashieringSummary.bundleTable.createReport}" disabled="#{pc_cashieringSummary.bundleTable.disableCreateReports || !pc_cashieringSummary.bundleTable.eligibleForLock}"/>
		<!-- End NBA228 -->	
	</h:panelGroup>

</jsp:root>




