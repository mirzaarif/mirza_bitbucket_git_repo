<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA182           7       Cashiering Workbench Rewrite -->
<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGrid id="cashieringTitleHeader" styleClass="sectionSubheader" style="width: 100%" columns="1">
		<h:outputLabel id="CashieringSummaryTitle" value="#{property.cwTitle}"/>
	</h:panelGrid>
	
	<h:panelGroup id="companySummary" styleClass="ovDivTableHeader" style="width: 100%">
		<h:panelGrid columns="2" styleClass="ovTableHeader" columnClasses="ovColHdrText615,ovColHdrText615" cellspacing="0">
			<h:commandLink id="cwSummaryCol1" value="#{property.cwSummaryCol1}" styleClass="ovColSorted#{pc_cashieringSummary.cashieringTable.sortedByCol1}" actionListener="#{pc_cashieringSummary.cashieringTable.sortColumn}" />
			<h:commandLink id="cwSummaryCol2" value="#{property.cwSummaryCol2}" styleClass="ovColSorted#{pc_cashieringSummary.cashieringTable.sortedByCol2}" actionListener="#{pc_cashieringSummary.cashieringTable.sortColumn}" />
		</h:panelGrid>
	</h:panelGroup>

	<h:panelGroup id="companyData" styleClass="ovDivTableData" style="width: 100%;height:#{pc_cashieringSummary.cashieringTable.maxHeight}px">
		<h:dataTable id="comapnyTable" styleClass="ovTableData" cellspacing="0" rows="0"  
			binding="#{pc_cashieringSummary.cashieringTable.dataTable}" value="#{pc_cashieringSummary.cashieringTable.results}" var="company"
			 rowClasses="#{pc_cashieringSummary.cashieringTable.rowStyles}" columnClasses="ovColText615,ovColText615" >
			<h:column>
				<h:commandLink id="nameCol" value="#{company.companyName}" styleClass="ovFullCellSelect"
					action="#{pc_cashieringSummary.cashieringTable.selectRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="bundlesCol" value="#{company.numberOfBundles}" styleClass="ovFullCellSelect"
					style="width: 98%;text-align: right;" action="#{pc_cashieringSummary.cashieringTable.selectRow}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	
	
	<h:panelGroup styleClass="ovButtonBar">
				<h:commandButton id="btnRetrieve" value="#{property.buttonRetrieve}" styleClass="formButtonRight" style="left: 1120px;"
						action="#{pc_cashieringSummary.cashieringTable.retrieve}" onclick="resetTargetFrame()" disabled="#{pc_cashieringSummary.cashieringTable.disableRetrieve}"/>
	</h:panelGroup>

</jsp:root>


