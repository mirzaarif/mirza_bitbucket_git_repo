<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA182 		  7		  	Cashiering Workbench Rewrite -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Cashiering Summary</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_cashieringSummary'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cashieringSummary'].target='';
			return false;
		}
		
	</script>
	
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_CASHIERINGSUMMARY" value="#{pc_cashieringSummary}" />
		<h:form id="form_cashieringSummary">
			
			<f:subview id="cashieringSummaryInfo" rendered="#{pc_cashieringSummary.showCompany}">
				<c:import url="/nbaCashiering/subviews/nbaCashieringTable.jsp" />
			</f:subview>
			<f:subview id="bundleSummary" rendered="#{pc_cashieringSummary.cashieringTable.showBundles}" >
				<c:import url="/nbaCashiering/subviews/nbaBundleTable.jsp" />
			</f:subview>
				
			<h:panelGroup styleClass="tabButtonBar">
					<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="formButtonLeft" 
						action="#{pc_cashieringSummary.refresh}" immediate="true"/>
			</h:panelGroup>
			
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
