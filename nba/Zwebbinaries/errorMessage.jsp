<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA213            7      Unified User Interface -->
 
<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Error Message Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
</head>
<body>
	ERROR DIALOG
	<f:view>
		<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
		<div id="Messages"><h:messages /></div>
		<div id="MessageTitle"><h:outputText value="#{bundle.error}"/></div>
	</f:view>
	<script type="text/javascript">
		try{
   		    var innerText=document.all["Messages"].innerHTML;
		    innerText=top.getInnerHTML(innerText);		    
		    document.all["Messages"].innerHTML= innerText;  
			if(document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != ""){
				parent.showWindow('<%=path%>','faces/error.jsp', this);
			}
		} catch(err){
		}
		parent.hideWait();
	</script>
</body>
</html>
