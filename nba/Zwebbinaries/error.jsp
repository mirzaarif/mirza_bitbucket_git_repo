 
 <!-- CHANGE LOG -->
 <!-- Audit Number   Version   Change Description -->
 <!-- SPR3356          7       When nbA is deployed on cluster, memory - to - memory session replication fails -->
 <!-- ACEL3757         -       JavaScript error -->
 <!-- NBA341         NB-1401   Internet Explorer 11 Certification -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ page language="java" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>


<html>
<head>
<base href="<%=basePath%>" target="controlFrame">
<title>Error</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var titleName = null;
		var width=360;
		var height=240;
	//-->
	</script>
<script type="text/javascript">
	<!--
	
		
		function enableButtons(){
		}
		
		function initMessages(){
			var details = "";
			try{
				if(window.opener != null && window.opener.document != null &&  window.opener.document.getElementById("Messages") != null){
				    //Begin SPR3356	
			        var innerText=window.opener.document.getElementById("Messages").innerHTML;
		    	    innerText=top.getInnerHTML(innerText);		    
		        	window.opener.document.getElementById("Messages").innerHTML= innerText; 	   
			        //End SPR3356 
					details = window.opener.document.getElementById("Messages").innerHTML;
				} else if(top.controlFrame.document.getElementById("Messages") != null){
				    //Begin SPR3356	
			        var innerText=top.controlFrame.document.getElementById("Messages").innerHTML;
		    	    innerText=top.getInnerHTML(innerText);		    
		        	top.controlFrame.document.getElementById("Messages").innerHTML= innerText; 	   
			        //End SPR3356 
					details = top.controlFrame.document.getElementById("Messages").innerHTML;
				} else {
					details = "Unhandled Exception occurred.  Please contact your system administrator.";
				}
			
				if(window.opener != null && window.opener.document != null && window.opener.document.getElementById("MessageTitle") != null){
					titleName = window.opener.document.getElementById("MessageTitle").innerHTML;
				} else {
					titleName = null;
				}
				details = details.replace(/&amp;/g,"&");
				details = details.replace(/&lt;/g,"<");
				details = details.replace(/&gt;/g,">");						
				details = getWrappedtext(details, 60);
			}catch(err){
				details = "Unhandled Exception occurred.  Please contact your system administrator.";
			}
			
			errorForm.errors.value = details;
		}
		
	
		
 	/**
	 * Extracts a text wrapped version of given text
	 * @author Channel Services Development Team
	 * @param text to wrap
	 * @param wrap point
	 * @return wrapped text
	 */
	function getWrappedtext(source, maxWidth){
		try{
			var remaining = source;
			var current = '';
			while(remaining != null && remaining != ''){
				if( remaining.length > maxWidth){
					// locate first appropriate stopping point in text,
					var position = remaining.indexOf(". ");
					if(position != -1){
						current += remaining.substr(0,position + 1) + "\n\r\n\r";
						remaining = remaining.substr(position + 1);
						while(remaining != null && remaining != "" && remaining.charAt(0) == ' '){
							remaining = remaining.substr(1);
						}
					} else {
						position = remaining.indexOf(",");
						if(position != -1 && position < maxWidth){
							current += remaining.substr(0,position+1) + "\n\r";
							remaining = remaining.substr(position+1);
							while(remaining != null && remaining != "" && remaining.charAt(0) == ' '){
								remaining = remaining.substr(1);
							}
						} else {
							position = remaining.lastIndexOf(' ',maxWidth);
							if(position == -1){
								position = maxWidth;
							}
							if(position == maxWidth && (current.charAt(position + 1) != ' ' && current.charAt(position) != ' ')){
								current += remaining.substr(0,position + 1) + "-\n\r";
								remaining = remaining.substr(position + 1);
							} else {
								current += remaining.substr(0,position + 1) + "\n\r";
								remaining = remaining.substr(position + 1);
							}
						}
					}
				} else {
					current += remaining;
					remaining = '';
				}
			}
			return current;
		} catch (er){
			reportException(er, "getWrappedText");
			return source;
		}
	 }

	function closeErrorDialog(){
		if(top.lastButtonClicked != null){
			try{
				top.lastButtonClicked.isClicked="false";
			}catch(err){
			}
		}
		// begin NBA341
		try{
			var currentPage = top.mainContentFrame.location.href.length;
			if(top.mainContentFrame.location.href.substr(currentPage - 9) == "logon.jsp") {
				top.mainContentFrame.location.href = top.mainContentFrame.location.href; // for Login page
			} else {
				window.opener.focus(); // for the pages other than Login page
			}
		}catch(err){
			reportException(err, "closeErrorDialog");
		}
		// end NBA341		
		// indicate that this close is coming from error dialog
		var clickVal = document.getElementById('errorForm:clicked').innerHTML;			
		if(clickVal=='false'){
			top.closeWindow(null,"true");
		}else {
				top.closeWindow(null,"false");
				top.closeWindow(null,"true");	
		}
	}
	function setTargetFrame() {
		document.forms['errorForm'].target='controlFrame';
		return false;
	}
		
		
		
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="initMessages(); popupInit();this.errorForm.ok.focus();">  <!-- ACEL3757 -->
<f:view>
	<h:form id="errorForm">
	<table width="350" cellpadding="0" cellspacing="0">
		<tbody>
			<tr >
					<td class="text" colspan="2"><textarea readonly="readonly"
						id="errors" rows="15" cols="53" class="text"></textarea></td> <!-- NBA341 -->
			</tr>
			<tr valign="bottom">
					<td align="left"><h:commandButton id="force"
						styleClass="buttonTiny" value="Force"
						action="#{pc_ForceError.submit}"
						rendered="#{pc_ForceError.showForceButton}"
						onclick="setTargetFrame();"></h:commandButton></td>
					<td align="right"><button id="ok" name="ok" class="button"
						onclick="return closeErrorDialog();" tabindex="1">OK</button>
					</td>
				</tr>
				<tr>
					<td><h:outputText id="clicked" value="#{pc_ForceError.click}"
						style="display: none"></h:outputText></td>
			</tr>
		</tbody>
	</table>
	</h:form>
</f:view>
</body>
</html>
