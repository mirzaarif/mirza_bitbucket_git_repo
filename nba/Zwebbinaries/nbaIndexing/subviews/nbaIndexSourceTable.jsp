<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173           7       Indexing UI Rewrite -->
<!-- NBA212           7       Content Services -->
<!-- NBA317         NB-1301   	PCI Compliance For Credit Card Numbers Using Web Service  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="indexingOverviewTitleBar" styleClass="formTitleBar">
		<h:outputLabel id="indexingOverviewTitle" value="#{property.indexingOverviewTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="indexOverviewHeader" styleClass="ovDivTableHeader">
		<h:panelGrid id="indexOverviewHeaderGrid" columns="2" styleClass="ovTableHeader" cellspacing="0"  border="0"
			columnClasses="ovColHdrText200,ovColHdrText410">
			<h:commandLink id="indexingOverviewHdrCol1" actionListener="#{pc_indexTable.sortColumn}" immediate="true"
				value="#{property.indexingOverviewCol1}" styleClass="ovColSorted#{pc_indexTable.sortedByCol1}" />
			<h:commandLink id="indexingOverviewHdrCol2" actionListener="#{pc_indexTable.sortColumn}" immediate="true"
				value="#{property.indexingOverviewCol2}" styleClass="ovColSorted#{pc_indexTable.sortedByCol2}" />
		</h:panelGrid>
	</h:panelGroup>

	<h:panelGroup id="indexingOverviewData" styleClass="ovDivTableData" style="height: 110px;">
		<h:dataTable id="indexingOverviewDataTable" styleClass="ovTableData" cellspacing="0" rows="0" border="0" binding="#{pc_indexTable.dataTable}"
			rowClasses="#{pc_indexTable.rowStyles}" value="#{pc_indexTable.indexDataList}" var="indexRow" 
			columnClasses="ovColText200,ovColText410">
			<h:column>
				<h:commandLink id="image" style="width: 100%;" styleClass="ovFullCellSelect" value="#{indexRow.imageType}"
					action="#{pc_indexTable.selectIndexSourceRow}" onmousedown="handleSourceSelectionChange(this.innerText);resetTargetFrame();"> <!-- NBA212 NBA317 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="message" style="width:100%" styleClass="ovFullCellSelect" value="#{indexRow.validationMessage}"
					action="#{pc_indexTable.selectIndexSourceRow}" onmousedown="resetTargetFrame();"> <!-- NBA212 -->
				</h:commandLink>
			</h:column>
		</h:dataTable>
	</h:panelGroup>

	<h:panelGroup id="PagingBar" styleClass="ovStatusBar">
		<h:commandLink id="previousAbsolute" value="#{property.previousAbsolute}" rendered="#{pc_indexTable.showPrevious}"
			styleClass="ovStatusBarText" action="#{pc_indexTable.previousPageAbsolute}" immediate="true" />
		<h:commandLink id="previousPage" value="#{property.previousPage}" rendered="#{pc_indexTable.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.previousPage}" immediate="true" />
		<h:commandLink id="previousPageSet" value="#{property.previousPageSet}" rendered="#{pc_indexTable.showPreviousSet}"
			styleClass="ovStatusBarText" action="#{pc_indexTable.previousPageSet}" immediate="true" />
		<h:commandLink id="page1Number" value="#{pc_indexTable.page1Number}" rendered="#{pc_indexTable.showPage1}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page1}" immediate="true" />
		<h:outputText id="curpage1Number" value="#{pc_indexTable.page1Number}" rendered="#{pc_indexTable.currentPage1}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page2Number" value="#{pc_indexTable.page2Number}" rendered="#{pc_indexTable.showPage2}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page2}" immediate="true" />
		<h:outputText id="curpage2Number" value="#{pc_indexTable.page2Number}" rendered="#{pc_indexTable.currentPage2}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page3Number" value="#{pc_indexTable.page3Number}" rendered="#{pc_indexTable.showPage3}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page3}" immediate="true" />
		<h:outputText id="curpage3Number" value="#{pc_indexTable.page3Number}" rendered="#{pc_indexTable.currentPage3}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page4Number" value="#{pc_indexTable.page4Number}" rendered="#{pc_indexTable.showPage4}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page4}" immediate="true" />
		<h:outputText id="curpage4Number" value="#{pc_indexTable.page4Number}" rendered="#{pc_indexTable.currentPage4}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page5Number" value="#{pc_indexTable.page5Number}" rendered="#{pc_indexTable.showPage5}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page5}" immediate="true" />
		<h:outputText id="curpage5Number" value="#{pc_indexTable.page5Number}" rendered="#{pc_indexTable.currentPage5}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page6Number" value="#{pc_indexTable.page6Number}" rendered="#{pc_indexTable.showPage6}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page6}" immediate="true" />
		<h:outputText id="curpage6Number" value="#{pc_indexTable.page6Number}" rendered="#{pc_indexTable.currentPage6}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page7Number" value="#{pc_indexTable.page7Number}" rendered="#{pc_indexTable.showPage7}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page7}" immediate="true" />
		<h:outputText id="curpage7Number" value="#{pc_indexTable.page7Number}" rendered="#{pc_indexTable.currentPage7}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="page8Number" value="#{pc_indexTable.page8Number}" rendered="#{pc_indexTable.showPage8}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.page8}" immediate="true" />
		<h:outputText id="curpage8Number" value="#{pc_indexTable.page8Number}" rendered="#{pc_indexTable.currentPage8}"
			styleClass="ovStatusBarTextBold" />
		<h:commandLink id="nextPageSet" value="#{property.nextPageSet}" rendered="#{pc_indexTable.showNextSet}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.nextPageSet}" immediate="true" />
		<h:commandLink id="nextPage" value="#{property.nextPage}" rendered="#{pc_indexTable.showNext}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.nextPage}" immediate="true" />
		<h:commandLink id="nextAbsolute" value="#{property.nextAbsolute}" rendered="#{pc_indexTable.showNext}" styleClass="ovStatusBarText"
			action="#{pc_indexTable.nextPageAbsolute}" immediate="true" />
	</h:panelGroup>
</jsp:root>
