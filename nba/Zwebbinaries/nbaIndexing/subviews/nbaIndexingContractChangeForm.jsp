<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            7      Indexing UI Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formTitleBar">
		<h:outputLabel value="#{property.contractChangeTitleBar}" styleClass="shTextLarge" />
	</h:panelGroup>

	<h:panelGroup id="companyLine" styleClass="formDataEntryTopLine">
		<h:outputText id="companyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width:160px" />
		<h:selectOneMenu id="companyList" style="width :440px" styleClass="formEntryTextHalf"
			value="#{pc_indexCommon.company}">
			<f:selectItems value="#{pc_indexCommon.companyList}" />
		</h:selectOneMenu>
	</h:panelGroup>

	<h:panelGroup id="contractLine" styleClass="formDataEntryLine">
		<h:outputText id="contractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width:160px" />
		<h:inputText id="contract" styleClass="formEntryText" style="width :440px" value="#{pc_indexCommon.policyNumber}" maxlength="15"/>
	</h:panelGroup>

	<h:panelGroup id="changeStateLine" styleClass="formDataEntryLine">
		<h:outputText id="changeStateProvinceLabel" value="#{property.changeStateProvinceLabel}" styleClass="formLabel"
			style="width:160px" />
		<h:selectOneMenu id="changeStateProvinceCombo" style="width :440px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.changeState}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.stateProvinceList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="cntChgFormLine" styleClass="formDataEntryLineBottom">
		<h:outputText id="cntChgForm" value="#{property.formLabel}" styleClass="formLabel" style="width:160px" />
		<h:selectOneMenu id="cntChgFormsList" style="width :440px" styleClass="formEntryTextHalf" value="#{pc_indexTable.selectedIndexRowBean.formNumber}">
			<f:selectItems id="cntChgFormsValue" value="#{pc_indexCommon.reqFormList}" />
		</h:selectOneMenu>
	</h:panelGroup>

</jsp:root>
