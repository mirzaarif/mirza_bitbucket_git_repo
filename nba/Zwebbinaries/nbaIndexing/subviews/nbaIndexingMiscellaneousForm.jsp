<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            7      Indexing UI Rewrite -->
<!-- SPR3558		   8	  Blank Indexing View Returned when Split Multiple Check Box is Selected for CWA Check on NBAPPLCTN and for Payment Check on NBPAYMENT  -->
<!-- FNB011             NB-1101 Work Tracking --> 
<!-- NBA231             NB-1101 Replacement Processing -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->
<!-- NBA340		    NB-1501     Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup id="formTitle" styleClass="formTitleBar"> <!-- SPR3558 -->
		<h:outputLabel id="miscTitle" value="#{property.miscellaneousTitleBar}" styleClass="shTextLarge" /> <!-- SPR3558 -->
	</h:panelGroup>
	<h:panelGroup id="companyLine" styleClass="formDataEntryTopLine">
		<h:outputText id="companyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="companyList" style="width: 450px" styleClass="formEntryText"
			value="#{pc_indexCommon.company}" onchange="resetTargetFrame();submitForm();"> <!-- SPRNBA-659 -->
			<f:selectItems id="companyList2" value="#{pc_indexCommon.companyList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractLine" styleClass="formDataEntryLine" style="margin-top: 3px">
		<h:outputText id="contractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="contract" styleClass="formEntryText" style="width: 450px" maxlength="15"
			value="#{pc_indexCommon.policyNumber}" />
	</h:panelGroup>
	<h:panelGroup id="planLine" styleClass="formDataEntryLine">
		<h:outputText id="planLabel" value="#{property.planLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="planList" style="width: 450px" styleClass="formEntryText"
			value="#{pc_indexCommon.plan}" onchange="submitForm();"> <!-- SPRNBA-659 -->
			<f:selectItems id="planList2" value="#{pc_indexCommon.planList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid columns="1" binding="#{pc_indexTable.partyGrid}" id="PartyGrid" cellspacing="0" cellpadding="0"> 
		<h:panelGroup id="pInsCheckBoxLine" styleClass="formDataEntryLine" rendered="#{pc_indexTable.sourceAttachedToApplication}">
			<h:outputText id="emptySpaceSplit" value="" style="width:145px" />
			<h:selectBooleanCheckbox id="otherThanPInsCheckbox" value="#{pc_indexTable.selectedIndexRowBean.otherThanPIns}" onclick="submitForm();" valueChangeListener="#{pc_indexTable.checkOtherThanPrimaryInsured}"/>  <!-- SPRNBA-659 -->
			<h:outputText id="otherThanPrimaryInsured" value="#{property.otherThanPrimaryInsuredLabel}" style="width:175px"
				styleClass="formLabel" />
		</h:panelGroup>
		<h:panelGroup id="misclastNameLine" styleClass="formDataEntryLine">
			<h:outputText id="lastNameLabel" value="#{property.lastNameLabel}" styleClass="formLabel" style="width:150px" />
			<h:inputText id="lastName" styleClass="formEntryText" style="width: 450px"
				value="#{pc_indexTable.selectedIndexRowBean.indexParty.lastName}" maxlength="21"
				rendered="#{pc_indexTable.renderEnterablePartyFields}"/>
			<h:outputText id="lastNameDisplay" styleClass="formEntryText" style="width :450px"
				value="#{pc_indexCommon.indexParty.lastName}" 
				rendered="#{pc_indexTable.renderDisplayablePartyFields}"/>
		</h:panelGroup>
		<h:panelGroup id="firstNameLine" styleClass="formDataEntryLine">
			<h:outputText id="firstNameLabel" value="#{property.firstNameLabel}" styleClass="formLabel" style="width:150px" />
			<h:inputText id="firstName" styleClass="formEntryText" style="width :450px"
				value="#{pc_indexTable.selectedIndexRowBean.indexParty.firstName}" maxlength="20"
				rendered="#{pc_indexTable.renderEnterablePartyFields}"/>
			<h:outputText id="firstNameDisplay" styleClass="formEntryText" style="width :450px"
				value="#{pc_indexCommon.indexParty.firstName}" 
				rendered="#{pc_indexTable.renderDisplayablePartyFields}"/>
		</h:panelGroup>
		<h:panelGroup id="middleInitialLine" styleClass="formDataEntryLine">
			<h:outputText id="middleInitialLabel" value="#{property.middleInitialLabel}" styleClass="formLabel"
				style="width:150px" />
			<h:inputText id="middleInitial" styleClass="formEntryText" style="width :450px"
				value="#{pc_indexTable.selectedIndexRowBean.indexParty.middleInitial}"
				rendered="#{pc_indexTable.renderEnterablePartyFields}"/>	<!-- FNB011 -->
			<h:outputText id="middleInitialDisplay" styleClass="formEntryText" style="width :450px"
				value="#{pc_indexCommon.indexParty.middleInitial}" 
				rendered="#{pc_indexTable.renderDisplayablePartyFields}"/>	<!-- FNB011 -->
		</h:panelGroup>
	</h:panelGrid>
	<h:panelGroup id="miscdobGenderLine" styleClass="formDataEntryLine"> 
		<h:outputText id="miscDateOfBirthLabel" value="#{property.dateOfBirthLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="miscDateofBirth" styleClass="formEntryText" value="#{pc_indexCommon.indexParty.dateOfBirth}" 
					style="width:135px" maxlength="10" onchange="submitForm(this);">
			<f:convertDateTime pattern="#{property.datePattern}" /> 
		</h:inputText>
		<h:outputText id="miscgenderLabel" value="#{property.genderLabel}" styleClass="formLabel" style="width: 160px" />
		<h:selectOneMenu id="miscgenderDD" value="#{pc_indexCommon.indexParty.gender}" styleClass="formEntryText"
					style="width: 155px; vertical-align: middle">
			<f:selectItems id="miscgenderList" value="#{pc_indexCommon.indexParty.genderList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid id="TaxIdenGrid" columns="2" cellspacing="0" cellpadding="0" 
				style="width: 605px; vertical-align: text-top" columnClasses="formLabelTable, formEntryText"> <!-- NBA231 -->
		<h:outputText id="taxInformationLabel" value="#{property.taxInformationLabel}" styleClass="formLabel" style="width: 150px; margin-top: 10px" />
	
		<h:panelGrid id="irsnGrid" columns="2" cellspacing="0" cellpadding="0" border="0" style="width: 455px;"
					columnClasses="formEntryText">
			<h:column>
				<h:selectOneRadio id="irstRadio" value="#{pc_indexCommon.indexParty.taxInformation}" layout="pageDirection"
					styleClass="formEntryText" style="height: 100px; width: 155px" rendered="#{pc_indexTable.renderDisplayablePartyFields}" disabled="#{!pc_indexTable.selectedIndexRowBean.otherThanPIns}">
					<f:selectItems id="taxInformationList1" value="#{pc_indexCommon.indexParty.taxInformationList}" /> <!-- SPR3558 -->
				</h:selectOneRadio>
				<h:selectOneRadio id="govtIdType" value="#{pc_indexTable.selectedIndexRowBean.indexParty.taxInformation}" layout="pageDirection"
					styleClass="formEntryText" style="height: 100px; width: 155px" rendered="#{pc_indexTable.renderEnterablePartyFields}"><!--NBA340-->
					<f:selectItems id="taxInformationList2" value="#{pc_indexTable.selectedIndexRowBean.indexParty.taxInformationList}" /> <!-- SPR3558 -->
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:panelGrid id="irsnInputGrid" border="0" cellpadding="5" cellspacing="0" columns="1"
					rendered="#{pc_indexTable.renderEnterablePartyFields}">
					<!-- begin NBA340 -->					
					<h:inputText id="govtIdInput" style="width: 290px;margin-top:70px;" styleClass="formEntryText" maxlength="9"
						value="#{pc_indexTable.selectedIndexRowBean.indexParty.ssn}">						 
					</h:inputText>						
					<h:inputHidden id="govtIdKey" value="#{pc_indexTable.selectedIndexRowBean.indexParty.govtIdKey}"></h:inputHidden>
					<!-- end NBA340 -->
				</h:panelGrid>
				<h:panelGrid id="irsnOutputGrid" border="0" cellpadding="0" cellspacing="0" columns="1" 
					rendered="#{pc_indexTable.renderDisplayablePartyFields}" style="margin-left: 15px;width: 290px; ">
					<h:outputText id="ssnDisplay" style="height:23px; margin-top: 0px;" styleClass="formEntryText" 
						value="#{pc_indexCommon.indexParty.ssn}" >						 
					</h:outputText>	
					<h:outputText id="tinDisplay" style="height:23px; margin-bottom: 6px; margin-top: 8px;" styleClass="formEntryText"
						value="#{pc_indexCommon.indexParty.tin}" >						
					</h:outputText>	
					<h:outputText id="sinDisplay" style="height:23px; margin-bottom: -10px;" styleClass="formEntryText" 
						value="#{pc_indexCommon.indexParty.sin}" >						
					</h:outputText>
				</h:panelGrid>
			</h:column>
		</h:panelGrid>
	</h:panelGrid>
	<h:panelGroup id="requirementVendorLine" styleClass="formDataEntryLine">
		<h:outputText id="requirementVendor" value="#{property.requirementVendorLabel}" styleClass="formLabel"
			style="width:150px" />
		<h:selectOneMenu id="requirementVendorList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.requirementVendor}">
			<f:selectItems id="requirementVendorList2" value="#{pc_indexTable.selectedIndexRowBean.requirementVendorList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="requirementTypeLine" styleClass="formDataEntryLine">
		<h:outputText id="requirementType" value="#{property.requirementTypeLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="requirementTypeList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.requirementType}">
			<f:selectItems id="requirementTypeList2" value="#{pc_indexTable.selectedIndexRowBean.requirementTypeList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="formLine" styleClass="formDataEntryLine">
		<h:outputText id="form" value="#{property.formLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="formsList" style="width :450px" styleClass="formEntryTextHalf" value="#{pc_indexTable.selectedIndexRowBean.formNumber}">
			<f:selectItems id="reqFormList" value="#{pc_indexCommon.reqFormList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="names" styleClass="formDataEntryLine"> <!-- SPR3558 -->
		<h:outputLabel id="oldrName" value="#{property.reqDoctor}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="itdrFirstName" value="#{pc_indexTable.selectedIndexRowBean.drFirstName}" styleClass="entryFieldShort" style="width: 215px;" maxlength="19"/> 
		<h:inputText id="itdrMI" value="#{pc_indexTable.selectedIndexRowBean.drMiddleName}" styleClass="entryFieldSingleChar" maxlength="1"/> 
		<h:inputText id="itdrLastName" value="#{pc_indexTable.selectedIndexRowBean.drLastName}" styleClass="entryFieldName" style="width: 215px" maxlength="20"/> 
	</h:panelGroup>
</jsp:root>
