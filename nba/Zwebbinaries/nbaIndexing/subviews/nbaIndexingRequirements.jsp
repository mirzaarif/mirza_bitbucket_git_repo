<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            7      Indexing UI Rewrite -->
<!-- FNB011             NB-1101 Work Tracking -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->
<!-- NBA340		    NB-1501     Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup id="reqTitleBarGrp" styleClass="formTitleBar">
		<h:outputLabel id="reqTitleBar" value="#{property.requirementsTitleBar}" styleClass="shTextLarge" />
	</h:panelGroup>

	<h:panelGroup id="reqcompanyLine" styleClass="formDataEntryTopLine">
		<h:outputText id="reqcompanyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reqcompanyList" style="width: 450px" styleClass="formEntryText"
			value="#{pc_indexCommon.company}" onchange="resetTargetFrame();submitForm();"> <!-- SPRNBA-659 -->
			<f:selectItems id="reqcompanyListValue" value="#{pc_indexCommon.companyList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="reqContractLine" styleClass="formDataEntryLine">
		<h:outputText id="reqContractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reqContract" styleClass="formEntryText" style="width: 450px" maxlength="15"
			value="#{pc_indexCommon.policyNumber}" />
	</h:panelGroup>
	<h:panelGroup id="reqPlanLine" styleClass="formDataEntryLine">
		<h:outputText id="reqPlanLabel" value="#{property.planLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reqPlanList" style="width: 450px" styleClass="formEntryText"
			value="#{pc_indexCommon.plan}" onchange="submitForm();">
			<f:selectItems id="reqPlanListValue" value="#{pc_indexCommon.planList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="reqLastNameLine" styleClass="formDataEntryLine">
		<h:outputText id="reqLastNameLabel" value="#{property.lastNameLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reqLastName" styleClass="formEntryText" style="width :450px" maxlength="21"
			value="#{pc_indexCommon.indexParty.lastName}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>
		<h:outputText id="reqLastNameDisplay" styleClass="formEntryText" style="width: 450px"
			value="#{pc_indexCommon.indexParty.lastName}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGroup id="reqfirstNameLine" styleClass="formDataEntryLine">
		<h:outputText id="reqfirstNameLabel" value="#{property.firstNameLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reqfirstName" styleClass="formEntryText" style="width: 450px" maxlength="20"
			value="#{pc_indexCommon.indexParty.firstName}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>
		<h:outputText id="reqfirstNameDisplay" styleClass="formEntryText" style="width: 450px"
			value="#{pc_indexCommon.indexParty.firstName}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGroup id="reqmiddleInitialLine" styleClass="formDataEntryLine">
		<h:outputText id="reqmiddleInitialLabel" value="#{property.middleInitialLabel}" styleClass="formLabel"
			style="width:150px" />
		<h:inputText id="reqmiddleInitial" styleClass="formEntryText" style="width: 450px" 
			value="#{pc_indexCommon.indexParty.middleInitial}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>	<!-- FNB011 -->
		<h:outputText id="reqmiddleInitialDisplay" styleClass="formEntryText" style="width :450px"
			value="#{pc_indexCommon.indexParty.middleInitial}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGroup id="reqdobGenderLine" styleClass="formDataEntryLine"> 
		<h:outputText id="reqDateOfBirthLabel" value="#{property.dateOfBirthLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reqDateofBirth" styleClass="formEntryText" value="#{pc_indexCommon.indexParty.dateOfBirth}" 
					style="width:135px" maxlength="10">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
		<h:outputText id="reqgenderLabel" value="#{property.genderLabel}" styleClass="formLabel" style="width: 160px" />
		<h:selectOneMenu id="reqgenderDD" value="#{pc_indexCommon.indexParty.gender}" styleClass="formEntryText"
					style="width: 155px; vertical-align: middle">
			<f:selectItems id="reqgenderList" value="#{pc_indexCommon.indexParty.genderList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid id="reqTaxIdenGrid" columns="2" cellspacing="0" cellpadding="0" binding="#{pc_indexTable.taxIdenGrid}"
				style="width: 605px; vertical-align: text-top" columnClasses="formLabelTable, formEntryText"> 
		<h:outputText id="reqtaxInformationLabel" value="#{property.taxInformationLabel}" styleClass="formLabel" style="width: 150px; margin-top: 10px" />
	
		<h:panelGrid id="reqirsnGrid" columns="2" cellspacing="0" cellpadding="0" border="0" style="width: 455px;" columnClasses="formEntryText">
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_indexCommon.indexParty.taxInformation}" layout="pageDirection"
					styleClass="formEntryText" style="height: 100px; width: 155px" disabled="#{pc_indexTable.sourceAttachedToApplication}"> <!-- NBA340 -->
					<f:selectItems id="reqGovtIDRadioValue" value="#{pc_indexCommon.indexParty.taxInformationList}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:panelGrid id="reqGovtIDGrid" border="0" cellpadding="5" cellspacing="0" columns="1"
							rendered="#{!pc_indexTable.sourceAttachedToApplication}">
					<!-- begin NBA340 -->
					<h:inputText id="govtIdInput" style="width: 290px;margin-top:70px;" styleClass="formEntryText" maxlength="9"
						value="#{pc_indexCommon.indexParty.ssn}">						 
					</h:inputText>						
					<h:inputHidden id="govtIdKey" value="#{pc_indexCommon.indexParty.govtIdKey}"></h:inputHidden>
					<!-- end NBA340 -->
				</h:panelGrid>
				<h:panelGrid id="reqGovtIDGridDisplay" border="0" cellpadding="0" cellspacing="0" columns="1" 
							rendered="#{pc_indexTable.sourceAttachedToApplication}" style="margin-left: 15px;width: 290px; ">
					<h:outputText id="reqssnDisplay" style="height:23px; margin-top: 0px;" styleClass="formEntryText" 
								value="#{pc_indexCommon.indexParty.ssn}" >						
					</h:outputText>	
					<h:outputText id="reqtinDisplay" style="height:23px; margin-bottom: 6px; margin-top: 8px;" styleClass="formEntryText"
								value="#{pc_indexCommon.indexParty.tin}" >						
					</h:outputText>	
					<h:outputText id="reqsinDisplay" style="height:23px; margin-bottom: -10px;" styleClass="formEntryText" 
								value="#{pc_indexCommon.indexParty.sin}" >						
					</h:outputText>	
				
				</h:panelGrid>
			</h:column>
		</h:panelGrid>
	</h:panelGrid>
	
	<h:panelGroup id="requirementVendorLine" styleClass="formDataEntryLine">
		<h:outputText id="requirementVendor" value="#{property.requirementVendorLabel}" styleClass="formLabel"
			style="width:150px" />
		<h:selectOneMenu id="requirementVendorList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.requirementVendor}">
			<f:selectItems id="requirementVendorListValue" value="#{pc_indexTable.selectedIndexRowBean.requirementVendorList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="requirementTypeLine" styleClass="formDataEntryLine">
		<h:outputText id="requirementType" value="#{property.requirementTypeLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="requirementTypeList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.requirementType}">
			<f:selectItems id="requirementTypeListValue" value="#{pc_indexTable.selectedIndexRowBean.requirementTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="reqformLine" styleClass="formDataEntryLine">
		<h:outputText id="reqform" value="#{property.formLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reqformsList" style="width :450px" styleClass="formEntryTextHalf" value="#{pc_indexTable.selectedIndexRowBean.formNumber}">
			<f:selectItems id="reqformsListValue" value="#{pc_indexCommon.reqFormList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" id="reqDoctorNameGrp">
		<h:outputLabel id="reqoldrName" value="#{property.reqDoctor}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reqitdrFirstName" value="#{pc_indexTable.selectedIndexRowBean.drFirstName}" styleClass="entryFieldShort" style="width: 215px;" maxlength="19"/> 
		<h:inputText id="reqitdrMI" value="#{pc_indexTable.selectedIndexRowBean.drMiddleName}" styleClass="entryFieldSingleChar" maxlength="1"/> 
		<h:inputText id="reqitdrLastName" value="#{pc_indexTable.selectedIndexRowBean.drLastName}" styleClass="entryFieldName" style="width: 215px" maxlength="20"/> 
	</h:panelGroup>
</jsp:root>