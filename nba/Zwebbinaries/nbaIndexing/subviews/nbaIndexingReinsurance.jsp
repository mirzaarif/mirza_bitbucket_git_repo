<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            7      Indexing UI Rewrite -->
<!-- SPR3558            8      Blank Indexing View Returned  -->
<!-- FNB011             NB-1101 Work Tracking -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->
<!-- NBA340		    NB-1501     Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="formTitle" styleClass="formTitleBar"> <!-- SPR3558 -->
		<h:outputLabel id="reinsTitle" value="#{property.reinsuranceTitleBar}" styleClass="shTextLarge" /> <!-- SPR3558 -->
	</h:panelGroup>

	<h:panelGroup id="reincompanyLine" styleClass="formDataEntryTopLine">
		<h:outputText id="reincompanyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reincompanyList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexCommon.company}" onchange="submitForm();"> <!-- SPRNBA-659 -->
			<f:selectItems id="reincompanyList2" value="#{pc_indexCommon.companyList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="reinContractLine" styleClass="formDataEntryLine">
		<h:outputText id="reinContractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reinContract" styleClass="formEntryText" style="width :450px" maxlength="15"
			value="#{pc_indexCommon.policyNumber}"/>
	</h:panelGroup>
	<h:panelGroup id="reinPlanLine" styleClass="formDataEntryLine">
		<h:outputText id="reinPlanLabel" value="#{property.planLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reinPlanList" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexCommon.plan}" onchange="submitForm();">  <!-- SPRNBA-659 -->
			<f:selectItems id="reinPlanList2" value="#{pc_indexCommon.planList}" /> <!-- SPR3558 -->
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="reinLastNameLine" styleClass="formDataEntryLine">
		<h:outputText id="reinLastNameLabel" value="#{property.lastNameLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reinLastName" styleClass="formEntryText" style="width :450px" maxlength="21"
			value="#{pc_indexCommon.indexParty.lastName}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>
		<h:outputText id="reinLastNameDisplay" styleClass="formEntryText" style="width :450px"
			value="#{pc_indexCommon.indexParty.lastName}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGroup id="reinfirstNameLine" styleClass="formDataEntryLine">
		<h:outputText id="reinfirstNameLabel" value="#{property.firstNameLabel}" styleClass="formLabel" style="width:150px" />
		<h:inputText id="reinfirstName" styleClass="formEntryText" style="width :450px" maxlength="20"
			value="#{pc_indexCommon.indexParty.firstName}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>
		<h:outputText id="reinfirstNameDisplay" styleClass="formEntryText" style="width :450px"
			value="#{pc_indexCommon.indexParty.firstName}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGroup id="reinmiddleInitialLine" styleClass="formDataEntryLine">
		<h:outputText id="reinmiddleInitialLabel" value="#{property.middleInitialLabel}" styleClass="formLabel"
			style="width:150px" />
		<h:inputText id="reinmiddleInitial" styleClass="formEntryText" style="width :450px" 
			value="#{pc_indexCommon.indexParty.middleInitial}" 
			rendered="#{!pc_indexTable.sourceAttachedToApplication}"/>	<!-- FNB011 -->
		<h:outputText id="reinmiddleInitialDisplay" styleClass="formEntryText" style="width :450px"
			value="#{pc_indexCommon.indexParty.middleInitial}" 
			rendered="#{pc_indexTable.sourceAttachedToApplication}"/>
	</h:panelGroup>
	<h:panelGrid id="reindobGenderLine" styleClass="formDataEntryLine" columns="2" border="0"> 
		<h:panelGroup id="reinDateOfBirthLine" styleClass="formDataEntryLine" style="width:295px">
			<h:outputText id="reinDateOfBirthLabel" value="#{property.dateOfBirthLabel}" style="width:150px" styleClass="formLabel" />
			<h:inputText id="reincheckDate" styleClass="formEntryText" value="#{pc_indexCommon.indexParty.dateOfBirth}" style="width :135px" maxlength="10">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		<h:panelGroup id="reingenderLine" styleClass="formDataEntryLine" style="width:295px">
			<h:outputText id="reingenderLabel" value="#{property.genderLabel}" style="width :125px" styleClass="formLabel" />
			<h:selectOneMenu id="reingenderDD" value="#{pc_indexCommon.indexParty.gender}" styleClass="formEntryTextHalf"
				style="width: 155px;">
				<f:selectItems id="reingenderList" value="#{pc_indexCommon.indexParty.genderList}" />
			</h:selectOneMenu>
			<h:outputText id="blank" value=" " style="width: 15px;"></h:outputText>
		</h:panelGroup>
	</h:panelGrid>
	<h:panelGroup id="reintaxInformationLine" styleClass="formDataEntryLine">
		<h:outputText id="reintaxInformationLabel" value="#{property.taxInformationLabel}" styleClass="formLabel"
			style="width:150px" />

		<h:panelGrid id="reintaxInformationLine2" columns="3" cellspacing="0" cellpadding="0" border="0"
			columnClasses="labelAppEntry,labelAppEntry,colMIBLong"> <!-- SPR3558 -->
			<h:column id="reinblankAmountUnitsColumn"> <!-- SPR3558 -->
				<h:outputText id="reinblankAmountUnits" style="width :142px;" value=""></h:outputText>
			</h:column>

			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_indexCommon.indexParty.taxInformation}" layout="pageDirection"
					styleClass="formEntryText" style="height: 100px" disabled="#{pc_indexTable.sourceAttachedToApplication}">  <!-- SPR3558 --><!-- NBA340 -->
					<f:selectItems id="taxInformationList" value="#{pc_indexCommon.indexParty.taxInformationList}" /> <!-- SPR3558 -->
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:panelGrid id="sourceAttachedToApplication" border="0" cellpadding="5" cellspacing="0" columns="1"
					rendered="#{!pc_indexTable.sourceAttachedToApplication}"> <!-- SPR3558 -->
					<!-- begin NBA340 -->
					<h:inputText id="govtIdInput" style="width: 290px;margin-top:70px;" styleClass="formEntryText" maxlength="9"
						value="#{pc_indexCommon.indexParty.ssn}">						 
					</h:inputText>						
					<h:inputHidden id="govtIdKey" value="#{pc_indexCommon.indexParty.govtIdKey}"></h:inputHidden>
					<!-- end NBA340 -->
				</h:panelGrid>
				<h:panelGrid id="reinGovtIDGridDisplay" border="0" cellpadding="0" cellspacing="0" columns="1" 
					rendered="#{pc_indexTable.sourceAttachedToApplication}" style="margin-left: 15px;width: 308px; ">
					<h:outputText id="reinssnDisplay" style="height:23px; margin-top: 0px;" styleClass="formEntryText" 
						value="#{pc_indexCommon.indexParty.ssn}">						
					</h:outputText>	
					<h:outputText id="reintinDisplay" style="height:23px; margin-bottom: 6px; margin-top: 8px;" styleClass="formEntryText"
						value="#{pc_indexCommon.indexParty.tin}">						
					</h:outputText>	
					<h:outputText id="reinsinDisplay" style="height:23px; margin-bottom: -10px;" styleClass="formEntryText" 
						value="#{pc_indexCommon.indexParty.sin}">						
					</h:outputText>						
				</h:panelGrid>
			</h:column>				
		</h:panelGrid>

	</h:panelGroup>
	<h:panelGroup id="reinsuranceCompanyLine" styleClass="formDataEntryLine">
		<h:outputText id="reinsuranceCompanyLabel" value="#{property.reinsuranceCompanyLabel}" styleClass="formLabel" style="width:150px" />
		<h:selectOneMenu id="reinsuranceCompanyListsd" style="width :450px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.reinVendor}">
			<f:selectItems id="reinsuranceCompanyListValue" value="#{pc_indexTable.selectedIndexRowBean.reinsuranceCompanyList}" />
		</h:selectOneMenu>
	</h:panelGroup>

</jsp:root>

