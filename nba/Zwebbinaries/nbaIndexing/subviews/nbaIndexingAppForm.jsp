<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173           7       Indexing UI Rewrite-->
<!-- NBA139           7       Plan Code Determination -->
<!-- SPR3709          8       Blank Index view is returned  when tried to Index the Applications " Formal originating from Trial " or "Trial" -->
<!-- NBA251           8       nbA Case Manager and Companion Case Assignment -->
<!-- FNB011             NB-1101 Work Tracking -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->
<!-- NBA340		    NB-1501     Mask Government ID -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formTitleBar">
		<h:outputLabel value="#{property.applicationTitleBar}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGrid id="typeLine" styleClass="formDataEntryTopLine" columns="2" cellspacing="0" style="width: 600px">
		<h:outputText id="typeLabel" value="Type:" styleClass="formLabel" style="width: 175px" />
		<h:selectOneRadio value="#{pc_indexTable.selectedIndexRowBean.type}" layout="lineDirection"
			styleClass="formEntryText" style="width: 425px" onclick="resetTargetFrame();submitForm();"><!-- NBA139,SPR3709 SPRNBA-659 -->
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.typeList}" />
		</h:selectOneRadio>
	</h:panelGrid>
	<h:panelGroup id="companyLine" styleClass="formDataEntryLine">
		<h:outputText id="companyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width :175px" />
		<h:selectOneMenu id="companyList" style="width: 425px" styleClass="formEntryText"
			value="#{pc_indexCommon.company}" onchange="resetTargetFrame();submitForm();"> <!-- SPRNBA-659 -->
			<f:selectItems value="#{pc_indexCommon.companyList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="contractLine" styleClass="formDataEntryLine">
		<h:outputText id="contractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width :175px" />
		<h:inputText id="contract" styleClass="formEntryText" style="width: 425px"
			value="#{pc_indexCommon.policyNumber}" maxlength="15"/>
	</h:panelGroup>
	<h:panelGroup id="planLine" styleClass="formDataEntryLine">
		<h:outputText id="planLabel" value="#{property.planLabel}" styleClass="formLabel" style="width :175px" />
		<h:selectOneMenu id="planList" style="width: 425px" styleClass="formEntryTextHalf"
			value="#{pc_indexCommon.plan}">
			<f:selectItems value="#{pc_indexCommon.planList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="lastNameLine" styleClass="formDataEntryLine">
		<h:outputText id="lastNameLabel" value="#{property.lastNameLabel}" styleClass="formLabel" style="width :175px" />
		<h:inputText id="lastName" styleClass="formEntryText" style="width: 425px" maxlength="21"
			value="#{pc_indexCommon.indexParty.lastName}" />
	</h:panelGroup>
	<h:panelGroup id="firstNameLine" styleClass="formDataEntryLine">
		<h:outputText id="firstNameLabel" value="#{property.firstNameLabel}" styleClass="formLabel" style="width :175px" />
		<h:inputText id="firstName" styleClass="formEntryText" style="width: 425px" maxlength="20"
			value="#{pc_indexCommon.indexParty.firstName}" />
	</h:panelGroup>
	<h:panelGroup id="middleInitialLine" styleClass="formDataEntryLine">
		<h:outputText id="middleInitialLabel" value="#{property.middleInitialLabel}" styleClass="formLabel"
			style="width :175px" />
		<h:inputText id="middleInitial" styleClass="formEntryText" style="width: 425px" 
			value="#{pc_indexCommon.indexParty.middleInitial}" />	<!-- FNB011 -->
	</h:panelGroup>
	<h:panelGrid id="taxInformation" columns="2" cellspacing="0" cellpadding="0"
				style="width: 605px; vertical-align: text-top" columnClasses="formLabelTable, formEntryText"> 
		<h:outputText id="taxInformationLabel" value="#{property.taxInformationLabel}" styleClass="formLabel" style="width: 175px; margin-top: 10px" />
	
		<h:panelGrid id="irsnGrid" columns="2" cellspacing="0" cellpadding="0" border="0" style="width: 430px;"
					columnClasses="formEntryText">
			<h:column>
				<h:selectOneRadio id="govtIdType" value="#{pc_indexCommon.indexParty.taxInformation}" layout="pageDirection"
								styleClass="formEntryText" style="height: 100px; width: 155px">
					<f:selectItems value="#{pc_indexCommon.indexParty.taxInformationList}" />
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:panelGrid id="irsnInputGrid" border="0" cellpadding="5" cellspacing="0" columns="1">
					<!-- begin NBA340 -->
					<h:inputText id="govtIdInput" style="width: 265px;margin-top:70px;" styleClass="formEntryText" maxlength="9"
								value="#{pc_indexCommon.indexParty.ssn}">
					</h:inputText>					
					<h:inputHidden id="govtIdKey" value="#{pc_indexCommon.indexParty.govtIdKey}"></h:inputHidden>
					<!-- end NBA340 -->
				</h:panelGrid>
			</h:column>
		</h:panelGrid>
	</h:panelGrid>
	<h:panelGroup id="replaceExchangeLine" styleClass="formDataEntryLine">
		<h:outputText id="replaceExchange" value="#{property.replaceExchangeLabel}" styleClass="formLabel"
			style="width :175px" />
		<h:selectOneMenu id="replaceExchangeLineType" styleClass="formEntryTextHalf" style="width: 425px"
			value="#{pc_indexTable.selectedIndexRowBean.replaceExchange}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.replaceExchangeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="appstateLine" styleClass="formDataEntryLine">
		<h:outputText id="stateProvince" value="#{property.stateProvinceLabel}" style="width :175px" styleClass="formLabel" />
		<h:selectOneMenu id="stateProvinceList" style="width: 425px" styleClass="formEntryTextHalf"
			value="#{pc_indexTable.selectedIndexRowBean.stateProvince}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.stateProvinceList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="formLine" styleClass="formDataEntryLine">
		<h:outputText id="form" value="#{property.formLabel}" styleClass="formLabel" style="width:175px" />
		<h:selectOneMenu id="formsList" style="width :425px" styleClass="formEntryTextHalf" value="#{pc_indexTable.selectedIndexRowBean.formNumber}">
			<f:selectItems value="#{pc_indexCommon.appFormList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="companionCaseLine" styleClass="formDataEntryLine">
		<h:outputText id="companionCase" value="#{property.companionCaseTypeLabel}" styleClass="formLabel"
			style="width :175px" />
		<h:selectOneMenu id="companionCaseType" styleClass="formEntryTextHalf" style="width: 425px"
			value="#{pc_indexTable.selectedIndexRowBean.companionCaseType}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.companionCaseTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="underwriterQueueLine" styleClass="formDataEntryLine">
		<h:outputText id="underwriterQueue" value="#{property.underwriterQueueLabel}" styleClass="formLabel"
			style="width :175px" />
		<h:selectOneMenu id="underwriterQueueType" styleClass="formEntryTextHalf" style="width: 425px"
			value="#{pc_indexTable.selectedIndexRowBean.underwriterQueue}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.underwriterQueueList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<!-- NBA251 Begins -->
	<h:panelGroup id="caseManagerQueueLine" styleClass="formDataEntryLine">
		<h:outputText id="caseManagerQueue" value="#{property.caseManagerQueue}" styleClass="formLabel"
			style="width :175px" />
		<h:selectOneMenu id="caseManagerQueueType" styleClass="formEntryTextHalf" style="width: 425px"
			value="#{pc_indexTable.selectedIndexRowBean.caseManagerQueue}">
			<f:selectItems value="#{pc_indexTable.selectedIndexRowBean.caseManagerQueueList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<!-- NBA251 Ends -->	
</jsp:root>
