<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            7      Indexing UI Rewrite -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- NBA317         NB-1301   PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formTitleBar">
		<h:outputLabel id="createCCPTitle" value="#{property.createCCPTitle}" styleClass="shTextLarge" />
	</h:panelGroup>

	<h:panelGroup id="companyLine" styleClass="formDataEntryTopLine">
		<h:outputText id="companyLabel" value="#{property.companyLabel}" styleClass="formLabel" />
		<h:selectOneMenu id="companyList" style="width :465px" styleClass="formEntryTextHalf"
			value="#{pc_indexCommon.company}">
			<f:selectItems value="#{pc_indexCommon.companyList}" />
		</h:selectOneMenu>
	</h:panelGroup>

	<h:panelGroup id="contractLine" styleClass="formDataEntryLine">
		<h:outputText id="contractLabel" value="#{property.contractLabel}" styleClass="formLabel" />
		<h:inputText id="contract" styleClass="formEntryText" style="width :465px" value="#{pc_indexCommon.policyNumber}" maxlength="15"/>
	</h:panelGroup>


	<h:panelGroup id="lastNameLine" styleClass="formDataEntryLine">
		<h:outputText id="lastNameLabel" value="#{property.lastNameLabel}" styleClass="formLabel" />
		<h:inputText id="lastName" styleClass="formEntryText" style="width :465px" value="#{pc_indexCommon.indexParty.lastName}" maxlength="21"/>
	</h:panelGroup>

	<h:panelGroup id="inforceLine" styleClass="formDataEntryLine">
		<h:outputText id="emptySpaceInforce" value="" style="width:120px" />
		<h:selectBooleanCheckbox id="inforceCheckbox" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.inforce}" 
			valueChangeListener="#{pc_indexTable.selectedIndexRowBean.indexPayment.processInforceChkBoxEvent}" onclick="submitForm();"/> <!-- SPRNBA-659 -->
		<h:outputText id="inforceLabel" value="Inforce" styleClass="formLabel"
			style="text-align :left" />
	</h:panelGroup>

	<h:panelGroup id="paymentTypePGroup" styleClass="formDataEntryLine">
		<h:outputText id="pType" value="#{property.ccpPaymentType}" styleClass="formLabel" />
		<h:selectOneMenu id="pTypeDD" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.paymentType}" styleClass="formEntryText" style="width: 465px;" 
			rendered="#{!pc_indexTable.selectedIndexRowBean.indexPayment.inforce}">
			<f:selectItems id="pTypeList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.paymentTypeList}" />
		</h:selectOneMenu>
		<h:selectOneMenu id="iPTypeDD" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.paymentType}" styleClass="formEntryText" style="width: 465px;" 
			rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.inforce}">
			<f:selectItems id="ipTypeList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.inforcePaymentTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGroup id="ccpAmount" styleClass="formDataEntryLine">
		<h:outputText id="chargeAmt" value="#{property.ccpAmount}" styleClass="formLabel" />
		<h:inputText id="chargeAmtEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.amount}" 
			disabled="#{pc_indexTable.selectedIndexRowBean.indexPayment.disableAmount}" styleClass="formEntryText"
			style="width: 170px;">
			<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
		</h:inputText>
		<h:outputText id="effDate" value="#{property.ccpDate}" styleClass="formLabel" style="width: 125px;" />
		<h:inputText id="effDateEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.effectiveDate}" maxlength="10"
			styleClass="formEntryText" style="width:170px" >
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>

	</h:panelGroup>

	<h:panelGroup id="ccTypePGroup" styleClass="formDataEntryLine">
		<h:outputText id="ccType" value="#{property.ccpCardType}" styleClass="formLabel" />
		<h:selectOneMenu id="ccTypeDD"
			value="#{pc_indexTable.selectedIndexRowBean.indexPayment.creditCardType}"
			styleClass="formEntryText" style="width: 170px;"
			onchange="processCreditCardInformation('indexCreditCardForm',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');">
			<!-- NBA317 -->
			<f:selectItems id="ccTypeList"
				value="#{pc_indexTable.selectedIndexRowBean.indexPayment.creditCardTypeList}" />
		</h:selectOneMenu>
		<h:inputHidden id="ccLabel" value="#{pc_indexTable.selectedCardLabel}"></h:inputHidden><!-- NBA317 -->
		<h:outputText id="emptySpaceForApplyManual" value="" style="width :120px" />
		<h:selectBooleanCheckbox id="applyManualCheckbox" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.applyManual}" />
		<h:outputText id="applyManualLabel" value="#{property.applyManualLabel}" styleClass="formLabel"
			style="text-align :left" />
	</h:panelGroup>

	<h:panelGroup id="cardNumberPGroup" styleClass="formDataEntryLine">
		<h:outputText id="cardNumber" value="#{property.ccpCardNo}" styleClass="formLabel" />
		<h:inputText id="cardNumberEF"
			value="#{pc_indexTable.selectedIndexRowBean.indexPayment.cardNumberFormatted}"
			binding="#{pc_indexTable.selectedIndexRowBean.indexPayment.uiCardNumber}"
			styleClass="formEntryText" style="width: 170px;" maxlength="24"
			onchange="formatCreditCardNumber('indexCreditCardForm','cardNumberEF','cardNumberEFHidden');processCreditCardInformation('indexCreditCardForm',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');" />
		<!-- NBA317 -->
		<h:inputHidden id="cardNumberEFHidden" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.creditCardNo}"></h:inputHidden><!-- NBA317 -->
		<h:outputText id="cardNumberV" value="#{property.ccpVerifyCardNo}" styleClass="formLabel" style="width: 125px;" />
		<h:inputText id="cardNumberVEF"
			value="#{pc_indexTable.selectedIndexRowBean.indexPayment.verifyCardNumberFormatted}"
			binding="#{pc_indexTable.selectedIndexRowBean.indexPayment.uiVerifyCardNumber}"
			styleClass="formEntryText" style="width: 170px;" maxlength="24"
			onchange="formatCreditCardNumber('indexCreditCardForm','cardNumberVEF','cardNumberVEFHidden');processCreditCardInformation('indexCreditCardForm',true,'ccTypeDD','ccLabel','cardNumberEFHidden','cardNumberVEFHidden','cardNumberEF','cardNumberVEF');" />
		<!-- NBA317 -->
		<h:inputHidden id="cardNumberVEFHidden"
			value="#{pc_indexTable.selectedIndexRowBean.indexPayment.verifyCreditCardNo}"></h:inputHidden>
		<!-- NBA317 -->
	</h:panelGroup>
	<h:panelGroup id="expirationPGroup" styleClass="formDataEntryLine">
		<h:outputText id="exp" value="#{property.ccpCardExpiration}" styleClass="formLabel" />
		<h:selectOneMenu id="expMonthDD" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.expirationMonth}" styleClass="formEntryTextFull" style="width: 170px">
			<f:selectItems id="expMonthList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.expirationMonthList}" />
		</h:selectOneMenu>
		<h:outputText id="expMonth" value="#{property.ccpExpirationMonth}" styleClass="formLabel" style="width: 70px;" />
		<h:outputText id="emptySpace" value="" style="width: 55px"></h:outputText>
		<h:selectOneMenu id="expYearDD" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.expirationYear}" styleClass="formEntryTextHalf"
			style="width: 100px;">
			<f:selectItems id="expYearList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.expirationYearList}" />
		</h:selectOneMenu>
		<h:outputText id="expYear" value="#{property.ccpExpirationYear}" styleClass="formLabel" style="width: 65px; margin-left: -5px;" />
	</h:panelGroup>
	<h:panelGroup id="namePGroup" styleClass="formDataEntryLine">
		<h:outputText id="nameCard" value="#{property.ccpNameOnCard}" styleClass="formLabel" />
		<h:inputText id="nameCardEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.nameOnCard}" 
			styleClass="formEntryText" style="width: 465px" maxlength="39" />
	</h:panelGroup>
	<h:panelGroup id="billingAddressPGroup" styleClass="formDataEntryLine">
		<h:outputText id="billingAdd" value="#{property.ccpBillingAddress}" styleClass="formLabel" />
		<h:inputText id="billingAddEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.billingAddress}" styleClass="formEntryText" style="width: 465px" maxlength="40" />
	</h:panelGroup>
	<h:panelGroup id="ccCityPGroup" styleClass="formDataEntryLine">
		<h:outputText id="city1" value="#{property.ccpCity}" styleClass="formLabel"/>
		<h:inputText id="city1EF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.city}" styleClass="formEntryText" 
			style="width: 170px;" maxlength="30" />
		<h:outputText id="state1" value="#{property.ccpStateProvince}" styleClass="formLabel" style="width: 125px;" />
		<h:selectOneMenu id="state1DD" value="#{pc_indexTable.selectedIndexRowBean.stateProvince}" styleClass="formEntryTextFull" style="width:170px">
			<f:selectItems id="ccState1List" value="#{pc_indexTable.selectedIndexRowBean.stateProvinceList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<h:panelGrid id="ccCodePGroup" columnClasses="labelAppEntry,colMIBShort,colMIBShort" columns="3" cellpadding="0" cellspacing="0"
		style="padding-top: 6px;">
		<h:column>
			<h:outputText id="ccZipCode" value="#{property.ccpCode}" styleClass="formLabel" />
		</h:column>
		<h:column>
			<h:selectOneRadio id="pinsZipRB" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.zipPostalType}" layout="pageDirection" styleClass="radioLabel"
				style="width: 70px;margin-left: -5px;">
				<f:selectItems id="ccZipTypeCodes" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.zipPostalTypeList}" />
			</h:selectOneRadio>
		</h:column>
		<h:column>
			<h:inputText id="zipEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.zipCode}" styleClass="formEntryText" style="width: 100px;" maxlength="11">
				<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}" /> <!-- SPRNBA-493 -->
			</h:inputText>

			<h:inputText id="postalEF" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.postalCode}" disabled="#{pc_indexTable.selectedIndexRowBean.indexPayment.zipCodeCheck}" 
				styleClass="formEntryText" style="width: 100px;" maxlength="11" />
		</h:column>
	</h:panelGrid>

</jsp:root>
