<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173           7       Indexing UI Rewrite-->
<!-- SPR3616          8       Adding comments through the Comments button bar on the NBCWACHECK indexing view deletes values entered in some of the tabs-->
<!-- NBA221 		  8		  Payment Origination Project -->
<!-- SPR3505          8       Selected Payment Row Updated Prior to Invoking Update Push Button-->
<!-- SPRNBA-397       NB-1101 Unable to index a CWA check on an application to be split between the app and inforce contracts when the app uses auto contract numbering-->
<!-- SPRNBA-659     NB-1301   Issues in entering data on indexing view  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html" xmlns:DynaDiv="/WEB-INF/tld/DynamicDiv.tld">

	<h:panelGroup id="chkTitleBarGrp" styleClass="formTitleBar">
		<h:outputLabel id="chkTitleBar" value="#{property.checkTitleBar}" styleClass="shTextLarge" />
	</h:panelGroup>

	<h:panelGroup id="numberDateLine" styleClass="formDataEntryTopLine">
		<h:outputText id="numberLabel" value="#{property.numberLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="number" styleClass="formEntryText" style="width: 200px" maxlength="11" 
			value="#{pc_indexTable.selectedIndexRowBean.indexPayment.checkNumber}" onchange="resetTargetFrame();submitForm();"/>

		<h:outputText id="checkDateLabel" value="#{property.dateLabel}" styleClass="formLabel" />
		<h:inputText id="checkDate" styleClass="formEntryText" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.cwaDate}" 
			onchange="resetTargetFrame();submitForm(this);" maxlength="10">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>

	<h:panelGroup id="amountLine" styleClass="formDataEntryLine">
		<h:outputText id="amountLabel" value="#{property.amountLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="amount" styleClass="formEntryText" style="width: 200px" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.checkAmount}" onchange="resetTargetFrame();submitForm(this);">
			<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
		</h:inputText>
		<h:outputText id="emptySpaceSplit" value="" style="width:122px" />
		<h:selectBooleanCheckbox id="splitMultipleCheckbox" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}" 
			disabled="#{pc_indexTable.selectedIndexRowBean.disableSplitMultiple}" onclick="resetTargetFrame();submitForm();"
			valueChangeListener="#{pc_indexTable.selectedIndexRowBean.checkSplitMultiple}"/>
		<h:outputText id="splitMultipleLine" value="#{property.splitMultipleLabel}" styleClass="formLabel"
			style="text-align :left" />
	</h:panelGroup>

	<h:panelGroup id="chklastNameLine" styleClass="formDataEntryLine">
		<h:outputText id="chklastNameLabel" value="#{property.checkLastNameLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="chklastName" styleClass="formEntryText" style="width: 200px" 
			value="#{pc_indexTable.selectedIndexRowBean.indexParty.lastName}" maxlength="21"/>
	</h:panelGroup>
	<DynaDiv:DynamicDiv rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}" id="renderTableDiv">
	<h:panelGrid id="splitMultipleTable" columns="1" rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}">
		<f:verbatim>
			<hr class="formSeparator" style="margin-top: 2px;margin-bottom: 2px;" />
		</f:verbatim>
	
		<h:panelGroup id="checkHeader" styleClass="ovDivTableHeader">
			<h:panelGrid columns="3" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrText270,ovColHdrText140,ovColHdrText155">
				<h:commandLink id="checkHdrCol1" actionListener="#{pc_indexTable.selectedIndexRowBean.checkTable.sortColumn}" immediate="true"
					value="#{property.checkCol1}" styleClass="ovColSorted#{pc_indexTable.selectedIndexRowBean.checkTable.sortedByCol1}" /><!-- NBA221 -->
				<h:commandLink id="checkHdrCol2" actionListener="#{pc_indexTable.selectedIndexRowBean.checkTable.sortColumn}" immediate="true"
					value="#{property.checkCol2}" styleClass="ovColSorted#{pc_indexTable.selectedIndexRowBean.checkTable.sortedByCol2}" /><!-- NBA221 -->
				<h:commandLink id="checkHdrCol3" actionListener="#{pc_indexTable.selectedIndexRowBean.checkTable.sortColumn}" immediate="true"
					value="#{property.checkCol3}" styleClass="ovColSorted#{pc_indexTable.selectedIndexRowBean.checkTable.sortedByCol3}" /><!-- NBA221 -->
			</h:panelGrid>
		</h:panelGroup>
		<!-- Table Data -->
		<h:panelGroup id="checkData" styleClass="ovDivTableData6" style="height: 65px">
			<h:dataTable id="checkDataTable" styleClass="ovTableData" cellspacing="0" binding="#{pc_indexTable.selectedIndexRowBean.checkTable.dataTable}"
				value="#{pc_indexTable.selectedIndexRowBean.checkTable.checkList}"
				rowClasses="#{pc_indexTable.selectedIndexRowBean.checkTable.checkRowStyles}" columnClasses="ovColText270,ovColText140,ovColText155"
				rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}" var="checkTableRow"><!-- NBA221 --><!-- SPR3505  SPRNBA-397 -->
				<h:column>
					<h:commandLink id="company" style="width: 100%;" value="" styleClass="ovFullCellSelect" action="#{pc_indexTable.selectedIndexRowBean.checkTable.selectCheckTableRow}"><!-- NBA221 -->
						<h:outputText id="companyVal" value="#{checkTableRow.checkCompanyName}"></h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="contract" style="width: 100%;" value="" styleClass="ovFullCellSelect" action="#{pc_indexTable.selectedIndexRowBean.checkTable.selectCheckTableRow}"><!-- NBA221 -->
						<h:outputText id="contractVal" value="#{checkTableRow.checkContract}"></h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="amountToApply" style="width: 100%;" value="" styleClass="ovFullCellSelect" action="#{pc_indexTable.selectedIndexRowBean.checkTable.selectCheckTableRow}"><!-- NBA221 -->
						<h:outputText id="amountToApplyVal" value="#{checkTableRow.amountToApply}"></h:outputText>
					</h:commandLink>
				</h:column>
			</h:dataTable>
		</h:panelGroup>

	</h:panelGrid>
	
	<h:panelGroup styleClass="formButtonBar">
		<h:commandButton id="btnDelete" value="#{property.buttonDelete}" styleClass="formButtonLeft"
			action="#{pc_indexTable.selectedIndexRowBean.checkTable.delete}" onclick="resetTargetFrame();" 
			rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}"
			disabled="#{!pc_indexTable.selectedIndexRowBean.checkTable.checkPaymentRowSelected}"/><!-- NBA221 -->
	</h:panelGroup>
	</DynaDiv:DynamicDiv>
	<h:panelGroup id="checkcompanyLine" styleClass="formDataEntryLine">
		<h:outputText id="checkcompanyLabel" value="#{property.companyLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:selectOneMenu id="checkcompanyList" styleClass="formEntryTextHalf" style="width: 475px"
			value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.checkCompany}"><!-- NBA221 -->
			<f:selectItems id="checkCompanyListValue" value="#{pc_indexCommon.companyList}" />
		</h:selectOneMenu>
	</h:panelGroup>

	<h:panelGroup id="chkcontractLine" styleClass="formDataEntryLine">
		<h:outputText id="chkcontractLabel" value="#{property.contractLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="chkcontract" styleClass="formEntryText" style="width: 475px" maxlength="15"
			value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.checkContract}" onchange="resetTargetFrame();submitForm();"/><!-- NBA221 -->
	</h:panelGroup>

	<h:panelGroup id="chkinforceLine" styleClass="formDataEntryLine">
		<h:outputText id="emptySpaceInforce" value="" style="width:130px"/><!-- NBA221-->
		<h:selectBooleanCheckbox id="chkinforceCheckbox" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.inforce}" 
			valueChangeListener="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.processInforceChkBoxEvent}" onclick="submitForm();"/><!-- NBA221 SPRNBA-659 -->
		<h:outputText id="chkinforceLabel" value="Inforce" styleClass="formLabel"
			style="text-align :left" />
	</h:panelGroup>
  
	<h:panelGroup id="paymentTypePGroup" styleClass="formDataEntryLine">
		<h:outputText id="pType" value="#{property.paymentTypeLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:selectOneMenu id="pTypeDD" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.paymentType}" styleClass="formEntryText" style="width: 475px;" 
			rendered="#{!pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.inforce}" onchange="submitForm();"> <!-- SPR3616 --><!-- NBA221 SPRNBA-659 -->
			<f:selectItems id="pTypeList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.paymentTypeList}" />
		</h:selectOneMenu>
		<h:selectOneMenu id="iPTypeDD" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.paymentType}" styleClass="formEntryText" style="width: 475px;" 
			rendered="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.inforce}" onchange="submitForm();"> <!-- SPR3616 --><!-- NBA221 SPRNBA-659 -->
			<f:selectItems id="ipTypeList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.inforcePaymentTypeList}" />
		</h:selectOneMenu>
	</h:panelGroup>
		
    <!-- NBA221 Begin-->
 	<h:panelGroup id="paymentSourceGroup" styleClass="formDataEntryLine">
		<h:outputText id="pSource" value="#{property.paymentMoneySourceLabel}" styleClass="formLabel" style="width: 135px"/>
		<h:selectOneMenu id="pSourceDD" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.paymentSource}" styleClass="formEntryText" style="width: 475px;">
			<f:selectItems id="pSourceList" value="#{pc_indexTable.selectedIndexRowBean.indexPayment.moneySourceList}" />
		</h:selectOneMenu>
	</h:panelGroup>
	<!-- NBA221 End-->
	
	<h:panelGroup id="dateLine" styleClass="formDataEntryLine">
		<h:outputText id="dateLabel" value="#{property.dateLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="date" styleClass="formEntryText" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.checkDate}" maxlength="10" onchange="resetTargetFrame();submitForm(this);"> <!-- SPR3616 --><!-- NBA221 -->
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>

	<h:panelGroup id="paymentTypeCostBasisLine" styleClass="formDataEntryLine">
		<h:outputText id="costBasisLabel" value="#{property.costBasisLabel}" styleClass="formLabel" style="width: 135px"/><!-- NBA221-->
		<h:inputText id="costBasis" styleClass="formEntryText" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.costBasis}" ><!-- NBA221 -->
			<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
		</h:inputText>

		<h:outputText id="amountToApplyLabel" value="#{property.amountToApplyLabel}" styleClass="formLabel"
			style="width: 200px" rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}"/>
		<h:inputText id="amountToApply" styleClass="formEntryText" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.amountToApply}"
			rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}" onchange="resetTargetFrame();submitForm(this);"><!-- NBA221 -->
			<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
		</h:inputText>
	</h:panelGroup>

	<h:panelGroup id="applyManualLine" styleClass="formDataEntryLineBottom">
		<h:outputText id="emptySpacePrevTxYr" value="" style="width:130px" /><!-- NBA221-->
		<h:selectBooleanCheckbox id="previousTaxYearCheckbox" 
			value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.previousTaxYear}" /><!-- NBA221 -->
		<h:outputText id="previousTaxYearLabel" value="#{property.previoustaxYearLabel}" styleClass="formLabel"
			style="text-align :left" />

		<h:outputText id="emptySpaceForApplyManual" value="" style="width :195px" />
		<h:selectBooleanCheckbox id="applyManualCheckbox" value="#{pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.applyManual}" 
			rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypePaymentForm}"
			disabled="#{!pc_indexTable.selectedIndexRowBean.checkTable.selectedCheckPayment.inforce}"/><!-- NBA221 -->
		<h:outputText id="applyManualLabel" value="#{property.applyManualLabel}" styleClass="formLabel"
			style="text-align :left" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypePaymentForm}"/>
	</h:panelGroup>
	<h:panelGroup id="btnGrpBar" styleClass="formButtonBar" rendered="#{pc_indexTable.selectedIndexRowBean.indexPayment.splitMultiple}">
		<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft"
			action="#{pc_indexTable.selectedIndexRowBean.checkTable.clear}" onclick="resetTargetFrame();" /><!-- NBA221 -->
		<h:commandButton id="btnUpdate" styleClass="formButtonRight-1" value="#{property.buttonUpdate}"
			onclick="resetTargetFrame();" action="#{pc_indexTable.selectedIndexRowBean.checkTable.update}"
			disabled="#{ pc_indexTable.selectedIndexRowBean.checkTable.updateDisabled}"/><!-- NBA221 SPRNBA-397 -->
		<h:commandButton id="btnAdd" styleClass="formButtonRight" value="#{property.buttonAdd}" onclick="resetTargetFrame();" 
			action="#{pc_indexTable.selectedIndexRowBean.checkTable.add}" disabled="#{pc_indexTable.selectedIndexRowBean.checkTable.addDisabled}"/><!-- NBA221 -->
	</h:panelGroup>
</jsp:root>