<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA173            	7      	Indexing View Rewrite-->
<!-- NBA212            	7      	Content Services -->
<!-- NBA213 		   	7	  	Unified User Interface -->
<!-- SPR1613			8	  	Some Business Functions should be disabled on an Issued Contract  --> 
<!-- SPR3558			8	  	Blank Indexing View Returned when Split Multiple Check Box is Selected for CWA Check on NBAPPLCTN and for Payment Check on NBPAYMENT  -->
<!-- SPR3554			8	  	White Space and Vertical Scroll Bar --> 
<!-- SPR3484			8	  	Source selected is not the source in focus on the image viewer on app with multiple sources --> 
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- NBA317         NB-1301   	PCI Compliance For Credit Card Numbers Using Web Service  -->
<!-- SPRNBA-659     NB-1301   	Issues in entering data on indexing view  -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->
<!-- NBA340		    NB-1501     Mask Government ID -->
<!-- SPRNBA-700		NB-1501 	Some Index Values Deleted When Add Comments from Comments Button Bar on NBAPPLCTN-->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/DynamicDiv.tld" prefix="DynaDiv"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Indexing</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/jquery.js"></script><!-- NBA317 -->
<script type="text/javascript" src="include/creditCard.js"></script><!-- NBA317 -->
<script type="text/javascript" src="javascript/nbaUtils.js"></script> <!-- SPRNBA-659 -->
<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
<script type="text/javascript">
		<!--
					
			var contextpath = '<%=path%>';	//FNB011
			//begin NBA317
			var formID = 'form_indexApplication';
			var NBAPAYCC = "Credit Card Payment (NBPAYCC)";
			var indexCCForm = 'indexCreditCardForm';
			var cardNumberEFHidden = 'cardNumberEFHidden';
			var cardNumberVEFHidden = 'cardNumberVEFHidden';
			var actionMenuCardNumber1Hidden = 'actionMenuForm:cardNumber1Hidden';
			var actionMenuVerifyCardNumber1Hidden = 'actionMenuForm:verifyCard1NumberHidden';	
			//end NBA317	
		
			function resetTargetFrame() {
				document.forms['form_indexApplication'].target='';
				return false;
			}
	
			function setTargetFrame() {
				document.forms['form_indexApplication'].target='controlFrame';
				return false;
			}
			//NBA212 New Method
			function viewImages() {
				imageFrame.location.href = imageFrame.location.href;  //SPR3484
			}
			
			function submitForm(elem) { 				
 				if (elem != null) {
					elem.onblur();
				}	
				//begin SPRNBA-659			
				var active = document.activeElement;
				if (active != null) {	
					parent.setHiddenFieldValues(lastactiveField,active.id);
				}
				//end SPRNBA-659							 
				document.forms['form_indexApplication'].submit();
			}

			function sizeIndexContentArea() {
//				debugger;
				var indexPane = document.getElementById("indexContentArea");
				if (indexPane != null) {
					indexPane.height = 400;
//					alert('parent height = ' + parent.style.height);
				}
			}
		//-->
		//begin NBA317
			function formatCreditCardNumber(subviewID,field,hiddenField) {
				return formatCardNumber(formID,subviewID,field,hiddenField);
			}
		
			function processCreditCardInformation(subviewID,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID){				
				var errorMessageDiv = 'Messages';
				var skipCreditCardValidation = top.ccTokenizationFrame.isSkipCCValidation();
				var tokenizationServiceURL = top.ccTokenizationFrame.getTokenizationURL();
				var tokenizationServiceRequest = top.ccTokenizationFrame.getTokenizationRequest();
				processCreditCardTokenization(formID,subviewID,skipCreditCardValidation,tokenizationServiceURL,tokenizationServiceRequest,isVerifyFieldAvailable,cardTypeID,cardLabelID,cardNumberID,verifyCardNumberID,viewCardNumberID,viewVerifyCardNumberID,errorMessageDiv);
			}
			
			function authorizeCommit(formID,subviewID,enable){
				document.getElementById(formID + ':btnCommit').disabled = !enable;
				document.forms[formID][formID + ':ccTokenized'].value=enable;
			}
			
			function handleSourceSelectionChange(newSel){
				if(newSel != NBAPAYCC){
					if('false' == document.forms[formID][formID + ':ccTokenized'].value){
						if(isCreditCardFieldAvailable()){
							var cardNumberEFHiddenValue = getValue(formID,indexCCForm,cardNumberEFHidden);
							var cardNumberVEFHiddenValue = getValue(formID,indexCCForm,cardNumberVEFHidden);
							//set the card numbers in the nbFile.jsp's hidden card number attributes
							top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber1Hidden,cardNumberEFHiddenValue);
							top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuVerifyCardNumber1Hidden,cardNumberVEFHiddenValue);
							//delete the card numbers from the hidden fields on the jsp
							setValue(formID,indexCCForm,cardNumberEFHidden,'');
							setValue(formID,indexCCForm,cardNumberVEFHidden,'');
						}
					}
				}
			}
			
			function loadHiddenCardNumberFieldValues(){
				if('false' == document.forms[formID][formID + ':ccTokenized'].value){
					if(isCreditCardFieldAvailable()){
						var cardNumberEFHiddenValue = top.mainContentFrame.contentRightFrame.getHiddenFieldValue(actionMenuCardNumber1Hidden);
						var cardNumberVEFHiddenValue = top.mainContentFrame.contentRightFrame.getHiddenFieldValue(actionMenuVerifyCardNumber1Hidden);
						//Reload the values for the card number fields from nbFile.jsp
						setValue(formID,indexCCForm,cardNumberEFHidden,cardNumberEFHiddenValue);
						setValue(formID,indexCCForm,cardNumberVEFHidden,cardNumberVEFHiddenValue);
						//Delete the values from nbFile.jsp once they have been reloaded in the life app entry jsps
						top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuCardNumber1Hidden,'');
						top.mainContentFrame.contentRightFrame.setHiddenFieldValues(actionMenuVerifyCardNumber1Hidden,'');
					}
				}
			}
			
			function isCreditCardFieldAvailable(){
				var field = getUIField(formID,indexCCForm,cardNumberEFHidden);
				if(field != null){
					return true;
				}
				return false;
			} 		
		//end NBA317		   
			
			//NBA340 new Function
		    $(document).ready(function() {
		        $("input[name$='govtIdType']").govtidType();			
	            $("input[name$='govtIdInput']").govtidValue();
		    })
			
			//NBA340 new Function
			function checkGovtIdError(){
			  if($(this).checkErrorMessage()){
			     setTargetFrame();
			  }else{
			     resetTargetFrame();
			  }
			}
</script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="sizeIndexContentArea();filePageInit();viewImages();loadHiddenCardNumberFieldValues();restoreFocus('form_indexApplication')"  style="overflow-x: hidden; overflow-y: auto"><!-- NBA317 SPRNBA-659-->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_INDEX_APPLICATION" value="#{pc_indexTable}" />
	<h:form id="form_indexApplication">
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr id="indexContentArea" style="height:*; width:100%; vertical-align: top;">
					<td><!-- scrollable div -->
						<h:panelGroup id="indexPGrp" style="height:130px;">
							<f:subview id="indexSourceTable">
								<c:import url="/nbaIndexing/subviews/nbaIndexSourceTable.jsp" />
							</f:subview> 
						</h:panelGroup>
						<!-- indexing form for the source -->
						<div class="inputFormMat">
							<div class="inputForm">
								<!-- Only one of the subviews will be rendered depending on the type of source selected in the table-->
								<f:subview id="indexApplicationForm" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeApplication}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingAppForm.jsp" />
								</f:subview>
								<f:subview id="indexCheckform" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeCheckForm ||
																			pc_indexTable.selectedIndexRowBean.sourceTypePaymentForm}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingCheckForm.jsp" />
								</f:subview>
								<f:subview id="indexContractChangeForm" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeContractChange}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingContractChangeForm.jsp" />
								</f:subview>
								<f:subview id="indexCreditCardForm" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeCreditCard}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingCreditCardForm.jsp" />
								</f:subview>
								<DynaDiv:DynamicDiv rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeMiscellaneous}" id="renderTableDiv">  <!-- SPR3558 -->
								<f:subview id="indexMiscellaneousForm"> <!-- SPR3558 -->
									<c:import url="/nbaIndexing/subviews/nbaIndexingMiscellaneousForm.jsp" />  
								</f:subview> 
								</DynaDiv:DynamicDiv>  <!-- SPR3558 -->
								<f:subview id="indexReinsurance" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeReinsurance}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingReinsurance.jsp" />
								</f:subview>
								<f:subview id="indexRequirementsForm" rendered="#{pc_indexTable.selectedIndexRowBean.sourceTypeRequirements}">
									<c:import url="/nbaIndexing/subviews/nbaIndexingRequirements.jsp" />
								</f:subview>
							</div>
						</div>
					</td>
				</tr>
				<tr style="height:20px; vertical-align: bottom;padding-top: 0px;">
					<td><h:panelGroup id="indexCommentBarGrp" style="height:20px"><f:subview id="nbaCommentBar">
						<iframe id="commentFrame" name="commentBar" style="width: 100%; height: 21px;" src="./nbaAppEntry/nbaAECommentBar.faces" scrolling="no" frameborder="0"></iframe><!-- SPRNBA-700  -->
					</f:subview></h:panelGroup></td>
				</tr>
				<tr style="height:20px; vertical-align: top;padding-top: 0px;">
					<td><h:panelGroup styleClass="tabButtonBar" style="height:20px;">
						<h:commandButton id="btnCommit" styleClass="tabButtonRight" value="#{property.buttonCommit}"
							disabled="#{pc_indexTable.auth.enablement['Commit'] || 
										pc_indexTable.notLocked || 
										pc_indexTable.issued ||
										!pc_indexTable.creditCardTokenized}"
							onclick="checkGovtIdError();" action="#{pc_indexTable.commit}" />	<!-- NBA213 SPR1613 NBA317-->
					</h:panelGroup></td>
					<!-- begin NBA317  -->
					<td><h:panelGroup>
						<h:inputHidden id="guid" value="#{pc_indexTable.guid}"></h:inputHidden>
						<h:inputHidden id="txnDate" value="#{pc_indexTable.txnDate}"></h:inputHidden>
						<h:inputHidden id="txnTime" value="#{pc_indexTable.txnTime}"></h:inputHidden>
						<h:inputHidden id="ccTokenized" value="#{pc_indexTable.creditCardTokenized}"></h:inputHidden>
						 
					</h:panelGroup></td>
					<!-- end NBA317  -->
				<tr>
			</tbody>
		</table>
		<!-- SPR3484 code deleted -->
	</h:form>
	<!-- begin NBA212 -->				
	<h:form id="form_IndexImages">
	<div id="otherInsSection" style="display:'none'"> <!-- SPR3554 -->		
		<!-- SPR3484 code deleted -->
		<iframe id="imageFrame" name="imageFrame" src="launchImage.faces" width="100%" frameborder="0"></iframe>
	</div> <!-- SPR3554 -->
	</h:form>
	<!-- end NBA212 -->
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>

</body>
</html>
