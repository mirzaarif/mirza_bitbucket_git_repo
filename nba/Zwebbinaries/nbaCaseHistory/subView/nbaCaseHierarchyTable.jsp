<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA163           6       Case History Rewrite -->
<!-- SPRNBA-589		NB-1301   Improve Response Time for History View -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="casehierarchyHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="2" styleClass="ovTableHeader" cellspacing="0" columnClasses="ovColHdrText455,ovColHdrText150">
			<h:outputLabel id="caseHierarchyHdrCol1" value="#{property.caseHierarchyCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="caseHierarchyHdrCol2" value="#{property.caseHierarchyCol2}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="casehierarchyData" styleClass="ovDivTableData">
		<h:dataTable id="casehierarchyTable" styleClass="ovTableData" cellspacing="0" rows="0" border="0" binding="#{pc_caseHierarchy.dataTable}"
			value="#{pc_caseHierarchy.caseHierarchyItems}" var="caseHierItem" rowClasses="#{pc_caseHierarchy.caseHierarchyRowStyles}"
			columnClasses="ovColText455,ovColText150" style="min-height: 19px;">
			<h:column>
				<h:panelGroup>
					<h:commandButton id="caseHierItemIcon1" image="images/hierarchies/#{caseHierItem.icon1}" rendered="#{caseHierItem.icon1Rendered}"
						onmousedown="saveTableScrollPosition();"
						styleClass="#{caseHierItem.icon1StyleClass}" style="margin-left: 5px; vertical-align: top" />	<!-- SPRNBA-589 -->
					<h:commandButton id="caseHierItemIcon2" image="images/hierarchies/#{caseHierItem.icon2}" rendered="#{caseHierItem.icon2Rendered}"
						onmousedown="saveTableScrollPosition();"					
						styleClass="#{caseHierItem.icon2StyleClass}" style="margin-left: 10px; vertical-align: top" />	<!-- SPRNBA-589 -->
					<h:commandButton id="caseHierItemIcon3" image="images/hierarchies/#{caseHierItem.icon3}" rendered="#{caseHierItem.icon3Rendered}"
						onmousedown="saveTableScrollPosition();"					
						styleClass="#{caseHierItem.icon3StyleClass}" style="margin-left: 15px; vertical-align: top" />
					<h:commandLink id="caseHierItemCol1" action="#{pc_caseHierarchy.selectCaseHierarchyRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- SPRNBA-589 -->
						<h:inputTextarea readonly="true" value="#{caseHierItem.itemName}" styleClass="ovMultiLine" style="margin-left: 5px; width: 400px" />
					</h:commandLink>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:commandLink id="itemType" value="#{caseHierItem.itemType}" styleClass="ovFullCellSelect" style="width:100%"
						onmousedown="saveTableScrollPosition();"
						action="#{pc_caseHierarchy.selectCaseHierarchyRow}" immediate="true" />	<!-- SPRNBA-589 -->
				</h:panelGroup>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:inputHidden id="searchTableVScroll" value="#{pc_caseHierarchy.VScrollPosition}" />  	<!-- SPRNBA-589 -->
</jsp:root>
