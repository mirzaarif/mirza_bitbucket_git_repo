<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA163           6       Case History Rewrite -->
<!-- NBA213           7       Unified User Interface -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="casehistoryHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="7" styleClass="ovTableHeader" cellspacing="0"
			columnClasses="ovColHdrText85,ovColHdrDate,ovColHdrText70,ovColHdrText100,ovColHdrText105,ovColHdrText165">
			<h:commandLink id="caseHistHdrCol1" value="#{property.caseHistoryCol1}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol1}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
			<h:commandLink id="caseHistHdrCol2" value="#{property.caseHistoryCol2}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol2}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
			<h:commandLink id="caseHistHdrCol3" value="#{property.caseHistoryCol3}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol3}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
			<h:commandLink id="caseHistHdrCol4" value="#{property.caseHistoryCol4}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol4}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
			<h:commandLink id="caseHistHdrCol5" value="#{property.caseHistoryCol5}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol5}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
			<h:commandLink id="caseHistHdrCol6" value="#{property.caseHistoryCol6}" style="width: 100%; height: 100%"
				styleClass="ovColSorted#{pc_caseHistory.sortedByCol6}" actionListener="#{pc_caseHistory.sortColumn}" immediate="true" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="casehistoryData" styleClass="ovDivTableData" style="height:345px">  <!-- NBA213 -->
		<h:dataTable id="casehistoryTable" styleClass="ovTableData" cellspacing="0" binding="#{pc_caseHistory.dataTable}"
			value="#{pc_caseHistory.caseHistoryItems}" var="caseHistItem" rowClasses="#{pc_caseHistory.rowStyles}"
			columnClasses="ovColText85,ovColDate,ovColText70,ovColText100,ovColText105,ovColText165">
			<h:column>
				<h:commandLink id="eventType" value="#{caseHistItem.eventType}" styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
			<h:column>
				<h:commandLink id="dateTime" styleClass="ovFullCellSelect" style="width:100%">
					<h:outputText value="#{caseHistItem.dateTime}">
						<f:convertDateTime pattern="#{property.dateTimePattern}" />
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="user" value="#{caseHistItem.caseHistUser}" styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
			<h:column>
				<h:commandLink id="queue" value="#{caseHistItem.queue}" styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
			<h:column>
				<h:commandLink id="status" value="#{caseHistItem.status}" styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
			<h:column>
				<h:commandLink id="commments" value="#{caseHistItem.comments}" styleClass="ovFullCellSelect" style="width:100%" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_caseHistory.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.previousPageAbsolute}" immediate="true" />
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_caseHistory.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.previousPage}" immediate="true" />
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_caseHistory.showPreviousSet}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.previousPageSet}" immediate="true" />
		<h:commandLink value="#{pc_caseHistory.page1Number}" rendered="#{pc_caseHistory.showPage1}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page1}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page1Number}" rendered="#{pc_caseHistory.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page2Number}" rendered="#{pc_caseHistory.showPage2}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page2}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page2Number}" rendered="#{pc_caseHistory.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page3Number}" rendered="#{pc_caseHistory.showPage3}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page3}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page3Number}" rendered="#{pc_caseHistory.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page4Number}" rendered="#{pc_caseHistory.showPage4}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page4}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page4Number}" rendered="#{pc_caseHistory.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page5Number}" rendered="#{pc_caseHistory.showPage5}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page5}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page5Number}" rendered="#{pc_caseHistory.currentPage5}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page6Number}" rendered="#{pc_caseHistory.showPage6}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page6}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page6Number}" rendered="#{pc_caseHistory.currentPage6}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page7Number}" rendered="#{pc_caseHistory.showPage7}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page7}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page7Number}" rendered="#{pc_caseHistory.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_caseHistory.page8Number}" rendered="#{pc_caseHistory.showPage8}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.page8}" immediate="true" />
		<h:outputText value="#{pc_caseHistory.page8Number}" rendered="#{pc_caseHistory.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_caseHistory.showNextSet}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.nextPageSet}" immediate="true" />
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_caseHistory.showNext}" styleClass="ovStatusBarText" action="#{pc_caseHistory.nextPage}"
			immediate="true" />
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_caseHistory.showNext}" styleClass="ovStatusBarText"
			action="#{pc_caseHistory.nextPageAbsolute}" immediate="true" />
	</h:panelGroup>
</jsp:root>
