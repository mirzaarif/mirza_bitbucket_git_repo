<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA163           6         Case History Rewrite -->
<!-- NBA158 		  6		  	Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-589		NB-1301   Improve Response Time for History View -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Case History</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/scroll.js"></script>	<!-- SPRNBA-589 -->
<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_casehistory'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_casehistory'].target='';
			return false;
		}

		//Perform initialization of the page
		//NBA213 New Method
		function initPage() {
			filePageInit();

			if (document.getElementById('form_casehistory:nbaCommentBar:lockOpen') != null) {
				document.getElementById('form_casehistory:caseHistoryTbl:casehistoryData').style.height = 320;
			}
		}
		
		//SPRNBA-589 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_casehistory:caseHierarchyTbl:casehierarchyData', 'form_casehistory:caseHierarchyTbl:searchTableVScroll');
			return false;
		}
		//SPRNBA-589 New Method
		function scrollTablePosition() {
			scrollToPosition('form_casehistory:caseHierarchyTbl:casehierarchyData', 'form_casehistory:caseHierarchyTbl:searchTableVScroll');
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!--  NBA213 - common tab style for margin -->
<body class="whitebody" onload="initPage();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll">  <!-- NBA213 SPRNBA-589-->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CASEHISTORY" value="#{pc_caseHistory}" />

	<h:form id="form_casehistory" onsubmit="saveTableScrollPosition();">		<!-- SPRNBA-589 -->
		<h:panelGroup styleClass="sectionSubheader" style="margin-left: -10px">  <!-- NBA213 -->
			<h:outputLabel value="#{property.caseHistoryTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<f:subview id="caseHierarchyTbl">
			<c:import url="/nbaCaseHistory/subView/nbaCaseHierarchyTable.jsp" />
		</f:subview>
		<h:panelGroup styleClass="ovButtonBar" />
		<f:subview id="caseHistoryTbl">
			<c:import url="/nbaCaseHistory/subView/nbaCaseHistoryTable.jsp" />
		</f:subview>

		<f:subview id="nbaCommentBar" rendered="#{pc_caseHierarchy.workLocked}">  <!-- NBA213 -->
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_caseHierarchy.refresh}" immediate="true" styleClass="tabButtonLeft" />
		</h:panelGroup>
	</h:form>
	<!-- NBA213 code deleted -->
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
