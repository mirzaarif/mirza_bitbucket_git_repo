<%
	String documentName = request.getParameter("documentName");
	String displayName = request.getParameter("displayName");
	java.util.Map docMap = com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getCorrespondenceGeneric().getDocuments();
	response.setContentType("application/msword");
	response.addHeader("Content-Disposition", "attachment; filename=" + displayName +".doc");
	String doc = (String) docMap.get(documentName);
	doc = doc.trim();
	response.setContentLength((int) doc.length());
	out.write(doc);
%>