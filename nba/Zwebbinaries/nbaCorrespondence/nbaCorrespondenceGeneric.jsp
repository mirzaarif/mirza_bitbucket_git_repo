<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA129            5       Design and develop xPression correspondence interface --> 
<!-- NBA184            7       Correspondence Rewrite -->
<!-- NBA213			   7	   Unified User Interface  -->
<!-- FNB011 	    NB-1101	   Work Tracking -->
<!-- FNB009         NB-1101    Design and develop interface for Correspondence -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>nbA Correspondence</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript"	src="javascript/global/desktopComponent.js"></script>
<SCRIPT language="JavaScript" src="include/common.js"></SCRIPT>
<!-- FNB011 code deleted -->
<script language="JavaScript" type="text/javascript">

	    var contextpath = '<%=path%>';	//FNB011
	    
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_correspondence'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_correspondence'].target='';
			return false;
		}
		function doResponse(url) {
			prefWidth=1200;
			prefHeight=1024;
			
			winHeight=window.screen.availHeight - 100;
			winWidth=window.screen.availWidth;	
			
			if(winHeight > prefHeight)
				winHeight=prefHeight;
		
			if(winWidth > prefWidth)
				winWidth=prefWidth;
		
			if (url.length > 0) {
				if(url == 'TRUE'){ //FNB009
					popup=window.showModalDialog(contextpath+"/common/popup/popupFrameset.html?popup="+contextpath+"/nbaCorrespondence/launchDoc.faces",'nbaCorrespondence','dialogWidth:500px;dialogHeight:350px;center:yes'); //FNB009
				}else {  //FNB009
            		popup=window.open(url, "Response","height=" + winHeight + ",width=" + winWidth +",toolbar=yes,menu=yes,scrollbars=yes,resize=yes");
					//FNB009 Code deleted
				}
			 	popup.focus(); //FNB009
			}else{
					alert('Please select a letter');
			}
			
			return false;
		}

	</script>
</head>
<body onload="filePageInit();">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CORRESPONDENCE_GENERIC" 	value="#{pc_correspondence}" /> <!-- FNB009-->
	<h:form id="form_correspondence"> <!-- FNB009-->
		<div id="createNew" class="inputFormMat" style="overflow: auto; height: 700px; width: 100%;"> <!-- NBA184-->
			<div class="inputForm"style="overflow: auto; height: 680px; width: 100%;"> <!-- NBA184-->
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel value="Category:" styleClass="formLabel" />
					<h:selectOneMenu styleClass="formEntryTextFull" style="width: 470px;"value="#{pc_correspondence.category}"
								valueChangeListener="#{pc_correspondence.selectCategory}"	onchange="submit()"> <!-- FNB009-->
						<f:selectItems value="#{pc_correspondence.categories}" />
					</h:selectOneMenu>
				</h:panelGroup>
				
				
		 		<h:panelGroup id="lettersHeader" styleClass="ovDivTableHeader"	style="width: 550px;margin-left: 9px;">
					<h:panelGrid columns="1" styleClass="ovTableHeader"	columnClasses="ovColHdrText155"	style="width: 580px;margin-left: 9px;" cellspacing="0">
						<h:commandLink id="letterHdrCol1" value="Letters"styleClass="ovColSorted#{pc_correspondenceTable.sortedByCol1}"	
						actionListener="#{pc_correspondenceTable.sortColumn}"immediate="true" /><!-- FNB009-->
					</h:panelGrid>
				</h:panelGroup> 
				<h:panelGroup id="letterData" styleClass="ovDivTableData"style="height:550px; width: 590px;margin-left: 9px;"><!-- NBA184-->
					<h:dataTable id="letterTable" styleClass="ovTableData"cellspacing="0" rows="0" style="width: 585px;"
					binding="#{pc_correspondenceTable.lettersTable}" value="#{pc_correspondenceTable.lettersList}" var="letter"
					rowClasses="#{pc_correspondenceTable.rowStyles}"
					columnClasses="ovColText155"> <!-- FNB009-->
						<h:column>
							<h:commandLink id="letterCol1" value="#{letter.col1}"title="Letters" styleClass="ovFullCellSelect"action="#{pc_correspondenceTable.selectRow}" immediate="true" /><!-- FNB009-->
						</h:column>
					</h:dataTable>
				</h:panelGroup>
			</div>
		</div>
		<f:subview id="nbaCommentBar"> <!-- NBA184-->
			<c:import url="/common/subviews/NbaCommentBar.jsp" /> <!-- NBA184-->
		</f:subview> <!-- NBA184-->

		<h:panelGroup styleClass="formButtonBar">
				<h:commandButton value="#{property.buttonClear}"styleClass="formButtonLeft" style="left: 6px"action="#{pc_correspondenceTable.clear}" accesskey="L" /> <!-- FNB009-->
				<h:commandButton value="#{property.buttonCreate}"styleClass="formButtonRight" style="left: 550px" onclick="doResponse('#{pc_correspondence.responseURL}');"
				disabled="#{pc_correspondence.auth.enablement['Commit'] || 
									pc_correspondence.notLocked}"
				action="#{pc_correspondence.createLetter}" immediate="true" /> <!-- FNB009-->
		</h:panelGroup>
	</h:form>

		<!-- NBA213 code deleted -->
	<div id="Messages" style="display: none">
	<h:messages />
	</div>
</f:view>
</body>
</html>
