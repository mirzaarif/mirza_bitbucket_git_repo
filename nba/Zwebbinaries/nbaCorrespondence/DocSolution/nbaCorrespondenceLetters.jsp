<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA184            7      Correspondence Rewrite -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
<SCRIPT language="JavaScript" src="include/common.js"></SCRIPT>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaCorrespondence/DocSolution/nbaCorrespondenceLetters.faces';
		parent.fileLocationHRef = '<%=basePath%>' + 'nbaCorrespondence/DocSolution/nbaCorrespondenceLetters.faces';
		
		function setTargetFrame() {
			document.forms['form_correspondence'].target='controlFrame';
			return false;
		}
		
		function resetTargetFrame() {
			document.forms['form_correspondence'].target='';
			return false;
		}
		
		function decideTargetFrame()
		{
			if(document.forms['form_correspondence']['form_correspondence:localPrintChkBox'].checked)
			  setTargetFrame();
			else
			  resetTargetFrame();
		}
	</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CORRESPONDENCE" value="#{pc_correspondenceLetters}" />
	<h:form id="form_correspondence">
		<div id="createNew" class="inputFormMat"  style="overflow:auto; height:505px; width:100%;">
			<div id="lettertable" class="inputForm" style="height:485px; width:100%;"> 
				<h:panelGroup styleClass="formTitleBar">
						<h:outputLabel id="letterTableTitle" value="#{property.letterTableTitle}" styleClass="shTextLarge" />
				</h:panelGroup>		
				<h:panelGroup id="lettersHeader" styleClass="ovDivTableHeader" style="width: 550px;margin-left: 9px; margin-top: 16px;">
					<h:panelGrid columns="2" styleClass="ovTableHeader" columnClasses="ovColHdrText190,ovColHdrText190" style="width: 580px;margin-left: 9px;" cellspacing="0">
						<h:commandLink id="letterHdrCol1" value="#{property.correspondenceCol1}" styleClass="ovColSorted#{pc_correspondenceLetterTable.sortedByCol1}"
							actionListener="#{pc_correspondenceLetterTable.sortColumn}" immediate="true" />
						<h:commandLink id="letterHdrCol2" value="#{property.correspondenceCol2}" styleClass="ovColSorted#{pc_correspondenceLetterTable.sortedByCol2}"
							actionListener="#{pc_correspondenceLetterTable.sortColumn}" immediate="true" />
					</h:panelGrid>
		
				</h:panelGroup> 
				
				<h:panelGroup id="letterData" styleClass="ovDivTableData" style="height:390px; width: 590px;margin-left: 9px;">
					<h:dataTable id="letterTable" styleClass="ovTableData" cellspacing="0" rows="0" style="width: 585px;"
						binding="#{pc_correspondenceLetterTable.lettersTable}" value="#{pc_correspondenceLetterTable.lettersList}" var="letter"
						rowClasses="#{pc_correspondenceLetterTable.rowStyles}" columnClasses="ovColText210,ovColText190" >
						<h:column>
							<h:commandLink id="letterCol" value="#{letter.letter}" title="Letters" styleClass="ovFullCellSelect"
								action="#{pc_correspondenceLetterTable.selectRow}"/>
						</h:column>
						<h:column>
							<h:commandLink id="descriptionCol" value="#{letter.description}" title="Description" styleClass="ovFullCellSelect"
								action="#{pc_correspondenceLetterTable.selectRow}"/>
						</h:column>
					</h:dataTable>
				</h:panelGroup>
				<h:panelGroup styleClass="formButtonBar">
					<h:selectBooleanCheckbox id="localPrintChkBox"  value="#{pc_correspondenceLetters.localPrint}" styleClass="formEntryCheckbox" style="margin-left: 510px;margin-top: 5px;"
						disabled="#{pc_correspondenceLetters.localPrintDisabled}"/><h:outputText id="localPrintLabel" value="#{property.localPrint}" styleClass="formLabelRight" style="font-weight: normal;margin-top: 5px;" />
				</h:panelGroup>
			</div>			
		</div>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="viewBtn" value="#{property.buttonView}" styleClass="formButtonRight-1"
				onclick="setTargetFrame();" action="#{pc_correspondenceLetters.viewLetter}"
				disabled="#{pc_correspondenceLetters.viewDisabled}" />
			<h:commandButton id="createBtn" value="#{property.buttonCreate}" styleClass="formButtonRight"
				onclick="decideTargetFrame();" action="#{pc_correspondenceLetters.createLetter}"
				disabled="#{pc_correspondenceLetters.createDisabled}" />
		</h:panelGroup>
		</h:form>
		<div id="Messages" style="display:none">	
			<h:messages  />
		</div>
	</f:view>
</body>
</html>
