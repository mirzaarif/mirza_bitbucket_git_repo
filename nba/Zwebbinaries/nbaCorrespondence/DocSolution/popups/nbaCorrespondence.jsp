<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA184            7      Correspondence Rewrite -->
<!-- NBA213			   7	  Unified User Interface  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.correspondenceTitleBar}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" type="text/javascript">
		
		function setTargetFrame() {
			document.forms['form_correspondencehtmlpdfView'].target='controlFrame';
			return false;
		}
		
		function resetTargetFrame() {
			document.forms['form_correspondencehtmlpdfView'].target='';
			return false;
		}
		
        //create XML string to represent changed data
		function createdChangedXML() {
			var strXML = "";
			var iDocument = document.frames.item(0).document;
			if(iDocument.plugins.length == 0 && iDocument.forms.length > 0){		
				strXML += '<?xml version="1.0" ?><formset><form>';		
				strXML += '<name>' + iDocument.forms(0).name + '</name>';
				strXML += '<images>';				
				var images = iDocument.getElementsByTagName("DIV");
				var fields = null;
				for(var i = 0; i < images.length; i++){
					strXML += '<image>';
					strXML += '<name>' + images[i].id + '</name>';
					strXML += '<data>';
					fields = images[i].getElementsByTagName("INPUT");
					for(var j = 0; j < fields.length; j++){					
						if(isFieldInvalid(fields[j])){
							document.forms['form_correspondencehtmlpdfView']['form_correspondencehtmlpdfView:hiddenInput'].value = fields[j].name;
							setTargetFrame();
						 	return false;
						}
						strXML += '<field>' + fields[j].name + '</field>';
						strXML += '<value>' + fields[j].value + '</value>';					
					}
					strXML += '</data>';
					strXML += '</image>';
				}				
				strXML += '</images></form></formset>';
			}
			document.forms['form_correspondencehtmlpdfView']['form_correspondencehtmlpdfView:hiddenInput'].value = "";			
			document.forms['form_correspondencehtmlpdfView']['form_correspondencehtmlpdfView:customerXML'].value = strXML;
			resetTargetFrame();	
			return true;
		}
	
		function isFieldInvalid(aField) {
			var isInvalid = false;
			// 'E' stands for Extract and 'V' stands for Validator				
			if(aField.name.substr(0,1) != "E" && aField.name.substr(2,1) == "V" && aField.value.length == 0){
				aField.select();
				isInvalid = true;		
			}
			return isInvalid;  	
		}
		
		function closeWindow() {
	  		window.close();
		}
	
		function submitHiddenBtn() { 
		   		document.forms['form_correspondencehtmlpdfView']['form_correspondencehtmlpdfView:viewPdfHiddenButton'].click();
		}		

	</script>
	</head>
	<body onload="submitHiddenBtn();">
	<h:form id="form_correspondencehtmlpdfView">
		<IFRAME id="htmlPdfFrame" name="htmlPdfContainer" style="Left:10px;width: 100%;  height: 860px; "></IFRAME>
		<h:panelGrid columns="1" styleClass="commentBar" cellpadding="0" cellspacing="0">
			<h:column>
				<h:panelGroup styleClass="formButtonBar">
					<h:commandButton id="buttonClose" value="#{property.buttonClose}" styleClass="formButtonLeft" onclick="closeWindow();"
						action="#{pc_correspondenceLetters.closeLetter}" />
					<h:commandButton id="buttonBack" value="#{property.buttonBack}" styleClass="formButtonRight" onclick="resetTargetFrame();" 
						disabled="#{pc_correspondenceLetters.backBtnDisabled}"	action="#{pc_correspondenceLetters.viewChangedLetter}" 
						rendered="#{pc_correspondenceLetters.renderPdf}"  style="left: 1097px;"/>
					<h:commandButton id="buttonCreate" value="#{property.buttonCreate}" styleClass="formButtonRight" onclick="createdChangedXML();"
						disabled="#{pc_correspondenceLetters.auth.enablement['Commit'] || 
									pc_correspondenceLetters.notLocked}"
						action="#{pc_correspondenceLetters.createLetterFromHtml}" rendered="#{!pc_correspondenceLetters.renderPdf}"  style="left: 1097px"/> <!-- NBA213 -->
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
		<h:inputHidden id="customerXML" value="#{pc_correspondenceLetters.customerXML}" />
		<h:commandButton id="viewPdfHiddenButton" style="display:none;" value="HiddenButton" actionListener="#{pc_correspondenceLetters.viewPdf}"
			onclick="document.forms['form_correspondencehtmlpdfView'].target='htmlPdfContainer';" />
		<h:inputHidden id="hiddenInput" value="#{pc_correspondenceLetters.invalidField}" />
	</h:form>
	<div id="Messages"><h:messages /></div>
	</body>
</f:view>
</html>
