<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- FNB009         NB-1101   Correspondence- Design and develop new interface to display information  of generated letters   -->


<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>nbA Correspondence</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script type="text/javascript"	src="javascript/global/desktopComponent.js"></script>
<script language="JavaScript" type="text/javascript">
						
				function setTargetFrame() {					
					document.forms['form_launchDoc'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {					
					document.forms['form_launchDoc'].target='';
					return false;
				}
				function load(documentId) {
					try {
						var frameElement = document.getElementById('Frame'+documentId);
						frameElement.contentWindow.location.href = frameElement.contentWindow.location.href;
						} catch (ex) {
						alert(ex.message);
							}
					return false;
				
				}

</script>
</head>
<body>
	<div id="contentArea" style="height: 300; background-color: Transparent; overflow-x: hidden; overflow-y: hidden">
		<form id="form_launchDoc">
			<span id="pGroup1"	class="popupDivTableHeader" style="margin-left: 0px; width: 100%;">
				<table class="popupTableHeader" cellspacing="0" style="width: 100%;">
					<tbody>
						<tr>
							<td class="ovColHdrText50">Letter No.</td>
							<td class="ovColHdrText165">Letter Name</td>
							<td class="ovColHdrText165">Party Name</td>
						</tr>
					</tbody>
				</table>
			</span>
			 <span id="searchData" class="popupDivTableData6"style="height: 400px; width: 100%; margin-left: 0px; overflow-x: hidden; overflow-y: auto">
				<table id="lettersTable" class="ovTableData" cellspacing="0"style="width: 100%;">
	<tbody>

		<%
			java.util.Map docMap = com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getCorrespondenceGeneric().getDocuments();
			java.util.ResourceBundle rs = com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getCorrespondenceGeneric().getProps();

			String documentName = null;
			String letterName = com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getCorrespondenceGeneric().getLetter();
			java.util.List frameArr = new java.util.ArrayList();
			java.util.Iterator docIterator = docMap.keySet().iterator();
			int i = 0;
					
			while (docIterator.hasNext()) {
				String retrieveValue = (String) docIterator.next();
				String documentStr = (String)docMap.get(retrieveValue);
				boolean error = false;
				if (documentStr.equals("")) {
					error = true ;
				}
				java.util.StringTokenizer st = new java.util.StringTokenizer(retrieveValue, ":");
				String token = "";
				while (st.hasMoreTokens()) {
					token = st.nextToken();
					break;
				}
				String key = rs.getString(token);
				i++;
				String displayName = key;
				String link = "";
				
				if(!error) {
					link = "<tr class=\"onrowselect\"><td class=\"ovColText50\"><a id=\'" + retrieveValue
						+ "\' style=\"margin-left: 10px;\" isClicked=\'false\' text-decoration: none; href=\'#\'  onclick=\'try{load(\""
						+ retrieveValue + "\")}catch(ex){}; return false\'>" + i + "     . </a></td><td class=\"ovColText165\"><a id=\'"
						+ retrieveValue + "\' style=\"margin-left: 10px;\" isClicked=\'false\' href=\'#\' onclick=\'try{load(\"" + retrieveValue
						+ "\")}catch(ex){}; return false\'>" + letterName + "</a>	</td><td class=\"ovColText165\"><a id=\'" + retrieveValue
						+ "\' isClicked=\'false\' href=\'#\' onclick=\'try{load(\"" + retrieveValue + "\")}catch(ex){}; return false\'>" + key
						+ "</a></td></tr>";
					String frame = "<iframe id=\'Frame" + retrieveValue + "\' name=\'Frame" + retrieveValue + "\' style=\"position: absolute;left: " + i
						+ "px; top: " + i + "px;  \" src=\'" + basePath + "/nbaCorrespondence/showDoc.faces?displayName=" + displayName
						+ "&documentName=" + retrieveValue + "\' />";

					frameArr.add(frame);
				} else {
					java.util.ResourceBundle resournceBundle = java.util.ResourceBundle.getBundle("properties.nbaApplicationMessages");
					String errorMsgPattern = resournceBundle.getString("mx_ValidationError");
					String errorMsg = java.text.MessageFormat.format(errorMsgPattern,new Object[]{key});
					link = "<tr class=\"onrowselect\"><td class=\"ovColText50\"><a id=\'" + retrieveValue
						+ "\' style=\"margin-left: 10px;\" isClicked=\'false\' text-decoration: none; href=\'#\'  onclick=\'return false\'>" + i + "     . </a></td><td class=\"ovColText165\"><a id=\'"
						+ retrieveValue + "\' style=\"margin-left: 10px;\" isClicked=\'false\' href=\'#\' onclick=\'return false\'>" + letterName + "</a>	</td><td class=\"ovColText165\"><a id=\'" + retrieveValue
						+ "\' isClicked=\'false\' href=\'#\' onclick=\'return false\'>" + key +" : <font color=\"red\">"+errorMsg
						+ "</font></a></td></tr>";
				}
				out.println(link);
				
			}
		%>
		<tr rowspan="4">
			<td colspan="2"><span id="pGroup5" class="buttonBar"> <input
				id="cancelButton1" type="submit" value="Cancel" class="buttonRight"
				onclick="top.window.close();  " />
			</span></td>
		</tr>
	</tbody>


	<%
		for (int j = 0; j < frameArr.size(); j++) {
			out.println(frameArr.get(j));
		}
	%>


</table>
			</span>
		</form>
	</div>
	</body>
</html>



