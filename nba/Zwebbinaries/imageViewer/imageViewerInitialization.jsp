<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA350     	NB-1401   	Applet Implementation of AWD Image Viewer -->


<%@ page language="java" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Image Viewer Initiialzation</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>

	/*
	This jsp is loaded into the ImgViewer iframe by applicationFrameset.html.
	The initialize() function in /nba/WebContent/desktops/menus/coordinator.jsp calls the 
	loadImageViewer(userid) function below. 
	The loadImageViewer()function first calls the createSession(userId, url)to create an AWD jsession  for the User.
	Once this is done, the contents of the ImgViewer iframe (which contains this JSP) is replaced by imageViewer.jsp.
	imageViewer.jsp contains an APPLET tag which points to the jar file containing the code for the Image Viewer
	run time. The APPLET tag causes the Image Viewer to be loaded.
	If the Image Viewer had been loaded prior to establishing the AWD jsession, the user would have been prompted by Java for 
	authentication information.    
	Since this JSP has been replaced in the ImgViewer iframe, subsequent javacript references to the iframe reference the Image Viewer.   
	*/
	function loadImageViewer(userid, userpwd, url) {
	 	//alert(url);
		if (url) {	//URL is defined for the AWD10xApplet only
			createSession(userid, userpwd, url);
			top.document.getElementById("ImgViewer").src = "imageViewer/imageViewer.jsp";	//Replace the contents of ImgViewer iframe with imageViewer.jsp
		}
	}
	/*
	Establish an AWD jsession 
	*/
	function createSession(userId, userpwd, url){	
		//alert(userId + ' ' + userpwd); 
		var http;
 		if (window.XMLHttpRequest)
   			{// code for IE7+, Firefox, Chrome, Opera, Safari
   				http=new XMLHttpRequest();
   			}
 		else
   			{// code for IE6, IE5
   				http=new ActiveXObject("Microsoft.XMLHTTP");
   			}
		var awdrequest='<?xml version="1.0" encoding="UTF-8"?><DST><jobName>SRVAuthenticate</jobName><AWD xml:lang="en-US"><userID>'+userId+'</userID><password>'+userpwd+'</password></AWD></DST>';
		http.open("POST", url, true);
		http.onreadystatechange = function() {//Call a function when the state changes.
			if(http.readyState == 4 && http.status == 200) {
				//alert('http.responseText: '+http.responseText);
			}
		}			
		//Set the Header
		http.setRequestHeader("DST_REQUEST", "SRVResource");
		http.setRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)");
		http.setRequestHeader("Accept", "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
		http.setRequestHeader("content-type", "text/xml");
		http.setRequestHeader("Content-length", awdrequest.length);
		http.setRequestHeader("Connection", "keep-alive");
		try {
			http.send(awdrequest);
		} catch(err){ 
		}
	 }
	 
</script>

</head>
	<body class="darkBlueBody">
</body>
</html>