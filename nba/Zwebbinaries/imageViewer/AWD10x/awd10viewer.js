//Javascript specific to AWD10x Content Viewer Applet.  It requires a valid session from the AWD server
// before calling the content viewer.

// 	Audit Number   	Version   Change Description 
//  NBA350			NB-1401   Applet Implementation of AWD Image Viewer 


function showAWDImageWindow(xmlData,url){
	alert('showAWDImageWindow(xmlData,url) called: '+xmlData);
	getJSessionID(xmlData,url);		
	AWDImageWindow.loadImageByAWDInterface(xmlData);
	AWDImageWindow.getImageWindow();			
	top.refreshDT();			
}
// Update the following as needed for a specific environment
// AWD Content Viewer requires call to SRVAuthenticate to establish session before calling applet. 
function getJSessionID(xmlData,url) {
	var userpwdindx=xmlData.indexOf("<password>");
	var userpwdindxend=xmlData.indexOf("</password>");
	var userpwd=xmlData.substring(userpwdindx+10,userpwdindxend);
	var useridindx=xmlData.indexOf("<userID>");
	var useridindxend=xmlData.indexOf("</userID>");
	var userid=xmlData.substring(useridindx+8,useridindxend);
	var http = new ActiveXObject("Microsoft.XMLHTTP");
	var awdrequest='<?xml version="1.0" encoding="UTF-8"?><DST><jobName>SRVAuthenticate</jobName><AWD xml:lang="en-US"><userID>'+userid+'</userID><password>'+userpwd+'</password></AWD></DST>';
	http.open("POST", url, true);
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			//alert('http.responseText: '+http.responseText);
		}
	}			
	//Set the Header
	http.setRequestHeader("DST_REQUEST", "SRVResource");
	http.setRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)");
	http.setRequestHeader("Accept", "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
	http.setRequestHeader("content-type", "text/xml");
	http.setRequestHeader("Content-length", awdrequest.length);
	http.setRequestHeader("Connection", "keep-alive");

	http.send(awdrequest);
}		

function showImages(xmlData) {
	try {
		ImageViewer.showAlerts = true;
		ImageViewer.loadImageByAWDInterface(xmlData);
		ImageViewer.getImageWindow();
	} catch (e) {
		alert("showImages exception " + e);
	}
}
function closeImages(xmlData){
	closeImages(); 
}
function closeImages() {
	try {
		ImageViewer.getImageWindow().closeAllImages();
	} catch (e) {
		alert("closeImages exception 1 " + e);
	}
}

function reopenImages(xmlData) {
	showImages(xmlData);
}
function closeImageViewer() {
	closeImages();
}
function activateImages(xmlData) {
	showImages(xmlData);
}

