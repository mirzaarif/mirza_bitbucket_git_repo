

function showImages(imageAttrib){
	try { 
		ImageViewer.showAlerts = true;
		ImageViewer.OpenDocumentsByXML(imageAttrib);
	} catch(e) {
	}
}

function closeImages(imageAttrib){
	try {
		ImageViewer.showAlerts = true;
		ImageViewer.CloseDocumentsByXML(imageAttrib);
	} catch (e) {
		// supressing any exceptions if the viewer is closed and reference to Active X instance is lost
	}
}

// SPR3519 new method
function closeImages(){
	try {
		ImageViewer.showAlerts = true;
		ImageViewer.CloseAllDocuments();
	} catch (e) {
		try {
			var newImgViewer = new ActiveXObject( "DST.ImageWin3.1" );
			newImgViewer.CloseAllDocuments();
			ImageViewer = newImgViewer;
		} catch(err) {
		}	
	}
}

//NBA212 new method
function reopenImages(imageAttrib){
	try {
		ImageViewer.showAlerts = true;
		ImageViewer.closeAllDocuments();
		showImages(imageAttrib);
	} catch(e) {
		// supressing any exceptions if the viewer is closed and reference to Active X instance is lost	
		// calling show images method to open the images
		showImages(imageAttrib);
	}
}
//NBA212 new method
function closeImageViewer(){
	try {
		ImageViewer.showAlerts = true;
		ImageViewer.closeAllDocuments();
		ImageViewer.ExitOnRelease( true );
		ImageViewer = null;
	} catch (e) {
		// supressing any exceptions if the viewer is closed and reference to Active X instance is lost	
		// Begin SPR3519
	   try {
	   		var newImgViewer = new ActiveXObject( "DST.ImageWin3.1" );
			newImgViewer.CloseAllDocuments();
			newImgViewer.ExitOnRelease( true );
			ImageViewer = null;
			top.ImgViewer.location.href = top.ImgViewer.location.href;
		} catch(err) {
		}
		// End SPR3519
	}
}

//NBA212 new method
function activateImages(imageAttrib){
	try{
		ImageViewer.showAlerts = true;
		ImageViewer.ActivateDocumentsByXML(imageAttrib);
		ImageViewer.ExitOnRelease( true );
	} catch(e) {
	}
}
