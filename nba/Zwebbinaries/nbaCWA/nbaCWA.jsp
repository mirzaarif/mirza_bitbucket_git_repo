<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA169           	6         	CWA Apply Reverse and Refund Rewrite -->
<!-- NBA158 		   		6	  		Websphere 6.0 upgrade -->
<!-- NBA213 		  		7		  	Unified User Interface -->
<!-- SPR1613			   	8	  		Some Business Functions should be disabled on an Issued Contract  --> 
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-747     NB-1401   	General Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPR3282     	NB-1401   Table Pane Focus Should Not Reset to Top When Activity is Selected -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Cash With Application</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script><!-- SPR3282  -->
<!-- NBA213 code deleted -->
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_cwa'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cwa'].target='';
			return false;
		}
		//SPR3282 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_cwa:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwa:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
		//SPR3282 New Method
		function scrollTablePosition() {
			scrollToPosition('form_cwa:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwa:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll"><!-- SPR3282  -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CWA" value="#{pc_CWA}" />
	<h:form id="form_cwa" onsubmit="saveTableScrollPosition();"><!-- SPR3282  -->
		<f:subview id="cwaOverviewTable">
			<c:import url="/nbaCWA/subviews/nbaCwaOverviewTable.jsp" />
		</f:subview>
		<h:panelGrid id="buttonGrid" columns="3" styleClass="ovButtonBar" cellspacing="0">
			<h:panelGroup styleClass="formButtonBar">
				<h:commandButton id="btnDelete" value="#{property.buttonDelete}" onclick="setTargetFrame();" action="#{pc_CWAPaymentTable.showConfirmDelete}"
					styleClass="formButtonLeft" disabled="#{pc_CWAPaymentTable.disableDelete}" />
				<h:commandButton id="btnViewUpdate" value="#{property.buttonViewUpdate}" action="#{pc_CWA.viewUpdate}"
					disabled="#{pc_CWAPaymentTable.selectedCWAPayment==null}" styleClass="formButtonRight-1" />  <!-- SPRNBA-747 -->
				<h:commandButton id="btnCreate" value="#{property.buttonCreate}" action="#{pc_CWA.create}" styleClass="formButtonRight" />
			</h:panelGroup>
		</h:panelGrid>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnContRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft" action="#{pc_CWA.refresh}" immediate="true" />
			<h:commandButton id="btnContCommit" value="#{property.buttonCommit}"
				disabled="#{pc_CWA.auth.enablement['Commit'] || 
									pc_CWA.notLocked ||
									pc_CWA.issued}"
				styleClass="tabButtonRight" action="#{pc_CWA.commit}" /> <!-- SPR1613 -->
			<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<!-- NBA213 code delete -->
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
