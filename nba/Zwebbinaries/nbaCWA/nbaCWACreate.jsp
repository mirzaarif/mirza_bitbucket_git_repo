<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA169           6         CWA Apply Reverse and Refund Rewrite -->
<!-- NBA158 		  6			Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPR3282     	NB-1401   Table Pane Focus Should Not Reset to Top When Activity is Selected -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Requirements and Impairments Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script><!-- SPR3282  -->
<!-- NBA213 code deleted -->
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_cwaCreate'].target='controlFrame';	//NBA213
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cwaCreate'].target='';	//NBA213
			return false;
		}
		
		//SPR3282 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_cwaCreate:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwaCreate:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
		
		//SPR3282 New Method
		function scrollTablePosition() {
			scrollToPosition('form_cwaCreate:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwaCreate:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll"><!-- SPR3282  -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CWA" value="#{pc_CWA}" />

	<h:form id="form_cwaCreate" onsubmit="saveTableScrollPosition();">	<!-- NBA213 --><!-- SPR3282  -->
		<f:subview id="cwaOverviewTable">
			<c:import url="/nbaCWA/subviews/nbaCwaOverviewTable.jsp" />
		</f:subview>
		<div class="inputFormMat">
		<div class="inputForm" style="height: 350px"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createCWATitle" value="#{property.createCWATitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryTopLine">
			<h:outputText id="typeLabel" value="#{property.cwaType}" styleClass="formLabel" style="width: 150px" />
			<h:selectOneMenu id="Type" style="width: 460px" value="#{pc_CWA.createdCWAPayment.typeCode}" styleClass="formEntryTextHalf">
				<f:selectItems value="#{pc_CWA.financialActivityTypeList}" />
			</h:selectOneMenu>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="amountLabel" value="#{property.cwaAmount}" styleClass="formLabel" style="width: 150px" />
			<h:inputText id="Amount" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.amount}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="entryDateLabel" value="#{property.cwaEntryDate}" styleClass="formLabel" style="width: 150px" />
			<h:inputText id="EntryDate" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.entryDate}">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="originatorLabel" value="#{property.cwaOriginator}" styleClass="formLabel" style="width: 150px" />
			<h:inputText id="Originator" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.originator}">
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="asOfDateLabel" value="#{property.cwaAsOfDate}" styleClass="formLabel" style="width: 150px" />
			<h:inputText id="asOfDate" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.asOfDate}">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="setToIssue" value="#{property.cwaSetToIssue}" styleClass="formLabel" style="width: 150px" />
			<h:selectBooleanCheckbox id="setToIssueCheckBox" value="#{pc_CWA.createdCWAPayment.setToIssue}" />
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="previousTaxYear" styleClass="formLabel" style="width: 150px" value="#{property.cwaPreviousTaxYear}" />
			<h:selectBooleanCheckbox id="previousTaxYearCheckBox" value="#{pc_CWA.createdCWAPayment.previousTaxYear}" />
			<h:outputText id="costBasis" value="#{property.cwaCostBasis}" styleClass="formLabel" style="width: 200px" />
			<h:inputText id="costBasisInput" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.costBasis}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="mostBeneficialInterest" styleClass="formLabel" style="width: 150px" value="#{property.cwaMostBeneficialInterest}" />
			<h:selectBooleanCheckbox id="mostBeneficialInterestCheckBox" onclick="submit();" valueChangeListener="#{pc_CWA.mostBeneficialInterestChanged}"
				immediate="true" value="#{pc_CWA.createdCWAPayment.mostBeneficialInterest}" />
			<h:outputText id="overrideInterestRateLabel" styleClass="formLabel" style="width: 200px" value="#{property.cwaOverrideInterestRate}"
				rendered="#{!pc_CWA.createdCWAPayment.mostBeneficialInterest}">
			</h:outputText>
			<h:inputText id="overrideInterestRate" styleClass="formEntryText" rendered="#{!pc_CWA.createdCWAPayment.mostBeneficialInterest}"
				value="#{pc_CWA.createdCWAPayment.overrideInterestRate}">
				<f:convertNumber type="percent" maxFractionDigits="2" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLineBottom">
			<h:outputText id="preTEFRA" value="#{property.cwaPreTEFRA}" rendered="#{pc_CWA.annuity}" styleClass="formLabel" style="width: 150px" />
			<h:selectBooleanCheckbox id="preTEFRACheckBox" rendered="#{pc_CWA.annuity}" value="#{pc_CWA.createdCWAPayment.preTEFRA}" />
		</h:panelGroup> <h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" action="#{pc_CWA.cancel}" immediate="true" styleClass="formButtonLeft" />
			<h:commandButton id="btnClear" value="#{property.buttonClear}" action="#{pc_CWA.clear}" onclick="resetTargetFrame();" styleClass="formButtonLeft-1" />
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" action="#{pc_CWA.update}" onclick="resetTargetFrame();"
				styleClass="formButtonRight" rendered="#{pc_CWA.fromViewUpdate}" />
			<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" action="#{pc_CWA.addNew}" styleClass="formButtonRight-1"
				rendered="#{!pc_CWA.fromViewUpdate}" />
			<h:commandButton id="btnAdd" value="#{property.buttonAdd}" action="#{pc_CWA.add}" styleClass="formButtonRight" onclick="resetTargetFrame();"
				rendered="#{!pc_CWA.fromViewUpdate}" />
		</h:panelGroup></div>
		</div>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
