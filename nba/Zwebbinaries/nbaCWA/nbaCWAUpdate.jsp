<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA169           6         CWA Apply Reverse and Refund Rewrite -->
<!-- NBA158 		  6	  		Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-476		NB-1101		CWA Originator of Refund or Reversal Should Default to Requesting User and Entry Date to Current Date -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- SPR3282     	NB-1401   Table Pane Focus Should Not Reset to Top When Activity is Selected -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
        String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Requirements and Impairments Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script><!-- SPR3282  -->
<!-- NBA213 code deleted -->
<script language="JavaScript" type="text/javascript">
					
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_cwaUpdate'].target='controlFrame';	//NBA213
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_cwaUpdate'].target='';	//NBA213
			return false;
		}
		
		//SPR3282 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_cwaUpdate:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwaUpdate:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
		
		//SPR3282 New Method
		function scrollTablePosition() {
			scrollToPosition('form_cwaUpdate:cwaOverviewTable:cwaPaymentOverviewData', 'form_cwaUpdate:cwaOverviewTable:cwaTableVScroll');
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll"><!-- SPR3282  -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CWA" value="#{pc_CWA}" />

	<h:form id="form_cwaUpdate" onsubmit="saveTableScrollPosition();">	<!-- NBA213 --><!-- SPR3282  -->
		<f:subview id="cwaOverviewTable">
			<c:import url="/nbaCWA/subviews/nbaCwaOverviewTable.jsp" />
		</f:subview>
		<div class="inputFormMat">
		<div class="inputForm"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="viewUpdateTitle" value="#{property.cwaViewUpdateTitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryTopLine">
			<h:outputText id="type" value="#{property.cwaType}" styleClass="formLabel" style="vertical-align: top;width: 150px" />
			<h:inputTextarea readonly="true" value="#{pc_CWA.createdCWAPayment.type}" styleClass="formDisplayTextMultiLine" style="width:400px" />
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="amount" value="#{property.cwaAmount}" styleClass="formLabel" style="width: 150px" />
			<h:outputText id="amountText" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.amount}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
			</h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="entryDate" value="#{property.cwaEntryDate}" styleClass="formLabel" style="width: 150px" />
			<h:outputText id="entryDateText" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.entryDate}">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="originator" value="#{property.cwaOriginator}" styleClass="formLabel" style="width: 150px" />
			<h:outputText id="originatorText" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.originator}"></h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="disbursed" value="#{property.cwaDisbursed}" styleClass="formLabel" style="width: 150px" />
			<h:selectBooleanCheckbox id="disbursedCheckBox" disabled="true" value="#{pc_CWA.createdCWAPayment.disbursed}" />
		</h:panelGroup> <f:verbatim>
			<hr class="formSeparator" style="margin-top: 2px;margin-bottom: 2px;"/>
		</f:verbatim> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="asOfDateLabel" value="#{property.cwaAsOfDate}" styleClass="formLabel" style="width: 150px" />
			<h:inputText id="asOfDate" styleClass="formEntryText" rendered="#{pc_CWA.createdCWAPayment.canUpdate}"
				value="#{pc_CWA.createdCWAPayment.asOfDate}">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
			<h:outputText id="asOfDateText" value="#{pc_CWA.createdCWAPayment.asOfDate}" rendered="#{!pc_CWA.createdCWAPayment.canUpdate}"
				styleClass="formEntryText">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="setToIssue" value="#{property.cwaSetToIssue}" styleClass="formLabel" style="width: 150px" />
			<h:selectBooleanCheckbox id="setToIssueCheckBox" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}" value="#{pc_CWA.createdCWAPayment.setToIssue}" />
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="previousTaxYear" styleClass="formLabel" style="width: 150px" value="#{property.cwaPreviousTaxYear}" />
			<h:selectBooleanCheckbox id="previousTaxYearCheckBox" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}"
				value="#{pc_CWA.createdCWAPayment.previousTaxYear}" />
			<h:outputText id="costBasisLabel" value="#{property.cwaCostBasis}" styleClass="formLabel" style="width: 200px" />
			<h:inputText id="costBasis" styleClass="formEntryText" value="#{pc_CWA.createdCWAPayment.costBasis}"
				rendered="#{pc_CWA.createdCWAPayment.canUpdate}">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
			</h:inputText>
			<h:outputText id="costBasisText" value="#{pc_CWA.createdCWAPayment.costBasis}" rendered="#{!pc_CWA.createdCWAPayment.canUpdate}"
				styleClass="formEntryText">
				<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
			</h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLine">
			<h:outputText id="mostBeneficialInterest" styleClass="formLabel" style="width: 150px" value="#{property.cwaMostBeneficialInterest}" />
			<h:selectBooleanCheckbox id="mostBeneficialInterestCheckBox" onclick="submit();" valueChangeListener="#{pc_CWA.mostBeneficialInterestChanged}"
				immediate="true" value="#{pc_CWA.createdCWAPayment.mostBeneficialInterest}" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}" />

			<h:outputText id="overrideInterestRateLabel" styleClass="formLabel" style="width: 200px" value="#{property.cwaOverrideInterestRate}"
				rendered="#{!pc_CWA.createdCWAPayment.mostBeneficialInterest}">
			</h:outputText>
			<h:inputText id="overrideInterestRate" styleClass="formEntryText"
				rendered="#{!(pc_CWA.createdCWAPayment.mostBeneficialInterest||!pc_CWA.createdCWAPayment.canUpdate)}"
				value="#{pc_CWA.createdCWAPayment.overrideInterestRate}">
				<f:convertNumber type="percent" maxFractionDigits="2" />
			</h:inputText>
			<h:outputText id="overrideInterestRateText" value="#{pc_CWA.createdCWAPayment.overrideInterestRate}"
				rendered="#{!(pc_CWA.createdCWAPayment.mostBeneficialInterest||pc_CWA.createdCWAPayment.canUpdate)}" styleClass="formEntryText">
				<f:convertNumber type="percent" maxFractionDigits="2" />
			</h:outputText>
		</h:panelGroup> <h:panelGroup styleClass="formDataEntryLineBottom" rendered="#{pc_CWA.annuity}">
			<h:outputText id="preTEFRA" value="#{property.cwaPreTEFRA}" styleClass="formLabel" style="width: 150px" />
			<h:selectBooleanCheckbox id="preTEFRACheckBox" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}" value="#{pc_CWA.createdCWAPayment.preTEFRA}" />
		</h:panelGroup> <h:panelGroup>
			<h:panelGroup styleClass="formSectionBar">
				<h:outputLabel id="reversePartialRefundTitle" value="#{property.cwaRefundTitle}" />
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryTopLine">
				<h:outputText id="action" value="#{property.cwaAction}"
					rendered="#{!pc_CWA.createdCWAPayment.refundActionEmpty || pc_CWA.createdCWAPayment.canUpdate}" styleClass="formLabel" style="width: 150px" />
				<h:selectOneMenu id="actionCombo" value="#{pc_CWA.createdCWAPayment.refundActionCode}" valueChangeListener="#{pc_CWA.finActSubTypeChanged}"
					rendered="#{pc_CWA.createdCWAPayment.canUpdate}" styleClass="formEntryTextHalf" style="width: 360px" onchange="submit()" immediate="true">
					<f:selectItems value="#{pc_CWA.financialActivitySubTypeList}" />
				</h:selectOneMenu>
				<h:outputText id="actionText" value="#{pc_CWA.createdCWAPayment.refundAction}" rendered="#{!pc_CWA.createdCWAPayment.canUpdate}"
					styleClass="formEntryText" />
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine">
				<!-- begin SPRNBA-476 -->
				<h:outputText id="CurrentOriginatorLabel" value="#{property.cwaOriginator}" styleClass="formLabel" style="width: 150px"
					rendered="#{pc_CWA.renderCurrentOrgAndDate}" />
				<h:outputText id="CurrentOriginator" value="#{pc_CWA.createdCWAPayment.currentOriginator}" styleClass="formEntryText"
					rendered="#{pc_CWA.renderCurrentOrgAndDate}" >
				</h:outputText>
				<!-- end SPRNBA-476  -->
				<h:outputText id="errorCorrection" styleClass="formLabel" rendered="#{pc_CWA.renderErrCorr}" style="width: 255px"
					value="#{property.cwaErrorCorrection}" />
				<h:selectBooleanCheckbox id="errorCorrectionCheckBox" rendered="#{pc_CWA.renderErrCorr}"
					binding="#{pc_CWA.createdCWAPayment.errorCorrectionCheckbox}" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}"
					value="#{pc_CWA.createdCWAPayment.errorCorrection}" />

				<h:outputText id="refundAmountLabel" value="#{property.cwaAmount}" styleClass="formLabel" style="align: right;width: 155px"
					rendered="#{pc_CWA.renderGrp}" /> <!-- SPRNBA-476  -->
				<h:inputText id="RefundAmount" styleClass="formEntryText" style="align: right"
					rendered="#{!(!pc_CWA.renderGrp||!pc_CWA.createdCWAPayment.canUpdate)}" binding="#{pc_CWA.createdCWAPayment.refundAmountText}"
					value="#{pc_CWA.createdCWAPayment.refundAmount}">
					<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
				</h:inputText>
				<h:outputText id="refundAmountText" value="#{pc_CWA.createdCWAPayment.refundAmount}" styleClass="formEntryText"
					rendered="#{!(pc_CWA.createdCWAPayment.canUpdate||!pc_CWA.renderGrp)}">
					<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" maxFractionDigits="2" />
				</h:outputText>
			</h:panelGroup>
			<!-- begin SPRNBA-476 -->
			<h:panelGroup styleClass="formDataEntryLine" rendered="#{pc_CWA.renderGrp || pc_CWA.renderCurrentOrgAndDate}">
				<h:outputText id="CurrentEntryDateLabel" value="#{property.cwaEntryDate}" styleClass="formLabel" style="width: 150px"
					rendered="#{pc_CWA.renderCurrentOrgAndDate}" />
				<h:outputText id="CurrentEntryDate" value="#{pc_CWA.createdCWAPayment.currentEntryDate}" styleClass="formEntryText"
					rendered="#{pc_CWA.renderCurrentOrgAndDate}" style="width: 70px" >
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
				<h:outputText id="refundAsOfDateLabel" styleClass="formLabel" style="width: 142px" value="#{property.cwaAsOf}" rendered="#{pc_CWA.renderGrp}"/>
				<h:inputText id="refundAsOfDate" styleClass="formEntryText" style="align: right" rendered="#{!(!pc_CWA.renderGrp||!pc_CWA.createdCWAPayment.canUpdate)}"
					binding="#{pc_CWA.createdCWAPayment.refundAsOfText}" value="#{pc_CWA.createdCWAPayment.refundAsOfDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<!-- end SPRNBA-476  -->
				<h:outputText id="refundAsOfDateText" value="#{pc_CWA.createdCWAPayment.refundAsOfDate}" styleClass="formEntryText"
					rendered="#{!(pc_CWA.createdCWAPayment.canUpdate||!pc_CWA.renderGrp)}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>	
			</h:panelGroup>
		</h:panelGroup> <f:verbatim>
			<hr class="formSeparator" style="margin-top: 2px;margin-bottom: 2px;"/>
		</f:verbatim> <h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" action="#{pc_CWA.cancel}" onclick="resetTargetFrame();" immediate="true"
				styleClass="formButtonLeft" />
			<h:commandButton id="btnClear" value="#{property.buttonClear}" action="#{pc_CWA.clear}" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}"
				onclick="resetTargetFrame();" styleClass="formButtonLeft-1" />
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" action="#{pc_CWA.update}" disabled="#{!pc_CWA.createdCWAPayment.canUpdate}"
				onclick="resetTargetFrame();" styleClass="formButtonRight" />
		</h:panelGroup></div>
		</div>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
