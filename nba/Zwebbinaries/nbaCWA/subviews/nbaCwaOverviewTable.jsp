<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA169           	6       	CWA Apply Reverse and Refund Rewrite -->
<!-- SPR3562           	8       	Premium Amounts Larger than Seven Whole Numbers and Two Decimals Are Not Handled Correctly -->
<!-- SPRNBA-694		NB-1301 		CWA Activity Column Not Italicized on Draft-->
<!-- SPR3282     	NB-1401   		Table Pane Focus Should Not Reset to Top When Activity is Selected -->
<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formTitleBar">
		<h:outputLabel id="cwaOverviewTitle" value="#{property.cwaOverviewTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="cwaOverviewHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" cellspacing="0" style="width: 605px"
			columnClasses="ovColHdrIcon,ovColHdrText170,ovColHdrText140,ovColHdrDate,ovColHdrText125,ovColHdrText70"> <!-- SPR3562 -->
			<h:commandLink id="cwaPaymentOverviewHdrCol1" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol1}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol1}" />
			<h:commandLink id="cwaPaymentOverviewHdrCol2" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol2}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol2}" />
			<h:commandLink id="cwaPaymentOverviewHdrCol3" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol3}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol3}" />
			<h:commandLink id="cwaPaymentOverviewHdrCol4" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol4}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol4}" />
			<h:commandLink id="cwaPaymentOverviewHdrCol5" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol5}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol5}" />
			<h:commandLink id="cwaPaymentOverviewHdrCol6" actionListener="#{pc_CWAPaymentTable.sortColumn}" immediate="true"
				value="#{property.cwaPaymentOverviewCol6}" styleClass="ovColSorted#{pc_CWAPaymentTable.sortedByCol6}" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="cwaPaymentOverviewData" styleClass="ovDivTableData">
		<h:dataTable id="cwaPaymentOverviewDataTable" styleClass="ovTableData" cellspacing="0" rows="0" border="0" binding="#{pc_CWAPaymentTable.dataTable}"
			value="#{pc_CWAPaymentTable.cwaPaymentsList}" var="paymentItem" rowClasses="#{pc_CWAPaymentTable.rowStyles}"
			columnClasses="ovColIconTop,ovColText170,ovColText140,ovColDate,ovColText125,ovColText70"> <!-- SPR3562 -->
			<h:column>
				<h:commandLink id="creditCardPaymentIcon" styleClass="ovFullCellSelect" style="width: 100%;"
					action="#{pc_CWAPaymentTable.selectPaymentOverviewRow}" onmousedown="saveTableScrollPosition();" immediate="true"><!-- SPR3282  -->
					<h:graphicImage url="images/creditcardIcon.gif" rendered="#{paymentItem.renderCreditCard}" style="border-width: 0px;" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="type" value="#{paymentItem.type}" styleClass="ovFullCellSelect" style="width:100%"
					action="#{pc_CWAPaymentTable.selectPaymentOverviewRow}" onmousedown="saveTableScrollPosition();" immediate="true" /><!-- SPR3282 -->
			</h:column>
			<h:column>
				<h:commandLink id="activity" value="#{paymentItem.refundAction}" styleClass="ovMultiLine#{paymentItem.multilineDraftStyle}" style="width:100%"
					action="#{pc_CWAPaymentTable.selectPaymentOverviewRow}" onmousedown="saveTableScrollPosition();" immediate="true" />  <!-- SPR3562 --><!-- SPRNBA-694 --><!-- SPR3282  -->
			</h:column>
			<h:column>
				<h:commandLink id="date" styleClass="ovFullCellSelect" style="width:100%" action="#{pc_CWAPaymentTable.selectPaymentOverviewRow}" onmousedown="saveTableScrollPosition();" immediate="true"><!-- SPR3282  -->
					<h:outputText value="#{paymentItem.asOfDate}">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="amount" styleClass="ovFullCellSelect" style="text-align: right;width: 100%" action="#{pc_CWAPaymentTable.selectPaymentOverviewRow}"
					immediate="true" onmousedown="saveTableScrollPosition();"><!-- SPR3282  -->
					<h:outputText value="#{paymentItem.amount}">
						<f:convertNumber currencySymbol="#{property.currencySymbol}" type="currency" minFractionDigits="2" maxFractionDigits="2" />
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:selectBooleanCheckbox id="disbursedIndicator" value="#{paymentItem.disbursed}" styleClass="ovFullCellSelectCheckBox" style="width:100%"
					disabled="true" onmousedown="saveTableScrollPosition();" /><!-- SPR3282  -->
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.previousAbsolute}" rendered="#{pc_CWAPaymentTable.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.previousPageAbsolute}" immediate="true" />
		<h:commandLink value="#{property.previousPage}" rendered="#{pc_CWAPaymentTable.showPrevious}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.previousPage}" immediate="true" />
		<h:commandLink value="#{property.previousPageSet}" rendered="#{pc_CWAPaymentTable.showPreviousSet}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.previousPageSet}" immediate="true" />
		<h:commandLink value="#{pc_CWAPaymentTable.page1Number}" rendered="#{pc_CWAPaymentTable.showPage1}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page1}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page1Number}" rendered="#{pc_CWAPaymentTable.currentPage1}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page2Number}" rendered="#{pc_CWAPaymentTable.showPage2}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page2}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page2Number}" rendered="#{pc_CWAPaymentTable.currentPage2}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page3Number}" rendered="#{pc_CWAPaymentTable.showPage3}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page3}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page3Number}" rendered="#{pc_CWAPaymentTable.currentPage3}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page4Number}" rendered="#{pc_CWAPaymentTable.showPage4}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page4}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page4Number}" rendered="#{pc_CWAPaymentTable.currentPage4}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page5Number}" rendered="#{pc_CWAPaymentTable.showPage5}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page5}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page5Number}" rendered="#{pc_CWAPaymentTable.currentPage5}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page6Number}" rendered="#{pc_CWAPaymentTable.showPage6}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page6}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page6Number}" rendered="#{pc_CWAPaymentTable.currentPage6}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page7Number}" rendered="#{pc_CWAPaymentTable.showPage7}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page7}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page7Number}" rendered="#{pc_CWAPaymentTable.currentPage7}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{pc_CWAPaymentTable.page8Number}" rendered="#{pc_CWAPaymentTable.showPage8}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.page8}" immediate="true" />
		<h:outputText value="#{pc_CWAPaymentTable.page8Number}" rendered="#{pc_CWAPaymentTable.currentPage8}" styleClass="ovStatusBarTextBold" />
		<h:commandLink value="#{property.nextPageSet}" rendered="#{pc_CWAPaymentTable.showNextSet}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.nextPageSet}" immediate="true" />
		<h:commandLink value="#{property.nextPage}" rendered="#{pc_CWAPaymentTable.showNext}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.nextPage}" immediate="true" />
		<h:commandLink value="#{property.nextAbsolute}" rendered="#{pc_CWAPaymentTable.showNext}" styleClass="ovStatusBarText"
			action="#{pc_CWAPaymentTable.nextPageAbsolute}" immediate="true" />
	</h:panelGroup>
	<h:inputHidden id="cwaTableVScroll" value="#{pc_CWAPaymentTable.VScrollPosition}" />  <!-- SPR3282  -->
</jsp:root>
