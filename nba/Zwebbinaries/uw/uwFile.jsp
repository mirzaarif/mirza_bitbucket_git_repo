<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3055           6      Decoupling of Version 5 JSF views from Accelerator Framework -->
<!-- NBA208            7      Performance Tuning and Testing -->
<!-- SPRNBA-947 	NB-1601   Null Pointer Exception May Occur in Inbox if User Selects Different Work -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Underwriter Workbench File</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = -22;   //[TODO] need audit number, change required due to iteration_1a merge
			var fileLocationHRef = null;  //NBA213
			var defaultindex = 1;
			var commentsIndex = 2;
			var commentReturnIndex = 0;
			var draftChanges = false;

			// SPR2965 New Function
			function mainTabSize() {
				//NBA213 code deleted
				winHeight = window.screen.availHeight;
				// 175 = 37(browser title) + 33(title) + 23(menu) + 50(status) + 32(tabs)
				tabHeight = winHeight - 175;   //[TODO] need audit number, change required due to iteration_1a merge
				document.getElementById('mainTab').height = tabHeight;
			}
			
		//-->
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body class="desktopBody" style="overflow-x: hidden; overflow-y: hidden">  <!-- SPR2965, SPRNBA-947 remove top.hideWait -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>  <!-- SPR3055 -->
		<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />
		<!-- NBA208 code deleted -->

		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:31px">
					<td><FileLoader:Files location="uw/file/" numTabsPerRow="6" disableIndex="5" disableValue="#{pc_uwbean.disableTab}" defaultIndex="#{pc_uwbean.defaultIndex}"/></td>
					<FileLoader:DisableTab disableIndex="6" disableValue="#{pc_uwbean.disableTab}"/>
				</tr>
				<tr style="height:*; vertical-align: top;">  <!-- SPR2965 -->
					<td><iframe id="mainTab" name="file" src="" width="100%" frameborder="0" scrolling="auto" onload="mainTabSize();"></iframe></td>  <!-- SPR2965 -->
				</tr>
			</tbody>
		</table>
		<!-- NBA213 code deleted -->
	</f:view>
</body>
</html>
