<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213            7      Unified User Interface -->
<!-- FNB011            	NB-1101	Work Tracking -->
<!-- NBA231          NB-1101      Replacement Processing -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
 			document.forms['form_ContractDetail'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
 			document.forms['form_ContractDetail'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_ContractDetail']['form_ContractDetail:draftChanges'].value;  //NBA213
		}			
	</script>
	<!--  FNB011 code deleted -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACTSUMMARY" value="#{pc_contractSummary}" />
	<h:form id="form_ContractDetail">
		<h:panelGroup styleClass="pageHeader">
			<h:commandLink action="#{pc_NbaCoverageNavigation.actionCloseSubView}" value="#{property.backTo}" styleClass="phText"
				style="position: absolute; left: 20px" />
			<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 200px" rendered="#{pc_NbaCoverageNavigation.auth.visibility['Images']}"/><!-- FNB011 --> 
			<h:commandButton image="images/link_icons/documents-stack.gif"
				onclick="setTargetFrame();" action="#{pc_NbaCoverageNavigation.viewAllSourcesForUW}" 
				style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle" rendered="#{pc_NbaCoverageNavigation.auth.visibility['Images']}"/><!-- NBA212 FNB011 -->
		</h:panelGroup>
		<f:subview id="contractTable">
			<c:import url="/uw/subviews/NbaCoverageContractTable.jsp" />
		</f:subview>
		<h:panelGroup id="viewContract1" styleClass="inputFormMat" >
			<h:panelGroup id="viewContract2" styleClass="inputForm" style="height: 420px;">	<!--  NBA231 -->
				<h:panelGroup id="viewContractHeader" styleClass="formTitleBar">
					<h:outputLabel id="contractDetailTitle" value="#{property.uwViewContractTitle}" styleClass="shTextLarge" />
				</h:panelGroup>
				<h:panelGroup id="viewContractDetailGroup">
					<f:subview id="viewContractDetail">
						<c:import url="/uw/subviews/nbaContractSummaryDetail.jsp" />
					</f:subview>
				</h:panelGroup>
				<f:verbatim>
					<hr class="formBarSeparator" />
				</f:verbatim>
				<h:panelGroup styleClass="formButtonBar">
					<h:commandButton id="btnCovContractClose" value="#{property.buttonClose}" styleClass="ovButtonLeft" style="position: relative; right: 10px;"
						action="#{pc_NbaCoverageNavigation.actionCloseSubView}" />
				</h:panelGroup>
				<!-- begin NBA213 -->
				<f:subview id="nbaCommentBar">
					<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
				</f:subview>
				<!-- end NBA213 -->
			</h:panelGroup>
		</h:panelGroup>	
		<!-- NBA213 code deleted -->
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />			
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
