<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3396           8      	Client Row Not Shown in Draft Mode After Update and Prior to Commit -->
<!-- FNB011 		 NB-1101	Work Tracking -->
<!-- NBA245 		 NB-1301    Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798      NB-1401    Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Client Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() { 
			document.forms['form_clientOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() { 
			document.forms['form_clientOverview'].target='';
			return false;
		}
		function setDraftChanges() {

		}		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();setDraftChanges();" style="background-color: white; overflow-x: hidden; overflow-y: scroll">
<f:view>

		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />
	<!-- NBA208-32 code delete -->
	<PopulateBean:Load serviceName="RETRIEVE_CLIENTS" value="#{pc_NbaClientNavigation}" />
	<PopulateBean:Load serviceName="RETRIEVE_CLIENTS" value="#{pc_NbaClientTableData}" />
	<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientDetails}" /> <!-- NBA245 -->
	<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientContactInfoTableData}" /> <!-- NBA245 -->
	
	<h:form id="form_clientOverview">
		<f:subview id="clientNavigation">
			<c:import url="/uw/subviews/NbaClientNavigation.jsp" />
		</f:subview>

		<f:subview id="clientContractTableReadOnly">
			<c:import url="/uw/file/NbaClientContractTableReadOnly.jsp" />
		</f:subview>

			<f:subview id="clientPersonDetailsUpdate" rendered="#{pc_NbaClientDetails.personParty}">
				<c:import url="/uw/subviews/NbaClientPersonInfoUpdate.jsp" />
			</f:subview>
			
			<f:subview id="clientOrganizationDetailsUpdate" rendered="#{pc_NbaClientDetails.organizationParty}">
				<c:import url="/uw/subviews/NbaClientOrganizationInfoUpdate.jsp" />
			</f:subview>
 
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
			
		<h:panelGroup id="activityViewButtonBar" styleClass="formButtonBar">
				<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" action="#{pc_NbaClientDetails.cancel}" immediate="true"/>
				<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight" style="left: 507px;" onclick="resetTargetFrame();" 
					action="#{pc_NbaClientDetails.update}"/>
		</h:panelGroup>	

		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
	
</f:view>
</body>
</html>
