<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- SPR2880 		   7	  Reinsurance Tab - UI Issues with View Additional Information View -->
<!-- NBA208-36         7      Deferred Work Item retrieval -->
<!-- SPR2877           8      Reinsurance Tab - Unable to update draft additional information - Addl Info button is disabled -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaViewAdditionalRequest.faces';	//NBA212, NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_viewAdditionalInfo'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_viewAdditionalInfo'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_viewAdditionalInfo']['form_viewAdditionalInfo:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_ALL_SOURCES" value="#{pc_allSourcesWI}" /> <!-- NBA208-36 -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE_ADDNL_INFO" value="#{pc_nbaReinsuranceSendAddInfo}" /><!-- SPR2877 -->
		<h:form id="form_viewAdditionalInfo">
			<h:panelGroup id="viewInfoPG1" styleClass="sectionHeader">
				<h:outputText id="viewInfoPG1OT1" value="#{property.viewAddInfo}" styleClass="shTextLarge" style="position: absolute; left: 10px"/>	
			</h:panelGroup>
		
		<h:panelGroup id="viewAddInfoPG2" styleClass="ovDivTableData" style="height: 150px">
			<h:dataTable id="viewAddInfoTable" styleClass="ovTableData"
				cellspacing="0" rows="0"
				binding="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestResponseTable}"
				value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestResponseList}"
				var="reinsSendAddInfo" rowClasses="#{pc_nbaReinsuranceSendAddInfo.requestResponseRowStyles}"
				columnClasses="ovColText200,ovColText85,ovColText85,ovColDate,ovColIcon">
				<h:column>
				<h:commandButton id="reinCol1a" image="images/hierarchies/L.gif" rendered="#{reinsSendAddInfo.levelOne}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:commandButton id="reinCol1b" image="images/hierarchies/T.gif" rendered="#{reinsSendAddInfo.levelOneWithNext}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:outputText id="reinCol1c" value="#{reinsSendAddInfo.requests}"	style="overflow: auto; height: auto;vertical-align: top;"/>
				</h:column>
				<h:column>
					<h:outputText styleClass="ovFullCellSelect" id="reinCol2" value="#{reinsSendAddInfo.reinsured}"/>
				</h:column>
				<h:column>
					<h:outputText styleClass="ovFullCellSelect" id="reinCol3" value="#{reinsSendAddInfo.retained}"/>
				</h:column>
				<h:column>
					<h:outputText styleClass="ovFullCellSelect" id="reinCol4" value="#{reinsSendAddInfo.date}"/>
				</h:column>
				<h:column>
					<h:commandButton id="reinCol6a"
						image="images/link_icons/documents.gif"
						styleClass="ovViewIconTrue"
						onclick="setTargetFrame()" action="#{reinsSendAddInfo.showSelectedImage}"
						immediate="true" /> <!-- NBA212 -->
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		
		<!-- Begin SPR2880 -->
		<h:panelGroup id="addlInfoLevelPG" styleClass="formDataEntryLine" > 
			<h:outputText id="addlInfoLevelPGOT1" value="#{property.requestLevel}" styleClass="formLabel" />
			<h:outputText id="addlInfoLevelPGOT2" value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestLevel}"
					styleClass="formDisplayText"/> 
		</h:panelGroup>
		<h:panelGroup id="addlInfoLevelPG2" styleClass="formDataEntryLine" >
			<h:outputText id="addlInfoLevelPG2OT1" value="#{property.reinMessage}" styleClass="formLabel" style="vertical-align: top;" />
			<h:outputText id="addlInfoLevelPG2OT2" value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.message}"
					styleClass="formDisplayText" style="width: 499px; text-align: left;" />
		<!-- End SPR2880 -->
		</h:panelGroup>

		<h:panelGroup id="addlInfoImagePG" styleClass="formDataEntryLine" ><!-- SPR2880 -->
			<!-- SPR2880 Code Deleted-->
			<h:panelGroup style="width: 125px;vertical-align: top;" ><!-- SPR2880 -->
				<h:outputText id="addlInfoImagePGOT1" value="#{property.images}" styleClass="formLabel"/>
				<h:outputText id="addlInfoImagePGOT2" value=""	 style="position: relative; left: 30px;" />
				<h:outputText id="addlInfoImagePGOT3" value=""	 style="position: relative; left: 30px;"/>
			<!-- Begin SPR2880 -->
			</h:panelGroup>
			<h:panelGroup id="addlInfoImagePG2" styleClass="ovDivTableData"  style="width:499px; height: 100px;">
				<h:dataTable id="reinsAddlInfoImageTable" styleClass="ovTableData"
						cellspacing="0" 
						binding="#{pc_nbaReinsuranceSendAddInfo.selectedImageTable}"
						value="#{pc_nbaReinsNav.selectedImageList}"
						var="imageRow"
						columnClasses="ovColText250,ovColIcon"  style="width:497px;">
			<!-- End SPR2880 -->
						<h:column>
							<h:commandLink id="addlInfoImagePG2CL1" value="#{imageRow.sourceType}" immediate="true" />
						</h:column>
						<!-- SPR2880 Code Deleted-->
						<!-- SPR2880 Code Deleted-->
						<h:column>
							<h:commandButton id="reinsImageTableCol3a"
								image="images/link_icons/documents.gif"
								onclick="setTargetFrame()" action="#{imageRow.showSelectedImage}"
								styleClass="ovViewIconTrue" immediate="true" /> <!-- NBA212 -->
						</h:column>
				</h:dataTable>
			</h:panelGroup>
		<!-- SPR2880 Code Deleted-->
		</h:panelGroup>
		<!-- Begin SPR2880 -->
		<h:panelGroup id="viewAddlInfoMessage" styleClass="formDataEntryLine" >
			<h:outputText id="viewAddlInfoMessageOT1" value="#{property.additionalMessage}" styleClass="formLabel"	style="vertical-align: top;" />
			<h:outputText id="viewAddlInfoMessageOT2"  value="#{pc_nbaReinsNav.additionalInfoMessage}" styleClass="formLabel" style="width: 504px; text-align: left;" ></h:outputText>
		<!-- End SPR2880-->
		</h:panelGroup>

		<h:panelGroup id="viewAddlInfoButtonBar" styleClass="formButtonBar">
			<h:commandButton id="reinsViewClose" value="#{property.buttonClose}"
				action="#{pc_nbaReinsuranceSendAddInfo.cancel}" onclick="resetTargetFrame();"
				styleClass="formButtonLeft"/><!-- SPR2880 -->
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
