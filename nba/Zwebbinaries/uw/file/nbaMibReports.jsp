<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- SPR2965           6      Underwriter Workbench Scrollbars --%>
<%-- NBA171            6      nbA Linux re-certification project --%>
<%-- SPR3090           6      Table paging support should be generalized for use by any table panes --%>
<%-- NBA158 		   6	  Websphere 6.0 upgrade --%>
<%-- NBA213 		   7	  Unified User Interface --%>
<%-- NBA226            8      nba MIB Translation and validation --%>
<%-- FNB011 				NB-1101	Work Tracking --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- NBA390     	NB-1601   nbA MIB Enhancements --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Create New Requirements</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaMibReports.faces';  //NBA213
		var mibURL;//NBA226
		
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_updateMIB'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_updateMIB'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_updateMIB']['form_updateMIB:draftChanges'].value;  //NBA213
		}
					
		//NBA226 New function
		function launchMIBWindow() {
			var link = document.forms['form_updateMIB']['form_updateMIB:mibURL'].value;
			mibURL = window.open(link, 'mibWindow');
			mibURL.focus();
			top.mainContentFrame.contextMenu.mibURL = mibURL;
			return false;
		}
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_MIBREPORTS" value="#{pc_reqMIBReports}" /><!-- NBA226 -->
		<h:form id="form_updateMIB">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
			</f:subview>

			<f:subview id="impOverview">
				<c:import url="/uw/subviews/NbaImpairmentsTable.jsp" />
			</f:subview>

			<div id="mibUpdate" class="inputFormMat">
				<div class="inputForm">
					<h:outputLabel id="cnTitle" value="#{property.mibUpdateTitle}" styleClass="formTitleBar" />
					<f:subview id="mibReports">
						<c:import url="/uw/subviews/NbaMIBReports.jsp" />
					</f:subview>

					<!-- NBA226 code deleted-->

					<f:subview id="mibCodes">
						<c:import url="/uw/subviews/NbaMIBCodes.jsp" />
					</f:subview>

					<h:outputLabel value="#{property.mibOptData}" styleClass="formSectionBar" />
					<h:panelGroup styleClass="formDataEntryTopLine">
						<h:outputLabel value="#{property.mibDelay}" styleClass="formLabel"/>
						<h:inputText value="#{pc_reqMIBReports.delay}" disabled="#{pc_reqMIBReports.disabled}" 
									styleClass="formEntryText" style="width: 300px">
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
					</h:panelGroup>

					<f:subview id="mibReportsData">
						<c:import url="/uw/subviews/NbaMIBReportsData.jsp" />
					</f:subview>

					<hr class="formSeparator" />

					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft"
								onclick="resetTargetFrame();" action="#{pc_reqMIBReports.cancel}" immediate="true" />
						<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1"
								onclick="resetTargetFrame();" action="#{pc_reqMIBReports.clear}" immediate="true" />
						 <!-- begin NBA226 -->		
						<h:commandButton value="#{property.buttonCommit}" disabled="#{pc_reqMIBReports.mibCodeDisabled}" styleClass="formButtonRight"
								onclick="resetTargetFrame();" action="#{pc_reqMIBReports.commit}" /> <%-- NBA390 --%>
						<!-- end NBA226 -->		
					</h:panelGroup>
					<f:subview id="nbaCommentBar">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
					</f:subview>
				</div>
			</div>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
			<h:inputHidden id="mibURL" value="#{pc_reqMIBCodes.mibURL}" /><!-- NBA226 -->
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	