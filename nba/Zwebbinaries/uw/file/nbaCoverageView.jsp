<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223	  NB-1101	  Underwriter Final Disposition  --> 
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401  Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {			  
			document.forms['form_CoverageView'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {			 
			document.forms['form_CoverageView'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_CoverageView']['form_CoverageView:draftChanges'].value;  //NBA213
		}			
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body class="whiteBody"  onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: auto"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_COVERAGE_VIEW_UPDATE" value="#{pc_NbaCoverageViewUpdate}" />	
	<h:form id="form_CoverageView">
		<!-- ********************************************************
			coverage Table (optional)
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		<!-- ********************************************************
			View/Update Coverage
		 ******************************************************** -->
		<f:subview id="coverageView">
			<c:import url="/uw/subviews/NbaCoverage.jsp" />
		</f:subview>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<div id="Messages" style="display: none"><h:messages /></div>
	</h:form>
</f:view>
</body>
</html>
