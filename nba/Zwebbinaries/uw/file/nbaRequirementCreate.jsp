<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA130            6      Requirements Reinsurance Project -->
<!-- NBA154            6      Requirements Business Function Rewrtie -->
<!-- NBA138            6      Override Requirements Settings Project -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA192 		   7	  Requirement Management Enhancement -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA208-36 		   7	  Deferred Work item retrieval -->
<!-- SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implementation -->
<!-- SPR3684           8      Doctor name is not saved -->
<!-- NBA224            8      nbA Underwriter Workbench Requirements and Impairments Enhancement Project -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-478		NB-1101	Database Errors Occur when Comments on Requirements Exceed 100 Characters -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>  <!-- NBA213 -->
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Create New Requirements</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="include/common.js"></script> <!-- SPRNBA-478  --> 
	<!-- NBA213 code deleted -->
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'uw/file/nbaRequirementCreate.faces';
		//NBA213 deleted
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_createRequirement'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_createRequirement'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_createRequirement']['form_createRequirement:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_REQUIREMENT" value="#{pc_reqCreate}" /> <!-- NBA208-36 -->
		<h:form id="form_createRequirement">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
			</f:subview>
			
			<f:subview id="reqOverview">
				<c:import url="/uw/subviews/NbaRequirementsTableReadOnly.jsp" /> <!-- SPR3399 -->
			</f:subview>

			<div id="createNew" class="inputFormMat">
				<div class="inputForm">
					<h:outputLabel id="cnTitle" value="#{property.reqCreateTitle}" styleClass="formTitleBar" />
					<h:panelGrid columns="2" styleClass="formDataEntryTopLine" cellspacing="0" cellpadding="0">
						<h:column>
							<h:selectOneRadio value="#{pc_reqCreate.status}" layout="lineDirection" styleClass="formEntryText" style="position: relative; left: 116px; width: 190px">
								<f:selectItems value="#{pc_reqCreate.createStatuses}" />
							</h:selectOneRadio>
						</h:column>
						<h:column>
							<h:panelGroup style="position: relative; left: 100px">
								<h:outputLabel value="#{property.reqCreated}" styleClass="formLabel" style="width: 86px"/> <!-- NBA130 -->
								<h:outputText value="#{pc_reqCreate.createdDate}" styleClass="formDisplayDate"> <!-- NBA130 -->
									<f:convertDateTime pattern="#{property.datePattern}"/>
								</h:outputText> <!-- NBA130 -->
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqType}" styleClass="formLabel"/>
						<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_reqCreate.requirementType}"
									valueChangeListener="#{pc_reqCreate.selectType}" onchange="submit()" immediate="true">
							<f:selectItems value="#{pc_reqCreate.requirementTypes}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqOriginator}" styleClass="formLabel"/>
						<h:inputText styleClass="formEntryTextFull" value="#{pc_reqCreate.originator}" />
					</h:panelGroup>
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqRestrictions}" styleClass="formLabel"/>
						<h:selectOneMenu id="restrictions" value="#{pc_reqCreate.restrictions}" binding="#{pc_reqCreate.restrictionCode}"
										disabled="#{pc_reqCreate.overrideDisabled}" styleClass="formEntryTextFull">
							<f:selectItems value="#{pc_reqCreate.requirementRestrictions}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<!-- begin NBA138 -->
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqVendor}" styleClass="formLabel"/>
						<h:selectOneMenu id="vendor" value="#{pc_reqCreate.vendor}" binding="#{pc_reqCreate.vendorCode}" styleClass="formEntryText">
							<f:selectItems value="#{pc_reqCreate.vendorNames}" />
						</h:selectOneMenu>
						<h:outputLabel value="#{property.reqFollowUpFreq}" styleClass="formLabel" style="width: 170px"/>
						<h:inputText id="followUpFrequency" value="#{pc_reqCreate.followUpFreq}" binding="#{pc_reqCreate.freqHtmlComponent}" 
							maxlength="3" styleClass="formEntryText" style="width: 30px">
							<f:convertNumber integerOnly="true" type="integer" />
						</h:inputText>
					</h:panelGroup>
					<!-- end NBA138 -->
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqMedical}" styleClass="formLabel"/>
						<h:selectBooleanCheckbox value="#{pc_reqCreate.medical}" disabled="true" readonly="true" styleClass="formEntryCheckbox"/>
						<h:outputLabel value="#{property.reqReorder}" styleClass="formLabel"/> <!-- NBA192 -->
						<h:selectBooleanCheckbox value="#{pc_reqCreate.reorder}" styleClass="formEntryCheckbox"/> <!-- NBA192 -->
					</h:panelGroup>

					<hr class="formSeparator" />

					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqDoctor}" styleClass="formLabel"/>	
						<!-- begin NBA224 -->				
						<h:selectOneMenu id="doctorDD" value="#{pc_reqCreate.drPartyId}" style="width: 450px;" onchange="submit()" 
							disabled="#{pc_reqCreate.disableDoctorDD}" styleClass="entryFieldLong" >
							<f:selectItems value="#{pc_reqimpNav.doctorList}"/>
						</h:selectOneMenu>
						<!-- end NBA224 -->
						<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: bottom" onclick="setTargetFrame();" action="#{pc_reqCreate.doctor}" />
					</h:panelGroup>
					<!-- begin NBA154 -->
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="Form Number" styleClass="formLabel"/>
						<h:selectOneMenu id="formNumber" value="#{pc_reqCreate.formNumber}" binding="#{pc_reqCreate.formNumberCode}"
										 styleClass="formEntryTextFull">
							<f:selectItems value="#{pc_reqCreate.requirementFormNumbers}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<!-- end NBA154 -->
					<h:panelGroup styleClass="formDataEntryLine">
						<h:outputLabel value="#{property.reqMessage}" styleClass="formLabel" style="vertical-align: top" />
						<h:inputTextarea id="ReqComment" value="#{pc_reqCreate.message}" rows="5" styleClass="formEntryTextMultiLineFull"  
							onchange="limitText(this,100);" 
							onkeyup="limitText(this,100);" 							 
						/>	 	<!-- SPRNBA-478 --> 
					</h:panelGroup>

					<hr class="formSeparator" />

					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" action="#{pc_reqCreate.cancel}" immediate="true" />
						<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();" action="#{pc_reqCreate.clear}" accesskey="L" />
						<h:commandButton value="#{property.buttonAddNew}" styleClass="formButtonRight-1" onclick="resetTargetFrame();" action="#{pc_reqCreate.addNew}" rendered="#{pc_reqCreate.addMode}" />
						<h:commandButton value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="resetTargetFrame();" action="#{pc_reqCreate.add}" rendered="#{pc_reqCreate.addMode}" />
						<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight" onclick="resetTargetFrame();" action="#{pc_reqCreate.update}" rendered="#{!pc_reqCreate.addMode}" />
					</h:panelGroup>
					<f:subview id="nbaCommentBar">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
					</f:subview>
				</div>
			</div>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" rendered="#{pc_reqCreate.UWBench}"/> <!-- NBA154 -->
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
