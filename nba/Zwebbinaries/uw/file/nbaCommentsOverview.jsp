<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2836           6      General Code Clean Up for JSF Views Added in Version 5 -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- SPR3073 		   6	  The add comments dialog under the upgraded (JSF) look & feel should not cover nbA -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- NBA225            8      nbA Comments -->
<!-- FNB011 	 NB-1101  	  Work Tracking -->
<!-- FNB004 	 NB-1101 	  PHI -->
<!-- FNB004 - VEPL1274 	 NB-1101 	  Commit button not available -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->
<!-- SPRNBA-658  NB-1301      Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench-->
<!-- NBA323 	 NB-1301 	  nbA Comments Improvements -->
<!-- SPRNBA-583	 NB-1301 	  General Code Clean Up -->
<!-- SPRNBA-798  NB-1401      Change JSTL Specification Level -->
<!-- NBA353		 NB-1501	  Save Draft Comments when navigating from case to case -->
<!-- NBA356      NB-1501      Comments Floating View -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Comments Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		//NBA213 deleted
		function setTargetFrame() {
			//alert('calling set targeet frame');
			document.forms['form_commentsOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_commentsOverview'].target='';
			return false;
		}
		//FNB004 New Method
		function initPage(){
			filePageInit();
			if (document.getElementById('form_commentsOverview:commentsTable:commentData') != null) {
				<!-- FNB004 - VEPL1274 Code deleted-->
				if(top.mainContentFrame == this ){ //NBA356
					tabHeight = window.screen.availHeight - 270; //NBA356
				} else { //NBA356
					tabHeight = window.screen.availHeight - 525;			<!-- FNB004 - VEPL1274 -->	
				}
				document.getElementById('form_commentsOverview:commentsTable:commentData').style.height = tabHeight; <!-- FNB004 - VEPL1274 -->
			}
			checkCommentViewParent(); //NBA356
			draftCommentsPresent();	//SPRNBA-658
			
		}
		//NBA324 new method
		function launchPHIBriefcaseView() {			        
			phipopup = launchPopup('phipopup', '<%=path%>/common/popup/popupFramesetPHI.html?popup=<%=basePath%>/nbaPHI/file/PHIStatus.faces?readonly=true', 650, 690);
            phipopup.focus();
            top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup = phipopup;		//FNB004 - VEPL1224		
            return false;
		}
		
		//SPRNBA-658 New Method
		function draftCommentsPresent() {
			top.opener.top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = document.forms['form_commentsOverview']['form_commentsOverview:draftComments'].value; //NBA356
			top.opener.top.mainContentFrame.contentRightFrame.nbaContextMenu.draftCommentsOnOtherContract = document.forms['form_commentsOverview']['form_commentsOverview:draftCommentsOnOtherContract'].value;	//NBA353
		} 
		
		//NBA356 New Method
		function checkCommentViewParent() {
			uwNavigationPanel = document.getElementById('form_commentsOverview:tabHeader:uwNavigationPanel');
			btnRefer = document.getElementById('form_commentsOverview:btnRefer');
			btnReviewCmpt = document.getElementById('form_commentsOverview:btnReviewCmpt');
        	if(top.mainContentFrame == this) {
        		if(uwNavigationPanel != null) {
        			uwNavigationPanel.style.display = "none";
        		} 
        		if(btnRefer != null) {
        			btnRefer.style.display = "none";
        		}
        		if(btnReviewCmpt != null) {
        			btnReviewCmpt.style.display = "none";
        		}
        	}
        }
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden;"> <!-- SPR2965--><!-- NBA323 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />  <!-- NBA213 -->
		<PopulateBean:Load serviceName="RETRIEVE_COMMENTS" value="#{pc_commentsNavigation}" />  <!-- NBA213 -->
		<h:form id="form_commentsOverview">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaCommentNavigation.jsp" />
			</f:subview>
			<!-- begin NBA213 -->
			<h:panelGroup id="checkBoxes" styleClass="sectionSubheader" style="padding-top: 0px"> <!-- NBA225 -->
				<h:selectBooleanCheckbox id="commCB1" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.secure}" rendered="#{pc_commentsNavigation.UWBench}" />
				<h:graphicImage value="images/comments/lock-closed-forsectionhead.gif" styleClass="commentIcon" rendered="#{pc_commentsNavigation.UWBench}" />
				<h:outputText value="#{property.secureLabel}" styleClass="shText" rendered="#{pc_commentsNavigation.UWBench}"/> <!-- SPRNBA-583	 -->
				<!-- begin NBA323-->
				<h:outputText value="(" styleClass="shText" style="margin-left: 2px; font-size: 16px; "/>
				<h:selectBooleanCheckbox id="commCB7" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.excludeAutoSecure}"  />
				<h:outputText value="#{property.excludeAutomaticLabel}" styleClass="shText" />
				<h:outputText value=")" styleClass="shText" style="font-size: 16px; "/>
				<h:selectBooleanCheckbox id="commCB2" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.general}" style="margin-left: 8px;"/>
				<!-- end NBA323-->
				<h:graphicImage value="images/comments/lock-open-forsectionhead.gif" styleClass="commentIcon"  />
				<h:outputText value="#{property.generalLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
			
				<h:selectBooleanCheckbox id="commCB3" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.special}" style="margin-left: 8px; width: 20px" /> <!-- NBA323-->
				<h:graphicImage value="images/comments/exclamation-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.specialInstructionLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
			
				<!-- begin FNB004 -->
				<h:selectBooleanCheckbox id="commCB3a" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.PHI}" rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" style="margin-left: 8px; width: 20px;" /> <!-- NBA323 --> 
				<h:graphicImage id="phiImage" value="images/comments/PHI-forsectionhead.gif"  rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" styleClass="commentIcon" /> 
				<h:outputText id="phiCheckBox" value="#{property.phiLabel}" rendered ="#{pc_commentsNavigation.auth.visibility['PHICommentShowSelect']}" styleClass="shText" style="margin-left: 2px;"/> <!-- NBA323 --><!-- SPRNBA-583	 -->
				<!-- end FNB004 -->
				
				<h:selectBooleanCheckbox id="commCB4" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.automated}" style="margin-left: 8px; width: 20px;" /> <!-- NBA323-->
				<h:graphicImage value="images/comments/zigzag-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.automatedLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="ccommCB5" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.phone}" style="margin-left: 10px; width: 20px;" /> <!-- NBA323-->
				<h:graphicImage value="images/comments/phone-forsectionhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.phoneLabel}" styleClass="shText" /><!-- SPRNBA-583	 -->
		
				<h:selectBooleanCheckbox id="commCB6" styleClass="formEntryCheckbox" value="#{pc_commentsNavigation.email}" style="margin-left: 10px; width: 20px;" /> <!-- NBA323-->
				<h:graphicImage value="images/comments/envelope-forsectionsubhead.gif" styleClass="commentIcon" />
				<h:outputText value="#{property.emailLabel}" styleClass="shText" />	<!-- SPRNBA-583	 -->
			</h:panelGroup>
			<!-- NBA225 -->
			<h:panelGroup id="voidOption" styleClass="sectionSubheader" style="padding-top: 0px">
				<h:outputText id="voidedLabel" value="#{property.voidedLabel}" styleClass="formLabel" style="width: 90px; margin-left: -5px;"/>
				<h:selectOneMenu id="voidedList" styleClass="shMenu" style="width: 300px" value="#{pc_commentsNavigation.voidedOption}"
							 	 disabled="#{pc_commentsNavigation.voidOptionDisabled}">
					<f:selectItems id="voidedListValue" value="#{pc_commentsNavigation.voidedList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="clientRelation" styleClass="sectionSubheader" style="padding-top: 0px"> <!-- NBA225 -->
				<h:outputText value="#{property.commentRelates}" styleClass="formLabel" style="width: 90px; margin-left: -5px;" rendered="#{pc_commentsNavigation.UWBench}"/>		
				<h:selectOneMenu value="#{pc_commentsNavigation.selectedInsured}" styleClass="shMenu" style="width: 300px" rendered="#{pc_commentsNavigation.UWBench}">
					<f:selectItems value="#{pc_commentsNavigation.insuredClients}"/>
				</h:selectOneMenu>
				<h:commandButton value="Go" styleClass="formButtonRight" style="margin-top: 0px" action="#{pc_commentsTable.retrieveSelectedRecords}"/>
			</h:panelGroup>
			<!-- NBA213 -->
			<f:subview id="commentsTable">
				<c:import url="/uw/subviews/NbaCommentsTable.jsp" />
			</f:subview>
			<f:subview id="nbaCommentBar">
				<c:import url="/nbaComments/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
			</f:subview>

			<h:panelGroup id="commandButtons" styleClass="tabButtonBar">  <!-- SPR2836 --> <!-- NBA225 -->
				<h:commandButton id="btnCommRefresh" value="#{property.buttonRefresh}" styleClass="ovButtonLeft" action="#{pc_commentsNavigation.refresh}" immediate="true" onclick="setTargetFrame();"/> <!-- SPRNBA-658 -->
				<h:commandButton id="btnCommDelete" action="#{pc_commentsNavigation.deleteComment}" onclick="setTargetFrame();" 
					value="#{property.buttonDelete}" disabled="#{pc_commentsNavigation.allComments.deleteDisabled}" styleClass="ovButtonLeft-1" immediate="true"/> <!-- NBA225, NBA356 -->
				<h:commandButton id="btnCommVoid" action="#{pc_commentsNavigation.voidComment}" onclick="setTargetFrame();" 
					value="#{property.buttonVoid}" rendered="#{pc_commentsNavigation.allComments.voidRendered}" disabled="#{pc_commentsNavigation.allComments.voidDisabled}"
					styleClass="ovButtonLeft-1" immediate="true"/> <!-- NBA356 -->
				<h:commandButton id="btnRefer" value="#{property.buttonRefer}" action="#{pc_uwbean.refer}" immediate="true" 
					disabled="#{pc_uwbean.undCommitDisabledForFunctionalUpdates || pc_commentsNavigation.notLocked || pc_uwbean.informalApplication}" styleClass="ovButtonLeft-1" style="left: 190px"  
					onclick="setTargetFrame();"/> <!-- NBA186 -->
				<h:commandButton id="btnReviewCmpt" value="#{property.buttonReviewCmpt}" action="#{pc_uwbean.reviewComplete}" 
				 	rendered="#{pc_uwbean.renderReviewCompleteForAddnlApprovers}" disabled="#{pc_commentsNavigation.notLocked}" immediate="true" styleClass="ovButtonRight-2"  onclick="setTargetFrame();" 
				 	style="width: 110px; left: 320px"/> <!-- NBA186 -->
				<h:commandButton id="btnCommCommit" action="#{pc_commentsNavigation.allComments.commitComment}" value="#{property.buttonCommit}"
					disabled="#{pc_commentsNavigation.auth.enablement['Commit'] || pc_commentsNavigation.notLocked}"
					styleClass="ovButtonRight-1" />	<!-- NBA213, NBA186 -->
				<h:commandButton id="btnCommUpdate"  onclick="launchCommentPopUp('Update');" value="#{property.buttonUpdate}" disabled="#{pc_commentsNavigation.allComments.updateDisabled}" styleClass="ovButtonRight" /> <!--SPR3073 -->
			</h:panelGroup>
			<h:inputHidden id="draftComments" value="#{pc_uwbean.draftCommentsPresent}" />  <!-- SPRNBA-658 -->
			<h:inputHidden id="draftCommentsOnOtherContract" value="#{pc_uwbean.draftCommentsOnOtherContract}" />  <!-- NBA353 -->
		</h:form>
		<!-- SPR2836 deleted code -->
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
		<div id="contentArea" style="display:none;width: 675px;height: 872px" /> <!-- NBA356 -->
	</f:view>
</body>
</html>
	