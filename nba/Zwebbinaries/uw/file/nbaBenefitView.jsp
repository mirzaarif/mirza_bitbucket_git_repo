<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223		NB-1101	  Underwriter Final Disposition  --> 
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401  Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			  
			document.forms['form_BenefitView'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			 
			document.forms['form_BenefitView'].target='';
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: auto"> 
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_BENEFIT_VIEW_UPDATE" value="#{pc_NbaBenefitViewUpdate}" />
	<h:form id="form_BenefitView">
		<!-- ********************************************************
			coverage Table
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		<!-- ********************************************************
			View/Update Benefit
		 ******************************************************** -->
		<div id="viewUpdate" class="inputFormMat" style="background-color: #FFFFFF">
			<div class="inputForm" style="height : 225px; margin-left: 0px">
		<!-- Table Header -->
		<h:outputLabel id="viewBenefitTitle" value="#{property.uwCovViewBenefitTitle}" styleClass="formTitleBar" />
		<!-- Display Fields -->
			<h:panelGroup id="viewUpdateBen1" styleClass="formDataDisplayLine">
				<h:panelGroup id="viewUpdateBen2" style="width: 380px;  overflow: hidden;">
					<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
						<h:column id="viewUpdateCol1">
							<h:outputLabel value="#{property.uwCovOptionType}" styleClass="formLabel" />
						</h:column>
						<h:column id="viewUpdateCol2">
							<h:outputText value="#{pc_NbaBenefitViewUpdate.typeText}" styleClass="formDisplayText"/>
						</h:column>
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup id="viewUpdateBen3" style="position: absolute; left: 330px">
					<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
						<h:column id="viewUpdateCol3">
							<h:outputLabel value="#{property.uwEffective}" styleClass="formLabel" />
						</h:column>
						<h:column id="viewUpdateCol4">
							<h:outputText value="#{pc_NbaBenefitViewUpdate.effectiveDate}" styleClass="formEntryDate">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:column>
					</h:panelGrid>
				</h:panelGroup>
			</h:panelGroup>
			<h:panelGroup id="viewUpdateBen4" styleClass="formDataDisplayLine">
				<h:panelGroup style="width: 380px;  overflow: hidden;">
					<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
						<h:column id="viewUpdateCol5">
							<h:outputLabel value="#{property.uwStatus}" styleClass="formLabel" />
						</h:column>
						<h:column id="viewUpdateCol6">
							<h:outputText value="#{pc_NbaBenefitViewUpdate.statusText}" styleClass="formDisplayText"/>
						</h:column>
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup id="viewUpdateBen5" style="position: absolute; left: 330px">
					<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
						<h:column id="viewUpdateCol7">
							<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
						</h:column>
						<h:column id="viewUpdateCol8">
							<h:outputText id="Cease_Date" value="#{pc_NbaBenefitViewUpdate.ceaseDate}" styleClass="formEntryDate">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:column>
					</h:panelGrid>
				</h:panelGroup>
			</h:panelGroup>

			<h:panelGroup id="viewUpdateBen6" styleClass="formDataDisplayLine" rendered="#{pc_NbaBenefitViewUpdate.proposedUWStatusPresent}">
				<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
					<h:column id="viewUpdateCol9">
						<h:outputLabel value="#{property.uwProposeStatus}" styleClass="formLabel" />
					</h:column>
					<h:column id="viewUpdateCol10">
						<h:outputText value="#{pc_NbaBenefitViewUpdate.proposedUnderwritingStatusText}" styleClass="formDisplayText"/>
					</h:column>
				</h:panelGrid>
			</h:panelGroup>

			<f:verbatim>
				<hr class="formSeparator" />
			</f:verbatim>

			<h:panelGroup id="viewUpdateBen7" styleClass="formDataDisplayLine">
				<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
					<h:column id="viewUpdateCol11">
						<h:outputLabel value="#{property.uwAmount}" styleClass="formLabel" />
					</h:column>
					<h:column id="viewUpdateCol12">
						<h:outputText styleClass="formDisplayText" value="#{pc_NbaBenefitViewUpdate.amountText}"/>
					</h:column>
				</h:panelGrid>
			</h:panelGroup>
			<h:panelGroup id="viewUpdateBen8" styleClass="formDataDisplayLine">
				<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
					<h:column id="viewUpdateCol13">
						<h:outputLabel value="#{property.uwPremium}" styleClass="formLabel"/>
					</h:column>
					<h:column id="viewUpdateCol14">
						<h:outputText value="#{pc_NbaBenefitViewUpdate.premium}" styleClass="formDisplayText"/>
					</h:column>
				</h:panelGrid>
			</h:panelGroup>
			<!-- Button bar -->
			<h:panelGroup id="viewUpdateBen9" styleClass="formButtonBar">
				<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true" action="#{pc_finalDispNavigation.actionCloseSubView}" />	
			</h:panelGroup>	
			</div>
		</div>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
