<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA208-32		   7	  Workflow VO Convergence -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->
<!-- SPRNBA-658  NB-1301      Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench-->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA353		    NB-1501	  Save Draft Comments when navigating from case to case -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath =
		request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath =
		request.getScheme()
			+ "://"
			+ request.getServerName()
			+ ":"
			+ request.getServerPort()
			+ path
			+ "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Prior Insurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbaimages.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		//NBA213 deleted
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_priorinsOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_priorinsOverview'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_priorinsOverview']['form_priorinsOverview:draftChanges'].value;  //NBA213
		}
        //NBA324 new method
		function launchPHIBriefcaseView() {			        
			phipopup = launchPopup('phipopup', '<%=path%>/common/popup/popupFramesetPHI.html?popup=<%=basePath%>/nbaPHI/file/PHIStatus.faces?readonly=true', 650, 690);
            phipopup.focus();
            top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup = phipopup;		//FNB004 - VEPL1224		
            return false;
		}
		
		//SPRNBA-658 new method
		function draftCommentsPresent() {
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = document.forms['form_priorinsOverview']['form_priorinsOverview:draftComments'].value;		
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftCommentsOnOtherContract = document.forms['form_priorinsOverview']['form_priorinsOverview:draftCommentsOnOtherContract'].value;	//NBA353
		}
		
		//SPRNBA-658 new method
		function initPage(){
			filePageInit();
			setDraftChanges();
			draftCommentsPresent();
		}
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>

<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 SPRNBA-658-->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />  <!-- NBA213 -->
	<!-- NBA208-32 code deleted -->
	<PopulateBean:Load serviceName="RETRIEVE_PRIORINS" value="#{pc_priorInsNav}" />
	<h:form id="form_priorinsOverview">

		<f:subview id="navigation">
			<c:import url="/uw/subviews/NbaPriorInsuranceNavigation.jsp" />
		</f:subview>

		<f:subview id="priorInsOverviewTbl"
			rendered="#{pc_priorInsTable.clientList1Rendered}">
			<c:import url="/uw/subviews/NbaPriorInsuranceTable1.jsp" />
		</f:subview>

		<f:subview id="priorInsOverviewTb2"
			rendered="#{pc_priorInsTable.clientList2Rendered}">
			<c:import url="/uw/subviews/NbaPriorInsuranceTable2.jsp" />
		</f:subview>		

		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>

		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}"
				action="#{pc_priorInsNav.refresh}"
				styleClass="ovButtonLeft" />
			<h:commandButton id="btnRefer" value="#{property.buttonRefer}" action="#{pc_uwbean.refer}" 
					disabled="#{pc_uwbean.undCommitDisabledForFunctionalUpdates || pc_reqimpNav.notLocked || pc_uwbean.informalApplication}" immediate="true" 
					styleClass="formButtonLeft-1"  onclick="setTargetFrame();"/> <!-- NBA186 -->
		 	<h:commandButton id="btnReviewCmpt" value="#{property.buttonReviewCmpt}" action="#{pc_uwbean.reviewComplete}"  
		 		immediate="true" styleClass="ovButtonRight"  onclick="setTargetFrame();" rendered="#{pc_uwbean.renderReviewCompleteForAddnlApprovers}" 
		 		disabled="#{pc_reqimpNav.notLocked}" style="width: 110px; left: 500px"/> <!-- NBA186 -->
		</h:panelGroup>

		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
		<h:inputHidden id="draftComments" value="#{pc_uwbean.draftCommentsPresent}" />  <!-- SPRNBA-658 -->
		<h:inputHidden id="draftCommentsOnOtherContract" value="#{pc_uwbean.draftCommentsOnOtherContract}" />  <!-- NBA353 -->
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>

</f:view>

</body>

</html>
