<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2887           6      Reinsurance Tab - UI Issues with View Reinsurance Request View -->
<!-- SPR2836           6      General Code Clean Up for JSF Views Added in Version 5 -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA208-36         7      Deferred Work Item retrieval -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
					
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaViewReinsuranceRequest.faces';	 //NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_reinsuranceViewRequest'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_reinsuranceViewRequest'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_reinsuranceViewRequest']['form_reinsuranceViewRequest:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_ALL_SOURCES" value="#{pc_allSourcesWI}" /> <!-- NBA208-36 -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE" value="#{pc_nbaReinCreateRequest}" />
	<h:form id="form_reinsuranceViewRequest">
				<h:panelGroup id="viewRequestTitle" styleClass="sectionHeader">
					<h:outputText id="reinsuranceViewRequestTitle2" value="#{property.viewReinsuranceRequest}" styleClass="shTextLarge" style="position: absolute; left: 10px"/><!-- SPR2836-->	
				</h:panelGroup>

				<h:panelGroup id="viewRequestLevelGroup" styleClass="formDataEntryLine" >
					<h:outputText id="viewRequestLevelOT1" value="#{property.reinsLevel}" styleClass="formLabel" /> <!-- SPR2836-->
					<h:selectOneRadio id="reinsRB2" value="#{pc_nbaReinsNav.selectedRequestResponse.newRequest.reinLevel}" 
						disabled="true" 
						layout="lineDirection" styleClass="formEntryText" style="position: relative; left: 116px; top: -20px;">
						<f:selectItem id="Item4" itemValue="Contract" itemLabel="#{property.reinLevelContract}"/><!-- SPR2836-->
						<f:selectItem id="Item5" itemValue="Coverage" itemLabel="#{property.reinLevelCoverage}"/><!-- SPR2836-->
					</h:selectOneRadio>
					<h:outputText id="viewRequestLevelOT2" value="#{property.reinsSentDate}" styleClass="formLabel" style="position: relative; left: 325px; top: -38px;"/><!-- SPR2887--><!-- SPR2836-->
					<h:outputText id="viewRequestLevelOT3" value="#{pc_nbaReinsNav.selectedRequestResponse.date}" styleClass="shText" style="position: relative; left: 325px; top: -38px;"/><!-- SPR2887-->
				</h:panelGroup>
				<h:panelGroup id="viewRequestHeader" styleClass="ovDivTableHeader" >
					<h:panelGrid columns="5" columnClasses="ovColHdrText85,ovColHdrText85,ovColHdrText85,ovColHdrText85,ovColHdrText85"
						cellspacing="0" styleClass="ovTableHeader">
						<h:outputText id="viewRequestHeaderOT1" value="#{property.reinColCoverage}" styleClass="ovColSortedfalse"/><!-- SPR2836-->
						<h:outputText id="viewRequestHeaderOT2" value="#{property.reinColInsured}" styleClass="ovColSortedfalse"/><!-- SPR2836-->
						<h:outputText id="viewRequestHeaderOT3" value="#{property.reinColAmount}" styleClass="ovColSortedfalse"/><!-- SPR2836-->
						<h:outputText id="viewRequestHeaderOT4" value="#{property.reinColRetainedAmount}" styleClass="ovColSortedfalse"/><!-- SPR2836-->
						<h:outputText id="viewRequestHeaderOT5" value="#{property.reinColReinsuredAmount}" styleClass="ovColSortedfalse"/><!-- SPR2836-->
					</h:panelGrid>
				</h:panelGroup>			
				<h:panelGroup id="viewRequestTableData" styleClass="ovDivTableData" style="height: 125px">			
					<h:dataTable id="dataTable2" cellspacing="0" binding="#{pc_nbaReinCreateRequestTable.coverageTable}"
						 value="#{pc_nbaReinsNav.selectedRequestResponse.newRequest.selectedCoverageList}" var="coverage" rowClasses="#{pc_nbaReinCreateRequestTable.requestCoverageRowStyles}"
						columnClasses="ovColText85,ovColText85,ovColText85,ovColText85,ovColText85" styleClass="ovTableData" >
						<h:column>
							<h:outputText id="viewRequestTableOT1" value="#{coverage.plan}"	/>
						</h:column>
						<h:column>
							<h:outputText id="viewRequestTableOT2" value="#{coverage.insured}" />
						</h:column>
						<h:column>
							<h:outputText id="viewRequestTableOT3" value="#{coverage.amount}"/>
						</h:column>
						<h:column>
							<h:outputText id="viewRequestTableOT4" value="#{coverage.retainedAmountForDisplay}" /><!-- SPR2887-->
						</h:column>
						<h:column>
							<h:outputText id="viewRequestTableOT5" value="#{coverage.reinsuredAmount}" />
						</h:column>
					</h:dataTable>
				</h:panelGroup>
				<h:outputText id="CompanyName" value="#{property.reinCompany}" styleClass="formLabel"/><!-- SPR2887--><!-- SPR2836-->
				<h:outputText id="CompanyValue" value="#{pc_nbaReinsNav.selectedRequestResponse.requests}" styleClass="shText"/><!-- SPR2887-->
				<hr class="formSeparator"/>	
				<!--Begin SPR2887 -->
				<h:panelGroup id="ImageTable" styleClass="formDataEntryLine" >
					<h:panelGroup style="width: 125px;vertical-align: top;" >
						<h:outputText id="ImageTableOT1" value="#{property.reinImages}" styleClass="formLabel"/><!-- SPR2887--><!-- SPR2836-->
					</h:panelGroup>
					<h:panelGroup id="ImageTable2" styleClass="ovDivTableData" style="width:497px; height: 100px;"> <!-- SPR2887-->
						<h:dataTable id="viewImages" styleClass="ovTableData" binding="#{pc_nbaReinCreateRequestTable.imageTable}"
							cellspacing="0" value="#{pc_nbaReinsNav.selectedRequestResponse.newRequest.selectedImageList}" var="imageRow" rowClasses="#{pc_nbaReinCreateRequestTable.requestImagesRowStyles}"
							columnClasses="ovColText350,ovColIcon"  style="width:492px;">
						<!--End SPR2887 -->
							<h:column>
								<h:outputText id="ImageTable2OT1" value="#{imageRow.sourceType}" />
							</h:column>
							<h:column>
								<h:commandButton id="reinsImageTableCol3a"
									image="images/link_icons/documents.gif" onclick="setTargetFrame()"
									styleClass="ovViewIconTrue" immediate="true" action="#{imageRow.showSelectedImage}"/> <!-- NBA212 -->
							</h:column>
						</h:dataTable>
					</h:panelGroup>
					<!-- SPR2887 Code deleted-->
				</h:panelGroup>
				<h:panelGroup id="viewRequestMessage" styleClass="formDataEntryLine">		
					<h:outputText id="viewRequestMessageOT1" value="#{property.reinMessage}" styleClass="formLabel" style="position: relative;"/><!-- SPR2836-->
					<h:outputText id="viewRequestMessageOT2" value="#{pc_nbaReinsNav.selectedRequestResponse.newRequest.message}" styleClass="shText" style="position: relative;width: 502px; " />
				</h:panelGroup>
				
				<hr class="formSeparator"/>	
				
				<h:panelGroup id="buttonBar" styleClass="formButtonBar" style="position: relative; ">
					<h:commandButton id="viewRequestClose" value="#{property.buttonClose}" styleClass="formButtonLeft" 
						onclick="resetTargetFrame();" action="#{pc_nbaReinCreateRequest.cancelViewRequest}"/><!-- SPR2836-->
					<h:commandButton id="viewRequestAddlInfo" value="#{property.reinAddlInfo}" styleClass="formButtonRight" 
						onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.sendAdditionalInfo}" /><!-- SPR2836-->
				</h:panelGroup>
				<f:subview id="nbaCommentBar">
					<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
				</f:subview>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>

