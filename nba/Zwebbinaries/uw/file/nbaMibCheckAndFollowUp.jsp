<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implementation -->
<!-- NBA226            8      nba MIB Translation and validation -->
<!-- FNB011 		NB-1101	  Work Tracking -->
<!-- NBA308 		NB-1301	  MIB Follow Ups replaces nbaMibCheck.jsp -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Create New Requirements</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<!-- NBA213 code deleted -->
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/scroll.js"></script>	<!-- NBA308 -->
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaMibCheckAndFollowUp.faces';  //NBA213, NBA308
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_viewMIB'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_viewMIB'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_viewMIB']['form_viewMIB:draftChanges'].value;  //NBA213
		}
		//begin NBA308
		function saveTableScrollPosition() {
			//alert('saveTableScrollPosition');
			saveScrollPosition('form_viewMIB:mibCheckOverview:mibCheckTable', 'form_viewMIB:mibCheckOverview:mibCheckTableVScroll');
			return false;
		}
		function scrollTablePosition() {
			//alert('scrollTablePosition');
			scrollToPosition('form_viewMIB:mibCheckOverview:mibCheckTable', 'form_viewMIB:mibCheckOverview:mibCheckTableVScroll');
			return false;
		}			
		//end NBA308
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 NBA308 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_MIBCHECK_AND_FOLLOW_UPS" value="#{pc_reqMIBCheckAndFollowUpsBean}" /> <!-- NBA226 NBA308 -->
	<h:form id="form_viewMIB">
		<f:subview id="tabHeader">
			<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
		</f:subview>
		<f:subview id="reqOverview">
			<c:import url="/uw/subviews/NbaRequirementsTableReadOnly.jsp" />	<!-- SPR3399 -->
		</f:subview>
		<div id="mibCheck" class="inputFormMat">
			<div class="inputForm">
				<!-- begin NBA308 --> 				
				<h:panelGroup id="titleGroup" styleClass="formTitleBar">
					<h:outputLabel id="cnTitle" value="#{property.mibCheckTitle}" rendered="#{!pc_reqMIBCheckAndFollowUpsBean.followUpsPresent}" />
					<h:selectOneMenu value="#{pc_reqMIBCheckAndFollowUpsBean.entryID}" onchange="submit()" styleClass="shMenu" style="position: relative;top: -1px; width: 45%"
								rendered="#{pc_reqMIBCheckAndFollowUpsBean.followUpsPresent}">
						<f:selectItems value="#{pc_reqMIBCheckAndFollowUpsBean.dropDownList}" />
					</h:selectOneMenu>
				</h:panelGroup> 
				<f:subview id="mibFollowUpOverview" rendered="#{pc_reqMIBCheckAndFollowUpsBean.currentSelectionFollowUp}">
					<c:import url="/uw/subviews/NbaMIBFollowUpOverview.jsp" />
				</f:subview>					
				<f:subview id="mibCheckOverview" rendered="#{!pc_reqMIBCheckAndFollowUpsBean.currentSelectionFollowUp}">
					<c:import url="/uw/subviews/NbaMIBCheckOverview.jsp" />
				</f:subview> 
				<!-- end NBA308 --> 			
				<h:panelGroup styleClass="formButtonBar">
						<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft"
								onclick="resetTargetFrame();" action="#{pc_reqMIBCheckAndFollowUpsBean.cancel}" immediate="true" />
						<h:commandButton value="#{property.buttonCommit}" styleClass="formButtonRight"
								onclick="resetTargetFrame();" action="#{pc_reqMIBCheckAndFollowUpsBean.commit}" /><!-- NBA226 -->
				</h:panelGroup> 
				<f:subview id="nbaCommentBar">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
				</f:subview>
			</div>
		</div>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	