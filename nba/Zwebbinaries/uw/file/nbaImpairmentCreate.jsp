<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA224 		   8	  nbA Underwriter Workbench Requirements and Impairments Enhancement -->
<!-- NBA226            8      nba MIB Translation and validation -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Create New Impairments</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaImpairmentCreate.faces';  //NBA213
		function setTargetFrame() {
 			document.forms['form_createImpairment'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
 			document.forms['form_createImpairment'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_createImpairment']['form_createImpairment:draftChanges'].value;  //NBA213
		}
</script>
<!-- FNB011 code deleted -->
</head>
	<body onload="filePageInit();setDraftChanges();" style="overflow-x:hidden;overflow-y:scroll">  <!-- SPR2965 -->
		<f:view>
			<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
			<PopulateBean:Load serviceName="RETRIEVE_REQUIREMENTS" value="#{pc_impCreateUpdate}" />
			<h:form id="form_createImpairment">
				<f:subview id="tabHeader">
					<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp"/>
				</f:subview>
				<!-- ********************************************************
					Impairments table
				 ******************************************************** -->
				<f:subview id="impOverview">
					<c:import url="/uw/subviews/NbaImpairmentsTable.jsp"/>
				</f:subview>
				<!-- ********************************************************
					Create/Update Impairment
				 ******************************************************** -->
				<div id="impCreate" class="inputFormMat">  <!-- SPR2965 -->
					<div class="inputForm">
						<h:outputLabel id="cnTitle" value="#{property.impCreateTitle}" styleClass="formTitleBar"/>
						<!-- ********************************************************
							Search
						 ******************************************************** -->						
						<f:subview id="impSearch">
							<c:import url="/uw/subviews/NbaImpairmentSearch.jsp"/>
						</f:subview>
						<!-- ********************************************************
							Impairment Type/Description/Debit/Credit Information
						 ******************************************************** -->												
						<h:outputLabel value="#{property.impInfo}" styleClass="formSectionBar"/>
						<h:panelGroup styleClass="formDataDisplayTopLine">
							<h:outputLabel value="#{property.impType}" styleClass="formLabel" />
							<h:selectOneMenu styleClass="formEntryText" style="width: 370px" value="#{pc_impCreateUpdate.impairmentType}" binding="#{pc_impCreateUpdate.compImpairmentType}" >
								<f:selectItems value="#{pc_impCreateUpdate.impairmentTypeList}" />
							</h:selectOneMenu>
							<h:inputText id="Date" value="#{pc_impCreateUpdate.impairmentDate}" styleClass="formEntryDate" style="width:98px; margin-left: 10px">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:inputText>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataDisplayLine">
							<h:outputLabel value="#{property.impDesc}" styleClass="formLabel"/>
							<h:inputText value="#{pc_impCreateUpdate.description}" binding="#{pc_impCreateUpdate.compDescription}" styleClass="formEntryTextFull"/>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputLabel value="#{property.impDebit}" styleClass="formLabel"/>
							<h:inputText id="Debit" value="#{pc_impCreateUpdate.debit}" styleClass="formEntryText">
								<f:convertNumber maxIntegerDigits="6" integerOnly="true" type="integer" />
							</h:inputText>
							<h:outputLabel value="#{property.impCredit}" styleClass="formLabel"/>
							<h:inputText id="Credit" value="#{pc_impCreateUpdate.credit}" styleClass="formEntryText">
								<f:convertNumber maxIntegerDigits="6" integerOnly="true" type="integer" />
							</h:inputText>
						</h:panelGroup>
						<!-- begin NBA224 -->
						<h:panelGroup id="restrictPGroup" styleClass="formDataEntryLine">
							<h:outputLabel id="restrictLbl" value="#{property.impRestrict}" styleClass="formLabel" />
							<h:selectBooleanCheckbox id="restrictCheck" value="#{pc_impCreateUpdate.restrict}" disabled="true"/>
						</h:panelGroup>
						<!-- end NBA224 -->
						<!-- ********************************************************
							Ratings
						 ******************************************************** -->																								
						<hr class="formSeparator"/>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:panelGroup styleClass="formBarSeparator">
								<h:outputLabel value="#{property.impPermRating}" styleClass="formLabel"/>
								<h:inputText id="Permanent_Rating" value="#{pc_impCreateUpdate.permFlatExtraAmt}" styleClass="formEntryText" style="width: 100px">
									<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
								</h:inputText>
							</h:panelGroup>
							<h:outputLabel value="#{property.impTempRating}" styleClass="formLabel" style="margin-left: 10px"/>							 
							<h:inputText id="Temporary_Rating" value="#{pc_impCreateUpdate.tempFlatExtraAmt}" styleClass="formEntryText" style="width: 100px">
								<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
							</h:inputText>		
							<h:inputText   value="#{pc_impCreateUpdate.impairmentDuration}" styleClass="formEntryText" style="margin-left: 10px; width: 50px">
								<f:convertNumber maxFractionDigits="0" maxIntegerDigits="2" type="integer" />
							</h:inputText>
							<h:outputLabel id="Years" value="#{property.impTempYears}" styleClass="formLabelRight"/>							
						</h:panelGroup>
						<!-- ********************************************************
							Impairment Messages
						 ******************************************************** -->		
						<hr class="formSeparator"/>
						<f:subview id="impMessagesTable">
							<c:import url="/uw/subviews/NbaImpairmentMessagesTable.jsp"/>
						</f:subview>
						<!-- ********************************************************
							MIB codes
						 ******************************************************** -->																		
						<!-- NBA226 MIB section deleted -->
						<!-- ********************************************************
							Status
						 ******************************************************** -->																														
						<hr class="formSeparator"/>
						<h:panelGroup styleClass="formDataEntryLine">
							<h:outputLabel value="#{property.impStatus}" styleClass="formLabel"/>						
							<h:selectOneMenu styleClass="formEntryText" style="width: 300px" value="#{pc_impCreateUpdate.impairmentStatus}" >
								<f:selectItems value="#{pc_impCreateUpdate.impairmentStatusList}" />
							</h:selectOneMenu>							
						</h:panelGroup>
						<!-- ********************************************************
							Command Buttons
						 ******************************************************** -->																		
						<hr class="formSeparator"/>
						<h:panelGroup styleClass="formButtonBar">
							<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();"
								action="#{pc_impCreateUpdate.actionCancel}" immediate="true" />
							<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
								action="#{pc_impCreateUpdate.actionClear}" immediate="true" />
							<h:commandButton value="#{property.buttonAddNew}" styleClass="formButtonRight-1" onclick="resetTargetFrame();"
								action="#{pc_impCreateUpdate.actionAddNew}" rendered="#{pc_impCreateUpdate.renderAdd}"/>
							<h:commandButton value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="resetTargetFrame();" 
								action="#{pc_impCreateUpdate.actionAdd}" rendered="#{pc_impCreateUpdate.renderAdd}"/>
							<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight" onclick="resetTargetFrame();"
								action="#{pc_impCreateUpdate.actionUpdate}" rendered="#{!pc_impCreateUpdate.renderAdd}"/>
						</h:panelGroup> 
						<f:subview id="nbaCommentBar">
							<c:import url="/common/subviews/NbaCommentBar.jsp"/>  <!-- SPR3090 -->
						</f:subview>
					</div>
				</div>
			<div id="Messages" style="display:none">
				<h:messages />
			</div>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
			</h:form>

		</f:view>
	</body>
</html>
