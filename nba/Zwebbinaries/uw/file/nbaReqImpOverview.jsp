<%-- CHANGE LOG
	Audit Number   Version   Change Description
	NBA122            5      Underwriter Workbench Rewrite
	SPR2965           6      Underwriter Workbench Scrollbars
	SPR3090           6      Table paging support should be generalized for use by any table panes
	NBA158 		   	  6	  	 Websphere 6.0 upgrade
	NBA212            7      Content Services
	NBA213 		   	  7	  	 Unified User Interface
	NBA208-32		  7	  	 Workflow VO Convergence
	SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implem
	FNB011 			NB-1101	  Work Tracking
	SPRNBA-469     	NB-1101   Initial View Displayed After Commit From Underwriter Workbench Requirements/Impairments
	FNB004         	NB-1101	  PHI
	NBA223			NB-1101	  Underwriter Final Disposition
	SPRNBA-557		NB-1301	  ReAutoUnderwrite Checkbox Enabled for Approved and Negatively Disposed Cases
	NBA309			NB-1301	  Pharmaceutical Information
	SPRNBA-658     	NB-1301   Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench
	SPR3103      	NB-1401   Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View
	SPRNBA-747     	NB-1401   General Code Clean Up
	NBA329			NB-1401   Retain Denied Coverage and Benefit
	SPRNBA-798     	NB-1401   Change JSTL Specification Level
	NBA353		    NB-1501	  Save Draft Comments when navigating from case to case
	NBA356      	NB-1501   Comments Floating View
	NBA362      	NB-1501   Save Draft Requirements and Impairments when navigating from case to case
	SPRNBA-1005    	NB-1601   May Receive Erroneous Confirmation For Uncommitted Comments 
--%>

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>  <%-- NBA213 --%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Requirements and Impairments Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<!-- NBA213 code deleted -->
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'uw/file/nbaReqImpOverview.faces';  //NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_reqimpoverview'].target='controlFrame';
			document.getElementById("form_reqimpoverview:parentView").value = 'requirement';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_reqimpoverview'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_reqimpoverview']['form_reqimpoverview:draftChanges'].value;  //NBA213
		}
		// Update the notification bar on the tab in case a commit causes a status change.
		//SPRNBA-469 New Method
		function updateNotificationBar() {
			parent.notifyTab('6_1', document.forms['form_reqimpoverview']['form_reqimpoverview:attnRequired'].value);
		}
		//SPRNBA-469 New Method
		function initialize() {
			setDraftChanges();
			updateNotificationBar();
			draftCommentsPresent(); //SPRNBA-658
			draftReqImpPresent(); //NBA362
		}
        //FNB004 new method
		function launchPHIBriefcaseView() {			        
			phipopup = launchPopup('phipopup', '<%=path%>/common/popup/popupFramesetPHI.html?popup=<%=basePath%>/nbaPHI/file/PHIStatus.faces?readonly=true', 650, 690);
            phipopup.focus();
            top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup = phipopup;		//FNB004 - VEPL1224		
            return false;
		}
		
		//SPRNBA-658 new method
		function draftCommentsPresent() {
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = document.forms['form_reqimpoverview']['form_reqimpoverview:draftComments'].value;
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftCommentsOnOtherContract = document.forms['form_reqimpoverview']['form_reqimpoverview:draftCommentsOnOtherContract'].value;	//NBA353
		}
		
		//NBA362 new method
		function draftReqImpPresent() {
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImp = document.forms['form_reqimpoverview']['form_reqimpoverview:draftReqImp'].value;
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftReqImpOnOtherContract = document.forms['form_reqimpoverview']['form_reqimpoverview:draftReqImpOnOtherContract'].value;
		}
		
		//SPRNBA-1005 new method
		function resetDraftCommentStatus(){
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- NBA158 --%>
</head>
<%-- NBA213 Common Tab Style --%>
<body class="whiteBody" onload="filePageInit();initialize();refreshAppFrameExt();" style="overflow-x: hidden; overflow-y: scroll">  <%-- SPR2965, SPRNBA-469, NBA356 --%>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />  <%-- NBA213 --%>
		<%--  NBA208-32 code deleted --%>
		<PopulateBean:Load serviceName="RETRIEVE_REQUIREMENTS" value="#{pc_reqimpNav}" />
		<h:form id="form_reqimpoverview">
			<%-- SPR2965 deleted code --%>
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
			</f:subview>
			<%-- SPR2965 deleted code --%>
			<h:outputLabel id="reqTableTitle" value="#{property.requirementTitle}" styleClass="sectionSubheader" />  <%-- SPR2965 --%>
			<f:subview id="reqOverviewTbl">
				<c:import url="/uw/subviews/NbaRequirementsTable.jsp" />
			</f:subview>
			<h:panelGroup id="reqButtonBar" styleClass="ovButtonBar"> <%-- SPR3399 --%>
				<h:commandButton id="btnReqDelete" value="#{property.buttonDelete}" styleClass="ovButtonLeft"
						disabled="#{pc_reqimpNav.selectedInsuredsRequirements.deleteDisabled}" immediate="true"
						onclick="setTargetFrame();" action="#{pc_reqimpNav.selectedInsuredsRequirements.deleteRequirement}" />
				<%-- begin NBA212 --%>
				<h:commandButton id="btnReqMIBReport" value="#{property.buttonMIBReport}" disabled="#{pc_reqimpNav.selectedInsuredsRequirements.mibReportDisabled}"
						styleClass="ovButtonRight-3" action="#{pc_reqimpNav.selectedInsuredsRequirements.mibReport}"
						onclick="resetTargetFrame()" immediate="true" />
				<h:commandButton id="btnReqViewMIB" value="#{property.buttonViewMIB}" rendered="#{pc_reqimpNav.selectedInsuredsRequirements.viewMIBRendered}"
						styleClass="ovButtonRight-2" action="#{pc_reqimpNav.selectedInsuredsRequirements.viewMIB}"
						onclick="resetTargetFrame()" immediate="true" />	<%-- NBA309 --%>
				<h:commandButton id="btnReqViewRX" value="#{property.buttonViewRx}" rendered="#{pc_reqimpNav.selectedInsuredsRequirements.viewRXRendered}"
						styleClass="ovButtonRight-2" action="#{pc_reqimpNav.selectedInsuredsRequirements.viewRx}"
						onclick="resetTargetFrame()" immediate="true" />	<%-- NBA309 --%>						
				<h:commandButton id="btnReqView" value="#{property.buttonViewUpdate}" disabled="#{pc_reqimpNav.selectedInsuredsRequirements.updateDisabled}"
						styleClass="ovButtonRight-1" action="#{pc_reqimpNav.selectedInsuredsRequirements.updateRequirement}"
						onclick="resetTargetFrame()" immediate="true" />  <%-- SPRNBA-747 --%>
				<h:commandButton id="btnReqCreate" value="#{property.buttonCreate}"
						styleClass="ovButtonRight" action="#{pc_reqimpNav.selectedInsuredsRequirements.createRequirement}"
						disabled="#{pc_reqimpNav.createDisabled}"
						onclick="resetTargetFrame()" immediate="true" />  <%-- NBA329 --%>
				<%-- end NBA212 --%>
			</h:panelGroup>

			<h:outputLabel id="impTableTitle" value="#{property.impairmentTitle}" styleClass="sectionSubheader"/>
			<f:subview id="impOverviewTbl">
				<c:import url="/uw/subviews/NbaImpairmentsTable.jsp" />
			</f:subview>
			<h:panelGroup id="reqimpStatusBar" styleClass="ovStatusBar"> <%-- SPR3399 --%>
				<h:outputFormat value="{0} {1}" styleClass="ovStatusBarTextBold">
					<f:param value="#{property.impairmentTotal}"></f:param>
					<f:param value="#{pc_impTable.impairmentTotal}"></f:param>
				</h:outputFormat>
				<h:outputText value="#{pc_impTable.debitTotal}" styleClass="ovStatusBarDebit" />
				<h:outputText value="#{pc_impTable.creditTotal}" styleClass="ovStatusBarCredit" />
				<h:outputText value="#{property.impairmentActivitiesPresent}" rendered="#{pc_reqimpNav.selectedImpairments.lifeStyleActivitiesPresent}"
							style="position: absolute; font-weight: bold; font-style: italic; left: 325px; padding-top: 3px" />
				<%-- begin NBA223 --%>
				<h:selectBooleanCheckbox id="reautounderwrite"
					style="margin-left:350px" value="#{pc_reqimpNav.reAutoUnderwrite}"
					disabled="#{pc_reqimpNav.negativeDisposition || pc_reqimpNav.uwApproved || pc_reqimpNav.tentDispForLvlOnePresent}"  /> <%-- SPRNBA-557 --%>
				<h:outputLabel value="#{property.uwfinalDispReautoUnderwrite}"
					styleClass="formLabelRight" />
				<%-- end NBA223 --%>
			</h:panelGroup>
			<h:panelGroup id="impButtonBar" styleClass="ovButtonBar"><%-- SPR3399 --%>
				<h:commandButton id="btnImpDelete" value="#{property.buttonDelete}" disabled="#{pc_reqimpNav.selectedImpairments.deleteDisabled}"
							styleClass="ovButtonLeft" onclick="setTargetFrame();" action="#{pc_reqimpNav.selectedImpairments.deleteImpairment}" immediate="true" />
				<h:commandButton id="btnImpView" value="#{property.buttonViewUpdate}" disabled="#{pc_reqimpNav.selectedImpairments.updateDisabled}"
							styleClass="ovButtonRight-1" action="#{pc_reqimpNav.selectedImpairments.updateImpairment}"
							onclick="resetTargetFrame()" immediate="true" />  <%-- NBA212, SPRNBA-747 --%>
				<h:commandButton id="btnImpCreate" value="#{property.buttonCreate}"
							styleClass="ovButtonRight" action="#{pc_reqimpNav.selectedImpairments.createImpairment}"
							disabled="#{pc_reqimpNav.createDisabled}"
							onclick="resetTargetFrame()" immediate="true" />  <%-- NBA212, NBA329 --%>
			</h:panelGroup>
			<h:inputHidden id="parentView" value="#{pc_commentsPopUpData.parentView}"/>
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />  <%-- SPR3090 --%>
			</f:subview>

			<h:panelGroup id="reqimptabButtonBar" styleClass="tabButtonBar"><%-- SPR3399 --%>
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_uwbean.refresh}"
							onclick="setTargetFrame();" immediate="true" styleClass="tabButtonLeft" />  <%-- NBA212 , SPR3103 --%>
				<h:commandButton id="btnRefer" value="#{property.buttonRefer}" action="#{pc_uwbean.refer}" immediate="true" styleClass="tabButtonLeft-1"  
					disabled="#{pc_uwbean.undCommitDisabledForFunctionalUpdates || pc_reqimpNav.notLocked || pc_uwbean.informalApplication}" onclick="setTargetFrame();"/> <%-- NBA186 --%>
				<h:commandButton id="btnReviewCmpt" value="#{property.buttonReviewCmpt}" action="#{pc_uwbean.reviewComplete}"  immediate="true" 
					rendered="#{pc_uwbean.renderReviewCompleteForAddnlApprovers}" disabled="#{pc_reqimpNav.notLocked}" styleClass="tabButtonRight-2"  onclick="setTargetFrame();" style="width: 110px; left: 320px" /> <%-- NBA186 --%>
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" action="#{pc_reqimpNav.commit}"
					disabled="#{pc_reqimpNav.auth.enablement['Commit'] || 
								pc_reqimpNav.notLocked || pc_uwbean.undCommitDisabledForFunctionalUpdates}" styleClass="tabButtonRight-1" onclick="setAppFrameExt();" /> <%-- NBA213, NBA186, NBA356 --%>
				<h:commandButton id="btnMIBTransmit" value="#{property.buttonMIBTransmit}" styleClass="tabButtonRight" action="#{pc_reqimpNav.transmitMIB}"
					onclick="resetTargetFrame()"
					disabled="#{pc_reqimpNav.auth.enablement['Commit'] ||
								pc_reqimpNav.notLocked ||
								pc_reqimpNav.transmitMIBDisabled}" />	<%-- NBA213, NBA212 --%>
			</h:panelGroup>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
			<h:inputHidden id="attnRequired" value="#{pc_attntnreqbean.reqImpRequiresAttn}" /><%-- SPRNBA-469 --%>
			<%-- SPR2965 deleted code --%>
			<h:inputHidden id="draftComments" value="#{pc_uwbean.draftCommentsPresent}" />  <%-- SPRNBA-658 --%>
			<h:inputHidden id="draftCommentsOnOtherContract" value="#{pc_uwbean.draftCommentsOnOtherContract}" />  <%-- NBA353 --%>
			<h:inputHidden id="draftReqImp" value="#{pc_uwbean.draftReqImpPresent}" />  <%-- NBA362 --%>
			<h:inputHidden id="draftReqImpOnOtherContract" value="#{pc_uwbean.draftReqImpOnOtherContract}" />  <%-- NBA362 --%>
		</h:form>
				
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	