<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA275         NB-1101   nbA Wrappered wmA Contract Change-->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>TX502 Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	
		
		function setTargetFrame() {
			document.forms['form_TX502'].target='controlFrame';
			return false;
		}
		
		function resetTargetFrame() {
			document.forms['form_TX502'].target='';
			return false;
		}
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body class="whiteBody" style="overflow-x: hidden; overflow-y: scroll">
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_TX502XML" value="#{pc_retrieveTX502}" />
		<h:form id="form_TX502">
			<h:outputLabel id="formatedXML" value="#{pc_retrieveTX502.formatedTX502Request}" />
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>	
</html>	
		  