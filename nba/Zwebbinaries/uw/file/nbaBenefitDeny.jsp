<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223			NB-1101   Underwriter Final Disposition  --> 
<!-- SPRNBA-576		NB-1301   Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			  
			document.forms['form_BenefitDeny'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			 
			document.forms['form_BenefitDeny'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_BenefitDeny']['form_BenefitDeny:draftChanges'].value;  //NBA213
		}			
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> 
</head>
<body class="whiteBody" onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: auto"> 
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_BENEFIT_VIEW_UPDATE" value="#{pc_NbaBenefitViewUpdate}" />
	<h:form id="form_BenefitDeny">
		<!-- ********************************************************
			coverage Table
		 ******************************************************** -->
		<f:subview id="client1Table"> <!-- SPRNBA-576 -->
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" /> <!-- SPRNBA-576 -->
		</f:subview>
		<!-- SPRNBA-576 code deleted -->
		<!-- ********************************************************
			View/Update Benefit
		 ******************************************************** -->
		<div id="viewUpdate" class="inputFormMat" >
			<div class="inputForm" style="height : 225px;">
				<!-- Table Header -->
				<h:outputLabel id="viewBenefitTitle" value="#{property.uwCovDenyBenefitTitle}" styleClass="formTitleBar" />
				<!-- Display Fields -->
					<h:panelGroup id="BenDenyPan1" styleClass="formDataDisplayLine">
						<h:panelGroup id="BenDenyPan2" style="width: 380px;  overflow: hidden;">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwCovOptionType}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText value="#{pc_NbaBenefitViewUpdate.typeText}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup id="BenDenyPan3" style="position: absolute; left: 330px">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwEffective}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText value="#{pc_NbaBenefitViewUpdate.effectiveDate}" styleClass="formEntryDate">
										<f:convertDateTime pattern="#{property.datePattern}" />
									</h:outputText>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
					</h:panelGroup>
					<h:panelGroup id="BenDenyPan4" styleClass="formDataDisplayLine">
						<h:panelGroup style="width: 380px;  overflow: hidden;">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwStatus}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText value="#{pc_NbaBenefitViewUpdate.statusText}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup id="BenDenyPan5" style="position: absolute; left: 330px">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText id="Cease_Date" value="#{pc_NbaBenefitViewUpdate.ceaseDate}" styleClass="formEntryDate">
										<f:convertDateTime pattern="#{property.datePattern}" />
									</h:outputText>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
					</h:panelGroup>

					<h:panelGroup id="BenDenyPan6" styleClass="formDataDisplayLine" rendered="#{pc_NbaBenefitViewUpdate.proposedUWStatusPresent}">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column>
								<h:outputLabel value="#{property.uwProposeStatus}" styleClass="formLabel" />
							</h:column>
							<h:column>
								<h:outputText value="#{pc_NbaBenefitViewUpdate.proposedUnderwritingStatusText}" styleClass="formDisplayText"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
		
					<f:verbatim>
						<hr class="formSeparator" />
					</f:verbatim>
	
					<h:panelGroup id="BenDenyPan7" styleClass="formDataDisplayLine">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column>
								<h:outputLabel value="#{property.uwAmount}" styleClass="formLabel" />
							</h:column>
							<h:column>
								<h:outputText styleClass="formDisplayText" value="#{pc_NbaBenefitViewUpdate.amountText}"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup id="BenDenyPan8" styleClass="formDataDisplayLine">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column>
								<h:outputLabel value="#{property.uwPremium}" styleClass="formLabel"/>
							</h:column>
							<h:column>
								<h:outputText value="#{pc_NbaBenefitViewUpdate.premium}" styleClass="formDisplayText"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
				<!-- Input Fields -->
				<!-- begin NBA329 -->
				<h:panelGroup styleClass="formInputSection">
					<h:panelGroup id="BenDenyPan9" styleClass="formDataEntryLine" style="margin-bottom: 7px">
						<h:outputLabel value="#{property.uwAction}" styleClass="formLabel" />
						<h:selectBooleanCheckbox value="#{pc_NbaBenefitViewUpdate.denyChecked}" />
						<h:outputLabel value="#{property.buttonDeny}" styleClass="formLabelRight" style="margin-left: 0px;" />
						<h:commandButton id="reasonButton" image="images/link_icons/circle_i.gif" 
								style="margin-left: 10px; margin-bottom: -5px" 
								onclick="setTargetFrame();" 
								action="#{pc_NbaBenefitViewUpdate.launchReasonView}" /> <!-- SPRNBA-576 -->
					</h:panelGroup>
				</h:panelGroup>
				<!-- end NBA329 -->
				<!-- Button bar -->
				<h:panelGroup id="BenDenyPan10" styleClass="formButtonBar" style="margin-top: 10px">  <!-- NBA329 -->
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" immediate="true" action="#{pc_finalDispNavigation.actionCloseSubView}" /> <!-- SPRNBA-576 -->
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_NbaBenefitViewUpdate.actionUpdate}" onclick="resetTargetFrame(); submit()"/>
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();" action="#{pc_NbaBenefitViewUpdate.actionClear}"/>
				</h:panelGroup>							
			</div>
		</div>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" /> 
		</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
