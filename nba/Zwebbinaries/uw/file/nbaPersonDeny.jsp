<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223			NB-1101   Underwriter Final Disposition -->
<!-- SPRNBA-576		NB-1301   Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!-- NBA329 code deleted -->
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			  
			document.forms['form_PersonDeny'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			 
			document.forms['form_PersonDeny'].target='';
			return false;
		}
	</script>
	<!-- NBA329 code deleted -->
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: auto">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_PERSON_VIEW_UPDATE" value="#{pc_NbaPersonViewUpdate}" />
		<h:form id="form_PersonDeny">
		<!-- NBA223 code deleted -->
		<!-- ********************************************************
			coverage Table
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		
		<!-- begin NBA329 -->
		<h:panelGroup id="viewUpdate" styleClass="inputFormMat">
			<h:panelGroup id="inputform" styleClass="inputForm" style="height: 150px">
				<h:outputLabel id="viewPersonTitle" value="#{property.uwCovDenyPersonTitle}" styleClass="formTitleBar" />
				<!-- Display Fields -->
				<h:panelGroup styleClass="formDataDisplayTopLine">
					<h:outputLabel value="#{property.uwName}" styleClass="formLabel" style="width:200px"/>
					<h:outputText value="#{pc_NbaPersonViewUpdate.name}" styleClass="formDisplayText"/>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataDisplayLine" rendered="#{pc_NbaPersonViewUpdate.proposedUWStatusPresent}">
					<h:outputLabel value="#{property.uwProposeStatus}" styleClass="formLabel" style="width:200px"/>
					<h:outputText value="#{pc_NbaPersonViewUpdate.proposedUnderwritingStatusText}" styleClass="formDisplayText"/>
				</h:panelGroup>	

				<!-- Input Fields -->
				<h:panelGroup styleClass="formInputSection" style="margin-top: 20px">	
					<h:panelGroup styleClass="formDataEntryLine" style="margin-bottom: 7px">
						<h:outputLabel value="#{property.uwAction}" styleClass="formLabel" style="width:200px" />
						<h:selectBooleanCheckbox  id="deny_CB" value="#{pc_NbaPersonViewUpdate.denyChecked}" />
						<h:outputLabel value="#{property.buttonDeny}" styleClass="formLabelRight" style="margin-left: 0px;" />
						<h:commandButton id="reasonButton" image="images/link_icons/circle_i.gif"
								style="margin-left: 10px; margin-bottom: -5px" 
								onclick="setTargetFrame();"
								action="#{pc_NbaPersonViewUpdate.launchReasonView}" />
					</h:panelGroup>
				</h:panelGroup>
				<!-- Button bar -->
				<h:panelGroup styleClass="formButtonBar" style="margin-top: 20px">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft"
								onclick="resetTargetFrame();" action="#{pc_finalDispNavigation.actionCloseSubView}"
								immediate="true" />
					<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1"
								onclick="resetTargetFrame();" action="#{pc_NbaPersonViewUpdate.actionClear}"/>
					<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight"
								onclick="resetTargetFrame();" action="#{pc_NbaPersonViewUpdate.actionUpdate}"/>
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
		<!-- end NBA329 -->

		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />		
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
