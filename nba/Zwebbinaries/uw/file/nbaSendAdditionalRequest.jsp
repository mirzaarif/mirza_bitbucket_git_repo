<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2882           6      Reinsurance Tab - UI Issues with Send Additional Information view -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA208-36         7      Deferred Work Item retrieval -->
<!-- SPR2877           8      Reinsurance Tab - Unable to update draft additional information - Addl Info button is disabled -->
<!-- FNB011 	    NB-1101	  Work Tracking -->
<!-- SPR2997  		NB-1301	  Reinsurance Tab: Incorrect Look and Feel of 'Select all ' and 'Deselect all' Push Buttons-->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
	
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaSendAdditionalRequest.faces';	//NBA212, NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_sendAdditionalInfo'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_sendAdditionalInfo'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_sendAdditionalInfo']['form_sendAdditionalInfo:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_ALL_SOURCES" value="#{pc_allSourcesWI}" /> <!-- NBA208-36 -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE_ADDNL_INFO" value="#{pc_nbaReinsuranceSendAddInfo}" /><!-- SPR2877 -->
	<h:form id="form_sendAdditionalInfo">
		<h:panelGroup id="sendInfoPG1" styleClass="sectionHeader">
			<h:outputText id="reinsuranceSendInfoTitle" value="#{property.sendAddInfo}" styleClass="shTextLarge" style="position: absolute; left: 10px" rendered="#{!pc_nbaReinsNav.renderViewUpdateAdditionalInfo}"/>	<!--SPR2877 -->
			<h:outputText id="reinsuranceUpdateSendInfoTitle" value="#{property.updateAddInfo}" styleClass="shTextLarge" style="position: absolute; left: 10px" rendered="#{pc_nbaReinsNav.renderViewUpdateAdditionalInfo}"/>	<!--SPR2877 -->
		</h:panelGroup>
		<h:panelGroup id="sendAddInfo" styleClass="ovDivTableData" style="height: 150px">
			<h:dataTable id="sendAddInfoTable" styleClass="ovTableData"
				cellspacing="0" rows="0"
				binding="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestResponseTable}"
				value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestResponseList}"
				var="reinsSendAddInfo" rowClasses="#{pc_nbaReinsuranceSendAddInfo.requestResponseRowStyles}"
				columnClasses="ovColText200,ovColText85,ovColText85,ovColDate,ovColIcon">
				<h:column>
				<h:commandButton id="reinCol1a" image="images/hierarchies/L.gif" rendered="#{reinsSendAddInfo.levelOne}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:commandButton id="reinCol1b" image="images/hierarchies/T.gif" rendered="#{reinsSendAddInfo.levelOneWithNext}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:outputText id="reinCol1c" value="#{reinsSendAddInfo.requests}"	style="vertical-align: top;"/>
				</h:column>
				<h:column>
					<h:outputText id="reinCol2" styleClass="ovFullCellSelect" value="#{reinsSendAddInfo.reinsured}"/>
				</h:column>
				<h:column>
					<h:outputText id="reinCol3" styleClass="ovFullCellSelect" value="#{reinsSendAddInfo.retained}"/>
				</h:column>
				<h:column>
					<h:outputText id="reinCol4" styleClass="ovFullCellSelect" value="#{reinsSendAddInfo.date}"/>
				</h:column>
				<h:column>
					<h:commandButton id="reinCol6a"
						image="images/link_icons/documents.gif"
						styleClass="ovViewIconTrue"
						onclick="setTargetFrame()"
						immediate="true" action="#{reinsSendAddInfo.showSelectedImage}"/> <!-- NBA212 -->
				</h:column>
			</h:dataTable>
		</h:panelGroup>

		<!--Begin SPR2882 -->		
		<h:panelGroup id="requestLevelPG" styleClass="formDataEntryLine">
			<h:outputText id="requestLevelPGOT1" value="#{property.requestLevel}" styleClass="formLabel" />
			<h:outputText id="requestLevelPGOT2" value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.requestLevel}"
				styleClass="formDisplayText" />
		</h:panelGroup>
		<h:panelGroup id="messagePG" styleClass="formDataEntryLine"> 
			<h:outputText id="requestLevelPGOT3" value="#{property.message}" styleClass="formLabel" />
			<h:outputText id="requestLevelPGOT4" value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.message}"
				styleClass="formDisplayText" />
		</h:panelGroup>
		<h:panelGroup id="addlInfoImagePG1" styleClass="formDataEntryLine">
			<h:column>              <!-- SPR2997 -->
				<h:panelGrid>   	<!-- SPR2997 -->
					<h:outputText id="addlInfoImagePGOT1" value="#{property.images}" styleClass="formLabel" />
					<!-- BEGIN SPR2997 -->
					<h:commandButton id="reinsViewSelectAll" styleClass="formButtonInterface" value="#{property.reinCreateRequestSelectAll}"
						action="#{pc_nbaReinsuranceSendAddInfo.selectAllImageRows}" onclick="resetTargetFrame();" 
						style="position: relative; left: 25px; border-style: none;" />
					<h:commandButton id="reinsViewDeSelectAll" styleClass="formButtonInterface" value="#{property.reinCreateRequestDeselectAll}"
						action="#{pc_nbaReinsuranceSendAddInfo.deselectAll}" onclick="resetTargetFrame();" 
						style="position: relative; left: 25px; border-style: none;" />
				</h:panelGrid>
			</h:column>
			<h:column>
				<h:panelGroup id="addlInfoImagePG2" styleClass="ovDivTableData" style="position:relative; width:499px; height: 100px; top:-80px; left: 125px;">
				<!--End SPR2882 --><!--END SPR2997 -->
					<h:dataTable id="reinsAddlInfoImageTable" styleClass="ovTableData"
						cellspacing="0"
						binding="#{pc_nbaReinsuranceSendAddInfo.imageTable}"
						value="#{pc_nbaReinsNav.allImageList}"
						var="imageRow"
						rowClasses="#{pc_nbaReinsuranceSendAddInfo.imageRowStyles}"
						columnClasses="ovColText200,ovColText50,ovColIcon" style="width:497px;">
						<h:column>
							<h:commandLink id="addlInfoImagePG2CL1" styleClass="ovFullCellSelect" value="#{imageRow.sourceType}" action="#{pc_nbaReinsuranceSendAddInfo.selectImageRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink id="addlInfoImagePG2CL2" styleClass="ovFullCellSelect" value="#{imageRow.send}" action="#{pc_nbaReinsuranceSendAddInfo.selectImageRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandButton id="reinsImageTableCol3a"
								image="images/link_icons/documents.gif"
								onclick="setTargetFrame()" styleClass="ovViewIconTrue" immediate="true" action="#{imageRow.showSelectedImage}" />    <!-- NBA212 -->
						</h:column>
					</h:dataTable>
				</h:panelGroup>
			</h:column>         <!-- SPR2997 -->
			<!--SPR2882 code deleted-->
		</h:panelGroup>
		
		<!--Begin SPR2882 -->		
		<h:panelGroup id="addlInfoMessagePG" styleClass="formDataEntryLine">
			<h:outputText id="addlInfoMessagePGOT1" value="#{property.additionalMessage}" styleClass="formLabel"
				style="vertical-align: top;" />
			<h:inputTextarea id="addlInfoMessagePGIT1" value="#{pc_nbaReinsNav.reinsuranceSendAddInfo.addInfoMessage}" rows="4" styleClass="formEntryTextMultiLineFull"
				style="width: 499px;" />
		<!--End SPR2882 -->		
		</h:panelGroup>

		<h:panelGroup id="addlInfoButtonBar" styleClass="formButtonBar">
			<h:commandButton id="reinsViewClose" value="#{property.buttonCancel}"
				action="#{pc_nbaReinsNav.actionCancel}" onclick="resetTargetFrame();"
				styleClass="formButtonLeft"/>
			<h:commandButton id="reinsViewSendInfo" value="#{property.buttonAdd}"
				action="#{pc_nbaReinsNav.reinsuranceSendAddInfo.actionAdd}" rendered="#{!pc_nbaReinsNav.renderViewUpdateAdditionalInfo}" onclick="resetTargetFrame();"
				styleClass="formButtonRight"/><!-- SPR2877 -->
			<h:commandButton id="reinsUpdateSendInfo" value="#{property.buttonUpdate}"
				action="#{pc_nbaReinsNav.reinsuranceSendAddInfo.actionUpdate}" rendered="#{pc_nbaReinsNav.renderViewUpdateAdditionalInfo}" onclick="resetTargetFrame();"
				styleClass="formButtonRight"/><!-- SPR2877 -->
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
		
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
