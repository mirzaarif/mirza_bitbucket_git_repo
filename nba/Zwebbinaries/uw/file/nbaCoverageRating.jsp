<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA223     	NB-1101   Underwriter final disposition -->
<!-- FNB011 		NB-1101	  Work Tracking -->
<!-- FNB013		    NB-1101	   DI Support for nbA -->
<!-- SPRNBA-576     NB-1301   Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-604     NB-1301   Clear Does Not Reset Rate Class Section -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level --> 

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {			  
			document.forms['form_Rating'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {			 
			document.forms['form_Rating'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_Rating']['form_Rating:draftChanges'].value;  //NBA213
		}			
	</script>
	<!--  FNB011 code deleted -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_RATING" value="#{pc_NbaRating}" />
	<!-- begin  NBA223 -->
	<h:form id="form_Rating">			
		<!-- ********************************************************
			coverage Table (optional)
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		<f:subview id="rateClass_area" rendered="#{pc_NbaRating.insuredSelected && !pc_NbaRating.viewMode}">			
			<c:import url="/uw/subviews/NbaRateClassUpdate.jsp" />
		</f:subview>
		
		<f:subview id="rating_area">
			<c:import url="/uw/subviews/NbaRating.jsp" />
		</f:subview>
		
		<!-- begin  FNB013 -->
		<f:subview id="occupationClass_area" rendered="#{pc_NbaRating.di}">				
			<c:import url="/uw/subviews/NbaOccupationClassUpdate.jsp" />
		</f:subview>
		<!-- end  FNB013 -->
		
		<f:subview id="preferredLevel_area" rendered="#{pc_NbaRating.insuredSelected && !pc_NbaRating.viewMode}">
			<c:import url="/uw/subviews/NbaPreferredLevelUpdate.jsp" />
		</f:subview> 
		
		<f:subview id="nbaCommentBarView">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  
		</f:subview>
			
		<h:panelGroup id="covRatingbutton" styleClass="formButtonBar" style="height : 55px;"> <!-- NBA213 FNB013-->
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft"
					action="#{pc_finalDispNavigation.actionCloseSubView}" onclick="resetTargetFrame();"  immediate="true" /> 
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" action="#{pc_NbaRating.actionClear}" onclick="resetTargetFrame();" rendered="#{!pc_NbaRating.viewMode}" /> <!-- SPRNBA-604 -->
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_NbaRating.actionUpdate}" onclick="resetTargetFrame();" rendered="#{!pc_NbaRating.viewMode}"  />	<!-- SPRNBA-604 -->
		</h:panelGroup>		

		<!-- end NBA223 -->

		<!-- NBA213 code deleted-->
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />		
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
