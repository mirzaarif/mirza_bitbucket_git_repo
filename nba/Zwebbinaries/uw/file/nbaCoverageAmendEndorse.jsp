<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA223         NB-1101   Underwriter Final Disposition -->
<!-- FNB011 		NB-1101	  Work Tracking -->
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401  Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaCoverageAmendEndorse.faces';
		
		function setTargetFrame() {			  
			document.forms['form_AmendEndorse'].target='controlFrame'; //NBA223
			return false;
		}
		function resetTargetFrame() {			 
			document.forms['form_AmendEndorse'].target=''; //NBA223
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_AmendEndorse']['form_AmendEndorse:draftChanges'].value;  //NBA213
		}		
	</script>
	<!-- FNB011 code deleted -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_AMEND_ENDORSE" value="#{pc_NbaAmendEndorse}" />
	<!-- NBA223 code deleted -->
	<h:form id="form_AmendEndorse" > <!--NBA223 -->
		<!-- ********************************************************
			coverage Table (optional)
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		
		<!-- ********************************************************
			Amendment Endorsement fields
		 ******************************************************** -->
		<!--begin NBA223 -->
		<f:subview id="amend_endorse_area">
			<c:import url="/uw/subviews/NbaAmendEndorse.jsp" />
		</f:subview>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>
	<!--  NBA223 end -->		
		</h:form>
		<!-- NBA213 code deleted -->
		
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
