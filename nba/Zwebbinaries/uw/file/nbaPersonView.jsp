<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223    		 NB-1101      Underwriter Final Disposition -->
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			document.forms['form_PersonView'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_PersonView'].target='';
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: auto"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_PERSON_VIEW_UPDATE" value="#{pc_NbaPersonViewUpdate}" />
	<!--  NBA223 code deleted -->
	<h:form id="form_PersonView">
		<!-- ********************************************************
			coverage Table 
		 ******************************************************** -->
		<f:subview id="client1Table">
			<c:import url="/uw/finalDisp/subviews/NbaCoverageTable.jsp" />
		</f:subview>
		<!-- ********************************************************
			View/Update Person
		 ******************************************************** -->
		<f:subview id="personView">
			<c:import url="/uw/subviews/NbaPerson.jsp"/>
		</f:subview><!--
									<h:outputText></h:outputText>
									<h:outputText></h:outputText>
									<h:outputText></h:outputText>
									<h:outputText></h:outputText>
									<h:outputText></h:outputText>																		
									<h:outputText></h:outputText>																		
									<h:outputText></h:outputText>																											
									<h:outputText></h:outputText>																		
		
		--><h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />			
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
