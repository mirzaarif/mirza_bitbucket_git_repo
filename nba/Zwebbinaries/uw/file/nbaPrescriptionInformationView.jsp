<!-- CHANGE LOG -->
<!-- Audit Number   Version   			Change Description -->
<!-- NBA309 		Version NB-1301		Pharmaceutical Information -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%
    String path = request.getContextPath();
    String basePath = "";
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Create New Requirements</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/global/scroll.js"></script> 
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="include/common.js"></script>
<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaPrescriptionInformationView.faces';
		 
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_prescriptionInformation'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_prescriptionInformation'].target='';
			return false;
		}
		function saveTableScrollPosition() {
			//alert('saveTableScrollPosition');
			saveScrollPosition('form_prescriptionInformation:prescriptionSubView:prescriptionData', 'form_prescriptionInformation:prescriptionSubView:prescriptionTableVScroll');
			return false;
		}
		function scrollTablePosition() {
			//alert('scrollTablePosition');
			scrollToPosition('form_prescriptionInformation:prescriptionSubView:prescriptionData', 'form_prescriptionInformation:prescriptionSubView:prescriptionTableVScroll');
			return false;
		}		
 
	</script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body onload="filePageInit();scrollTablePosition();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_PRESCRIPTION_INFORMATION" value="#{pc_reqPrescriptionInformation}" />
	<h:form id="form_prescriptionInformation" onsubmit="saveTableScrollPosition();">
		<f:subview id="tabHeader">
			<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
		</f:subview>
		<f:subview id="reqOverview">
			<c:import url="/uw/subviews/NbaRequirementsTableReadOnly.jsp" />
		</f:subview>
		<div id="prescriptionInformation" class="inputFormMat">
			<div class="inputForm"><h:outputLabel id="cnTitle" value="#{property.rxResultsTitle}" styleClass="formTitleBar" /> 
				<f:subview id="prescriptionSubView">
					<c:import url="/uw/subviews/NbaPrescriptionTable.jsp" />
				</f:subview> 
				<h:panelGroup styleClass="formButtonBar">
						<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" action="#{pc_reqPrescriptionInformation.cancel}" immediate="true" />
				</h:panelGroup> 
				<f:subview id="nbaCommentBar">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />
				</f:subview>
			</div>
		</div>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
