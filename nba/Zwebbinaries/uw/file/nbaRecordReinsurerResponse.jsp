<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- SPR2872 		   8	  Reinsurance Tab - UI Issues with Record Reinsurer's Response view -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- NBA-399	1101	SPR/Defect -->
<!-- SPRNBA-616		NB-1301		Correct Problems with Reinsurance Responses -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaRecordReinsurerResponse.faces';  //NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_recordReinsuranceResponse'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_recordReinsuranceResponse'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_recordReinsuranceResponse']['form_recordReinsuranceResponse:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE" value="#{pc_nbaReinsResponse}" />
	<h:form id="form_recordReinsuranceResponse">
		<h:panelGroup styleClass="sectionHeader">
			<h:outputText value="#{property.recRecordReinsurerResponse}" styleClass="shTextLarge" /><!-- SPR2872 -->
		</h:panelGroup><!-- SPR2872 -->
		<h:panelGroup styleClass="sectionSubheader"><!-- SPR2872 -->
			<h:outputText value="#{pc_nbaReinsResponse.displayCompanyName}"
				styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup styleClass="ovDivTableHeader">
			<h:panelGrid columns="5"
				columnClasses="ovColHdrText105,ovColHdrText105,ovColHdrText105,ovColHdrText105,ovColHdrText105"
				cellspacing="0" styleClass="ovTableHeader">
				<h:column>
					<h:outputText value="#{property.recReinsCoverage}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{property.recReinsReinsuranceAccepted}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{property.recReinsCeded}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{property.recReinRetained}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{property.recReinsAccept}" styleClass="shText" />
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup id="Renstabladata" styleClass="ovDivTableData"
			style="height: 150px;">
			<h:dataTable id="reinsRecordReinsuranceResponseTable" cellspacing="0"
				binding="#{pc_nbaReinsResponse.reinsuranceResponseTable}"
				value="#{pc_nbaReinsResponse.reinsuranceTableList}" var="rein"
				rowClasses="#{pc_nbaReinsResponse.rowStyles}"
				styleClass="ovTableData" columnClasses="ovColText105,ovColText105,ovColText105,ovColText105,ovColText105"><!-- SPR2872 SPRNBA-616 --> 
				<h:column>
					<h:commandLink id="reinResCol1" value="#{rein.coverageNameForDisplay}"
						action="#{pc_nbaReinsResponse.selectRow}" /><!--SPR2872, NBA-399-->	
				</h:column>
				<h:column>
					<!--  begin SPR2872 -->
					<h:commandLink id="reinResCol2" action="#{pc_nbaReinsResponse.selectRow}">
					<h:outputText value="#{rein.acceptedAmount}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/>
					</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="reinResCol3" action="#{pc_nbaReinsResponse.selectRow}">
					<h:outputText value="#{rein.cededAmount}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="reinResCol4" action="#{pc_nbaReinsResponse.selectRow}">
					<h:outputText value="#{rein.retainedAmount}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
					</h:outputText>
					</h:commandLink>
					<!-- end SPR2872 -->
				</h:column>
				<h:column>
					<h:selectBooleanCheckbox value="#{rein.acceptInd}"
						disabled="#{rein.acceptDisable}" readonly=""
						styleClass="formEntryCheckbox" />
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		<h:panelGroup styleClass="ovButtonBar" style="height = 30px;">
			<h:commandButton id="reinsViewUpdate" value="#{property.buttonUpdate}"
				action="#{pc_nbaReinsResponse.updateValue}"
				onclick="resetTargetFrame();" styleClass="ovButtonRight"
				style="top:213 px;" disabled="#{pc_nbaReinsResponse.showUpdate}" />
		</h:panelGroup>
		<h:panelGroup styleClass="ovTableData" style="height = 30px;">
			<h:panelGrid columns="4" columnClasses="ovColText165,ovColText165,ovColText165,ovColText165" cellspacing="0">
				<h:column>
					<h:outputText value="#{property.recRenReceived}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{pc_nbaReinsResponse.offerReceivedDate}"
						styleClass="shText">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:outputText>
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenExpires}" styleClass="shText" style="margin-left: 10px;" /><!-- SPR2872 -->
				</h:column>
				<h:column>
					<h:inputText
						value="#{pc_nbaReinsResponse.responseVO.reinsuranceOffer.offerExpirationDate}"
						styleClass="formEntryDate" style="width: 95px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}" >
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>

		<!-- NBA171 code deleted -->
		<h:panelGroup styleClass="ovTableData" style="height = 30px;">
			<h:panelGrid columns="4"
				columnClasses="ovColText165,ovColText165,ovColText165,ovColText165"
				cellspacing="0">
				<h:column>
					<h:outputText value="#{property.recRenReinAmtAccepted}"
						styleClass="shText" />
				</h:column>
				<h:column>
					<h:inputText id="Amount1"
						value="#{pc_nbaReinsResponse.reinsuredAmt}"
						styleClass="formEntryText" style="width: 110px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/><!-- SPR2872 -->
					</h:inputText>
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenReatinedAmount}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:inputText id="Amount2"
						value="#{pc_nbaReinsResponse.retainedAmt}"
						styleClass="formEntryText" style="width: 110px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/><!-- SPR2872 -->
					</h:inputText>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup styleClass="ovTableData" style="height = 30px;">
			<h:panelGrid columns="4"
				columnClasses="ovColText165,ovColText165,ovColText165,ovColText165"
				cellspacing="0">
				<h:column>
					<h:outputText value="#{property.recRenCededAmt}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:inputText id="Amount4" value="#{pc_nbaReinsResponse.cededAmt}"
						style="width: 110px;" styleClass="formEntryText" disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" /><!-- SPR2872 -->
					</h:inputText>
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenRateClass}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:selectOneMenu styleClass="formEntryDate" style="width: 110px;"
						value="#{pc_nbaReinsResponse.responseVO.reinsuranceOffer.rateClass}" disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:selectItems value="#{pc_nbaReinsResponse.requestedRateClass}" />
					</h:selectOneMenu>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup styleClass="ovTableData"><!-- SPR2872 -->
			<h:panelGrid columns="2" columnClasses="ovColText295,ovColText295"
				cellspacing="0">
				<h:column>
					<h:outputText value="#{property.recRenReinsurerMessage}" styleClass="shText"  />
				</h:column>
				<h:column>
					<h:inputTextarea value="#{pc_nbaReinsResponse.reinsuranceMessage}"
						rows="3" style="width: 470px; " disabled="#{pc_nbaReinsResponse.setAllDisbaled}" />
				</h:column>
			</h:panelGrid>
		</h:panelGroup>

		<h:panelGroup styleClass="sectionSubheader">
			<h:outputText value="#{property.recRenReinsurerRequiredRatings}"
				styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup styleClass="ovTableData"><!-- SPR2872 -->
			<h:panelGrid columns="4"
				columnClasses="ovColTextNoUnderLine125,ovColText205,ovColTextNoUnderLine125,ovColText205"
				cellspacing="0"><!-- SPR2872 -->
				<h:column>
					<h:outputText value="#{property.recRenTableRating}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:selectOneMenu styleClass="formEntryDate" style="width: 175px;" value="#{pc_nbaReinsResponse.ratingType}"	disabled="#{pc_nbaReinsResponse.setAllDisbaled}"><!-- SPR2872 -->
						<f:selectItems value="#{pc_nbaReinsResponse.tableRatingType}" />
					</h:selectOneMenu>
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenFlatRating}" styleClass="shText"/>
				</h:column>
				<h:column>
					<h:selectOneMenu styleClass="formEntryDate" style="width: 175px;"
						value="#{pc_nbaReinsResponse.flatRatingType}" disabled="#{pc_nbaReinsResponse.setAllDisbaled}"><!-- SPR2872 -->
						<f:selectItems value="#{pc_nbaReinsResponse.tableRating}" />
					</h:selectOneMenu>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup styleClass="ovTableData"><!-- SPR2872 -->
			<h:panelGrid columns="4"
				columnClasses="ovColTextNoUnderLine125,ovColText205,ovColTextNoUnderLine125,ovColText205"
				cellspacing="0"><!-- SPR2872 -->
				<h:column>
					<h:outputText value="#{property.recRenTable}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:selectOneMenu styleClass="formEntryDate" style="width: 175px;" value="#{pc_nbaReinsResponse.tableRatingPermOtTemp}" disabled="#{pc_nbaReinsResponse.setAllDisbaled}"><!-- SPR2872 -->
						<f:selectItems value="#{pc_nbaReinsResponse.table}" />
					</h:selectOneMenu>
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenAmount}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:inputText id="Amount6"
						value="#{pc_nbaReinsResponse.flatAmountPermOtTemp}"
						styleClass="formEntryText" style="width: 175px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}"><!-- SPR2872 -->
    					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}"/><!-- SPR2872 -->
					</h:inputText>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>

		<h:panelGroup style="margin-left: 10px;">
			<h:panelGrid columns="2" columnClasses="ovColText295,ovColText295"
				cellspacing="0">
				<h:column>
					<h:selectOneRadio id="reinsRB1" value="#{pc_nbaReinsResponse.tempTableRatingEndDateType}" layout="pageDirection" valueChangeListener="#{pc_nbaReinsResponse.enableTableRatingDateField}" onclick="submit();" immediate="true" 
						styleClass="shText"	disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:selectItem id="Item1" itemValue="Date" itemLabel="Date" />
						<f:selectItem id="Item2" itemValue="Duration" itemLabel="Duration" />
					</h:selectOneRadio>
				</h:column>
				<h:column>
					<h:selectOneRadio id="reinsRB2"	value="#{pc_nbaReinsResponse.tempFlatEndDateOrDurationType}" layout="pageDirection"	valueChangeListener="#{pc_nbaReinsResponse.enableFlatRatingDateField}" onclick="submit();" immediate="true" 
						styleClass="shText"	disabled="#{pc_nbaReinsResponse.setAllDisbaled}">
						<f:selectItem id="Item3" itemValue="Date" itemLabel="Date" />
						<f:selectItem id="Item4" itemValue="Duration" itemLabel="Duration" />
					</h:selectOneRadio>
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup style="margin-left: 10px;">
			<h:panelGrid columns="2" columnClasses="ovColText295,ovColText295"
				cellspacing="0">
				<h:column>
				<h:inputText value="#{pc_nbaReinsResponse.tempTableRatingDateOrDuration}" styleClass="formEntryDate" style="width: 110px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}" rendered="#{!pc_nbaReinsResponse.showTableRatingDate}">
					</h:inputText>
				<h:inputText value="#{pc_nbaReinsResponse.tempTableEndDate}"	styleClass="formEntryDate" style="width: 110px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}" rendered="#{pc_nbaReinsResponse.showTableRatingDate}">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>
					<h:outputText value="Years" styleClass="shText" />
				</h:column>
				<h:column>
				<h:inputText value="#{pc_nbaReinsResponse.tempFlatEndDateOrDuration}"	styleClass="formEntryDate" style="width: 110x;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}" rendered="#{!pc_nbaReinsResponse.showFlatRatingDate}">
					</h:inputText>
				<h:inputText value="#{pc_nbaReinsResponse.tempFlatEndDate}"	styleClass="formEntryDate" style="width: 110px;" disabled="#{pc_nbaReinsResponse.setAllDisbaled}" rendered="#{pc_nbaReinsResponse.showFlatRatingDate}">
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>
					<h:outputText value="#{property.recRenYears}" styleClass="shText" />
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup style="margin-left: 10px;">
			<h:panelGrid columns="2" columnClasses="ovColText295,ovColText295"
				cellspacing="0">
				<h:column>

				</h:column>
				<h:column>

				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup styleClass="sectionSubHeader"	style="margin-left: -10px;"><!-- SPR2872 -->
			<h:outputText value="Response History" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup styleClass="ovDivTableHeader">
			<h:panelGrid columns="7"
				columnClasses="ovColHdrText75,ovColHdrText75,ovColHdrText75"
				cellspacing="0" styleClass="ovTableHeader">
				<h:column>
					<h:outputText value="#{property.recRenImage}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="#{property.recRenReceivedTablepanes}" styleClass="shText" />
				</h:column>
				<h:column>
					<h:outputText value="" />
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup id="RensImagedata" styleClass="ovDivTableData"
			style="height: 100px;">
			<h:dataTable id="reinsResponsHistoryTable" cellspacing="0"
				binding="#{pc_nbaReinsResponse.responseImageTable}"
				value="#{pc_nbaReinsResponse.responseImageList}"
				var="reinResponseHis"
				columnClasses="ovColText75,ovColText75,ovColText75"
				styleClass="ovTableData">
				<h:column>
					<h:commandLink value="#{reinResponseHis.sourceType}" />
				</h:column>
				<h:column>
					<h:commandLink value="#{reinResponseHis.receivedDateTime}" />
				</h:column>
				<h:column>
					<h:commandButton id="lockClosed"
						image="images/link_icons/documents.gif"
						onclick="setTargetFrame()" action="#{reinResponseHis.showSelectedImage}"/> <!-- NBA212 -->
				</h:column>
			</h:dataTable>
		</h:panelGroup>
        <h:panelGrid columns="1" style="background:#BED8DE;width:100%">
        	<h:panelGroup styleClass="ovButtonBar">
				<h:commandButton id="reinsViewClose" value="#{property.buttonCancel}"
					action="#{pc_nbaReinsResponse.cancel}" onclick="resetTargetFrame();"
					styleClass="ovButtonLeft" />
				<h:commandButton id="reinsViewClear" value="Clear"
					action="#{pc_nbaReinsResponse.clearForm}"
					onclick="resetTargetFrame();" styleClass="ovButtonLeft-1" />
				<h:commandButton id="reinsViewAddAndAddlInfo" value="#{property.recRenAddNAddlInfo}"
					action="#{pc_nbaReinsResponse.addResponseAdditionalInfo}"
					onclick="resetTargetFrame();" styleClass="ovButtonRight-1" style="width: 100px ; left:430px" /><!-- SPR2872 -->
	
				<h:commandButton id="reinsViewAdd" value="#{property.buttonAdd}"
					action="#{pc_nbaReinsResponse.addResponse}"
					onclick="resetTargetFrame();" styleClass="ovButtonRight" rendered="#{!pc_nbaReinsResponse.commited}" />
	
				<h:commandButton id="reinsViewUpdateOrAdd" value="#{property.buttonUpdate}"
					action="#{pc_nbaReinsResponse.addResponse}"
					onclick="resetTargetFrame();" styleClass="ovButtonRight" rendered="#{pc_nbaReinsResponse.commited}" />
			</h:panelGroup>
		</h:panelGrid>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
