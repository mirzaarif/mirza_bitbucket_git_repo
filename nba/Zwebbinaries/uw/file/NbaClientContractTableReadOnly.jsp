<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="clientContractTitleHeaderUpdate" styleClass="sectionSubheader">
		<h:outputLabel id="clientContractTableTitleUpdate" value="#{property.clientContractTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="clientContractHeaderUpdate" styleClass="ovDivTableHeader">
		<h:panelGrid id="clientContractPanelGrid" columns="6" styleClass="ovTableHeader" columnClasses="ovColHdrText155,ovColHdrText165,ovColHdrText105,ovColHdrText50,ovColHdrText50,ovColHdrText70" cellspacing="0">
			<h:outputLabel id="clientContractHdrCol1Update" value="#{property.clientContractCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol2Update" value="#{property.clientContractCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol3Update" value="#{property.clientContractCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol4Update" value="#{property.clientContractCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol5Update" value="#{property.clientContractCol5}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol6Update" value="#{property.clientContractCol6}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="clientContractTitleHeaderUpdate1" styleClass="ovDivTableData" style="height: 115px;">
		<h:dataTable id="clientContractTableUpdatedDT" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientTableData.htmlDataTable}"
			value="#{pc_NbaClientTableData.rows}" var="nbaClient" rowClasses="#{pc_NbaClientTableData.rowStyles}"
			columnClasses="ovColText155,ovColText165,ovColText105,ovColText50,ovColText50,ovColText70">
			<h:column>
				<h:panelGroup id="panelGroupUpdate" styleClass="ovFullCellSelect" style="width: 200%;">					
					<h:graphicImage id="clientIcon1Update" styleClass="menu" url="images/hierarchies/#{nbaClient.icon1}" rendered="#{nbaClient.icon1Rendered}"/>
					<h:graphicImage id="clientIcon2Update" styleClass="menu" url="images/hierarchies/#{nbaClient.icon2}" rendered="#{nbaClient.icon2Rendered}"/>
					<h:outputText id="clientContractCol1_Update" value="#{nbaClient.col1}" style="vertical-align: top; color: #000000;" />
				</h:panelGroup>
			</h:column>
				<h:column>
				<h:outputText id="clientContractCol2Update" value="#{nbaClient.col2}" styleClass="ovFullCellSelect" />
			</h:column>
			<h:column>
				<h:outputText id="clientContractCol3Update" value="#{nbaClient.col3}" styleClass="ovFullCellSelect" />	
			</h:column>
			<h:column>
				<h:outputText id="clientContractCol4Update" value="#{nbaClient.col4}" styleClass="ovFullCellSelect" />
			</h:column>
			<h:column>
				<h:outputText id="clientContractCol5Update" value="#{nbaClient.col5}" styleClass="ovFullCellSelect" />
			</h:column>
			<h:column>
				<h:outputText id="clientContractCol6Update" value="#{nbaClient.col6}" styleClass="ovFullCellSelect" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
