<%-- CHANGE LOG
Audit Number   Version   Change Description
NBA122            5      Underwriter Workbench Rewrite
SPR2965           6      Underwriter Workbench Scrollbars
SPR3090           6      Table paging support should be generalized for use by any table panes
NBA130            6      Requirements Reinsurance Project
NBA154            6      Requirements Business Function Rewrtie
NBA138            6      Override Requirements Settings Project
NBA158 		      6		 Websphere 6.0 upgrade
NBA192 		      7	  	 Requirement Management Enhancement
NBA213 		   	  7	  	 Unified User Interface
SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implementation
NBA208-36         7      Deferred Work Item retrieval
SPR3684           8      Doctor name is not saved
NBA224            8      nbA Underwriter Workbench Requirements and Impairments Enhancement Project
FNB011 		   NB-1101	 Work Tracking
SPRNBA-478	   NB-1101	 Database Errors Occur when Comments on Requirements Exceed 100 Characters
SPRNBA-579     NB-1301   Requirement Restriction Override Checkbox Enabled for Wrappered in View/Update Requirement
SPRNBA-798     NB-1401   Change JSTL Specification Level
SPRNBA-947	   NB-1601	 Null Pointer Exception May Occur in Inbox if User Selects Different Work
NBA391		   NB-1601	 nbA Capture and Display User ID for Waived Requirements 
--%>

<%@ page language="java" extends="com.csc.fs.accel.ui.nba.NbaBaseJSPServlet" %>  <%-- NBA213 --%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>View/Update Requirements</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="include/common.js"></script> <%-- SPRNBA-478  --%>
	<%-- NBA213 code deleted --%>
	<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaRequirementUpdate.faces'; //nba154
		//NBA213 deleted
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_updateRequirement'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_updateRequirement'].target='';
			return false;
		}
		function setDraftChanges() { //SPRNBA-947
			if (document.forms['form_updateRequirement']['form_updateRequirement:draftChanges']) {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_updateRequirement']['form_updateRequirement:draftChanges'].value;  //NBA213
			} //SPRNBA-947
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- NBA158 --%>
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <%-- SPR2965 --%>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<%-- NBA208-36 code deleted--%>
		<h:form id="form_updateRequirement">
			<f:subview id="tabHeader">
				<c:import url="/uw/subviews/NbaRequirementsNavigation.jsp" />
			</f:subview>

			<f:subview id="reqOverview">
				<c:import url="/uw/subviews/NbaRequirementsTableReadOnly.jsp" /> <%-- SPR3399 --%>
			</f:subview>

			<div id="viewUpdate" class="inputFormMat">
				<div class="inputForm">
					<h:outputLabel id="cnTitle" value="#{property.reqUpdateTitle}" styleClass="formTitleBar" />
					<h:panelGroup id="reqformDataDisplay1" styleClass="formDataDisplayTopLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqType}" styleClass="formLabel" />
						<h:outputText value="#{pc_reqUpdate.typeName}" styleClass="formDisplayText" style="width: 310px" />
						<h:outputLabel value="#{property.reqCreated}" styleClass="formLabel" style="width: 86px" /> <%-- NBA130 --%>
						<h:outputText value="#{pc_reqUpdate.createdDate}" styleClass="formDisplayDate"> <%-- NBA130 --%>
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:outputText>
					</h:panelGroup>
					<h:panelGroup id="reqformDataDisplay2" styleClass="formDataDisplayTopLine" rendered="false" ><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqType}" styleClass="formLabel"/>
						<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_reqUpdate.requirementType}" style="width: 290px"
									valueChangeListener="#{pc_reqUpdate.selectType}" onchange="submit()" immediate="true">
							<f:selectItems value="#{pc_reqUpdate.requirementTypes}" />
						</h:selectOneMenu>
						<h:outputLabel value="#{property.reqCreated}" styleClass="formLabel" style="width: 86px" /> <%-- NBA130 --%>
						<h:inputText styleClass="formEntryDate" value="#{pc_reqUpdate.createdDate}" > <%-- NBA130 --%>
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:inputText>
					</h:panelGroup>
					<h:panelGroup id="reqformDataDisplay3" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqOriginator}" styleClass="formLabel"/>
						<h:outputText value="#{pc_reqUpdate.originator}" styleClass="formDisplayTextFull" />
					</h:panelGroup>
					<h:panelGroup id="reqformDataDisplay4" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqRestrictions}" styleClass="formLabel"/>
						<h:outputText value="#{pc_reqUpdate.restrictionsText}" styleClass="formDisplayText" style="width: 400px" />
						<h:selectBooleanCheckbox value="#{pc_reqUpdate.override}" disabled="#{!pc_reqUpdate.standAlone || pc_reqUpdate.overrideDisabled}"
									valueChangeListener="#{pc_reqUpdate.overrideRestriction}" immediate="true" onclick="submit()" style="position: absolute; top: inherit; left: 528px" /><%-- SPRNBA-579 --%>
						<h:outputLabel value="#{property.reqOverride}" styleClass="formLabelRight" style="position: absolute; top: inherit; margin-top: 5px; left: 545px" />
					</h:panelGroup>

					<hr class="formSeparator" />

					<h:panelGroup id="reqformDataDisplay5" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqDoctor}" styleClass="formLabel"/>
						<%-- begin SPR3684 --%>
						<h:panelGroup id="ruDrDisabled" rendered="#{pc_reqUpdate.updateDisabled}" > 
							<h:outputText id="ruDrDisabledFull" value="#{pc_reqUpdate.drFullName}" styleClass="formDisplayTextFull" /> 
						</h:panelGroup>								
						<h:panelGroup id="ruDrEnabled" rendered="#{!pc_reqUpdate.updateDisabled}" > 
							<%-- begin NBA224 --%>
							<h:selectOneMenu id="doctorDD" value="#{pc_reqUpdate.drPartyId}" style="width: 450px;" onchange="submit()" 
								disabled="#{pc_reqUpdate.disableDoctorDD}" styleClass="entryFieldLong" >
								<f:selectItems value="#{pc_reqimpNav.doctorList}"/>
							</h:selectOneMenu>
							<%-- end NBA224 --%>
						</h:panelGroup>
						<%-- end SPR3684 --%>							
						<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle"
									disabled="#{pc_reqUpdate.updateDisabled}" onclick="setTargetFrame();"
									action="#{pc_reqUpdate.doctor}" immediate="true" />
					</h:panelGroup>
					<%--  begin NBA154 --%>
					<h:panelGroup id="reqformDataDisplay6" styleClass="formDataEntryLine"> <%-- SPR3399 --%>
						<h:outputLabel value="Form Number" styleClass="formLabel"/>
						<h:selectOneMenu id="formNumber" value="#{pc_reqUpdate.formNumber}" binding="#{pc_reqUpdate.formNumberCode}"
										 styleClass="formEntryTextFull" disabled="#{pc_reqUpdate.updateDisabled}">
							<f:selectItems value="#{pc_reqUpdate.requirementFormNumbers}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<%--  end NBA154 --%>
					<h:panelGroup id="reqformDataDisplay7" styleClass="formDataDisplayLine"> <%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqOrderMsg}" styleClass="formLabel" style="vertical-align: top" />
						<h:inputTextarea value="#{pc_reqUpdate.message}" rows="#{pc_reqUpdate.messageRows}"
									disabled="#{pc_reqUpdate.updateDisabled}" styleClass="formEntryTextMultiLineFull" 
									onchange="limitText(this,100);" 
									onkeyup="limitText(this,100);" 			
						/> 	<%-- SPRNBA-478 --%>
					</h:panelGroup>
					<%-- begin NBA138 --%>
					<h:panelGroup id="reqformDataDisplay8" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqVendor}" styleClass="formLabel"/>
						<h:selectOneMenu id="vendor" value="#{pc_reqUpdate.vendor}" binding="#{pc_reqUpdate.vendorCode}" 
						styleClass="formEntryText" disabled="#{pc_reqUpdate.vendorFreqDisabled}">
							<f:selectItems value="#{pc_reqUpdate.vendorNames}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<%-- end NBA138 --%>
					<h:panelGroup id="reqformDataDisplay9" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqDetails}" styleClass="formLabel" style="vertical-align: top" />
						<h:outputText value="#{pc_reqUpdate.details}" styleClass="formDisplayTextFull" />
					</h:panelGroup>
					<%-- begin NBA138 --%>
					<h:panelGroup id="reqformDataDisplay10" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqFollowUpFreq}" styleClass="formLabel"/>
						<h:inputText id="followUpFrequency" value="#{pc_reqUpdate.followUpFreq}" binding="#{pc_reqUpdate.freqHtmlComponent}" 
							maxlength="3" styleClass="formEntryText" style="width: 30px" disabled="#{pc_reqUpdate.vendorFreqDisabled}">
							<f:convertNumber integerOnly="true" type="integer" />
						</h:inputText>
						<h:outputLabel value="#{property.reqFollowUpRequested}" styleClass="formLabel" style="width: 170px"/>
						<h:outputText value="#{pc_reqUpdate.followUp}" styleClass="formDisplayText" />
						<%-- begin NBA192 --%>
						<h:outputLabel value="#{property.reqFollowUpDate}" styleClass="formLabel" style="width: 170px"/>
						<h:outputText value="#{pc_reqUpdate.followUpDate}" styleClass="formDisplayText" > 	
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:outputText>		
						<%-- end NBA192 --%>				
					</h:panelGroup>
					<%-- end NBA138 --%>
					<h:panelGroup id="reqformDataDisplay11" styleClass="formDataDisplayLine"><%-- SPR3399 --%>
						<h:outputLabel value="#{property.reqCrossRef}" styleClass="formLabel" style="vertical-align: top" />
						<h:outputText value="#{pc_reqUpdate.crossReference}" styleClass="formDisplayTextFull" />
					</h:panelGroup>

					<div class="formInputSection">
						<h:panelGrid id="reqformDataDisplay12" columns="2" styleClass="formDataEntryTopLine" columnClasses="formColumnLabel, formColumnData" cellspacing="0" cellpadding="0"><%-- SPR3399 --%>
							<h:column>
								<h:outputLabel value="#{property.reqAction}" styleClass="formLabel" />
							</h:column>
							<h:column>
								<%-- begin NBA192 --%>
								<h:selectOneRadio value="#{pc_reqUpdate.status}" disabled="#{pc_reqUpdate.actionDisabled}"
										valueChangeListener="#{pc_reqUpdate.updateStatusChange}" immediate="true" onclick="submit()"
										layout="lineDirection" styleClass="formEntryText" style="position: relative; left: -5px; width: 400px" >										
										<f:selectItem itemLabel="#{pc_reqUpdate.updateStatuses[0].label}" itemValue="#{pc_reqUpdate.updateStatuses[0].value}"/>
										<f:selectItem itemLabel="#{pc_reqUpdate.updateStatuses[1].label}" itemValue="#{pc_reqUpdate.updateStatuses[1].value}"/>
										<f:selectItem itemLabel="#{pc_reqUpdate.updateStatuses[2].label}" itemValue="#{pc_reqUpdate.updateStatuses[2].value}" itemDisabled="#{pc_reqUpdate.orderedDisabled}"/>
										<f:selectItem itemLabel="#{pc_reqUpdate.updateStatuses[3].label}" itemValue="#{pc_reqUpdate.updateStatuses[3].value}" itemDisabled="#{pc_reqUpdate.followUpDisabled}"/>										
								</h:selectOneRadio>
								<%-- end NBA192 --%>		
							</h:column>
						</h:panelGrid>
						<h:panelGroup id="reqformDataDisplay13" styleClass="formDataEntryLine"><%-- SPR3399 --%>
							<h:outputLabel value="#{property.reqMessage}" styleClass="formLabel" style="vertical-align: top" />
							<h:inputTextarea value="#{pc_reqUpdate.satisfyMessage}" disabled="#{pc_reqUpdate.actionDisabled || pc_reqUpdate.waivedSelected}"
										rows="5" styleClass="formEntryTextMultiLineFull" binding="#{pc_reqUpdate.messageComponent}" /> <%--  SPR3042 --%>
						</h:panelGroup>
						<h:panelGroup id="reqformDataDisplay14" styleClass="formDataEntryLine"><%-- SPR3399 --%>
							<h:selectBooleanCheckbox value="#{pc_reqUpdate.seeComments}" disabled="#{pc_reqUpdate.actionDisabled}"
										style="position: relative; left: 120px" />
							<h:outputLabel value="#{property.reqSeeComments}" styleClass="formLabelRight"
										style="position: relative; left: 115px; margin-top: 3px" />
							<h:inputText value="#{pc_reqUpdate.waivedID}" disabled="#{!pc_reqUpdate.waiveable}" maxlength="8"
										styleClass="formEntryText" style="position: relative; left: 256px; width: 100px" /> <%-- NBA391 --%>
							<h:outputLabel value="#{property.reqWaivedBy}" styleClass="formLabelRight"
											style="position: relative; left: 250px; margin-top: 6px" /> <%-- NBA391 --%>
							
						</h:panelGroup>

						<hr class="formSeparator" />

						<h:panelGroup id="reqformDataDisplay15" styleClass="formDataEntryLineBottom"><%-- SPR3399 --%>
							<h:outputLabel value="#{property.reqReview}" styleClass="formLabel"/>
							<h:selectBooleanCheckbox value="#{pc_reqUpdate.reviewed}" disabled="#{!pc_reqUpdate.reviewable}"
											style="position: relative; top: inherit; left: -5px" />
							<h:outputLabel value="#{property.reqRevComp}" styleClass="formLabelRight"
											style="position: relative; top: inherit; left: -10px; margin-top: 3px" />
							<h:inputText value="#{pc_reqUpdate.reviewDate}" readonly="readonly" disabled="true" styleClass="formEntryDate"
											style="position: relative; left: 35px; width: 100px" >
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>
							<h:outputLabel value="#{property.reqRevDate}" styleClass="formLabelRight"
											style="position: relative; left: 35px; margin-top: 6px" />
							<h:inputText value="#{pc_reqUpdate.reviewID}" disabled="#{!pc_reqUpdate.reviewable}" maxlength="8"
											styleClass="formEntryText" style="position: relative; left: 70px; width: 100px" >
							</h:inputText>
							<h:outputLabel value="#{property.reqReviewer}" styleClass="formLabelRight"
											style="position: relative; left: 70px; margin-top: 6px" />
						</h:panelGroup>
					</div>

					<h:panelGroup id="reqformDataDisplay16" styleClass="formButtonBar"><%-- SPR3399 --%>
						<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" action="#{pc_reqUpdate.cancel}" immediate="true" />
						<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();" action="#{pc_reqUpdate.clear}" />
						<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight" onclick="resetTargetFrame();" 
							disabled="#{pc_reqUpdate.updateButtonDisabled}" 
							action="#{pc_reqUpdate.update}" />	<%-- NBA208-36 --%>
					</h:panelGroup>
					<f:subview id="nbaCommentBar">
						<c:import url="/common/subviews/NbaCommentBar.jsp" />  <%-- SPR3090 --%>
					</f:subview>
				</div>
			</div>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" rendered="#{pc_reqUpdate.UWBench}" /> <%--  NBA154 --%>
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	