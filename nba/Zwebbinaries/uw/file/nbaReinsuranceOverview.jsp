<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR2836           6      General Code Clean Up for JSF Views Added in Version 5 -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA208-32		   7	  Workflow VO Convergence -->
<!-- NBA208-36		   7	  Deferred Work Item Retrieval -->
<!-- SPR1613		   8	  Some Business Functions should be disabled on an Issued Contract  --> 
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->
<!-- SPRNBA-658  NB-1301      Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench-->
<!-- SPR3103   	 NB-1401      Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA353		    NB-1501	  Save Draft Comments when navigating from case to case -->
<!-- NBA356      	NB-1501   Comments Floating View -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		var fileLocationHRef  = '<%=basePath%>' + 'uw/file/nbaReinsuranceOverview.faces'; //SPR3103
		
		//NBA213 deleted
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_reinsuranceOverview'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_reinsuranceOverview'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_reinsuranceOverview']['form_reinsuranceOverview:draftChanges'].value;  //NBA213
		}
        //NBA324 new method
		function launchPHIBriefcaseView() {			        
			phipopup = launchPopup('phipopup', '<%=path%>/common/popup/popupFramesetPHI.html?popup=<%=basePath%>/nbaPHI/file/PHIStatus.faces?readonly=true', 650, 690);
            phipopup.focus();
            top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup = phipopup;		//FNB004 - VEPL1224		
            return false;
		}
		
		//SPRNBA-658 new method
		function draftCommentsPresent() {
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = document.forms['form_reinsuranceOverview']['form_reinsuranceOverview:draftComments'].value;		
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftCommentsOnOtherContract = document.forms['form_reinsuranceOverview']['form_reinsuranceOverview:draftCommentsOnOtherContract'].value;	//NBA353
		} 
		
		//SPRNBA-658 new method
		function initPage(){
			filePageInit();
			setDraftChanges();
			draftCommentsPresent();
		}
		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="initPage();refreshAppFrameExt();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 SPRNBA-658, NBA356-->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />  <!-- NBA213 -->
	<!--  NBA208-32 code deleted -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE_WI" value ="#{pc_reinsuranceWI}" /> <!--  NBA208-36  -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE" value="#{pc_nbaReinsNav}" />
	<h:form id="form_reinsuranceOverview">
		<f:subview id="reinsNav">
			<c:import url="/uw/subviews/NbaReinsuranceNavigation.jsp" />
		</f:subview>
		<f:subview id="reinsTable1">
			<c:import url="/uw/subviews/NbaReinsuranceTable1.jsp" />
		</f:subview>
		<f:subview id="reinTable2">
			<c:import url="/uw/subviews/NbaReinsuranceTable2.jsp" />
		</f:subview>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnCommRefresh" action="#{pc_uwbean.refresh}" value="#{property.buttonRefresh}" styleClass="ovButtonLeft" onclick="setTargetFrame();" />  <!-- SPR2836 , SPR3103 -->
			<h:commandButton id="btnRefer" value="#{property.buttonRefer}" action="#{pc_uwbean.refer}" 
				disabled="#{pc_uwbean.undCommitDisabledForFunctionalUpdates || pc_nbaReinsNav.notLocked || !pc_reinsuranceWI.lockedByMe || pc_uwbean.informalApplication}" immediate="true" 
				styleClass="ovButtonLeft-1"  onclick="setTargetFrame();"/> <!-- NBA186 -->
			<h:commandButton id="btnReviewCmpt" value="#{property.buttonReviewCmpt}" action="#{pc_uwbean.reviewComplete}" 
				rendered="#{pc_uwbean.renderReviewCompleteForAddnlApprovers}" disabled="#{pc_nbaReinsNav.notLocked || !pc_reinsuranceWI.lockedByMe}" 
				immediate="true" styleClass="ovButtonRight-1"  onclick="setTargetFrame();" 
				style="width: 110px;left: 420px" /> <!-- NBA186 -->
			<h:commandButton id="btnCommCommit" onclick="resetTargetFrame();setAppFrameExt();" action="#{pc_nbaReinsNav.commitReinsurance}"
				disabled="#{pc_nbaReinsNav.auth.enablement['Commit'] || pc_nbaReinsNav.notLocked ||	!pc_reinsuranceWI.lockedByMe ||	pc_uwbean.undCommitDisabledForFunctionalUpdates}" 
				value="#{property.buttonCommit}" styleClass="ovButtonRight" /> <!-- SPR2836, NBA213, NBA208-36, SPR1613, NBA186, NBA356 -->
		</h:panelGroup>
		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
		<h:inputHidden id="draftComments" value="#{pc_uwbean.draftCommentsPresent}" />  <!-- SPRNBA-658 -->
		<h:inputHidden id="draftCommentsOnOtherContract" value="#{pc_uwbean.draftCommentsOnOtherContract}" />  <!-- NBA353 -->
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>
