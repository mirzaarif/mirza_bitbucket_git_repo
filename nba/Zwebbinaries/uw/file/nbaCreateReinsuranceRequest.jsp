<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2885           6      Reinsurance Tab - View name is not displayed correctly when a draft reinsurance request is opened for update -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3090           6      Table paging support should be generalized for use by any table panes -->
<!-- NBA158 		   6	  Websphere 6.0 upgrade -->
<!-- NBA212            7      Content Services -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- SPR3398           7      Reinsurance tab - selecting the reinsurance amount causes the UI to display a white page -->
<!-- NBA208-36		   7	  Deferred Work Item Retrieval -->
<!-- SPR2854		   8	  Reinsurance Tab - UI issues in Create Reinsurance Request View and in Send Additional Information View -->
<!-- FNB011 		NB-1101	  Work Tracking -->
<!-- SPR2997  	    NB-1301	  Reinsurance Tab: Incorrect Look and Feel of 'Select all ' and 'Deselect all' Push Buttons-->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Reinsurance Overview</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<!-- NBA213 code deleted -->
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef = '<%=basePath%>' + 'uw/file/nbaCreateReinsuranceRequest.faces';  //NBA213
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_reinsuranceCreateRequest'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_reinsuranceCreateRequest'].target='';
			return false;
		}
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_reinsuranceCreateRequest']['form_reinsuranceCreateRequest:draftChanges'].value;  //NBA213
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<body onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> <!-- SPR2965 -->
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_ALL_SOURCES" value="#{pc_allSourcesWI}" /> <!-- NBA208-36 -->
	<PopulateBean:Load serviceName="RETRIEVE_REINSURANCE" value="#{pc_nbaReinCreateRequest}" />
	<h:form id="form_reinsuranceCreateRequest">
		<h:panelGroup id="createRequestTitle1" styleClass="sectionHeader">
			<h:outputText id="creteRequestTitleOT1" value="#{property.reinCreateTypeHeader}" styleClass="shTextLarge" style="position: absolute; left: 10px"/>	
		</h:panelGroup>
		<h:panelGroup id="createRequestTitle2" styleClass="formDataEntryLine">
			<h:outputText id="creteRequestTitleOT2" value="#{property.reinCreateType}" styleClass="formLabel" />
			<h:selectOneRadio id="reinsRB1" value="#{pc_nbaReinsNav.reinsuranceType}" layout="pageDirection" 
				disabled="#{pc_nbaReinsNav.disableReinsuranceType}" onclick="resetTargetFrame();submit();"	
				valueChangeListener="#{pc_nbaReinCreateRequest.valueChangeRadio}"
				styleClass="formEntryText" style="position: relative; left: 116px; top: -20px;">
				<f:selectItem id="Item1" itemValue="A" itemLabel="#{property.reinCreateTypeAutomatic}"/>
				<f:selectItem id="Item2" itemValue="F" itemLabel="#{property.reinCreateTypeFacultative}" />
				<f:selectItem id="Item3" itemValue="N" itemLabel="#{property.reinCreateTypeNone}"/>
			</h:selectOneRadio>
		</h:panelGroup>
		
		<div id="createNew" class="inputFormMat" style="height: 670px;">
			<div class="inputForm">
				<h:panelGroup id="createRequestDivHeader" styleClass="sectionHeader">
					<h:outputText id="createRequestDivHeaderOT1" value="#{property.reinCreateRequestHeader}" styleClass="shTextLarge" style="position: absolute; left: 10px" rendered="#{pc_nbaReinCreateRequest.renderAdd}"/>	<!--SPR2885 -->
					<h:outputText id="createRequestDivHeaderOT2" value="#{property.reinUpdateRequestHeader}" styleClass="shTextLarge" style="position: absolute; left: 10px" rendered="#{!pc_nbaReinCreateRequest.renderAdd}"/>	<!--SPR2885 -->
				</h:panelGroup>
				<h:panelGroup id="reinsuranceLevelGroup" styleClass="formDataEntryLine">
					<h:outputText id="reinsuranceLevelGroupOT1" value="#{property.reinCreateRequestLevel}" styleClass="formLabel" />
					<h:selectOneRadio id="reinsRB2" value="#{pc_nbaReinCreateRequest.reinLevel}" 
						disabled="#{pc_nbaReinCreateRequest.disableCreateRequest}" 
						layout="lineDirection" styleClass="formEntryText" style="position: relative; left: 116px; top: -20px;">
						<f:selectItem id="Item4" itemValue="Contract" itemLabel="#{property.reinCreateRequestContract}"/>
						<f:selectItem id="Item5" itemValue="Coverage" itemLabel="#{property.reinCreateRequestCoverage}"/>
					</h:selectOneRadio>
				</h:panelGroup>
				<h:panelGroup id="createRequestCoverageHeader" styleClass="formDivTableHeader" >
					<h:panelGrid columns="5" columnClasses="ovColHdrText85,ovColHdrText85,ovColHdrText85,ovColHdrText85,ovColHdrText85"
						cellspacing="0" styleClass="formTableHeader">
						<h:outputText id="createRequestCoverageHeaderOT1" value="#{property.reinCreateRequestCovCol1}" styleClass="ovColSortedFalse"/>
						<h:outputText id="createRequestCoverageHeaderOT2" value="#{property.reinCreateRequestCovCol2}" styleClass="ovColSortedFalse"/>
						<h:outputText id="createRequestCoverageHeaderOT3" value="#{property.reinCreateRequestCovCol3}" styleClass="ovColSortedFalse"/>
						<h:outputText id="createRequestCoverageHeaderOT4" value="#{property.reinCreateRequestCovCol4}" styleClass="ovColSortedFalse"/>
						<h:outputText id="createRequestCoverageHeaderOT5" value="#{property.reinCreateRequestCovCol5}" styleClass="ovColSortedFalse"/>
					</h:panelGrid>
				</h:panelGroup>			
				<h:panelGroup id="createRequestTableData" styleClass="formDivTableData3">			
					<h:dataTable id="reinsCreateReinsuranceRequestTable" cellspacing="0" binding="#{pc_nbaReinCreateRequestTable.coverageTable}"
						 value="#{pc_nbaReinCreateRequestTable.selectedCoverageList}" var="coverage" rowClasses="#{pc_nbaReinCreateRequestTable.requestCoverageRowStyles}"
						columnClasses="ovColText85,ovColText85,ovColText85,ovColText85,ovColText85" styleClass="formTableData" >
						<h:column>
							<h:commandLink id="createRequestCoverageTableDataCL1" value="#{coverage.plan}" styleClass="ovFullCellSelect" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink id="createRequestCoverageTableDataCL2" value="#{coverage.insured}" styleClass="ovFullCellSelect" immediate="true" title="#{coverage.jointInsured}"/>
						</h:column>
						<h:column>
							<h:commandLink id="createRequestCoverageTableDataCL3" value="#{coverage.amount}" styleClass="ovFullCellSelect" immediate="true" />
						</h:column>
						<h:column>
							<h:inputText id="createRequestCoverageTableDataIT1" value="#{coverage.retainedAmount}"  style="width: 200%; height: 100%; vertical-align: center; font-size: 9px; padding: 1px 1px 1px 1px; border: 1px solid;" immediate="true" disabled="#{pc_nbaReinCreateRequest.disableCreateRequest}" 
							valueChangeListener="#{pc_nbaReinCreateRequest.valueChangeReinsuredAmount}" onchange="submit();"><!-- SPR2854 -->
								<f:convertNumber maxFractionDigits="0" type="currency" currencySymbol="#{property.currencySymbol}" /><!-- SPR2854 -->
							</h:inputText>
						</h:column>
						<h:column>
							<h:commandLink id="createRequestCoverageTableDataCL4" value="#{coverage.reinsuredAmount}"  styleClass="ovFullCellSelect"  immediate="true" /><!-- SPR3398 -->
						</h:column>
					</h:dataTable>
				</h:panelGroup>
		
				<hr class="formSeparator"/>	
					
				<h:panelGroup id="createRequestCompanyTable" styleClass="formDataEntryLine">
					<h:outputText id="createRequestCompanyTableOT1" value="#{property.reinCreateRequestCompany}" styleClass="formLabel" style="vertical-align: top;"/>
					<h:panelGroup styleClass="ovDivTableData" style="width:475px; height: 100px;">				
						<h:dataTable id="reinsCreateReinsuranceCompanyTable" binding="#{pc_nbaReinCreateRequestTable.companyTable}" 
							value="#{pc_nbaReinsNav.requestCompanyList}" cellspacing="0" var="company" 
							rowClasses="#{pc_nbaReinCreateRequestTable.requestCompanyRowStyles}" 
							columnClasses="ovColText375" styleClass="ovTableData" style="width:470px;">
							<h:column>
								<h:commandLink value="#{company.companyName}" id="createRequestCompanyTableCL1"
									action="#{pc_nbaReinCreateRequestTable.selectCompanyRows}" 
									rendered="#{!pc_nbaReinCreateRequest.disableCreateRequest}"/>
								<h:outputText id="createRequestCompanyTableOT2" value="#{company.companyName}" rendered="#{pc_nbaReinCreateRequest.disableCreateRequest}"/>
							</h:column>
						</h:dataTable>
					</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup id="createRequestImageTable" styleClass="formDataEntryLine" >
					<h:column>
						<h:panelGrid>
							<h:outputText id="createRequestImageTableOT1" value="#{property.reinCreateRequestImages}" styleClass="formLabel"/>
							<h:commandButton id="reinsViewSelectAll" styleClass="formButtonInterface" value="#{property.reinCreateRequestSelectAll}" action="#{pc_nbaReinCreateRequestTable.selectAllImageRows}" 
								onclick="resetTargetFrame();" style="position: relative; left: 20px; border-style: none;" disabled="#{pc_nbaReinCreateRequest.disableCreateRequest}" />                              <!-- SPR2997 -->
							<h:commandButton id="reinsViewDeSelectAll" styleClass="formButtonInterface" value="#{property.reinCreateRequestDeselectAll}" action="#{pc_nbaReinCreateRequestTable.deselectAllImageRows}"
								onclick="resetTargetFrame();" style="position: relative; left: 20px; border-style: none;" disabled="#{pc_nbaReinCreateRequest.disableCreateRequest}" />                              <!-- SPR2997 -->
						</h:panelGrid>
					</h:column>
					<h:column>
						<h:panelGroup id="createRequestImageTable2"  styleClass="ovDivTableData"  style="position: relative; width:475px; height: 100px; top:-80px; left: 125px;">
							<h:dataTable id="reinsAddlInfoTable2" styleClass="ovTableData" binding="#{pc_nbaReinCreateRequestTable.imageTable}"
								cellspacing="0" value="#{pc_nbaReinsNav.allImageList}" var="imageRow" rowClasses="#{pc_nbaReinCreateRequestTable.requestImagesRowStyles}"
								columnClasses="ovColText350,ovColIcon"  style="width:375px;">
								<h:column>
									<h:commandLink id="createRequestImageTable2CL1" value="#{imageRow.sourceType}" action="#{pc_nbaReinCreateRequestTable.selectImageRows}" rendered="#{!pc_nbaReinCreateRequest.disableCreateRequest}"/>
									<h:outputText id="createRequestImageTable2OT1"  value="#{imageRow.sourceType}" rendered="#{pc_nbaReinCreateRequest.disableCreateRequest}"/>
								</h:column>
								<h:column>
									<h:commandButton id="reinsImageTableCol3a" disabled="#{pc_nbaReinCreateRequest.disableCreateRequest}" 
										image="images/link_icons/documents.gif" onclick="setTargetFrame()"
										styleClass="ovViewIconTrue" action="#{imageRow.showSelectedImage}" immediate="true"/> <!-- NBA212 -->
								</h:column>
							</h:dataTable>
						</h:panelGroup>
					</h:column>
				</h:panelGroup>
				<h:panelGroup id="createRequestMessage" styleClass="formDataEntryLine">		
					<h:outputText id="createRequestMessageOT1" value="#{property.reinCreateRequestMessage}" styleClass="formLabel" style="position: relative; top: -145px"/>
					<h:inputTextarea id="createRequestMessageIN1" value="#{pc_nbaReinCreateRequest.message}" rows="6" styleClass="formEntryTextMultiLineFull" style="position: relative;width: 475px; top: -78px" />
				</h:panelGroup>
				
				<hr class="formSeparator"/>	
				
				<h:panelGroup id="createRequestCommandButtonBar" styleClass="formButtonBar" >
					<h:commandButton id="createRequestCommandButton1" value="Cancel" styleClass="formButtonLeft"  
						onclick="resetTargetFrame();" action="#{pc_nbaReinCreateRequest.cancelReinsuranceRequest}"/>
					<h:commandButton id="createRequestCommandButton2"  value="Clear" styleClass="formButtonLeft-1"  
						onclick="resetTargetFrame();" action="#{pc_nbaReinCreateRequest.clearRequest}" />
					<h:commandButton id="createRequestCommandButton3" value="Add" styleClass="formButtonRight" onclick="resetTargetFrame();" 
						action="#{pc_nbaReinCreateRequest.addReinsuranceRequest}" rendered="#{pc_nbaReinCreateRequest.renderAdd}" />
					<h:commandButton id="createRequestCommandButton4" value="Update" styleClass="formButtonRight" onclick="resetTargetFrame();" 
						action="#{pc_nbaReinCreateRequest.updateReinsuranceRequest}" rendered="#{!pc_nbaReinCreateRequest.renderAdd}" />
					<h:commandButton id="createRequestCommandButton5" value="Update" styleClass="formButtonRight" 
						onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.update}" rendered="false" />
				</h:panelGroup>
			</div>
		</div>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />  <!-- SPR3090 -->
		</f:subview>

		<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
	</h:form>
	<div id="Messages" style="display:none">
		<h:messages />
	</div>
</f:view>
</body>
</html>

