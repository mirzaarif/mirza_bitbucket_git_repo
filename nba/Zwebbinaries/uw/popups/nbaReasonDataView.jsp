<%--CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA223  		NB-1101		Underwriter Final Disposition  --%>
<%-- SPRNBA-605     NB-1301		Spouse Rating Reason Incorrectly Defaulted to Insured Rating Reason --%>
<%-- NBA329			NB-1401   Retain Denied Coverage and Benefit --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- SPRNBA-728     NB-1601   Optional text is repeated on the Final Disposition Reason pop-up --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>			
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />		
		<head>
			<base href="<%=basePath%>">
			<title>
				<%-- begin NBA329 --%>
				<h:outputText id="reasonViewTitle" value="#{property.finalDispReasonTitle}" style="font-size: 12px;" rendered="#{pc_reasonTable.finalDispReason}"></h:outputText>
				<h:outputText id="reasonViewTitle2" value="#{property.ratingReasonTitle}" style="font-size: 12px;" rendered="#{pc_reasonTable.ratingReason}"></h:outputText>
				<h:outputText id="reasonViewTitle3" value="#{property.rateClassReasonTitle}" style="font-size: 12px;" rendered="#{pc_reasonTable.rateClassReason}"></h:outputText>
				<h:outputText id="reasonViewTitle4" value="#{property.denyReasonTitle}" style="font-size: 12px;" rendered="#{pc_reasonTable.denyReason}"></h:outputText>
				<h:outputText id="reasonViewTitle5" value="#{property.prefLvlReasonTitle}" style="font-size: 12px;" rendered="#{pc_reasonTable.prefLvlReason}"></h:outputText>
				<%-- end NBA329 --%>
			</title>
			
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script type="text/javascript" src="javascript/global/nbapopup.js"></script>
			<%-- NBA329 code deleted --%>
			<script language="JavaScript" type="text/javascript">
				var width=600;
				var height=415;  //NBA329
				
				function setTargetFrame() {				
					document.forms['form_reasonView'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {			
					document.forms['form_reasonView'].target= '';
					return false;
				}
			</script>
		</head>
		<body onload="popupInit();">
		<%-- begin NBA329 --%>
		<h:form id="form_reasonView">
			<h:panelGroup id="insuredInfoGP" styleClass="summaryHeader">
				<h:outputText value="#{pc_reason.insuredName}" styleClass="summaryInsured" style="margin-left: 25px" /> <%-- SPRNBA-605 --%>								
				<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
			</h:panelGroup>
			<h:panelGrid id="reasonInformation" columns="2" cellspacing="0" cellpadding="0"
				style="width: 590px; vertical-align: text-bottom" columnClasses="formLabelTable, formEntryText"> 
				<h:outputText id="reasonInformationLabel" value="#{property.reasonTypeLabel}" styleClass="formLabel" style="margin-top: 10px;" />	
				<h:panelGrid id="reasonInfoGrid" columns="2" cellspacing="0" cellpadding="0" border="0" style="width: 430px;"
							columnClasses="formEntryText">
					<h:column>
						<h:selectOneRadio id="reasonType_RB1"  layout="pageDirection" styleClass="radioMenu" 
							  onclick="resetTargetFrame();submit();" value="#{pc_reason.reasonSelectType}" valueChangeListener="#{pc_reason.reasonTypeListener}" style="width: 125px" >							
							<f:selectItem id="selection_SI" itemValue="1" itemLabel="#{property.selectionLabel}"/>							
						</h:selectOneRadio>
						<h:panelGroup id="optionalTextLine">
							<h:outputText id="optionalTextLabel" value="#{property.optionalTextLabel}" styleClass="formLabel" style="width: 150px" />								
						</h:panelGroup>
						<h:selectOneRadio id="reasonType_RB2"  layout="pageDirection" styleClass="radioMenu" 
							  onclick="resetTargetFrame();submit();" value="#{pc_reason.reasonFFType}" valueChangeListener="#{pc_reason.reasonTypeListener}" style="width: 125px" > 
							<f:selectItem id="freeForm_SI" itemValue="2" itemLabel="#{property.freeFormTextLabel}"/>
						</h:selectOneRadio>
						<h:outputText value=" " styleClass="formLabel" style="width: 150px" />
						<h:outputText value=" " styleClass="formLabel" style="width: 150px" />																
					</h:column>					
					<h:column>
						<h:panelGrid id="reasonInfoInputGrid" border="0"  cellspacing="0" columns="1">
							<h:selectOneMenu id="reasonList" style="width: 290px" styleClass="formEntryTextHalf" value="#{pc_reason.selection}"  
							valueChangeListener="#{pc_reason.updateSelectionTextChange}" onchange="submit()" disabled="#{pc_reason.reasonSelectionDisabled}" > <%-- SPRNBA-728 --%>	
								<f:selectItems value="#{pc_reason.selectionList}" />
							</h:selectOneMenu>	
							<h:inputText id="optionalText" style="width: 290px" styleClass="formEntryText" value="#{pc_reason.optionalText}"  disabled="#{pc_reason.optionalSelectionDisabled}">  <%-- SPRNBA-728 --%>	
							</h:inputText>
							<h:inputTextarea  value="#{pc_reason.freeFormText}" rows="3" styleClass="formEntryTextMultiLineFull"  style="width: 290px"  disabled="#{pc_reason.freeFormDisabled}"/>																						
						</h:panelGrid>
					</h:column>
				</h:panelGrid>
			</h:panelGrid>			
			<h:panelGroup styleClass="entryLine">
				<h:commandButton id="delete" value="#{property.buttonDelete}" disabled="#{pc_reasonTable.deleteDisabled}"
								action="#{pc_reasonTable.delete}" immediate="true" onclick="setTargetFrame();" style="margin-left: 35px;" styleClass="buttonLeft" />				
				<h:commandButton id="add" value="#{property.buttonAdd}" 
								action="#{pc_reasonTable.add}"  onclick="resetTargetFrame();" style="margin-right: 15px;" styleClass="buttonRight" />																
			</h:panelGroup>	
			<%-- Reason Table --%>
			<h:panelGrid id="reasonTable_PGrid" columns="1" cellpadding="0" cellspacing="0" style="margin-left: 35px; margin-top: 30px; width: 520px">
				<h:column>	
					<%-- Header for Reason table --%>
					<h:panelGroup id="reasonTableHeader_PGroup" styleClass="formDivTableHeaderRightButtons">
						<h:panelGrid id="reasonTableHeader_PGrid" columns="1" styleClass="formTableHeaderRightButtons" columnClasses="ovColHdrText500" cellspacing="0">
							<h:outputLabel id="reasonCol1Hdr_OL" value="#{property.reasonDataTableLabel}" styleClass="ovColSortedFalse" />							
						</h:panelGrid>
					</h:panelGroup>	
					<h:panelGroup id="reasonTable_PGroup" styleClass="formDivTableDataRightButtons3" style="height: 95px; background-color: #FFFFFF">
						<h:dataTable id="reason_DataTable" styleClass="formTableDataRightButtons" cellspacing="0" binding="#{pc_reasonTable.dataTable}"
									value="#{pc_reasonTable.resultRows}" style="min-height: 5px;"
									var="currRow" columnClasses="ovColText600" rowClasses="#{pc_reasonTable.rowStyles}">
							<h:column>
								<h:panelGroup id="reasonColl_PGroup">
									<h:commandLink id="reasonCol1_Link" action="#{pc_reasonTable.selectForMultipleRows}" immediate="true">
										<h:inputTextarea id="reasonCol1_TA" readonly="true" value="#{currRow.displayReason}" styleClass="ovMultiLine#{currRow.draftText}"
											style="margin-top: 3px;width: 500px" />
									</h:commandLink>
								</h:panelGroup>
							</h:column>										
						</h:dataTable>
					</h:panelGroup>					
				</h:column>				
			</h:panelGrid>
			<h:panelGroup id="overrideRatingReasonGP" styleClass="formDataEntryLine"
							rendered="#{!(pc_reasonTable.finalDispReason || pc_reasonTable.denyReason)}">
				<h:outputText value="#{property.overrideRatingReasonLabel}" styleClass="formLabel" style="margin-top: 15px; width: 200px" />								
				<h:inputText value="#{pc_reasonTable.overrideRatingReason}" styleClass="formEntryText" style="margin-top: 15px; width: 365px" />				
			</h:panelGroup>	
			<h:panelGroup styleClass="buttonBar">
				<h:commandButton id="drCancel" value="#{property.buttonCancel}"
								action="#{pc_reasonTable.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
				<h:commandButton id="drClear" value="#{property.buttonClear}" 
								action="#{pc_reasonTable.clear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
				<h:commandButton id="drUpdate" value="#{property.buttonUpdate}" 
								action="#{pc_reasonTable.update}" onclick="setTargetFrame();" styleClass="buttonRight" />																
			</h:panelGroup>				
		</h:form>
		<%-- end NBA329 --%>
		<div id="Messages" style="display:none"><h:messages/></div>
		</body>
	</f:view>
</html>