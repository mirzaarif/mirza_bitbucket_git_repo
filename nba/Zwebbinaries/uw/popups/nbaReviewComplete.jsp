<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- SPRNBA-626  NB-1301      Credit Card CWA Work Item Not Moved from Credit Card Hold Process on Manual Approval of Application -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<!-- SPRNBA-626 code deleted -->
	<head>
		<base href="<%=basePath%>">
		<title><h:outputText value="#{property.reviewComplete}" /></title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">    
		<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
		<script language="JavaScript" type="text/javascript">
		<!--
			var width = 550;
			var height = 70;
			function setTargetFrame() {
				document.forms['form_reviewComplete'].target='controlFrame';
				return false;
			}
			function resetTargetFrame() {
				document.forms['form_reviewComplete'].target='';
				return false;
			}
		//-->
		</script>
	</head> 
	<body onload="popupInit();">
		<h:form id="form_reviewComplete">
			<h:panelGroup styleClass="buttonBar"  style="padding-top: 1px;">
				<h:commandButton id="reviewCompleteCancel" value="#{property.buttonCancel}" action="#{pc_reviewComplete.cancel}" 
					onclick="setTargetFrame();" styleClass="buttonLeft" />
				<h:commandButton id="reviewCompleteDisagree" value="#{property.buttonDisagree}" action="#{pc_reviewComplete.disagree}" 
					onclick="setTargetFrame();" styleClass="buttonLeft-1"/>
				<h:commandButton id="reviewCompleteAgreeWithChanges" value="#{property.buttonAgreeWithChanges}" action="#{pc_reviewComplete.agreeWithChanges}" 
					onclick="setTargetFrame();" styleClass="buttonRight-1" style="width: 130px"/>
				<h:commandButton id="reviewCompleteAgree" value="#{property.buttonAgree}" action="#{pc_reviewComplete.agree}" 
					onclick="setTargetFrame();" styleClass="buttonRight"/>								
			</h:panelGroup>
		</h:form>
		<div id="Messages"><h:messages /></div>
	</body>
</f:view>
</html>