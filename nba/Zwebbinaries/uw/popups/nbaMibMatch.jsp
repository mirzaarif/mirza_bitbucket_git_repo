<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA122            5      Underwriter Workbench Rewrite --%>
<%-- NBA171            6      nbA Linux re-certification project --%>
<%-- SPR3535		   8      MIB Pop Up Views have White Stripe Behind Push Buttons --%>
<%-- NBA226            8      nba MIB Translation and validation --%>
<%-- SPRNBA-798     NB-1401   Change JSTL Specification Level --%>
<%-- NBA390     	NB-1601   nbA MIB Enhancements --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><%-- NBA171 --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_MIBCHECK_MATCH" value="#{pc_reqMIBMatch}" /><%-- NBA226 --%>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.mibMatchTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=550; //NBA390
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_mibMatch'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_mibMatch'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
			<h:form id="form_mibMatch" >
				<h:panelGroup styleClass="summaryHeader">
					<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
					<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
				</h:panelGroup>
				
				<f:subview id="nbaMIBCheck">
					<c:import url="/uw/subviews/NbaMIBCheck.jsp" />
				</f:subview>

				<h:panelGroup styleClass="displayLine" />
				<h:panelGrid columns="3" styleClass="tableLinePadded" columnClasses="tableColumnLabel,tableColumn150,tableColumn100" cellspacing="0" cellpadding="0">
					<h:column>
						<h:outputLabel value="#{property.mibResolution}" styleClass="entryLabelTop" />
					</h:column>
					<h:column>
						<h:selectOneRadio value="#{pc_reqMIBMatch.resolutionType}" layout="pageDirection" styleClass="radioMenu" disabled="#{pc_reqMIBMatch.matched}"><%-- NBA390 --%>
							<f:selectItems value="#{pc_reqMIBMatch.resolutionTypes}"/>
						</h:selectOneRadio>
					</h:column>
					<h:column>
					</h:column>
				</h:panelGrid>
				<%--begin NBA390 --%>
				<hr class="formSeparator" />
				<f:subview id="nbaMIBAltNameInfo">
					<c:import url="/uw/subviews/NbaMIBAltNameInfo.jsp" />
				</f:subview>
				<%--end NBA390 --%>
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="checkCancel" value="#{property.buttonCancel}"
									action="#{pc_reqMIBMatch.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="checkAdd" value="#{property.buttonUpdate}"
									action="#{pc_reqMIBMatch.update}" onclick="setTargetFrame();" styleClass="buttonRight" disabled="#{pc_reqMIBMatch.matched}"/><%-- NBA390 --%>
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display:none"><h:messages /></div><%-- SPR3535 --%>
		</body>
	</f:view>
</html>
	