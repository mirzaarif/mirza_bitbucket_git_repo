<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText id="prfPopUpTitle" value="Preferred Profile" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>

	<script language="JavaScript" type="text/javascript">
		var width=615;
		var height=800;
		function setTargetFrame() {
			document.forms['form_prefProfileDetails'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
		//alert('Resetting Target Frame');
			document.forms['form_prefProfileDetails'].target='';
			return false;
		}				
	</script>

	</head>
	<body onload="popupInit();" style="background-color: #BED8DE;">
	<PopulateBean:Load serviceName="RETRIEVE_CLIENT_PREFPPROFILE"
		value="#{pc_nbaClientPrefProfile}" />
	<h:form id="form_prefProfileDetails">
		<!-- Table Column Headers -->
		<h:panelGroup id="prfPGroup1"  style="#{pc_nbaClientPrefProfile.mainGroupStyle}">
			<h:panelGroup id="prfPGroup2" styleClass="sectionHeader " style="align:center;width:100%;height: 35px">
				<h:outputText id="prfOpText1" value="#{pc_nbaClientPrefProfile.insuredName}" styleClass="shTextLarge" style="position: absolute; left 100px" />
				<h:outputText id="prfOpText2" value="#{pc_nbaClientPrefProfile.insuredGender}" styleClass="shText" style="position: absolute; left: 320px" />
				<h:outputText id="prfOpText3" value="#{property.clientPrfProfileAge}" styleClass="shText" style="position: absolute; left: 365px" />
				<h:outputText id="prfOpText4" value="#{pc_nbaClientPrefProfile.insuredAge}" styleClass="shText" style="position: absolute; left: 395px" />
				<h:outputText id="prfOpText5" value="#{pc_nbaClientPrefProfile.insuredPreference}" styleClass="shText" style="position: absolute; left: 425px" />
			</h:panelGroup>

			<h:panelGroup id="prfPGroup3" styleClass="sectionSubheader" style="width:600px">
				<h:outputLabel id="prefProfileTitle1" value=" " styleClass="shTextLarge" />
			</h:panelGroup>

			<h:panelGrid id="prefGenCrtTab" styleClass="ovTableHeader" style="width: 600px;" cellspacing="0" columns="2" columnClasses="ovColHdrText375,ovColHdrText225"
				rendered="#{pc_nbaClientPrefProfile.genRendered}">
				<h:panelGrid id="genralCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;vertical-align: top;" cellspacing="0"
					rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.genCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup4" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.genStyle}">
					<h:panelGrid id="genCrtPrfLevelDataPanel" styleClass="ovTableData" cellspacing="0" rowClasses="#{pc_nbaClientPrefProfile.rowStyles}"
						binding="#{pc_nbaClientPrefProfile.genCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid>

			<h:panelGroup id="prfPGroup5" styleClass="sectionSubheader" style="text-indent: -10px; width:610px">
				<h:outputLabel id="prefProfileTitle2" value="#{property.clientPrfProfileFamilyTitle}" styleClass="shTextLarge" style="text-indent: 20px;" />
			</h:panelGroup>
			<h:panelGrid id="prefFlmCrtTab" styleClass="ovTableHeader" 	style="width: 600px;" cellspacing="0" columns="2" columnClasses="ovColHdrText375,ovColHdrText225"
				rendered="#{pc_nbaClientPrefProfile.flmRendered}">
				<h:panelGrid id="familyCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;" cellspacing="0"
					rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.flmCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup6" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.flmStyle}">
					<h:panelGrid id="flmCrtPrfLevelDataPanel" styleClass="ovTableData" cellspacing="0" rowClasses="#{pc_nbaClientPrefProfile.rowStyles}"
						binding="#{pc_nbaClientPrefProfile.flmCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid>

			<h:panelGroup id="prfPGroup7" styleClass="sectionSubheader" style="text-indent: -10px;width:610px">
				<h:outputLabel id="prefProfileTitle3" value=" " styleClass="shTextLarge" />
			</h:panelGroup>
			<h:panelGrid id="prefHgtWgtCrtTab" styleClass="ovTableHeader"
				style="width: 600px;" cellspacing="0" columns="2" columnClasses="ovColHdrText375,ovColHdrText225" rendered="#{pc_nbaClientPrefProfile.hgtWgtRendered}">
				<h:panelGrid id="hgtWgtCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;" cellspacing="0"
					rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.hgtWgtCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup8" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.hgtWgtStyle}">
					<h:panelGrid id="hgtWgtCrtPrfLevelDataPanel" styleClass="ovTableData" cellspacing="0"
						rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.hgtWgtCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid>


			<h:panelGroup id="prfPGroup9" styleClass="sectionSubheader" style="text-indent: -10px;width:610px">
				<h:outputLabel id="prefProfileTitle4" value=" " styleClass="shTextLarge"  />
			</h:panelGroup>
			<h:panelGrid id="prefImpCrtTab" styleClass="ovTableHeader" style="width: 600px;" cellspacing="0" columns="2"
				columnClasses="ovColHdrText375,ovColHdrText225" rendered="#{pc_nbaClientPrefProfile.impRendered}">
				<h:panelGrid id="impCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;" cellspacing="0" 
				rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.impCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup10" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.impStyle}">
					<h:panelGrid id="impCrtPrfLevelDataPanel" styleClass="ovTableData" cellspacing="0" rowClasses="#{pc_nbaClientPrefProfile.rowStyles}"
						binding="#{pc_nbaClientPrefProfile.impCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid>

			<h:panelGroup id="prfPGroup11" styleClass="sectionSubheader" style="text-indent: -10px; width:610px">
				<h:outputLabel id="prefProfileTitle5" value="#{property.clientPrfProfileLabTitle}" styleClass="shTextLarge" style="text-indent: 20px;" />
			</h:panelGroup>
			<h:panelGrid id="prefLabCrtTab" styleClass="ovTableHeader" style="width: 600px;" cellspacing="0" columns="2" 
				columnClasses="ovColHdrText375,ovColHdrText225" rendered="#{pc_nbaClientPrefProfile.labRendered}">
				<h:panelGrid id="labCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;" cellspacing="0" 
					rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.labCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup12" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.labStyle}">
					<h:panelGrid id="labCrtPrfLevelDataPanel" styleClass="ovTableData" cellspacing="0" rowClasses="#{pc_nbaClientPrefProfile.rowStyles}"
						binding="#{pc_nbaClientPrefProfile.labCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid>

			<h:panelGroup id="prfPGroup13" styleClass="sectionSubheader" style="text-indent: -10px;width:610px">
				<h:outputLabel id="prefProfileTitle6" value=" " styleClass="shTextLarge" />
			</h:panelGroup>
			<h:panelGrid id="prefSmkCrtTab" styleClass="ovTableHeader" style="width: 600px;" cellspacing="0" columns="2" 
					columnClasses="ovColHdrText375,ovColHdrText225" rendered="#{pc_nbaClientPrefProfile.smkRendered}">
				<h:panelGrid id="smkCriterialTypeDataTable" styleClass="ovTableData" style="width: 375px;" cellspacing="0"
					rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.smkCrtTypeDataPanel}" />
				<h:panelGroup id="prfPGroup14" styleClass="ovDivTableData" style="#{pc_nbaClientPrefProfile.smkStyle}">
					<h:panelGrid id="smkCrtPrfLevelDataPanel" styleClass="ovTableData"
						cellspacing="0" rowClasses="#{pc_nbaClientPrefProfile.rowStyles}" binding="#{pc_nbaClientPrefProfile.smkCrtPrfLevelDataPanel}" />
				</h:panelGroup>
			</h:panelGrid> 
		</h:panelGroup>
		<h:panelGroup id="prfPGroup18"  style="#{pc_nbaClientPrefProfile.legendGroupStyle}">
			<h:panelGroup id="prfPGroup15" styleClass="sectionSubheader" style="text-indent: -10px; width:624px" rendered="#{pc_nbaClientPrefProfile.legendRendered}">
				<h:outputLabel id="prefProfileTitle7" value="#{property.clientPrfProfileLegendInfoTitle}" styleClass="shTextLarge" style="text-indent: 20px;" />
			</h:panelGroup>
			<h:panelGrid id="prfLegTable" columns="2" style="background-color: #BED8DE;" columnClasses="ovPrfLeg150,ovPrfLeg150" rendered="#{pc_nbaClientPrefProfile.legendRendered}">
				<h:dataTable id="prfLevelTable" cellspacing="0" binding="#{pc_nbaClientPrefProfile.legendInfoPrfLevelDataTable}"
					value="#{pc_nbaClientPrefProfile.legendInfoPrfLevel}" var="prfLevel" style="vertical-align: top;width:200;">
					<h:column id="prefColumn1">
						<h:outputLabel id="legCol1" value="#{prfLevel.legInfoPrfLevel}" style="font-family: Verdana;font-size: 11px;width:50" />
					</h:column>
					<h:column id="prefColumn2">
						<h:outputLabel id="legCol3" value="#{prfLevel.legInfoPrfLevelTrnsVal}" style="	font-family: Verdana;font-size: 11px;width:150" />
					</h:column>
				</h:dataTable>
				<h:panelGrid id="legendDataPanel" cellspacing="0" binding="#{pc_nbaClientPrefProfile.legendInfoPrfAnsDataTable}"
					style="background-color: #BED8DE;width:350;" />
			</h:panelGrid>			
		</h:panelGroup>			
		<h:panelGroup id="prfPGroup17" styleClass="formButtonBar" >  
			<h:commandButton id="prfCb1" value="#{property.buttonClose}" action="#{pc_nbaClientPrefProfile.cancel}" onclick="setTargetFrame();" styleClass="formButtonRight"/>
		</h:panelGroup>			
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
	</body>
</html>
</f:view>
<!--  NBA171 code deleted -->

