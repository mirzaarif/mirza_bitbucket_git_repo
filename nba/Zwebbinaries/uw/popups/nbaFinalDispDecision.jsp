<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223        NB-1101      Underwriter Final Disposition -->
<!-- SPRNBA-576    NB-1301      Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-606    NB-1301      Final Disposition Decision Popup Does Not Follow UI Standards -->
<!-- SPRNBA-798    NB-1401      Change JSTL Specification Level -->


<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.finalDispDecisionTitle}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>	
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
		var width=620;
		var height=445;
		function setTargetFrame() {
			document.forms['form_finalDispDecision'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
		//alert('Resetting Target Frame');
			document.forms['form_finalDispDecision'].target='';
			return false;
		}				
	</script>
	
	</head>
	<body onload="popupInit();">	<!-- SPRNBA-606 -->	
		<h:form id="form_finalDispDecision">
			<h:panelGroup id="insuredInfoGP" styleClass="summaryHeader">
				<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" style="margin-left: 50px;width: 200px" />						
	
			
				<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" style="width: 200px" />
			</h:panelGroup>
			<!-- Tentative disposition table -->
			<h:panelGroup id="tentativeDispTitleHeader" styleClass="sectionSubheader" style="margin-left: 10px; width: 595px">
				<h:outputText id="tentativeDispTableTitle" value="#{property.uwfinalDispTentDispTitle}" styleClass="shTextLarge" />
			</h:panelGroup>
			<f:subview id="tentDispTable">
				<c:import url="/uw/subviews/NbaTentativeDispositionTable.jsp" />
			</f:subview>
			<!-- Initial decision table -->
			<h:panelGroup id="decisionTitleHeader" styleClass="sectionSubheader" style="margin-left: 10px; margin-top: 10px; width: 595px">
				<h:outputText id="decisionTableTitle" value="#{property.uwfinalDispInitDecTitle}" styleClass="shTextLarge" />
			</h:panelGroup>
			<f:subview id="initialDecisionTable">
				<c:import url="/uw/subviews/NbaInitialDecisionTable.jsp" />
			</f:subview>	
			<h:panelGroup styleClass="buttonBar">
			<h:commandButton id="drCancel" value="#{property.buttonCancel}"
							action="#{pc_finalDispNavigation.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />			<!-- SPRNBA-576 -->												
		    </h:panelGroup>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
	</body>
</html>
</f:view>