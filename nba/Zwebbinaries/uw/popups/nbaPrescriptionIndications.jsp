<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA309	 		NB-1301   Pharmaceutical Information -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.titleDiseaseDescriptions}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=340; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_prescription_indications'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_prescription_indications'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">			
			<h:form id="form_prescription_indications">				 
				<div class="inputFormMat">
					<h:panelGroup styleClass="summaryHeader">
						<h:outputText id="prescriptionInsuredNameLabel" value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
						<h:outputText id="prescriptionInsuredNameValue" value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />  
					</h:panelGroup> 
					<h:panelGroup styleClass="entryLine">
						<h:outputText id="prescriptionNameLabel" value="#{property.prescription}" styleClass="formLabel" />
						<h:outputText id="prescriptionNameValue" value="#{pc_reqPrescriptionInformation.selectedPrescription.prescriptionLabel}" styleClass="formDisplayText" />  
					</h:panelGroup> 						
					<h:panelGroup styleClass="entryLine" >
						<h:outputLabel id="prescriptionDiseaseDescriptionLabel" value="#{property.prescriptionDiseaseDescriptions}" styleClass="formLabel" style="vertical-align: top;"/>
						<h:inputTextarea id="prescriptionDiseaseDescriptionValue"value="#{pc_reqPrescriptionInformation.selectedPrescription.diseaseDescriptionsListDisplay}" styleClass="formDisplayText" style="width: 400px; height: 220px; background-color: #CED6E5;" />							 
					</h:panelGroup> 												
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="drCancel" value="#{property.buttonCancel}" action="#{pc_reqDoctor.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					</h:panelGroup>
				</div>				 
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
		</body>
	</f:view>
</html>
	