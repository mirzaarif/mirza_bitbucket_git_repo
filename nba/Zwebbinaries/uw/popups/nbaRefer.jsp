<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>	
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.refer}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width = 450;
				var height = 100;
				function setTargetFrame() {					
					document.forms['form_refer'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {					
					document.forms['form_refer'].target='';
					return false;
				}
			//-->
			</script>
		</head> 
		<body onload="popupInit();">
			<h:form id="form_refer">
				<h:panelGroup styleClass="formDataEntryLine">
					<h:outputLabel id="uwqueue"  value="#{property.uwqueue}" styleClass="entryLabel" style="width:150px;"/>
					<h:selectOneMenu id="queueList" style="width:250px;" value="#{pc_refer.selectedQueue}" immediate="false" styleClass="formEntryText">
						<f:selectItems value="#{pc_refer.queueList}"/>
					</h:selectOneMenu>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine" style="height: 25px;" >
					<h:outputLabel id="priority" value="#{property.priority}" styleClass="entryLabel"  style="width:150px;"/>
					<h:selectOneMenu id="priorityList" style="width:250px;" value="#{pc_refer.priority}" immediate="false" styleClass="formEntryText">
						<f:selectItems value="#{pc_refer.priorityList}"/>
					</h:selectOneMenu>					
				</h:panelGroup>			
				<h:panelGroup styleClass="buttonBar" style="height: 40px;" >
					<h:commandButton id="referCancel" value="#{property.buttonCancel}"
									action="#{pc_refer.cancel}" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="referOk" value="#{property.buttonOK}"
									action="#{pc_refer.update}" onclick="setTargetFrame();" styleClass="buttonRight"/>
				</h:panelGroup>
			</h:form>
			<div id="Messages"><h:messages /></div>
		</body>
	</f:view>
</html>