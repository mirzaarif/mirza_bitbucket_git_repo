<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- NBA130            6      Requirements Reinsurance Project -->
<!-- SPR3535           8      Doctor Pop Up View has a White Stripe Behind Push Buttons -->
<!-- NBA224            8      nbA Underwriter Workbench Requirements and Impairments Enhancement Project -->
<!-- SPRNBA-493 	NB-1101   Standalone and Wrappered Adapters Should Be Changed to Send Dash in Zip Code Greater Than 5 Position -->
<!-- SPRNBA-617 	NB-1301   Doctor Last Name field allowing too many characters -->
<!-- SPRNBA-693 	NB-1301   Improve Alignment of the Last Name Entry Field on the Doctor Pop Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.drTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=350; //NBA130
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_doctor'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_doctor'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
			<PopulateBean:Load serviceName="RETRIEVE_DOCTOR" value="#{pc_reqDoctor}" />
			<h:form id="form_doctor" >
				<h:panelGroup styleClass="summaryHeader">
					<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
					<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLine"> <!--NBA130-->
					<h:outputLabel id="oldrName" value="#{property.drName}" styleClass="entryLabel" />
					<h:inputText id="itdrFirstName" value="#{pc_reqDoctor.drFirstName}" styleClass="entryFieldShort" style="width: 190px"  /> <!--NBA130--> <!--SPRNBA-693-->
					<h:inputText id="itdrMI" value="#{pc_reqDoctor.drMiddleName}" styleClass="entryFieldSingleChar"  /> <!--NBA130-->
					<h:inputText id="itdrLastName" value="#{pc_reqDoctor.drLastName}" styleClass="entryFieldName" style="width: 190px" maxlength="#{pc_reqDoctor.maxDrLastNameLength}" /> <!--NBA130 SPRNBA-617--> <!--SPRNBA-693-->				
				</h:panelGroup>
				<h:panelGroup styleClass="entryLine">
					<h:outputLabel id="oldrAddr" value="#{property.drAddress}" styleClass="entryLabel" />
					<h:inputText id="itdrAddrLine1" value="#{pc_reqDoctor.drAddrLine1}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePaddedNoLabel">
					<h:inputText id="itdrAddrLine2" value="#{pc_reqDoctor.drAddrLine2}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="oldrAddrCity" value="#{property.drCity}" styleClass="entryLabel" />
					<h:inputText id="itdrAddrCity" value="#{pc_reqDoctor.drAddrCity}" styleClass="entryFieldLong" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="oldrState" value="#{property.drState}" styleClass="entryLabel" />
					<h:selectOneMenu id="itdrState" value="#{pc_reqDoctor.drAddrState}" styleClass="entryFieldLong" >
						<f:selectItems value="#{pc_reqDoctor.states}"/>
					</h:selectOneMenu>
				</h:panelGroup>

				<h:panelGrid columns="3" styleClass="tableLinePadded" columnClasses="tableColumnLabel,tableColumn100,tableColumn150" cellspacing="0" cellpadding="0">
					<h:column>
						<h:outputLabel id="oldrZipCode" value="#{property.drZipCode}" styleClass="entryLabelTop" />
					</h:column>
					<h:column>
						<h:selectOneRadio id="sordrZipCode" value="#{pc_reqDoctor.drAddrZipTC}" layout="pageDirection" styleClass="radioMenu">
							<f:selectItems value="#{pc_reqDoctor.zipTypes}"/>
						</h:selectOneRadio>
					</h:column>
					<h:column>
						<h:panelGroup>
							<h:inputText id="itdrZipCode" value="#{pc_reqDoctor.drAddrZip}" styleClass="entryFieldShort" > <!-- SPRNBA-493 -->
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}"/> <!-- SPRNBA-493 -->
							</h:inputText> <!-- SPRNBA-493 -->
							<h:inputText id="itdrPostalCode" value="#{pc_reqDoctor.drAddrPostal}" styleClass="entryFieldShortRadioPage" />
						</h:panelGroup>
					</h:column>
				</h:panelGrid>

				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel id="oldrPhone" value="#{property.drPhone}" styleClass="entryLabel" />
					<h:inputText id="itdrAreaCode" value="#{pc_reqDoctor.drPhoneAreaCode}" styleClass="entryFieldPhoneAreaCode" /> <!--NBA130-->
					<h:inputText id="itdrPhone" value="#{pc_reqDoctor.drPhoneDialNumber}" styleClass="entryFieldPhoneDialNumber" /> <!--NBA130-->
				</h:panelGroup>
				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="drCancel" value="#{property.buttonCancel}"
									action="#{pc_reqDoctor.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" /> <!-- SPRNBA-493 -->
					<h:commandButton id="drClear" value="#{property.buttonClear}"
									action="#{pc_reqDoctor.clear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
					<h:commandButton id="drUpdate" value="#{property.buttonUpdate}"
									rendered="#{!pc_reqDoctor.renderAdd}" action="#{pc_reqDoctor.add}" onclick="setTargetFrame();" styleClass="buttonRight"/> <!--NBA224-->
					<h:commandButton id="drAdd" value="#{property.buttonAdd}"
									rendered="#{pc_reqDoctor.renderAdd}" action="#{pc_reqDoctor.add}" onclick="setTargetFrame();" styleClass="buttonRight"/> <!--NBA224-->
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div><!-- SPR3535 -->
		</body>
	</f:view>
</html>
	