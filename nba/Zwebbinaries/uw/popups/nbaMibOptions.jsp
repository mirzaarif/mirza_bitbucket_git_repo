<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3535		   8      MIB Pop Up Views have White Stripe Behind Push Buttons -->
<!-- NBA226            8      nba MIB Translation and validation -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_MIB_OPTIONNOTE" value="#{pc_reqMIBNote}" /><!-- NBA226 -->
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.mibOptionsTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=280;
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_mibOptions'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_mibOptions'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
			<h:form id="form_mibOptions" >
				<h:panelGroup styleClass="summaryHeader">
					<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
					<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
				</h:panelGroup>
				
				<h:panelGrid columns="3" styleClass="tableLinePadded" columnClasses="tableColumnLabel,tableColumn195,tableColumn150" cellspacing="0" cellpadding="0">
					<h:column>
						<h:outputLabel value="#{property.mibMisc}" styleClass="entryLabelTop" />
					</h:column>
					<h:column>
						<h:selectOneRadio value="#{pc_reqMIBNote.altMiscIndicator}" layout="pageDirection" styleClass="radioMenu">
							<f:selectItems value="#{pc_reqMIBNote.types}" />
						</h:selectOneRadio>
					</h:column>
					<h:column>
						<h:panelGroup>
							<h:inputText value="#{pc_reqMIBNote.submitDate}" styleClass="entryFieldDate" style="width: 100px" >
								<f:convertDateTime pattern="#{property.datePattern}"/>
							</h:inputText>
						</h:panelGroup>
					</h:column>
				</h:panelGrid>

				<h:panelGroup styleClass="promptLine">
					<h:outputLabel value="#{property.mibFirstName}" styleClass="promptLabel_FirstName" />
					<h:outputLabel value="#{property.mibMiddleInit}" styleClass="promptLabel_MiddleInit" />
					<h:outputLabel value="#{property.mibLastName}" styleClass="promptLabel_LastName" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel value="#{property.mibSearchName}" styleClass="entryLabel" />
					<h:inputText value="#{pc_reqMIBNote.firstName}" styleClass="entryFieldName" />
					<h:inputText value="#{pc_reqMIBNote.middleInitial}" styleClass="entryFieldSingleCharNext" />
					<h:inputText value="#{pc_reqMIBNote.lastName}" styleClass="entryFieldNameNext" />
				</h:panelGroup>
				<h:panelGroup styleClass="entryLinePadded">
					<h:outputLabel value="#{property.mibBirthDate}" styleClass="entryLabel" />
					<h:inputText value="#{pc_reqMIBNote.birthDate}" styleClass="entryFieldDate" style="width: 100px" >
						<f:convertDateTime pattern="#{property.datePattern}"/>
					</h:inputText>
					<h:outputLabel value="#{property.mibBirthState}" styleClass="entryLabelRightOfDate" />
					<h:selectOneMenu value="#{pc_reqMIBNote.birthState}" styleClass="entryFieldName" >
						<f:selectItems value="#{pc_reqMIBNote.states}" />
					</h:selectOneMenu>
				</h:panelGroup>

				<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="buttonCancel" value="#{property.buttonCancel}"
									action="#{pc_reqMIBNote.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
					<h:commandButton id="buttonClear" value="#{property.buttonClear}"
									action="#{pc_reqMIBNote.clear}" onclick="resetTargetFrame();" styleClass="buttonLeft-1" />
					<h:commandButton id="buttonAddNew"  value="#{property.buttonAddNew}"
									action="#{pc_reqMIBNote.addNew}" onclick="resetTargetFrame();" styleClass="buttonRight-1" />
					<h:commandButton id="buttonAdd" value="#{property.buttonAdd}"
									action="#{pc_reqMIBNote.add}" onclick="setTargetFrame();" styleClass="buttonRight" />
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display:none"><h:messages /></div><!-- SPR3535 -->
		</body>
	</f:view>
</html>
