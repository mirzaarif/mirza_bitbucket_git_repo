<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA309	 		NB-1301   Pharmaceutical Information -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.titlePharmacy}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=550;
				var height=250; 
				function setTargetFrame() {
					//alert('Setting Target Frame');
					document.forms['form_prescription_pharmacy'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					//alert('Resetting Target Frame');
					document.forms['form_prescription_pharmacy'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
			<PopulateBean:Load serviceName="RETRIEVE_PHARMACEUTICAL_PHARMACY" value="#{pc_reqDoctor}" />
			<h:form id="form_prescription_pharmacy">
				<div class="inputFormMat">
					<div class="inputForm">
						<h:panelGroup styleClass="summaryHeader">
							<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
							<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drNameLabel" value="#{property.drName}" styleClass="formLabel" />
							<h:outputText id="drNameValue" value="#{pc_reqDoctor.fullName}" styleClass="formDisplayText" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drAddrLine1Lable" value="#{property.drAddress}" styleClass="formLabel" />
							<h:outputText id="drAddrLine1Value" value="#{pc_reqDoctor.drAddrLine1}" styleClass="formDisplayText" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLinePaddedNoLabel" rendered="#{pc_reqDoctor.renderAddressLine2}">
							<h:outputText id="drAddrLine2Value" value="#{pc_reqDoctor.drAddrLine2}" styleClass="formDisplayText"  />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drAddrCityLabel" value="#{property.drCity}" styleClass="formLabel" />
							<h:outputText id="drAddrCityValue" value="#{pc_reqDoctor.drAddrCity}" styleClass="formDisplayText" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drStateLabel" value="#{property.drState}" styleClass="formLabel" />
							<h:outputText id="drStateValue" value="#{pc_reqDoctor.drAddrState}" styleClass="formDisplayText" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drZipCodeLabel" value="#{property.prescriptionZipCode}" styleClass="formLabel" rendered="#{pc_reqDoctor.codeZip}" />
							<h:outputText id="drZipCodeValue" value="#{pc_reqDoctor.drAddrZip}" styleClass="formDisplayText" rendered="#{pc_reqDoctor.codeZip}">
								<f:convertNumber type="zip" integerOnly="true" pattern="#{property.zipPattern}" />
							</h:outputText>
							<h:outputLabel id="drPostalCodeLabel" value="#{property.prescriptionPostalCode}" styleClass="formLabel" rendered="#{!pc_reqDoctor.codeZip}" />
							<h:outputText id="drPostalCodeValue" value="#{pc_reqDoctor.drAddrPostal}" styleClass="formDisplayText" rendered="#{!pc_reqDoctor.codeZip}" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drPhoneLabel" value="#{property.drPhone}" styleClass="formLabel" />							
							<h:outputText id="drPhoneValue" value="#{pc_reqDoctor.drPhoneDialNumber}" styleClass="formDisplayText" />
						</h:panelGroup> 
						<h:panelGroup styleClass="entryLine">
							<h:outputLabel id="drProviderNumberLabel" value="#{property.prescriptionProviderNumber}" styleClass="formLabel" />
							<h:outputText id="drProviderNumberValue" value="#{pc_reqDoctor.providerNumber}" styleClass="formDisplayText" />
						</h:panelGroup>  												
						<h:panelGroup styleClass="formButtonBar">
							<h:commandButton id="drCancel" value="#{property.buttonCancel}" action="#{pc_reqDoctor.cancel}" immediate="true" onclick="setTargetFrame();" styleClass="buttonLeft" />
						</h:panelGroup>
					</div>
				</div>
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
		</body>
	</f:view>
</html>
	