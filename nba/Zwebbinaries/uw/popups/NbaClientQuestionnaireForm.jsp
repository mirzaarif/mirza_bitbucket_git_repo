<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      	Underwriter Workbench Rewrite -->
<!-- NBA171            6      	nbA Linux re-certification project -->
<!-- FNB011 		NB-1101		Work Tracking -->
<!-- NBA245 		NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798     NB-1401     Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.questionnaireTitle}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>	
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var width=620;
		var height=245;
		function setTargetFrame() {
			document.forms['form_aviationQuationnaire'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
		//alert('Resetting Target Frame');
			document.forms['form_aviationQuationnaire'].target='';
			return false;
		}				
	</script>
	
	</head>
	<body onload="popupInit();" style="background-color: white;">
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientAviationQuestionnaire}" /> <!-- NBA245 -->
		<h:form id="form_aviationQuationnaire">
			<f:subview id="clientQuationnaireView" rendered="#{!pc_NbaClientAviationQuestionnaire.updateMode}">
				<c:import url="/uw/subviews/NbaClientQuestionnaireDetails.jsp"/>
			</f:subview>	
			<f:subview id="clientQuationnaireUpdate" rendered="#{pc_NbaClientAviationQuestionnaire.updateMode}">
				<c:import url="/uw/subviews/NbaClientQuestionnaireUpdate.jsp"/>
			</f:subview>			
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
	</body>
</html>
</f:view>
<!-- NBA171 code deleted -->
