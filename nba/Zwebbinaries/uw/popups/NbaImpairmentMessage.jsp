<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- SPR3232           6      Final Disposition view is showing horizontal scroll bars -->
<!-- SPR3535           8      Doctor Pop Up View has a White Stripe Behind Push Buttons -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<head>
	<base href="<%=basePath%>">
	<title><h:outputText value="#{property.impMessageTitle}" /></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
	<script language="JavaScript" type="text/javascript">
		var width=550;
		var height=330;
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_impMessage'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_impMessage'].target='';
			return false;
		}
		 
	</script>
	</head>
	<body onload="popupInit();">
	<PopulateBean:Load serviceName="ADD_IMPAIRMENT_MESSAGE" value="#{pc_impMessage}" />
	<h:form id="form_impMessage">
		<h:panelGroup styleClass="summaryHeader">
			<h:outputText value="#{pc_contractStatus.insuredName}" styleClass="summaryInsured" />
			<h:outputText value="#{pc_contractStatus.polNumber}" styleClass="summaryContractNumber" />
		</h:panelGroup>
		<h:panelGroup styleClass="entryLinePadded">
			<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeft, formSplitRight" cellpadding="0" cellspacing="0">  <!-- SPR3232 -->
				<h:column>
					<h:outputLabel value="#{property.impDate}" styleClass="entryLabel" />
					<h:inputText id="Date" value="#{pc_impMessage.date}" styleClass="entryFieldDate" >
						<f:convertDateTime pattern="#{property.datePattern}" />
					</h:inputText>
				</h:column>
				<h:column>
					<h:outputLabel value="#{property.impUser}" styleClass="entryLabelRightOfDate" style="width:40px; "/>
					<h:inputText value="#{pc_impMessage.col3}" styleClass="entryFieldName" style="width: 178px;" />
				</h:column>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup styleClass="entryLinePadded">
			<h:outputLabel value="#{property.impMessage}" styleClass="entryLabelTop" />
			<h:inputTextarea value="#{pc_impMessage.col2}" rows="10" styleClass="entryFieldLong" />
		</h:panelGroup>
		<h:panelGroup styleClass="buttonBar">
			<h:commandButton value="#{property.buttonCancel}" styleClass="buttonLeft" action="#{pc_impMessage.actionCancel}" immediate="true" onclick="setTargetFrame();" />
			<h:commandButton value="#{property.buttonClear}" styleClass="buttonLeft-1" action="#{pc_impMessage.actionClear}" immediate="true" onclick="resetTargetFrame();" />
			<h:commandButton value="#{property.buttonAddNew}" styleClass="buttonRight-1" action="#{pc_impMessage.actionAddNew}" onclick="resetTargetFrame();" />
			<h:commandButton value="#{property.buttonAdd}" styleClass="buttonRight" action="#{pc_impMessage.actionAdd}" onclick="setTargetFrame();" />
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div><!-- SPR3535 -->
	</body>
</f:view>
</html>
