<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA366      	NB-1501   nbA Requirement Order Statuses from Third Party Providers -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.reqStatusTitle}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
			<!--
				var width=600;
				var height=300; 
				function setTargetFrame() {
					document.forms['form_req_status'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_req_status'].target='';
					return false;
				}
			//-->
			</script>
		</head>
		<body onload="popupInit();">
		<PopulateBean:Load serviceName="RETRIEVE_REQ_STATUS" value="#{pc_reqStatus}" />
			<h:form id="form_req_status" >
			<h:outputText id="statusTitle" value="#{property.statusHistory}"
				styleClass="sectionSubheader" style="margin-top:5px; width:580px;" />
				<h:panelGroup id="statusHeader" styleClass="ovDivTableHeader" style="width: 580px; margin-left:10px;">
					<h:panelGrid columns="5" styleClass="formTableHeader" cellspacing="0" style="width: 560px;"
						columnClasses="ovColHdrDate,ovColHdrText140,ovColHdrText120,ovColHdrDate,ovColHdrText140"> 
						<h:commandLink id="statusHdrCol1" value="#{property.reqStatusCol1}" style="width: 100%; height: 100%" 
								styleClass="ovColSorted#{pc_reqStatus.sortedByCol1}" actionListener="#{pc_reqStatus.sortColumn}" /> 
						<h:commandLink id="statusHdrCol2" value="#{property.reqStatusCol2}" style="width: 100%; height: 100%" 
								styleClass="ovColSorted#{pc_reqStatus.sortedByCol2}" actionListener="#{pc_reqStatus.sortColumn}" /> 
						<h:commandLink id="statusHdrCol3" value="#{property.reqStatusCol3}" style="width: 100%; height: 100%" 
								styleClass="ovColSorted#{pc_reqStatus.sortedByCol3}" actionListener="#{pc_reqStatus.sortColumn}" /> 
						<h:commandLink id="statusHdrCol4" value="#{property.reqStatusCol4}" style="width: 100%; height: 100%" 
								styleClass="ovColSorted#{pc_reqStatus.sortedByCol4}" actionListener="#{pc_reqStatus.sortColumn}" /> 
						<h:commandLink id="statusHdrCol5" value="#{property.reqStatusCol5}" style="width: 100%; height: 100%"
								styleClass="ovColSorted#{pc_reqStatus.sortedByCol5}" actionListener="#{pc_reqStatus.sortColumn}" /> 
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup id="statusData" styleClass="ovDivTableData6"
					style="width: 580px; background-color: white; margin-left:10px; height: 175px;">
					<h:dataTable id="statusTable" styleClass="ovTableData" cellspacing="0"
						binding="#{pc_reqStatus.dataTable}" value="#{pc_reqStatus.statusList}" var="status"
						rowClasses="#{pc_reqStatus.rowStyles}" style="width: 560px;"
						columnClasses="ovColDate,ovColText140,ovColText120,ovColDate,ovColText140">
						<h:column>
							<h:outputText id="statusCol1" value="#{status.statusDate}">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:column>
						<h:column>
							<h:outputText id="statusCol2" value="#{status.status}"></h:outputText>
						</h:column>
						<h:column>
							<h:outputText id="statusCol3" value="#{status.statusEventCode}"></h:outputText>
						</h:column>
						<h:column>
							<h:outputText id="statusCol4" value="#{status.statusEventDate}">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:column>
						<h:column>
							<h:outputText id="statusCol5" value="#{status.statusEventDetails}"></h:outputText>
						</h:column>
					</h:dataTable>
				</h:panelGroup>
				<h:panelGroup id="commandButtons" styleClass="tabButtonBar" style="margin-top:15px; background-color: #CED6E5;">  
					<h:commandButton id="btnCommCancel"  action="#{pc_reqStatus.cancel}" value="#{property.buttonCancel}" styleClass="ovButtonLeft" 
					immediate="true" onclick="setTargetFrame();" style="margin-left:5px"/>
				</h:panelGroup>
			</h:form>
			<div id="Messages" style="display: none"><h:messages /></div>
		</body>
	</f:view>
</html>