<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- SPRNBA-576  NB-1301      Underwriter Final Disposition --%>
<%-- NBA322      NB-1301      Initiate Negative Disposition from Work Support Entities --%>
<%-- NBA324 	 NB-1301 	  nbAFull Personal History Interview --%>
<%-- SPRNBA-658  NB-1301      Missing Prompt for Uncommitted Comments On Exit from the Underwriter Workbench--%>
<%-- SRPNBA-627  NB-1301      Commit is disabled on the Underwriter workbench Final dispostion tab for Increase Contracts --%>
<%-- SPR3454     NB-1401      Not Prompted for Uncommitted Draft Changes When Navigating to Another Tab or Another Business Function --%>
<%-- SPR3103     NB-1401      Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View  --%>
<%-- SPRNBA-798  NB-1401      Change JSTL Specification Level --%>
<%-- SPRNBA-868  NB-1501      MIB - The Final Disposition MIB transmit button is not submitting the XML402 when invoked.  --%>
<%-- NBA353		 NB-1501	  Save Draft Comments when navigating from case to case --%>
<%-- NBA356      NB-1501      Comments Floating View --%>
<%-- SPRNBA-838  NB-1601      Approve Radio Button Not Deselected on Refresh --%>

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%-- SPRNBA-798 --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Final Disposition Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		div { background-color: transparent; overflow: hidden; }
	</style>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <%-- NBA324 --%>
	<script type="text/javascript">
		<!--
			var contextpath = '<%=path%>';
			var topOffset = 9;
			var leftOffset = 5;
			var fileLocationHRef = '<%=basePath%>' + 'uw/finalDisp/nbaFinalDispOverview.faces';

			function setTargetFrame() {
				//alert('Setting Target Frame');
				document.forms['form_FinalDispOverview'].target='controlFrame';		
				return false;
			}
			function resetTargetFrame() {
				//alert('Resetting Target Frame');
				document.forms['form_FinalDispOverview'].target='';
				return false;
			}
			function handleCommitAction() {
				approve = document.forms['form_FinalDispOverview']['form_FinalDispOverview:finalDispSub:approve'];
				pendingAcceptence = document.forms['form_FinalDispOverview']['form_FinalDispOverview:finalDispSub:trialAccept'];
				offerAccepted = document.forms['form_FinalDispOverview']['form_FinalDispOverview:finalDispSub:trialAccept'];
				doNotIssue = document.forms['form_FinalDispOverview']['form_FinalDispOverview:finalDispSub:donotIssue'];
				isWSE = document.getElementById('form_FinalDispOverview:isWSE').innerHTML; //NBA322
			
	     			if ((approve != null && approve.checked) ||
		 			(pendingAcceptence  != null && pendingAcceptence.item(0).checked) ||
	 				(offerAccepted != null && offerAccepted.item(1).checked) ||
	              			(isWSE == "false" && doNotIssue != null && doNotIssue.checked)) { //NBA322
	             			setTargetFrame();
	          		} else {
	                    	resetTargetFrame();
	            	}
	      }
	        //NBA324 new method
			function launchPHIBriefcaseView() {			        
				phipopup = launchPopup('phipopup', '<%=path%>/common/popup/popupFramesetPHI.html?popup=<%=basePath%>/nbaPHI/file/PHIStatus.faces?readonly=true', 650, 690);
	            phipopup.focus();
	            top.mainContentFrame.contentRightFrame.nbaContextMenu.phipopup = phipopup;		//FNB004 - VEPL1224		
	            return false;
			}
			
		//SPRNBA-658 new method
		function draftCommentsPresent() {
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftComments = document.forms['form_FinalDispOverview']['form_FinalDispOverview:draftComments'].value;		
			top.mainContentFrame.contentRightFrame.nbaContextMenu.draftCommentsOnOtherContract = document.forms['form_FinalDispOverview']['form_FinalDispOverview:draftCommentsOnOtherContract'].value;	//NBA353
		} 
		
		//SPR3454 new method
		function setDraftChanges() {
			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_FinalDispOverview']['form_FinalDispOverview:draftChanges'].value;
		}
		
		//SPRNBA-658 new method
		function initPage(){
			filePageInit();
			draftCommentsPresent();
			setDraftChanges(); //SPR3454
		} 	 		      
		
		//-->
	</script>
</head>
<body class="whiteBody" onload="initPage();refreshAppFrameExt();" style="overflow-x: hidden; overflow-y: scroll; height:100%"><%-- SPRNBA-658, NBA356 --%>
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_UWNAVIGATION" value="#{pc_uwbean}" />  
		<PopulateBean:Load serviceName="RETRIEVE_FINALDISPNAV" value="#{pc_finalDispNavigation}" />
		<PopulateBean:Load serviceName="RETRIEVE_FINALDISP" value="#{pc_NbaFinalDisposition}" />

		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_FinalDispOverview">
			<f:subview id="navigation">
				<c:import url="/uw/finalDisp/subviews/NbaFinalDispNavigation.jsp" />
			</f:subview> 

			<iframe id="finalDispParty1" name="fdParty1" src="<%=path%>/uw/finalDisp/nbaFinalDispParty1.faces" onload=""
								height="255px" width="642px" frameborder="0" scrolling="no"></iframe>
<%
	if (com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getFinalDispParty2() != null) {
 %>
			<iframe id="finalDispParty2" name="fdParty2" src="<%=path%>/uw/finalDisp/nbaFinalDispParty2.faces" onload=""
								height="255px" width="642px" frameborder="0" scrolling="no"></iframe>
<%
	}
 %>
			<f:subview id="finalDispSub">
				<c:import url="/uw/finalDisp/subviews/NbaAssignFinalDisposition.jsp" />
			</f:subview> 
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" /> 
			</f:subview>
			<h:panelGroup id="clientOverViewBut" styleClass="tabButtonBar">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"
						action="#{pc_uwbean.refresh}" 
						onclick="if (#{pc_uwbean.draftChanges}) { setTargetFrame(); } else { resetTargetFrame(); }"/> <%-- SPR3103, SPRNBA-838 --%>
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight-1"
						disabled="#{pc_finalDispNavigation.auth.enablement['Commit'] || 
									pc_finalDispNavigation.notLocked|| 
									pc_finalDispNavigation.negativeDisposition || 
									pc_uwbean.undCommitDisabledForFunctionalUpdates ||
									pc_finalDispNavigation.issuedNotContractChange}" 
						action="#{pc_finalDispNavigation.actionCommit}" onclick="handleCommitAction();setAppFrameExt();"/> <%-- SRPNBA-627, NBA356 --%>
				<h:commandButton id="btnMIBTransmit" value="#{property.buttonMIBTransmit}" styleClass="tabButtonRight" action="#{pc_finalDispNavigation.transmitMIB}"/><%-- SPRNBA-868 --%>
				<h:commandButton id="btnClose" action="#{pc_finalDispNavigation.actionClose}" style="display:none" />
			</h:panelGroup>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />
			<h:outputText id="isWSE" value="#{pc_finalDispNavigation.transaction}" style="visibility:hidden;"/> <%-- NBA322 --%>
			<h:inputHidden id="draftComments" value="#{pc_uwbean.draftCommentsPresent}" />  <%-- SPRNBA-658 --%>
			<h:inputHidden id="draftCommentsOnOtherContract" value="#{pc_uwbean.draftCommentsOnOtherContract}" />  <%-- NBA353 --%>
		</h:form>
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>