<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-576     NB-1301   Underwriter Final Disposition -->
<!-- NBA245     	NB-1301   Coverage/Party User Interface Rewrite  -->
<!-- NBA329			NB-1401   Retain Denied Coverage and Benefit -->
<!-- SPRNBA-747     NB-1401   General Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Party 2 Coverage Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript" src="javascript/global/scroll.js"></script>  <!-- NBA329 -->
	<script language="JavaScript" type="text/javascript">		
		function setTargetFrame() {
				//alert('Setting Target Frame');
			document.forms['form_CoverageOverview2'].target='controlFrame';		
			return false;
		}
		function resetTargetFrame() {
				//alert('Resetting Target Frame');
			document.forms['form_CoverageOverview2'].target='';
			return false;
		}
		function setParentFrame() {
			//alert('Setting Parent Frame');
			document.forms['form_CoverageOverview2'].target='nbFile';		
			return false;
		}
		//NBA329 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_CoverageOverview2:covClient2TabData', 'form_CoverageOverview2:cov2TableVScroll');
			return false;
		}
		//NBA329 New Method
		function scrollTablePosition() {
			scrollToPosition('form_CoverageOverview2:covClient2TabData', 'form_CoverageOverview2:cov2TableVScroll');
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> 
</head>
<body class="whiteBody" onload="filePageInit();scrollTablePosition();" style="overflow-x: hidden; overflow-y: hidden">  <!-- NBA329 -->
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_COVERAGES2" value="#{pc_uwCovTable2}" /> <!--NBA245 -->
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_CoverageOverview2" onsubmit="saveTableScrollPosition();">  <!-- NBA329 -->
			<!-- Table Column Headers -->
			<h:panelGroup id="cov2TabHeader" styleClass="ovDivTableHeader">
				<h:panelGrid columns="5" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText225,ovColHdrText190,ovColHdrDate,ovColHdrDate"
					cellspacing="0">
					<h:outputLabel id="cov2HdrCol0" value="" styleClass="ovColSortedFalse" />
					<h:outputLabel id="cov2HdrCol1" value="#{property.uwCovClientCol1}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="cov2HdrCol3" value="#{property.uwCovClientCol3}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="cov2HdrCol4" value="#{property.uwCovClientCol4}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="cov2HdrCol5" value="#{property.uwCovClientCol5}" styleClass="ovColSortedFalse" />
				</h:panelGrid>
			</h:panelGroup>
			<!-- Table Columns -->
			<h:panelGroup id="covClient2TabData" styleClass="ovDivTableData" style="height: 135px;">
			 	<h:dataTable id="coverageTable2" styleClass="ovTableData"
							cellspacing="0" 
							cellpadding="0"
							rows="0" 
							binding="#{pc_uwCovTable2.dataTable}"  
							value="#{pc_uwCovTable2.rows}" 
							var="nbaCoverage" 
							rowClasses="#{pc_uwCovTable2.rowStyles}"
							style="min-height:19px;"
							columnClasses="ovColIcon,ovColText225,ovColText190,ovColText85,ovColText85" > <!--NBA245 -->
	
					<h:column id="cov2Col1">
						<!-- begin NBA329 -->
						<h:commandButton id="covCol1_insured" image="images/coverage/insured.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.insured && !nbaCoverage.denied}"
									onmousedown="saveTableScrollPosition();"
									action="#{pc_uwCovTable2.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_insuredDenied" image="images/coverage/insured-denied.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.insured && nbaCoverage.denied}"
									onmousedown="saveTableScrollPosition();"
									action="#{pc_uwCovTable2.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_base" image="images/coverage/base-coverage.gif" styleClass="ovViewIconTrue"
									rendered="#{nbaCoverage.baseCoverage}"
									onmousedown="saveTableScrollPosition();"
									action="#{pc_uwCovTable2.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_denied" image="images/coverage/denied.gif" styleClass="ovViewIconTrue"
									rendered="#{!nbaCoverage.insured && nbaCoverage.denied}"
									onmousedown="saveTableScrollPosition();"
									action="#{pc_uwCovTable2.selectRow}" immediate="true" />
						<h:commandButton id="covCol1_none" image="images/needs_attention/clear.gif" styleClass="ovViewIconFalse"
									rendered="#{!(nbaCoverage.insured || nbaCoverage.denied || nbaCoverage.baseCoverage)}" />
						<!-- end NBA329 -->
					</h:column>
					<h:column id="cov2Col2">
						<h:panelGroup id ="cov2Span1"> <!-- FNB013 --> 
							<h:commandButton id="cov2Icon11" image="images/hierarchies/#{nbaCoverage.icon1}" rendered="#{nbaCoverage.icon1Rendered}"
										styleClass="#{nbaCoverage.icon1StyleClass}"
										style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;"
										onmousedown="saveTableScrollPosition();"
										action="#{pc_uwCovTable2.selectRow}" immediate="true" />  <!-- NBA329 -->
							<h:commandButton id="cov2Icon21" image="images/hierarchies/#{nbaCoverage.icon2}" rendered="#{nbaCoverage.icon2Rendered}"
										styleClass="#{nbaCoverage.icon2StyleClass}"
										style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;"
										onmousedown="saveTableScrollPosition();"
										action="#{pc_uwCovTable2.selectRow}" immediate="true" />  <!-- NBA329 -->
							<h:commandButton id="cov2Icon31" image="images/hierarchies/#{nbaCoverage.icon3}" rendered="#{nbaCoverage.icon3Rendered}"
										styleClass="#{nbaCoverage.icon3StyleClass}"
										style="margin-left: 5px; margin-top: -3px; margin-bottom: -3px; vertical-align:top;"
										onmousedown="saveTableScrollPosition();"
										action="#{pc_uwCovTable2.selectRow}" immediate="true" />  <!-- NBA245, NBA329 -->
							<h:commandLink id="cov2Col2a" title="#{nbaCoverage.rateClassText}"
										onmousedown="saveTableScrollPosition();"
										action="#{pc_uwCovTable2.selectRow}" immediate="true">  <!--NBA245, NBA329 -->
								<h:inputTextarea id="cov2ita1" readonly="true" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:175px;" rendered="#{!nbaCoverage.icon3Rendered}"/> <!--NBA245 -->
								<h:inputTextarea id="covita2"readonly="true" value="#{nbaCoverage.col2}" styleClass="ovMultiLine#{nbaCoverage.draftText}"
									style="width:165px;" rendered="#{nbaCoverage.icon3Rendered}"/> <!--NBA245 -->
							</h:commandLink> 
						</h:panelGroup>
					</h:column>				
					<h:column id="cov2Col3" >
						<h:commandLink id="covCol3a" title="#{nbaCoverage.detailHoverText}"
									onmousedown="saveTableScrollPosition();"
									action="#{pc_uwCovTable2.selectRow}" immediate="true">  <!--NBA245, NBA329 -->
							<h:inputTextarea id="cov2ita3" readonly="true" value="#{nbaCoverage.col3}" styleClass="ovMultiLine#{nbaCoverage.draftText}" style="width: 175px;"  />
						</h:commandLink> 
					</h:column>
					 <!--begin NBA245 -->
					<h:column id="cov2Col4">
						<h:commandLink id="cov2col4a"  action="#{pc_uwCovTable2.selectRow}" immediate="true" styleClass="ovFullCellSelectPrf" 
									onmousedown="saveTableScrollPosition();" >  <!-- NBA329 -->
							<h:outputText value="#{nbaCoverage.col4}"  styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
									<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:commandLink>
					</h:column>
					<h:column id="cov2Col5">
						<h:commandLink id="cov2col5a"  action="#{pc_uwCovTable2.selectRow}" immediate="true" styleClass="ovFullCellSelectPrf" 
									onmousedown="saveTableScrollPosition();" >  <!-- NBA329 -->
							<h:outputText value="#{nbaCoverage.col5}" styleClass="ovMultiLine#{nbaCoverage.draftText}"  style="margin-left:6px;width:80px;">
									<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</h:commandLink> 
					</h:column>
					<!-- end NBA245 -->
				</h:dataTable>
			</h:panelGroup>
			<!-- begin NBA245 -->
			<h:panelGroup styleClass="ovButtonBar" style="width: 628px">
				<h:commandButton id="btnCov2Delete" value="#{property.buttonDelete}" styleClass="ovButtonLeft" style="margin-left: 5px"
						action="#{pc_uwCovTable2.actionDelete}"
						disabled="#{pc_uwCovTable2.deleteDisabled}"
						onclick="setTargetFrame()"
						immediate="true" />
				<h:commandButton id="btnCov2Deny" value="#{property.buttonDeny}" styleClass="ovButtonLeft-1"  style="margin-left: 5px"
						action="#{pc_uwCovTable2.actionDeny}"
						disabled="#{pc_uwCovTable2.denyDisabled}"
						rendered="#{pc_uwCovTable2.denyRendered}"
						onclick="window.parent.disableAllTabs();setParentFrame()"
						immediate="true" /> <!--NBA245, NBA329 -->
				<h:commandButton id="btnCovUnDeny" value="#{property.buttonUndeny}" styleClass="ovButtonLeft-1" style="margin-left: 5px"
						action="#{pc_uwCovTable2.actionDeny}"
						disabled="#{pc_uwCovTable2.denyDisabled}"
						rendered="#{!pc_uwCovTable2.denyRendered}"
						onclick="window.parent.disableAllTabs();setParentFrame()"
						immediate="true" /> <!--NBA329 -->
				
				<h:commandButton id="btnCov2AmendEndorse" value="#{property.buttonAmendEndorse}" styleClass="ovButtonRight-2" style="width: 100px;left: 335px;"
						action="#{pc_uwCovTable2.actionAmendEndorse}"
						disabled="#{pc_uwCovTable2.amendDisabled}"
						onclick="window.parent.disableAllTabs();setParentFrame()"
						immediate="true" />
				<h:commandButton id="btnCov2View" value="#{property.buttonViewUpdate}" styleClass="ovButtonRight-1"
						action="#{pc_uwCovTable2.actionView}"
						disabled="#{pc_uwCovTable2.viewDisabled}"
						onclick="window.parent.disableAllTabs();setParentFrame();"
						immediate="true" />  <!-- SPRNBA-747 -->
				<h:commandButton id="btnCov2AddRating" value="#{property.buttonAddRating}" styleClass="ovButtonRight"
						action="#{pc_uwCovTable2.actionAddRating}"
						disabled="#{pc_uwCovTable2.addRatingDisabled}"
						onclick="window.parent.disableAllTabs();setParentFrame()"
						immediate="true" />
			</h:panelGroup>
			<h:outputLabel id="tabIndex" value="#{pc_uwCovTable2.currentIndex}" style="visibility:hidden;" /> <!-- NBA245 -->
			<!-- end NBA245 -->
			<h:inputHidden id="cov2TableVScroll" value="#{pc_uwCovTable2.VScrollPosition}" />  <!-- NBA329 -->
		</h:form>
	
		<div id="Messages" style="display:none"><h:messages /></div>
	</f:view>
</body>
</html>
