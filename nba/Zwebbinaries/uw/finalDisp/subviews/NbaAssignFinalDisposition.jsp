<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-576		 NB-1301	  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:DynaDiv="/WEB-INF/tld/DynamicDiv.tld"> <!-- SPRNBA-798 -->
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<h:panelGroup id="assignFinalDispView" styleClass="inputFormMat">
		<h:panelGroup id="assignFinalDispSubView" styleClass="inputForm" style="height: 200px;">
			<h:panelGroup id="assignFinalDispHeader" styleClass="formTitleBar">
				<h:outputLabel id="finalDispositionTitle" styleClass="shTextLarge" 
					value="Assign Final Disposition" />
			</h:panelGroup>
			<h:panelGroup id="finalDispositionGroup">
				<DynaDiv:DynamicDiv id="formalDiv" rendered="#{!pc_NbaFinalDisposition.informalApplication}">
					<h:panelGrid columns="2" styleClass="formSplit"
						columnClasses="formSplit22, formSplit78" cellpadding="0" cellspacing="0">
						<h:column id="finalDispositionCol1">
							<h:panelGroup id="finalDispositionRadio1" style="height: 19px; width: 100%; padding-bottom: 10px">
								<h:selectOneRadio id="approve" styleClass="formEntryText" style="width:200px"
											value="#{pc_NbaFinalDisposition.undApprove}"
											valueChangeListener="#{pc_NbaFinalDisposition.selectApprove}"
											disabled="#{pc_NbaFinalDisposition.approveDisabled}"
											binding="#{pc_NbaFinalDisposition.approveRadio}"
											onclick="resetTargetFrame();submit()" immediate="true"> 
									<f:selectItems value="#{pc_NbaFinalDisposition.undApproveItems}" />
								</h:selectOneRadio>
							</h:panelGroup>
						</h:column>
						<h:column id="finalDispositionCol2">
							<h:panelGrid columns="2" styleClass="formSplit"	style="margin-top:0px"
								columnClasses="formSplitleft, formSplitRight" cellpadding="0" cellspacing="0">
								<h:column id="finalDispositionCol3">
									<h:panelGroup id="finalDispositionCheck1" style="width: 100%;margin-top:3px">
										<h:selectBooleanCheckbox id="otherThanAppliedFor1" style="margin-left:10px;"
													value="#{pc_NbaFinalDisposition.otherThanAppliedFor}"
													valueChangeListener="#{pc_NbaFinalDisposition.selectOtherThanAppliedFor}"
													disabled="#{pc_NbaFinalDisposition.otherThanAppliedForDisabled}"
													binding="#{pc_NbaFinalDisposition.otherThanAppliedForCheckbox}"
													onclick="submit()" immediate="true" />
										
										<h:outputLabel value="#{property.uwfinalDispOtherAppliedFor}" styleClass="formLabelRight" />
									</h:panelGroup>
								</h:column>
								<h:column id="finalDispositionCol4">
									<h:panelGroup id="finalDispositionDate1" style="width: 100%">
										<h:outputLabel value="#{property.uwfinalDispContractDate}"
											style="left: 100px" styleClass="formLabel" />
										<h:inputText id="issueDate1" style="width: 100px "
											value="#{pc_NbaFinalDisposition.issueDate}"
											disabled="#{pc_NbaFinalDisposition.issueDateDisabled}">
											<f:convertDateTime pattern="#{property.datePattern}" />
										</h:inputText>
									</h:panelGroup>
								</h:column>
							</h:panelGrid>
						</h:column>
					</h:panelGrid>
				</DynaDiv:DynamicDiv>

				<DynaDiv:DynamicDiv id="informalDiv" rendered="#{pc_NbaFinalDisposition.informalApplication}">
					<h:panelGrid columns="2" styleClass="formSplit" style="margin-bottom:10px;"
						columnClasses="formSplitLeft formSplitRight" cellpadding="0" cellspacing="0">  
						<h:column id="finalDispositionCol5">
							<h:selectOneRadio id="trialAccept" styleClass="formEntryText" style="width: 200px;padding-bottom:20px"
										layout="pageDirection"
										value="#{pc_NbaFinalDisposition.informalAppApproval}"
										valueChangeListener="#{pc_NbaFinalDisposition.selectInformalAccept}"
										rendered="#{pc_NbaFinalDisposition.informalApplication}"
										disabled="#{pc_NbaFinalDisposition.approveDisabled}"
										binding="#{pc_NbaFinalDisposition.informalAcceptRadio}"
										onclick="submit()" immediate="true">
								<f:selectItems value="#{pc_NbaFinalDisposition.undInformalAcceptItems}" />
							</h:selectOneRadio>
						</h:column>
						<h:column id="finalDispositionCol6">
							<h:panelGroup id="finalDispositionCheck2"  styleClass="formDataEntryLine" style="margin-top:-5px">
								<h:selectBooleanCheckbox id="otherThanAppliedFor2"
											value="#{pc_NbaFinalDisposition.otherThanAppliedFor}"
											valueChangeListener="#{pc_NbaFinalDisposition.selectOtherThanAppliedFor}"
											disabled="#{pc_NbaFinalDisposition.otherThanAppliedForDisabled}"
											binding="#{pc_NbaFinalDisposition.otherThanAppliedForCheckbox}" />
								<h:outputLabel value="#{property.uwfinalDispOtherAppliedFor}" styleClass="formLabelRight" />
							</h:panelGroup>
							<h:panelGroup id="finalDispositionDate2" styleClass="formDataEntryLine" style="margin-top:-7px">
								<h:outputLabel value="#{property.uwfinalDispContractDate}" styleClass="formLabel" style="width:95px" />
								<h:inputText id="issueDate2" style="width: 100px"
											value="#{pc_NbaFinalDisposition.issueDate}"
											disabled="#{pc_NbaFinalDisposition.issueDateDisabled}">
									<f:convertDateTime pattern="#{property.datePattern}" />
								</h:inputText>
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</DynaDiv:DynamicDiv>
				
				<h:panelGrid columns="2" styleClass="formSplit" style="margin-top:0px"
					columnClasses="formSplit22, formSplit78" cellpadding="0" cellspacing="0">
					<h:column id="finalDispositionCol9">
						<h:selectOneRadio id="donotIssue" styleClass="formEntryText" style="width: 200px"
									value="#{pc_NbaFinalDisposition.undDoNotIssueString}"
									valueChangeListener="#{pc_NbaFinalDisposition.selectDonotIssue}"
									disabled="#{pc_NbaFinalDisposition.doNotIssueDisabled}"
									binding="#{pc_NbaFinalDisposition.doNotIssueRadio}"
									onclick="resetTargetFrame();submit()" immediate="true"> 
								<f:selectItem id="type" itemValue="0" itemLabel="#{property.uwfinalDispDonotIssue}" />
						</h:selectOneRadio>
					</h:column>
					<h:column id="finalDispositionCol10">
						<h:selectOneMenu styleClass="formEntryTextFull" style="width:430px;margin-top:4px;margin-left:10px"
									value="#{pc_NbaFinalDisposition.undStatus}"
									valueChangeListener="#{pc_NbaFinalDisposition.selectUndStatus}"
									disabled="#{pc_NbaFinalDisposition.undStatusDisabled}"
									onchange="submit()" immediate="true">
							<f:selectItems value="#{pc_NbaFinalDisposition.undStatuses}" />
						</h:selectOneMenu>
						<h:commandButton id="beneficairy_CButton" image="images/link_icons/circle_i.gif"
							style="position: relative; left: 5px; vertical-align: bottom"
							action="#{pc_NbaFinalDisposition.launchFinalDispReasonView}"
							disabled="#{pc_NbaFinalDisposition.finalDispositionReasonViewDisabled}"
							onclick="setTargetFrame();" />
					</h:column>
				</h:panelGrid>

				<f:verbatim>
					<hr class="formBarSeparator" />
				</f:verbatim>
				<h:panelGroup id="txErr" styleClass="text" rendered="#{pc_NbaFinalDisposition.tranErrors}">
					<h:message for="form_FinalDisp"></h:message>
					<f:verbatim>
						<hr class="formBarSeparator" />
					</f:verbatim>
				</h:panelGroup>
				<h:panelGroup id="secondLevelUnd" styleClass="formDataEntryLine" style="height: 41px">
					<h:outputText value="#{property.overrideNextLevelApprovalLabel}" styleClass="formLabel" style="width: 200px" />
					
					<h:selectOneMenu id="selectQueue" styleClass="formEntryText" style="width: 375px; margin-top:5px"
								value="#{pc_NbaFinalDisposition.secondLvlDecisionQueue}">
						<f:selectItems value="#{pc_NbaFinalDisposition.secondLevelDecisionQueueList}" />
					</h:selectOneMenu>
					<h:commandButton id="FinalDispositionDecision_CButton" image="images/link_icons/circle_i.gif"
								style="position: relative; left: 5px; vertical-align: bottom"
								action="#{pc_NbaFinalDisposition.launchFinalDispDecisionView}"
								onclick="setTargetFrame();" />
				</h:panelGroup>
				
				<h:panelGroup id="dispComplete" styleClass="formDataEntryLine" rendered="#{pc_NbaFinalDisposition.finalDispComplete}">
					<h:outputText value="#{property.uwfinalDispFinalDispComplete}" styleClass="formEntryText"
								style="margin-left: 200px; width: 200px" />
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
	</h:panelGroup>
	<h:panelGroup id="finalDispButtons" styleClass="ovButtonBar" style="height:33px;margin-top:-5px">
		<h:commandButton id="btnDispReset" styleClass="finalDispButtonRight-1" style="left: 360px; width: 160px"
					value="#{property.buttonResetDisp}"
					action="#{pc_NbaFinalDisposition.actionResetDisp}"
					disabled="#{pc_NbaFinalDisposition.resetPushButtonDisabled || pc_NbaFinalDisposition.notLocked}"
					onclick="resetTargetFrame()" />
					
		<h:commandButton id="btnDispUnapprove" styleClass="finalDispButtonRight"
					value="#{property.buttonUnapprove}"
					action="#{pc_NbaFinalDisposition.actionUnapproveCase}"
					disabled="#{pc_finalDispNavigation.auth.enablement['Commit'] ||
								pc_NbaFinalDisposition.notLocked ||
								pc_NbaFinalDisposition.unapprovePushButtonDisabled ||
								pc_NbaFinalDisposition.issued}"
					onclick="setTargetFrame()" immediate="true" />
	</h:panelGroup>
</jsp:root>
