<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-576     NB-1301   Underwriter Final Disposition -->
<!-- NBA324 	 NB-1301 	  nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<!-- ********************************************************
			Overview Navigation header - contains Client Icons and Images link
		 ******************************************************** -->	
	<h:panelGroup styleClass="pageHeader" rendered="#{pc_finalDispNavigation.overview}">
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon1}" title="#{pc_finalDispNavigation.icon1Title}"
			action="#{pc_finalDispNavigation.selectIcon1}" rendered="#{pc_finalDispNavigation.icon1Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon2}" title="#{pc_finalDispNavigation.icon2Title}"
			action="#{pc_finalDispNavigation.selectIcon2}" rendered="#{pc_finalDispNavigation.icon2Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon3}" title="#{pc_finalDispNavigation.icon3Title}"
			action="#{pc_finalDispNavigation.selectIcon3}" rendered="#{pc_finalDispNavigation.icon3Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon4}" title="#{pc_finalDispNavigation.icon4Title}"
			action="#{pc_finalDispNavigation.selectIcon4}" rendered="#{pc_finalDispNavigation.icon4Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon5}" title="#{pc_finalDispNavigation.icon5Title}"
			action="#{pc_finalDispNavigation.selectIcon5}" rendered="#{pc_finalDispNavigation.icon5Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon6}" title="#{pc_finalDispNavigation.icon6Title}"
			action="#{pc_finalDispNavigation.selectIcon6}" rendered="#{pc_finalDispNavigation.icon6Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon7}" title="#{pc_finalDispNavigation.icon7Title}"
			action="#{pc_finalDispNavigation.selectIcon7}" rendered="#{pc_finalDispNavigation.icon7Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/#{pc_finalDispNavigation.icon8}" title="#{pc_finalDispNavigation.icon8Title}"
			action="#{pc_finalDispNavigation.selectIcon8}" rendered="#{pc_finalDispNavigation.icon8Rendered}"
			style="position: relative; left: 15px; top: 3px" />
		<h:commandButton id="phiBriefCaseFinalDisp" image="images/phiBriefCase.gif" title="#{property.reviewPHI}" onclick="launchPHIBriefcaseView();" rendered="#{pc_reqimpNav.showPhiBriefcase}" style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"/> <!-- FNB004 NBA324 -->								
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 40px" rendered="#{pc_finalDispNavigation.auth.visibility['Images']}"/><!-- FNB011 NBA324 --> 
		<h:commandButton image="images/link_icons/documents-stack.gif"
			rendered="#{pc_finalDispNavigation.auth.visibility['Images']}"
			onclick="setTargetFrame();" action="#{pc_finalDispNavigation.viewAllSourcesForUW}"
			style="position: absolute; margin-top: 2px; right: 15px; vertical-align: middle" /> <!--  FNB011 NBA324-->
	</h:panelGroup>
	<!-- ********************************************************
			Subview Navigation header - contains back link and Images link
		 ******************************************************** -->
	<h:panelGroup styleClass="pageHeader" rendered="#{!pc_finalDispNavigation.overview}">
		<h:panelGroup styleClass="pageHeader">
			<h:commandLink value="#{property.backTo}" styleClass="phText" style="position: absolute; left: 20px"
						action="#{pc_finalDispNavigation.backToOverview}" />
			<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 200px"
						rendered="#{pc_finalDispNavigation.auth.visibility['Images']}" />  <!-- FNB011 -->
			<h:commandButton image="images/link_icons/documents-stack.gif"
						style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"
						rendered="#{pc_finalDispNavigation.auth.visibility['Images']}"
						action="#{pc_finalDispNavigation.viewAllSourcesForUW}"
						onclick="setTargetFrame();" />  <!-- FNB011 -->
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
