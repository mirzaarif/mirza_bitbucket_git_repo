<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-576     NB-1301   Underwriter Final Disposition -->
<!-- SPRNBA-577     NB-1301   Issue Word Missing from Issue Age Label in Header of Coverages Pane -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Final Disposition First Party</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		div { background-color: transparent; overflow: hidden; }
	</style>
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = 8;
			var leftOffset = 3;
			var fileLocationHRef = '<%=basePath%>';
		//-->
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: hidden; height:100%">
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_FinalDispParty1">

			<table height="230px" width="100%" border="0" cellpadding="0" cellspacing="0" class="sectionHeader">
				<tbody>
					<tr style="height: 25px; margin-top: 5px;">
						<%
						if ((Boolean) com.csc.fsg.nba.ui.jsf.utils.NbaSessionUtils.getFinalDispParty1().isPerson()) {
						%>
							<td style="width: 50%">
						<%
						} else { 
						%>
							<td style="width: 90%">
						
						<%
						}
						%>
							<h:outputLabel value="#{pc_finalDispParty1.name}" styleClass="shTextLarge"
										style="margin-left: 7px; text-transform: capitalize;"/>
						</td>
						<td>
							<h:outputText value="#{pc_finalDispParty1.gender}" styleClass="shText"
										rendered="#{pc_finalDispParty1.person}"/>
						</td>
						<td align="center" colspan="2">
							<h:outputText value="#{property.birthDate}" styleClass="shText" 
										rendered="#{pc_finalDispParty1.person}" />
							<h:outputText value="#{pc_finalDispParty1.birthDate}" styleClass="shText" style="margin-left: 7px"
										rendered="#{pc_finalDispParty1.person}">
								<f:convertDateTime pattern="#{property.datePattern}" />
							</h:outputText>
						</td>
						<td align="right" style="padding-right: 7px">
							<h:outputText value="#{property.statusIssAge}" styleClass="shText"
										rendered="#{pc_finalDispParty1.person}" /> <!-- SPRNBA-577 -->
							<h:outputText value="#{pc_finalDispParty1.age}" styleClass="shText" style="margin-left: 7px"
										rendered="#{pc_finalDispParty1.person}" />
						</td>
					</tr>
					<tr style="height: 31px; padding-bottom: 10px" >
						<td>
							<FileLoader:Files location="uw/finalDisp/party1/" defaultIndex="#{pc_finalDispParty1.defaultTabIndex}" numTabsPerRow="2"/>
							<FileLoader:DisableTab disableIndex="1" disableValue="#{pc_finalDispParty1.coverageTabDisabled}"/>
							<FileLoader:DisableTab disableIndex="2" disableValue="#{pc_finalDispParty1.clientTabDisabled}"/>
						</td>
						<td colspan="2">
							<h:outputText value="#{property.personRateClassTitle}" styleClass="shText"
										rendered="#{pc_finalDispParty1.person}" />
							<h:outputText value="#{pc_finalDispParty1.rateClass}" styleClass="shText"
										style="margin-left: 7px; margin-bottom: 7px"
										rendered="#{pc_finalDispParty1.person}" />
						</td>
						<td align="right" colspan="2" style="padding-right: 7px">
							<h:outputText value="#{pc_finalDispParty1.tobacco}" styleClass="shText"
										rendered="#{pc_finalDispParty1.person && !pc_finalDispNavigation.productDI}"/> <!-- FNB013 -->
							<h:outputText value="#{property.occupClass}" styleClass="shText"
										rendered="#{pc_finalDispParty1.person && pc_finalDispNavigation.productDI}"/><!-- FNB013 -->
							<h:outputText value="#{pc_finalDispParty1.employmentClass}" styleClass="shText" style="left: 7px;"
										rendered="#{pc_finalDispParty1.person && pc_finalDispNavigation.productDI}"/>	<!-- FNB013 -->
						</td>
					</tr>
					<tr style="height:*; vertical-align: top;">
						<td colspan="5"><iframe id="finalDispParty1File" name="file" src="" onload=""
									height="200px" width="100%" frameborder="0" scrolling="no"></iframe></td>
					</tr>
				</tbody>
			</table>
		</h:form>
	</f:view>
</body>
</html>