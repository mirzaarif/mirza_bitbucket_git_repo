<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPRNBA-576     NB-1301   Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- NBA245         NB-1301   Coverage/Party User Interface Rewrite  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
	String path = request.getContextPath();
	String basePath = "";
	if (request.getServerPort() == 80) {
		basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
	} else {
		basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>"> 
	<title>Client Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_ClientOveriew'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_ClientOverview'].target='';
			return false;
		}
		function setParentFrame() {
			//alert('Setting Parent Frame');
			document.forms['form_ClientOverview'].target='nbFile';		
			return false;
		}

	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script>
</head>
<body class="whiteBody" onload="filePageInit();" style="overflow-x: hidden; overflow-y: hidden;"> 
	<f:view>
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT1" value="#{pc_clientTable1}" /> <!-- NBA245 -->
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<h:form id="form_ClientOverview">
			<h:panelGroup id="clientHeaderTable" styleClass="ovDivTableHeader">
				<h:panelGrid columns="6" styleClass="ovTableHeader"
						columnClasses="ovColHdrText155,ovColHdrText165,ovColHdrText105,ovColHdrText50,ovColHdrText50,ovColHdrText70"
						cellspacing="0">
					<h:outputLabel id="clientHdrCol1Table2" value="#{property.clientContractCol1}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol2Table2" value="#{property.clientContractCol2}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol3Table2" value="#{property.clientContractCol3}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol4Table2" value="#{property.clientContractCol4}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol5Table2" value="#{property.clientContractCol5}" styleClass="ovColSortedFalse" />
					<h:outputLabel id="clientHdrCol6Table2" value="#{property.clientContractCol6}" styleClass="ovColSortedFalse" />
				</h:panelGrid>
			</h:panelGroup>
			<!-- Table Columns -->
			<h:panelGroup id="clientConTab" styleClass="ovDivTableData" style="height: 135px;">
				<h:dataTable id="clientContract2Table" styleClass="ovTableData" cellspacing="0" rows="0"
							binding="#{pc_clientTable1.dataTable}"
							value="#{pc_clientTable1.rows}" var="nbaClient"
							rowClasses="#{pc_clientTable1.rowStyles}"
							columnClasses="ovColText155,ovColText165,ovColText105,ovColText50,ovColText50,ovColText70">
					<h:column id ="client1ConCol1">
						<h:panelGroup id ="ClientConSpan2" styleClass="ovFullCellSelect" style="width: 200%;">
							<h:commandButton id="clientIcon1Table1" image="images/hierarchies/#{nbaClient.icon1}"
										styleClass="#{nbaClient.icon1StyleClass}" style="margin-left: 5px; margin-top: -6px;"
										rendered="#{nbaClient.icon1Rendered}"
										action="#{pc_clientTable1.selectSingleRow}"
										immediate="true" />
							<h:commandButton id="clientIcon2Table1" image="images/hierarchies/#{nbaClient.icon2}"
										styleClass="#{nbaClient.icon2StyleClass}" style="margin-left: 5px; margin-top: -6px;"
										rendered="#{nbaClient.icon2Rendered}" 
										action="#{pc_clientTable1.selectSingleRow}"
										immediate="true" />
							<h:commandLink id="clientCol1Table1" value="#{nbaClient.col1}" style="vertical-align: top; color: #000000;"
										action="#{pc_clientTable1.selectSingleRow}" immediate="true" />
						</h:panelGroup>
					</h:column>
					<h:column id ="client1ConCol2">
						<h:commandLink id="clientCol2Table2" value="#{nbaClient.col2}" styleClass="ovFullCellSelect"
									action="#{pc_clientTable1.selectSingleRow}"
									immediate="true" />
					</h:column>
					<h:column id ="client1ConCol3">
						<h:commandLink id="clientCol3Table2" value="#{nbaClient.col3}" styleClass="ovFullCellSelect"
									action="#{pc_clientTable1.selectSingleRow}"
									immediate="true" />
					</h:column>
					<h:column id ="client1ConCol4">
						<h:commandLink id="clientCol4Table2" value="#{nbaClient.col4}" styleClass="ovFullCellSelect"
									action="#{pc_clientTable1.selectSingleRow}"
									immediate="true" />
					</h:column>
					<h:column id ="client1ConCol5">
						<h:commandLink id="clientCol5Table2" value="#{nbaClient.col5}" styleClass="ovFullCellSelect"
									style="width: 45px; text-align: right;"
									action="#{pc_clientTable1.selectSingleRow}"
									immediate="true" />
					</h:column>
					<h:column id ="client1ConCol6">
						<h:commandLink id="clientCol6Table2" value="#{nbaClient.col6}" styleClass="ovFullCellSelect"
									action="#{pc_clientTable1.selectSingleRow}"
									immediate="true" />
					</h:column>
				</h:dataTable>   
			</h:panelGroup>
			<!-- Button bar -->
			<h:panelGroup id="client1Button2" styleClass="ovButtonBar" style="width: 628px">
				<h:commandButton id="btnClientView2" value="#{property.buttonView}" styleClass="ovButtonRight"
							disabled="#{pc_clientTable1.viewDisabled}"
							action="#{pc_clientTable1.viewUpdateButtonTable}"
							onclick="window.parent.disableAllTabs();setParentFrame()" immediate="true" />
			</h:panelGroup>
			<h:outputLabel id="tabIndex" value="#{pc_clientTable1.currentIndex}" style="visibility:hidden;" /> <!-- NBA245 -->
		</h:form>
	</f:view>
</body>
</html>