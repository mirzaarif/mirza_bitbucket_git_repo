<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2965           6      Underwriter Workbench Scrollbars -->
<!-- NBA213            7      Unified Workspace -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Underwriter Workbench File</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = -22;   //[TODO] need audit number, change required due to iteration_1a merge
			var fileLocationHRef = '<%=basePath%>';
		//-->
		//SPR3396 New Method
		function hideUpdate() {
			parent.hideUpdate();
		}
		//SPR3396 New Method
		function showUpdate() {
			parent.showUpdate();
		}
		</script>
		<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body class="desktopBody">
	<f:view>
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:31px">
					<td><FileLoader:Files location="uw/file/" configFileName="fileConfigClientTabs.properties" defaultIndex="1" numTabsPerRow="4"/> <!-- NBA213 -->
						<FileLoader:DisableTab disableIndex="2" disableValue="#{pc_NbaClientNavigation.disableActivityTab}"/>
						<FileLoader:DisableTab disableIndex="3" disableValue="#{pc_NbaClientNavigation.disableActivityTab}"/>
						<FileLoader:DisableTab disableIndex="4" disableValue="#{pc_NbaClientNavigation.disableActivityTab}"/>												
					</td>
				</tr>
				<tr style="height:*">
					<td><iframe name="file" src="" height="444px" width="100%" frameborder="0" scrolling="auto"></iframe></td>  <!-- SPR2965, NBA213 -->
				</tr>
			</tbody>
		</table>
	</f:view>
</body>
</html>
