<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!--  NBA186  		8			nbA Underwriter Additional Approval and Referral Project -->
<!-- SPRNBA-606    NB-1301      Final Disposition Decision Popup Does Not Follow UI Standards -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" 
	xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Column Headers -->
	<!-- begin SPRNBA-606 -->
	<h:panelGroup id="decisionHeader" styleClass="popupDivTableHeader" style="width: 595px">
		<h:panelGrid columns="5" styleClass="popupTableHeader" columnClasses="popupColHdrText"  style="width: 575px; table-layout: auto;" cellspacing="0" cellpadding="0">
			<h:commandLink id="tentHdrCol1" value="#{property.uwfinalDispDispCol1}"	styleClass="ovColSorted#{pc_tentativeDispositionTable.sortedByTentCol1}" actionListener="#{pc_tentativeDispositionTable.sortColumn}" style="width:70px"/>
			<h:commandLink id="tentHdrCol2"	value="#{property.uwfinalDispDispCol2}"	styleClass="ovColSorted#{pc_tentativeDispositionTable.sortedByTentCol2}" actionListener="#{pc_tentativeDispositionTable.sortColumn}" style="width:105px"/>
			<h:commandLink id="tentHdrCol3"	value="#{property.uwfinalDispDispCol3}"	styleClass="ovColSorted#{pc_tentativeDispositionTable.sortedByTentCol3}" actionListener="#{pc_tentativeDispositionTable.sortColumn}" style="width:105px"/>
			<h:commandLink id="tentHdrCol4" value="#{property.uwfinalDispDispCol4}" styleClass="ovColSorted#{pc_tentativeDispositionTable.sortedByTentCol4}" actionListener="#{pc_tentativeDispositionTable.sortColumn}" style="width:125px"/>
			<h:commandLink id="tentHdrCol5" value="#{property.uwfinalDispDispCol5}" styleClass="ovColSorted#{pc_tentativeDispositionTable.sortedByTentCol5}" actionListener="#{pc_tentativeDispositionTable.sortColumn}" style="width:170px"/>
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="popupDivTableData6" style="width: 595px">
		<h:dataTable id="tentDispositionTable" styleClass="popupTableData" cellspacing="0" rows="0" 
		binding="#{pc_tentativeDispositionTable.dataTable}" value="#{pc_tentativeDispositionTable.tentDispnTableList}" 
			var="tentDisposition" columnClasses="popupColText" style="width: 575px">
			<h:column>
				<h:outputText id="contractCol1" value="#{tentDisposition.dispLevel}" styleClass="ovFullCellSelect" style="width:70px"/>
			</h:column>
			<h:column>
				<h:outputText id="contractCol2" value="#{tentDisposition.underwriterId}" styleClass="ovFullCellSelect" style="width:105px"/>
			</h:column>
			<h:column>
				<h:outputText id="contractCol3" value="#{tentDisposition.dispDate}" styleClass="ovFullCellSelect" style="width:105px"/>
			</h:column>
			<h:column>
				<h:outputText id="contractCol4" value="#{tentDisposition.disposition}" styleClass="ovFullCellSelect" style="width:125px"/>
			</h:column>
			<h:column>
				<h:outputText id="contractCol5" value="#{tentDisposition.dispReason}" styleClass="ovFullCellSelect" style="width:170px"/>
			</h:column>						
		</h:dataTable>
	</h:panelGroup>
	<!-- end SPRNBA-606 -->
</jsp:root>