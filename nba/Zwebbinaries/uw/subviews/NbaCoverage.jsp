<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223        NB-1101      Underwriter Final Disposition -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />

	<h:panelGroup id="viewUpdate" styleClass="inputFormMat" style="background-color: #FFFFFF">
		<h:panelGroup id="ViewUpdateForm" styleClass="inputForm" style="height: 220px; margin-left: 0px">
				<!-- Table Header -->
				<h:outputLabel id="viewCoverageTitle" value="#{property.uwCovViewCoverageTitle}" styleClass="formTitleBar" />
				<!-- Display Fields -->
					<h:panelGroup id="viewUpdatePlan" styleClass="formDataDisplayLine">
						<h:panelGroup id="viewUpdatePlanTxt" style="width: 380px;  overflow: hidden;">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column id="viewUpdateCol1">
									<h:outputLabel value="#{property.uwPlan}" styleClass="formLabel" />
								</h:column>
								<h:column id="viewUpdateCol2">
									<h:outputText value="#{pc_NbaCoverageViewUpdate.planText}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup id="viewUpdateEff" style="position: absolute; left: 330px">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column id="viewUpdateCol3">
									<h:outputLabel value="#{property.uwEffective}" styleClass="formLabel" />
								</h:column>
								<h:column id="viewUpdateCol4">
									<h:outputText value="#{pc_NbaCoverageViewUpdate.effectiveDate}" styleClass="formEntryDate">
										<f:convertDateTime pattern="#{property.datePattern}" />
									</h:outputText>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
					</h:panelGroup>
					<h:panelGroup id="viewUpdateStatus" styleClass="formDataDisplayLine">
						<h:panelGroup id="viewUpdateUWStatus1" style="width: 380px;  overflow: hidden;">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column id="viewUpdateCol5">
									<h:outputLabel value="#{property.uwStatus}" styleClass="formLabel" />
								</h:column>
								<h:column id="viewUpdateCol6">
									<h:outputText value="#{pc_NbaCoverageViewUpdate.statusText}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup id="viewUpdateCease" style="position: absolute; left: 330px">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column id="viewUpdateCol7">
									<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
								</h:column>
								<h:column id="viewUpdateCol8">
									<h:outputText id="Cease_Date" value="#{pc_NbaCoverageViewUpdate.ceaseDate}" styleClass="formEntryDate">
										<f:convertDateTime pattern="#{property.datePattern}" />
									</h:outputText>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
					</h:panelGroup>
		
					<f:verbatim>
						<hr class="formSeparator" />
					</f:verbatim>
	
					<h:panelGroup id="viewUpdateAmt" styleClass="formDataDisplayLine">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column id="viewUpdateCol9">
								<h:outputLabel value="#{property.uwAmount}" styleClass="formLabel" />
							</h:column>
							<h:column id="viewUpdateCol10">
								<h:outputText styleClass="formDisplayText" value="#{pc_NbaCoverageViewUpdate.amountText}"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup id="viewUpdatePremium" styleClass="formDataDisplayLine">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column id="viewUpdateCol11">
								<h:outputLabel value="#{property.uwPremium}" styleClass="formLabel"/>
							</h:column>
							<h:column id="viewUpdateCol12">
								<h:outputText value="#{pc_NbaCoverageViewUpdate.premium}" styleClass="formDisplayText"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup id="viewUpdateRateClass" styleClass="formDataDisplayLine">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
							<h:column id="viewUpdateCol13">
								<h:outputLabel value="#{property.uwRateClass}" styleClass="formLabel"/>
							</h:column>
							<h:column id="viewUpdateCol14">
								<h:outputText value="#{pc_NbaCoverageViewUpdate.rateClassText}" styleClass="formDisplayText"/>
							</h:column>
						</h:panelGrid>
					</h:panelGroup>
	
					<h:panelGroup id="viewUpdateSep" rendered="#{pc_NbaCoverageViewUpdate.benficiaryApplicable}">
						<f:verbatim>
							<hr class="formSeparator" />
						</f:verbatim>
						<h:panelGroup id="viewUpdateBen" styleClass="formDataDisplayLine">
							<h:panelGroup style="width: 380px;  overflow: hidden;">
								<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
									<h:column  id="viewUpdateCol15">
										<h:outputLabel value="#{property.uwBeneficiary}" styleClass="formLabel" />
									</h:column>
									<h:column id="viewUpdateCol16">
										<h:outputText styleClass="formDisplayText" value="#{pc_NbaCoverageViewUpdate.beneficiary}"/>
									</h:column>
								</h:panelGrid>
							</h:panelGroup>
							<h:panelGroup id="viewUpdateBenType" style="position: absolute; left: 330px">
								<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
									<h:column id="viewUpdateCol17">
										<h:outputLabel value="#{property.uwBeneficiaryType}" styleClass="formLabel"/>
									</h:column>
									<h:column id="viewUpdateCol18">
										<h:outputText value="#{pc_NbaCoverageViewUpdate.beneficiaryTypeText}" styleClass="formDisplayText"/>
									</h:column>
								</h:panelGrid>
							</h:panelGroup>
						</h:panelGroup>
						<h:panelGroup id="viewUpdateBenDesg" styleClass="formDataDisplayLine">
							<h:panelGroup style="width: 380px;  overflow: hidden;">
								<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
									<h:column id="viewUpdateCol19">
										<h:outputLabel value="#{property.uwBeneficiaryDesignation}" styleClass="formLabel"/>
									</h:column>
									<h:column id="viewUpdateCol20">
										<h:outputText value="#{pc_NbaCoverageViewUpdate.beneficiaryDesignationText}" styleClass="formDisplayText"/>
									</h:column>
								</h:panelGrid>
							</h:panelGroup>
							<h:panelGroup id="viewUpdateIrr" style="position: absolute; left: 450px">
								<h:panelGrid columns="1" cellspacing="0" cellpadding="0" style="height=19px;">
									<h:column id="viewUpdateCol21">
										<h:outputText value="#{property.uwIrrevocable}" styleClass="formDisplayText" rendered="#{pc_NbaCoverageViewUpdate.irrevocable}"/>
									</h:column>
								</h:panelGrid>
							</h:panelGroup>
						</h:panelGroup>
					</h:panelGroup>				
				<!-- Input Fields -->
				<!-- Button bar -->
				<h:panelGroup id="viewUpdateBut" styleClass="formButtonBar">
					<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true" action="#{pc_finalDispNavigation.actionCloseSubView}" /> <!-- SPRNBA-576 -->
					<!--  NBA223 code deleted -->				
				</h:panelGroup>								
			</h:panelGroup>	
		</h:panelGroup>	
</jsp:root>		