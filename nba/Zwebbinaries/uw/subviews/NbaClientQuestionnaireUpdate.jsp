<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" 
	xmlns:h="http://java.sun.com/jsf/html" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="questionnaireSection1" style="background-color: white;">
		<h:panelGroup id="questionnaireSubSection1" styleClass="formDataDisplayTopLine" >
			<h:outputLabel id="questionnaireAnginaWithCatheter" value="#{property.questionnaireAnginaWithCatheter}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireAnginaWithCatheterText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode1}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireAffectiveDisorders" value="#{property.questionnaireAffectiveDisorders}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireAffectiveDisordersText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode9}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection2" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireAnginaWithoutCatheter" value="#{property.questionnaireAnginaWithoutCatheter}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireAnginaWithoutCatheterText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode2}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireSuicideAttempt" value="#{property.questionnaireSuicideAttempt}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireSuicideAttemptText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode10}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection3" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireCABG" value="#{property.questionnaireCABG}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireCABGText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode3}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireEpilepsy" value="#{property.questionnaireEpilepsy}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireEpilepsyText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode11}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection4" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireMIWithCatheter" value="#{property.questionnaireMIWithCatheter}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireMIWithCatheterText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode4}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireDiabetesMellitus" value="#{property.questionnaireDiabetesMellitus}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireDiabetesMellitusText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode12}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection5" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireMIWithoutCatheter" value="#{property.questionnaireMIWithoutCatheter}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireMIWithoutCatheterText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode5}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireAlcoholAbuse" value="#{property.questionnaireAlcoholAbuse}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireAlcoholAbuseText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode13}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection6" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireCongestiveHeartFailure" value="#{property.questionnaireCongestiveHeartFailure}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireCongestiveHeartFailureText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode6}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireDrugAbuse" value="#{property.questionnaireDrugAbuse}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireDrugAbuseText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode14}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection7" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireCardiomyopahty" value="#{property.questionnaireCardiomyopahty}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireCardiomyopahtyText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode7}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireSyncope" value="#{property.questionnaireSyncope}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireSyncopeText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode15}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="questionnaireSubSection8" styleClass="formDataDisplayLine" >
			<h:outputLabel id="questionnaireHeartValveReplacement" value="#{property.questionnaireHeartValveReplacement}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireHeartValveReplacementText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode8}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>
			<h:outputLabel id="questionnaireTreatmentOfChronicCondition" value="#{property.questionnaireTreatmentOfChronicCondition}" styleClass="formLabel" style="width: 200px"/>
			<h:selectOneMenu id="questionnaireTreatmentOfChronicConditionText" styleClass="formDisplayText" style="width: 100px" value="#{pc_NbaClientAviationQuestionnaire.responseCode16}">
				<f:selectItems value="#{pc_NbaClientAviationQuestionnaire.answerList}" />
			</h:selectOneMenu>			
		</h:panelGroup>			
		<h:panelGroup id="questionnaireViewButtonBar" styleClass="formButtonBar">
			<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="setTargetFrame();" action="#{pc_NbaClientAviationQuestionnaire.cancel}" immediate="true"/>
			<h:commandButton value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();" action="#{pc_NbaClientAviationQuestionnaire.clear}" immediate="true"/>
			<h:commandButton value="#{property.buttonSave}" styleClass="formButtonRight" onclick="setTargetFrame();" action="#{pc_NbaClientAviationQuestionnaire.save}" />
		</h:panelGroup>			
	</h:panelGroup>		
</jsp:root>

