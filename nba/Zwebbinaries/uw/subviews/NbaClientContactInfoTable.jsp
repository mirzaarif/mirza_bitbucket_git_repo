<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- SPRNBA-576     NB-1301   Underwriter Final Disposition -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="clientContactInfoTitleHeader1" styleClass="formTitleBar" rendered="#{pc_NbaClientDetails.personParty}"> <!-- NBA213 -->
		<h:outputLabel id="clientContactInfoTableTitle1" value="#{property.clientContactInfoTitleAsTab}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="clientContactInfoTitleHeader2" styleClass="sectionSubheader" rendered="#{pc_NbaClientDetails.organizationParty}">
		<h:outputLabel id="clientContactInfoTableTitle2" value="#{property.clientContactInfoTitleAsTable}" styleClass="shTextLarge" />
	</h:panelGroup>
	
	<!-- Table Column Headers -->
	<h:panelGroup id="clientContactInfoHeader" styleClass="ovDivTableData" style="width:620px;"> <!--NBA213, SPRNBA-576 -->
		<h:panelGrid columns="#{pc_NbaClientContactInfoTableData.numOfColumns}" styleClass="ovTableData" columnClasses="ovColTextTop" cellspacing="0" style="width:600px;"> <!-- NBA213 -->
			<h:outputLabel id="clientContactInfoHdrCol1" value="" style="height: 10px" rendered="#{pc_NbaClientContactInfoTableData.renderAddressColumn}" />
			<h:outputLabel id="clientContactInfoHdrCol2" value="" style="height: 10px" rendered="#{pc_NbaClientContactInfoTableData.renderPhoneColumn}"/> 
			<h:outputLabel id="clientContactInfoHdrCol3" value="" style="height: 10px" rendered="#{pc_NbaClientContactInfoTableData.renderEmailColumn}"/>
		
			<h:outputLabel id="clientContactInfoHdrCol4" value="#{property.clientContactInfoCol1}" style="color: gray; font-weight: bold;" rendered="#{pc_NbaClientContactInfoTableData.renderAddressColumn}"/>
			<h:outputLabel id="clientContactInfoHdrCol5" value="#{property.clientContactInfoCol2}" style="color: gray; font-weight: bold;" rendered="#{pc_NbaClientContactInfoTableData.renderPhoneColumn}"/>
			<h:outputLabel id="clientContactInfoHdrCol6" value="#{property.clientContactInfoCol3}" style="color: gray; font-weight: bold;" rendered="#{pc_NbaClientContactInfoTableData.renderEmailColumn}"/>
		</h:panelGrid>
	
		<h:panelGrid columns="#{pc_NbaClientContactInfoTableData.numOfColumns}" styleClass="ovTableData" columnClasses="ovColTextTop"  
			style="vertical-align: text-top; width:600px;" cellspacing="0"> <!-- NBA213 -->
			<h:column rendered="#{pc_NbaClientContactInfoTableData.renderAddressColumn}">
				<h:dataTable id="clientContactInfoAddressTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientContactInfoTableData.addressHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.addressRows}" var="nbaContactInfoAddress" rowClasses="#{pc_NbaClientContactInfoTableData.rowStyles}"
					columnClasses="ovColTextTop"> <!-- NBA213 -->
					<h:column>
						<h:outputLabel id="clientContactInfoAddressCol1" value="#{nbaContactInfoAddress.col1}" style="#{nbaContactInfoAddress.rowStyle}"/>						
					</h:column>				
				</h:dataTable>
			</h:column>
			<h:column rendered="#{pc_NbaClientContactInfoTableData.renderPhoneColumn}">
				<h:dataTable id="clientContactInfoPhoneTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientContactInfoTableData.phoneHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.phoneRows}" var="nbaContactInfo1Phone" rowClasses="#{pc_NbaClientContactInfoTableData.rowStyles}"
					columnClasses="ovColTextTop"> <!-- NBA213 -->
					<h:column>
						<h:outputLabel id="clientContactInfoPhoneCol1" value="#{nbaContactInfo1Phone.col1}" style="#{nbaContactInfo1Phone.rowStyle}"/>						
					</h:column>
				</h:dataTable>
			</h:column>
			<h:column rendered="#{pc_NbaClientContactInfoTableData.renderEmailColumn}">
				<h:dataTable id="clientContactInfoEmailTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientContactInfoTableData.emailHtmlDataTable}"
					value="#{pc_NbaClientContactInfoTableData.emailRows}" var="nbaContactInfoEmail" rowClasses="#{pc_NbaClientContactInfoTableData.rowStyles}"
					columnClasses="ovColTextTop"> <!-- NBA213 -->
					<h:column>
						<h:outputLabel id="clientContactInfoEmailCol1" value="#{nbaContactInfoEmail.col1}" style="#{nbaContactInfoEmail.rowStyle}"/>												
					</h:column>
				</h:dataTable>
			</h:column>
		</h:panelGrid>
	</h:panelGroup>
</jsp:root>
