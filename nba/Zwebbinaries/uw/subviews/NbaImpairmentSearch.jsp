<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDataEntryTopLine">
		<h:outputLabel value="#{property.impSearchCriteria}" styleClass="formLabel" />
		<h:inputText value="#{pc_impCreateUpdate.searchCriteria}" binding="#{pc_impCreateUpdate.compSearchCriteria}" styleClass="formEntryText" style="width: 270px" />
		<h:selectOneMenu styleClass="formEntryText" style="margin-left: 2px; width: 114px" value="#{pc_impCreateUpdate.searchType}"
			binding="#{pc_impCreateUpdate.compSearchType}">
			<f:selectItems value="#{pc_impCreateUpdate.searchTypeList}" />
		</h:selectOneMenu>
		<h:commandButton value="#{property.buttonSearch}" styleClass="formButtonRight" style="margin-top: -2px" action="#{pc_impCreateUpdate.actionSearch}"
			immediate="true" disabled="#{pc_impCreateUpdate.searchDisabled}"/>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataDisplayLine" />
	<h:panelGroup styleClass="formDivTableHeader">
		<h:panelGrid columns="2" styleClass="formTableHeader" columnClasses="ovColHdrText250,ovColHdrText250" cellspacing="0">
			<h:commandLink id="impSearchCol1" value="#{property.impSearchCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="impSearchCol2" value="#{property.impSearchCol2}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="formDivTableData3">
		<h:dataTable id="searchResultsTable" binding="#{pc_impCreateUpdate.searchResultsTable}" value="#{pc_impCreateUpdate.searchResults}" var="rslt" rows="0"
			styleClass="formTableData" columnClasses="ovColText250,ovColText250" rowClasses="#{pc_impCreateUpdate.searchRowStyles}" cellspacing="0">
			<h:column>
				<h:commandLink value="#{rslt.col1}" styleClass="ovFullCellSelect" action="#{pc_impCreateUpdate.actionSearchResultsRowSelected}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="#{rslt.col2}" styleClass="ovFullCellSelect" action="#{pc_impCreateUpdate.actionSearchResultsRowSelected}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
