<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDataDisplayLine" />
	<h:panelGroup styleClass="formDivTableHeader">
		<h:panelGrid columns="2" styleClass="formTableHeader" columnClasses="ovColHdrText495,ovColHdrDate" cellspacing="0">
			<h:commandLink id="mibReportCol1" value="#{property.mibReportCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibReportCol2" value="#{property.mibReportCol2}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup styleClass="formDivTableData3">
		<h:dataTable id="mibsTable" rows="0" styleClass="formTableData"
					binding="#{pc_reqMIBReports.resultsTable}" value="#{pc_reqMIBReports.mibReports}" var="mib"
					rowClasses="#{pc_reqMIBReports.mibReportsRowStyles}"
					columnClasses="ovColText495,ovColDate" cellspacing="0" >
				<h:column>
					<h:commandLink value="#{mib.name}" styleClass="ovFullCellSelect" style="width: 525px" action="#{pc_reqMIBReports.selectRow}" />
				</h:column>
				<h:column>
					<h:commandLink value="#{mib.submitted}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectRow}" />
				</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>