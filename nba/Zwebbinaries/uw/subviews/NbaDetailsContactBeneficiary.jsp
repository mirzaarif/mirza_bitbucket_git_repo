<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223       NB-1101      Underwriter Final Disposition -->
<!-- SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Contract Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {			  
			document.forms['form_ContactDetails'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {			 
			document.forms['form_ContactDetails'].target='';
			return false;
		}
		function setDraftChanges() {	 
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_ContactDetails']['form_ContactDetails:draftChanges'].value;  
		}		
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> 
</head>
<body class="whiteBody" onload="filePageInit();setDraftChanges();" style="overflow-x: hidden; overflow-y: scroll"> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientDetails}" /> <!-- NBA245 -->
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientContactInfoTableData}" /> <!-- NBA245 -->
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientBeneficiaryTableData}" /> <!-- NBA245 -->
		<h:form id="form_ContactDetails">
			
		<f:subview id="navigationtable"> <!-- SPRNBA-576 -->
			<c:import url="/uw/finalDisp/subviews/NbaClientTable.jsp" /> <!-- SPRNBA-576 -->
		</f:subview>
		<!-- SPRNBA-576 code deleted -->
			
			<div id="viewBenDetails" class="inputFormMat">
				<div class="inputForm">
					<f:subview id="clientPersonDetails" rendered="#{pc_NbaClientDetails.personParty}">
						<c:import url="/uw/subviews/NbaClientPersonInfo.jsp" />
					</f:subview>
					<f:subview id="clientOrganizationDetails" rendered="#{pc_NbaClientDetails.organizationParty}">
						<c:import url="/uw/subviews/NbaClientOrganizationInfo.jsp" />
					</f:subview>				
					<f:subview id="clientContactInfoTable">
						<c:import url="/uw/subviews/NbaClientContactInfoTable.jsp" />
					</f:subview>	
					<f:subview id="clientBeneficiaryTable">
						<c:import url="/uw/subviews/NbaClientBeneficiaryTable.jsp" />
					</f:subview>
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true" action="#{pc_finalDispNavigation.actionCloseSubView}" /> <!-- SPRNBA-576 -->
					</h:panelGroup>
				</div>
			</div>
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />	
			<f:subview id="nbaCommentBar">
				<c:import url="/common/subviews/NbaCommentBar.jsp" />  
			</f:subview>
		</h:form>
		<div id="Messages" style="display: none"><h:messages /></div>
	</f:view>
</body>
</html>