<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- NBA226            8      nba MIB Translation and validation --%>
<%-- NBA390     	NB-1601   nbA MIB Enhancements --%>
<%-- SPRNBA-894     NB-1601   The Add push button on the MIB Report view is disabled after being invoked upon adding an MIB Code --%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>


	<!-- Check box Bar  -->
	<h:panelGroup id="checkBoxes" styleClass="formSectionBar" style="padding-top: 0px">
		<h:outputText value="#{property.mibCodes}" styleClass="shTextLarge" /> 
		<h:selectBooleanCheckbox id="commCB1" styleClass="formEntryCheckbox" 
			value="#{pc_reqMIBCodes.showReceived}" style="margin-left: 12px" />
		<h:graphicImage value="images/needs_attention/filledcircle-onwhite.gif" styleClass="commentIcon"  />
		<h:outputText value="#{property.mibCodeReceived}" styleClass="shText" />

		<h:selectBooleanCheckbox id="commCB2" styleClass="formEntryCheckbox" 
			value="#{pc_reqMIBCodes.showTransmitted}" style="margin-left: 12px; width: 20px" />
		<h:graphicImage value="images/needs_attention/zigzag-onwhite.gif" styleClass="commentIcon" />
		<h:outputText value="#{property.mibCodeTransmitted}" styleClass="shText" />

		<h:selectBooleanCheckbox id="commCB3" styleClass="formEntryCheckbox" 
			value="#{pc_reqMIBCodes.showRequested}" style="margin-left: 12px; width: 20px;" />
		<h:graphicImage value="images/needs_attention/flag-onwhite.gif" styleClass="commentIcon" />
		<h:outputText value="#{property.mibCodePending}" styleClass="shText" />

		<h:commandButton value="#{property.buttonGo}" styleClass="formButton" 
			style="margin-top: 2px; margin-left: 26px; width: 60px;" action="#{pc_reqMIBCodes.retrieveSelectedCodes}" onmousedown="resetTargetFrame();"/> 
	</h:panelGroup>
	
	<!-- MIB table header -->
	<h:panelGroup id="mibHeader" styleClass="ovDivTableHeader" style="margin-left: 6px;width: 615px;">
		<h:panelGrid columns="4" cellspacing="0" border="0" styleClass="ovTableHeader" style="width: 595px;"
			columnClasses="ovColHdrIcon,ovColHdrText85,ovColHdrText455,ovColHdrIcon"> 
				<h:commandLink id="type_asc" value="#{property.mibBlankCol}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_reqMIBCodes.sortColumn}" styleClass="ovColSorted#{pc_reqMIBCodes.sortedByCol1}"/>
				<h:commandLink id="code_asc" value="#{property.mibCol2}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_reqMIBCodes.sortColumn}" styleClass="ovColSorted#{pc_reqMIBCodes.sortedByCol2}"/>
				<h:commandLink id="translation_asc" value="#{property.mibCol3}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_reqMIBCodes.sortColumn}" styleClass="ovColSorted#{pc_reqMIBCodes.sortedByCol3}"/>
				<h:commandLink id="hitIndicator_asc" value="#{property.mibBlankCol}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_reqMIBCodes.sortColumn}"  styleClass="ovColSorted#{pc_reqMIBCodes.sortedByCol4}"/>
		</h:panelGrid>	
	</h:panelGroup>
	
	<!-- MIB table data -->	
	<h:panelGroup id="mibCodesData" styleClass="ovDivTableData6" style="margin-left: 6px;width: 615px;" > 
		<h:dataTable id="codesTable" styleClass="ovTableData" cellspacing="0" style="width: 595px;"
					binding="#{pc_reqMIBCodes.mibCodesTable}" value="#{pc_reqMIBCodes.selectedCodeList}" var="codes"
					rowClasses="#{pc_reqMIBCodes.mibCodeRowStyles}"
					columnClasses="ovColIconTop,ovColText85,ovColText455,ovColIconTop" >
			<h:column>
				<h:commandButton id="imgCol1a" image="images/needs_attention/filledcircle-onwhite.gif" rendered="#{codes.codeReceived}"
					 styleClass="ovViewIconTrue" action="#{pc_reqMIBCodes.selectRow}" /> 
				<h:commandButton id="imgCol1b" image="images/needs_attention/flag-onwhite.gif" rendered="#{codes.codePending}" 
					 styleClass="ovViewIconTrue" action="#{pc_reqMIBCodes.selectRow}" />
				<h:commandButton id="imgCol1c" image="images/needs_attention/zigzag-onwhite.gif" rendered="#{codes.codeTransmitted}"
					 styleClass="ovViewIconTrue" action="#{pc_reqMIBCodes.selectRow}" />
			</h:column>
			<h:column>
				<h:commandLink action="#{pc_reqMIBCodes.selectRow}" > 
					<h:inputTextarea readonly="true" value="#{codes.code}" styleClass="ovMultiLine#{codes.draftText}" style="width: 85px;"  /> 
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink action="#{pc_reqMIBCodes.selectRow}" > 
					<h:inputTextarea readonly="true" value="#{codes.translation}"  styleClass="ovMultiLine#{codes.draftText}"  style="width: 425px;"/> 
				</h:commandLink>
			</h:column>
			<h:column>
				<h:panelGroup id="trySameAs"  rendered="#{codes.trySameAs}" >
					<h:commandLink action="#{pc_reqMIBCodes.selectRow}"> 
						<h:inputTextarea readonly="true"  value="#{property.trySameAsSymbol}" style="width: 50px;font-weight: bold;" styleClass="ovMultiLine#{codes.draftText}" /> 
					</h:commandLink>
				</h:panelGroup>	
				<h:panelGroup id="tryUnresolved"  rendered="#{codes.tryUnresolved}" >
					<h:commandLink action="#{pc_reqMIBCodes.selectRow}" > 
						<h:inputTextarea readonly="true" value="#{property.tryUnresolvedSymbol}" style="width: 50px;color: #ff0000;font-weight: bold;" styleClass="ovMultiLine#{codes.draftText}"  /> 
					</h:commandLink>
				</h:panelGroup>
				<h:panelGroup id="hitResolved" rendered="#{codes.hitResolved}" >	
					<h:commandLink action="#{pc_reqMIBCodes.selectRow}" > 
						<h:inputTextarea readonly="true"  value="#{property.hitResolvedSymbol}" style="width: 50px;color: #ff0000;font-weight: bold;"  styleClass="ovMultiLine#{codes.draftText}"/> 
					</h:commandLink>
				</h:panelGroup>
				<h:panelGroup id="blank" rendered="#{! codes.hitResolved and ! codes.tryUnresolved and ! codes.trySameAs}" >	
					<h:commandLink action="#{pc_reqMIBCodes.selectRow}" > 
						<h:inputTextarea readonly="true"  value="" style="width: 50px;"  styleClass="ovMultiLine#{codes.draftText}"/> 
					</h:commandLink>
				</h:panelGroup>				
     		</h:column>
		</h:dataTable>
	</h:panelGroup>
	
	
	<!-- MIB Code and Occupation fields -->
	<h:panelGroup id="codeGroup" styleClass="formDataEntryLine" >
		<h:outputText id="selectedCode" value="#{property.mibCode}"  styleClass="formLabel" style="width: 90px;" /> 
		<h:inputText id="selectedVal" value="#{pc_reqMIBCodes.mibCode}" disabled="#{pc_reqMIBReports.mibCodeDisabled}" styleClass="formEntryText" style="width: 160px;" /><%-- NBA390 --%>		
		
		<h:commandButton image="./images/link_icons/circle_i_onrow.gif" onclick="setTargetFrame();launchMIBWindow();" 
		action="" styleClass="ovAppendIconToTextCell" />
		
		<h:outputText id="occupation" value="#{property.mibOccupation}"  styleClass="formLabel" style="width: 100px; text-align: right;"  /> 
		<h:inputText id="occupationVal" value="#{pc_reqMIBReports.occupation}" disabled="#{pc_reqMIBReports.disabled}" styleClass="formEntryText" style="width: 250px;" />	
	</h:panelGroup>
	
	
	<!-- Delete Add and Update Button group -->
	<h:panelGroup id="mibButton" styleClass="tabButtonBar">
		<h:commandButton id="mibDelete" value="#{property.buttonDelete}" disabled="#{pc_reqMIBCodes.transmittedReport || ! pc_reqMIBCodes.updateMode}"
					styleClass="tabButtonLeft" onclick="setTargetFrame();" action="#{pc_reqMIBCodes.delete}" style="margin-left: 10px"; />
		<h:commandButton id="mibAdd" value="#{property.buttonAdd}" disabled="#{pc_reqMIBCodes.transmittedReport}"
					styleClass="tabButtonRight-1" action="#{pc_reqMIBCodes.add}"
					onclick="resetTargetFrame()" />  <%-- SPRNBA-894 --%>
		<h:commandButton id="mibUpdate" value="#{property.buttonUpdate}" disabled="#{pc_reqMIBCodes.transmittedReport || ! pc_reqMIBCodes.updateMode}"
					styleClass="tabButtonRight" action="#{pc_reqMIBCodes.update}"
					onclick="resetTargetFrame()"/> 
	</h:panelGroup>	

