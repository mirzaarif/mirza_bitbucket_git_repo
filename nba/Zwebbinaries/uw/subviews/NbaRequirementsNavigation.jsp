<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA122            	5     		Underwriter Workbench Rewrite -->
<!-- NBA154            	6      		Requirement Business Function Rewrite -->
<!-- NBA212            	7      		Content Services -->
<!-- FNB011         NB-1101			Work Tracking -->
<!-- NBA324 	 	NB-1301 	  	nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401         Change JSTL Specification Level -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	
	<h:panelGroup styleClass="pageHeader">
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon1}.gif" action="#{pc_reqimpNav.selectInsured1}"
							title="#{pc_reqimpNav.insuredTitle1}" rendered="#{pc_reqimpNav.renderInsuredIcon1}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon2}.gif" action="#{pc_reqimpNav.selectInsured2}"
							title="#{pc_reqimpNav.insuredTitle2}" rendered="#{pc_reqimpNav.renderInsuredIcon2}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon3}.gif" action="#{pc_reqimpNav.selectInsured3}"
							title="#{pc_reqimpNav.insuredTitle3}" rendered="#{pc_reqimpNav.renderInsuredIcon3}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon4}.gif" action="#{pc_reqimpNav.selectInsured4}"
							title="#{pc_reqimpNav.insuredTitle4}" rendered="#{pc_reqimpNav.renderInsuredIcon4}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon5}.gif" action="#{pc_reqimpNav.selectInsured5}"
							title="#{pc_reqimpNav.insuredTitle5}" rendered="#{pc_reqimpNav.renderInsuredIcon5}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon6}.gif" action="#{pc_reqimpNav.selectInsured6}"
							title="#{pc_reqimpNav.insuredTitle6}" rendered="#{pc_reqimpNav.renderInsuredIcon6}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon7}.gif" action="#{pc_reqimpNav.selectInsured7}"
							title="#{pc_reqimpNav.insuredTitle7}" rendered="#{pc_reqimpNav.renderInsuredIcon7}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon8}.gif" action="#{pc_reqimpNav.selectInsured8}"
							title="#{pc_reqimpNav.insuredTitle8}" rendered="#{pc_reqimpNav.renderInsuredIcon8}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon9}.gif" action="#{pc_reqimpNav.selectInsured9}"
							title="#{pc_reqimpNav.insuredTitle9}" rendered="#{pc_reqimpNav.renderInsuredIcon9}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon10}.gif" action="#{pc_reqimpNav.selectInsured10}"
							title="#{pc_reqimpNav.insuredTitle10}" rendered="#{pc_reqimpNav.renderInsuredIcon10}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon11}.gif" action="#{pc_reqimpNav.selectInsured11}"
							title="#{pc_reqimpNav.insuredTitle11}" rendered="#{pc_reqimpNav.renderInsuredIcon11}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon12}.gif" action="#{pc_reqimpNav.selectInsured12}"
							title="#{pc_reqimpNav.insuredTitle12}" rendered="#{pc_reqimpNav.renderInsuredIcon12}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon13}.gif" action="#{pc_reqimpNav.selectInsured13}"
							title="#{pc_reqimpNav.insuredTitle13}" rendered="#{pc_reqimpNav.renderInsuredIcon13}"
							style="position: relative; left: 15px; top: 3px"/>
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon14}.gif" action="#{pc_reqimpNav.selectInsured14}"
							title="#{pc_reqimpNav.insuredTitle14}" rendered="#{pc_reqimpNav.renderInsuredIcon14}"
							style="position: relative; left: 15px; top: 3px" />
		<h:commandButton image="images/paging/1person#{pc_reqimpNav.insuredIcon15}.gif" action="#{pc_reqimpNav.selectInsured15}"
							title="#{pc_reqimpNav.insuredTitle15}" rendered="#{pc_reqimpNav.renderInsuredIcon15}"
							style="position: relative; left: 15px; top: 3px" />
		
		<h:commandLink value="#{property.backTo}" action="#{pc_reqimpNav.backToOverview}" immediate="true"
							rendered="#{!pc_reqimpNav.overview}" styleClass="phText" style="position: absolute; left: 20px" />
		<h:commandButton id="phibriefcase" image="images/phiBriefCase.gif" title="#{property.reviewPHI}" onclick="launchPHIBriefcaseView();" rendered="#{pc_reqimpNav.showPhiBriefcase}" style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"/> <!-- FNB004 NBA324-->					
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 40px" rendered="#{pc_reqimpNav.auth.visibility['Images'] &amp;&amp; pc_reqimpNav.UWBench}"/>  <!-- NBA154 FNB011 NBA324-->
		<h:commandButton image="images/link_icons/documents-stack.gif" style="position: absolute; margin-top: 2px; right: 15px; vertical-align: middle"
							onclick="setTargetFrame();" action="#{pc_reqimpNav.viewAllSourcesForUW}" rendered="#{pc_reqimpNav.auth.visibility['Images'] &amp;&amp; pc_reqimpNav.UWBench}"/> <!-- NBA212 FNB011 NBA324-->
	<!-- NBA223 code deleted -->
	</h:panelGroup>
	<h:panelGroup styleClass="sectionHeader">
		<h:selectOneMenu value="#{pc_reqimpNav.insuredID}" onchange="submit()" styleClass="shMenu" style="position: relative; left: 5px; width: 300px" >
			<f:selectItems value="#{pc_reqimpNav.insuredClients}" />
		</h:selectOneMenu>
		<h:outputText value="#{pc_reqimpNav.insuredGender}" styleClass="shText" style="position: absolute; left: 330px; width: 50px; text-align: center" />
		<h:outputText value="#{property.birthDate}" styleClass="shText" style="position: absolute; left: 422px" />
		<h:outputText value="#{pc_reqimpNav.insuredBirthDate}" styleClass="shText" style="position: absolute; left: 462px">
			<f:convertDateTime pattern="#{property.datePattern}"/>
		</h:outputText>
		<h:outputFormat value="{0} {1}" styleClass="shText" style="position: absolute; right: 15px">
			<f:param value="#{property.age}" />
			<f:param value="#{pc_reqimpNav.insuredAge}" />
		</h:outputFormat>
	</h:panelGroup>
</jsp:root>