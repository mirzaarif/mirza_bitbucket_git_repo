<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223    		 NB-1101      Underwriter Final Disposition -->
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401  Change JSTL Specification Level -->
 

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />

		<h:panelGroup id="viewUpdate" styleClass="inputFormMat" style="background-color: #FFFFFF">
			<h:panelGroup id="ViewUpdateForm" styleClass="inputForm" style="height: 150px; margin-right: 0px">
					<!-- Table Header -->
					<h:outputLabel id="viewPersonTitle" value="#{property.uwCovViewPersonTitle}" styleClass="formTitleBar" />
					<!-- Display Fields -->
						<h:panelGroup styleClass="formDataDisplayLine">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwName}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText value="#{pc_NbaPersonViewUpdate.name}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup styleClass="formDataDisplayLine" rendered="#{pc_NbaPersonViewUpdate.proposedUWStatusPresent}">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=19px; ">
								<h:column>
									<h:outputLabel value="#{property.uwProposeStatus}" styleClass="formLabel" />
								</h:column>
								<h:column>
									<h:outputText value="#{pc_NbaPersonViewUpdate.proposedUnderwritingStatusText}" styleClass="formDisplayText"/>
								</h:column>
							</h:panelGrid>
						</h:panelGroup>
					
					<f:verbatim>
							<hr class="formSeparator" />
					</f:verbatim>
					<!-- Button bar -->					
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true" action="#{pc_finalDispNavigation.actionCloseSubView}" />		<!-- SPRNBA-576 -->				
					</h:panelGroup>				
			</h:panelGroup>
		</h:panelGroup>

</jsp:root>		