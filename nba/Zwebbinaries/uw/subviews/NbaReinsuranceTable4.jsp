<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	
	<h:panelGroup id="reInsuranceTable4Header">
	<h:panelGrid columns="2">
		<h:outputText value="Images:" styleClass="shTextLarge" style = ""/>
		<h:dataTable id="reInsuranceCoverageTable4" cellspacing="0" value="" var="" rowClasses=""
			columnClasses="ovColText165,ovColIcon" styleClass = "ovTableData">
			<h:column>
				<h:commandLink value="Images"
					style="overflow: auto; height: auto;" action="" immediate="true" />
			</h:column>
			<h:column>
				<h:commandButton id="reinCol1a" image="images/link_icons/documents.gif" rendered="true" styleClass="ovViewIconTrue" action="" immediate="true" />
			</h:column>
		</h:dataTable>
		</h:panelGrid>
	</h:panelGroup>
	


	</jsp:root>
