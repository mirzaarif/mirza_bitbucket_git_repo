<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR3075           6      Uncommitted comments are not displayed in draft mode (italics) in the Message field  of the Underwriter work bench Comments tab -->
<!-- SPR3112           6      Cannot view some comment details in the Upgraded Underwriter Workbench.  -->
<!-- NBA213            7      Unified User Interface  -->
<!-- NBA225            8      Comments -->
<!-- FNB004 	    NB-1101 	  PHI -->
<!-- SPRNBA-416     NB-1101   Comments Sort Incorrectly by Date for Comments Entered on the Same Day" -->
<!-- NBA323         NB-1301   nbA Comments Improvements -->
<!-- NBA337         NB-1401   Email Enhancement & support email attachment -->
<!-- SPRNBA-1006    NB-1601   Lengthy Wrapped Comments May Truncate Some Letters by Second Column -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<!-- NBA213 code deleted -->
	<!-- begin SPR2929 -->
	<h:panelGroup id="commentsHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" width="100%" cellspacing="0" border="0" styleClass="ovTableHeader" 
			columnClasses="ovColHdrIcon,ovColHdrText275,ovColHdrText105,ovColHdrText105,ovColHdrDate,ovColHdrIcon"> <!-- SPRxxxx NBA337 -->
				<h:commandLink id="image_asc" value="#{property.commentsCol1}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}" styleClass="ovColSorted#{pc_commentsTable.sortedByCol1}"/>
				<h:commandLink id="text_asc" value="#{property.commentsCol2}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}" styleClass="ovColSorted#{pc_commentsTable.sortedByCol2}" />
				<h:commandLink id="relatesTo_asc" value="#{property.commentsCol3}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}" styleClass="ovColSorted#{pc_commentsTable.sortedByCol3}"  rendered="#{pc_commentsNavigation.UWBench}"/>
				<h:commandLink id="workType_asc" value="#{property.commentsCol3b}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}"  styleClass="ovColSorted#{pc_commentsTable.sortedByCol4}"  rendered="#{!pc_commentsNavigation.UWBench}"/>
				<h:commandLink id="originator_asc" value="#{property.commentsCol4}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}"  styleClass="ovColSorted#{pc_commentsTable.sortedByCol5}" />
				<h:commandLink id="date_asc" value="#{property.commentsCol5}" onmouseover="resetTargetFrame();"
					actionListener="#{pc_commentsTable.sortColumn}"  styleClass="ovColSorted#{pc_commentsTable.sortedByCol6}" /> <!-- SPR2836--> 
				<h:commandLink id="attachment_asc" value="#{property.commentsCol6}" onmouseover="resetTargetFrame();"				
					actionListener="#{pc_commentsTable.sortColumn}"  styleClass="ovColSorted#{pc_commentsTable.sortedByCol7}" /> <!-- NBA337 -->	
			
	</h:panelGrid>	
    <!-- end SPR2929 -->
	</h:panelGroup>	
	<h:panelGroup id="commentData" styleClass="ovDivTableData" > <!-- NBA213 -->
		<h:dataTable id="commentsTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_commentsTable.commentsTable}" value="#{pc_commentsTable.selectedCommentsList}" var="comm"
					rowClasses="#{pc_commentsTable.rowStyles}"
					columnClasses="ovColIconTop,ovColText275ML,ovColText105,ovColText105,ovColDate,ovColIconTop" > <!-- NBA152 NBA337 -->
			<h:column>
				<h:commandButton id="commCol1a" image="images/comments/lock-closed-forlist.gif" rendered="#{comm.comment.note and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA225 -->
				<h:commandButton id="commCol1b" image="images/comments/lock-open-forlist.gif" rendered="#{comm.comment.general and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA225 -->
				<h:commandButton id="commCol1c" image="images/comments/zigzag-forlist.gif" rendered="#{comm.comment.processingError}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" />
				<h:commandButton id="commCol1d" image="images/comments/envelope-forlist.gif" rendered="#{comm.comment.email and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA225 -->
				<h:commandButton id="commCol1e" image="images/comments/phone-forlist.gif" rendered="#{comm.comment.phone and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA225 -->
				<h:commandButton id="commCol1f" image="images/comments/exclamation-forlist.gif" rendered="#{comm.comment.specialInstruction and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA225 -->
				<h:commandButton id="commCol1k" image="images/comments/PHI-forlist.gif" rendered="#{comm.comment.PHI and !comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- FNB004 - VEPL1235  -->
				<!-- begin NBA225 -->
				<h:commandButton id="commCol1g" image="images/comments/voided-secure-forlist.gif" rendered="#{comm.comment.note and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" />
				<h:commandButton id="commCol1h" image="images/comments/voided-general-forlist.gif" rendered="#{comm.comment.general and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" />
				<h:commandButton id="commCol1i" image="images/comments/voided-email-forlist.gif" rendered="#{comm.comment.email and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" />
				<h:commandButton id="commCol1j" image="images/comments/voided-phone-forlist.gif" rendered="#{comm.comment.phone and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" />
				<h:commandButton id="commCol1m" image="images/comments/voided-special-forlist.gif" rendered="#{comm.comment.specialInstruction and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- FNB004 - VEPL1235  -->
				<h:commandButton id="commCol1l" image="images/comments/voided-PHI-forlist.gif" rendered="#{comm.comment.PHI and comm.comment.voided}" styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /><!-- FNB004 -->
				<!-- end NBA225 -->
			</h:column>
			<h:column>
				<h:commandLink action="#{pc_commentsTable.selectRow}" title="#{comm.textToolTip}" immediate="true"> <!-- SPR3112 -->
					<h:inputTextarea readonly="true" value="#{comm.text}" styleClass="ovMultiLine#{comm.draftText}" style="width: 280px;" cols="35" rows="#{comm.rowsCount}"/> <!-- SPR3075 --><!-- SPRNBA-1006 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<!-- begin NBA152 -->
				<h:commandLink value="#{comm.relatesTo}" styleClass="ovFullCellSelect" action="#{pc_commentsTable.selectRow}" immediate="true" rendered="#{pc_commentsTable.UWBench || pc_commentsNavigation.PHI}"/><!-- FNB004 -->
				<h:commandLink value="#{comm.workType}" styleClass="ovFullCellSelectPrf" action="#{pc_commentsTable.selectRow}" immediate="true" rendered="#{!(pc_commentsTable.UWBench || pc_commentsNavigation.PHI)}"
						style="font-weight: normal; overflow: visible; border: 0px;border-style: none; width: 105px; text-decoration:none; background-color: transparent;"/><!-- FNB004 -->
				<!-- end NBA152 -->
			</h:column>
			<h:column>
				<h:commandLink value="#{comm.originator}" styleClass="ovFullCellSelect" action="#{pc_commentsTable.selectRow}" immediate="true" />
			</h:column>
			<h:column>
				<!-- begin SPRNBA-416 -->
				<h:commandLink styleClass="ovFullCellSelect" action="#{pc_commentsTable.selectRow}" immediate="true" style="width:100%" ><!-- NBA323 -->
					<h:outputText value="#{comm.date}">
						<f:convertDateTime pattern="#{property.dateTimePatternAmPm}"/><!-- NBA323 -->
					</h:outputText>
				</h:commandLink>
				<!-- end SPRNBA-416 -->
			</h:column>
			<h:column> <!-- NBA337 -->
				<h:commandButton id="commCol6a" image="images/comments/paperclip-forlist.gif" rendered="#{comm.comment.email and comm.attachment}" title="#{comm.attachmentToolTip}"
				 styleClass="ovViewIconTrue" action="#{pc_commentsTable.selectRow}" immediate="true" /> <!-- NBA337 -->
			</h:column> <!-- NBA337 -->
		</h:dataTable>
	</h:panelGroup>
</jsp:root>