<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA154            6      Requirement Business Function Rewrite -->
<!-- NBA130            6      Requirements and Reinsurance -->
<!-- NBA192 		   7	  Requirement Management Enhancement -->
<!-- NBA212            7      Content Services -->
<!-- NBA208-36         7      Deferred Work Item retrieval -->
<!-- NBA308         NB-1301	  MIB Follow Ups -->
<!-- NBA366      	NB-1501   nbA Requirement Order Statuses from Third Party Providers -->
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="requirementsHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="8" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrIcon,ovColHdrText165,ovColHdrText75,ovColHdrDate,ovColHdrDate,ovColHdrDate,ovColHdrTextStatus#{pc_reqimpNav.sourceAndStatusDisplayable},ovColHdrIconStatus#{pc_reqimpNav.sourceAndStatusDisplayable}"><!-- NBA366 -->
			<!-- begin NBA212 -->
			<h:commandLink id="reqHdrCol1" value="#{property.requirementCol1}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol1}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol2" value="#{property.requirementCol2}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol2}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol3" value="#{property.requirementCol3}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol3}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol4" value="#{property.requirementCol4}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol4}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol5" value="#{property.requirementCol5}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol5}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol6" value="#{property.requirementCol6}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol6}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol7" value="#{property.requirementCol7}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol7}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="reqHdrCol8" value="#{property.requirementCol8}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol8}" actionListener="#{pc_reqTable.sortColumn}"
					onmouseover="resetTargetFrame()" immediate="true" />
			<!-- end NBA212 -->
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="requirementData" styleClass="ovDivTableData#{pc_reqTable.rowCount}"> <!-- NBA154 -->
			<h:dataTable id="requirementsTable" styleClass="ovTableData" cellspacing="0" rows="0"
					binding="#{pc_reqTable.requirementsTable}" value="#{pc_reqTable.requirementList}" var="req"
					rowClasses="#{pc_reqTable.rowStyles}" 
					columnClasses="ovColIcon,ovColText165,ovColText75,ovColDate,ovColDate,ovColDate,ovColTextStatus#{pc_reqimpNav.sourceAndStatusDisplayable},ovColIconStatus#{pc_reqimpNav.sourceAndStatusDisplayable}" ><!-- NBA366 -->
				<h:column>
					<h:commandButton id="reqCol1a" image="images/needs_attention/flag-onwhite.gif" title="#{req.messageText}"
								rendered="#{req.attentionNeeded}" styleClass="ovViewIconTrue" action="#{pc_reqTable.selectRow}"
								onclick="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
					<h:commandButton id="reqCol1aa" image="images/needs_attention/manual.gif" title="#{req.messageText}"
								rendered="#{req.manualAttention}" styleClass="ovViewIconTrue" action="#{pc_reqTable.selectRow}"
								onclick="resetTargetFrame()" immediate="true" />  <!-- NBA192, NBA212 -->							
					<h:commandButton id="reqCol1b" image="images/needs_attention/filledcircle-onwhite.gif" title="#{req.messageText}"
								rendered="#{req.reviewCommitted &amp;&amp; !req.planFReviewNeeded}" styleClass="ovViewIconTrue" action="#{pc_reqTable.selectRow}"
								onclick="resetTargetFrame()" immediate="true" />  <!-- NBA212 NBA308-->
					<h:commandButton id="reqCol1d" image="images/needs_attention/planF.gif" title="#{req.messageText}"
								rendered="#{req.planFReviewNeeded}" styleClass="ovViewIconTrue" action="#{pc_reqTable.selectRow}"
								onclick="resetTargetFrame()" immediate="true" />  <!-- NBA308 -->									
					<h:commandButton id="reqCol1c" image="images/needs_attention/clear.gif" title="#{req.messageText}"
								styleClass="ovViewIconFalse" action="#{pc_reqTable.selectRow}"
								onclick="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
							
				</h:column>
				<h:column>
					<h:commandLink id="reqCol2" value="#{req.typeName}" title="#{req.messageText}"
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" /> <!-- NBA212 -->
				</h:column>
				<h:column>
					<h:commandLink id="reqCol3" value="#{req.statusText}" title="#{req.messageText}"
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" /> <!-- NBA212 -->
				</h:column>
				<h:column>
					<h:commandLink id="reqCol4" value="#{req.created}" title="#{req.messageText}" 
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" /> <!-- NBA130, NBA212 -->
				</h:column>
				<h:column>
					<h:commandLink id="reqCol5" value="#{req.received}" title="#{req.messageText}"
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
				</h:column>
				<h:column>
					<h:selectBooleanCheckbox id="reqCol6a" value="#{req.reviewed}" title="#{req.messageText}"
								styleClass="ovFullCellSelectCheckBox" rendered="#{req.reviewable}" 
								valueChangeListener="#{req.overviewReviewedValueChanged}" onclick="resetTargetFrame();submit();" /> <!-- NBA208-36 -->					
                     <h:commandLink id="reqCol6b" value="#{req.reviewedDate}" title="#{req.messageText}"
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" rendered="#{!req.reviewable}" />  <!-- NBA212 -->
				</h:column>
				<h:column>
					<h:commandLink id="reqCol7" value="#{req.reviewerID}" title="#{req.messageText}"
								styleClass="ovFullCellSelect" action="#{pc_reqTable.selectRow}"
								onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
				</h:column>
				<h:column>
					<h:commandButton id="reqStatus" image="images/link_icons/reqStatus.gif" onclick="setTargetFrame();"
								styleClass="ovViewIconTrue" rendered="#{req.statusReceived}" action="#{req.reqStatus}"/> <!-- NBA366 -->
					<h:commandButton id="reqCol8" image="images/link_icons/documents.gif" styleClass="ovViewIcon#{req.imageViewable}"
								onclick="setTargetFrame()" action="#{req.showSelectedImage}" immediate="true" style="margin-left: 3px;"
								disabled="#{req.documentDisabled}"/> <!-- NBA154, NBA212, NBA366 -->
				</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>