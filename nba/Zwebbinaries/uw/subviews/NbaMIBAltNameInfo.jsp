<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA390     	NB-1601   nbA MIB Enhancements -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	
	<h:outputLabel id="altNameTitle" value="#{property.altNameInfoTitle}" styleClass="sectionSubheader" style="width: 525px;" />
	<h:panelGroup styleClass="popupDivTableHeader">
		<h:panelGrid columns="3" styleClass="popupTableHeader"
				columnClasses="ovColHdrText170,ovColHdrText170,ovColHdrText170" cellspacing="0">
			<h:outputText id="mibHdrCol1" value="#{property.mibAltCol1}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol2" value="#{property.mibAltCol2}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol3" value="#{property.mibAltCol3}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup styleClass="popupDivTableData6">
		<h:dataTable id="mibsCheckTable" styleClass="popupTableData" cellspacing="0" rows="0" 
					binding="#{pc_reqMIBMatch.resultsTable}" value="#{pc_reqMIBMatch.altNameInfo}" var="mibResult"
					rowClasses="#{pc_reqMIBMatch.rowStyles}"
					columnClasses="ovColText170,ovColText170,ovColText170" >
				<h:column>
					<h:outputText value="#{mibResult.firstName}" styleClass="ovFullCellSelect"/>
				</h:column>
				<h:column>
					<h:outputText value="#{mibResult.middleName}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column>
					<h:outputText value="#{mibResult.lastName}" styleClass="ovFullCellSelect" />
				</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>