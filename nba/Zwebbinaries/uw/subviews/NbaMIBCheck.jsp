<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2954           6      View MIB Report view - Tool tip needed for codes returned from MIB -->
<!-- SPR3013           6      Rquirements/Impairment Tab - Birth State is not displaying on the MIB Check detail view -->
<!-- NBA390     	NB-1601   nbA MIB Enhancements -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="popupDivTableHeader">
		<h:panelGrid columns="6" styleClass="popupTableHeader" 
				columnClasses="ovColHdrText170,ovColHdrText50,ovColHdrText60,ovColHdrText60,ovColHdrText85,ovColHdrDate" cellspacing="0"><!-- SPR3013 -->
			<!-- begin NBA390 -->
			<h:outputText id="mibHdrCol1" value="#{property.mibChkCol1}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol2" value="#{property.mibChkCol2}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol4" value="#{property.mibChkCol4}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol5" value="#{property.mibChkCol5}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol6" value="#{property.mibChkCol6}" styleClass="ovColSortedFalse" />
			<h:outputText id="mibHdrCol7" value="#{property.mibChkCol7}" styleClass="ovColSortedFalse" />
			<!-- end NBA390 -->
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup styleClass="popupDivTableData6">
		<h:dataTable id="mibsCheckTable" styleClass="popupTableData" cellspacing="0" rows="0" 
					binding="#{pc_reqMIBMatch.resultsTable}" value="#{pc_reqMIBMatch.mibCheckResults}" var="mibResult"
					rowClasses="#{pc_reqMIBMatch.rowStyles}"
					columnClasses="ovColText170,ovColText50,ovColText60,ovColText60,ovColText85,ovColDate" ><!-- SPR3013 -->
				<h:column rendered="#{!mibResult.response}">
					<h:outputText value="#{mibResult.name}" styleClass="ovFullCellSelect" style="z-index:1000;"/>
				</h:column>
				<h:column rendered="#{!mibResult.response}">
					<h:outputText value="#{mibResult.match}" title="#{mibResult.matchSource}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column rendered="#{!mibResult.response}">
					<h:outputText value="#{mibResult.territory}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column rendered="#{!mibResult.response}">
					<h:outputText value="#{mibResult.company}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column rendered="#{!mibResult.response}">
				<!--  Begin SPR3013 -->
					<h:panelGroup style="width:300px;">
						<h:outputText value="#{mibResult.birthDate}">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
						<h:outputText value="-" />
						<h:outputText value="#{mibResult.birthState}">
						</h:outputText>
					</h:panelGroup>
				<!--  End SPR3013 -->				
				</h:column>
				<h:column rendered="#{!mibResult.response}">
					<h:outputText value="#{mibResult.submitDate}" styleClass="ovFullCellSelect">
						<f:convertDateTime pattern="#{property.datePattern}"/>
					</h:outputText>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup style="width:200%;">
						<h:graphicImage value="./images/hierarchies/T_onrow.gif" rendered="#{!mibResult.lastResponse}" style="margin-left: 5px" />
						<h:graphicImage value="./images/hierarchies/L_onrow.gif" rendered="#{mibResult.lastResponse}" style="margin-left: 5px" />
						<h:outputText value="#{mibResult.responseData}" title="#{mibResult.responseData}"
									style="vertical-align: top; padding-left: 2px; margin-top: 5px;" />  <!-- SPR2954 -->
					</h:panelGroup>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup>
						<h:outputText value="empty" styleClass="ovViewIconFalse" />
					</h:panelGroup>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup>
						<h:outputText value="empty" styleClass="ovViewIconFalse" />
					</h:panelGroup>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup>
						<h:outputText value="empty" styleClass="ovViewIconFalse" />
					</h:panelGroup>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup>
						<h:outputText value="empty" styleClass="ovViewIconFalse" />
					</h:panelGroup>
				</h:column>
				<h:column rendered="#{mibResult.response}">
					<h:panelGroup>
						<h:outputText value="empty" styleClass="ovViewIconFalse" />
					</h:panelGroup>
				</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>