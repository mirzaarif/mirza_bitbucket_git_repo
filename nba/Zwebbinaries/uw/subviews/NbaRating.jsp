<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR3070           6      Null Pointer Exception while updating the rating type from Temporary Flat Extra to Special Class -->
<!-- SPR1006           6      First to Die Joint Coverage Requires Premium Per Unit Field and Edits for Success -->
<!-- SPR3232           6      Final Disposition view is showing horizontal scroll bars -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA223        NB-1101      Underwriter Final Disposition -->
<!-- FNB013	       NB-1101	  DI Support for nbA  -->
 <!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
 <!-- SPRNBA-798     NB-1401  Change JSTL Specification Level -->
<!-- SPRNBA-84	     NB-1601  Premium Per Unit Field Missing from Rating Detail View  -->
 

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- ********************************************************
		Create/Update Rating 
	 ******************************************************** -->
	<h:panelGroup id="createUpdateRatingArea" styleClass="inputFormMat" rendered="#{!pc_NbaRating.viewMode}"> <!-- NBA213 -->
		<h:panelGroup id="createUpdateRatingForm" styleClass="inputForm" style="height:190px;"> <!-- NBA213, NBA223 -->
			<!-- Table Header -->
			<!-- NBA223 begin -->
			<h:outputLabel id="createTitle" value="#{property.uwCovRatingTitle}" styleClass="formTitleBar" />  <!-- NBA223 -->
			<!-- Entry Fields -->
			<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeft, formSplitRight" cellpadding="0" cellspacing="0">  <!-- SPR3232 -->
				<h:column id="createUpdateRatingCol1">
					<h:panelGroup id="createUpdateUWFor" style="height=31px; width: 380px;  overflow: hidden;">  <!-- NBA223 -->
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0">
							<h:outputLabel value="#{property.uwFor}" styleClass="formLabel" />
							<h:outputText value="#{pc_NbaRating.parentDescription}" styleClass="formDisplayText" style="width:200% " />  <!-- NBA223, FNB013 -->
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup id="createUpdateUWType" style="height=31px;width: 380px;  overflow: hidden;">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwType}" styleClass="formLabel" />
						<h:selectOneMenu id="ratingTypeList" styleClass="formEntryTextFull" value="#{pc_NbaRating.ratingType}" style="width: 180px" onchange="resetTargetFrame();submit()" immediate="true">  <!-- SPR3232, NBA223 -->
							<f:selectItems value="#{pc_NbaRating.ratingTypeList}" />
						</h:selectOneMenu>						
					</h:panelGroup>
					<h:panelGroup id="createUpdateUWTable" style="height=31px; width: 380px;  overflow: hidden;" rendered="#{pc_NbaRating.tableRendered}">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwTable}" styleClass="formLabel" />
						<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_NbaRating.ratingTable}" style="width: 180px">  <!-- SPR3232 -->
							<f:selectItems value="#{pc_NbaRating.ratingTableList}" />
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup id="createUpdateRatingUWAmt" style="height=31px;width: 380px;  overflow: hidden;" rendered="#{pc_NbaRating.amountRendered}">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwAmount}" styleClass="formLabel" />
						<h:inputText id="Amount" value="#{pc_NbaRating.amount}" styleClass="formEntryText">
							<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
						</h:inputText>
					</h:panelGroup>
					<h:panelGroup id="createUpdateRatingUWPer" style="height=31px;width: 380px;  overflow: hidden;" rendered="#{pc_NbaRating.pctRendered}">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwPercentage}" styleClass="formLabel" />
						<h:inputText id="Percentage" value="#{pc_NbaRating.percentage}" styleClass="formEntryText">
							<f:convertNumber maxFractionDigits="0" minIntegerDigits="3" maxIntegerDigits="3"  type="integer" />
						</h:inputText>
					</h:panelGroup>
				</h:column>
				<h:column id="createUpdateRatingCol2">
					<h:panelGroup id="createUpdateRatingUWEff" style="height=31px;">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwEffective}" styleClass="formLabel" />
						<h:outputText value="#{pc_NbaRating.effDate}" styleClass="formEntryDate">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
					</h:panelGroup>
					<h:panelGrid columns="2" columnClasses="formSplitLeft, formSplitRight" cellpadding="0" cellspacing="0">  <!-- SPR3232 -->
						<h:column id="createUpdateRatingCol3">
							<h:commandButton id="RatingReasonViewIcon" image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: bottom" 
							onclick="setTargetFrame();" action="#{pc_NbaRating.launchRatingReasonView}" /><!-- NBA223, SPRNBA-576 -->				
							<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
						</h:column>
						<h:column id="createUpdateRatingCol4">
							<h:panelGroup id="createUpdateRatingRadio1" style="position: relative; left: -4px">
								<h:panelGroup id="createUpdateRatingRadio2" style="position: relative; top:-4px;  left: -6px">  <!-- NBA223 -->
									<h:selectOneRadio disabled="#{!pc_NbaRating.tempRating}" layout="pageDirection" styleClass="formEntryText"
										value="#{pc_NbaRating.durDateType}" valueChangeListener="#{pc_NbaRating.durDateTypeChangedListener}"
										binding="#{pc_NbaRating.compDurDateType}" onclick="submit();" immediate="true" ><!-- NBA223 -->
										<f:selectItems value="#{pc_NbaRating.durDatelist}" />
									</h:selectOneRadio>
								</h:panelGroup>
								<h:panelGroup id="createUpdateRatingDate1" style="height=31px; width: 100%" rendered="#{!pc_NbaRating.ceaseDurationRendered}">  <!-- NBA223 -->
									<h:inputText id="Cease_Date" value="#{pc_NbaRating.ceaseDate}" binding="#{pc_NbaRating.compCeaseDate}" styleClass="formEntryDate"
										disabled="#{!pc_NbaRating.ceaseDateRendered}">
										<f:convertDateTime pattern="#{property.datePattern}" />
									</h:inputText>
								</h:panelGroup>
								<h:panelGroup id="createUpdateRatingDur1" style="height=31px; width: 100%" rendered="#{pc_NbaRating.ceaseDurationRendered}"> <!-- NBA223 -->
									<h:inputText id="Cease_Duration" value="#{pc_NbaRating.ceaseDuration}" styleClass="formEntryText" disabled="#{!pc_NbaRating.tempRating}"><!-- SPR3070 -->
										<f:convertNumber maxFractionDigits="0" maxIntegerDigits="2" type="integer" />
									</h:inputText>
								</h:panelGroup>
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:column>
			</h:panelGrid>
			<f:verbatim>
				<hr class="formSeparator" />
			</f:verbatim>
			<!-- NBA223 code deleted -->
			<!-- begin SPR1006 -->
			<h:panelGroup id="rating_line_4" styleClass="formDataEntryLine" rendered="#{pc_NbaRating.extraPremiumRendered}">
				<h:outputLabel value="#{property.uwAnnualPremium}:" styleClass="formLabel" rendered="#{!pc_NbaRating.flexible}"/>
				<h:outputLabel value="#{property.uwMonthlyPremium}:" styleClass="formLabel" rendered="#{pc_NbaRating.flexible}"/>
				<h:inputText id="extraPremium" value="#{pc_NbaRating.extraPremium}" styleClass="formEntryText">
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
				</h:inputText>
			</h:panelGroup>
			<!-- end SPR1006 -->
			<h:panelGroup id="rating_line_5" styleClass="formDataEntryLine" rendered="#{pc_NbaRating.specialClassCodeRendered}">
				<h:outputLabel value="#{property.uwSpecialClass}" styleClass="formLabel" />
				<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_NbaRating.specialClassCode}">
					<f:selectItems value="#{pc_NbaRating.specialClassCodeList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup rendered="vantageBESSystem">
				<f:verbatim>
					<hr class="formSeparator" />
				</f:verbatim>
			</h:panelGroup>
			<h:panelGroup id="rating_line_6" styleClass="formDataEntryLine" rendered="#{pc_NbaRating.commisionableCodeRendered}">
				<h:outputLabel value="#{property.uwCommisionable}" styleClass="formLabel" />
				<h:selectOneMenu styleClass="formEntryTextFull" value="#{pc_NbaRating.commisionableCode}">
					<f:selectItems value="#{pc_NbaRating.commisionableCodeList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<!-- NBA223 code deleted -->
			<!-- Button bar -->
			<h:panelGroup id="createUpdateRatingBut1" styleClass="formButtonBar"> <!-- NBA213 NBA223-->				
				<h:commandButton id="btnAddNew" onclick="resetTargetFrame();" value="#{property.buttonAddNew}" rendered="#{pc_NbaRating.createMode}" styleClass="formButtonRight" action="#{pc_NbaRating.actionAddNew}"/><!-- NBA223 -->				
			</h:panelGroup>			
		</h:panelGroup>
	</h:panelGroup>
	<!-- ********************************************************
		View Rating
	 ******************************************************** -->
	<h:panelGroup id="viewRating" styleClass="inputFormMat" rendered="#{pc_NbaRating.viewMode}">
		<h:panelGroup id="viewContract2" styleClass="inputForm" style="height:190px;"> <!-- NBA213, NBA223 -->
			<!-- Table Header -->
			<h:outputLabel id="viewRatingTitle" value="#{property.uwCovViewRatingTitle}" styleClass="formTitleBar" rendered="#{!pc_NbaRating.proposedInd}" />
			<h:outputLabel id="viewProposedRating" value="#{property.uwCovViewProposedRatingTitle}" styleClass="formTitleBar"
				rendered="#{pc_NbaRating.proposedInd}" />
			<!-- Display Fields -->
			<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeft, formSplitRight" cellpadding="0" cellspacing="0">  <!-- SPR3232 -->
				<!-- begin NBA223 -->
				<h:column id="createUpdateRatingCol5">
					<h:panelGrid columns="3"> 
						<h:column id="createUpdateRatingCol6">
							<h:outputLabel value="#{property.uwType}" styleClass="formLabel" />
						</h:column>
						<h:column id="createUpdateRatingCol7">
							<h:outputText value="#{pc_NbaRating.ratingTypeTranslated}" styleClass="formDisplayText" style="width=200%;" />
						</h:column>
						
						<h:column id="createUpdateRatingCol11">
								<h:commandButton id="RatingReasonViewIcon1" image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: bottom" 
								onclick="setTargetFrame();" action="#{pc_NbaRating.launchRatingReasonView}" /> <!-- SPRNBA-576 -->
						</h:column>
						<!--  NBA223 End -->
					</h:panelGrid>
					<h:panelGrid columns="2">
						<h:column id="createUpdateRatingCol8"> <!-- NBA223 -->
							<h:outputLabel value="#{property.uwTable}" styleClass="formLabel" rendered="#{pc_NbaRating.tableRendered}" />
							<h:outputLabel value="#{property.uwAmount}" styleClass="formLabel" rendered="#{pc_NbaRating.amountRendered}" />
							<h:outputLabel value="#{property.uwPercentage}" styleClass="formLabel" rendered="#{pc_NbaRating.pctRendered}" />
						</h:column>
						<h:column id="createUpdateRatingCol9"> <!-- NBA223 -->
							<h:outputText value="#{pc_NbaRating.ratingTableTranslated}" styleClass="formDisplayText" style="width:200% "
								rendered="#{pc_NbaRating.tableRendered}" />
							<h:outputText value="#{pc_NbaRating.amount}" styleClass="formEntryText" rendered="#{pc_NbaRating.amountRendered}">
								<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
							</h:outputText>
							<h:outputText value="#{pc_NbaRating.percentage}" styleClass="formEntryText" rendered="#{pc_NbaRating.pctRendered}">
								<f:convertNumber pattern="###.##" maxFractionDigits="2" maxIntegerDigits="3" type="number" />
							</h:outputText>
						</h:column>
					</h:panelGrid>
				</h:column>
				<h:column id="createUpdateRatingCol10" rendered="#{!pc_NbaRating.dateDisabled}"> <!--NBA223 -->
					<h:panelGroup id="viewContractUWEff" styleClass="formDataDisplayLine"> <!--NBA223 -->
						<h:outputLabel value="#{property.uwEffective}" styleClass="formLabel" />
						<h:outputText value="#{pc_NbaRating.effDate}" styleClass="formEntryDate">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
					</h:panelGroup>
					<h:panelGroup id="viewContractUWCease" styleClass="formDataDisplayLine"> <!-- NBA223 -->
						<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
						<h:outputText value="#{pc_NbaRating.ceaseDate}" styleClass="formEntryDate" rendered="#{!pc_NbaRating.ceaseDurationRendered}">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
						<h:outputText value="#{pc_NbaRating.ceaseDuration}" styleClass="formEntryText" rendered="#{pc_NbaRating.ceaseDurationRendered}">
							<f:convertNumber maxFractionDigits="0" maxIntegerDigits="2" type="integer" />
						</h:outputText>
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
			<!-- NBA223 code deleted-->
			<!-- Button bar -->
			<f:verbatim>
					<hr class="formSeparator" />
			</f:verbatim>			
			<!-- NBA223 code deleted-->
			<!-- begin SPRNBA-84 -->
			<h:panelGroup id="viewRatingPremium" styleClass="formDataDisplayLine" rendered="#{pc_NbaRating.extraPremiumRendered}" style="margin-top: 10px;">
				<h:outputLabel value="#{property.uwAnnualPremium}:" styleClass="formLabel" rendered="#{!pc_NbaRating.flexible}" />
				<h:outputLabel value="#{property.uwMonthlyPremium}:" styleClass="formLabel" rendered="#{pc_NbaRating.flexible}" />
				<h:outputText id="extraPremiumView" value="#{pc_NbaRating.extraPremium}" styleClass="formEntryText">
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
			</h:panelGroup>
			<!-- end SPRNBA-84 -->
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
