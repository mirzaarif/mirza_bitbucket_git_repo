<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA186            8      nbA Underwriter Additional Approval and Referral Project -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
<PopulateBean:Load serviceName="RETRIEVE_FINALDISP_COV" value="#{pc_NbaFinalDispCoverage}" />
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="contractTitleHeader" styleClass="formTitleBar">
		<h:outputLabel id="contractTableTitle" value="#{property.uwfinalDispTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="contractHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="3" styleClass="ovTableHeader" columnClasses="ovColHdrText250,ovColHdrText250,ovColHdrText105" cellspacing="0">
			<h:outputLabel id="contractHdrCol1" value="#{property.uwfinalDispCovCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="contractHdrCol2" value="#{property.uwfinalDispCovCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="contractHdrCol3" value="#{property.uwfinalDispCovCol3}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="ovDivTableData" style="height: 171px;">  <!-- NBA186 -->
		<h:dataTable id="contractCoverageTable" styleClass="ovTableData" cellspacing="0" rows="0" 
			value="#{pc_NbaFinalDispCoverage.nbaCoveragesList}" var="nbaCoverage" columnClasses="ovColText250,ovColText250,ovColText105">
			<h:column>
				<h:outputText id="contractCol1" value="#{nbaCoverage.col1}" />
			</h:column>
			<h:column>
				<h:outputText id="contractCol2" value="#{nbaCoverage.col2}" title="#{nbaCoverage.tooltipContent}" />
			</h:column>
			<h:column>
				<h:outputText id="contractCol3" value="#{nbaCoverage.col3}" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
