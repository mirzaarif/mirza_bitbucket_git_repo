<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA308 		NB-1301	  MIB Follow Ups -->
<!-- NBA348         NB-1401   MIB Code Translation -->
<!-- NBA362         NB-1501   Save Draft Requirements and Impairments when navigating from case to case -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDataDisplayTopLine">
		<h:outputLabel value="#{property.mibMsgType}" styleClass="formLabel"  style="width: 165px"/>
		<h:outputText value="#{pc_reqMIBFollowUp.messageType}" styleClass="formDisplayText" style="width: 230px" />
		<h:outputLabel value="#{property.mibResponse}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBFollowUp.responseDate}" styleClass="formDisplayDate">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataDisplayLine"> 		
		<h:outputLabel value="#{property.mibFollowUpRequestDate}" styleClass="formLabel" style="width: 165px"/>
		<h:outputText value="#{pc_reqMIBFollowUp.followUpRequestDate}" styleClass="formDisplayText" style="width: 200px" /> 
		<h:outputLabel value="#{property.mibFollowUpReviewed}" styleClass="formLabel" style="width: 155px" />
		<h:selectBooleanCheckbox value="#{pc_reqMIBCheckAndFollowUpsBean.followUpsReviewed}" onclick="submit()" immediate="true" valueChangeListener="#{pc_reqMIBCheckAndFollowUpsBean.updateNbaRequirement}" /><!-- NBA362 -->
	</h:panelGroup>
 
	<h:panelGroup styleClass="formDataDisplayLine" />
	<h:panelGroup styleClass="formDivTableHeader">
		<h:panelGrid columns="6" styleClass="formTableHeader"
				columnClasses="ovColHdrText170,ovColHdrText50,ovColHdrText60,ovColHdrText60,ovColHdrText100,ovColHdrDate" cellspacing="0">
			<h:commandLink id="mibHdrCol1" value="#{property.mibChkCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol2" value="#{property.mibChkCol2}" styleClass="ovColSortedFalse" />
 
			<h:commandLink id="mibHdrCol4" value="#{property.mibChkCol4}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol5" value="#{property.mibChkCol5}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol6" value="#{property.mibChkCol6}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol7" value="#{property.mibChkCol7}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	
	<h:panelGroup id="mibFollowUpTable" styleClass="formDivTableData12">	
		
		<h:dataTable id="mibFollowUpSubTable" styleClass="formTableData" cellspacing="0" rows="0"
					binding="#{pc_reqMIBFollowUp.resultsTable}" value="#{pc_reqMIBFollowUp.mibCheckResults}" var="mibResult"
					rowClasses="#{pc_reqMIBFollowUp.rowStyles}"
					columnClasses="ovColText495" > 
			
					<h:column>
						<h:panelGrid id="subHeaderPGrid" columns="7" rendered="#{!mibResult.response}" styleClass="formTableData" cellspacing="0"
							style="overflow: hidden; background-color: transparent;" columnClasses="ovColText170,ovColText50,ovColText60,ovColText60,ovColText100,ovColDate">	
								<h:column>
									<h:commandLink id="mibCol1Cl" action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	
										<h:outputText id="otName" value="#{mibResult.name}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>	
								</h:column>
								<h:column>
									<h:panelGroup id="pGroup1"  >
										<h:outputText value="#{mibResult.match}" title="#{mibResult.matchSource}" styleClass="shText#{mibResult.draftText}"/>

									</h:panelGroup>
								</h:column>
 
								<h:column>
									<h:commandLink id="mibCol1C4" action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	
										<h:outputText id="otTer" value="#{mibResult.territory}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>												
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C5" action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	
										<h:outputText id="otComp" value="#{mibResult.company}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>																						
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C6" action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();"> 	
										
									<h:panelGroup id="pGroup12" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}">
											<h:outputText value="#{mibResult.birthDate}">
												<f:convertDateTime pattern="#{property.datePattern}" />
											</h:outputText>
											<h:outputText value="-" />
											<h:outputText value="#{mibResult.birthState}">
											</h:outputText>
										</h:panelGroup>
										
									</h:commandLink>																						
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C7" action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	
										<h:outputText id="otSubDate" value="#{mibResult.submitDate}" styleClass="shText#{mibResult.draftText}" >
											<f:convertDateTime pattern="#{property.datePattern}"/>
										</h:outputText>	
									</h:commandLink>																						
								</h:column>
						</h:panelGrid>
						
						<h:outputText id="otResponse" rendered="#{mibResult.response}">
							<h:panelGroup id="pGroup2" style="width:550px;" > <!-- NBA348 -->
								<h:graphicImage value="./images/hierarchies/T_onrow.gif" rendered="#{!mibResult.lastResponse}" style="margin-left: 5px" />
								<h:graphicImage value="./images/hierarchies/L_onrow.gif" rendered="#{mibResult.lastResponse}" style="margin-left: 5px" />
								<h:commandLink id="mibRsp1Cl"  action="#{pc_reqMIBFollowUp.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	
									<h:outputText value="#{mibResult.responseData}" title="#{mibResult.responseCompleteDescription}"
										style="vertical-align: top; padding-left: 2px; margin-top: 5px;" />  
								</h:commandLink>												
							</h:panelGroup>
						</h:outputText>
					</h:column>
		</h:dataTable>

	</h:panelGroup>
	<h:inputHidden id="mibCheckTableVScroll" binding="#{pc_reqMIBFollowUp.scrollValue}" /> 	
</jsp:root>