<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA223       NB-1101      Underwriter Final Disposition. -->
<!-- SPRNBA-813    NB-1501    The Final Disposition Tab And Coverage Party Business Function Receive White Screen On Selecting or Updating The Beneficiary Table -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="clientBeneficiaryTitleHeader" styleClass="sectionHeader">
		<h:outputLabel id="clientBeneficiaryTableTitle" value="#{property.clientBeneficiaryTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="clientBeneficiaryTitleSubHeader" styleClass="sectionSubheader" style="text-indent: 0px;">
		<h:outputLabel id="clientBeneficiaryTableSubTitle" value="#{property.clientBeneficiarySubTitle}" styleClass="pageHeader " />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="clientBeneficiaryHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" columnClasses="ovColHdrText165,ovColHdrText125,ovColHdrText150,ovColHdrText85,ovColHdrText85" cellspacing="0">
			<h:outputLabel id="clientBeneficiaryHdrCol1" value="#{property.clientBeneficiaryCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol2" value="#{property.clientBeneficiaryCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol3" value="#{property.clientBeneficiaryCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol4" value="#{property.clientBeneficiaryCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientBeneficiaryHdrCol5" value="#{property.clientBeneficiaryCol5}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="ovDivTableData"  style="height: 150px;"> <!--NBA213 NBA223-->
		<h:dataTable id="clientBeneficiaryTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientBeneficiaryTableData.htmlDataTable}"
			value="#{pc_NbaClientBeneficiaryTableData.clientBeneficiaryRows}" var="nbaBeneficiary" rowClasses="#{pc_NbaClientBeneficiaryTableData.rowStyles}"
			columnClasses="ovColText165,ovColText125,ovColText150,ovColText85,ovColText85">
			<h:column>
				<h:outputText id="clientBeneficiaryCol1" value="#{nbaBeneficiary.name}" styleClass="ovFullCellSelect" /> <!-- SPRNBA-813 -->
			</h:column>
			<h:column>
				<h:outputText id="clientBeneficiaryCol2" value="#{nbaBeneficiary.relationship}" styleClass="ovFullCellSelect" /> <!-- SPRNBA-813 -->
			</h:column>
			<h:column>
				<h:outputText id="clientBeneficiaryCol3" value="#{nbaBeneficiary.type}" styleClass="ovFullCellSelect" /> <!-- SPRNBA-813 -->
			</h:column>
			<h:column>
				<h:outputText id="clientBeneficiaryCol4" value="#{nbaBeneficiary.distributionText}" styleClass="ovFullCellSelect" /> <!-- SPRNBA-813 -->
			</h:column>
			<h:column>
				<!-- SPRNBA-813 code deleted -->
				<h:outputText id="clientBeneficiaryCol5Amount" value="#{nbaBeneficiary.interestAmount}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneficiary.amount}">
					<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
				</h:outputText>
				<h:outputText id="clientBeneficiaryCol5Percent" value="#{nbaBeneficiary.interestPercent}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneficiary.percent}" >
					<f:convertNumber type="percent" pattern="###.##" /> 
				</h:outputText>
				<h:outputText id="clientBeneficiaryCol5Text" value="#{nbaBeneficiary.distributionText}" 
					styleClass="ovFullCellSelect"
					rendered = "#{nbaBeneficiary.balanceEqualUnknown}" />
				<!-- SPRNBA-813 code deleted -->
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
