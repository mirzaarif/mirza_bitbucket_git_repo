<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA130            6      Requirements and Reinsurance -->
<!-- SPR3221           7      Invalid Push Button Location After Commit of Automatic Reinsurance Requests -->
<!-- SPR2889           7      Reinsurance Tab - reinsurance company name is not displayed in the tool tip when name is rolled over -->
<!-- NBA212            7      Content Services -->
<!-- SPR2877           8      Reinsurance Tab - Unable to update draft additional information - Addl Info button is disabled -->
<!-- SPR3548           8      General Code Clean Up for Version 8 -->
<!-- SPR2871		   8	  Reinsurance Tab - Issues with Request table pane in Reinsurance Overview Tab -->		
<!-- NBA-399	1101	SPR/Defect -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<style type="text/css">
		.colWidth80 {
			width: 80px;
		}
		.tableWidth {
			width: 680px;
		}
		.lastColHdr {
			border-right-style: none;
		}
	</style>
	<h:panelGroup id="requestDataHeader" styleClass="ovDivTableHeader tableWidth" >
		<h:panelGrid columns="7" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText295,ovColHdrText105,ovColHdrText105,ovColHdrDate,ovColHdrIcon" cellspacing="0"  >
			<h:outputText id="rein2HdrCol1" value="#{property.reinsuranceOverviewRequestcol1}" styleClass="ovColSortedFalse" />
			<h:outputText id="rein2HdrCol2" value="#{property.reinsuranceOverviewRequestcol2}" styleClass="ovColSortedFalse" />
			<h:outputText id="rein2HdrCol3" value="#{property.reinsuranceOverviewRequestcol3}" styleClass="ovColSortedFalse" />
			<h:outputText id="rein2HdrCol4" value="#{property.reinsuranceOverviewRequestcol4}" styleClass="ovColSortedFalse" />
			<h:outputText id="rein2HdrCol5" value="#{property.reinsuranceOverviewRequestcol5}" styleClass="ovColSortedFalse" />
			<h:outputText id="rein2HdrCol6" value="#{property.reinsuranceOverviewRequestcol6}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="reInsuranceRequestData" styleClass="ovDivTableData tableWidth" style="height: 350px;">
		<h:dataTable id="reInsuranceOverviewRequestTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_nbaReinsuranceOverviewTable.requestResponseTable}" value="#{pc_nbaReinsuranceOverviewTable.requestResponseList}" var="requestResponse"
					rowClasses="#{pc_nbaReinsNav.overviewRequestsRowStyle}"					
					columnClasses="ovColIcon,ovColText225,ovColText75 colWidth80,ovColText105,ovColText105,ovColDate,ovColIcon" >
			<h:column>
				<h:commandButton id="reinCol1a" image="images/needs_attention/opencircle-onpurple.gif" rendered="#{requestResponse.offerNotAccepted}" styleClass="ovViewIconTrue" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" />
				<h:commandButton id="reinCol1b" image="images/needs_attention/filledcircle-onpurple.gif" rendered="#{requestResponse.offerAccepted}" styleClass="ovViewIconTrue" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" />
				<h:commandButton id="reinCol1c" image="images/needs_attention/clear.gif" styleClass="ovViewIconTrue" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" /> <!-- SPR2871 -->
			</h:column>
			<h:column>
				<h:commandButton id="reinCol2a" image="images/hierarchies/L.gif" rendered="#{requestResponse.levelOne}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:commandButton id="reinCol2b" image="images/hierarchies/T.gif" rendered="#{requestResponse.levelOneWithNext}" styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" immediate="true" />
				<h:commandLink id="reinCol2c" value="#{requestResponse.requests}" title="#{requestResponse.textToolTip}" style="vertical-align: top;" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" /><!-- SPR2889 -->
			</h:column>
			<h:column>
				<h:commandLink id="reinCol3" value="#{requestResponse.coverageKeysForDisplay}" style="vertical-align: top;" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" /><!-- SPR2889 -->
			</h:column>
			<h:column>
				<h:commandLink id="reinCol4" styleClass="ovFullCellSelect" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" >
					<h:outputText value="#{requestResponse.reinsured}" style="width: 100px; height: 17px; text-align: right;">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="0"/>
					</h:outputText>
					<!-- end NBA130 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="reinCol5"  styleClass="ovFullCellSelect" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" >
					<h:outputText value="#{requestResponse.retained}" style="width: 100px; height: 17px; text-align: right;">
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="0"/>
					</h:outputText>
					<!-- end NBA130 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="reinCol6" value="#{requestResponse.date}" styleClass="ovFullCellSelect" title ="#{requestResponse.expirationDate}" action="#{pc_nbaReinsuranceOverviewTable.selectRequestResponseRow}" immediate="true" /> <!-- SPR2871-->
			</h:column>
			<h:column>
				<h:commandButton id="reinCol6a" image="images/link_icons/documents.gif" rendered="#{requestResponse.showImage}" 
					styleClass="ovViewIconTrue" action="#{requestResponse.showSelectedImage}" 
					immediate="true" onclick="setTargetFrame()" /> <!-- NBA212 -->
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovButtonBar">
		<h:commandButton id="btnDelete" action="#{pc_nbaReinsNav.deleteReinsurance}" value="#{property.buttonDelete}" onclick="setTargetFrame();" disabled="#{pc_nbaReinsNav.disableDelete}" styleClass="ovButtonLeft" immediate="true"/><!-- SPR3548 -->
		<h:commandButton id="btnViewUpdate" onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.updateRequestResponse}" disabled="#{pc_nbaReinsNav.disableViewUpdate}" value="#{property.buttonViewUpdate}" styleClass="ovButtonRight-1" style="left: #{pc_nbaReinsNav.viewUpdateLeftPosition}" immediate="true" rendered="#{pc_nbaReinsNav.renderViewUpdate}"/><!--SPR3221 SPR2877 SPR3548-->
		<h:commandButton id="btnView" onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.viewRequest}" value="#{property.buttonView}" styleClass="ovButtonRight-1" immediate="true" rendered="#{!pc_nbaReinsNav.renderViewUpdate}"/><!-- SPR3548 -->			
		<h:commandButton id="btnAddInfo" onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.sendAdditionalInfo}" value="#{property.buttonAddlInfo}" disabled="#{pc_nbaReinsNav.disableAddInfo}" styleClass="ovButtonRight" rendered="#{!pc_nbaReinsNav.renderViewAdditionalInfo}" /><!-- SPR3548 -->
		<h:commandButton id="btnViewAddInfo" onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.viewAdditionalInfo}" value="#{property.buttonViewAddlInfo}" rendered="#{pc_nbaReinsNav.renderViewAdditionalInfo}" styleClass="ovButtonRight" /><!-- SPR3548 -->
		<h:commandButton id="btnViewUpdateAddInfo" onclick="resetTargetFrame();" action="#{pc_nbaReinsNav.viewUpdateAdditionalInfo}" value="#{property.buttonViewUpdateAddlInfo}" rendered="#{pc_nbaReinsNav.renderViewUpdateAdditionalInfo}" styleClass="ovButtonRight" style="left: 491px; width: 140px;"/> <!-- SPR2877 -->
	</h:panelGroup>
</jsp:root>
