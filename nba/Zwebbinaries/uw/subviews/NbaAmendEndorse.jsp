<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR3232           6      Final Disposition view is showing horizontal scroll bars -->
<!-- NBA213            7      Unified User Interface -->
<!-- NBA223			NB-1101	  Underwriter Final Disposition  -->
<!--SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-583		NB-1301	  NB-1301 Code Clean Up -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- ********************************************************
		Create/Update Amendment/Endorsement 
	 ******************************************************** -->
	<h:panelGroup id="createUpdateAmendEndorseArea" styleClass="inputFormMat" rendered="#{!pc_NbaAmendEndorse.viewMode}">
		<h:panelGroup id="createUpdateAmendEndorseForm" styleClass="inputForm" style="height: 370px ">  <!-- NBA223 -->
			<!-- Table Header -->
			<h:outputLabel id="createTitle" value="#{property.uwCovCreateAmmendmentTitle}" rendered="#{pc_NbaAmendEndorse.createMode}"
				styleClass="formTitleBar" />
			<h:outputLabel id="updateAmendTitle" value="#{property.uwCovUpdateAmmendmentTitle}" rendered="#{pc_NbaAmendEndorse.amendmentUpdateMode}"
				styleClass="formTitleBar" />
			<h:outputLabel id="updateEndTitle" value="#{property.uwCovUpdateEndorsementTitle}" rendered="#{pc_NbaAmendEndorse.endorsementUpdateMode}"
				styleClass="formTitleBar" />
			<!-- Entry Fields -->
			<!-- NBA223 deleted code -->
				<h:panelGroup styleClass="formDataEntryLine">
					<h:panelGroup style="width: 380px;  overflow: hidden;">
						<h:panelGrid columns="2" cellspacing="0" cellpadding="0" style="height=31px; ">
							<h:outputLabel value="#{property.uwFor}" styleClass="formLabel" />
							<h:outputText value="#{pc_NbaAmendEndorse.parentDescription}" styleClass="formDisplayText" style="width:200% " />
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup style="position: absolute; left: 330px; margin-top: 5px">  <!-- NBA223 -->
						<h:outputLabel value="#{property.uwCease}" styleClass="formLabel" />
						<h:inputText id="Cease_Date" value="#{pc_NbaAmendEndorse.ceaseDate}" binding="#{pc_NbaAmendEndorse.compCeaseDate}" styleClass="formEntryDate"
							disabled="#{pc_NbaAmendEndorse.ceaseDateDisabled}">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:inputText>
					</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup styleClass="formDataEntryLine">
					<h:panelGrid columns="2" cellspacing="0" cellpadding="0">
						<h:panelGroup style="width: 390px">
							<h:panelGrid columns="2" cellspacing="0" cellpadding="0">
								<h:outputLabel value="#{property.uwAction}" styleClass="formLabel" style=" position: relative; top: -12px" />
								<h:panelGroup>
									<h:selectOneRadio disabled="#{pc_NbaAmendEndorse.typeDisabled}" layout="pageDirection" styleClass="formEntryText"
										value="#{pc_NbaAmendEndorse.endType}" valueChangeListener="#{pc_NbaAmendEndorse.endTypeChangedListener}"
										onclick="submit()" immediate="true" style="position: relative; left: -5px">  <!-- NBA223 -->
										<f:selectItems value="#{pc_NbaAmendEndorse.actionsList}" />
									</h:selectOneRadio>
								</h:panelGroup>
							</h:panelGrid>
						</h:panelGroup>
						<h:panelGroup style=" position: relative; top: -12px">
							<h:selectBooleanCheckbox value="#{pc_NbaAmendEndorse.referToContract}" binding="#{pc_NbaAmendEndorse.compReferTo}"
								disabled="#{pc_NbaAmendEndorse.referToContractDisabled}" />
							<h:outputLabel value="#{property.uwAppliesToContract}" styleClass="formLabelRight" />  <!-- NBA223 -->
						</h:panelGroup>
					</h:panelGrid>
				</h:panelGroup>
				<f:verbatim>
					<hr class="formSeparator" />
				</f:verbatim>
				
				<!-- begin NBA223 -->
				<h:panelGrid columns="4">
					<h:column id="createUpdateAmendEndorseCol1">
						<h:outputLabel value="" style="text-align: left;width: 40px" />
					</h:column>
					<h:column id="createUpdateAmendEndorseCol2">
						<h:selectOneRadio value="#{pc_NbaAmendEndorse.typeRB}"  
							valueChangeListener="#{pc_NbaAmendEndorse.typeRBChangedListener}" 
							disabled="#{pc_NbaAmendEndorse.typeRBDisabled}"
							style="position: relative;"  
							onclick="submit()" 
							immediate="true" 
							layout="pageDirection"  >
							<f:selectItem id="type" itemValue="type"  />		
							<f:selectItem id="form" itemValue="form"  />		
						</h:selectOneRadio>
					</h:column>
					<h:column id="createUpdateAmendEndorseCol3">
						<h:outputLabel value="#{property.uwType}" styleClass="formLabelRight" style="width: 30px; margin-left:-3px;"/>
						<h:outputLabel value="#{property.uwForm}" styleClass="formLabelRight" style="width: 40px; padding-top:14px; margin-left:-3px;"/>
					</h:column>
					<h:column id="createUpdateAmendEndorseCol4">
						<h:panelGroup id="groupCodelist" styleClass="formDataEntryLine">
							<h:selectOneMenu styleClass="formEntryTextFull" style= "width: 450px;margin-right:30px;"
									value="#{pc_NbaAmendEndorse.endorsementCode}" valueChangeListener="#{pc_NbaAmendEndorse.typeSelectedListener}"
									disabled="#{pc_NbaAmendEndorse.endorsementCodeDisabled}"
									onchange="submit()">
								<f:selectItems value="#{pc_NbaAmendEndorse.endorsementCodeList}" />
							</h:selectOneMenu>
						</h:panelGroup>
						<h:panelGroup id="groupFormlist" styleClass="formDataEntryLine">
							<h:selectOneMenu styleClass="formEntryTextFull" style= "width: 450px;"
									value="#{pc_NbaAmendEndorse.form}" valueChangeListener="#{pc_NbaAmendEndorse.formDDChangedListener}"
									disabled="#{pc_NbaAmendEndorse.formDisabled}"
									onchange="submit()">
								<f:selectItems value="#{pc_NbaAmendEndorse.formList}" />
							</h:selectOneMenu>
						</h:panelGroup>
					</h:column>
				</h:panelGrid>
			
				<h:panelGroup id="createUpdateAmendEndorseusQuestion" styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.uwQuestionNumber}" styleClass="formLabel" />
					<h:selectOneMenu styleClass="formEntryTextFull" style= "width: 450px;"
							value="#{pc_NbaAmendEndorse.questionNumber}" valueChangeListener="#{pc_NbaAmendEndorse.questionSelectedListener}"
							disabled="#{pc_NbaAmendEndorse.questionDisabled}"
							onchange="submit()">
						<f:selectItems value="#{pc_NbaAmendEndorse.questionNumberList}" />
					</h:selectOneMenu>
				</h:panelGroup>
				
				<!-- End NBA223 -->
				
				<h:panelGroup id="createUpdateAmendEndorseusMsg" styleClass="formDataEntryLine">
					<h:outputLabel value="#{property.uwMessage}" styleClass="formLabel" style="vertical-align: top" />
					<h:inputTextarea value="#{pc_NbaAmendEndorse.message}" rows="5" styleClass="formEntryTextMultiLineFull"
						disabled="#{pc_NbaAmendEndorse.messageDisabled}" binding="#{pc_NbaAmendEndorse.endorseMsg}" style= "width: 450px;" />
				</h:panelGroup>
			<!-- NBA223 code deleted -->
			<!-- Button bar -->
			<h:panelGroup id="createUpdateAmendEndorseBut" styleClass="formButtonBar" style="margin-top: 20px">  <!-- NBA223 -->
				<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" immediate="true"
					action="#{pc_finalDispNavigation.actionCloseSubView}" onclick="resetTargetFrame();" />  <!-- NBA223, SPRNBA-576 -->
				<h:message for="btnCancel" />
	           <h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
					action="#{pc_NbaAmendEndorse.actionClear}"  rendered="#{pc_NbaAmendEndorse.createMode}" /><!-- NBA223 -->
				<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_NbaAmendEndorse.actionAddNew}"
					onclick="resetTargetFrame();"  rendered="#{pc_NbaAmendEndorse.createMode}" />
				<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_NbaAmendEndorse.actionAdd}"
					onclick="resetTargetFrame();"  rendered="#{pc_NbaAmendEndorse.createMode}" /> <!-- NBA223 -->
				<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_NbaAmendEndorse.actionUpdate}"
					onclick="resetTargetFrame();"  rendered="#{pc_NbaAmendEndorse.updateMode}" /> <!-- NBA223 -->
			</h:panelGroup>
				<!-- NBA223 code deleted -->
		</h:panelGroup>
	</h:panelGroup>
	<!-- ********************************************************
		View Amendment/Endorsement 
	 ******************************************************** -->
	<h:panelGroup id="viewAmendEndorse" styleClass="inputFormMat" rendered="#{pc_NbaAmendEndorse.viewMode}">
		<h:panelGroup id="viewContract2" styleClass="inputForm" style="height: 200px;">
			<!-- Table Header -->
			<h:panelGroup id="ViewTitleHeader" styleClass="formTitleBar">
				<h:outputLabel id="viewAmendment" value="#{property.uwCovViewAmmendmentTitle}" rendered="#{pc_NbaAmendEndorse.amendment}" styleClass="shTextLarge" />
				<h:outputLabel id="viewEndorsement" value="#{property.uwCovViewEndorsementTitle}" rendered="#{pc_NbaAmendEndorse.endorsement}"
					styleClass="shTextLarge" />
			</h:panelGroup>
			<!-- Display Fields -->
			<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeftBorder, formSplitRight" cellpadding="0" cellspacing="0">  <!-- SPR3232 -->
				<h:column>
					<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
						<h:outputLabel value="#{property.uwFor}" styleClass="popupLabel" />
						<h:outputText value="#{pc_NbaAmendEndorse.parentDescription}" />
					</h:panelGroup>
					<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
						<h:outputLabel value="#{property.uwAction}" styleClass="popupLabel" />
						<h:outputText value="#{property.uwAmendment}" rendered="#{pc_NbaAmendEndorse.amendment}" /> <!-- SPRNBA-583 -->
						<h:outputText value="#{property.uwEndorsement}" rendered="#{pc_NbaAmendEndorse.endorsement}" />
					</h:panelGroup>
				</h:column>
				<h:column>
					<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
						<h:outputLabel value="#{property.uwCease}" styleClass="popupLabel" />
						<h:outputText value="#{pc_NbaAmendEndorse.ceaseDate}">
							<f:convertDateTime pattern="#{property.datePattern}" />
						</h:outputText>
					</h:panelGroup>
					<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
						<h:outputText value="#{property.uwAppliesToContract}" rendered="#{pc_NbaAmendEndorse.referToContract}" styleClass="indEnabledTrue"
							style="font-weight: bold; margin-left: 78px;" />  <!-- NBA223 -->
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
			<f:verbatim>
				<hr class="formSeparator" />
			</f:verbatim>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.uwType}" styleClass="popupLabel" />
				<h:outputText value="#{pc_NbaAmendEndorse.endorsementCodeTranslated}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.uwMessage}" styleClass="popupLabel" style="vertical-align: top;" />
				<h:inputTextarea value="#{pc_NbaAmendEndorse.message}" rows="5" styleClass="formEntryTextMultiLineFull" style="width: 476px;" readonly="true" />
			</h:panelGroup>
			<!-- Button bar -->
			<f:verbatim>
				<hr class="formSeparator" />
			</f:verbatim>
			<h:panelGroup styleClass="formButtonBar">
				<h:commandButton id="btnClose" value="#{property.buttonClose}" styleClass="formButtonLeft" action="#{pc_finalDispNavigation.actionCloseSubView}" /> <!-- SPRNBA-576 -->
			</h:panelGroup>
			<!-- NBA223 code deleted -->
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
