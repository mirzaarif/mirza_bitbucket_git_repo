<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA212            7      Content Services -->
<!-- FNB011            	NB-1101	Work Tracking -->
<!-- NBA324 	 	NB-1301   nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->

	<h:panelGroup styleClass="pageHeader">

 		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon1}"
			title="#{pc_priorInsNav.icon1Title}"
			action="#{pc_priorInsNav.icon1Selected}"
			rendered="#{pc_priorInsNav.icon1Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon2}"
			title="#{pc_priorInsNav.icon2Title}"
			action="#{pc_priorInsNav.icon2Selected}"
			rendered="#{pc_priorInsNav.icon2Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon3}"
			title="#{pc_priorInsNav.icon3Title}"
			action="#{pc_priorInsNav.icon3Selected}"
			rendered="#{pc_priorInsNav.icon3Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon4}"
			title="#{pc_priorInsNav.icon4Title}"
			action="#{pc_priorInsNav.icon4Selected}"
			rendered="#{pc_priorInsNav.icon4Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon5}"
			title="#{pc_priorInsNav.icon5Title}"
			action="#{pc_priorInsNav.icon5Selected}"
			rendered="#{pc_priorInsNav.icon5Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon6}"
			title="#{pc_priorInsNav.icon6Title}"
			action="#{pc_priorInsNav.icon6Selected}"
			rendered="#{pc_priorInsNav.icon6Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon7}"
			title="#{pc_priorInsNav.icon7Title}"
			action="#{pc_priorInsNav.icon7Selected}"
			rendered="#{pc_priorInsNav.icon7Rendered}"
			style="position: relative; left: 15px; top: 3px" />
 
  		<h:commandButton
			image="images/paging/#{pc_priorInsNav.icon8}"
			title="#{pc_priorInsNav.icon8Title}"
			action="#{pc_priorInsNav.icon8Selected}"
			rendered="#{pc_priorInsNav.icon8Rendered}"
			style="position: relative; left: 15px; top: 3px" />

		<h:commandButton id="phiBriefCasePrior" image="images/phiBriefCase.gif" title="#{property.reviewPHI}" onclick="launchPHIBriefcaseView();" rendered="#{pc_reqimpNav.showPhiBriefcase}" style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"/> <!-- NBA324-->							 
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 40px"  rendered="#{pc_priorInsNav.auth.visibility['Images']}"/>		<!--  FNB011 NBA324 -->
		<h:commandButton image="images/link_icons/documents-stack.gif" onclick="setTargetFrame();" action="#{pc_priorInsNav.viewAllSourcesForUW}" style="position: absolute; margin-top: 2px; right: 15px; vertical-align: middle" rendered="#{pc_priorInsNav.auth.visibility['Images']}"/> <!-- NBA212 FNB011 NBA324-->
	</h:panelGroup>

</jsp:root>
