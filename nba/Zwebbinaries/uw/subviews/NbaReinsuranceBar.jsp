<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
		<h:panelGroup styleClass="commentBar">
		<h:commandButton id="lockClosed" image="images/comments/lock-closed-forbar.gif" onclick="setTargetFrame();" title="Test" action="" styleClass="commentIcon" style="position: absolute; left: 35%" />
		<h:commandButton id="lockOpen" image="images/comments/lock-open-forbar.gif" title="Test Open Lock" action="" onclick="setTargetFrame();" styleClass="commentIcon" style="position: absolute; left: 43%" />
		<h:commandButton id="exclamation" image="images/comments/exclamation-forbar.gif" title="Test Exclamation" action="" onclick="setTargetFrame();" styleClass="commentIcon" style="position: absolute; left: 50%" />
		<h:commandButton id="phone" image="images/comments/phone-forbar.gif" title="Test Phone" action="" onclick="setTargetFrame();" styleClass="commentIcon" style="position: absolute; left: 57%" />
		<h:commandButton id="envelope" image="images/comments/envelope-forbar.gif" title="Test Envelop" action="" onclick="setTargetFrame();" styleClass="commentIcon" style="position: absolute; left: 65%" />
		<h:commandLink value="ADD COMMENT" action="" immediate="true" rendered="true" styleClass="ovButtonRight" style = "width =100px;" />
	</h:panelGroup>
</jsp:root>