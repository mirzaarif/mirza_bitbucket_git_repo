<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<h:panelGroup id="reInsuranceHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="4" styleClass="ovTableHeader" columnClasses="ovColText155,ovColText155,ovColText155,ovColText155" cellspacing="0">
			<h:commandLink id="reInsuranceHdrCol1" value="Coverage" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol2" value="Insured" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol3" value="Contract Amount" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol4" value="Reinsured Amount" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
</jsp:root>