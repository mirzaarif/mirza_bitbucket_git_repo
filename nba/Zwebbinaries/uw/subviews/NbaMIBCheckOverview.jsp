<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2954           6      View MIB Report view - Tool tip needed for codes returned from MIB -->
<!-- SPR3013           6      Rquirements/Impairment Tab - Birth State is not displaying on the MIB Check detail view -->
<!-- NBA226            8      nba MIB Translation and validation -->
<!-- NBA308 		NB-1301	  MIB Follow Ups -->
<!-- SPRNBA-819		NB-1401   Occupation not displayed on MIB results view -->
<!-- NBA348         NB-1401   MIB Code Translation -->
<!-- NBA390     	NB-1601   nbA MIB Enhancements -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<!-- begin NBA308 -->
	<h:panelGroup styleClass="formDataDisplayTopLine">
		<h:outputLabel value="#{property.mibCompany}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.carrierCode}" styleClass="formDisplayText" style="width: 270px" />
		<h:outputLabel value="#{property.mibResponse}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.responseDate}" styleClass="formDisplayDate">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataDisplayLine">
		<h:outputLabel value="#{property.mibInqType}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.inquiryType}" styleClass="formDisplayText" style="width: 270px" />
		<h:outputLabel value="#{property.mibInqDate}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.inquiryDate}" styleClass="formDisplayDate">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataDisplayLine">
		<h:outputLabel value="#{property.mibMsgType}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.messageType}" styleClass="formDisplayText" style="width: 470px" /><!-- SPRNBA-819 -->
	</h:panelGroup>
	<!-- begin SPRNBA-819 -->
	<h:panelGroup styleClass="formDataDisplayLine">
		<h:outputLabel value="#{property.mibOccupation}" styleClass="formLabel" />
		<h:outputText value="#{pc_reqMIBCheck.occupation}" styleClass="formDisplayText" />
	</h:panelGroup>
	<!-- end SPRNBA-819 -->
	<!-- end NBA308 -->	
	<h:panelGroup styleClass="formDataDisplayLine" />
	<h:panelGroup styleClass="formDivTableHeader">
		<h:panelGrid columns="7" styleClass="formTableHeader"
				columnClasses="ovColHdrText150,ovColHdrText70,ovColHdrText70,ovColHdrText60,ovColHdrText60,ovColHdrText85,ovColHdrDate" cellspacing="0"><!-- SPR3013 NBA390-->
			<h:commandLink id="mibHdrCol1" value="#{property.mibChkCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol2" value="#{property.mibChkCol2}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol3" value="#{property.mibChkCol3}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol4" value="#{property.mibChkCol4}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol5" value="#{property.mibChkCol5}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol6" value="#{property.mibChkCol6}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibHdrCol7" value="#{property.mibChkCol7}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	
	<h:panelGroup id="mibCheckTable" styleClass="formDivTableData12">	<!-- NBA308 -->
		<!--begin NBA226 -->
		<h:dataTable id="mibsTable" styleClass="formTableData" cellspacing="0" rows="0"
					binding="#{pc_reqMIBCheck.resultsTable}" value="#{pc_reqMIBCheck.mibCheckResults}" var="mibResult"
					rowClasses="#{pc_reqMIBCheck.rowStyles}"
					columnClasses="ovColText495" ><!-- SPR3013 -->
			
					<h:column>
						<h:panelGrid id="subHeaderPGrid" columns="7" rendered="#{!mibResult.response}" styleClass="formTableData" cellspacing="0"
							style="overflow: hidden; background-color: transparent;" columnClasses="ovColText150,ovColText70,ovColText70,ovColText60,ovColText60,ovColText85,ovColDate">	<!-- NBA308 NBA390-->
								<h:column>
									<h:commandLink id="mibCol1Cl" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
										<h:outputText id="otName" value="#{mibResult.name}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>	<!-- NBA308 -->
								</h:column>
								<h:column>
									<h:panelGroup id="pGroup1"  >
										<h:outputText value="#{mibResult.match}" title="#{mibResult.matchSource}" styleClass="shText#{mibResult.draftText}"/>
										<h:outputText value="#{property.altName}" styleClass="shText#{mibResult.draftText}" rendered="#{mibResult.altNameInfoPresent}"/> <!-- NBA390 -->
										<h:commandButton image="./images/link_icons/circle_i_onrow.gif" onclick="setTargetFrame(); saveTableScrollPosition();" rendered="#{!mibResult.matched || mibResult.altNameInfoPresent}" action="#{pc_reqMIBCheck.matchCheck}" styleClass="ovAppendIconToTextCell" /> 	<!-- NBA308 --><!-- NBA390 -->
									</h:panelGroup>
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C3" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
										<h:outputText id="otRes" value="#{mibResult.resolution}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}"/>
									</h:commandLink>	<!-- NBA308 -->											
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C4" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
										<h:outputText id="otTer" value="#{mibResult.territory}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>	<!-- NBA308 -->											
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C5" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
										<h:outputText id="otComp" value="#{mibResult.company}" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}" />
									</h:commandLink>	<!-- NBA308 -->																					
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C6" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();"> 	<!-- NBA308 -->
										<!--  Begin SPR3013 -->
									<h:panelGroup id="pGroup12" style="white-space: nowrap;" styleClass="shText#{mibResult.draftText}">
											<h:outputText value="#{mibResult.birthDate}">
												<f:convertDateTime pattern="#{property.datePattern}" />
											</h:outputText>
											<h:outputText value="-" />
											<h:outputText value="#{mibResult.birthState}">
											</h:outputText>
										</h:panelGroup>
										<!--  End SPR3013 -->
									</h:commandLink>	<!-- NBA308 -->																					
								</h:column>
								<h:column>
									<h:commandLink id="mibCol1C7" action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
										<h:outputText id="otSubDate" value="#{mibResult.submitDate}" styleClass="shText#{mibResult.draftText}" >
											<f:convertDateTime pattern="#{property.datePattern}"/>
										</h:outputText>	
									</h:commandLink>	<!-- NBA308 -->																					
								</h:column>
						</h:panelGrid>
						
						<h:outputText id="otResponse" rendered="#{mibResult.response}">
							<h:panelGroup id="pGroup2" style="width:550px;" > <!-- NBA348 -->
								<h:graphicImage value="./images/hierarchies/T_onrow.gif" rendered="#{!mibResult.lastResponse}" style="margin-left: 5px" />
								<h:graphicImage value="./images/hierarchies/L_onrow.gif" rendered="#{mibResult.lastResponse}" style="margin-left: 5px" />
								<h:commandLink id="mibRsp1Cl"  action="#{pc_reqMIBCheck.selectRow}" immediate="true" onmousedown="saveTableScrollPosition();">	<!-- NBA308 -->
									<h:outputText value="#{mibResult.responseData}" title="#{mibResult.responseCompleteDescription}"
										style="vertical-align: top; padding-left: 2px; margin-top: 5px;" />  <!-- SPR2954 -->
								</h:commandLink>	<!-- NBA308 -->											
							</h:panelGroup>
						</h:outputText>
					</h:column>
		</h:dataTable>
		<!--end NBA226 -->
	</h:panelGroup>
	<h:inputHidden id="mibCheckTableVScroll" binding="#{pc_reqMIBCheck.scrollValue}" /> 	<!-- NBA308 -->
</jsp:root>