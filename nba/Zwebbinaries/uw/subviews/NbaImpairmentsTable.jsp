<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA212            7      Content Services -->
<!-- NBA224            8      nbA Underwriter Workbench Requirements and Impairments Enhancement Project -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="impairmentsHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="8" styleClass="ovTableHeader" columnClasses="ovColHdrIcon,ovColHdrText100,ovColHdrAmt,ovColHdrAmt,ovColHdrText60,ovColHdrText140,ovColHdrText70,ovColHdrDate" cellspacing="0"> <!--NBA224 -->
			<!-- begin NBA212 -->
			<h:commandLink id="impHdrCol1" value="#{property.impairmentCol1}" styleClass="ovColSorted#{pc_impTable.sortedByCol1}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol2" value="#{property.impairmentCol2}" styleClass="ovColSorted#{pc_impTable.sortedByCol2}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol3" value="#{property.impairmentCol3}" styleClass="ovColSorted#{pc_impTable.sortedByCol3}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol4" value="#{property.impairmentCol4}" styleClass="ovColSorted#{pc_impTable.sortedByCol4}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol8" value="#{property.impairmentCol8}" styleClass="ovColSorted#{pc_impTable.sortedByCol8}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" /> <!--NBA224 -->
			<h:commandLink id="impHdrCol5" value="#{property.impairmentCol5}" styleClass="ovColSorted#{pc_impTable.sortedByCol5}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol6" value="#{property.impairmentCol6}" styleClass="ovColSorted#{pc_impTable.sortedByCol6}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<h:commandLink id="impHdrCol7" value="#{property.impairmentCol7}" styleClass="ovColSorted#{pc_impTable.sortedByCol7}"
					actionListener="#{pc_impTable.sortColumn}" onmouseover="resetTargetFrame()" immediate="true" />
			<!--  end NBA212 -->
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="impairmentData" styleClass="ovDivTableData#{pc_impTable.rowCount}">
		<h:dataTable id="impairmentsTable" styleClass="ovTableData" cellspacing="0" rows="0"
					binding="#{pc_impTable.impairmentsTable}" value="#{pc_impTable.impairmentList}" var="imp"
					rowClasses="#{pc_impTable.rowStyles}"
					columnClasses="ovColIcon,ovColText100,ovColAmt,ovColAmt,ovColText60,ovColText140,ovColText70,ovColDate" > <!--NBA224 -->
			<h:column>
				<!-- begin NBA212 -->
				<h:commandButton id="impCol1a" image="images/needs_attention/flag-onwhite.gif" rendered="#{imp.pending}"
							styleClass="ovViewIconTrue" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
				<h:commandButton id="impCol1b" image="images/needs_attention/filledcircle-onwhite.gif" rendered="#{imp.underwriterResolved}"
							styleClass="ovViewIconTrue" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
				<h:commandButton id="impCol1c" image="images/needs_attention/zigzag-onwhite.gif" rendered="#{imp.systemResolved}"
							styleClass="ovViewIconTrue" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
				<h:commandButton id="impCol1d" image="images/needs_attention/redzigzag-onwhite.gif" rendered="#{imp.tenative}"
							styleClass="ovViewIconTrue" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
				<h:commandButton id="impCol1e" image="images/needs_attention/clear.gif"
							styleClass="ovViewIconFalse" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
				<!-- end NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="impCol2" value="#{imp.typeName}" styleClass="ovFullCellSelect" action="#{pc_impTable.selectRow}"
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="impCol3" value="#{imp.debit}" styleClass="ovFullCellSelect" style="width: 100%;" action="#{pc_impTable.selectRow}" 
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="impCol4" value="#{imp.credit}" styleClass="ovFullCellSelect" style="width: 100%;" action="#{pc_impTable.selectRow}" 
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
			<!-- begin NBA224 -->
			<h:column>				
				<h:commandButton id="impCol8" image="images/needs_attention/check-onwhite.gif" styleClass="ovViewIcon#{imp.restrict}" 
					style="margin-left: 20px;" action="#{pc_impTable.selectRow}" onclick="resetTargetFrame()" immediate="true" />
			</h:column>
			<!-- end NBA224 -->
			<h:column>
				<h:commandLink id="impCol5" value="#{imp.description}" styleClass="ovFullCellSelect" action="#{pc_impTable.selectRow}" 
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="impCol6" value="#{imp.statusText}" styleClass="ovFullCellSelect" action="#{pc_impTable.selectRow}" 
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
			<h:column>
				<h:commandLink id="impCol7" value="#{imp.date}" styleClass="ovFullCellSelect" action="#{pc_impTable.selectRow}" 
					onmouseover="resetTargetFrame()" immediate="true" />  <!-- NBA212 -->
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>