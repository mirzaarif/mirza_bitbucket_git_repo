<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<h:panelGroup id="priorInsHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="8" styleClass="ovTableHeader" columnClasses="ovColHdrText200,ovColHdrText75,ovColHdrText155,ovColHdrIcon,ovColHdrDate,ovColHdrDate" cellspacing="0">
			<h:commandLink id="priorInsHdrCol1" value="#{property.priorInsCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol2" value="#{property.priorInsCol2}" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol3" value="#{property.priorInsCol3}" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol4" value="  " styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol5" value="#{property.priorInsCol5}" styleClass="ovColSortedFalse" />
			<h:commandLink id="priorInsHdrCol6" value="#{property.priorInsCol6}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
</jsp:root>