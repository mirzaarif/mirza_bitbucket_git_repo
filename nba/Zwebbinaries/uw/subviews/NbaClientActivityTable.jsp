<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />	
	<!-- Table Title Bar -->
	<h:panelGroup id="clientActivityTitleHeader" styleClass="sectionHeader">
		<h:outputLabel id="clientActivityTableTitle1" value="#{property.clientViewActivityTitle}" 
			rendered="#{!pc_NbaClientActivityDetails.entryAllowed}" styleClass="shTextLarge" />
		<h:outputLabel id="clientActivityTableTitle2" value="#{property.clientUpdateActivityTitle}" 
			rendered="#{pc_NbaClientActivityDetails.updateMode}" styleClass="shTextLarge" />
		<h:outputLabel id="clientActivityTableTitle3" value="#{property.clientCreateActivityTitle}" 
			rendered="#{pc_NbaClientActivityDetails.createMode}" styleClass="shTextLarge" />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="clientActivityHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="2" styleClass="ovTableHeader" columnClasses="ovColHdrText295,ovColHdrText295" cellspacing="0">
			<h:outputLabel id="clientActivityHdrCol1" value="#{property.clientActivityCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientActivityHdrCol2" value="#{property.clientActivityCol2}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="ovDivTableData" style="height: 115px;">
		<h:dataTable id="clientActivityTable" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientActivityTableData.htmlDataTable}"
			value="#{pc_NbaClientActivityTableData.clientActivityRows}" var="nbaActivity" rowClasses="#{pc_NbaClientActivityTableData.rowStyles}"
			columnClasses="ovColText295,ovColText295">
			<h:column>
				<h:commandLink id="clientActivityCol1" value="#{nbaActivity.col1}" styleClass="ovFullCellSelect" action="#{pc_NbaClientActivityTableData.selectClientActivityRow}" 
					immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientActivityCol2" value="#{nbaActivity.col2}" styleClass="ovFullCellSelect" action="#{pc_NbaClientActivityTableData.selectClientActivityRow}"
					immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
