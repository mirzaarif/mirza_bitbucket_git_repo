<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDataDisplayLine" />
	<h:panelGroup styleClass="formDivTableHeaderRightButtons">
		<h:panelGrid columns="7" styleClass="formTableHeaderRightButtons" columnClasses="ovColHdrText70,ovColHdrDate,ovColHdrText75,ovColHdrText50,ovColHdrText75,ovColHdrDate,ovColHdrText70" cellspacing="0">
			<h:commandLink id="mibDataCol1" value="#{property.mibDataCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol2" value="#{property.mibDataCol2}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol3" value="#{property.mibDataCol3}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol4" value="#{property.mibDataCol4}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol5" value="#{property.mibDataCol5}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol6" value="#{property.mibDataCol6}" styleClass="ovColSortedFalse" />
			<h:commandLink id="mibDataCol7" value="#{property.mibDataCol7}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<h:column>
			<h:panelGroup styleClass="formDivTableDataRightButtons5">
				<h:dataTable id="mibs" rows="0" styleClass="formTableData" style="width: 500px;" cellspacing="0"
							binding="#{pc_reqMIBReports.optionsTable}" value="#{pc_reqMIBReports.optionNotes}" var="note"
							rowClasses="#{pc_reqMIBReports.optionsRowStyles}"
							columnClasses="ovColText70,ovColDate,ovColText75,ovColText50,ovColText75,ovColDate,ovColText70" >
						<h:column>
							<h:commandLink value="#{note.type}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.submitted}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.firstName}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.middleInitial}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.lastName}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.birthDateText}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
						<h:column>
							<h:commandLink value="#{note.birthStateText}" styleClass="ovFullCellSelect" action="#{pc_reqMIBReports.selectOptionRow}" immediate="true" />
						</h:column>
				</h:dataTable>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup>
				<h:commandButton image="./images/link_icons/circle_i.gif" style="margin-left: 32px; margin-bottom: 10px; max-height: 26px"
								disabled="#{pc_reqMIBReports.disabled}" onclick="setTargetFrame()" action="#{pc_reqMIBReports.addOptionNotes}" />
				<h:commandLink value="#{property.buttonDelete}" styleClass="formButtonInterface" style="padding-top: 6px; margin-left: 10px"
								rendered="#{!pc_reqMIBReports.disabled}" onmouseover="resetTargetFrame()" onkeydown="resetTargetFrame()" action="#{pc_reqMIBReports.deleteOptionNotes}" />
				<h:outputText value="#{property.buttonDelete}" styleClass="formButtonInterface" style="padding-top: 6px; margin-left: 10px"
								rendered="#{pc_reqMIBReports.disabled}" />
			</h:panelGroup>
		</h:column>
	</h:panelGrid>
</jsp:root>