<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="personBusinessBuySellSection"  rendered="#{pc_NbaClientDetails.businessBuySellApplicable}">
		<h:panelGroup id="clientPersonBusinessBuySellHeader" styleClass="sectionSubheader">
			<h:outputLabel id="clientPersonBusinessBuySellTitle" value="#{property.personBusinessBuySellTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="personBusinessBuySellSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="personPercentOwnership1" value="#{property.personPercentOwnership}" styleClass="formLabel" />
			<h:outputText id="personPercentOwnershipText1" value="#{pc_NbaClientDetails.percentOwnership}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertNumber type="percent" pattern="###.##" />
			</h:outputText>		
			<h:outputLabel id="personPriorBusinessInsuranceAmount1" value="#{property.personPriorBusinessInsuranceAmount}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="personPriorBusinessInsuranceAmountText1" value="#{pc_NbaClientDetails.priorBusinessInsAmt}" styleClass="formDisplayText"  >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>		
		</h:panelGroup>		
	</h:panelGroup>	
	
	<h:panelGroup id="personBusinessKeyPersonSection"  rendered="#{pc_NbaClientDetails.businessKeyPersonApplicable}">
		<h:panelGroup id="clientPersonBusinessKeyPersonHeader" styleClass="sectionSubheader">
			<h:outputLabel id="clientPersonBusinessKeyPersonTitle" value="#{property.personBusinessKeyPersonTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="personBusinessKeyPersonSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="personJobTitle" value="#{property.personJobTitle}" styleClass="formLabel" />
			<h:outputText id="personJobTitleText" value="#{pc_NbaClientDetails.jobTitle}" styleClass="formDisplayText" style="width: 150px" />
			<h:outputLabel id="personEmployedFullTime" value="#{property.personEmployedFullTime}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="personEmployedFullTimeText" value="#{pc_NbaClientDetails.employesFullTime}" styleClass="formDisplayText" />				
		</h:panelGroup>	
		<h:panelGroup id="personBusinessKeyPersonSubSection2" styleClass="formDataDisplayLine">
			<h:outputLabel id="personYearsToReplace" value="#{property.personYearsToReplace}" styleClass="formLabel" />
			<h:outputText id="personYearsToReplaceText" value="#{pc_NbaClientDetails.yearsToReplace}" styleClass="formDisplayText" style="width: 150px" >
				<f:convertNumber integerOnly="true" type="integer" />
			</h:outputText>		
			<h:outputLabel id="personYearsOfExperience" value="#{property.personYearsOfExperience}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="personYearsOfExperienceText" value="#{pc_NbaClientDetails.yearsOfExperience}" styleClass="formDisplayText"  >
				<f:convertNumber integerOnly="true" type="integer" />
			</h:outputText>		
		</h:panelGroup>	
		<h:panelGroup id="personBusinessKeyPersonSubSection3" styleClass="formDataDisplayLine">
			<h:outputLabel id="personAnnualSalary" value="#{property.personAnnualSalary}" styleClass="formLabel" />
			<h:outputText id="personAnnualSalaryText" value="#{pc_NbaClientDetails.annualSalary}" styleClass="formDisplayText"  style="width: 100px" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>		
			<h:outputLabel id="personBonusAndOtherCompensation" value="#{property.personBonusAndOtherCompensation}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="personBonusAndOtherCompensationText" value="#{pc_NbaClientDetails.bonusOtherCompensation}" styleClass="formDisplayText"  >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>		
		</h:panelGroup>	
		<h:panelGroup id="personBusinessKeyPersonSubSection4" styleClass="formDataDisplayLine">
			<h:outputLabel id="personPercentOwnership2" value="#{property.personPercentOwnership}" styleClass="formLabel" />
			<h:outputText id="personPercentOwnershipText2" value="#{pc_NbaClientDetails.percentOwnership}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertNumber type="percent" pattern="###.##" />
			</h:outputText>		
			<h:outputLabel id="personPriorBusinessInsuranceAmount2" value="#{property.personPriorBusinessInsuranceAmount}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="personPriorBusinessInsuranceAmountText2" value="#{pc_NbaClientDetails.priorBusinessInsAmt}" styleClass="formDisplayText"  >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>		
		</h:panelGroup>				
		<h:panelGroup id="personBusinessKeyPersonSubSection5" styleClass="formDataDisplayLine">
			<h:outputLabel id="personOtherApplications" value="#{property.personOtherApplications}" styleClass="formLabel" />
			<h:outputText id="personOtherApplicationsText" value="#{pc_NbaClientDetails.otherApplications}" styleClass="formDisplayText" />				
		</h:panelGroup>	
	</h:panelGroup>		
</jsp:root>

