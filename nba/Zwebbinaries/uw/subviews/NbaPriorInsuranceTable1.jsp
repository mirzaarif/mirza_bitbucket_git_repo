<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- FNB013 		NB-1101	        DI Support for nbA -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	
	<h:panelGroup styleClass="sectionHeader">
		<h:outputLabel value="#{pc_priorInsTable.insuredName1}" styleClass="shTextLarge"/>		
		<h:outputText value="#{pc_priorInsTable.insuredGender1}" styleClass="shText" style="position: absolute; left: 330px; width: 50px; text-align: center" />
		<h:outputText value="#{property.birthDate}" styleClass="shText" style="position: absolute; left: 422px" />
		<h:outputText value="#{pc_priorInsTable.insuredBirthDate1}" styleClass="shText" style="position: absolute; left: 462px">
			<f:convertDateTime pattern="#{property.datePattern}"/>
		</h:outputText>
		<h:outputFormat value="{0} {1}" styleClass="shText" style="position: absolute; right: 15px">
			<f:param value="#{property.age}" />
			<f:param value="#{pc_priorInsTable.insuredAge1}" />
		</h:outputFormat>
	</h:panelGroup>
	
	<f:subview id="tableHeader">
		<c:import url="/uw/subviews/NbaPriorInsuranceHeader.jsp" />
	</f:subview>
	
	<h:panelGroup id="priorInsData1" styleClass="ovDivTableData">
		<h:dataTable id="priorInsTable1" styleClass="ovTableData" cellspacing="0" rows="0"
					binding="#{pc_priorInsTable.clientTable1}" value="#{pc_priorInsTable.clientList1}" var="priorIns"
					rowClasses="#{pc_priorInsTable.client1RowStyles}"
					columnClasses="ovColText200,ovColText75,ovColText155,ovColIcon,ovColDate,ovColDate" >
				<h:column>
					<h:panelGroup>
						<h:commandButton id="priorInsIcon1" image="images/hierarchies/#{priorIns.icon1}" rendered="#{priorIns.icon1Rendered}"
							styleClass="#{priorIns.icon1StyleClass}" style="margin-left: 5px; margin-top: -6px;" />
						<h:commandButton id="priorInsIcon2" image="images/hierarchies/#{priorIns.icon2}" rendered="#{priorIns.icon2Rendered}"
							styleClass="#{priorIns.icon2StyleClass}" style="margin-left: 10px; margin-top: -6px;" />
						<h:commandButton id="priorInsIcon3" image="images/hierarchies/#{priorIns.icon3}" rendered="#{priorIns.icon3Rendered}"
							styleClass="#{priorIns.icon3StyleClass}" style="margin-left: 15px; margin-top: -6px;" />
						<h:commandLink id="priorInsCol1" value="#{priorIns.itemCol}" styleClass=" #{priorIns.cell1Style}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true"/>
					</h:panelGroup>
				</h:column>
				<h:column>
					<h:commandLink id="priorInsCol2" value="#{priorIns.status}" styleClass="#{priorIns.cell2Style}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true" />
				</h:column>
				<h:column>
					<h:commandLink id="priorInsCol3" value="#{priorIns.detail}" styleClass="#{priorIns.cell3Style}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true" />
				</h:column>
				<h:column>
					<h:commandLink id="priorInsCol4" value="#{priorIns.dbIndicator}" styleClass="#{priorIns.cell4Style}" title="#{priorIns.deathBenefitToolTip}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true"/>
				</h:column>
				<h:column>
					<h:commandLink id="priorInsCol5" value="#{priorIns.effectiveDate}" styleClass="#{priorIns.cell5Style}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true"/>
				</h:column>
				<h:column>
					<h:commandLink id="priorInsCol6" value="#{priorIns.ceaseDate}" styleClass="#{priorIns.cell6Style}" action="#{pc_priorInsTable.selectClient1Row}" immediate="true"/>
				</h:column>
		</h:dataTable>
	</h:panelGroup>
	
	<h:panelGroup styleClass="ovStatusBar">
		<h:commandLink value="#{property.uwPriorInsPendingInforce}" styleClass="ovStatusBarTextBold" style="color: #ffffff;" title="#{pc_priorInsTable.pendingInforceToolTip1}"/>		
		<h:outputText value="#{pc_priorInsTable.totalPendingInforce1}" styleClass="ovStatusBarTextBold" style="font-weight: normal;"/>
		<h:outputFormat value="{0} {1}" styleClass="ovStatusBarTextBold" style="font-weight: normal; position: absolute; left: 40%">
			<f:param value="#{property.uwPriorInsAdditional}"></f:param>
			<f:param value="#{pc_priorInsTable.totalAdditional1}"></f:param>
		</h:outputFormat>
		
		<h:outputFormat value="{0} {1}" styleClass="ovStatusBarTextBold" style="font-weight: normal; position: absolute; left: 70%">
			<f:param value="#{property.uwPriorInsHighestAlt}"></f:param>
			<f:param value="#{pc_priorInsTable.totalHighestAlt1}"></f:param>
		</h:outputFormat>
	</h:panelGroup>
	<!-- Begin FNB013-->
	<h:panelGroup styleClass="ovStatusBar" rendered="#{pc_priorInsTable.diRowRenderForClient1}" >
		<h:commandLink value="#{property.uwPriorInsPendingInforceDI}" styleClass="ovStatusBarTextBold" style="color: #ffffff;" title="#{pc_priorInsTable.pendingInforceToolTipDI1}"/>		
		<h:outputText value="#{pc_priorInsTable.totalPendingInforceDI1}" styleClass="ovStatusBarTextBold" style="font-weight: normal;"/>
		<h:outputFormat value="{0} {1}" styleClass="ovStatusBarTextBold" style="font-weight: normal; position: absolute; left: 40%">
			<f:param value="#{property.uwPriorInsAdditional}"></f:param>
			<f:param value="#{pc_priorInsTable.totalAdditionalDI1}"></f:param>
		</h:outputFormat>
		
		<h:outputFormat value="{0} {1}" styleClass="ovStatusBarTextBold" style="font-weight: normal; position: absolute; left: 70%">
			<f:param value="#{property.uwPriorInsHighestAlt}"></f:param>
			<f:param value="#{pc_priorInsTable.totalHighestAltDI1}"></f:param>
		</h:outputFormat>
	</h:panelGroup>
	<!-- End FNB013-->
</jsp:root>