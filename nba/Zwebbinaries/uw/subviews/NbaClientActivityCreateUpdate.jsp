<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" 
	xmlns:h="http://java.sun.com/jsf/html" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="activityDetailsSection1" style="background-color: white; overflow: scroll;">
		<h:panelGroup id="activityDetailsSubSection1" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection1}" >
			<h:outputLabel id="activityType" value="#{property.activityType}" styleClass="formLabel" style="width: 175px"/>
			<h:selectOneMenu id="activityTypeText" styleClass="formDisplayText" style="width: 250px;" value="#{pc_NbaClientActivityDetails.activityType}" valueChangeListener="#{pc_NbaClientActivityDetails.activityChange}" 
				disabled="#{!pc_NbaClientActivityDetails.createMode || pc_NbaClientActivityDetails.activityLifeStyleActivity}" 
				onchange="submit()" immediate="true" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityTypeList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection2" styleClass="formDataDisplayLine">
			<h:panelGroup id="activityDetailsSubSection2a" style="width: 300px" rendered="#{pc_NbaClientActivityDetails.renderSection2a}" >
				<h:selectBooleanCheckbox id="activityMovingViolationCheckBox" value="#{pc_NbaClientActivityDetails.activityMovingViolation}" valueChangeListener="#{pc_NbaClientActivityDetails.movingViolationChange}" 
					disabled="#{pc_NbaClientActivityDetails.activityMovingViolation}" 
					onclick="submit()" immediate="true" styleClass="formEntryCheckbox" style="position: relative; left: 50px; width: 150px; padding-left: 120px" />
				<h:outputLabel id="activityMovingViolation" value="#{property.activityMovingViolation}" styleClass="formLabel" style="text-align: left; width: 150px;"/>			
			</h:panelGroup>		
			<h:panelGroup id="activityDetailsSubSection2b" style="width: 300px" rendered="#{pc_NbaClientActivityDetails.renderSection2b}" >
				<h:selectBooleanCheckbox id="activityAdditionalRiskCheckBox" value="#{pc_NbaClientActivityDetails.activityAdditionalRisk}" valueChangeListener="#{pc_NbaClientActivityDetails.additionalRiskChange}" 
					disabled="#{pc_NbaClientActivityDetails.activityAdditionalRisk || pc_NbaClientActivityDetails.disableAdditionalRisk}" 
					onclick="submit()" immediate="true" styleClass="formEntryCheckbox" style="position: relative; left: 25px; width: 150px; padding-left: 120px" />
				<h:outputLabel id="activityAdditionalRisk" value="#{property.activityAdditionalRisk}" styleClass="formLabel" style="text-align: left;"/>			
			</h:panelGroup>				
		</h:panelGroup>				
		<h:panelGroup id="activityDetailsSubSection3" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection3}">
			<h:outputLabel id="activityAviationType" value="#{property.activityAviationType}" styleClass="formLabel"  style="width: 175px" />
			<h:selectOneMenu id="activityAviationTypeText" styleClass="formDisplayText" style="width: 250px;" value="#{pc_NbaClientActivityDetails.activityAviationType}" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityAviationTypeList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection4" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection4}">
			<h:outputLabel id="activityPurpose" value="#{property.activityPurpose}" styleClass="formLabel" style="width: 175px"/>
			<h:selectOneMenu id="activityPurposeText" styleClass="formDisplayText" style="width: 250px;" value="#{pc_NbaClientActivityDetails.activityPurpose}" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityPurposeTypeList}" />
			</h:selectOneMenu>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection5" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection5}">
			<h:outputLabel id="activityAircraftType" value="#{property.activityAircraftType}" styleClass="formLabel" style="width: 175px"/>
			<h:selectOneMenu id="activityAircraftTypeText" styleClass="formDisplayText" style="width: 250px;" value="#{pc_NbaClientActivityDetails.activityAircraftType}" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityAircraftTypeList}" />
			</h:selectOneMenu>
			<h:outputLabel id="activityQuestionnaire" value="#{property.activityQuestionnaire}" styleClass="formLabel" style="font-weight: bold; width: 155px"/>			
			<h:commandButton image="images/link_icons/circle_i.gif" style="vertical-align: bottom" onclick="setTargetFrame();" action="#{pc_NbaClientActivityDetails.openQuestionnaire}" immediate="true"/>			
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection6" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection6}">
			<h:outputLabel id="activityHighestQualificationLevel" value="#{property.activityHighestQualificationLevel}" styleClass="formLabel" style="width: 175px"/>
			<h:selectOneMenu id="activityHighestQualificationLevelText" styleClass="formDisplayText" style="width: 250px;" value="#{pc_NbaClientActivityDetails.activityHighestQualificationLevel}" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityQualificationLevelList}" />
			</h:selectOneMenu>
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection7" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection7}">
			<h:outputLabel id="activityMonthsOfExperience" value="#{property.activityMonthsOfExperience}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityMonthsOfExperienceText" value="#{pc_NbaClientActivityDetails.activityMonthsOfExperience}" styleClass="formDisplayText" style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputLabel id="months_1" value="#{property.activityMonths}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>
			<h:selectBooleanCheckbox id="activityHazardousAreasCheckBox" value="#{pc_NbaClientActivityDetails.activityHazardousAreas}"  styleClass="formEntryCheckbox" style="width: 40px;" />
			<h:outputLabel id="activityHazardousAreas" value="#{property.activityHazardousAreas}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleHazardousAreas}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection8" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection8}">
			<h:outputLabel id="activityRecordAttempts" value="#{property.activityRecordAttempts}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityRecordAttemptsText" value="#{pc_NbaClientActivityDetails.activityRecordAttempts}" styleClass="formDisplayText"   style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>			
			<h:outputLabel id="total_1" value="#{property.activityTotal}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>
			<h:selectBooleanCheckbox id="activityCompetitionActivityCheckBox" value="#{pc_NbaClientActivityDetails.activityCompetitionActivity}"  styleClass="formEntryCheckbox" style="width: 40px;" />
			<h:outputLabel id="activityCompetitionActivity" value="#{property.activityCompetitionActivity}" styleClass="formLabel" style="width:130px; text-align: left; #{pc_NbaClientActivityDetails.styleCompetitionActivity}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection9" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection9}">
			<h:outputLabel id="activityTotal" value="#{property.activityTotalHours}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityTotalText" value="#{pc_NbaClientActivityDetails.activityTotal}" styleClass="formDisplayText"   style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputLabel id="hours_1" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>			
			<h:selectBooleanCheckbox id="activityTethredOrIFRCheckBox" value="#{pc_NbaClientActivityDetails.activityTethredOrIFR}"  styleClass="formEntryCheckbox" style="width: 40px;" />
			<h:outputLabel id="activityIFR" value="#{property.activityIFR}" styleClass="formLabel" style="text-align: left; #{pc_NbaClientActivityDetails.styleIFR}" rendered="#{pc_NbaClientActivityDetails.renderIFR}"/>
			<h:outputLabel id="activityTethred" value="#{property.activityTethred}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleTethred}" rendered="#{!pc_NbaClientActivityDetails.renderIFR}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection10" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection10}">
			<h:outputLabel id="activityLast12Months" value="#{property.activityLast12Months}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityLast12MonthsText" value="#{pc_NbaClientActivityDetails.activityLast12Months}" styleClass="formDisplayText"  style="width: 100px;" >
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>	
			<h:outputLabel id="hours_2" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>					
			<h:selectBooleanCheckbox id="activityCoPilotPresentCheckBox" value="#{pc_NbaClientActivityDetails.activityCoPilotPresent}"  styleClass="formEntryCheckbox" style="width: 40px;" />
			<h:outputLabel id="activityCoPilotPresent" value="#{property.activityCoPilotPresent}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleCoPilotPresent}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection11" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection11}">
			<h:outputLabel id="activityPrevious1To2Years" value="#{property.activityPrevious1To2Years}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityPrevious1To2YearsText" value="#{pc_NbaClientActivityDetails.activityPrevious1To2Years}" styleClass="formDisplayText"   style="width: 100px;" >
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>		
			<h:outputLabel id="hours_3" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>				
			<h:selectBooleanCheckbox id="activityFutureAviationCheckBox" value="#{pc_NbaClientActivityDetails.activityFutureAviation}"  styleClass="formEntryCheckbox" style="width: 40px;" />
			<h:outputLabel id="activityFutureAviation" value="#{property.activityFutureAviation}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleFutureAviation}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection12" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection12}">
			<h:outputLabel id="activityPlannedNext12Months" value="#{property.activityPlannedNext12Months}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityPlannedNext12MonthsText" value="#{pc_NbaClientActivityDetails.activityPlannedNext12Months}" styleClass="formDisplayText"   style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputLabel id="hours_4" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>						
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection13" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection13}">
			<h:outputLabel id="activityAverageHoursPerYear" value="#{property.activityAverageHoursPerYear}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityAverageHoursPerYearText" value="#{pc_NbaClientActivityDetails.activityAverageHoursPerYear}" styleClass="formDisplayText"  style="width: 100px;" >
				<f:convertNumber type="number" />
			</h:inputText>
			<h:outputLabel id="hours_5" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>						
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection14" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection14}">
			<h:outputLabel id="activityIFRHours" value="#{property.activityIFRHours}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityIFRHoursText" value="#{pc_NbaClientActivityDetails.activityIFRHours}" styleClass="formDisplayText"   style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>	
			<h:outputLabel id="hours_6" value="#{property.activityHours}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>					
		</h:panelGroup>			
		<h:panelGroup id="activityDetailsSubSection15" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection15}">
			<h:outputLabel id="activityFAAViolationType" value="#{property.activityFAAViolationType}" styleClass="formLabel" style="width: 175px"/>
			<h:selectOneMenu id="activityFAAViolationTypeText" styleClass="formDisplayText" value="#{pc_NbaClientActivityDetails.activityFAAViolationType}"  style="width: 100px;">
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityFAAViolationTypeList}" />
			</h:selectOneMenu>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection16" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection16}">
			<h:outputLabel id="activityTotalViolations" value="#{property.activityTotalViolations}" styleClass="formLabel" style="width: 175px"/>
			<h:inputText id="activityTotalViolationsText" value="#{pc_NbaClientActivityDetails.activityTotalViolations}" styleClass="formDisplayText"  style="width: 100px;" >
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputLabel id="total_2" value="#{property.activityTotal}" styleClass="formDisplayText" style="width: 160px; padding-left: 10px; text-align: left;"/>	
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection17" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection17}">
			<h:outputLabel id="activityJobChangeCount" value="#{property.activityJobChangeCount}" styleClass="formLabel" style="width: 150px"/>
			<h:inputText id="activityJobChangeCountText" value="#{pc_NbaClientActivityDetails.activityJobChangeCount}" styleClass="formDisplayText"   style="width: 100px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>			
			<h:selectBooleanCheckbox id="activityHazardousActivitiesCheckBox" value="#{pc_NbaClientActivityDetails.activityHazardousActivities}"  styleClass="formEntryCheckbox" style="position: relative;left: 100px;width: 40px;" />
			<h:outputLabel id="activityHazardousActivities" value="#{property.activityHazardousActivities}" styleClass="formLabel" style="position: relative;left: 110px; width:150px; text-align: left; #{pc_NbaClientActivityDetails.styleHazardousActivities}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection18" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection18}">
			<h:outputLabel id="activityViolationType" value="#{property.activityViolationType}" styleClass="formLabel"/>
			<h:selectOneMenu id="activityViolationTypeText" styleClass="formDisplayText" value="#{pc_NbaClientActivityDetails.activityViolationType}" >
				<f:selectItems value="#{pc_NbaClientActivityDetails.activityViolationTypeList}" />
			</h:selectOneMenu>			
		</h:panelGroup>
		<h:panelGroup id="activityDetailsSubSection19" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection19}">
			<h:outputLabel id="activityViolationDate" value="#{property.activityViolationDate}" styleClass="formLabel"/>
			<h:inputText id="activityAviationDateText" value="#{pc_NbaClientActivityDetails.activityViolationDate}" styleClass="formDisplayText" >
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		
		
		<f:verbatim>
			<hr id="hr_ActivityUpdate_1"/>
		</f:verbatim>
		
		<h:panelGroup id="activityViewButtonBar" styleClass="formButtonBar">
			<h:commandButton value="#{property.buttonCancel}" styleClass="formButtonLeft" onclick="resetTargetFrame();" action="#{pc_NbaClientActivityDetails.cancel}" immediate="true"/>
			<h:commandButton value="#{property.buttonAddNew}" styleClass="formButtonRight-1" onclick="resetTargetFrame();" action="#{pc_NbaClientActivityDetails.addNew}" rendered="#{pc_NbaClientActivityDetails.createMode}" />
			<h:commandButton value="#{property.buttonAdd}" styleClass="formButtonRight" onclick="resetTargetFrame();" action="#{pc_NbaClientActivityDetails.add}" rendered="#{pc_NbaClientActivityDetails.createMode}" />
			<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight" onclick="resetTargetFrame();" action="#{pc_NbaClientActivityDetails.update}" rendered="#{pc_NbaClientActivityDetails.updateMode}"/>
		</h:panelGroup>			
	</h:panelGroup>		
</jsp:root>

