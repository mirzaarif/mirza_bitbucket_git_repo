<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!--  NBA186  		8			nbA Underwriter Additional Approval and Referral Project -->
<!-- SPRNBA-606    NB-1301      Final Disposition Decision Popup Does Not Follow UI Standards -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" 
	xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Column Headers -->
	<!-- begin SPRNBA-606 -->
	<h:panelGroup id="decisionHeader" styleClass="popupDivTableHeader" style="width: 595px">
		<h:panelGrid columns="4" styleClass="popupTableHeader" columnClasses="popupColHdrText" cellspacing="0" cellpadding="0" style="width: 575px; table-layout: auto;">
			<h:commandLink id="initHdrCol1" value="#{property.uwfinalDispInitDecCol1}" styleClass="ovColSorted#{pc_initialDecisionTable.sortedByInitCol1}" actionListener="#{pc_initialDecisionTable.sortColumn}" style="width:70px"/>
			<h:commandLink id="initHdrCol2" value="#{property.uwfinalDispInitDecCol2}" styleClass="ovColSorted#{pc_initialDecisionTable.sortedByInitCol2}" actionListener="#{pc_initialDecisionTable.sortColumn}" style="width:105px"/>
			<h:commandLink id="initHdrCol3" value="#{property.uwfinalDispInitDecCol3}" styleClass="ovColSorted#{pc_initialDecisionTable.sortedByInitCol3}" actionListener="#{pc_initialDecisionTable.sortColumn}" style="width:105px"/>
			<h:commandLink id="initHdrCol4" value="#{property.uwfinalDispInitDecCol4}" styleClass="ovColSorted#{pc_initialDecisionTable.sortedByInitCol4}" actionListener="#{pc_initialDecisionTable.sortColumn}" style="width:295px"/>
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup styleClass="popupDivTableData6" style="width: 595px">
		<h:dataTable id="initialDecisionTable" styleClass="popupTableData" cellspacing="0" rows="0" 
			binding="#{pc_initialDecisionTable.dataTable}" value="#{pc_initialDecisionTable.initDscnTableList}" var="initialDecision" 
			columnClasses="popupColText" style="width: 575px">
			<h:column> 
				<h:outputText id="decisionCol1" value="#{initialDecision.decisionLevel}" styleClass="ovFullCellSelect" style="width:70px"/>
			</h:column>
			<h:column>
				<h:outputText id="decisionCol2" value="#{initialDecision.underwriterId}" styleClass="ovFullCellSelect" style="width:105px"/>
			</h:column>
			<h:column>
				<h:outputText id="decisionCol3" value="#{initialDecision.decisionDate}" styleClass="ovFullCellSelect" style="width:105px"/>
			</h:column>
			<h:column>
				<h:outputText id="decisionCol4" value="#{initialDecision.decision}" styleClass="ovFullCellSelect" style="width:295px"/>
			</h:column>				
		</h:dataTable>
	</h:panelGroup>
	<!-- end SPRNBA-606 -->
</jsp:root>