<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA309        	NB-1301   Pharmaceutical Information -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<div id="prescriptionDetails" >
		<h:panelGroup styleClass="formDataDisplayLine"/>
		<h:panelGroup styleClass="formDivTableHeader">
			<h:panelGrid columns="5" styleClass="formTableHeader" columnClasses="ovColHdrIcon,ovColHdrText300,ovColHdrText70, ovColHdrText60,ovColHdrDate" cellspacing="0">
				<h:commandLink id="prescriptionHdrCol1" value="#{property.prescriptionCol1}" styleClass="ovColSorted#{pc_reqPrescriptionInformation.sortedByCol1}" actionListener="#{pc_reqPrescriptionInformation.sortColumn}" onmouseover="resetTargetFrame()" immediate="true"/>
				<h:commandLink id="prescriptionHdrCol2" value="#{property.prescriptionCol2}" styleClass="ovColSorted#{pc_reqPrescriptionInformation.sortedByCol2}" actionListener="#{pc_reqPrescriptionInformation.sortColumn}" onmouseover="resetTargetFrame()" immediate="true"/>
				<h:commandLink id="prescriptionHdrCol3" value="#{property.prescriptionCol3}" styleClass="ovColSorted#{pc_reqPrescriptionInformation.sortedByCol3}" actionListener="#{pc_reqPrescriptionInformation.sortColumn}" onmouseover="resetTargetFrame()" immediate="true"/>
				<h:commandLink id="prescriptionHdrCol4" value="#{property.prescriptionCol4}" styleClass="ovColSorted#{pc_reqPrescriptionInformation.sortedByCol4}" actionListener="#{pc_reqPrescriptionInformation.sortColumn}" onmouseover="resetTargetFrame()" immediate="true"/>
				<h:commandLink id="prescriptionHdrCol5" value="#{property.prescriptionCol5}" styleClass="ovColSorted#{pc_reqPrescriptionInformation.sortedByCol5}" actionListener="#{pc_reqPrescriptionInformation.sortColumn}" onmouseover="resetTargetFrame()" immediate="true"/>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup id="prescriptionData" styleClass="formDivTableData6">
			<h:dataTable id="prescriptionDataTable" styleClass="formTableData" cellspacing="0" binding="#{pc_reqPrescriptionInformation.prescriptionTable}" value="#{pc_reqPrescriptionInformation.prescriptionList}" var="prescriptionLine" rowClasses="#{pc_reqPrescriptionInformation.rowStyles}" columnClasses="ovColIcon,ovColText300,ovColText70,ovColText60,ovColDate">
				<h:column>
					<h:commandButton id="presCol1Score0" onmousedown="saveTableScrollPosition();" image="images/needs_attention/filledcircle-onwhite.gif" rendered="#{prescriptionLine.score0}" styleClass="ovViewIconTrue" action="#{pc_reqPrescriptionInformation.selectRow}" onclick="resetTargetFrame()" immediate="true"/>
					<h:commandButton id="presCol1Score5" onmousedown="saveTableScrollPosition();" image="images/needs_attention/yield.gif" rendered="#{prescriptionLine.score5}" styleClass="ovViewIconTrue" action="#{pc_reqPrescriptionInformation.selectRow}" onclick="resetTargetFrame()" immediate="true"/>
					<h:commandButton id="presCol1Score10" onmousedown="saveTableScrollPosition();" image="images/needs_attention/flag-onwhite.gif" rendered="#{prescriptionLine.score10}" styleClass="ovViewIconTrue" action="#{pc_reqPrescriptionInformation.selectRow}" onclick="resetTargetFrame()" immediate="true"/>
					<h:commandButton id="presCol1ScoreDefault" onmousedown="saveTableScrollPosition();" image="images/needs_attention/clear.gif" rendered="#{prescriptionLine.scoreDefault}" styleClass="ovViewIconTrue" action="#{pc_reqPrescriptionInformation.selectRow}" onclick="resetTargetFrame()" immediate="true"/>
				</h:column>
				<h:column>
					<h:commandLink id="presCol2" onmousedown="saveTableScrollPosition();" value="#{prescriptionLine.prescriptionLabel}" styleClass="ovFullCellSelect" action="#{pc_reqPrescriptionInformation.selectRow}" onmouseover="resetTargetFrame()" immediate="true"/>
				</h:column>
				<h:column>
					<h:commandLink id="presCol3" onmousedown="saveTableScrollPosition();" styleClass="ovFullCellSelect" action="#{pc_reqPrescriptionInformation.selectRow}" onmouseover="resetTargetFrame()" immediate="true">
						<h:outputText value="#{prescriptionLine.prescriptionQuantityDispensed}">
							<f:convertNumber type="number" integerOnly="true"/>
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="presCol4" onmousedown="saveTableScrollPosition();" styleClass="ovFullCellSelect" action="#{pc_reqPrescriptionInformation.selectRow}" onmouseover="resetTargetFrame()" immediate="true">
						<h:outputText value="#{prescriptionLine.prescriptionTotalRefillsAllowed}">
							<f:convertNumber type="number" integerOnly="true"/>  
						</h:outputText>
					</h:commandLink>
				</h:column>
				<h:column>
					<h:commandLink id="presCol5" onmousedown="saveTableScrollPosition();" styleClass="ovFullCellSelect" action="#{pc_reqPrescriptionInformation.selectRow}" onmouseover="resetTargetFrame()" immediate="true">
						<h:outputText value="#{prescriptionLine.prescriptionFillDate}">
							<f:convertDateTime pattern="#{property.datePattern}"/>
						</h:outputText>
					</h:commandLink>
				</h:column>
			</h:dataTable>
		</h:panelGroup>
		<h:panelGroup style="width: 100%; padding-left: 10px; padding-top: 20px;" rendered="#{pc_reqPrescriptionInformation.renderRxEvalDescription}">
			<h:outputLabel value="#{property.prescriptionEvaluationStatus}" styleClass="formLabel" style="text-align: left;"/>
			<h:outputText value="#{pc_reqPrescriptionInformation.rxEvalDescription}" styleClass="formDisplayText"/>
		</h:panelGroup>		
		<h:panelGroup style="width: 100%; padding-left: 10px; padding-top: 20px; padding-bottom: 15px">
			<h:outputLabel value="#{property.prescriptionAdditionalDetails}" styleClass="formLabel" style="text-align: left;"/>
		</h:panelGroup>
		<h:panelGroup style="height: 31px; width: 100%">
			<h:outputLabel value="#{property.prescriptionPhysician}" styleClass="formLabel" style="width: 160px"/>
			<h:graphicImage url="images/needs_attention/flag-onwhite.gif" rendered="#{pc_reqPrescriptionInformation.highRiskPhysician}"/>			
			<h:outputText value="#{pc_reqPrescriptionInformation.physicianWithSpecialties}" styleClass="formDisplayText"/>			
			<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle" 
				action="#{pc_reqPrescriptionInformation.viewPhysician}"
						onclick="setTargetFrame()" immediate="true"
				rendered="#{pc_reqPrescriptionInformation.renderPhysicianIcon}"/>			
		</h:panelGroup>
		<h:panelGroup style="height: 31px; width: 100%">
			<h:outputLabel value="#{property.prescriptionpPharmacy}" styleClass="formLabel" style="width: 160px"/>
			<h:outputText value="#{pc_reqPrescriptionInformation.pharmacy}" styleClass="formDisplayText"/>
			<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle" 
				action="#{pc_reqPrescriptionInformation.viewPharmacy}"
						onclick="setTargetFrame()" immediate="true"			
				rendered="#{pc_reqPrescriptionInformation.renderPharmacyIcon}"/>
		</h:panelGroup>
		<h:panelGroup style="height: 31px; width: 100%">
			<h:outputLabel value="#{property.prescriptionHowTaken}" styleClass="formLabel" style="width: 160px"/>
			<h:outputText value="#{pc_reqPrescriptionInformation.howTaken}" style="width: 300px" styleClass="formDisplayText"/>
			<h:outputLabel value="#{property.prescriptionIndications}" styleClass="formLabel" style="width: 50px"/>
			<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: middle" 
				action="#{pc_reqPrescriptionInformation.viewIndications}"
						onclick="setTargetFrame()" immediate="true"			
				rendered="#{pc_reqPrescriptionInformation.renderIndicationsIcon}"/>
		</h:panelGroup>
		<h:panelGroup style="height: 31px; width: 100%">
			<h:outputLabel value="#{property.prescriptionTherapeuticCategory}" styleClass="formLabel" style="width: 160px"/>
			<h:outputText value="#{pc_reqPrescriptionInformation.therapeuticCategory}" styleClass="formDisplayText"/>
		</h:panelGroup>
		<h:panelGroup styleClass="formDataDisplayLine"/>		
	</div>
	<h:inputHidden id="prescriptionTableVScroll" binding="#{pc_reqPrescriptionInformation.scrollValue}" />  
</jsp:root>
