<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<!-- Update Preferred Section -->
	<h:panelGroup id="personPreferredArea" styleClass="inputFormMat">
		<h:panelGroup id="personPreferredSection" styleClass="inputForm" style="height:160px;" rendered="#{pc_NbaRating.preferredApplicable}"> <!-- rendered="#{pc_NbaRating.preferredApplicable}" -->
			<h:outputLabel id="clientPersonPreferredTitle" value="#{property.personPreferredLevelTitle}" styleClass="formTitleBar" />
			<h:panelGroup id="personPreferredSubSectionStyle">
				<h:panelGroup id="personPreferredSubSection1" styleClass="formDataDisplayTopLine">
					<h:outputLabel id="personPreferredSystem" value="#{property.personPreferredSystem}" styleClass="formLabel" />
					<h:outputText id="personPreferredSystemText" value="#{pc_NbaRating.preferredSystem}" styleClass="formDisplayText"/>
					<h:commandButton image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: bottom"
						onclick="setTargetFrame();" action="#{pc_NbaRating.launchPrefProfile}" />
				</h:panelGroup>		
				<h:panelGroup id="personPreferredSubSection2" styleClass="formDataDisplayLine">
					<h:outputLabel id="personPreferredApproved" value="#{property.personPreferredApproved}" styleClass="formLabel" />
					<h:outputText id="personPreferredApprovedText" value="#{pc_NbaRating.preferredApproved}" styleClass="formDisplayText" />					
				</h:panelGroup>		
				<h:panelGroup id="personPreferredSubSection3" styleClass="formDataDisplayLine">
					<h:outputLabel id="personPreferredOverride" value="#{property.personPreferredOverride}" styleClass="formLabel" />
					<h:selectOneRadio id="RB1" value="#{pc_NbaRating.preferredOverrideType}" layout="lineDirection" styleClass="formDisplayText" style="position:absolute;left: 125px; width: 250px; vertical-align: top;">
						<f:selectItem id="Item1" itemValue="Permanent" itemLabel="Permanent"/>
						<f:selectItem id="Item2" itemValue="Ceiling" itemLabel="Ceiling"/>
					</h:selectOneRadio>
					<h:selectOneMenu id="personPreferredOverrideValue" value="#{pc_NbaRating.preferredOverrideValue}" styleClass="formDisplayText" style="position:relative; left:240px; width: 200px; top: 4px">
						<f:selectItems value="#{pc_NbaRating.ufpTopLevelList}" />
					</h:selectOneMenu>
					<h:commandButton id="PrefLevelReasonViewIcon" image="images/link_icons/circle_i.gif" style="position: relative; left: 250px; vertical-align: bottom" 
					onclick="setTargetFrame();" action="#{pc_NbaRating.launchPreferredLevelReasonView}" /><!-- NBA223, SPRNBA-576 -->							
				</h:panelGroup>				
				<h:panelGroup id="personPreferredSubSection4" styleClass="formDataDisplayLine">
					<h:outputLabel id="personPreferredOverrideReason" value="#{property.personPreferredOverrideReason}" styleClass="formLabel" />
					<h:inputText id="personPreferredOverrideReasonText" value="#{pc_NbaRating.preferredUWOverrideReason}" styleClass="formDisplayText" style="width: 225px; "/>
				</h:panelGroup>			
			</h:panelGroup>			
		</h:panelGroup>
	</h:panelGroup>	
</jsp:root>