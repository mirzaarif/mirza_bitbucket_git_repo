<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version    Change Description -->
<!-- FNB013		    NB-1101	   DI Support for nbA -->
<!-- SPRNBA-798     NB-1401    Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="personOccupationClassArea" styleClass="inputFormMat">
		<h:panelGroup id="personOccupationClassTobaccoSection" styleClass="inputForm" style="height:130px;">
			<h:panelGroup id="personOccupationClassAreaTitle" styleClass="formTitleBar">
				<h:outputLabel id="clientPersonOccupationClassTitle" value="#{property.personOccupationClassTitle}"/>
			</h:panelGroup>
			<h:panelGroup id="personOccupationClassSubSectionStyle">
				<h:panelGroup id="personOccupationClassSubSection" styleClass="formDataDisplayLine">
					<h:outputLabel id="personOccupationClass" value="#{property.personOccupationClass}" styleClass="formLabel" />
					<h:selectOneMenu id="personOccupationClassText" value="#{pc_NbaRating.occupationClass}" onchange="submit()" 
						immediate="true" valueChangeListener="#{pc_NbaRating.rateClassChange}" styleClass="formDisplayText" >
						<f:selectItems value="#{pc_NbaRating.occupationClassList}" />
					</h:selectOneMenu>							
				</h:panelGroup>				
			</h:panelGroup>					
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>