<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR2890		   8      Reinsurance Tab - Uncommitted reinsurance type is not displayed in italics but always displayed in regular text -->
<!-- SPR2883		   8      Reinsurance Tab - Issues with edits in reinsurance overview tab -->
<!-- SPR2891		   8      Reinsurance Tab - Issues with information in the contract table pane on the Overview tab -->
<!-- SPRNBA-300	NB-1101 On the Reinsurance view there needs to be a way to distinguish between riders with the same insured, plan, reinsured amount and company -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="reinsuranceSectionHeader1" styleClass="sectionHeader" >
		<h:outputLabel id="PG1Label1" value="#{property.uwReinContractTitle}" styleClass="shTextLarge" style="position: relative; text-indent: 10px;"/>
		<h:outputLabel id="PG1Label2" value="#{property.uwReinReinsuranceType}" styleClass="shText" style="position: relative; left: 30px;"/>
		<h:outputLabel id="PG1Label3" value="#{pc_nbaReinsNav.titleReinsuranceType}" styleClass="shText#{pc_nbaReinsNav.titleReinsuranceTypeStyle}" style="position: relative; left: 32px;"/><!-- SPR2890 SPR2871 -->
		<h:outputLabel id="PG1Label4" value="#{property.uwReinReinsuranceAmount}" styleClass="shText" style="position: relative; left: 150px;"/>
		<h:outputLabel id="PG1Label5" value="#{pc_nbaReinsNav.titleReinsuranceAmount}" styleClass="shText" style="position: relative; left: 150px;"/>
	</h:panelGroup>
	<h:panelGroup id="reInsuranceTable1Header" styleClass="ovDivTableHeader" >
		<h:panelGrid columns="4" styleClass="ovTableHeader"
			columnClasses="ovColHdrText200,ovColHdrText150,ovColHdrText125,ovColHdrText125"
			cellspacing="0">
			<h:commandLink id="PG2Label1" value="#{property.reinsuranceOverviewCoveragecol1}"
					styleClass="ovColSorted#{pc_nbaReinsuranceOverviewTable.sortedByCol1}" actionListener="#{pc_nbaReinsuranceOverviewTable.sortColumn}" immediate="true" />
			<h:commandLink id="PG2Label2" value="#{property.reinsuranceOverviewCoveragecol2}"
					styleClass="ovColSorted#{pc_nbaReinsuranceOverviewTable.sortedByCol2}" actionListener="#{pc_nbaReinsuranceOverviewTable.sortColumn}" immediate="true" />
			<h:commandLink id="PG2Label3" value="#{property.reinsuranceOverviewCoveragecol3}"
					styleClass="ovColSorted#{pc_nbaReinsuranceOverviewTable.sortedByCol3}" actionListener="#{pc_nbaReinsuranceOverviewTable.sortColumn}" immediate="true" />
			<h:commandLink id="PG2Label4" value="#{property.reinsuranceOverviewCoveragecol4}"
					styleClass="ovColSorted#{pc_nbaReinsuranceOverviewTable.sortedByCol4}" actionListener="#{pc_nbaReinsuranceOverviewTable.sortColumn}" immediate="true" />
		</h:panelGrid>
	</h:panelGroup> 
	
	<h:panelGroup id="reInsuranceCoverageData" styleClass="ovDivTableData" style="height: 180px;">
		<h:dataTable id="reinsuranceOverviewCoverageTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_nbaReinsuranceOverviewTable.coverageTable}" value="#{pc_nbaReinsuranceOverviewTable.coverageList}" var="coverage"
					rowClasses="#{pc_nbaReinsNav.overviewCoverageRowStyle}" 
					columnClasses="ovColText200,ovColText150,ovColText125,ovColText125" >
			<h:column>
				<h:commandLink id="PG3Label1" action="#{pc_nbaReinsuranceOverviewTable.selectCoverageRow}" immediate="true" >		<!-- SPRNBA-399 -->
					<h:inputTextarea readonly="true" value="#{coverage.plan}" styleClass="ovMultiLine" style="width: 200px"/> 	<!-- SPRNBA-399 -->
				</h:commandLink>	<!-- SPRNBA-399 -->
			</h:column>
			<h:column>
				<h:commandLink id="PG3Label2" action="#{pc_nbaReinsuranceOverviewTable.selectCoverageRow}" immediate="true" title="#{coverage.toolTipParticipants}"> <!-- SPR2891 SPRNBA-399 -->
					<h:inputTextarea readonly="true" value="#{coverage.insured}" styleClass="ovMultiLine" style="width: 150px"/> 	<!-- SPRNBA-399 -->
				</h:commandLink>		<!-- SPRNBA-399 -->					
			</h:column>
			<h:column>
				<h:commandLink id="PG3Label3" value="#{coverage.amount}" action="#{pc_nbaReinsuranceOverviewTable.selectCoverageRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="PG3Label4" value="#{coverage.reinsuredAmount}" action="#{pc_nbaReinsuranceOverviewTable.selectCoverageRow}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup styleClass="ovButtonBar">
				<h:commandButton id="btnCreateRequest" action="#{pc_nbaReinCreateRequest.createRequest}" value="Create" disabled="#{pc_nbaReinsuranceOverviewTable.createDisabled}" styleClass="ovButtonRight"/><!-- SPR2883 -->
	</h:panelGroup>
</jsp:root>
