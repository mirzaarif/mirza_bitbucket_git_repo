<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA171            6      nbA Linux re-certification project -->
<!-- NBA213            7      Unified User Interface -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->
<!-- NBA245 		 NB-1301     Coverage/Party User Interface Rewrite -->
<!-- SPRNBA-798      NB-1401  Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %><!-- NBA171 -->
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Client Overview</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="javascript/global/file.js"></script>
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_clientBeneficiary'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_clientBeneficiary'].target='';
			return false;
		}
		function setDraftChanges() {
 			top.mainContentFrame.contentRightFrame.draftChanges = document.forms['form_clientBeneficiary']['form_clientBeneficiary:draftChanges'].value;  //NBA213
		}
		//SPR3396 New Method
		function hideUpdate() {
			parent.hideUpdate();
		}		
	</script>
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();setDraftChanges();hideUpdate();" style="margin-left: 0px; margin-right: 0px;"><!--SPR3396-->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property" />
		<PopulateBean:Load serviceName="RETRIEVE_UW_CLIENT_INFO" value="#{pc_NbaClientBeneficiaryTableData}" /> <!-- NBA245 -->
		<h:form id="form_clientBeneficiary">
			<f:subview id="clientBeneficiaryTable">
				<c:import url="/uw/subviews/NbaClientBeneficiaryTable.jsp" />
			</f:subview>	
			<h:inputHidden id="draftChanges" value="#{pc_uwbean.draftChanges}" />												
		</h:form>				
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	