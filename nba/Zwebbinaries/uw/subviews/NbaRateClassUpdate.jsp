<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA223        NB-1101      Underwriter Final Disposition -->
<!-- SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

 
<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 --> <!-- NBA213 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Update RateClass Section -->
	<h:panelGroup id="personRateClassArea" styleClass="inputFormMat">
		<h:panelGroup id="personRateClassTobaccoSection" styleClass="inputForm" style="height:160px;" rendered="#{pc_NbaRating.rateClassAndTobaccoPremiumApplicable}">
			<h:panelGroup id="personRateClassAreaTitle" styleClass="formTitleBar">
				<h:outputLabel id="clientPersonRateClassTitle" value="#{property.personRateClassTitle}"/>
				<h:outputLabel id="clientPersonTobaccoPrimiumTitle" value="#{property.personTobaccoPremiumBasisTitle}" style="position: relative; left: 250px"/>
			</h:panelGroup>
			<h:panelGroup id="personRateClassTobaccoSubSectionStyle">
				<h:panelGroup id="personRateClassSubSection1" styleClass="formDataDisplayTopLine">
					<h:outputLabel id="personRateClassAppliedFor" value="#{property.personRateClassAppliedFor}" styleClass="formLabel" />
					<h:outputText id="personRateClassAppliedForText" value="#{pc_NbaRating.rateClassAppliedFor}" styleClass="formDisplayText" style="width: 125px"/>
					<h:outputLabel id="personTobaccoSystem" value="#{property.personTobaccoSystem}" styleClass="formLabel" style="width: 200px"/>
					<h:outputText id="personTobaccoSystemText" value="#{pc_NbaRating.tobaccoSystem}" styleClass="formDisplayText" />				
				</h:panelGroup>		
				<h:panelGroup id="personRateClassTobaccoSubSection2" styleClass="formDataDisplayLine">
					<h:outputLabel id="personRateClassSystem" value="#{property.personRateClassSystem}" styleClass="formLabel" />
					<h:outputText id="personRateClassSystemText" value="#{pc_NbaRating.rateClassSystem}" styleClass="formDisplayText" style="width: 125px"/>
					<h:outputLabel id="personTobaccoOverride" value="#{property.personTobaccoOverride}" styleClass="formLabel" style="width: 200px"/>					
					<h:selectOneMenu id="personTobaccoOverrideValue" value="#{pc_NbaRating.tobaccoOverride}" styleClass="formDisplayText" style="width: 150px">
						<f:selectItems value="#{pc_NbaRating.tobaccoOverrideList}" />
					</h:selectOneMenu>									
				</h:panelGroup>				
				<h:panelGroup id="personRateClassTobaccoSubSection3" styleClass="formDataDisplayLine">
					<h:outputLabel id="personRateClassOverride" value="#{property.personRateClassOverride}" styleClass="formLabel" />
					<h:selectBooleanCheckbox id="personRateClassOverrideCheckBox" value="#{pc_NbaRating.rateClassOverrideInd}"  styleClass="formEntryCheckbox"/>
					<h:selectOneMenu id="personRateClassOverrideText" value="#{pc_NbaRating.rateClassOverride}" onchange="submit()" 
						immediate="true" valueChangeListener="#{pc_NbaRating.rateClassChange}" styleClass="formDisplayText" ><!-- ALS2643 -->
						<f:selectItems value="#{pc_NbaRating.rateClassOverrideList}" />
					</h:selectOneMenu>
					<h:commandButton id="RateClassReason_CButton" image="images/link_icons/circle_i.gif" style="position: relative; left: 5px; vertical-align: bottom" 
					onclick="setTargetFrame();" action="#{pc_NbaRating.launchRateClassReasonView}" />		<!-- SPRNBA-576 -->						
				</h:panelGroup>				
			</h:panelGroup>					
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>