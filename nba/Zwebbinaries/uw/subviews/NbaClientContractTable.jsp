<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<!-- Table Title Bar -->
	<h:panelGroup id="clientContractTitleHeader" styleClass="sectionSubheader">
		<h:outputLabel id="clientContractTableTitle" value="#{property.clientContractTitle}" styleClass="shTextLarge" />
	</h:panelGroup>
	<!-- Table Column Headers -->
	<h:panelGroup id="clientContractHeader" styleClass="ovDivTableHeader">
		<h:panelGrid id="clientContractPGUpdt" columns="6" styleClass="ovTableHeader" columnClasses="ovColHdrText155,ovColHdrText165,ovColHdrText105,ovColHdrText50,ovColHdrText50,ovColHdrText70" cellspacing="0"><!-- SPR3396-->
			<h:outputLabel id="clientContractHdrCol1" value="#{property.clientContractCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol2" value="#{property.clientContractCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol3" value="#{property.clientContractCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol4" value="#{property.clientContractCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol5" value="#{property.clientContractCol5}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientContractHdrCol6" value="#{property.clientContractCol6}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<!-- Table Columns -->
	<h:panelGroup id="clientContractPG1" styleClass="ovDivTableData" style="height: 115px;"><!-- SPR3396-->
		<h:dataTable id="clientContractTableDT" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_NbaClientTableData.htmlDataTable}"
			value="#{pc_NbaClientTableData.rows}" var="nbaClient" rowClasses="#{pc_NbaClientTableData.rowStyles}"
			columnClasses="ovColText155,ovColText165,ovColText105,ovColText50,ovColText50,ovColText70"><!-- SPR3396-->
			<h:column>
				<h:panelGroup id="clientContractPG2" styleClass="ovFullCellSelect" style="width: 200%;"><!-- SPR3396-->
					<h:commandButton id="clientIcon1" image="images/hierarchies/#{nbaClient.icon1}" rendered="#{nbaClient.icon1Rendered}"
						styleClass="#{nbaClient.icon1StyleClass}" style="margin-left: 5px; margin-top: -6px;" action="#{pc_NbaClientTableData.selectClientRow}"
						immediate="true" />
					<h:commandButton id="clientIcon2" image="images/hierarchies/#{nbaClient.icon2}" rendered="#{nbaClient.icon2Rendered}"
						styleClass="#{nbaClient.icon2StyleClass}" style="margin-left: 5px; margin-top: -6px;" action="#{pc_NbaClientTableData.selectClientRow}"
						immediate="true" />
					<h:commandLink id="clientContractCol1" value="#{nbaClient.col1}" style="vertical-align: top; color: #000000;"
						action="#{pc_NbaClientTableData.selectClientRow}" immediate="true" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:commandLink id="clientContractCol2" value="#{nbaClient.col2}" styleClass="ovFullCellSelect" action="#{pc_NbaClientTableData.selectClientRow}"
					immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientContractCol3" value="#{nbaClient.col3}" styleClass="ovFullCellSelect" action="#{pc_NbaClientTableData.selectClientRow}"
					immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientContractCol4" value="#{nbaClient.col4}" styleClass="ovFullCellSelect" action="#{pc_NbaClientTableData.selectClientRow}"
					immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientContractCol5" value="#{nbaClient.col5}" styleClass="ovFullCellSelect" action="#{pc_NbaClientTableData.selectClientRow}"
					immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientContractCol6" value="#{nbaClient.col6}" styleClass="ovFullCellSelect" action="#{pc_NbaClientTableData.selectClientRow}"
					immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>
