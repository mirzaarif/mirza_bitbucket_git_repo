<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA212            7      Content Services -->
<!-- FNB011            	NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<h:panelGroup styleClass="pageHeader">
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 200px"  rendered="#{pc_NbaClientNavigation.auth.visibility['Images']}"/>		<!-- FNB011 --> 
		<h:commandButton image="images/link_icons/documents-stack.gif"
			onclick="setTargetFrame()" action="#{pc_NbaClientNavigation.viewAllSourcesForUW}"
			style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"  rendered="#{pc_NbaClientNavigation.auth.visibility['Images']}"/> <!--  NBA212 FNB011-->
		<h:commandLink value="#{property.finalDisp}" styleClass="phText" style="position: absolute; right: 15px" 
			action="#{pc_NbaClientNavigation.finalDisp}"/>
	</h:panelGroup>
</jsp:root>
