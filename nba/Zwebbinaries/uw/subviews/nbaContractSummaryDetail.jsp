<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA134            6      nbA Assignment and Reassignment of an Underwriter Queue Project -->
<!-- NBA230            8      Reopen Date Enhancement Project -->
<!-- NBA251            8      nbA Case Manager and Companion Case Assignment -->
<!-- SPR3798		NB-1101	  Performance Plus Agent Volume Share Displays As Zero on Summary View - Should Use Renewal Interest for P+ Agents -->
<!-- NBA254		    NB-1101	  nbA Automatic Closure and Refund of CWA -->
<!-- NBA231          NB-1101      Replacement Processing -->
<!-- NBA244		    NB-1401	  Combine Contract Status and Contract Summary View -->
<!-- NBA297		    NB-1401	  Suitability Enhancement -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup styleClass="summaryHeader">
		<h:outputText value="#{pc_contractSummary.insuredName}"
			styleClass="summaryInsured" />
		<h:outputText value="#{pc_contractSummary.jointInsuredName}"
			rendered="#{pc_contractSummary.joint}" styleClass="summaryInsured" />
		<h:outputText value="#{pc_contractSummary.polNumber}"
			styleClass="summaryContractNumber" />
	</h:panelGroup>
	<h:panelGrid columns="2" styleClass="summaryGrid"
		columnClasses="summaryLeft, summaryRight" cellpadding="0"
		cellspacing="0">
		<h:column>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csGender}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.genderTranslation}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csIssAge}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.issueAge}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csDOB}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.birthDate}"
					styleClass="outputFixedLeftMargin">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputText value="#{property.csSSN}" styleClass="popupLabel"
					style="width: 155px" rendered="#{pc_contractSummary.SSN}" />
				<h:outputText value="#{property.csTIN}" styleClass="popupLabel"
					style="width: 155px" rendered="#{pc_contractSummary.TIN}" />
				<h:outputText value="#{property.csSIN}" styleClass="popupLabel"
					style="width: 155px" rendered="#{pc_contractSummary.SIN}" />
				<h:outputText value="#{pc_contractSummary.govtID}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.ownerPresent}">
				<h:outputLabel value="#{property.csOwner}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.ownerName}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csPlan}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.planName}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px"
				rendered="#{pc_contractSummary.currentAmtApplicable}">
				<h:outputLabel value="#{property.csAmount}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.currentAmt}">
					<f:convertNumber maxFractionDigits="2" type="currency"
						currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.planOptionApplicable}">
				<h:outputLabel value="#{property.csPlanOpt}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.planOptionTranslation}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.divOptionApplicable}">
				<h:outputLabel value="#{property.csDivOption}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.divOptionTranslation}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.qualifiedCodePresent}">
				<h:outputLabel value="#{property.csQualification}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.qualifiedCodeTranslation}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csForm}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.paymentMethodText}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csMode}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.paymentModeText}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csPremium}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.paymentAmt}">
					<f:convertNumber maxFractionDigits="2" type="currency"
						currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
				<h:outputText value="#{property.csModal}"
					style="position: relative; left: 5px" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="" styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.annualPaymentAmount}">
					<f:convertNumber maxFractionDigits="2" type="currency"
						currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
				<h:outputText value="#{property.csAnnual}"
					style="position: relative; left: 5px" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.nonStdBillApplicable}">
				<h:outputLabel value="#{property.csNonStdBill}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.nonStandBillAmt}">
					<f:convertNumber maxFractionDigits="2" type="currency"
						currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csCwa}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.CWAAmt}">
					<f:convertNumber maxFractionDigits="2" type="currency"
						currencySymbol="#{property.currencySymbol}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csAppDate}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.appDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csEffDate}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.effDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csAppType}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.applicationTypeText}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px"
				rendered="#{pc_contractSummary.purposeRendered}">
				<h:outputLabel value="#{property.csPurpose}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.purposeText}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csEntryDate}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.createDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.pendingClosureDate}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.pendingClosureDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
				<h:outputText value="#{property.overridden}"
					style="width: 80px;text-align: center"
					rendered="#{pc_contractSummary.closureOverrideInd}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px"
				rendered="#{pc_contractSummary.reopenDateExist}">
				<h:outputLabel value="#{property.csReopenDate}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.reopenDate}">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup style="height: 19px; width: 100%; padding-bottom: 10px">
				<h:outputLabel value="#{property.csSystem}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.backendSystemText}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csQueue}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.underwriterQueue}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:panelGrid columns="2"
					style="font-family: Verdana;font-size: 11px;font-weight: normal;font-style: normal;">
					<h:outputLabel value="#{property.caseManagerQueue}"
						styleClass="popupLabel" style="width: 155px;margin-left:-4px;" />
					<h:outputText value="#{pc_contractSummary.caseManagerQueue}"
						style="margin-left:-2px;" />
				</h:panelGrid>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.originRendered}">
				<h:outputLabel value="#{property.csOrigin}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.origin}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.reg60Rendered}">
				<h:panelGrid columns="2" style="font-family: Verdana;font-size: 11px;font-weight: normal;font-style: normal;">
				<h:outputLabel value="#{property.reg60Review}"
					styleClass="popupLabel" style="width: 155px;margin-left:-4px;" />
				<h:outputText value="#{pc_contractSummary.reg60ReviewText}" style="margin-left:-2px;" />
				</h:panelGrid>
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px"
				rendered="#{pc_contractSummary.reg60Rendered}">
				<h:outputLabel value="#{property.reg60PSDecision}"
					styleClass="popupLabel" style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.reg60PSDecisionText}" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100% padding-bottom: 10px">
				<h:outputLabel value="#{property.csFinDisp}" styleClass="popupLabel"
					style="width: 155px" />
				<h:outputText value="#{pc_contractSummary.dispTranslation}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<h:panelGroup style="height: 19px; width: 100%;padding-bottom: 10px">
				<h:outputLabel value="#{property.csAuUnd}" styleClass="popupLabel"
					style="width: 155px" rendered="#{!pc_contractSummary.annuity}" />
				<h:outputLabel value="#{property.csAuSuit}" styleClass="popupLabel"
					style="width: 155px" rendered="#{pc_contractSummary.annuity}" />
				<h:outputText value="#{pc_contractSummary.autoUnderwriting}"
					styleClass="outputFixedLeftMargin" />
			</h:panelGroup>
			<!-- Begin NBA297 -->
			<h:panelGroup style="height: 19px; width: 100% padding-bottom: 10px" rendered="#{pc_contractSummary.suitabilityApplicable}">
				<h:panelGrid columns="2"
						style="font-family: Verdana;font-size: 11px;font-weight: normal;font-style: normal;">
					<h:outputLabel value="#{property.csSuitDecision}" styleClass="popupLabel"
						style="width: 155px;margin-left:-4px"/>
					<h:outputText value="#{pc_contractSummary.suitabilityDecision}"
						styleClass="outputFixedLeftMargin" style="margin-left:-2px" />
				</h:panelGrid>
			</h:panelGroup>
			<!-- End NBA297 -->
			<h:panelGroup styleClass="agentDiv" style="height:135px;">
				<h:dataTable styleClass="agentTable" rowClasses="agentRow"
					value="#{pc_contractSummary.producers}" var="producer"
					cellpadding="0" cellspacing="0">
					<h:column>
						<h:panelGroup style="height: 19px; width: 100%">
							<h:outputLabel value="#{property.csAgentName}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{producer.name}"
								style="text-transform: capitalize" />
						</h:panelGroup>
						<h:panelGroup style="height: 19px; width: 100%">
							<h:outputLabel value="#{property.csAgentID}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{producer.producerID}" />
						</h:panelGroup>
						<h:panelGroup style="height: 19px; width: 100%">
							<h:outputLabel value="#{property.csCommission}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{producer.interestPercent}" />
						</h:panelGroup>
						<h:panelGroup style="height: 19px; width: 100%"
							rendered="#{!pc_contractSummary.agentSystemPERF}">
							<h:outputLabel value="#{property.csVolume}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{producer.volumeSharePct}" />
						</h:panelGroup>
						<h:panelGroup style="height: 19px; width: 100%"
							rendered="#{pc_contractSummary.agentSystemPERF}">
							<h:outputLabel value="#{property.csRenewal}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{producer.renewalInterestPct}" />
						</h:panelGroup>
						<h:panelGroup style="height: 19px; width: 100%">
							<h:outputLabel value="#{property.csAgency}"
								styleClass="popupLabel" style="width: 155px" />
							<h:outputText value="#{pc_contractSummary.agency}"
								styleClass="outputFixedLeftMargin" />
						</h:panelGroup>
					</h:column>
				</h:dataTable>
			</h:panelGroup>
			<h:panelGrid columns="2" styleClass="indicators" cellpadding="0"
				cellspacing="0">
				<h:column>
					<h:outputLabel value="#{property.csInd01}"
						styleClass="indEnabled#{pc_contractSummary.ind01}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd02}"
						styleClass="indEnabled#{pc_contractSummary.ind02}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd03}"
						styleClass="indEnabled#{pc_contractSummary.ind03}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd04}"
						styleClass="indEnabled#{pc_contractSummary.ind04}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd05}"
						styleClass="indEnabled#{pc_contractSummary.ind05}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd06}"
						styleClass="indEnabled#{pc_contractSummary.ind06}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd07}"
						styleClass="indEnabled#{pc_contractSummary.ind07}"
						style="width: 130px" />
				</h:column>
				<h:column>
					<h:outputLabel value="#{property.csInd08}"
						styleClass="indEnabled#{pc_contractSummary.ind08}"
						style="width: 130px" /><!--NBA297 -->
					<h:outputLabel value="#{property.csInd09}"
						styleClass="indEnabled#{pc_contractSummary.ind09}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd10}"
						styleClass="indEnabled#{pc_contractSummary.ind10}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd11}"
						styleClass="indEnabled#{pc_contractSummary.ind11}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd12}"
						styleClass="indEnabled#{pc_contractSummary.ind12}"
						style="width: 130px" />
					<h:outputLabel value="#{property.csInd14}"
						styleClass="indEnabled#{pc_contractSummary.ind14}"
						style="width: 130px" rendered="#{pc_contractSummary.suitabilityApplicable}" /><!--NBA297 -->
					<h:outputLabel value="#{property.csInd13}"
						styleClass="indEnabled#{pc_contractSummary.ind13}"
						style="width: 130px" />
				</h:column>
			</h:panelGrid>
		</h:column>
	</h:panelGrid>
	<f:verbatim>
		<hr class="formSeparator" />
	</f:verbatim>
	<div>
	<!-- Companion case table pane --> 
	<!-- Table Header -->
	 <h:panelGroup id="compCaseHeader" styleClass="popupDivTableHeader"
		style="position:relative;margin:0 0 0 -285px;left:50%">
		<h:panelGrid columns="4" styleClass="popupTableHeader" cellspacing="0"
			columnClasses="ovColHdrIcon,ovColHdrText120,ovColHdrText270,ovColHdrText155">
			<h:commandLink id="compCaseHdrCol1"
				actionListener="#{pc_contractSummary.sortColumn}"
				styleClass="ovColSorted#{pc_contractSummary.sortedByImage}" />
			<h:commandLink id="compCaseHdrCol2"
				value="#{property.csTableContractHeader}"
				actionListener="#{pc_contractSummary.sortColumn}"
				styleClass="ovColSorted#{pc_contractSummary.sortedByContract}" />
			<h:commandLink id="compCaseHdrCol3"
				value="#{property.csTableNameHeader}"
				actionListener="#{pc_contractSummary.sortColumn}"
				styleClass="ovColSorted#{pc_contractSummary.sortedByName}" />
			<h:commandLink id="compCaseHdrCol4"
				value="#{property.csTableOverrideHeader}"
				actionListener="#{pc_contractSummary.sortColumn}"
				styleClass="ovColSorted#{pc_contractSummary.sortedByOverrideInfo}" />
		</h:panelGrid>
	</h:panelGroup>
	 <!-- Table Data --> 
	 <h:panelGroup id="companionCaseData" styleClass="popupDivTableData6"
		style="position:relative;margin:16px 0 0 -567px;left:50%;height: 0px;width:566px;overflow: hidden;padding-bottom:25px;">
		<h:dataTable id="companionCaseTable" styleClass="popupTableData"
			cellspacing="0" binding="#{pc_contractSummary.compCaseTable}"
			value="#{pc_contractSummary.compCaseList}" var="compCase"
			rowClasses="#{pc_contractSummary.rowStyles}"
			columnClasses="ovColIcon,ovColText120,ovColText270,ovColText155">
			<h:column>
				<h:commandLink id="Col1" styleClass="ovFullCellSelect"
					style="width: 100%; height: 100%;"
					action="#{pc_contractSummary.selectRow}" immediate="true">
					<h:graphicImage url="images/needs_attention/reset.gif"
						rendered="#{compCase.CCOverriddenReset}"
						styleClass="ovViewIconTrue"
						style="border-width: 0px; margin-top: -2px" />
					<h:graphicImage url="images/needs_attention/yield.gif"
						rendered="#{compCase.CCOverridden}" styleClass="ovViewIconTrue"
						style="border-width: 0px; margin-top: -2px" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink value="#{compCase.contract}"
					styleClass="ovFullCellSelectPrf"
					action="#{pc_contractSummary.selectRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="#{compCase.name}" style="color: #000000;"
					action="#{pc_contractSummary.selectRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="#{compCase.overrideInfo}"
					styleClass="ovFullCellSelect"
					action="#{pc_contractSummary.selectRow}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	</div>
</jsp:root>