<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->
<!-- SPRNBA-603      NB-1301  Contact Info Section Of Final Disposition Client Detail View Repeats-->
<!-- SPRNBA-798      NB-1401  Change JSTL Specification Level -->
 

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" 
	xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld"> <!-- SPRNBA-798 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />	
	<h:panelGroup id="clientOrganizationDetailsHeader" styleClass="sectionHeader">
		<h:outputLabel id="organizationViewDetailsTitle1" value="#{property.organizationViewDetailsTitle}" 
			rendered="#{!pc_NbaClientDetails.updateMode}" styleClass="shTextLarge" />
		<h:outputLabel id="organizationViewDetailsTitle2" value="#{property.organizationUpdateDetailsTitle}" 
			rendered="#{pc_NbaClientDetails.updateMode}" styleClass="shTextLarge" />			
	</h:panelGroup>
	
	<h:panelGroup id="organizationDetailsSection" style="background-color: white">
		<h:panelGroup id="organizationDetailsSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="organizationName" value="#{property.organizationName}" styleClass="formLabel" />
			<h:outputText id="organizationNameText" value="#{pc_NbaClientDetails.name}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="organizationSocialSecurity" value="#{property.organizationSocialSecurity}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.socialSecurity}" />
			<h:outputLabel id="organizationTaxIdentification" value="#{property.organizationTaxIdentification}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.taxIdentification}"/>
			<h:outputLabel id="organizationSocialInsurance" value="#{property.organizationSocialInsurance}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.socialInsurance}"/>
			<h:outputText id="organizationSocialSecurityText"  value="#{pc_NbaClientDetails.govtId}" styleClass="formDisplayText" />
		</h:panelGroup>	
	</h:panelGroup>
	
	<f:subview id="clientOrganizationBusinessInfo"><!--SPR3396-->
		<c:import url="/uw/subviews/NbaClientOrganizationBusinessInfo.jsp" />
	</f:subview>			
	<!--SPR3396 Code Deleted-->
	<!-- SPRNBA-603 code deleted -->
</jsp:root>

