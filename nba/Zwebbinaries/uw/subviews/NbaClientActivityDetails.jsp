<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" 
	xmlns:h="http://java.sun.com/jsf/html" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />	
	<h:panelGroup id="activityDetailsSection1" style="background-color: white; border: medium;">
		<h:panelGroup id="activityViewButtonBar" styleClass="formButtonBar" style="top: 15px">
			<h:commandButton value="#{property.buttonDelete}" styleClass="formButtonLeft" onclick="setTargetFrame();" 
				action="#{pc_NbaClientActivityDetails.delete}" disabled="#{pc_NbaClientActivityDetails.disableDelete}" immediate="true" />
			<h:commandButton value="#{property.buttonUpdate}" styleClass="formButtonRight-1" onclick="resetTargetFrame();" 
				action="#{pc_NbaClientActivityDetails.viewInUpdateMode}" disabled="#{pc_NbaClientActivityDetails.disableUpdate}" immediate="true" />
			<h:commandButton value="#{property.buttonCreate}" styleClass="formButtonRight" onclick="resetTargetFrame();" 
				action="#{pc_NbaClientActivityDetails.viewInCreateMode}" immediate="true" />
		</h:panelGroup>		
		<f:verbatim>
			<hr id="hr_ActivityDetail_2"/>
		</f:verbatim>			
		
		<h:panelGroup id="activityDetailsSubSection1" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection1}" >
			<h:outputLabel id="activityType" value="#{property.activityType}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="activityTypeOutText" value="#{pc_NbaClientActivityDetails.activityType}" styleClass="formDisplayText" style="width: 200px;"/>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection2" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection2a}" >
			<h:outputLabel id="activityMovingViolation" value="#{property.activityMovingViolation}" styleClass="formLabel" style="text-align: left; width: 260"/>			
			<h:outputLabel id="activityAdditionalRisk" value="#{property.activityAdditionalRisk}" styleClass="formLabel" style="text-align: left;"/>			
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection3" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection3}">
			<h:outputLabel id="activityAviationType" value="#{property.activityAviationType}" styleClass="formLabel"  style="width: 175px" />
			<h:outputText id="activityAviationTypeOutText" value="#{pc_NbaClientActivityDetails.activityAviationType}" styleClass="formDisplayText" style="width: 200px;"  />		
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection4" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection4}">
			<h:outputLabel id="activityPurpose" value="#{property.activityPurpose}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="activityPurposeOutText" value="#{pc_NbaClientActivityDetails.activityPurpose}" styleClass="formDisplayText" style="width: 200px;"  />
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection5" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection5}">
			<h:outputLabel id="activityAircraftType" value="#{property.activityAircraftType}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="activityAircraftTypeOutText" value="#{pc_NbaClientActivityDetails.activityAircraftType}" styleClass="formDisplayText" style="width: 260"/>
			<h:outputLabel id="activityQuestionnaire" value="#{property.activityQuestionnaire}" styleClass="formLabel" style="font-weight: bold; width: 100px"/>			
			<h:commandButton image="images/link_icons/circle_i.gif" style="vertical-align: middle;" onclick="setTargetFrame();" action="#{pc_NbaClientActivityDetails.openQuestionnaire}" immediate="true"/>			
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection6" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection6}">
			<h:outputLabel id="activityHighestQualificationLevel" value="#{property.activityHighestQualificationLevel}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="activityHighestQualificationLevelOutText" value="#{pc_NbaClientActivityDetails.activityHighestQualificationLevel}" styleClass="formDisplayText" style="width: 300px;"  />
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection7" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection7}">
			<h:outputLabel id="activityMonthsOfExperience" value="#{property.activityMonthsOfExperience}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup1" style="width: 260">
				<h:outputText id="activityMonthsOfExperienceOutText" value="#{pc_NbaClientActivityDetails.activityMonthsOfExperience}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="months_1" value="#{property.activityMonths}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>
			</h:panelGroup>	
			<h:outputLabel id="activityHazardousAreas" value="#{property.activityHazardousAreas}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleHazardousAreas}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection8" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection8}">
			<h:outputLabel id="activityRecordAttempts" value="#{property.activityRecordAttempts}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup2" style="width: 260">
				<h:outputText id="activityRecordAttemptsOutText" value="#{pc_NbaClientActivityDetails.activityRecordAttempts}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="total_1" value="#{property.activityTotal}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>
			</h:panelGroup>	
			<h:outputLabel id="activityCompetitionActivity" value="#{property.activityCompetitionActivity}" styleClass="formLabel" style="width:150px; text-align: left; #{pc_NbaClientActivityDetails.styleCompetitionActivity}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection9" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection9}">
			<h:outputLabel id="activityTotal" value="#{property.activityTotalHours}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup3" style="width: 260">
				<h:outputText id="activityTotalOutText" value="#{pc_NbaClientActivityDetails.activityTotal}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="hours_1" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>	
			<h:outputLabel id="activityIFR" value="#{property.activityIFR}" styleClass="formLabel" style="text-align: left; #{pc_NbaClientActivityDetails.styleIFR}" rendered="#{pc_NbaClientActivityDetails.renderIFR}"/>
			<h:outputLabel id="activityTethred" value="#{property.activityTethred}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleTethred}" rendered="#{!pc_NbaClientActivityDetails.renderIFR}"/>						
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection10" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection10}">
			<h:outputLabel id="activityLast12Months" value="#{property.activityLast12Months}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup4" style="width: 260">
				<h:outputText id="activityLast12MonthsOutText" value="#{pc_NbaClientActivityDetails.activityLast12Months}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="hours_2" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>
			<h:outputLabel id="activityCoPilotPresent" value="#{property.activityCoPilotPresent}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleCoPilotPresent}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection11" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection11}">
			<h:outputLabel id="activityPrevious1To2Years" value="#{property.activityPrevious1To2Years}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup5" style="width: 260">
				<h:outputText id="activityPrevious1To2YearsOutText" value="#{pc_NbaClientActivityDetails.activityPrevious1To2Years}" styleClass="formDisplayText">
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="hours_3" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>
			<h:outputLabel id="activityFutureAviation" value="#{property.activityFutureAviation}" styleClass="formLabel" style="text-align: left;  #{pc_NbaClientActivityDetails.styleFutureAviation}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection12" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection12}">
			<h:outputLabel id="activityPlannedNext12Months" value="#{property.activityPlannedNext12Months}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup6" style="width: 260">
				<h:outputText id="activityPlannedNext12MonthsOutText" value="#{pc_NbaClientActivityDetails.activityPlannedNext12Months}" styleClass="formDisplayText"  >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="hours_4" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection13" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection13}">
			<h:outputLabel id="activityAverageHoursPerYear" value="#{property.activityAverageHoursPerYear}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup7" style="width: 260">
				<h:outputText id="activityAverageHoursPerYearOutText" value="#{pc_NbaClientActivityDetails.activityAverageHoursPerYear}" styleClass="formDisplayText" >
					<f:convertNumber type="number" />
				</h:outputText>
				<h:outputLabel id="hours_5" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection14" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection14}">
			<h:outputLabel id="activityIFRHours" value="#{property.activityIFRHours}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup8" style="width: 260">
				<h:outputText id="activityIFRHoursOutText" value="#{pc_NbaClientActivityDetails.activityIFRHours}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="hours_6" value="#{property.activityHours}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>			
			</h:panelGroup>
		</h:panelGroup>			
		<h:panelGroup id="activityDetailsSubSection15" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection15}">
			<h:outputLabel id="activityFAAViolationType" value="#{property.activityFAAViolationType}" styleClass="formLabel" style="width: 175px"/>
			<h:outputText id="activityFAAViolationTypeOutText" value="#{pc_NbaClientActivityDetails.activityFAAViolationType}" styleClass="formDisplayText"  style="width: 100px;"  />
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection16" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection16}">
			<h:outputLabel id="activityTotalViolations" value="#{property.activityTotalViolations}" styleClass="formLabel" style="width: 175px"/>
			<h:panelGroup id="activitygroup9" style="width: 260">
				<h:outputText id="activityTotalViolationsOutText" value="#{pc_NbaClientActivityDetails.activityTotalViolations}" styleClass="formDisplayText" >
					<f:convertNumber integerOnly="true" type="integer" />
				</h:outputText>
				<h:outputLabel id="total_2" value="#{property.activityTotal}" styleClass="formDisplayText" style="padding-left: 4px; text-align: left;"/>
			</h:panelGroup>
		</h:panelGroup>		
		<h:panelGroup id="activityDetailsSubSection17" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection17}">
			<h:outputLabel id="activityJobChangeCount" value="#{property.activityJobChangeCount}" styleClass="formLabel" style="width: 150px"/>
			<h:outputText id="activityJobChangeCountOutText" value="#{pc_NbaClientActivityDetails.activityJobChangeCount}" styleClass="formDisplayText"  style="width: 260px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:outputText>
			<h:outputLabel id="activityHazardousActivities" value="#{property.activityHazardousActivities}" styleClass="formLabel" style="width:150px; text-align: left; #{pc_NbaClientActivityDetails.styleHazardousActivities}"/>			
		</h:panelGroup>	
		<h:panelGroup id="activityDetailsSubSection18" styleClass="formDataDisplayTopLine" rendered="#{pc_NbaClientActivityDetails.renderSection18}">
			<h:outputLabel id="activityViolationType" value="#{property.activityViolationType}" styleClass="formLabel"/>
			<h:outputText id="activityViolationTypeOutText" value="#{pc_NbaClientActivityDetails.activityViolationType}" styleClass="formDisplayText" />
		</h:panelGroup>
		<h:panelGroup id="activityDetailsSubSection19" styleClass="formDataDisplayLine" rendered="#{pc_NbaClientActivityDetails.renderSection19}">
			<h:outputLabel id="activityViolationDate" value="#{property.activityViolationDate}" styleClass="formLabel"/>
			<h:outputText id="activityAviationDateOutText" value="#{pc_NbaClientActivityDetails.activityViolationDate}" styleClass="formDisplayText"  >
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:outputText>
		</h:panelGroup>
		
	</h:panelGroup>		
</jsp:root>

