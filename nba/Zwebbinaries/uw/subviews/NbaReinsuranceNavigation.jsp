<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- NBA212            7      Content Services -->
<!-- FNB011            	NB-1101	Work Tracking -->
<!-- NBA324 	 	NB-1301   nbAFull Personal History Interview -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"> <!-- SPRNBA-798 -->
	<!-- ********************************************************
			Subview Navigation header - contains back link and Images link
		 ******************************************************** -->
	<h:panelGroup styleClass="pageHeader">
		<h:commandButton id="phiBriefCaseRein" image="images/phiBriefCase.gif" title="#{property.reviewPHI}" onclick="launchPHIBriefcaseView();" rendered="#{pc_reqimpNav.showPhiBriefcase}" style="position: absolute; margin-top: 2px; right: 175px; vertical-align: middle"/> <!-- NBA324 -->					
		<h:outputText value="#{property.viewAllDocs}" styleClass="phText" style="position: absolute; right: 40px" rendered="#{pc_nbaReinsNav.auth.visibility['Images']}"/> <!-- FNB011 --> 
		<h:commandButton image="images/link_icons/documents-stack.gif"
			onclick="setTargetFrame();" action="#{pc_nbaReinsNav.viewAllSourcesForUW}"
			style="position: absolute; margin-top: 2px; right: 15px; vertical-align: middle"  rendered="#{pc_nbaReinsNav.auth.visibility['Images']}"/> <!-- NBA212 FNB011 -->
	</h:panelGroup>
</jsp:root>
