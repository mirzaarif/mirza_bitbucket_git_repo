<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3396           8      Client Row Not Shown in Draft Mode After Update and Prior to Commit -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->


<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:c="http://java.sun.com/jsp/jstl/core"> <!-- SPRNBA-798 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="clientPersonDetailsHeader" styleClass="sectionHeader">
		<h:outputLabel id="clientPersonDetailsTitle1" value="#{property.personViewDetailsTitle}" 
			rendered="#{!pc_NbaClientDetails.updateMode}" styleClass="shTextLarge" />
		<h:outputLabel id="clientPersonDetailsTitle2" value="#{property.personUpdateDetailsTitle}" 
			rendered="#{pc_NbaClientDetails.updateMode}" styleClass="shTextLarge" />
	</h:panelGroup>
	<h:panelGroup id="personDetailsSection" >
		<h:panelGroup id="personDetailsSubSection1" styleClass="formDataDisplayTopLine" style="">
			<h:outputLabel id="personName" value="#{property.personName}" styleClass="formLabel" />
			<h:outputText id="personNameText" value="#{pc_NbaClientDetails.name}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="personSocialSecurity" value="#{property.personSocialSecurity}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.socialSecurity}" />
			<h:outputLabel id="personTaxIdentification" value="#{property.personTaxIdentification}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.taxIdentification}"/>
			<h:outputLabel id="personSocialInsurance" value="#{property.personSocialInsurance}" styleClass="formLabel"  rendered="#{pc_NbaClientDetails.socialInsurance}"/>
			<h:outputText id="personSocialSecurityText"  value="#{pc_NbaClientDetails.govtId}" styleClass="formDisplayText" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection2" styleClass="formDataDisplayLine">
			<!--begin SPR3210  -->
			<h:outputLabel id="personBirth" value="#{property.personBirth}" styleClass="formLabel"  style="vertical-align: top"/>
			<h:outputText id="personBirthtext" value="#{pc_NbaClientDetails.birth}" styleClass="formDisplayText" style="width: 200px;vertical-align: top" />
			<h:outputLabel id="personLicense" value="#{property.personLicense}" styleClass="formLabel"  style="vertical-align: top"/>
			<h:outputText id="personLicenseText" value="#{pc_NbaClientDetails.license}" styleClass="formDisplayText" style="width: 125px;vertical-align: top"/>
			<!--end SPR3210  -->
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection3" styleClass="formDataDisplayLine">
			<h:outputLabel id="personHeightWeight" value="#{property.personHeightWeight}" styleClass="formLabel" />
			<h:outputText id="personHeightWeightText" value="#{pc_NbaClientDetails.hightWeight}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="personCitizenship" value="#{property.personCitizenship}" styleClass="formLabel" />
			<h:outputText id="personCitizenshipText" value="#{pc_NbaClientDetails.citizenship}" styleClass="formDisplayText" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection4" styleClass="formDataDisplayLine">
			<h:outputLabel id="personResidenceState" value="#{property.personResidenceState}" styleClass="formLabel" />
			<h:outputText id="personResidenceStateText" value="#{pc_NbaClientDetails.residenceState}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="personProfileScore" value="#{property.personProfileScore}" styleClass="formLabel" />
			<h:outputText id="personProfileScoreText" value="#{pc_NbaClientDetails.profileScore}" styleClass="formDisplayText" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection5" styleClass="formDataDisplayLine">
			<h:outputLabel id="personMaritalStatus" value="#{property.personMaritalStatus}" styleClass="formLabel" />
			<h:outputText id="personMaritalStatusText" value="#{pc_NbaClientDetails.maritalStatus}" styleClass="formDisplayText" style="width: 200px" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection6" styleClass="formDataDisplayLine">
			<h:outputLabel id="personOccupation" value="#{property.personOccupation}" styleClass="formLabel" />
			<h:outputText id="personOccupationText" value="#{pc_NbaClientDetails.occupation}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="personActivitiesPresent" value="#{property.personActivitiesPresent}" styleClass="formLabel" />
			<h:outputText id="personActivitiesPresentText" value="#{pc_NbaClientDetails.activitiesPresent}" styleClass="formDisplayText" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection7" styleClass="formDataDisplayLine">
			<h:outputLabel id="personAnnualIncome" value="#{property.personAnnualIncome}" styleClass="formLabel" />
			<h:outputText id="personAnnualIncomeText" value="#{pc_NbaClientDetails.annualIncome}" styleClass="formDisplayText" style="width: 200px" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" /> 
			</h:outputText>
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection8" styleClass="formDataDisplayLine">
			<h:outputLabel id="personWithholding" value="#{property.personWithholding}" styleClass="formLabel" />
			<h:outputText id="personWithholdingText" value="#{pc_NbaClientDetails.withholding}" styleClass="formDisplayText" style="width: 200px" />
		</h:panelGroup>	
		<h:panelGroup id="personDetailsSubSection9" styleClass="formDataDisplayLine">
			<h:outputLabel id="personNetWorth" value="#{property.personNetWorth}" styleClass="formLabel" />
			<h:outputText id="personNetWorthText" value="#{pc_NbaClientDetails.netWorth}" styleClass="formDisplayText" style="width: 200px" > 
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>	
		</h:panelGroup>			
	</h:panelGroup>

	<f:subview id="clientPersonBusinessUpdate">
		<c:import url="/uw/subviews/NbaClientPersonBusinessUpdate.jsp" />
	</f:subview>	
		<f:subview id="clientPersonOtherUpdate">
		<c:import url="/uw/subviews/NbaClientPersonOtherUpdate.jsp" />
	</f:subview>	
</jsp:root>

