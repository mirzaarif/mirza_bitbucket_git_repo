<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="reInsuranceTable1Header"
		styleClass="ovDivTableHeader">
		<h:panelGrid columns="5" styleClass="ovTableHeader"
			columnClasses="ovColHdrText105,ovColHdrText105,ovColHdrText105,ovColHdrText105,ovColHdrText105"
			cellspacing="0">
			<h:outputText value="Coverage" styleClass="shText" />
			<h:outputText value="Insured" styleClass="shText" />
			<h:outputText value="Amount" styleClass="shText" />
			<h:outputText value="Retained Amount" styleClass="shText" />
			<h:outputText value="Reinsured Amount" styleClass="shText" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="reInsuranceCoverageData" styleClass="ovDivTableData"
		style="height: 130px;">
		<h:dataTable id="reInsuranceCoverageTable" styleClass="ovTableData"
			cellspacing="0" value="" var="" rowClasses=""
			columnClasses="ovColText105,ovColText105,ovColText105,ovColText105,ovColText105">
			<h:column>
				<h:commandLink value="Base Plan"
					style="overflow: auto; height: auto;" action="" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="James P Wood" action="" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="$000.000.000" action="" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="$0" action="" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink value="$000.000.000" action="" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	</jsp:root>
