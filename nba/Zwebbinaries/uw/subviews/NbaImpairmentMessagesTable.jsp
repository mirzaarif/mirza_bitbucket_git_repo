<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup styleClass="formDivTableHeaderRightButtons">
		<h:panelGrid columns="3" styleClass="formTableHeaderRightButtons" columnClasses="ovColHdrDate,ovColHdrText295,ovColHdrText125" cellspacing="0">
			<h:commandLink id="impMessageCol1" value="#{property.impMessageCol1}" styleClass="ovColSortedFalse" />
			<h:commandLink id="impMessageCol2" value="#{property.impMessageCol2}" styleClass="ovColSortedFalse" />
			<h:commandLink id="impMessageCol3" value="#{property.impMessageCol3}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<h:column>
			<h:panelGroup styleClass="formDivTableDataRightButtons3">
				<h:dataTable id="messagesTable" binding="#{pc_impCreateUpdate.messagesTable}" value="#{pc_impCreateUpdate.messagesList}" var="msg" rows="0"
					styleClass="formTableDataRightButtons " columnClasses="ovColDate,ovColText295,ovColText125" rowClasses="#{pc_impCreateUpdate.msgsRowStyles}"
					cellspacing="0">
					<h:column>
						<h:commandLink value="#{msg.col1}" styleClass="ovFullCellSelect" action="#{pc_impCreateUpdate.actionMessageRowSelected}" />
					</h:column>
					<h:column>
						<h:commandLink value="#{msg.col2}" styleClass="ovFullCellSelect" action="#{pc_impCreateUpdate.actionMessageRowSelected}" />
					</h:column>
					<h:column>
						<h:commandLink value="#{msg.col3}" styleClass="ovFullCellSelect" action="#{pc_impCreateUpdate.actionMessageRowSelected}" />
					</h:column>
				</h:dataTable>
			</h:panelGroup>
		</h:column>
		<h:column>
			<h:panelGroup>
				<h:commandLink value="#{property.buttonAddAnother}" styleClass="formButtonInterface" onmousedown="setTargetFrame();" 
					action="#{pc_impCreateUpdate.actionAddMessage}" />
				<h:commandLink value="#{property.buttonDelete}" styleClass="formButtonInterface" rendered="#{pc_impCreateUpdate.renderMessageDeleteLink}"
					action="#{pc_impCreateUpdate.actionMessageRowDelete}" immediate="true" style="margin-top: 7px; padding-top: 6px" />
			</h:panelGroup>
		</h:column>
	</h:panelGrid>
</jsp:root>
