<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- SPR3399           7      Duplicate Component ID exception is being thrown by the Sun JSF implementation -->
<!-- NBA308         NB-1301	  MIB Follow Ups -->
<!-- NBA366      	NB-1501   nbA Requirement Order Statuses from Third Party Providers -->
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="requirementsHeader_ro" styleClass="ovDivTableHeader">
		<h:panelGrid columns="8" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrIcon,ovColHdrText165,ovColHdrText75,ovColHdrDate,ovColHdrDate,ovColHdrDate,ovColHdrTextStatus#{pc_reqimpNav.sourceAndStatusDisplayable},ovColHdrIconStatus#{pc_reqimpNav.sourceAndStatusDisplayable}"><!-- NBA366 -->
			<h:commandLink id="reqHdrCol1_ro" value="#{property.requirementCol1}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol1}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol2_ro" value="#{property.requirementCol2}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol2}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol3_ro" value="#{property.requirementCol3}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol3}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol4_ro" value="#{property.requirementCol4}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol4}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol5_ro" value="#{property.requirementCol5}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol5}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol6_ro" value="#{property.requirementCol6}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol6}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol7_ro" value="#{property.requirementCol7}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol7}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
			<h:commandLink id="reqHdrCol8_ro" value="#{property.requirementCol8}"
					styleClass="ovColSorted#{pc_reqTable.sortedByCol8}" actionListener="#{pc_reqTable.sortColumn}" immediate="true" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="requirementData_ro" styleClass="ovDivTableData#{pc_reqTable.rowCount}"> 
			<h:dataTable id="requirementsTable_ro" styleClass="ovTableData" cellspacing="0" rows="0"
					binding="#{pc_reqTable.requirementsTable}" value="#{pc_reqTable.requirementList}" var="req"
					rowClasses="#{pc_reqTable.rowStyles}" 
					columnClasses="ovColIcon,ovColText165,ovColText75,ovColDate,ovColDate,ovColDate,ovColTextStatus#{pc_reqimpNav.sourceAndStatusDisplayable},ovColIconStatus#{pc_reqimpNav.sourceAndStatusDisplayable}" ><!-- NBA366 -->
				<h:column>
					<h:graphicImage id="reqCol1a_ro" styleClass="menu" url="images/needs_attention/flag-onwhite.gif" title="#{req.messageText}"
								rendered="#{req.attentionNeeded}"/>
					<h:graphicImage id="reqCol1aa_ro" styleClass="menu" url="images/needs_attention/manual.gif" title="#{req.messageText}"
								rendered="#{req.manualAttention}" />
					<h:graphicImage id="reqCol1b_ro" styleClass="menu" url="images/needs_attention/filledcircle-onwhite.gif" title="#{req.messageText}"
								rendered="#{req.reviewCommitted &amp;&amp; !req.planFReviewNeeded}"/>	<!-- NBA308 -->
					<h:graphicImage id="reqCol1d_ro" styleClass="menu" url="images/needs_attention/planF.gif" title="#{req.messageText}"												    				  
								rendered="#{req.planFReviewNeeded}" />  <!-- NBA308 -->									
					<h:graphicImage id="reqCol1c_ro" styleClass="menu" url="images/needs_attention/clear.gif" title="#{req.messageText}"
								rendered="#{req.manualAttention}" />
				</h:column>
				<h:column>
					<h:outputText id="reqCol2_ro" value="#{req.typeName}"  title="#{req.messageText}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column>
					<h:outputText id="reqCol3_ro" value="#{req.statusText}" title="#{req.messageText}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column>
					<h:outputText id="reqCol4_ro" value="#{req.created}" title="#{req.messageText}" styleClass="ovFullCellSelect" /> 
				</h:column>
				<h:column>
					<h:outputText id="reqCol5_ro" value="#{req.received}" title="#{req.messageText}" styleClass="ovFullCellSelect"/>
				</h:column>
				<h:column>
					<h:selectBooleanCheckbox id="reqCol6a_ro" value="#{req.reviewed}" title="#{req.messageText}"
								styleClass="ovFullCellSelectCheckBox" rendered="#{req.reviewable}" disabled="true"
								valueChangeListener="#{req.overviewReviewedValueChanged}" onclick="resetTargetFrame();submit();" /> 					
                     <h:outputText id="reqCol6b_ro" value="#{req.reviewedDate}" title="#{req.messageText}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column>
					<h:outputText id="reqCol7_ro" value="#{req.reviewerID}" title="#{req.messageText}" styleClass="ovFullCellSelect" />
				</h:column>
				<h:column>
					<h:graphicImage id="reqStatus" url="images/link_icons/reqStatus.gif" styleClass="ovViewIconTrue" rendered="#{req.statusReceived}" style="margin-right: 2px;"/> <!-- NBA366 -->
					<h:commandButton id="reqCol8_ro" image="images/link_icons/documents.gif" styleClass="ovViewIcon#{req.imageViewable}" style="margin-left: 2px;"
								onclick="setTargetFrame()" action="#{req.showSelectedImage}" immediate="true" 
								disabled="#{req.documentDisabled}"/>  <!-- NBA366 -->
				</h:column>
		</h:dataTable>
	</h:panelGroup>
</jsp:root>