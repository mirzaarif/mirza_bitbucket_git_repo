<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA122            5      Underwriter Workbench Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" 
	xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:PopulateBean="/WEB-INF/tld/PopulateBean.tld"> <!-- SPRNBA-798 -->
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup id="organizationBusinessBuySellSection" style="background-color: white" rendered="#{pc_NbaClientDetails.businessBuySellApplicable}">
		<h:panelGroup id="clientorganizationBusinessBuySellHeader" styleClass="sectionSubheader">
			<h:outputLabel id="clientorganizationBusinessBuySellTitle" value="#{property.organizationBusinessBuySellTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="organizationBusinessBuySellSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="organizationTypeOfBusiness" value="#{property.organizationTypeOfBusiness}" styleClass="formLabel" />
			<h:outputText id="organizationTypeOfBusinessText" value="#{pc_NbaClientDetails.typeOfBusiness}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="organizationNetWorth" value="#{property.organizationNetWorth}" styleClass="formLabel" />
			<h:outputText id="organizationNetWorthText" value="#{pc_NbaClientDetails.netWorth}" styleClass="formDisplayText" >			
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessBuySellSubSection2" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationTotalAssets" value="#{property.organizationTotalAssets}" styleClass="formLabel" />
			<h:outputText id="organizationTotalAssetsText" value="#{pc_NbaClientDetails.totalAsset}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>						
			<h:outputLabel id="organizationTotalPartnerCoverage" value="#{property.organizationTotalPartnerCoverage}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="organizationTotalPartnerCoverageText" value="#{pc_NbaClientDetails.totalPartnerCoverage}" styleClass="formDisplayText" >			
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessBuySellSubSection3" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationTotalLiabilities" value="#{property.organizationTotalLiabilities}" styleClass="formLabel" />
			<h:outputText id="organizationTotalLiabilitiesText" value="#{pc_NbaClientDetails.totalLiabilities}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>							
			<h:outputLabel id="organizationAllPartnersInsuredBuySell" value="#{property.organizationAllPartnersInsuredBuySell}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="organizationAllPartnersInsuredBuySellText" value="#{pc_NbaClientDetails.allPartnersInsured}" styleClass="formDisplayText" />			
		</h:panelGroup>			
		<h:panelGroup id="organizationBusinessBuySellSubSection4" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationEvaluationBasis" value="#{property.organizationEvaluationBasis}" styleClass="formLabel" />
			<h:outputText id="organizationEvaluationBasisText" value="#{pc_NbaClientDetails.evaluationBasis}" styleClass="formDisplayText" />			
		</h:panelGroup>				
	</h:panelGroup>	
	
	<h:panelGroup id="organizationBusinessKeyPersonSection" style="background-color: white" rendered="#{pc_NbaClientDetails.businessKeyPersonApplicable}">
		<h:panelGroup id="clientorganizationBusinessKeyPersonHeader" styleClass="sectionSubheader">
			<h:outputLabel id="clientorganizationBusinessKeyorganizationTitle" value="#{property.organizationBusinessKeyPersonTitle}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="organizationBusinessKeyPersonSubSection1" styleClass="formDataDisplayTopLine">
			<h:outputLabel id="organizationTypeOfBusiness2" value="#{property.organizationTypeOfBusiness}" styleClass="formLabel" />
			<h:outputText id="organizationTypeOfBusinessText2" value="#{pc_NbaClientDetails.typeOfBusiness}" styleClass="formDisplayText" style="width: 200px" />
			<h:outputLabel id="organizationNetWorth2" value="#{property.organizationNetWorth}" styleClass="formLabel" />
			<h:outputText id="organizationNetWorthText2" value="#{pc_NbaClientDetails.netWorth}" styleClass="formDisplayText" >			
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessKeyPersonSubSection3" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationTotalAssets2" value="#{property.organizationTotalAssets}" styleClass="formLabel" />
			<h:outputText id="organizationTotalAssetsText2" value="#{pc_NbaClientDetails.totalAsset}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
			<h:outputLabel id="organizationNumberOfEmployees" value="#{property.organizationNumberOfEmployees}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="organizationNumberOfEmployeesText" value="#{pc_NbaClientDetails.numberOfEmployee}" styleClass="formDisplayText">			
				<f:convertNumber integerOnly="true" type="integer" />
			</h:outputText>									
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessKeyPersonSubSection4" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationTotalLiabilities2" value="#{property.organizationTotalLiabilities}" styleClass="formLabel" />
			<h:outputText id="organizationTotalLiabilitiesText2" value="#{pc_NbaClientDetails.totalLiabilities}" styleClass="formDisplayText" style="width: 100px">
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
			<h:outputLabel id="organizationNumberOfOtherKeyPersons" value="#{property.organizationNumberOfOtherKeyPersons}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="organizationNumberOfOtherKeyPersonsText" value="#{pc_NbaClientDetails.numberOfOtherKeyPersons}" styleClass="formDisplayText" >			
				<f:convertNumber integerOnly="true" type="integer" />
			</h:outputText>									
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessKeyPersonSubSection5" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationEstablishedDate" value="#{property.organizationEstablishedDate}" styleClass="formLabel" />
			<h:outputText id="organizationEstablishedDateText" value="#{pc_NbaClientDetails.establishedDate}" styleClass="formDisplayText" style="width: 100px" >
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:outputText>									
			<h:outputLabel id="organizationAllKeyPersonsInsured" value="#{property.organizationAllKeyPersonsInsured}" styleClass="formLabel" style="width: 225px"/>
			<h:outputText id="organizationAllKeyPersonsInsuredText" value="#{pc_NbaClientDetails.allKeyPersonsInsured}" styleClass="formDisplayText" />						
		</h:panelGroup>	
		<h:panelGroup id="organizationBusinessKeyPersonSubSection7" styleClass="formDataDisplayLine">
			<h:outputLabel id="organizationNetIncome" value="#{property.organizationNetIncome}" styleClass="formLabel" />
			<h:outputText id="organizationNetIncomeText" value="#{pc_NbaClientDetails.netIncome}" styleClass="formDisplayText" style="width: 75px">
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
			<h:outputLabel id="organizationKeyEmployeesTotalSalaryAmount" value="#{property.organizationKeyEmployeesTotalSalaryAmount}" styleClass="formLabel" style="width: 250px"/>
			<h:outputText id="organizationKeyEmployeesTotalSalaryAmountText" value="#{pc_NbaClientDetails.keyEmployeeTotalSalaryAmount}" styleClass="formDisplayText" >
				<f:convertNumber maxFractionDigits="2" type="currency" currencySymbol="#{property.currencySymbol}" />
			</h:outputText>									
		</h:panelGroup>		
	</h:panelGroup>		
</jsp:root>

