<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%> 
<%-- NBA213			   7	  Unified User Interface  --%>
<%-- NBA229			   8	  nbA Work List and Search Results Enhancement --%>
<%-- NBA305			NB-1301	  Standardized CSC Product UI Colors --%>
<%-- SPR3811		NB-1301   Corrections to Companion Case View --%>
<%-- NBA352         NB-1501   MIB Plan F Information Received Notification --%>
<!-- SPRNBA-947 	NB-1601   Null Pointer Exception May Occur in Inbox if User Selects Different Work -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
String queryString = request.getQueryString();

//begin NBA213
pagecode.workflow.WorkflowFile bean = new pagecode.workFlow.NbaWorkflowFile(); //NBA229 SPR3811
java.util.Map visibility = bean.getAuth().getVisibility();
Boolean isActionOrInboxAuthorized = (Boolean) visibility.get("actionOrInbox");
Boolean isInboxAuthorized = (Boolean) visibility.get("inboxLabel");
Boolean isActionAuthorized = (Boolean) visibility.get("actionLabel");
Boolean isRelateAuthorized = (Boolean) visibility.get("Relate");
Boolean isCompanionCaseAuthorized = (Boolean) visibility.get("CompanionCase");
Boolean isMIBPlanFInformationAuthorized = (Boolean) visibility.get("MIBPlanFInformation"); // NBA352
//end NBA213
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>" target="control">
<title>Workflow File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
	var context = '<%=path%>';
	var topOffset = 8;

	function getArgs() {
		try {
			var args = new Object();
			var query = location.search.substring(1);
			var pairs = query.split("&");
			for (var i = 0; i < pairs.length; i++) {
				var pos = pairs[i].indexOf('=');
				if (pos == -1) {
					continue;
				}
				var argname = pairs[i].substring(0, pos);
				var value = pairs[i].substring(pos + 1);
				args[argname] = value;
			}
			return args;
		}catch(er){
			reportException(er, "common - getArgs");
		}
	}
	
	function initialize(){
		var args = getArgs();
		if(args['embedded'] != 'true'){
			document.body.className = "desktopBody";
			 
			element = document.getElementById("TitleBar");
			if(element != null){
				element.style.display = "inline";
				element.style.visibility = "visible";
			}
			element = document.getElementById("mainTitle");
			if(element != null){
				element.className = "textMainTitleBar";
			}
			element = document.getElementById("mainTitleHelp");
			if(element != null){
				element.className = "textMainTitleBar";
			}
		} else {
			document.body.className = "updatePanel";
		}
//begin NBA213
<%
	if (isActionOrInboxAuthorized != null && isActionOrInboxAuthorized.booleanValue()) {
 %>
		selectWorkView('workflow/worklistFile.faces', document.getElementById("inboxOption")); //NBA229
<% } else { %>
		selectWorkView('workflow/file/inbox.faces', document.getElementById("inboxOption"));
<% } %>
//end NBA213
	}	
	
	var lastSelection = null;
	
	function selectWorkView(page, item){ 
		 
		
		
		try{
			if(lastSelection != null){
				lastSelection.style.fontWeight = "normal";
				lastSelection.style.color = "white";  //NBA305
			}
			if(item!= null){
				item.style.fontWeight = "bold";
				item.style.color = "#68A64C";  //NBA-305
				lastSelection = item;
			}
		} catch(err){
		}
		loadPage('<%=basePath%>' + page);
		return false;
	}

	//NBA213 New Method
	function initFileSize() {
		tabHeight = window.screen.availHeight - 200;
		//begin SPRNBA-947
		try {
		document.getElementById('file').height = tabHeight;
		} catch(err){
		} 
		//end SPRNBA-947
	}
	
	//SPR3811 New Method
	function unlockCompanionCases() {		 
		document.getElementById("workFlowFileForm:unlockCompanionCases").click();
		
	}
	//SPR3811 New Method	
	function setTargetFrame() {
		document.forms['workFlowFileForm'].target='controlFrame'; 
		return false;
	}
	
	//-->
 	</script>
	<script type="text/javascript" src="javascript/loader.js"></script>
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="initialize();">
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />  <!-- NBA213 -->
	<h:form id="workFlowFileForm">	<!-- NBA213  --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0"
		border="0">
		<tbody>
			<tr style="height:36px;" valign="top" id="TitleBar" style="display:none;">	
				<td>
					<iframe style="position:absolute;zIndex:100;top:-1;left: 5px;height: px;" name="policySelection" src="faces/workflow/file/workSelection.jsp" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
				</td>
			</tr>
			<tr class="whiteBody" >
				<td>
				<table width="100%" height="100%" cellpadding="0" cellspacing="0" id="">
					<tr>
						<td class="textTitleBar" id="mainTitle" height="27px">
							<h:outputText value="#{bundle.interactive_title}"></h:outputText>
<!-- begin NBA213  --> 
<%
	if (isActionOrInboxAuthorized != null && isActionOrInboxAuthorized.booleanValue()) {
 %>
							<a class="textTitleDetail" href="#" onclick="unlockCompanionCases(); return selectWorkView('workflow/worklistFile.faces', this);" id="inboxOption"> <!-- NBA229 SPR3811-->
<%
		if (isInboxAuthorized != null && isInboxAuthorized.booleanValue()) {
 %>
								<h:outputText value="#{bundle.interactive_inbox}" /></a>
<%
		} else if (isActionAuthorized != null && isActionAuthorized.booleanValue()) {
 %>
								<h:outputText value="#{bundle.interactive_action}" /></a>
<%
		}
		if (isRelateAuthorized != null && isRelateAuthorized.booleanValue()) {
 %>
 							<h:outputText styleClass="textTitleDetail" id="bar1" value="#{bundle.interactive_bar}" />
							<a class="textTitleDetail" href="#" onclick="unlockCompanionCases(); return selectWorkView('nbaRelate/nbaRelate.faces', this);" id="relateOption"> <!-- SPR3811 -->
								<h:outputText value="#{bundle.interactive_relate}" /></a>
<%
		}
		if (isCompanionCaseAuthorized != null && isCompanionCaseAuthorized.booleanValue()) {
 %>
							<h:outputText styleClass="textTitleDetail" id="bar2" value="#{bundle.interactive_bar}" />
							<a class="textTitleDetail" href="#" onclick="unlockCompanionCases(); return selectWorkView('nbaCompanionCase/nbaCompanionCaseAction.faces', this);" id="companionCaseOption">  <!-- SPR3811 -->
								<h:outputText value="#{bundle.interactive_companionCase}" /></a>
<%-- begin NBA352 --%>
<%
		}
		if (isMIBPlanFInformationAuthorized != null && isMIBPlanFInformationAuthorized.booleanValue()) {
 %>
							<h:outputText styleClass="textTitleDetail" id="bar3" value="#{bundle.interactive_bar}" />
							<a class="textTitleDetail" href="#" onclick="unlockCompanionCases(); return selectWorkView('nbaMIBPlanFInformation/nbaMIBPlanFInformationAction.faces', this);" id="mibPlanFInformationOption">
								<h:outputText value="#{bundle.interactive_planFInformation}" /></a>
<%-- end NBA352 --%>
<%
		}
	} else {
 %>
<!-- end NBA213  --> 
							<a class="textTitleDetail" href="#" onclick="return selectWorkView('workflow/file/inbox.faces', this);" id="inboxOption">
								<h:outputText value="#{bundle.interactive_inbox}" /></a>
							<span class="textTitleDetail" > | </span>
							<a class="textTitleDetail" href="#" onclick="return selectWorkView('workflow/file/relationships.faces', this);">
								<h:outputText value="#{bundle.interactive_relate}" /></a>
							<span class="textTitleDetail"> | </span>
							<a class="textTitleDetail" href="#" onclick="return selectWorkView('workflow/file/history.faces', this);">
								<h:outputText value="History" /></a>
							<span class="textTitleDetail"> | </span>
							


							
<% } %>
						</td>
						<TD style="padding-right: 15px" align="right" class="textTitleBar" id="mainTitleHelp">
							<Help:Link contextId="CF_Workflow"  imageName="images/help_white.gif" />
						</TD>
					</tr>
					<tr>
						<td colspan="2" valign="top">
							<iframe name="file" src="" class="workflowFile"  height="100%" width="100%" frameborder="0" scrolling="auto" onload="initFileSize();"></iframe>
						</td>
					</tr>
				</table>
			</td>
			</tr>
		</tbody>
	</table>
	<h:commandButton id="unlockCompanionCases" style="display:none" onclick="setTargetFrame();" action="#{pc_WorkflowFile.unlockCompanionCases}" value="" />	<!-- SPR3811 --> 	
	</h:form>	<!-- NBA213  --> 
</f:view>

</body>
</html>
