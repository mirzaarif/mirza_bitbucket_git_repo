<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/tld/CompleteWorkSystems.tld" prefix="CompleteWorkSystems" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Call Center</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script>
	function handleCloseCSDEvent(objectOD,retcode){
		//Update clicked on CSD
		var buttonClicked = '';
		if(retcode == '1006632963')	{
			buttonClicked = 'update';
		}

		//Cancel clicked on CSD
		if(retcode == '1006632964')	{
			buttonClicked = 'cancel';
		}

		if(parent.mainContentFrame.contentLeftFrame.file.clickUnlockFromCSDButon) {
			parent.mainContentFrame.contentLeftFrame.file.clickUnlockFromCSDButon(buttonClicked);
		}
		
	}

	function handleLOBEvent(objectOD,retcode){
	}
	
	function handleLogEvent(text){
	    top.sendLogText("WorkflowExternalSystem",text);
	}
	
	function launchSystem(data){
	    try{
		    document.getElementById('CurrentXML').value = data;
	    }catch(err){
	    }
	    try{
		    SystemsLauncher.invoke(data);
	    }catch(err1){
	    }
	}

	function launchCSD(data){
	    try{
		    document.getElementById('CurrentXML').value = data;
	    }catch(err){
	    }
	    try{
			SystemsLauncher.invokeCSD(data);
	    }catch(err1){
	    }
	}
	
</script>
</head>
<body class="darkBlueBody">
	<CompleteWorkSystems:CompleteWorkSystems/>
	<form>
		CurrentXML:<TEXTAREA name="CurrentXML" id="CurrentXML"></TEXTAREA>
		<br/>
		<BUTTON onclick="SystemsLauncher.invoke(CurrentXML.value);">Launch System</BUTTON>
		<BUTTON onclick="SystemsLauncher.invokeCSD(CurrentXML.value);">Launch CSD</BUTTON>
	</form>	
</body>
</html>