<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA331.1     	NB-1401   AWD REST -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Create Source</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	var width=800;
	var height=450;		

	function setTargetFrame() {
		document.forms['createSourceForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		document.forms['createSourceForm'].target='';
		return false;
	}
	
	function getChoiceValue() {
		if(document.all['createSourceForm:choice'][1].checked) {
			return document.all['createSourceForm:choice'][1].value;
		}
		
		if(document.all['createSourceForm:choice'][2].checked) {
			return document.all['createSourceForm:choice'][2].value;
		}
		
		return 1;
	}
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" style="margin-right: 5px; margin-left: 5px;"
	onload="popupInit();">

<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<f:loadBundle var="bundle2"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<h:form id="createSourceForm">

		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td colspan="4" style="height: 10px" align="right"><Help:Link contextId="BO_CreateSource" /></td>
				</tr>

				<tr>
					<td colspan="4" valign="top">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top">
									<h:selectOneRadio id="choice"
										value="#{pc_CreateSource.sourceChoice}" styleClass="textLabelLeft"
										style="vertical-align: top; height: 50px;" layout="pageDirection"
										valueChangeListener="#{pc_CreateSource.valueChangeRadio}"
										immediate="true" onclick="javascript:submit();">
										<f:selectItem itemLabel="#{bundle2.bckOffice_selectFile}"
											itemValue="1" />
										<f:selectItem itemLabel="#{bundle2.bckOffice_inputText}"
											itemValue="2" />
									</h:selectOneRadio>								
								</td>
								<td valign="top">
									<iframe src="<%=basePath%>/workflow/services/fileUploader.jsp?action=save" width="600" scrolling="NO" frameborder="0"
									marginheight="0" marginwidth="0" height="40 px"></iframe> <!-- NBA331.1 -->
								</td>
							</tr>
							
							<tr>
								<td colspan="2" class="textInput"><h:inputTextarea id="InpText"
									cols="100" rows="6" value="#{pc_CreateSource.inpText}"
									styleClass="textInput"
									disabled="#{pc_CreateSource.sourceChoice != 2}"></h:inputTextarea></td>
							</tr>
							
						</table>
					</td>	
				</tr>

				<tr>
					<td style="height: 10px"></td>
				</tr>


				<tr>
					<td colspan="2" nowrap class="textLabel"><h:outputText
						id="lblReason" value="#{bundle2.create_source_sourceType}">
					</h:outputText></td>
					<td colspan="2"><h:selectOneMenu styleClass="textInput"
						style="width:350px" value="#{pc_CreateSource.sourceType}"
						valueChangeListener="#{pc_CreateSource.valueChangeSourceType}"
						immediate="true" onchange="javascript:submit();" id="sourceType">
						<f:selectItems value="#{pc_CreateSource.sourceTypeList}" />
					</h:selectOneMenu></td>

				</tr>
				<tr>
					<td style="height: 10px"></td>
				</tr>

				<tr>
					<td align="center" colspan="4">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td class="headerRow"><h:dataTable styleClass="header"
									cellpadding="0" width="100%" cellspacing="0" border="0"
									headerClass="headerTable">
									<h:column id="lobfield_asc">
										<f:facet name="header">
											<h:outputText value="#{bundle2.bckOffice_lob_fieldName}"
												styleClass="header" style="width:180px;"></h:outputText>
										</f:facet>
									</h:column>
									<h:column id="value_asc_asc">
										<f:facet name="header">
											<h:outputText value="#{bundle2.close_work_value}"
												styleClass="header" style="width:140px;"></h:outputText>
										</f:facet>
									</h:column>
								</h:dataTable></td>
							</tr>

							<tr>
								<td>
								<div class="divTable" style="height: 146px" id="lobServices"><h:dataTable
									value="#{pc_CreateSource.lobList}" var="currentRecord"
									styleClass="complexTable" columnClasses="column"
									headerClass="header" rowClasses="rowodd" cellpadding="2"
									cellspacing="0" width="100%" border="1"
									id="lobServicesDataTable">
									<h:column id="lobName">
										<h:outputText id="lob" value="#{currentRecord.displayName}"
											styleClass="textTable" style="width:180px;"></h:outputText>
									</h:column>
									<h:column id="lobValue">
										<h:inputText id="lobvalue" styleClass="textInput"
											value="#{currentRecord.dataValue}" style="width:140px;">
										</h:inputText>
									</h:column>
								</h:dataTable></div>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>

				<tr>
					<td style="height: 10px"></td>
				</tr>



				<tr>
					<td colspan="4">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td valign="top"><h:selectBooleanCheckbox
								value="#{pc_CreateSource.createRelationship}"
								id="createRelationship"
								disabled="#{!pc_CreateSource.enableCreateRelationship}" /></td>
							<td valign="bottom" class="textLabel" align="left"><h:outputText id="lblCreateRelation"
								value="#{bundle2.create_source_createRelationship}">
							</h:outputText></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left"><h:commandButton styleClass="button"
						value="#{bundle.cancel}" action="#{pc_CreateSource.cancelAndClear}"
						immediate="true" onclick="setTargetFrame();" /></td>
					<td colspan="2" align="right"><h:commandButton type="submit"
						id="ok" value="#{bundle.ok}" styleClass="button"
						action="#{pc_CreateSource.submit}" onclick="setTargetFrame()">
					</h:commandButton></td>
				</tr>
			</tbody>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
