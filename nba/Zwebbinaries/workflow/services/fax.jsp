<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>

<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                    + "/";
        }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" />
<title>Fax</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css" />
<script type="text/javascript">
	<!--		
	var width=780;
	var height=430;		
	var topOffset = 139;
	var leftOffset=5;

	
	function setTargetFrame() {		
		document.forms['faxForm'].target='controlFrame';
		return false;
	}
	
	
		function resetTargetFrame() {
			document.forms['faxForm'].target='';
			return false;
		}
		
		function setCursor(){
			document.forms[0].item('faxForm:recipient').focus;
		}
	
	//-->
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script type="text/javascript" src="javascript/table.js"></script>
</head>

<body class="updatePanel" onload="popupInit();">
<f:view>
	<PopulateBean:Load serviceName="GET_FAX" value="#{pc_Fax}" />
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle var="bundle2"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="faxForm">
		<table width="100%" align="center" cellpadding="2" cellspacing="0"
			border="0">
			<tbody>

				<tr>
					<td>
					<table width="100%" cellpadding="0" cellspacing="2" border="0">
						<tbody>
							<tr>
								<td colspan="4" align="right"><Help:Link contextId="BO_Fax" /></td>
							</tr>


							<tr>
								<td align="right"><h:outputText styleClass="textLabelMiddle"
									value=" #{bundle.fax_manual} "></h:outputText></td>
								<td><h:selectBooleanCheckbox id="manualRecipient1"
									value="#{pc_Fax.manualRecipient}"
									valueChangeListener="#{pc_Fax.enableRecipientTextBoxes}"
									onclick="javascript:submit();" immediate="true" /></td>

								<td align="right"><h:outputText styleClass="textLabelMiddle"
									value="#{bundle.fax_manual}"></h:outputText></td>
								<td><h:selectBooleanCheckbox id="manualSender"
									value="#{pc_Fax.manualSender}"
									valueChangeListener="#{pc_Fax.enableSenderTextBoxes}"
									onclick="javascript:submit();" immediate="true" /></td>

							</tr>

							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.fax_recipient}"></h:outputText></td>
								<td><h:inputText id="recipient" styleClass="textInput"
									value="#{pc_Fax.recipient}"
									rendered="#{pc_Fax.manualRecipient}"
									binding="#{pc_Fax.recNameInputText}">
								</h:inputText> <h:selectOneMenu value="#{pc_Fax.recipient}"
									id="RecipientInfo" rendered="#{!pc_Fax.manualRecipient}"
									valueChangeListener="#{pc_Fax.recipientValueChanged}"
									style="width:140px;" onchange="javascript:submit();"
									styleClass="textInput" immediate="true">
									<f:selectItems value="#{pc_Fax.recipientList}" />
								</h:selectOneMenu></td>
								<td class="textLabel"><h:outputText value="#{bundle.fax_sender}"></h:outputText></td>
								<td><h:inputText id="sender" styleClass="textInput"
									value="#{pc_Fax.sender}" rendered="#{pc_Fax.manualSender}"
									binding="#{pc_Fax.senderNameInputText}">
								</h:inputText><h:selectOneMenu value="#{pc_Fax.sender}"
									style="width:140px;" id="SenderInfo"
									rendered="#{!pc_Fax.manualSender}"
									valueChangeListener="#{pc_Fax.senderValueChanged}"
									immediate="true" onchange="submit();" styleClass="textInput">
									<f:selectItems value="#{pc_Fax.senderList}" />
								</h:selectOneMenu></td>
							</tr>



							<tr>

								<td class="textLabel"><h:outputText
									value="#{bundle.fax_company}"></h:outputText></td>

								<td><h:inputText id="recipientCompany" styleClass="textInput"
									value="#{pc_Fax.recipient_company}"
									disabled="#{!pc_Fax.manualRecipient && !pc_Fax.selectedRecipientName}"
									binding="#{pc_Fax.recCmpInputText}">
								</h:inputText></td>
								<td class="textLabel"><h:outputText
									value="#{bundle.fax_company}"></h:outputText></td>
								<td><h:inputText id="SenderCompany" styleClass="textInput"
									value="#{pc_Fax.sender_company}"
									disabled="#{!pc_Fax.manualSender && !pc_Fax.selectedSenderName}"
									binding="#{pc_Fax.senderCmpInputText}">
								</h:inputText></td>
							</tr>

							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.fax_phoneNumber}"></h:outputText></td>
								<td><h:inputText id="recipientPhoneNumber"
									styleClass="textInput" value="#{pc_Fax.recipient_phoneNumber}"
									disabled="#{!pc_Fax.manualRecipient && !pc_Fax.selectedRecipientName}"
									binding="#{pc_Fax.recPhoneNumberInputText}">
								</h:inputText></td>
								<td class="textLabel"><h:outputText
									value="#{bundle.fax_phoneNumber}"></h:outputText></td>
								<td><h:inputText id="senderPhoneNumber" styleClass="textInput"
									value="#{pc_Fax.sender_phoneNumber}"
									disabled="#{!pc_Fax.manualSender && !pc_Fax.selectedSenderName}"
									binding="#{pc_Fax.senderPhoneNumberInputText}">
								</h:inputText></td>
							</tr>

							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.fax_faxNumber}"></h:outputText></td>
								<td><h:inputText id="recipientFaxNumber" styleClass="textInput"
									value="#{pc_Fax.recipient_faxNumber}"
									disabled="#{!pc_Fax.manualRecipient && !pc_Fax.selectedRecipientName}"
									binding="#{pc_Fax.recFaxNumberInputText}">
								</h:inputText></td>
								<td class="textLabel"><h:outputText value="#{bundle.fax_Number}"></h:outputText></td>
								<td><h:inputText id="senderFaxNumber" styleClass="textInput"
									value="#{pc_Fax.sender_faxNumber}"
									disabled="#{!pc_Fax.manualSender && !pc_Fax.selectedSenderName}"
									binding="#{pc_Fax.senderFaxNumberInputText}">
								</h:inputText></td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>

				<tr>
					<td align="left">
					<table width="100%" cellpadding="0" cellspacing="2" border="0">
						<tbody>
							<tr>
								<td colspan="4">
								<hr>
								</td>
							</tr>
							<tr>
								<td colspan="4"><h:outputText styleClass="subTextTitle"
									value="#{bundle.fax_annotations}"></h:outputText></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"><h:selectBooleanCheckbox id="arrow"
									value="#{pc_Fax.arrow}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_arrow}">
								</h:outputText> <h:selectBooleanCheckbox id="highlight"
									value="#{pc_Fax.highlight}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_highlight}"></h:outputText>
								<h:selectBooleanCheckbox id="scribble"
									value="#{pc_Fax.scribble}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_scribble}">
								</h:outputText> <h:selectBooleanCheckbox id="overlay"
									value="#{pc_Fax.overlay}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_overlay}">
								</h:outputText> <h:selectBooleanCheckbox id="mask"
									value="#{pc_Fax.mask}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_mask}">
								</h:outputText><h:selectBooleanCheckbox id="sticky"
									value="#{pc_Fax.sticky}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_sticky}">
								</h:outputText> <h:selectBooleanCheckbox id="line"
									value="#{pc_Fax.line}" /><h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_line}">
								</h:outputText></td>
							</tr>
							<tr>
								<td colspan="4">
								<hr>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>

				<tr>
					<td>
					<table width="100%" cellpadding="0" cellspacing="2" border="0">
						<tbody>
							<tr>
								<td colspan="4" align="left">
								<table>
									<tr>
										<td><h:outputText styleClass="subTextTitle"
											value="#{bundle.fax_pages}"></h:outputText></td>
										<td><h:selectOneRadio required="true" layout="lineDirection"
											value="#{pc_Fax.pageType}" styleClass="textLabel" id="type">
											<f:selectItem itemLabel="#{bundle.fax_all}" itemValue="all" />
											<f:selectItem itemLabel="#{bundle.fax_selected}"
												itemValue="selected" />
										</h:selectOneRadio></td>
										<td><h:inputText id="page" styleClass="textInput"
											value="#{pc_Fax.selectedPage}" /></td>
									</tr>
								</table>
								</td>
							</tr>

							<tr>
								<td colspan="4"></td>
							</tr>

							<tr>
								<td colspan="4"><h:selectBooleanCheckbox id="includeCoverSheet"
									valueChangeListener="#{pc_Fax.enableTextArea}"
									onclick="javascript:submit();" immediate="true"
									value="#{pc_Fax.includeCoverSheet}" /> <h:outputText
									styleClass="textLabelMiddle" value="#{bundle.fax_include}">
								</h:outputText></td>
							</tr>

							<tr>
								<td class="textInput" colspan="4"><h:inputTextarea id="comments"
									cols="120" rows="8" required="false" tabindex="2"
									styleClass="textInput" value="#{pc_Fax.coverSheet}"
									disabled="#{!pc_Fax.includeCoverSheet}"></h:inputTextarea></td>
							</tr>

							<tr valign="bottom">
								<td align="right" colspan="4"><h:commandButton id="Cancel"
									styleClass="buttonCancel" value="#{bundle2.cancel}"
									action="#{pc_Fax.cancel}" onclick="setTargetFrame();"
									immediate="true" /> <h:commandButton
									styleClass="button" value="#{bundle2.ok}"
									action="#{pc_Fax.submit}" onclick="setTargetFrame();" /></td>
							</tr>

						</tbody>
					</table>
					</td>
				</tr>

			</tbody>
		</table>
		<div id="Messages" style="display: none"><h:messages></h:messages></div>
	</h:form>
</f:view>
</body>
</html>
