<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>


<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                    + "/";
        }
%>
<head>
<base href="<%=basePath%>">
<title>Create Work</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
<!-- 
  		var width=550;
		var height=320;
		
		function initCreateWork(){
			document.forms[0].item('createWork:ok').focus();
		}
		
		function resetParam(){
			document.forms['createWork']['createWork:SuspendUntil'].value= "";		
			document.forms['createWork']['createWork:SuspendReason'].value= "";
			//document.forms['createWork']['createWork:SuspendStatus'].value= "";			
		}
		
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['createWork'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['createWork'].target='';
			return false;
		}
          
		//-->
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="popupInit();">
<f:view>
	<PopulateBean:Load serviceName="BACKOFFICE_RETRIEVE_CREATE_WORK" value="#{pc_CreateWork}" />
	<h:form id="createWork">
		<h:outputText value="#{pc_CreateWork.refresh}" style="display:none;" id="lblRefresh"></h:outputText>
		<f:loadBundle var="bundle" 
			basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
		<f:loadBundle var="bundle2" 
			basename="com.csc.fs.accel.ui.config.ApplicationData" />
		<table cellpadding="3" cellspacing="1" width="100%" border="0" align="center">
			<tr>
				<td class="textLabel"><h:outputText id="lblCreate"
					value="#{bundle.create_work_creatework}">
				</h:outputText></td>
				<td colspan="3"><h:selectOneMenu id="CreateWork"
					styleClass="textInput" value="#{pc_CreateWork.workTemplate}"  style="width:350px"
					valueChangeListener="#{pc_CreateWork.createWorkListener}" onchange="resetTargetFrame();submit();" >
					<f:selectItems value="#{pc_CreateWork.createWorkList}" />
				</h:selectOneMenu>
				</td>
				<td>
					<Help:Link contextId="CSA_CreateWork"/>
				</td>
			</tr>
			<tr>
				<td class="textLabel"><h:outputText id="lblSuspend"
					value="#{bundle.create_work_suspendwork}">
				</h:outputText>
				</td>
				<td class="textLabel" style="text-align:left; vertical-align: middle;"><h:selectBooleanCheckbox
					value="#{pc_CreateWork.suspendWork}" immediate="true" id="suspendWrk"
					valueChangeListener="#{pc_CreateWork.suspensionListener}"
					onclick="resetTargetFrame();javascript:submit();" /> </td>
				<td class="textLabel"><h:outputText id="lblUntil"
					value="#{bundle.create_work_suspenduntil}">
				</h:outputText></td>
				<td><h:inputText id="suspendUntil" 
					value="#{pc_CreateWork.suspendUntil}"
					disabled="#{!pc_CreateWork.enableSuspended}" styleClass="textInput">
					<f:convertDateTime type="both" pattern="#{bundle2.dateTimePattern}" />
				</h:inputText></td>
			</tr>
			<tr>
				<td class="textLabel"><h:outputText id="lblReason"
					value="#{bundle.create_work_suspendreason}">
				</h:outputText></td>
				<td colspan="3"><h:selectOneMenu styleClass="textInput" style="width:350px"
					value="#{pc_CreateWork.suspendReason}" 
					disabled="#{!pc_CreateWork.enableSuspended}" id="suspendReason">
					<f:selectItems value="#{pc_CreateWork.suspendReasonList}" />
				</h:selectOneMenu></td>
			</tr>
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" width="98%" border="0">
						<tr>
					<td class="headerRow">
							<h:dataTable styleClass="header" headerClass="headerTable" cellpadding="0" width="100%"
						cellspacing="0" columnClasses="column" id="hdrWork">
						<h:column id="lobfield_asc">
							<f:facet name="header">
								<h:outputText value="#{bundle.create_work_lob}" id="lblLOB"
									styleClass="header" style="width:160px;"></h:outputText>
							</f:facet>
						</h:column>
						<h:column id="value_asc_asc">
							<f:facet name="header">
								<h:outputText value="#{bundle.create_work_value}" id="lblVal"
									styleClass="header" style="width:235px;"></h:outputText>
							</f:facet>
						</h:column>
					</h:dataTable>
					<div class="divTable" style="height: 176px" id="lobServices"><h:dataTable
						value="#{pc_CreateWork.lobList}" var="currentRecord"
						styleClass="complexTable" columnClasses="column"
						headerClass="header" rowClasses="row" cellpadding="2"
						cellspacing="0" width="100%" border="1" id="createLobTable">

						<h:column id="lobName">
							<h:outputText id="lob" value="#{currentRecord.displayName}" 							
								styleClass="textTable" style="width:160px;"></h:outputText>
						</h:column>
						<h:column id="lobValue">
							<h:inputText id="lobvalue"
								readonly="#{pc_CreateWork.enableSuspended}"
								styleClass="textInput" value="#{currentRecord.dataValue}"
								style="width:220px;">
							</h:inputText>
						</h:column>
					</h:dataTable></div>
					</td>
					</tr>
					</table>
					</td>
				</tr>
			<tr>
				<td align="right" colspan="4"><h:commandButton id="Ok"
					styleClass="button" value="#{bundle2.ok}"
					onclick="setTargetFrame();" style="width:70px"
					action="#{pc_CreateWork.submit}"></h:commandButton>
				<h:commandButton id="cancel" styleClass="buttonCancel"
					value="#{bundle2.cancel}"
					onclick="setTargetFrame();" style="width:70px" type="submit"
					immediate="true" action="#{pc_CreateWork.cancelAndClear}"></h:commandButton>					
				</td>
			</tr>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages/></div>
</f:view>
</body>
</html>
