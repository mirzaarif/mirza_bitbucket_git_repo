<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Search Work Item</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	var width=1000;
	var height=560;
	
	function clearSearchParam(){
		document.forms['searchWorkItemForm']['searchWorkItemForm:businessArea'].value = "";
		document.forms['searchWorkItemForm']['searchWorkItemForm:createDateTimeFrom'].value = "";
		document.forms['searchWorkItemForm']['searchWorkItemForm:createDateTimeTo'].value = "";	
		if(document.forms['searchWorkItemForm']['searchWorkItemForm:type'][0].checked) {
			document.forms['searchWorkItemForm']['searchWorkItemForm:workType'].value = "";
			document.forms['searchWorkItemForm']['searchWorkItemForm:status'].value = "";
			document.forms['searchWorkItemForm']['searchWorkItemForm:queue'].value = "";
		} else {
			document.forms['searchWorkItemForm']['searchWorkItemForm:sourceType'].value = "";
		}
	}
	
	function highlightRow(){
		top.scrollHighlight(document.getElementById('SearchTableBody'), 
					document.getElementById('searchWorkItemForm:selectedRow').innerHTML);
	}	

	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['searchWorkItemForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['searchWorkItemForm'].target='';
		return false;
	}
	
	function searching(){
		document.getElementById('SearchFormTable').style.display = "none";
		document.getElementById('waitSearching').style.display = "inline";
	}
	
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
<script type="text/javascript" src="javascript/table.js"></script>
</head>
<body class="updatePanel" style="margin-right: 5px; margin-left: 5px;"
	onload="popupInit();highlightRow();">
<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle var="bundle2"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="searchWorkItemForm">
		<h:outputText value="#{pc_SearchWorkItem.refresh}"></h:outputText>
		<table width="100%" align="center" cellpadding="0" cellspacing="0"
			border="0">
			<tbody>
				<tr>
					<td colspan="2" align="right"><Help:Link contextId="BO_Search" /></td>
				</tr>
				<tr>
					<td  align="center"  colspan="2">
						<h:outputText id="lookupmenulabel" styleClass="textLabel" style="vertical-align:text-bottom;"
							value="Search Favorites:"></h:outputText>
						<h:selectOneMenu id="lookupmenuList" styleClass="textInput" style="width:300px">
								<f:selectItem itemLabel="MySearch1"
								itemValue="MySearch2" />
								<f:selectItem itemLabel="MySearch2"
								itemValue="MySearch2" />
								<f:selectItem itemLabel="MySearch3"
								itemValue="MySearch3" />
								<f:selectItem itemLabel="--- System Searches ---"
								itemValue="--- System Searches ---" />
								<f:selectItem itemLabel="Policy WorkItem Search"
								itemValue="PolicyWorkItemSearch" />
								<f:selectItem itemLabel="Policy Sources Search"
								itemValue="PolicySourcesSearch" />
						</h:selectOneMenu>
						<h:commandButton id="SaveFavorite" type="submit"
							value="Save" styleClass="button"
							action=""
							onclick="resetTargetFrame();">
						</h:commandButton>
					</td>
					</tr>
<tr>
					<td colspan="2" height="5px">
					<HR>
					</td>
				</tr>					
					<tr>
						<td colspan="2" align="center">
						<h:selectOneRadio required="true"
							valueChangeListener="#{pc_SearchWorkItem.valueChangeRadio}"
							layout="lineDirection" value="#{pc_SearchWorkItem.lookupType}"
							onclick="javascript:submit();" styleClass="textLabel" id="type"
							immediate="true">
							<f:selectItem itemLabel="#{bundle.backoffice_work_item}"
								itemValue="W" />
							<f:selectItem itemLabel="#{bundle.bckOffice_SourceItem}"
								itemValue="S" />
						</h:selectOneRadio></td>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
					<table width="100%" align="center" cellpadding="0" cellspacing="0"
						border="0" height="150">
						<tbody>
							<tr>
								<td>
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="textLabel" width="50%"><h:outputText
												value="#{bundle.businessArea}:"></h:outputText></td>
											<td class="text"><h:inputText id="businessArea"
												value="#{pc_SearchWorkItem.businessArea}"
												styleClass="textInput"></h:inputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.workType}:"
												rendered="#{pc_SearchWorkItem.lookupType == 'W'}"></h:outputText>
											<h:outputText value="#{bundle.bckOffice_sourceType}:"
												rendered="#{pc_SearchWorkItem.lookupType == 'S'}"></h:outputText>
											</td>
											<td class="text"><h:inputText id="workType"
												value="#{pc_SearchWorkItem.workType}" styleClass="textInput"
												rendered="#{pc_SearchWorkItem.lookupType == 'W'}"></h:inputText>
											<h:inputText id="sourceType"
												value="#{pc_SearchWorkItem.sourceType}"
												styleClass="textInput"
												rendered="#{pc_SearchWorkItem.lookupType == 'S'}"></h:inputText>
											</td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText value="#{bundle.status}:"></h:outputText></td>
											<td class="text"><h:inputText id="status"
												value="#{pc_SearchWorkItem.status}" styleClass="textInput"
												disabled="#{pc_SearchWorkItem.lookupType == 'S'}"></h:inputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText value="#{bundle.queue}:"></h:outputText></td>
											<td class="text"><h:inputText id="queue"
												value="#{pc_SearchWorkItem.queue}" styleClass="textInput"
												disabled="#{pc_SearchWorkItem.lookupType == 'S'}"></h:inputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.createDateTimeFrom}:"></h:outputText></td>
											<td class="text"><h:inputText id="createDateTimeFrom"
												value="#{pc_SearchWorkItem.createDateTimeFrom}"
												styleClass="textInput">
												<f:convertDateTime type="date"
													pattern="#{bundle2.date_2Pattern}" />
											</h:inputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.createDateTimeTo}:"></h:outputText></td>
											<td class="text"><h:inputText id="createDateTimeTo"
												value="#{pc_SearchWorkItem.createDateTimeTo}"
												styleClass="textInput">
												<f:convertDateTime type="date"
													pattern="#{bundle2.date_2Pattern}" />
											</h:inputText></td>
										</tr>
									</tbody>
								</table>
								</td>
								<td align="left">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr class="textTitleBar">
											<td class="textLabel"><h:outputText id="addLobToCritertia"
												value="Search Field:" style="margin-top: 5px;"></h:outputText>
											</td>
											<td>
											<h:selectOneMenu id="addLobList" styleClass="textInput">
													<f:selectItems value="#{pc_SearchWorkItem.allowableLobList}" />
											</h:selectOneMenu>
											<h:commandButton id="AddLOB" type="submit"
												value="Add" styleClass="button"
												action=""
												onclick="resetTargetFrame();">
											</h:commandButton>
											</td>
										</tr>										
										<tr>
											<td class="headerRow" colspan="2"><h:dataTable styleClass="header"
												cellpadding="0" width="100%" cellspacing="0" border="0"
												headerClass="headerTable">
												<h:column id="lobfield_asc">
													<f:facet name="header">
														<h:outputText value="#{bundle.bckOffice_lob_fieldName}"
															styleClass="header" style="width:180px;"></h:outputText>
													</f:facet>
												</h:column>
												<h:column id="value_asc_asc">
													<f:facet name="header">
														<h:outputText value="#{bundle.close_work_value}"
															styleClass="header" style="width:140px;"></h:outputText>
													</f:facet>
												</h:column>
											</h:dataTable></td>
										</tr>
										<tr>
											<td colspan="2">
											<div class="divTable" style="height: 146px" id="lobServices"><h:dataTable
												value="#{pc_SearchWorkItem.lobList}" var="currentRecord"
												styleClass="complexTable" columnClasses="column"
												headerClass="header" rowClasses="rowodd" cellpadding="2"
												cellspacing="0" width="100%" border="1"
												id="lobServicesDataTable">
												<h:column id="lobName">
													<h:outputText id="lob" value="#{currentRecord.displayName}"
														styleClass="textTable" style="width:180px;"></h:outputText>
												</h:column>
												<h:column id="lobValue">
													<h:inputText id="lobvalue" styleClass="textInput" rendered="#{!currentRecord.useList}"
														value="#{currentRecord.dataValue}" style="width:140px;">
													</h:inputText>
													<h:selectOneMenu id="lobValueList" styleClass="textInput" rendered="#{currentRecord.useList}"
														value="#{currentRecord.dataValue}" style="width:140px;">
															<f:selectItems value="#{currentRecord.avList}" />
													</h:selectOneMenu>
												</h:column>
											</h:dataTable></div>
											</td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="right">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td colspan="4" align="right"><h:commandButton type="submit"
												value="#{bundle.bckOffice_search}" styleClass="button"
												action="#{pc_SearchWorkItem.searchWorkItem}"
												onclick="resetTargetFrame();"></h:commandButton> <h:commandButton
												type="submit" value="#{bundle.cust_clear}"
												styleClass="button"
												actionListener="#{pc_SearchWorkItem.reset}"
												onclick="clearSearchParam();resetTargetFrame();"
												immediate="true"></h:commandButton></td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
				<tr>
					<td height="10" colspan="2"></td>
				</tr>
				<tr class="textTitleBar">
					<td colspan="2"><h:outputText
						value="#{bundle.bckOffice_search_results}"></h:outputText></td>
				</tr>
				<tr valign="bottom">
					<td width="100%" colspan="2" class="headerRow"><h:dataTable
						styleClass="header" cellpadding="0" width="100%" cellspacing="0"
						border="0" headerClass="headerTable">
						<h:column>
							<f:facet name="header">
								<h:commandLink id="lockCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();">
									<h:outputText style="width:41px;"
										value="#{bundle.bckOffice_locked_icon}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="userIdCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:80px;"
										value="#{bundle.bckOffice_userId}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="polNumberCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:100px;"
										value="#{bundle.bckOffice_policyclaim_number}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="nameCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:120px;"
										value="#{bundle.result_name}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="bussAreaCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:120px;"
										value="#{bundle.businessArea}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="sourceTypeCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();" rendered="#{pc_SearchWorkItem.lookupType == 'S'}"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
											<h:outputText value="#{bundle.bckOffice_sourceType}" style="width:300px;" 
												rendered="#{pc_SearchWorkItem.lookupType == 'S'}"></h:outputText>
								</h:commandLink>
							</f:facet>	
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="workTypeCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();" rendered="#{pc_SearchWorkItem.lookupType == 'W'}"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
											<h:outputText style="width:100px;" 
												value="#{bundle.workType}"></h:outputText>
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="statusCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();" rendered="#{pc_SearchWorkItem.lookupType == 'W'}"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:100px;" value="#{bundle.status}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="queueCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();" rendered="#{pc_SearchWorkItem.lookupType == 'W'}"
									actionListener="#{pc_SearchWorkItem.sortColumn}">
									<h:outputText style="width:100px;" value="#{bundle.queue}" />
								</h:commandLink>
							</f:facet>
						</h:column>
					</h:dataTable></td>
				</tr>
				<tr>
					<td colspan="2">
					<div id="SearchTableBody" class="divTable" style="height: 150px"><h:dataTable
						id="dWISearchTable" var="currentRow" columnClasses="column"
						rowClasses="#{pc_SearchWorkItem.rowStyles}" headerClass="header"
						styleClass="complexTable" cellpadding="2" cellspacing="0"
						width="100%" border="5" binding="#{pc_SearchWorkItem.table}"
						value="#{pc_SearchWorkItem.records}">
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_SearchWorkItem.selectRow}"
								style="width:60px;text-align:center;">
								<h:graphicImage url="#{currentRow.imagePath}"
									style="border:0px;text-align:center;" />
								<h:graphicImage url="images/pause.gif" style="border:0px;"
									rendered="#{pc_SearchWorkItem.lookupType == 'W' && currentRow.suspendFlag == 'Y'}" />
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:80px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText 
									value="#{currentRow.lockStatus}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:100px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText 
									value="#{currentRow.policyAndClaimNumber}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:120px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText  value="#{currentRow.name}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:120px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText 
									value="#{currentRow.businessArea}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:120px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText 
									value="#{currentRow.workType}"
									rendered="#{pc_SearchWorkItem.lookupType == 'W'}"></h:outputText>
								<h:outputText 
									value="#{currentRow.sourceType}"
									rendered="#{pc_SearchWorkItem.lookupType == 'S' && currentRow.identifier != 'TempId'}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:100px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText value="#{currentRow.status}"
									rendered="#{pc_SearchWorkItem.lookupType == 'W'}"></h:outputText>
						</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" style="width:83px;"
								action="#{pc_SearchWorkItem.selectRow}" styleClass="textTable">
								<h:outputText value="#{currentRow.queueID}"
									rendered="#{pc_SearchWorkItem.lookupType == 'W'}"></h:outputText>
							</h:commandLink>
						</h:column>
					</h:dataTable></div>
					<h:outputText style="display:none;" id="selectedRow"
						value="#{pc_SearchWorkItem.selectedRowIndex}"></h:outputText></td>
				</tr>

				<tr valign="top" class="resultSummaryBar">
					<td width="100%" colspan="2"><h:outputText id="resultsSummary"
						value="#{pc_SearchWorkItem.noOfWorkItems} #{bundle.numRowsReturned}"></h:outputText>
					</td>
				</tr>
				<tr>
					<td><h:commandButton value="#{bundle2.cancel}" styleClass="button"
						immediate="true" action="#{pc_SearchWorkItem.cancelAndClear}"
						onclick="document.forms[0].target='controlFrame'">
					</h:commandButton></td>
					<td align="right">
						<h:commandButton type="submit" id="More"
						value="#{bundle.bckOffice_More}" styleClass="button"
						onclick="resetTargetFrame()"
						disabled="#{!pc_SearchWorkItem.nextEnabled}"
						action="#{pc_SearchWorkItem.nextWorkItems}">
					</h:commandButton>					
					<h:commandButton type="submit" id="ok"
						value="#{bundle2.ok}" styleClass="button"
						onclick="document.forms[0].target='controlFrame'"
						disabled="#{pc_SearchWorkItem.enableButton}"
						action="#{pc_SearchWorkItem.selectWorkItem}">
					</h:commandButton></td>
				</tr>
			</tbody>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>