<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
<head>
<base href="<%=basePath%>">
<title><h:outputText value"#{bundle.work_viewDetails}" /></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=550;
		var height=380;

		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['viewWorkDetailsForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['viewWorkDetailsForm'].target='';
			return false;
		}
		
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="popupInit();">
	<PopulateBean:Load serviceName="RETRIEVE_WORK_DETAILS" value="#{pc_WorkDetails}" />
	<h:form id="viewWorkDetailsForm">
		<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-top: 20px">
			<tbody>
				<tr>
					<td>
						<h:outputText styleClass="subTextTitle" style="margin-left: 10px"
							value="#{bundle.bckOffice_lobHeader}" /></td>
				</tr>
				<tr>
					<td>
						<div class="divTable" style="height: 300px; width: 530px; margin-left: 10px;">
							<h:dataTable
								var="lobData" border="1" id="inboxLobTable"
								value="#{pc_WorkDetails.lobData}"
								cellspacing="0"	cellpadding="0" columnClasses="column" rowClasses="row"
								headerClass="header" styleClass="complexTable" width="100%">
								<h:column>
									<h:outputText style="width:200px; padding-left: 3px"
										value="#{lobData.displayName}"></h:outputText>
								</h:column>
								<h:column>
									<h:outputText styleClass="text"
										style="vertical-align:text-bottom; width:325px; overflow: hidden;"
										value="#{lobData.dataValue}"></h:outputText>
								</h:column>
							</h:dataTable>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right" style="padding-top: 10px; padding-right: 10px">
						<h:commandButton id="ok"
							styleClass="button" value="#{bundle.work_close}"
							action="#{pc_WorkDetails.close}"
							onclick="setTargetFrame();"></h:commandButton>
					</td>
				</tr>
			<tbody>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
