<%@ page language="java"%>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA331.1     	NB-1401   AWD REST -->

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }

			//begin NBA331.1            
			String action = request.getQueryString();
			if(action != null){
				int i = action.indexOf("action=");
				if(i >= 0){
					action = action.substring(i + 7);
				}
			}
			//end NBA331.1 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Create Source</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	function setTargetFrame() {
		document.forms['FileUploader'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		document.forms['FileUploader'].target='';
		return false;
	}
	
	function initalizeUploader() {
		if(parent.location.href.indexOf("createSource") != -1 ){
			if(parent.getChoiceValue() != '1') {
				document.getElementById('source').disabled='true';
			}
		}
		
		var msg1 = '<%=session.getAttribute("fileUpload_message")%>';
		if(msg1 != 'null') {
			setFileMessage(msg1);
		}
	}
	
	function setFileMessage(msg1) {
		document.getElementById('file_message').innerHTML = msg1;
	}
	
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel" leftmargin="0" rightmargin="0" topmargin="0" onload="initalizeUploader()">
	<form name="FileUploader" action="fileUploader?action=<%=action%>" enctype="multipart/form-data" method="post">
		<table cellpadding="0" cellspacing="0" style="vertical-align: top" class="textLabelLeft" width="600">
			<tr>
				<td>
					<input type="file" name="source" style="width: 100px" class="fileUploaderClass" onchange="setFileMessage('Uploading file');resetTargetFrame(); this.form.submit();"/>				
				</td>
				<td width="500">
					<span id="file_message" style="font: bold 10px 'Verdana'; color: navy; text-decoration: none;"></span>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>