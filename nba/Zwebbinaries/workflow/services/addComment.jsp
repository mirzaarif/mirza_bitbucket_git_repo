<%@ page language="java"
	extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Add Comment</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=510;
		var height=280;

		function setTargetFrame() {
			document.forms['commentForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['commentForm'].target='';
			return false;
		}
        function focusComment(){
	     try{
		    document.forms[0].item('commentForm:comments').focus();
	 	    }catch(err){
		    }
	    }

	function populateCombo() {
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		folderObj = fso.GetFolder(getCommentsFolder());
		files = folderObj.Files
		e  = new Enumerator(files); 
		var i=1;
		do { 
		   x  =  e.item(); 
		   if(getExtension(x.Name) == 'cmt'){
			   var name = getName(x.Name);
			   document.getElementById('ListOfCmtFiles').options[i] = new Option(name, x.Path)
			   i++;
			}
			e.moveNext(); 
		 }  while (!e.atEnd()); 
	}

	function getName(name) {
		return name.split('.')[0];
	}

	function getExtension(name) {
		return name.split('.')[1];	
	}
	
	function checkCommentsFolder() {
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var cmtFolder = document.getElementById('commentForm:cmtFolder').innerHTML
		//If folder does not exist then create it
		if(!fso.FolderExists(cmtFolder)) {
			folderArray = getFolderArray(cmtFolder);
			var folderStructure = '';
			for(i=0; i < folderArray.length; i++){	
				folderStructure += folderArray[i]
				fso = new ActiveXObject("Scripting.FileSystemObject");
				if(!fso.FolderExists(folderStructure)) {
					fso.CreateFolder(folderStructure);
				}
				folderStructure += '\\';
				
			}
		}
		return cmtFolder;		
	}

	function getFolderArray(folderPath) {
		var arr = folderPath.split('\\');
		if(arr.length >= 2) {
			folderArray = new Array(arr.length-1);
			folderArray[0] = arr[0] + '\\' + arr[1];
			for(i=2;i<arr.length;i++) {		
				folderArray[i-1] = arr[i];
			}
			return folderArray;
		}
		return arr;
	}

	function getCommentsFolder() {	
		var cmtFolder = document.getElementById('commentForm:cmtFolder').innerHTML
		return cmtFolder;		
	}
	
	function saveToFile() {
		var fileName = document.getElementById('commentForm:fileToSave').value
		if(fileName != '' || fileName.indexOf('.') != -1) {
			var path = getCommentsFolder() + '\\' + fileName + '.cmt';
			var TristateFalse = 0;
			var ForWriting = 2;
			myActiveXObject = new ActiveXObject("Scripting.FileSystemObject");
			myActiveXObject.CreateTextFile(path);
			file = myActiveXObject.GetFile(path);
			text = file.OpenAsTextStream(ForWriting, TristateFalse);
			text.Write(document.getElementById('commentForm:InpText').value);
			text.Close();
		}
	}

	function getFileContents(path) {
		var TristateFalse = 0;
		myActiveXObject = new ActiveXObject("Scripting.FileSystemObject");
		file = myActiveXObject.GetFile(path);
		text = file.OpenAsTextStream(1, TristateFalse); 
		document.getElementById('commentForm:InpText').value = text.readAll();
		file.close();
	}
	//-->
</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>
<body class="updatePanel"
	onload="popupInit();checkCommentsFolder();populateCombo();focusComment()">
<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle var="bundle2"
		basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="commentForm">
		<h:outputText style="display:none;" value="#{pc_WorkComment.refresh}"></h:outputText>
		<table width="100%" height="100%" border="0">
				<tr>
				<td colspan="2" align="right"><Help:Link contextId="BO_Add_Comment" />
				</td>
				</tr>
				<tr>
				<td class="textLabel"><h:outputText
					value="#{bundle.workItems_InsertFrom}" /></td>
				<td class="textInput"><select id="ListOfCmtFiles" style="width: 135px"
					name="ListOfCmtFiles" onchange="getFileContents(this.value);">
					<option value="-1"></option>
				</select></td>
			</tr>

			<tr>
				<td class="textLabel"><h:outputText value="#{bundle.bckOffice_selectComment}" /></td>
				<td valign="top"><input type="file" class="fileUploaderClass"
					onchange="getFileContents(this.value)" /></td>
			</tr>

			<tr>
				<td colspan="2" class="textInput"><h:inputTextarea id="InpText"
					cols="80" rows="6" value="#{pc_WorkComment.comments}"
					styleClass="textInput"></h:inputTextarea></td>
			</tr>

			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" width="400px">
						<tr>
							<td class="textLabel" width="150px"><h:outputText value="#{bundle.bckOffice_SaveInFile}" /></td>						
							<td class="textInput" width="150px"><h:inputText id="fileToSave"
									 value="#{pc_WorkComment.fileToSave}"></h:inputText></td>
							 <td width="100px"><h:commandButton id="save" 
								value="#{bundle.bckOffice_Save}" styleClass="button" onclick="saveToFile();resetTargetFrame();" action="#{pc_WorkComment.saveToFile}"/>
							</h:commandButton></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="1" align="left"><h:commandButton styleClass="button"
					value="#{bundle2.cancel}" action="#{pc_WorkComment.cancelAndClear}"
					immediate="true" onclick="setTargetFrame();" /></td>
				<td colspan="1" align="right"><h:commandButton type="submit" id="ok"
					value="#{bundle2.ok}" styleClass="button"
					action="#{pc_WorkComment.submit}" onclick="setTargetFrame()">
				</h:commandButton>
				<h:outputText value="#{pc_WorkComment.commentFolder}" id="cmtFolder" style="display: none"></h:outputText>
				</td>
			</tr>
		</table>
		<div id="Messages" style="display: none"><h:messages></h:messages></div>

	</h:form>
</f:view>
</body>
</html>
