<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>">
<title>Complete Quality Review</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=855;
		var height=430;

		function initCloseWork(){
			var currentComments = top.getDesktopComments();
			document.forms[0].item("closeWorkForm:comments").value = currentComments;
			document.forms[0].item('closeWorkForm:ok').focus();
		}
		function highlightRow(){
			top.scrollHighlight(document.getElementById('historyQualTable'), 
						document.getElementById('closeWorkForm:selectedRow').innerHTML);
			top.scrollHighlight(document.getElementById('reviewTable'), 
						document.getElementById('closeWorkForm:selectedReviewRow').innerHTML);						
		}
		function resetParam(){
		  if (document.forms['closeWorkForm']['closeWorkForm:SelectReason'].value == "Pass"){
				document.forms['closeWorkForm']['closeWorkForm:errorPoints'].value= "";		
				document.forms['closeWorkForm']['closeWorkForm:errorImpact'].checked= false;
				document.forms['closeWorkForm']['closeWorkForm:errorPoints'].disabled = true;
				document.forms['closeWorkForm']['closeWorkForm:errorImpact'].disabled = true;
			}else{
				if (document.forms['closeWorkForm']['closeWorkForm:errorPoints'].disabled == true){
				document.forms['closeWorkForm']['closeWorkForm:errorPoints'].disabled = false;
				document.forms['closeWorkForm']['closeWorkForm:errorImpact'].disabled = false;				
				}
			}
		}

	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['closeWorkForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['closeWorkForm'].target='';
		return false;
	}				
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel"
	onload="popupInit();initCloseWork();highlightRow();resetParam();">
<f:view>
	<PopulateBean:Load serviceName="GET_CLOSE_WORK_QUALITY"
		value="#{pc_CloseWorkQuality}" />
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle var="bundle2" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:form id="closeWorkForm">
		<table cellpadding="3" cellspacing="0" width="100%" border="0">
			<tbody>
				<tr>
					<td width="45%" valign="top">
					<table width="100%" align="center" cellpadding="0" cellspacing="0"
						border="0">
						<tbody>
							<tr>
								<td><h:outputText styleClass="subTextTitle"
									value="#{bundle.trad_ucdHistory}"></h:outputText></td>
							</tr>
							<tr>
							<td class="headerRow"><h:dataTable cellpadding="0"
									cellspacing="0" width="100%" styleClass="header"
									headerClass="headerTable" border="0">
									<h:column>
										<f:facet name="header">
											<h:commandLink id="processorCol_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_CloseWorkQuality.sortColumn}">
												<h:outputText style="width:90px;"
													value="#{bundle.bckOffice_processor}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="bussAreaCol_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_CloseWorkQuality.sortColumn}">
												<h:outputText style="width:90px;"
													value="#{bundle.businessArea}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="workTypeCol_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_CloseWorkQuality.sortColumn}">
												<h:outputText style="width:90px;"
													value="#{bundle.workType}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="statusCol_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_CloseWorkQuality.sortColumn}">
												<h:outputText style="width:90px;" value="#{bundle.status}" />
											</h:commandLink>
										</f:facet>
									</h:column>
								</h:dataTable></td>
							</tr>
							<tr>
								<td>
								<div class="divTable" style="height: 85px" id="historyQualTable"><h:dataTable
									id="dCloseQualityTable" var="currentRow" columnClasses="column"
									rowClasses="#{pc_CloseWorkQuality.historyRowStyle}"
									headerClass="header" styleClass="complexTable" cellpadding="2"
									cellspacing="0" width="100%" border="1"
									binding="#{pc_CloseWorkQuality.table}"
									value="#{pc_CloseWorkQuality.data}">
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectRow}"
											styleClass="textTable">
											<h:outputText style="width:100px;"
												value="#{currentRow.processor}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectRow}"
											styleClass="textTable">
											<h:outputText style="width:100px;"
												value="#{currentRow.businessArea}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectRow}"
											styleClass="textTable">
											<h:outputText style="width:100px;"
												value="#{currentRow.workType}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectRow}"
											styleClass="textTable">
											<h:outputText style="width:85px;"
												value="#{currentRow.status}"></h:outputText>
										</h:commandLink>
									</h:column>
								</h:dataTable></div>
								</td>
							</tr>
							<tr>
								<td><h:outputText styleClass="subTextTitle"
									value="#{bundle.bckOffice_history_detail_area}"></h:outputText></td>
							</tr>
							<tr>
								<td width="100%">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.businessArea}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedWorkItem.businessArea}"></h:outputText></td>
											<td class="textLabel"><h:outputText
												value="#{bundle.workType}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedWorkItem.workType}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText value="#{bundle.status}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedWorkItem.status}"></h:outputText></td>
											<td class="textLabel"><h:outputText value="#{bundle.queue}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedWorkItem.queueID}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.bckOffice_prority}"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedWorkItem.priority}"></h:outputText></td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
							<tr>
								<td><h:outputText styleClass="subTextTitle"
									value="#{bundle.close_work_comments}"></h:outputText></td>
							</tr>
							<tr>
								<td class="textInput"><h:inputTextarea id="comments" cols="65"
									rows="9" value="#{pc_CloseWorkQuality.comments}"
									styleClass="textInput"></h:inputTextarea></td>
							</tr>
						</tbody>
					</table>
					</td>
					<td width="5%" />
					<td width="50%" valign="top">
					<table border="0" width="100%">
						<tr>
							<td align="right"><Help:Link contextId="cma_backoffice_mgr_close" /></td>
						</tr>
						<tr>
							<td height="5px;"></td>
						</tr>
					</table>
					<table width="100%" align="center" cellpadding="0" cellspacing="0"
						border="0"  height="325px;">
						<tbody>
							<tr>
								<td><h:outputText styleClass="subTextTitle"
									value="#{bundle.bckOffice_qual_history}"></h:outputText></td>
							</tr>
							<tr>
							<td class="headerRow"><h:dataTable cellpadding="0"
									cellspacing="0" width="100%" styleClass="header"
									headerClass="headerTable" border="0">
									<h:column>
										<f:facet name="header">
											<h:commandLink id="type_asc" styleClass="header"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:68px;"
													value="#{bundle.address_type}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="userId_asc" styleClass="header"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:80px;"
													value="#{bundle.bckOffice_userId}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="error_asc" styleClass="header"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:100px;"
													value="#{bundle.errorCode}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="points_asc" styleClass="header"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:85px;"
													value="#{bundle.errorPoints}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="delFlag_asc" styleClass="header"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:65px;" value="Deleted" />
											</h:commandLink>
										</f:facet>
									</h:column>
								</h:dataTable></td>
							</tr>
							<tr valign="top">
								<td>
								<div class="divTable" style="height: 70px" id="reviewTable"><h:dataTable
									id="dReviewQualityTable" var="currentRow"
									columnClasses="column"
									rowClasses="#{pc_CloseWorkQuality.reviewRowStyle}"
									headerClass="header" styleClass="complexTable" cellpadding="2"
									cellspacing="0" width="100%" border="1"
									binding="#{pc_CloseWorkQuality.reviewTable}"
									value="#{pc_CloseWorkQuality.reviewData}">
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectReviewRow}"
											styleClass="textTable">
											<h:outputText style="width:70px;" value="#{currentRow.text}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectReviewRow}"
											styleClass="textTable">
											<h:outputText style="width:80px;"
												value="#{currentRow.userID}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectReviewRow}"
											styleClass="textTable">
											<h:outputText style="width:100px;"
												value="#{currentRow.errorCode}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectReviewRow}"
											styleClass="textTable">
											<h:outputText style="width:85px;"
												value="#{currentRow.errorPoints}"></h:outputText>
										</h:commandLink>
									</h:column>
									<h:column>
										<h:commandLink onmouseover="resetTargetFrame();"
											action="#{pc_CloseWorkQuality.selectReviewRow}"
											styleClass="textTable">
											<h:outputText style="width:50px;"
												value="#{currentRow.deleteFlag}"></h:outputText>
										</h:commandLink>
									</h:column>
								</h:dataTable></div>
								</td>
							</tr>
							<tr>
								<td>
								<table width="100%" align="center" cellpadding="0"
									cellspacing="0" border="0">
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.reviewOutCome}:"></h:outputText></td>
										<td class="textInput"><h:selectOneMenu id="SelectReason"
											styleClass="textInput"
											value="#{pc_CloseWorkQuality.selectedErrorValue}"
											disabled="#{pc_CloseWorkQuality.requireAddRowDisablement}"
											immediate="true"
											valueChangeListener="#{pc_CloseWorkQuality.errorCodeSelection}" onchange="submit();">
											<f:selectItems
												value="#{pc_CloseWorkQuality.selectedWorkItem.errorList}"></f:selectItems>
										</h:selectOneMenu></td>
										<td class="textLabel"><h:outputText
											value="#{bundle.impactFlag}:"></h:outputText></td>
										<td><h:selectBooleanCheckbox id="errorImpact"
											value="#{pc_CloseWorkQuality.errorImpactFlag}"
											disabled="#{pc_CloseWorkQuality.requireAddRowDisablement}" /></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.errorPoints}:"></h:outputText></td>
										<td class="text"><h:inputText id="errorPoints"
											value="#{pc_CloseWorkQuality.errorPoints}"
											disabled="#{pc_CloseWorkQuality.requireAddRowDisablement}"
											style="width=35px;">
										</h:inputText></td>
										<td class="textLabel"><h:outputText
											value="#{bundle.maxPoints}:"></h:outputText></td>
										<td class="textInput"><h:outputText
											value="#{pc_CloseWorkQuality.selectedWorkItem.maxQualityErrorPoints}"></h:outputText></td>
									</tr>
									<tr align="right">
										<td colspan="4"><h:commandButton id="add"
											disabled="#{pc_CloseWorkQuality.requireAddRowDisablement}"
											styleClass="buttontiny" value="#{bundle2.add}"
											onclick="document.forms[0].target=''"
											action="#{pc_CloseWorkQuality.addRow}" /> <h:commandButton
											id="del" styleClass="buttontiny" value="#{bundle2.del}"
											disabled="#{pc_CloseWorkQuality.requireDeleteButtonEnablement}"
											onclick="document.forms[0].target=''"
											action="#{pc_CloseWorkQuality.deleteRow}" /></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td><h:outputText styleClass="subTextTitle"
									value="#{bundle.reviewDetail}"></h:outputText></td>
							</tr>
							<tr>
								<td width="100%">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.bckOffice_userId}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.userID}"></h:outputText></td>
											<td class="textLabel"><h:outputText
												value="#{bundle.calldetails_name}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.name}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.reviewType}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.text}"></h:outputText></td>
											<td class="textLabel"><h:outputText
												value="#{bundle.errorCode}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.errorCode}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.errorPoints}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.errorPoints}"></h:outputText></td>
											<td class="textLabel"><h:outputText
												value="#{bundle.impactFlag}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.errorImpactFlag}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText value="#{bundle.day}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.dayCode}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.bckOffice_dateTime}:"></h:outputText></td>
											<td class="text" colspan="3"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.dateTime}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.deleteFlag}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.deleteFlag}"></h:outputText></td>
											<td class="textLabel"><h:outputText
												value="#{bundle.deletedBy}:"></h:outputText></td>
											<td class="text"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.deletion.userID}"></h:outputText></td>
										</tr>
										<tr>
											<td class="textLabel"><h:outputText
												value="#{bundle.deleteDateTime}:"></h:outputText></td>
											<td class="text" colspan="3"><h:outputText
												value="#{pc_CloseWorkQuality.selectedReviewItem.deletion.dateTime}"></h:outputText></td>
										</tr>
									</tbody>
								</table>
								</td>
								<h:outputText style="display:none;" id="selectedRow"
									value="#{pc_CloseWorkQuality.selectedRowIndex}"></h:outputText>
								<h:outputText style="display:none;" id="selectedReviewRow"
									value="#{pc_CloseWorkQuality.selectedReviewRowIndex}"></h:outputText>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</tbody>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
			<tr>
				<td align="right" width="100%">
				<h:commandButton id="ok"
					styleClass="button" value="#{bundle2.ok}"
					action="#{pc_CloseWorkQuality.submit}"
					onclick="document.forms[0].target='controlFrame'"></h:commandButton>
				<h:commandButton id="cancel" styleClass="buttonCancel"
					value="#{bundle2.cancel}" immediate="true"
					action="#{pc_CloseWorkQuality.cancelAndClear}"
					onclick="document.forms[0].target='controlFrame'"></h:commandButton>					
				</td>
			</tr>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
