<%@ page language="java" extends="com.csc.fs.accel.ui.BaseMaintainJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<base href="<%=basePath%>">
<title>Complete Work</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
	<!--
		var width=550;
		var height=380;

		function initCloseWork(){
			var currentComments = top.getDesktopComments();
			document.forms[0].item("closeWorkForm:comments").value = currentComments;
			document.forms[0].item('closeWorkForm:ok').focus();
		}
		
		function resetParam(){
			document.forms['closeWorkForm']['closeWorkForm:SuspendUntil'].value= "";		
			document.forms['closeWorkForm']['closeWorkForm:SuspendReason'].value= "";
			document.forms['closeWorkForm']['closeWorkForm:SuspendStatus'].value= "";			
		}
		
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['closeWorkForm'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['closeWorkForm'].target='';
			return false;
		}
		
	//-->
	</script>
<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
</head>

<body class="updatePanel" onload="popupInit();initCloseWork();">
<f:view>
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle var="bundle2" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<PopulateBean:Load serviceName="GET_CLOSE_WORK" value="#{pc_CloseWork}" />
	<h:form id="closeWorkForm">
		<h:outputText value="#{pc_CloseWork.refresh}" style="display:none;"></h:outputText>
		<table cellpadding="3" cellspacing="0" width="100%" border="0">
			<tbody>
				<tr style="height: 20px">
					<td align="right" colspan="2"><Help:Link
						contextId="cma_backoffice_close_work" /></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText
						value="#{bundle.close_work_comments}:" styleClass="textLabel"></h:outputText></td>
					<td class="textInput"><h:inputTextarea id="Comments" cols="55"
						rows="6" value="#{pc_CloseWork.comments}" styleClass="textInput"></h:inputTextarea></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText
						value="#{bundle.close_work_attitude}:" styleClass="textLabel"></h:outputText></td>
					<td class="textInput"><h:selectOneMenu id="Status"
						styleClass="textInput" value="#{pc_CloseWork.defaultExitStatus}">
						<f:selectItems value="#{pc_CloseWork.allowableExitStatuses}"></f:selectItems>
					</h:selectOneMenu></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText
						value="#{bundle.close_work_workFlow}:" styleClass="textLabel"></h:outputText></td>
					<td class="textInput">
					<table cellpadding="0" cellspacing="0" width="95%" border="0">
						<tr>
							<td width="30%"><h:selectBooleanCheckbox style="valign:top;"
								id="SuspendWork" onclick="resetTargetFrame();javascript:submit();"
								valueChangeListener="#{pc_CloseWork.suspendValueChange}"
								immediate="true" value="#{pc_CloseWork.booleanCheckBox}" /> <h:outputText
								value="#{bundle.close_work_suspend_work}" styleClass="textLabel" />
							</td>
							<td align="right" class="textLabel"><h:outputText
								value="#{bundle.close_work_suspend_until}:"
								styleClass="textLabel" /> <h:inputText id="SuspendUntil"
								disabled="#{pc_CloseWork.suspendFieldsDisabled}"
								styleClass="textInput" value="#{pc_CloseWork.suspendUntil}">
								<f:convertDateTime type="both"
									pattern="#{bundle2.dateTimePattern}" />
							</h:inputText></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText
						value="#{bundle.close_work_suspend_reason}:"
						styleClass="textLabel"></h:outputText></td>
					<td class="textInput"><h:selectOneMenu id="SuspendReason"
						disabled="#{pc_CloseWork.suspendFieldsDisabled}"
						styleClass="textInput" value="#{pc_CloseWork.suspendReason}">
						<f:selectItems value="#{pc_CloseWork.allowableSuspendReason}"></f:selectItems>
					</h:selectOneMenu></td>
				</tr>
				<tr>
					<td class="textLabel"><h:outputText
						value="#{bundle.close_work_suspend_status}:"
						styleClass="textLabel"></h:outputText></td>
					<td class="textInput"><h:selectOneMenu id="SuspendStatus"
						disabled="#{pc_CloseWork.suspendFieldsDisabled}"
						styleClass="textInput" value="#{pc_CloseWork.suspendStatus}">
						<f:selectItems value="#{pc_CloseWork.allowableSuspenseStatus}"></f:selectItems>
					</h:selectOneMenu></td>
				</tr>
				<tr>
					<td colspan="2">
					<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="headerRow">
							<h:dataTable styleClass="headerTable" headerClass="header" cellpadding="0" width="100%"
						cellspacing="0" columnClasses="column">
						<h:column id="lobfield_asc">
							<f:facet name="header">
								<h:outputText value="#{bundle.close_work_lob}"
									styleClass="header" style="width:160px;"></h:outputText>
							</f:facet>
						</h:column>
						<h:column id="value_asc_asc">
							<f:facet name="header">
								<h:outputText value="#{bundle.close_work_value}"
									styleClass="header" style="width:235px;"></h:outputText>
							</f:facet>
						</h:column>
					</h:dataTable>
							</td>
						</tr>
						<tr>
							<td>
					<div class="divTable" style="height: 76px" id="lobServices"><h:dataTable
						value="#{pc_CloseWork.lobList}" var="currentRecord"
						styleClass="complexTable" columnClasses="column"
						headerClass="header" rowClasses="row" cellpadding="2"
						cellspacing="0" width="100%" border="1">
						<h:column id="lobName">
							<h:outputText id="lob" value="#{currentRecord.displayName}"
								styleClass="textTable" style="width:160px;"></h:outputText>
						</h:column>
						<h:column id="lobValue">
							<h:inputText id="lobvalue"
								rendered="#{currentRecord.useStandard}"
								readonly="#{pc_CreateWork.enableSuspended}"
								styleClass="textInput" value="#{currentRecord.dataValue}"
								style="width:220px;">
							</h:inputText>
							<h:inputText id="lobvalueFormatted"
								rendered="#{currentRecord.useFormatted}"
								readonly="#{pc_CreateWork.enableSuspended}"
								styleClass="textInput" value="#{currentRecord.dataValue}"
								style="width:220px;">
								<f:convertNumber type="custom" pattern="#{currentRecord.dataFormat}"/>
							</h:inputText>
							<h:inputText id="lobvalueFormattedDate"
								rendered="#{currentRecord.useFormattedDate}"
								readonly="#{pc_CreateWork.enableSuspended}"
								styleClass="textInput" value="#{currentRecord.dataValue}"
								style="width:220px;">
								<f:convertDateTime type="both" pattern="#{bundle2.dateTimePattern}" />
							</h:inputText>
							<h:selectOneMenu id="lobValueList"
								rendered="#{currentRecord.useList}"
								readonly="#{pc_CreateWork.enableSuspended}"
								styleClass="textInput" value="#{currentRecord.dataValue}" style="width:220px">
								<f:selectItems value="#{currentRecord.avList}" />
							</h:selectOneMenu>							
						</h:column>
					</h:dataTable></div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="2"><h:commandButton id="ok"
						styleClass="button" value="#{bundle2.ok}"
						action="#{pc_CloseWork.submit}"
						onclick="setTargetFrame();"></h:commandButton>
					<h:commandButton id="cancel" styleClass="buttonCancel"
						value="#{bundle2.cancel}" immediate="true"
						action="#{pc_CloseWork.cancelAndClear}"
						onclick="setTargetFrame();"></h:commandButton>						
					</td>
				</tr>
			</tbody>
		</table>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
