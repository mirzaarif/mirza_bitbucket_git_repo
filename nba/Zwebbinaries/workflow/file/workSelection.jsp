<%@ page language="java" extends="com.csc.fs.accel.ui.BaseJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>" target='controlFrame'>
<title>Policy Selection</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript">
	<!--
	
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['selectWorkForm'].target='controlFrame';
			return false;
		}
	
		function launchProduct(){
			if (productChangedIndicator.innerHTML.indexOf("policyHasChanged") > -1){
				top.loadPolicyOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if (productChangedIndicator.innerHTML.indexOf("customerHasChanged") > -1){
				top.loadCustomerOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if (productChangedIndicator.innerHTML.indexOf("claimHasChanged") > -1){
				top.loadClaimOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if(productChangedIndicator.innerHTML.indexOf("newbusinessHasChanged") > -1){
				top.loadNewBusiness('<%=basePath%>');
				if (changeContext.innerHTML.indexOf("true") > -1) {  //NBA213
					if (top.currentOrientation != top.FULL_SCREEN) {  //FNB016
						top.showRight();  //NBA213
					}  //FNB016
				}  //NBA213
				top.launchWorkInProgress = true; //SPRNBA-947
				top.refreshLeftFile(); //SPRNBA-947 reload inbox
			} else if (productChangedIndicator.innerHTML.indexOf("policyWithProcessChanged") > -1){
				top.loadPolicyAndInvokeOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if (productChangedIndicator.innerHTML.indexOf("customerWithProcessChanged") > -1){
				top.loadCustomerAndInvokeOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if (productChangedIndicator.innerHTML.indexOf("claimWithProcessChanged") > -1){
				top.loadClaimAndInvokeOnRight('<%=basePath%>');
				top.refreshLeftFile();
			} else if(productChangedIndicator.innerHTML.indexOf("newbusinessWithProcessChanged") > -1){
				top.loadNewBusinessAndInvoke('<%=basePath%>');
				if (changeContext.innerHTML.indexOf("true") > -1) {  //NBA213
					if (top.currentOrientation != top.FULL_SCREEN) {  //FNB016
						top.showRight();  //NBA213
					}  //FNB016
				}  //NBA213
				top.launchWorkInProgress = true; //SPRNBA-947
				top.refreshLeftFile();
			} else if(productChangedIndicator.innerHTML.indexOf("indexWorkItem") > -1){
				try{
					document.getElementById('selectWorkForm:indexWorkItem').click();
				}catch(err){
				}
			} else if (productChangedIndicator.innerHTML.indexOf("invokeProcess") > -1){
				top.invoke('<%=basePath%>');
				top.refreshLeftFile();
			}
		}
		
		function resetRHS(){
			if (numLocked.innerHTML.indexOf("None") > -1){
			    top.loadRightFile("<%=basePath%>desktops/blank.html");
			}
		}

	//-->
	</script>
</head>
<body class="desktopBody" onload="setTargetFrame();launchProduct();resetRHS();"> <%-- NBA213 SPRNBA-947 remove filePageInit() --%>
<f:view>
	<table height="100%" width="100%">
		<tbody>
			<tr>	
				<td>
					<h:form id="selectWorkForm">
						<h:outputText value="#{pc_WorkflowFile.refresh}" style="display:none;"></h:outputText>
						<h:selectOneMenu
							id="selectPolicy" styleClass="desktopBody textInput" style="width:98%; border: 1px;border-color: gray;border-style: solid;"
							value="#{pc_WorkflowFile.currentLockedItemName}"
							valueChangeListener="#{pc_WorkflowFile.optionValueChanged}"
							onchange="top.showTimedWait('processing request',500);submit();">
							<f:selectItems value="#{pc_WorkflowFile.lockedWorkItems}" />
						</h:selectOneMenu>
						<div id="productChangedIndicator" style="display:none">
							<h:outputText value="#{pc_WorkflowFile.productChanged}"></h:outputText>
						</div>
						<div id="workSelectedIndicator" style="display:none">
							<h:outputText value="#{pc_WorkflowFile.currentLockedItemName}"></h:outputText>
						</div>						
						<div style="display:none">
							<h:commandButton id="indexWorkItem" action="#{pc_WorkflowFile.indexWorkItem}" onmousedown="setTargetFrame();"></h:commandButton>
						</div>
						<div id="numLocked" style="display:none">
							<h:outputText value="#{pc_WorkflowFile.numLockedItems}"></h:outputText>
						</div>
						<div id="changeContext" style="display:none">
							<h:outputText value="#{pc_WorkflowFile.changeContext}"></h:outputText>
						</div>
					</h:form>
				</td>
			</tr>
		</tbody>
	</table>
	<div id="Messages" style="display:none">	
		<h:messages />
	</div>
</f:view>
</body>
</html>
