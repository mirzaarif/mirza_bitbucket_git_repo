<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<head>
<base href="<%=basePath%>">
<title>History Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript">
	<!--
	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['historyForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['historyForm'].target='';
		return false;
	}
	function highlightRow(){
		top.scrollHighlight(document.getElementById('historydiv'), 
					document.getElementById('historyForm:selectedRow').innerHTML);
	}
	//-->
	</script>

</head>

<body class="whiteBody" onload="filePageInit();highlightRow();">
<f:view>
	<PopulateBean:Load serviceName="BACKOFFICE_HISTORY"
		value="#{pc_History}" />
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<h:form id="historyForm">
		<h:outputText value="#{pc_History.refresh}" style="display:none;"></h:outputText>
		<table width="100%" align="center" cellpadding="0" cellspacing="0"
			border="0">
			<tbody>
				<tr>
					<td align="right"><Help:Link contextId="BO_History"  /></td>
				</tr>
				<tr valign="bottom">
					<td width="100%" class="headerRow"><h:dataTable styleClass="header" cellpadding="0"
						width="100%" cellspacing="0" border="0" headerClass="headerTable">
						<h:column>
							<f:facet name="header">
								<h:commandLink id="processorCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_History.sortColumn}">
									<h:outputText style="width:100px;"
										value="#{bundle.bckOffice_processor}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="bussAreaCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_History.sortColumn}">
									<h:outputText style="width:100px;"
										value="#{bundle.businessArea}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="workTypeCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_History.sortColumn}">
									<h:outputText style="width:100px;" value="#{bundle.workType}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="statusCol_asc" styleClass="header"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_History.sortColumn}">
									<h:outputText style="width:100px;" value="#{bundle.status}" />
								</h:commandLink>
							</f:facet>
						</h:column>
					</h:dataTable></td>
				</tr>
				<tr>
					<td>
					<div class="divTable" style="height: 87px" id="historydiv"><h:dataTable
						id="dTableHistory" var="currentRow" columnClasses="column"
						rowClasses="#{pc_History.rowStyles}" headerClass="header"
						styleClass="complexTable" cellpadding="2" cellspacing="0"
						width="100%" border="1" binding="#{pc_History.table}"
						value="#{pc_History.data}">
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_History.selectRow}" styleClass="textTable">
								<h:outputText style="width:100px;"
									value="#{currentRow.processor}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_History.selectRow}" styleClass="textTable">
								<h:outputText style="width:100px;"
									value="#{currentRow.businessArea}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_History.selectRow}" styleClass="textTable">
								<h:outputText style="width:100px;"
									value="#{currentRow.workType}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_History.selectRow}" styleClass="textTable">
								<h:outputText style="width:85px;" value="#{currentRow.status}"></h:outputText>
							</h:commandLink>
						</h:column>
					</h:dataTable></div>
					</td>
				</tr>
				<tr>
					<td><h:outputText styleClass="subTextTitle"
						value="#{bundle.bckOffice_history_detail_area}"></h:outputText></td>
				</tr>
				<tr>
					<td width="100%">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.businessArea}:"></h:outputText></td>
								<td class="text"><h:outputText
									value="#{pc_History.selectedWorkItem.businessArea}"></h:outputText></td>
								<td class="textLabel"><h:outputText value="#{bundle.workType}:"></h:outputText></td>
								<td class="text"><h:outputText
									value="#{pc_History.selectedWorkItem.workType}"></h:outputText></td>
							</tr>
							<tr>
								<td class="textLabel"><h:outputText value="#{bundle.status}:"></h:outputText></td>
								<td class="text"><h:outputText
									value="#{pc_History.selectedWorkItem.status}"></h:outputText></td>
								<td class="textLabel"><h:outputText value="#{bundle.queue}:"></h:outputText></td>
								<td class="text"><h:outputText
									value="#{pc_History.selectedWorkItem.queueID}"></h:outputText></td>
							</tr>
							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.bckOffice_begin_date}:"></h:outputText></td>
								<td class="text" colspan="3"><h:outputText
									value="#{pc_History.selectedWorkItem.dateTime}"></h:outputText></td>
							</tr>
							<tr>
								<td class="textLabel"><h:outputText
									value="#{bundle.bckOffice_prority}"></h:outputText></td>
								<td class="text"><h:outputText
									value="#{pc_History.selectedWorkItem.priority}"></h:outputText></td>
							</tr>
						</tbody>
					</table>
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="subTextTitle" colspan="2"><h:outputText
								value="#{bundle.workItems_comments}" /></td>
						</tr>
					</table>

					<table width="100%" align="center" cellpadding="0" cellspacing="0"
						border="0">
						<tbody>
							<tr>
								<td></td>
							</tr>
							<tr valign="bottom">
								<td width="100%" class="headerRow"><h:dataTable styleClass="header"
									cellpadding="0" width="100%" cellspacing="0" border="0" headerClass="headerTable">
									<h:column>
										<f:facet name="header">
											<h:commandLink id="userID_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_History.sortCommentsColumn}">
												<h:outputText style="width:70px;"
													value="#{bundle.bckOffice_user}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink id="dateTime_asc" styleClass="header"
												onmouseover="resetTargetFrame();"
												actionListener="#{pc_History.sortCommentsColumn}">
												<h:outputText style="width:110px;"
													value="#{bundle.bckOffice_dateTime}" />
											</h:commandLink>
										</f:facet>
									</h:column>
									<h:column>
										<f:facet name="header">
											<h:commandLink styleClass="header" actionListener="#{pc_History.sortCommentsColumn}"
												id="text_asc"
												onmouseover="resetTargetFrame();">
												<h:outputText style="width:220px;"
													value="#{bundle.bckOffice_text}" />
											</h:commandLink>
										</f:facet>
									</h:column>
								</h:dataTable></td>
							</tr>
							<tr>
								<td>
								<div class="divTable" style="height: 80px"><h:dataTable
									var="comment" cellspacing="0" cellpadding="0"
									columnClasses="column" rowClasses="rowOdd,rowEven"
									headerClass="header" styleClass="complexTable" width="100%"
									border="1" value="#{pc_History.workItemCommentsList}">
									<h:column>
										<h:outputText style="width:70px;" value="#{comment.userID}"></h:outputText>
									</h:column>
									<h:column>
										<h:outputText style="width:110px;" value="#{comment.dateTime}"></h:outputText>
									</h:column>
									<h:column>
										<h:outputText style="width:205px;" value="#{comment.text}"></h:outputText>
									</h:column>
								</h:dataTable></div>
								<h:outputText style="display:none;" id="selectedRow"
									value="#{pc_History.selectedRowIndex}"></h:outputText></td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="stateIdentifier" />
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
