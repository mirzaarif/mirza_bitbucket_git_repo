<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- CHANGE LOG
Audit Number	Version		Change Description
NBA213			  7			Unified User Interface
NBA251            8			nbA Case Manager and Companion Case Assignment
NBA229            8			nbA Work List and Search Results Enhancement
SPRNBA-436		NB-1101		Page scrolling with screen resolution other than 1280 x 1024
SPRNBA-439		NB-1101		Receive this message in nbA - "Please wait initializing menu..."
NBA331			NB-1401		AWD REST
SPRNBA-949    	NB-1501		Correct Problems with Work Item Activation
SPRNBA-947		NB-1601		Null Pointer Exception May Occur in Inbox if User Selects Different Work
SPRNBA-976		NB-1601		Null session variables result from overlapping transactions initiated on the To-Do List user interface
SPRNBA-285		NB-1601		No Check Mark in Significant Column on Work List Tab --%>

<html>

<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                        + "/";
            }
%>

<head>
<base href="<%=basePath%>">
<title>Inbox</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" /> <%-- NBA229 --%>
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript">
	<!--
	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['inboxForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['inboxForm'].target='';
		return false;
	}
	function launchPolicy(){	
		if (document.getElementById('inboxForm:lockedSelectionChange').value == 'true') {  //SPRNBA-439
			if(parent != null && parent.parent != null && parent.parent.policySelection != null){
				top.showWait("Opening Work Item");	//SPRNBA-947
				var currPolicySel = parent.parent.policySelection.location.href;
				parent.parent.policySelection.location.href = currPolicySel; 
				//begin SPRNBA-976
				if (top.todoPopup && !top.todoPopup.closed) {
					top.todoPopup.top.showWait("The main nbA page is opening work");
				}
				//end SPRNBA-976
			}
			document.getElementById('inboxForm:lockedSelectionChange').value = 'false';  //SPRNBA-439
		//begin SPRNBA-947
		} else {
			if  (top.launchWorkInProgress) {
				//begin SPRNBA-976
				if (top.todoPopup && !top.todoPopup.closed) {
					top.todoPopup.top.showWait("The main nbA page is refreshing");
				}
				//end SPRNBA-976
				top.launchWorkInProgress = false;
			} else {
				top.hideWait();
				//begin SPRNBA-976
				if (top.todoPopup && !top.todoPopup.closed) {
					top.todoPopup.top.hideWait();
				}
				//end SPRNBA-976
			}
		//end SPRNBA-947
		}  //SPRNBA-439
	}

	function highlightRow(){
		top.scrollHighlight(document.getElementById('inboxdiv'), 
					document.getElementById('inboxForm:selectedRow').innerHTML);
	}	
	
	function clickUnlockFromCSDButon(buttonClicked) {
		document.getElementById('inboxForm:csdButtonClicked').value = buttonClicked;
		document.getElementById('inboxForm:unlockFromCSDButon').click();
	}
	//-->
</script>
</head>
<body class="whiteBody" onload="initMessagesAndScroll();launchPolicy();highlightRow();"> <%-- SPRNBA-947 remove filePageInit --%> 
<f:view>
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<f:loadBundle basename="properties.nbaApplicationData" var="property" /> <%-- NBA229 --%>
	<PopulateBean:Load serviceName="BACKOFFICE_INBOX" value="#{pc_Inbox}" />
	<h:form id="inboxForm">
		<h:outputText value="#{pc_Inbox.refresh}" style="display:none;"></h:outputText>
		<h:outputText value="#{pc_Inbox.currentIDs}" style="display:none;"></h:outputText>
		<h:outputText value="#{pc_Inbox.auth.visibility['getWorkItem']}"
			style="display:none;"></h:outputText>
		<table width="100%" align="center" cellpadding="0" cellspacing="0"
			border="0">
			<tbody>
				<TR>
					<td colspan="2">
					<TABLE width="100%" border="1" class="textMainTitleBar">
						<tr style="border: none">
							<td style="border: none">
								<h:outputText value="#{bundle.bckOffice_workItems}"
									rendered="#{pc_Inbox.auth.visibility['backOfficeLabel']}"></h:outputText>
								<h:outputText value="#{bundle.bckOffice_workflow}"
									rendered="#{pc_Inbox.auth.visibility['callCenterLabel']}"></h:outputText></td>
							<td align="right" style="border: none">
								<h:outputText value="#{bundle.bckOffice_favorites}" styleClass="textTitleDetail" style="vertical-align: middle" 
									rendered="#{pc_Inbox.auth.visibility['searchFavorites']}"/>								
								<h:selectOneMenu value="#{pc_Inbox.favoriteSelected}" style="width: 200px; margin-top: -20px"
									rendered="#{pc_Inbox.auth.visibility['searchFavorites']}">
									<f:selectItems value="#{pc_Inbox.favoritesList}" />
								</h:selectOneMenu>
								<h:commandButton type="submit" value="#{bundle.bckOffice_favorites_search}"
									action="#{pc_Inbox.searchFavorite}" onclick="setTargetFrame();"
									styleClass="button" style="width: 30px;font-weight: normal;"
									rendered="#{pc_Inbox.auth.visibility['searchFavorites']}"
									disabled="#{pc_Inbox.auth.enablement['searchFavorites']}" /> <%-- NBA229 --%>
								<h:commandButton type="submit" value="#{bundle.bckOffice_get_work}"
									styleClass="button" action="#{pc_Inbox.retrieveWorkItem}"
									onclick="resetTargetFrame();" style="font-weight: normal;"
									rendered="#{pc_Inbox.auth.visibility['getWorkItem']}"
									disabled="#{pc_Inbox.auth.enablement['getWorkItem']}"></h:commandButton> <%-- NBA229 --%>
								<h:commandButton type="submit" value="#{bundle.bckOffice_search}"
									styleClass="button" action="#{pc_Inbox.searchWorkItem}"
									onclick="setTargetFrame();" style="font-weight: normal;"
									rendered="#{pc_Inbox.auth.visibility['Search']}"
									disabled="#{pc_Inbox.auth.enablement['Search']}"></h:commandButton>   <%-- NBA229 --%>
							<Help:Link contextId="BO_Inbox" imageName="images/help_white.gif" />
							</td>
						</tr>
					</TABLE>
					</td>
				</TR>
				<tr valign="bottom">
					<td width="100%" class="headerRow"><h:dataTable styleClass="header"
						cellpadding="0" width="630px" cellspacing="0" border="0"
						headerClass="headerTable"><%-- SPRNBA-436 --%>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="lockCol" styleClass="ovColSorted#{pc_Inbox.sortedByCol1}"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> <%-- NBA229 --%>
									<h:outputText style="width:40px;"
										value="#{bundle.backOffice_Lock}" /> 
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="qEntryDate" styleClass="ovColSorted#{pc_Inbox.sortedByCol2}"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> <%-- NBA229 --%>
									<h:outputText style="width:90px;" styleClass="ovColSorted#{pc_Inbox.sortedByCol2}"
										value="#{property.qEntryDate}" /> <%-- NBA229 SPRNBA-436 --%>	
								</h:commandLink>
							</f:facet>
						</h:column>
						<%--  NBA251 Code Deleted --%>
						<h:column>
							<f:facet name="header">
								<h:commandButton id="siqReqCol1" styleClass="ovColSorted#{pc_Inbox.sortedByCol3}"
									onclick="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"
									image="./images/workflow/significantReq.gif"
									style="margin-right: 2px" /> <%-- NBA229 --%>
							</f:facet>
						</h:column>
						<%-- Begin NBA229 --%>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="priority" styleClass="ovColSorted#{pc_Inbox.sortedByCol4}"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> 
									<h:outputText style="width:40px;" styleClass="ovColSorted#{pc_Inbox.sortedByCol4}"
										value="#{property.priority}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="reasonWork" styleClass="ovColSorted#{pc_Inbox.sortedByCol5"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}">
									<h:outputText style="width:120px;" styleClass="ovColSorted#{pc_Inbox.sortedByCol5}" value="#{property.reasonWork}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<%-- end NBA229 --%>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="contractCol" styleClass="ovColSorted#{pc_Inbox.sortedByCol6"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> <%-- NBA229 --%>
									<h:outputText style="width:120px;" styleClass="ovColSorted#{pc_Inbox.sortedByCol6}" value="#{bundle.workItems_contractNumber}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="nameCol" styleClass="ovColSorted#{pc_Inbox.sortedByCol7"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> <%-- NBA229 --%>
									<h:outputText style="width:95px;" styleClass="ovColSorted#{pc_Inbox.sortedByCol7}" value="#{bundle.workItems_name}" />
								</h:commandLink>
							</f:facet>
						</h:column>
						<h:column>
							<f:facet name="header">
								<h:commandLink id="agency" styleClass="ovColSorted#{pc_Inbox.sortedByCol8"
									onmouseover="resetTargetFrame();"
									actionListener="#{pc_Inbox.sortColumn}"> <%-- NBA229 --%>
									<h:outputText style="width:55px;"  styleClass="ovColSorted#{pc_Inbox.sortedByCol8}" value="#{property.agency}" /> <%-- NBA229 --%>
								</h:commandLink>
							</f:facet>
						</h:column>
						<%--  NBA229 Code Deleted--%>
					</h:dataTable></td>
				</tr>
				<tr>
					<td>
					<div class="divTable" style="height: 400px" id="inboxdiv"><h:dataTable
						id="dTableInbox" var="currentRow" columnClasses="column"
						rowClasses="#{pc_Inbox.inboxRowStyles}" headerClass="header"
						styleClass="complexTable" cellpadding="0" cellspacing="0"
						width="630px" border="1" binding="#{pc_Inbox.table}"
						value="#{pc_Inbox.hierarichalData}"><%-- SPRNBA-436 --%>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								action="#{pc_Inbox.selectForMultipleRows}"
								style="width: 40px;text-align:center;" styleClass="textTable">
								<h:graphicImage url="#{currentRow.node.imagePath}"
									style="border:0px; vertical-align: top" title="#{currentRow.node.lockStatus}" />
								<h:graphicImage url="images/pause.gif" style="border:0px; vertical-align: top"
									rendered="#{currentRow.type == 'WorkItem' && currentRow.node.suspendFlag == 'Y'}" />
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" action="#{pc_Inbox.selectForMultipleRows}"
								rendered="#{currentRow.node.recordType == 'C'}"
								styleClass="textTable" style="width: 20px;text-align:center">
								<h:graphicImage url="images/workflow/case.gif"
									style="border:none; text-align:center; vertical-align: top"
									rendered="#{currentRow.node.recordType == 'C'}"></h:graphicImage>
							</h:commandLink>
							<h:commandLink onmouseover="setTargetFrame();" 
								rendered="#{currentRow.node.recordType == 'O'}"
								styleClass="textTable" style="width: 20px;text-align:center"
								title="View Source" action="#{pc_Inbox.showSelectedImage}">
								<h:graphicImage url="images/workflow/source.gif"
									style="border:none; text-align:center; vertical-align: top"
									rendered="#{currentRow.node.recordType == 'O'}"></h:graphicImage>
							</h:commandLink>
							<h:commandLink onmouseover="resetTargetFrame();" action="#{pc_Inbox.selectForMultipleRows}"
								rendered="#{currentRow.node.recordType == 'T'}"
								styleClass="textTable" style="width: 20px;text-align:center">
								<h:graphicImage url="images/workflow/transaction.gif"
									style="border:none; text-align:center; vertical-align: top"
									rendered="#{currentRow.node.recordType == 'T'}"></h:graphicImage>
							</h:commandLink>
						</h:column>
						
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();" action="#{pc_Inbox.selectForMultipleRows}"
								style="width:85px;text-align:left;" styleClass="textTable"> <%-- SPRNBA-436 SPRNBA-285 --%>
								<h:graphicImage url="images/rqa/join.gif" style="border:none; vertical-align: top; padding-right: 2px;"
									rendered="#{currentRow.level == 1}"></h:graphicImage>
								<h:graphicImage url="images/rqa/join.gif" style="border:none; margin-left: 5px"
									rendered="#{currentRow.level == 2}"></h:graphicImage>
								<h:graphicImage url="images/rqa/join.gif" style="border:none; margin-left: 10px"
									rendered="#{currentRow.level == 3 }"></h:graphicImage>
								<h:outputText style="text-align: left; vertical-align: top;"
									value="#{currentRow.queueEntryDate}"> <%-- NBA229 --%>
								</h:outputText>
							</h:commandLink>
						</h:column>
						<%-- NBA229 Code Deleted --%>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" style="width:23px"
								action="#{pc_Inbox.selectForMultipleRows}"> <%-- SPRNBA-285 --%>
								<h:selectBooleanCheckbox value="#{currentRow.node.significantReq}" style="vertical-align: top"
									rendered="#{currentRow.type == 'WorkItem' && currentRow.node.significantReq}" />
							</h:commandLink>
						</h:column>
						<%--Being NBA229 --%>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" style="width:53px"
								action="#{pc_Inbox.selectForMultipleRows}"> <%-- SPRNBA-285 --%>
								<h:outputText style="text-align: left; vertical-align: top"
									value="#{currentRow.node.priority}"
									rendered="#{currentRow.type == 'WorkItem'}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" style="width:120px"
								action="#{pc_Inbox.selectForMultipleRows}">
								<h:outputText style="text-align: left; vertical-align: top"
									value="#{currentRow.routeReason}">	<%-- NBA331 --%>
								</h:outputText>	<%-- NBA331 --%>
							</h:commandLink>
						</h:column>
						<%-- End NBA229 --%>					 																
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" style="width:120px"
								action="#{pc_Inbox.selectForMultipleRows}">
								<h:outputText style="text-align: left; vertical-align: top;"
									value="#{currentRow.node.policyNumber}"
									rendered="#{currentRow.type == 'WorkItem'}"></h:outputText>
							</h:commandLink>
						</h:column>
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								styleClass="textTable" style="width:95px"
								action="#{pc_Inbox.selectForMultipleRows}">
								<h:outputText style="text-align: left; vertical-align: top"
									value="#{currentRow.node.name}"
									rendered="#{currentRow.type == 'WorkItem'}"></h:outputText>
								<h:outputText style="text-align: left; vertical-align: top"
									value="#{currentRow.node.displayName}"
									rendered="#{currentRow.type == 'WorkItemSource'}"></h:outputText>
							</h:commandLink>
						</h:column>	
						<%-- NBA229 Code Deleted --%>				
						<h:column>
							<h:commandLink onmouseover="resetTargetFrame();"
								rendered="#{currentRow.type == 'WorkItem'}"
								styleClass="textTable" style="width:55px;"
								action="#{pc_Inbox.selectForMultipleRows}">
								<h:outputText style="text-align: left; vertical-align: top"
									value="#{currentRow.writingAgency}"
									rendered="#{currentRow.type == 'WorkItem'}"></h:outputText> <%-- NBA229 --%>
							</h:commandLink>
							<h:commandLink onmouseover="resetTargetFrame();"
								rendered="#{currentRow.type == 'WorkItemSource'}"
								styleClass="textTable" style="width:55px;">
							</h:commandLink>

						</h:column>
					</h:dataTable></div>

					</td>
				</tr>
				<tr>
					<td align="left">
						<h:commandButton type="submit" id="unlockButon"
							value="#{bundle.bckOffice_unlock}" styleClass="buttonTiny"
							action="#{pc_Inbox.unlockWorkItem}" onclick="setTargetFrame();" 
							rendered="#{pc_Inbox.auth.visibility['unlockWorkItem']}" 
							disabled="#{pc_Inbox.unlockDisabled || pc_Inbox.auth.enablement['unlockWorkItem']}" /> 
						<h:commandButton type="submit"
							value="#{bundle.bckOffice_unsuspend}"
							onclick="resetTargetFrame();" styleClass="buttonTiny" style="width: 80px"
							action="#{pc_Inbox.unsuspendWorkItem}"
							rendered="#{pc_Inbox.auth.visibility['unsuspendWorkItem']}"
							disabled="#{pc_Inbox.selectedWorkItem == null || !pc_Inbox.suspendDisabled || pc_Inbox.auth.enablement['unsuspendWorkItem']}" /> 
					</td>
				</tr>
				<tr>
					<td align="right">
						<h:commandButton type="submit" value="#{bundle.bckOffice_createSource}"
							action="#{pc_Inbox.createSource}" onclick="setTargetFrame();"
							styleClass="buttonTiny" style="width: 100px"
							rendered="#{pc_Inbox.auth.visibility['createSourceItem']}"
							disabled="#{pc_Inbox.selectedWorkItem == null ||
										pc_Inbox.unlockDisabled || pc_Inbox.auth.enablement['createSourceItem']}"/>
						<h:commandButton type="submit" value="#{bundle.bckOffice_changeScanType}"
							action="#{pc_Inbox.changeScanType}" onclick="setTargetFrame();"
							styleClass="buttonTiny" style="width: 130px"
							rendered="#{pc_Inbox.auth.visibility['ChangeScanData']}"
							disabled="#{pc_Inbox.selectedWorkItem == null ||
										pc_Inbox.unlockDisabled ||
										pc_Inbox.auth.enablement['ChangeScanData']}" /> 
						<%--  Begin NBA251 --%>				
						<h:commandButton type="submit" value="#{bundle.bckOffice_assignQueues}"
							action="#{pc_Inbox.assignQueues}" onclick="setTargetFrame();"
							styleClass="buttonTiny" style="width: 150px"
							rendered="#{pc_Inbox.auth.visibility['QueuesReassignment']}"
							disabled="#{pc_Inbox.selectedWorkItem == null ||
							  pc_Inbox.selectedWorkItemTransaction ||
										pc_Inbox.unlockDisabled ||
							  pc_Inbox.auth.enablement['QueuesReassignment']}" />
						<%--  End NBA251 --%>
						<h:commandButton type="submit" value="#{bundle.bckOffice_viewWorkItemData}"
							action="#{pc_Inbox.viewWorkDetails}" onclick="setTargetFrame();"
							styleClass="buttonTiny" style="width: 80px"
							rendered="#{pc_Inbox.auth.visibility['viewData']}"
							disabled="#{pc_Inbox.selectedWorkItem == null || pc_Inbox.auth.enablement['viewWorkItemData']}" />
						<h:commandButton type="submit" value="#{bundle.bckOffice_open}"
							action="#{pc_Inbox.processWorkItem}" onclick="resetTargetFrame();"
							styleClass="buttonTiny"
							rendered="#{pc_Inbox.auth.visibility['processWorkItem']}"
							disabled="#{pc_Inbox.processDisabled || pc_Inbox.auth.enablement['processWorkItem']}" />
  					</td>
				</tr>
				<tr>
					<td align="right">
						<h:commandButton type="submit" value="#{bundle.bckOffice_suspend}"
							action="#{pc_Inbox.suspendWorkItem}" onclick="setTargetFrame();"
							styleClass="buttonTiny"
							rendered="#{pc_Inbox.auth.visibility['SuspendCase']}"
							disabled="#{(pc_Inbox.selectedWorkItem == null && pc_Inbox.selectedWorkItemSource == null) ||
										pc_Inbox.suspendDisabled ||
										pc_Inbox.unlockDisabled ||
										pc_Inbox.auth.enablement['SuspendCase']}" />
						<h:commandButton type="submit" value="#{bundle.bckOffice_complete}"
							onclick="setTargetFrame();" styleClass="buttonTiny"
							action="#{pc_Inbox.completeWorkItem}"
							rendered="#{pc_Inbox.auth.visibility['completeWorkItem']}"
							disabled="#{pc_Inbox.completeDisabled || pc_Inbox.auth.enablement['completeWorkItem']}" />
						<h:commandButton type="submit" value="#{bundle.bckOffice_activate}"
							action="#{pc_Inbox.activateWorkItem}" onclick="resetTargetFrame();"
							styleClass="buttonTiny" 
							rendered="#{pc_Inbox.auth.visibility['activate']}"
							disabled="#{pc_Inbox.selectedWorkItem == null || 
										pc_Inbox.activateDisabled ||
										pc_Inbox.auth.enablement['activate']}" /> 	<%-- SPRNBA-949 --%>
						<h:commandButton type="submit" value="#{bundle.bckOffice_fax}"
							action="#{pc_Inbox.retrieveFax}" onclick="setTargetFrame();"
							styleClass="buttonTiny" 
							rendered="#{pc_Inbox.auth.visibility['fax']}"
							disabled="#{pc_Inbox.selectedWorkItemSource == null || pc_Inbox.auth.enablement['fax']}" />
						<h:commandButton type="submit" value="#{bundle.bckOffice_route}"
							action="#{pc_Inbox.route}" onclick="setTargetFrame();"
							styleClass="buttonTiny"
							rendered="#{pc_Inbox.auth.visibility['RouteCase']}"
							disabled="#{pc_Inbox.selectedWorkItem == null ||
										pc_Inbox.unlockDisabled ||
										pc_Inbox.auth.enablement['RouteCase']}" /> 
						<h:commandButton type="submit" value="#{bundle.bckOffice_viewImage}"
							action="#{pc_Inbox.viewImages}" onclick="setTargetFrame();top.hideWait();"
							styleClass="buttonTiny" style="width: 87px"
							rendered="#{pc_Inbox.auth.visibility['ImageViewer']}"
							disabled="#{pc_Inbox.selectedWorkItemSource == null || 
										pc_Inbox.auth.enablement['ImageViewer']}" /> <%-- NBA331.1 --%>
						<h:commandButton type="submit" value="#{bundle.bckOffice_viewAllImages}"
							action="#{pc_Inbox.viewAllImages}" onclick="setTargetFrame();top.hideWait();"
							styleClass="buttonTiny" style="width: 100px"
							rendered="#{pc_Inbox.auth.visibility['ImageViewerAll']}"
							disabled="#{pc_Inbox.selectedWorkItem == null || 
										pc_Inbox.auth.enablement['ImageViewerAll']}" />  <%-- NBA331.1 --%>
  					</td>
				</tr>
				<tr>
					<td width="100%">
					<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-top: 20px">
						<tbody>
							<tr>
								<td valign="top">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td colspan="4">
												<h:outputText value="#{bundle.bckOffice_work_item_details}"
												styleClass="subTextTitle"
													rendered="#{!pc_Inbox.showSourceOrItem}" />
												<h:outputText value="#{bundle.bckOffice_source_item_details}"
													styleClass="subTextTitle"
												rendered="#{pc_Inbox.showSourceOrItem}" /></td>
										</tr>
										<tr>
											<td class="textLabel" width="18%">
												<h:outputText value="#{bundle.work_govtID}:"/></td>
											<td class="text" width="18%">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.governmentID}"
													rendered="true" >
														<f:convertNumber type="ssn" integerOnly="true" pattern="#{bundle.ssnPattern}"/>
													</h:outputText>
												<h:outputText value="#{pc_Inbox.selectedWorkItem.governmentID}"
													rendered="#{pc_Inbox.selectedWorkItem.governmentIDType == '3'}" >
														<f:convertNumber type="ssn" integerOnly="true" pattern="#{bundle.sinPattern}"/>
													</h:outputText></td>
											<td class="textLabel" width="20%">
												<h:outputText value="#{bundle.work_faceAmt}:"/></td>
											<td class="text" width="15%">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.faceAmount}"
													rendered="true" >
														<f:convertNumber currencySymbol="#{bundle.currencySymbol}"
																		 type="currency"
																		 maxFractionDigits="2" />
													</h:outputText>
												<h:outputText value="#{pc_Inbox.selectedWorkItem.workType}"
													rendered="false" /></td>
											<td class="textLabel" width="15%">
												<h:outputText value="#{bundle.work_producerID}:"/></td>
											<td class="text">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.producerID}" /></td>
										</tr>
										<tr>
											<td class="textLabel">
												<h:outputText value="#{bundle.work_suspendDate}:" /></td>
											<td class="text">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.suspendDate}" >
														<f:convertDateTime pattern="#{bundle.datePattern}" />
													</h:outputText></td>
											<td class="textLabel">
												<h:outputText value="#{bundle.work_suspendOriginator}:" /></td>
											<td class="text">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.suspendOriginator}" /></td>
											<td class="textLabel">
												<h:outputText value="#{bundle.bckOffice_prority}" /></td>
											<td class="text">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.priority}" /></td>
										</tr>

										<tr>
											<td class="textLabel">
												<h:outputText value="#{bundle.work_activation}:" /></td>
											<td class="text">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.activationDate}" >
														<f:convertDateTime pattern="#{bundle.datePattern}" />
													</h:outputText></td>
											<td class="textLabel">
												<h:outputText value="#{bundle.work_actStatus}:" /></td>
											<td class="text" colspan="3">
												<h:outputText value="#{pc_Inbox.selectedWorkItem.activationStatus}" /></td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</tbody>
		</table>
		<h:commandButton type="submit" id="unlockFromCSDButon"
			value="" style="display:none"
			action="#{pc_Inbox.unlockItemFromLaunchCSD}" onclick="setTargetFrame();"/>		
		<h:inputHidden id="csdButtonClicked" value="#{pc_Inbox.csdButtonClicked}"></h:inputHidden>
		<h:outputText style="display:none;" id="selectedRow"
			value="#{pc_Inbox.selectedRowIndex}"></h:outputText>
		<h:inputHidden id="lockStatus" value="#{pc_Inbox.hasAnyLockedItem}" />
		<h:inputHidden id="lockedSelectionChange" value="#{pc_Inbox.lockedSelectionChanged}" />  <%-- SPRNBA-439 --%>
	</h:form>
	<div id="Messages" style="display: none"><h:messages /></div>
</f:view>
</body>
</html>
