<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<%@ page language="java" extends="com.csc.fs.accel.ui.BaseJSPServlet"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/components.tld" prefix="d" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>

<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>

<head>
<base href="<%=basePath%>">
<title>Relationships Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript">
	<!--
	function setTargetFrame() {
		//alert('Setting Target Frame');
		document.forms['relationshipsForm'].target='controlFrame';
		return false;
	}
	function resetTargetFrame() {
		//alert('Resetting Target Frame');
		document.forms['relationshipsForm'].target='';
		return false;
	}
	function setValue(arg){		
		document.forms['relationshipsForm']['relationshipsForm:selectedIdentifier'].value = arg;
		document.all["relationshipsForm:hiddenButton"].click()		
	}
	function showAndHide(ele) 
	{ 	   
		if (document.forms['relationshipsForm']['relationshipsForm:sourceOrWorkItemToDetailsDisplay'].value == "source"){
			return;		   					
		}	  
	  var div2; 
	  var eleName;
	  if(ele == "workItemDiv"){
	   	div2= "historyDiv";
	   	eleName = "workItemDetails";
	   }else{
	   	div2 = "workItemDiv";
	   	eleName = "historyDetails";	   	
	   }
	   if (document.getElementById(ele).style.display == "none"){	   		
	   		document.getElementById(ele).style.display = "block";
	   		document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value = ele;	   		
	   		document.getElementById(div2).style.display = "none";   		
	   }else{
	   		document.getElementById(ele).style.display = "none";
	   		document.getElementById(div2).style.display = "block";
	   		document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value = div2;			   		  
	   }
		top.hideWait();
		var compName = "relationshipsForm:"+eleName+""
		document.forms['relationshipsForm'][compName].isClicked = false;
		return false;
	}
	
	function recreateDisplay(){ 		
		if(document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value.length != 0){		
			if(document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value == "workItemDiv"){
				showAndHide("workItemDiv");
				showAndHide("workItemDiv");				
			}else{
				showAndHide("historyDiv");	
			}
		}else{
			document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value = "workItemDiv";
		}
		if (document.forms['relationshipsForm']['relationshipsForm:sourceOrWorkItemToDetailsDisplay'].value == "source"){
	   		document.getElementById("historyHeaderDiv").style.display = "none";			
	   		document.getElementById("historyDiv").style.display = "none";
	   		document.getElementById("workItemDiv").style.display = "none";
	   		document.getElementById("sourceDiv").style.display = "block";
	   		document.forms['relationshipsForm']['relationshipsForm:accordianStateIdentifier'].value = "workItemDiv";			   		  	   					
		}else{
			document.getElementById("sourceDiv").style.display = "none";
		}
	}
	
	function highlightRow(){
		top.scrollHighlight(document.getElementById('historyReldiv'), 
					document.getElementById('relationshipsForm:selectedRow').innerHTML);
	}			
	//-->
	</script>

</head>

<body class="whiteBody"
	onload="filePageInit();recreateDisplay();highlightRow();">
<f:view>
	<PopulateBean:Load serviceName="BACKOFFICE_RELATIONSHIPS"
		value="#{pc_Relationships}" />
	<f:loadBundle var="bundle"
		basename="com.csc.fs.accel.ui.config.workflow.ApplicationData" />
	<h:form id="relationshipsForm">
		<h:outputText value="#{pc_Relationships.refresh}" style="display:none;"></h:outputText>
		<table width="100%" align="center" cellpadding="0" cellspacing="0"
			border="0">
			<tbody>
				<tr>
					<td align="right"><Help:Link contextId="cma_related_work"/></td>
				</tr>
				<tr>
					<td width="100%">
					<div class="divTable textTable" style="height: 110px" id="treeDiv">
						<d:graph_menutree
							id="menu4" value="#{pc_Relationships.treeData}"
							selectedClass="highlight" unselectedClass="row"
							actionListener="#{pc_Relationships.processGraphEvent}" />
					</div>
					</td>
				</tr>
			</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td style="height: 5px"></td>
			</tr>
			<tr>
				<td><h:commandButton id="workItemDetails" type="submit"
					style="width=100%" value="#{bundle.bckOffice_work_item_details}"
					styleClass="accordian" onclick="return showAndHide('workItemDiv')"></h:commandButton>
				</td>
			</tr>
			<tr>
				<td>
				<div id="workItemDiv">
				<table width="100%" align="center" cellpadding="0" cellspacing="0"
					border="0">
					<tbody>
						<tr>
							<td valign="top">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td class="textLabel" width="25%"><h:outputText
											value="#{bundle.businessArea}:" /></td>
										<td class="text" width="43%"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.businessArea}"></h:outputText></td>
										<td class="textLabel" width="16%"><h:outputText
											value="#{bundle.workType}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.workType}"></h:outputText></td>

									</tr>
									<tr>
										<td class="textLabel"><h:outputText value="#{bundle.status}:" />
										</td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.status}"></h:outputText></td>
										<td class="textLabel"><h:outputText value="#{bundle.queue}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.queueID}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_prority}"></h:outputText></td>
										<td class="text" colspan="3"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.priority}"></h:outputText></td>

									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_lock_status}"></h:outputText></td>
										<td class="text" colspan="3"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.lockStatus}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_create_date_time}"></h:outputText></td>
										<td class="text" colspan="3"><h:outputText
											value="#{pc_Relationships.selectedWorkItemRow.createDateTime}"></h:outputText>
										</td>
									</tr>
								</tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td width="100%">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td colspan="2"><h:outputText styleClass="subTextTitle"
											value="#{bundle.bckOffice_lobHeader}"></h:outputText></td>
									</tr>
									<tr>
										<td colspan="2">
										<div class="divTable" style="height: 85px" style="width:80%"><h:dataTable
											var="lobData" border="1" id="relationshipLobTable"
											value="#{pc_Relationships.selectedWorkItemRow.lobData}"
											cellspacing="0" cellpadding="0" columnClasses="column"
											rowClasses="rowOdd,rowEven" headerClass="header"
											styleClass="complexTable" width="100%">
											<h:column>
												<h:outputText style="width:100px;"
													value="#{lobData.displayName}"></h:outputText>
											</h:column>
											<h:column>
												<h:outputText styleClass="text"
													style="vertical-align:text-bottom; width:100px; overflow: hidden;"
													value="#{lobData.dataValue}"></h:outputText>
											</h:column>
										</h:dataTable></div>
										</td>
									</tr>
								<tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td colspan="4" height="15" />
						</tr>
					</tbody>
				</table>
				</div>
				<div id="sourceDiv">
				<table width="100%" align="center" cellpadding="0" cellspacing="0"
					border="0">
					<tbody>
						<tr>
							<td valign="top">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td class="textLabel" width="35%"><h:outputText
											value="#{bundle.businessArea}:" /></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedSourceRow.businessArea}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_sourceType}:"></h:outputText></td>
										<td class="text" colspan="3"><h:outputText
											value="#{pc_Relationships.selectedSourceRow.sourceType}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_fileName}:" /></td>
										<td class="text" colspan="3"><h:outputText
											value="#{pc_Relationships.selectedSourceRow.document}"></h:outputText></td>
									</tr>
								</tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td width="100%">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td colspan="2"><h:outputText styleClass="subTextTitle"
											value="#{bundle.bckOffice_lobHeader}"></h:outputText></td>
									</tr>
									<tr>
										<td colspan="2">
										<div class="divTable" style="height: 85px" style="width:80%"><h:dataTable
											var="sourceLobData" border="1" id="sourceLobTable"
											value="#{pc_Relationships.selectedSourceRow.lobList}"
											cellspacing="0" cellpadding="0" columnClasses="column"
											rowClasses="rowOdd,rowEven" headerClass="header"
											styleClass="complexTable" width="100%">
											<h:column>
												<h:outputText style="width:100px;"
													value="#{sourceLobData.display}"></h:outputText>
											</h:column>
											<h:column>
												<h:outputText styleClass="text"
													style="vertical-align:text-bottom;"
													value="#{sourceLobData.dataValue}"></h:outputText>
											</h:column>
										</h:dataTable></div>
										</td>
									</tr>
								<tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td colspan="4" height="15" />
						</tr>
					</tbody>
				</table>
				</div>
				<div id="historyHeaderDiv">
				<table width="100%" align="center" cellpadding="0" cellspacing="0"
					border="0">
					<tbody>
						<tr>
							<td colspan="4"><h:commandButton id="historyDetails"
								type="submit" style="width=100%"
								value="#{bundle.bckOffice_history}" styleClass="accordian"
								onclick="return showAndHide('historyDiv')"></h:commandButton></td>
						</tr>
					</tbody>
				</table>
				</div>
				<div id="historyDiv" STYLE="display: none">
				<table width="100%" align="center" cellpadding="0" cellspacing="0"
					border="0">
					<tbody>
						<tr>
							<td height="5" />
						</tr>
						<tr valign="bottom">
							<td width="100%" class="headerRow"><h:dataTable styleClass="header" cellpadding="0"
								width="100%" cellspacing="0" border="0" headerClass="headerTable">
								<h:column>
									<f:facet name="header">
										<h:commandLink id="processorCol_asc" styleClass="header"
											onmouseover="resetTargetFrame();"
											actionListener="#{pc_Relationships.sortColumn}">
											<h:outputText style="width:95px;"
												value="#{bundle.bckOffice_processor}" />
										</h:commandLink>
									</f:facet>
								</h:column>
								<h:column>
									<f:facet name="header">
										<h:commandLink id="bussAreaCol_asc" styleClass="header"
											onmouseover="resetTargetFrame();"
											actionListener="#{pc_Relationships.sortColumn}">
											<h:outputText style="width:95px;"
												value="#{bundle.businessArea}" />
										</h:commandLink>
									</f:facet>
								</h:column>
								<h:column>
									<f:facet name="header">
										<h:commandLink id="workTypeCol_asc" styleClass="header"
											onmouseover="resetTargetFrame();"
											actionListener="#{pc_Relationships.sortColumn}">
											<h:outputText style="width:95px;" value="#{bundle.workType}" />
										</h:commandLink>
									</f:facet>
								</h:column>
								<h:column>
									<f:facet name="header">
										<h:commandLink id="statusCol_asc" styleClass="header"
											onmouseover="resetTargetFrame();"
											actionListener="#{pc_Relationships.sortColumn}">
											<h:outputText style="width:110px;" value="#{bundle.status}" />
										</h:commandLink>
									</f:facet>
								</h:column>
							</h:dataTable></td>
						</tr>
						<tr>
							<td>
							<div class="divTable" style="height: 50px" id="historyReldiv"><h:dataTable
								id="dTableRelationship" var="currentRow" columnClasses="column"
								rowClasses="#{pc_Relationships.rowStyles}" headerClass="header"
								styleClass="complexTable" cellpadding="2" cellspacing="0"
								width="100%" border="1"
								binding="#{pc_Relationships.historyDataTable}"
								value="#{pc_Relationships.relationShipHistoryData}">
								<h:column>
									<h:commandLink onmouseover="resetTargetFrame();"
										action="#{pc_Relationships.selectRow}" styleClass="textTable">
										<h:outputText style="width:95px;"
											value="#{currentRow.processor}"></h:outputText>
									</h:commandLink>
								</h:column>
								<h:column>
									<h:commandLink onmouseover="resetTargetFrame();"
										action="#{pc_Relationships.selectRow}" styleClass="textTable">
										<h:outputText style="width:95px;"
											value="#{currentRow.businessArea}"></h:outputText>
									</h:commandLink>
								</h:column>
								<h:column>
									<h:commandLink onmouseover="resetTargetFrame();"
										action="#{pc_Relationships.selectRow}" styleClass="textTable">
										<h:outputText style="width:95px;"
											value="#{currentRow.workType}"></h:outputText>
									</h:commandLink>
								</h:column>
								<h:column>
									<h:commandLink onmouseover="resetTargetFrame();"
										action="#{pc_Relationships.selectRow}" styleClass="textTable">
										<h:outputText style="width:95px;" value="#{currentRow.status}"></h:outputText>
									</h:commandLink>
								</h:column>
							</h:dataTable></div>
							</td>
						</tr>
						<tr>
							<td><h:outputText styleClass="subTextTitle"
								value="#{bundle.bckOffice_history_detail_area}"></h:outputText></td>
						</tr>
						<tr>
							<td width="100%">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td class="textLabel" width="20%"><h:outputText
											value="#{bundle.businessArea}:"></h:outputText></td>
										<td class="text" width="45%"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.businessArea}"></h:outputText></td>
										<td class="textLabel" width="16%"><h:outputText
											value="#{bundle.workType}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.workType}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText value="#{bundle.status}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.status}"></h:outputText></td>
										<td class="textLabel"><h:outputText value="#{bundle.queue}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.queueID}"></h:outputText></td>
									</tr>
									<tr>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_begin_date}:"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.dateTime}"></h:outputText></td>
										<td class="textLabel"><h:outputText
											value="#{bundle.bckOffice_prority}"></h:outputText></td>
										<td class="text"><h:outputText
											value="#{pc_Relationships.selectedHistoryRow.priority}"></h:outputText></td>
									</tr>
								</tbody>
							</table>
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="subTextTitle" colspan="2"><h:outputText
										value="#{bundle.workItems_comments}" /></td>
								</tr>
							</table>

							<table width="100%" align="center" cellpadding="0"
								cellspacing="0" border="0">
								<tbody>
									<tr>
										<td></td>
									</tr>
									<tr valign="bottom">
										<td width="100%" class="headerRow"><h:dataTable styleClass="header"
											cellpadding="0" width="100%" cellspacing="0" border="0" headerClass="headerTable">
											<h:column>
												<f:facet name="header">
													<h:commandLink id="userCol_asc" styleClass="header"
														onmouseover="resetTargetFrame();"
														actionListener="#{pc_Relationships.sortColumn}">
														<h:outputText style="width:70px;"
															value="#{bundle.bckOffice_user}" />
													</h:commandLink>
												</f:facet>
											</h:column>
											<h:column>
												<f:facet name="header">
													<h:commandLink id="dateTimeCol_asc" styleClass="header"
														onmouseover="resetTargetFrame();"
														actionListener="#{pc_Relationships.sortColumn}">
														<h:outputText style="width:110px;"
															value="#{bundle.bckOffice_dateTime}" />
													</h:commandLink>
												</f:facet>
											</h:column>
											<h:column>
												<f:facet name="header">
													<h:commandLink styleClass="header"
														onmouseover="resetTargetFrame();">
														<h:outputText style="width:220px;"
															value="#{bundle.bckOffice_text}" />
													</h:commandLink>
												</f:facet>
											</h:column>
										</h:dataTable></td>
									</tr>
									<tr>
										<td>
										<div class="divTable" style="height: 45px"><h:dataTable
											var="comment" cellspacing="0" cellpadding="0"
											columnClasses="column" rowClasses="rowOdd,rowEven"
											headerClass="header" styleClass="complexTable" width="100%"
											border="1" value="#{pc_Relationships.commentList}">
											<h:column>
												<h:outputText style="width:70px;" value="#{comment.userID}"></h:outputText>
											</h:column>
											<h:column>
												<h:outputText style="width:110px;"
													value="#{comment.dateTime}"></h:outputText>
											</h:column>
											<h:column>
												<h:outputText style="width:205px;" value="#{comment.text}"></h:outputText>
											</h:column>
										</h:dataTable></div>
										<h:outputText style="display:none;" id="selectedRow"
											value="#{pc_Relationships.selectedRowIndex}"></h:outputText></td>
									</tr>
								</tbody>
							</table>
							</td>
						</tr>
					</tbody>
				</table>
				</div>
				</td>
			</tr>
		</table>
		<div id="hiddenDiv" style="display: none"><h:commandButton
			id="hiddenButton" action="#{pc_Relationships.submit}" /> <h:inputHidden
			id="selectedIdentifier"
			value="#{pc_Relationships.selectedIdentifier}"></h:inputHidden> <h:inputHidden
			id="accordianStateIdentifier"
			value="#{pc_Relationships.accordianStateIdentifier}"></h:inputHidden>
		<h:inputHidden id="sourceOrWorkItemToDetailsDisplay"
			value="#{pc_Relationships.sourceOrWorkItemDetailsToDisplay}"></h:inputHidden>
		</div>
	</h:form>
</f:view>
</body>
</html>
