<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA132            6      Equitable Distribution of Work -->
<!-- NBA213            7      Unified user Interface -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Underwriter Workload</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<SCRIPT type="text/javascript" src="javascript/global/file.js"></SCRIPT>	
	<!-- NBA213 code deleted -->
	<script language="JavaScript" type="text/javascript">
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_underwriterWorkload'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_underwriterWorkload'].target='';
			return false;
		}
	</script>
</head>
<body onload="filePageInit();top.resetDefaultOrientation();" style="overflow-x: hidden; overflow-y: scroll"><!-- NBA213 -->
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_UWWORKLOAD" value="#{pc_uwWorkload}" />
		<h:form id="form_underwriterWorkload">
			<h:panelGroup styleClass="inputFormMat">
				<h:panelGroup styleClass="inputForm" style="height: 300px">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputLabel value="#{property.undWrkldTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
					<h:panelGroup id="undWrkldHeader" styleClass="formDivTableHeader" style="margin-top: 10px;">
						<h:panelGrid columns="2" styleClass="formTableHeader" cellspacing="0" columnClasses="ovColHdrText375,ovColHdrText200">
							<h:commandLink id="undWrkldHdrCol1" value="#{property.undWrkldCol1}"
									styleClass="ovColSorted#{pc_uwWorkload.sortedByCol1}" actionListener="#{pc_uwWorkload.sortColumn}" immediate="true" />
							<h:commandLink id="undWrkldHdrCol2" value="#{property.undWrkldCol2}"
									styleClass="ovColSorted#{pc_uwWorkload.sortedByCol2}" actionListener="#{pc_uwWorkload.sortColumn}" immediate="true" />
						</h:panelGrid>
					</h:panelGroup>
					<h:panelGroup id="undWrkldData" styleClass="formDivTableData12">
						<h:dataTable id="undWrkldTable" styleClass="formTableData" cellspacing="0" rows="0"
									binding="#{pc_uwWorkload.dataTable}" value="#{pc_uwWorkload.queues}" var="queue"
									rowClasses="#{pc_uwWorkload.rowStyles}" 
									columnClasses="ovColText375,ovColText200" >
							<h:column>
								<h:commandLink id="undWrkldCol1" value="#{queue.longName}"
											styleClass="ovFullCellSelect" action="#{pc_uwWorkload.selectRow}" immediate="true" />
							</h:column>
							<h:column>
								<h:commandLink id="undWrkldCol2" value="#{queue.workCount}"
											styleClass="ovFullCellSelect" action="#{pc_uwWorkload.selectRow}" immediate="true" />
							</h:column>
						</h:dataTable>
					</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup styleClass="tabButtonBar">
					<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" action="#{pc_uwWorkload.refresh}" immediate="true" styleClass="tabButtonLeft" />
				</h:panelGroup>
			</h:panelGroup>
		</h:form>

		<!-- NBA213 code deleted -->
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	