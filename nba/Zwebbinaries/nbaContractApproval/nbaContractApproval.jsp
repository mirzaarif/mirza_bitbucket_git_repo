<!-- CHANGE LOG -->
<!-- Audit Number	Version		Change Description -->
<!-- NBA165           6         nbA Contract Approve Decline View Rewrite Project -->
<!-- NBA158 		  6		  	Websphere 6.0 upgrade -->
<!-- NBA213 		  7		  	Unified User Interface -->
<!-- NBA208-36 		  7		  	Deferred Work Item retrieval -->
<!-- SPR1613		  8			Some Business Functions should be disabled on an Issued Contract  --> 
<!-- FNB011 		NB-1101		Work Tracking -->
<!-- NBA223			NB-1101	   	Underwriter Final Disposition Project -->
<!-- SPRNBA-626		NB-1301	   	Credit Card CWA Work Item Not Moved from Credit Card Hold Process on Manual Approval of Application -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>

<%String path = request.getContextPath();
        String basePath = "";
        if (request.getServerPort() == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        } else {
            basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/file.js"></script>
<!-- NBA213 code deleted -->
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		function setTargetFrame() {
			document.forms['form_contractapproval'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractapproval'].target='';
			return false;
		}
	</script>
	<script type="text/javascript" src="javascript/nbapopup.js"></script> <!-- NBA158 -->
</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<!-- SPRNBA-626 code deleted -->
	<PopulateBean:Load serviceName="RETRIEVE_APPROVEDECLINE" value="#{pc_contractApprDecl}" />
	<h:form id="form_contractapproval" >
		<h:panelGroup id="contrApprArea" styleClass="inputFormMat" style="height: 300px;">
			<h:panelGroup id="contrApprDeclArea" styleClass="inputForm" style="height: 270px;">
				<h:panelGroup id="contrApprHeader" styleClass="formTitleBar">
					<h:outputText id="contrApprTitle" value="#{property.contrApprTitle}" styleClass="shTextLarge" />
				</h:panelGroup>
				<h:panelGroup id="contrApprGroup" styleClass="formDataEntryLine">
					<h:panelGrid columns="2" styleClass="formSplit" columnClasses="formSplitLeftBorder, formSplitRight" cellpadding="10" cellspacing="0" border="0">
						<h:column>
							<h:panelGroup styleClass="contractApprDeclineDiv">
								<h:panelGrid id="contrApprLeftGrid" columns="2" columnClasses="contrApprShort,colMIBLong" border="0">
									<h:column>
										<h:outputText value="#{property.contractApprDisposition}" styleClass="formLabel" style="width: 50px; vertical-align: top;text-align: left;" />
										<h:outputText value="" style="width: 30px" />
									</h:column>
									<h:column>
										<h:selectOneRadio id="approve" styleClass="popupLabel" style="text-align: left;" value="#{pc_contractApprDecl.undApproveString}"
											valueChangeListener="#{pc_contractApprDecl.selectApprove}" disabled="#{pc_contractApprDecl.approveDisabled}"
											binding="#{pc_contractApprDecl.approveRadio}" rendered="#{!pc_contractApprDecl.approveIndicator}" onclick="submit()">
											<f:selectItem itemLabel="#{property.uwfinalDispApprove}" itemValue="Approved" />
										</h:selectOneRadio>
										<h:outputText value="#{property.contrApproveIndicator}" styleClass="popupLabel" rendered="#{pc_contractApprDecl.approveIndicator}"
											style="width: 190px; vertical-align: top;text-align: left;margin-left:8px" />
										<h:selectBooleanCheckbox id="otherThanAppliedFor" value="#{pc_contractApprDecl.otherThanAppliedFor}"
											disabled="#{pc_contractApprDecl.otherThanAppliedForDisabled}" style="margin-left: 3px" />
										<h:outputText value="#{property.uwfinalDispOtherAppliedFor}" styleClass="formLabel" style="width: 160px;text-align: left;" />
									</h:column>
								</h:panelGrid>
								<h:outputText id="valueBlank" value="" styleClass="formLabel" />
								<h:panelGrid id="contrApprBelowGrid" columns="2" columnClasses="colMIBShort,colMIBLong" border="0">
									<h:column>
										<h:outputText value="#{property.contractApprIssueDate}" styleClass="formLabel" style="width: 80px;text-align: right;" />
									</h:column>
									<h:column>
										<h:inputText id="issueDate" styleClass="formEntryText" style="width: 102px;margin-top: 11px;margin-left:9px;" value="#{pc_contractApprDecl.issueDate}"
											disabled="#{pc_contractApprDecl.issueDateDisabled||pc_contractApprDecl.issueComponentDisabled}">
											<f:convertDateTime pattern="#{property.datePattern}" />
										</h:inputText>
									</h:column>
								</h:panelGrid>
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup styleClass="contractApprDeclineDiv">
								<h:panelGrid id="contrApprRightGrid" columns="2" columnClasses="colMIBShort,colMIBLong" border="0">
									<h:column>
										<h:panelGroup id="doNotIssueCancelSelection" styleClass="formDataEntryLine" style="vertical-align: top;height: 19px">
											<h:selectOneRadio id="donotIssue" styleClass="popupLabel" value="#{pc_contractApprDecl.undDoNotIssueString}"
												style="width:125px;text-align: left;height: 20px;margin-top: 5px" valueChangeListener="#{pc_contractApprDecl.selectDonotIssue}"
												disabled="#{pc_contractApprDecl.doNotIssueRadioDisabled}" binding="#{pc_contractApprDecl.doNotIssueRadio}"
												rendered="#{!pc_contractApprDecl.approveIndicator&&pc_contractApprDecl.negativeDispIndicator||!pc_contractApprDecl.negativeDispIndicator&&!pc_contractApprDecl.approveIndicator}"
												onclick="submit()">
												<f:selectItem itemLabel="#{property.approveDeclineDoNoIssue}" itemValue="DoNotIssue" /> <!-- NBA223 -->
											</h:selectOneRadio>
											<h:selectOneRadio id="CancelIncomplete" styleClass="popupLabel" value="#{pc_contractApprDecl.cancelString}"
												style="width:190px;text-align: left;" valueChangeListener="#{pc_contractApprDecl.selectCancel}"
												binding="#{pc_contractApprDecl.cancelRadio}" layout="pageDirection"
												disabled="#{pc_contractApprDecl.approveIndicator && pc_contractApprDecl.negativeDispIndicator}"
												rendered="#{pc_contractApprDecl.approveIndicator}" onclick="submit()">
												<f:selectItem itemLabel="#{property.contractApprCancel}" itemValue="Cancel" />
												<f:selectItem itemLabel="#{property.contractApprIncomplete}" itemValue="Incomplete" />
											</h:selectOneRadio>
											<h:outputText id="emptyValue" value=" " style="height: 27px;"
												rendered="#{!pc_contractApprDecl.approveIndicator&&pc_contractApprDecl.negativeDispIndicator||!pc_contractApprDecl.negativeDispIndicator&&!pc_contractApprDecl.approveIndicator}" />
										</h:panelGroup>
									</h:column>
									<h:column>
										<h:panelGroup id="statusMenuGroup" styleClass="formDataEntryLine" style="height: 19px">
											<h:selectOneMenu id="statusMenu" styleClass="formEntryText" value="#{pc_contractApprDecl.undStatus}"
												style="width:148px;vertical-align: top;"
												rendered="#{pc_contractApprDecl.negativeDispIndicator&&!pc_contractApprDecl.approveIndicator||!pc_contractApprDecl.negativeDispIndicator&&!pc_contractApprDecl.approveIndicator}"
												disabled="#{pc_contractApprDecl.undStatusDisabled}">
												<f:selectItems value="#{pc_contractApprDecl.undStatuses}" />
											</h:selectOneMenu>
											<h:outputText id="blankVal" value=" " style="height: 27px;"
												rendered="#{!pc_contractApprDecl.approveIndicator&&pc_contractApprDecl.negativeDispIndicator||!pc_contractApprDecl.negativeDispIndicator&&!pc_contractApprDecl.approveIndicator}" />
										</h:panelGroup>
									</h:column>
								</h:panelGrid>
								<h:panelGroup id="statusReasonGroup" styleClass="formDataEntryLine" style="height: 19px">
									<h:selectOneMenu id="selectReason" styleClass="formEntryText" style="width:270px;;text-align: left;margin-top: 4px;margin-left: 10px"
										value="#{pc_contractApprDecl.statusReason}" disabled="#{pc_contractApprDecl.statusReasonDisabled}">
										<f:selectItems value="#{pc_contractApprDecl.undStatusReasons}" />
									</h:selectOneMenu>
								</h:panelGroup>
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:panelGroup>
				<h:panelGroup id="txErr" rendered="#{pc_contractApprDecl.tranErrors}" styleClass="text">
					<h:message for="form_contractapproval"></h:message>
					<f:verbatim>
						<hr class="formBarSeparator" />
					</f:verbatim>
				</h:panelGroup>
			</h:panelGroup>
		</h:panelGroup>
		<f:subview id="nbaCommentBar">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup id="contractApproveButtons" styleClass="formButtonBar">
			<h:commandButton id="cmdClear" value="#{property.buttonClear}" styleClass="ovButtonLeft" onclick="resetTargetFrame()"
				action="#{pc_contractApprDecl.clear}"
				disabled="#{(pc_contractApprDecl.approveIndicator && pc_contractApprDecl.negativeDispIndicator)||pc_contractApprDecl.clearDisabled}"/>
			<h:commandButton id="cmdUnapprove" value="#{property.buttonUnapprove}" styleClass="ovButtonRight-1" onclick="setTargetFrame()"
				rendered="#{pc_contractApprDecl.approveIndicator}"
				disabled="#{(pc_contractApprDecl.approveIndicator && pc_contractApprDecl.negativeDispIndicator)||pc_contractApprDecl.unapprovePushButtonDisabled}"
				action="#{pc_contractApprDecl.actionUnapproveCase}" immediate="true" />
			<h:commandButton id="cmdCommit" value="#{property.buttonCommit}" styleClass="ovButtonRight" onclick="setTargetFrame()"
				disabled="#{pc_contractApprDecl.auth.enablement['Commit'] || 
							pc_contractApprDecl.notLocked ||
							(pc_contractApprDecl.approveIndicator && pc_contractApprDecl.negativeDispIndicator) || 							
							pc_contractApprDecl.commitDisabled ||
							pc_contractApprDecl.notLocked ||
							pc_contractApprDecl.issuedNotContractChange}"
				action="#{pc_contractApprDecl.actionCommitCase}" />	<!-- NBA213 NBA208-36 SPR1613 SPRNBA-626-->
		</h:panelGroup>
	</h:form>
	<h:inputHidden id="titleBar" value="#{property.approveDeclineTitleBar}" />
	<div id="Messages" style="display: none"><h:messages rendered="#{!pc_contractApprDecl.tranErrors}" /></div>
</f:view>
</body>
</html>
