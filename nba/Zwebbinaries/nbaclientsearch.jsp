<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA067            3      New tab for client search on the app entry view -->
<!-- SPR1593           4      Search for Corporation using types Phonetic, Contains & Starts With does not retrieve details - CLIF -->
<!-- SPR1592           5      UI/Edit Issues for Appent view -->

<DIV ID="Content4" style="VISIBILITY: hidden" >
	<FIELDSET ALIGN=left STYLE="LEFT: 5px; WIDTH: 300px; POSITION: absolute; TOP: 10px; HEIGHT: 50px" ID="Clientsearch">
	    	<LEGEND VALIGN="TOP"><SPAN>Type</SPAN>
	        <INPUT ID="Exact" NAME="SearchType_RB" TYPE=radio STYLE ="LEFT: 5px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="0" accesskey="E">
	       <SPAN STYLE ="LEFT: 20px; WIDTH: 30px; POSITION: absolute; TOP: 22px; HEIGHT: 21px">
			 <u>E</u>xact
		   </SPAN>	
		   <INPUT ID="Phonetic" NAME="SearchType_RB" TYPE=radio STYLE ="LEFT: 55px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="1" accesskey="P"><!-- SPR1592 -->
		    <SPAN STYLE ="LEFT: 70px; WIDTH: 50px; POSITION: absolute; TOP: 22px; HEIGHT: 21px">
			 <P><U>P</U>honetic</P><!-- SPR1592 -->
		   </SPAN>	
		   <INPUT ID="Contains" NAME="SearchType_RB" TYPE=radio STYLE ="LEFT: 125px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="2" accesskey="N"><!-- SPR1592 -->
		    <SPAN STYLE ="LEFT: 140px; WIDTH: 50px; POSITION: absolute; TOP: 22px; HEIGHT: 21px">
			 <P>Co<U>n</U>tains</P><!-- SPR1592 -->
		   </SPAN>	
		   <INPUT ID="Startswith" NAME="SearchType_RB" TYPE=radio STYLE ="LEFT: 195px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="3" accesskey="W"><!-- SPR1592 -->
		    <SPAN STYLE ="LEFT: 210px; WIDTH: 80px; POSITION: absolute; TOP: 22px; HEIGHT: 21px">
			 <P>Starts <U>W</U>ith</P><!-- SPR1592 -->
		   </SPAN>	
	</FIELDSET>
    <FIELDSET ALIGN=left STYLE="LEFT: 315px; WIDTH: 300px; POSITION: absolute; TOP: 10px; HEIGHT: 50px" ID="Clienttype">
	    	<LEGEND VALIGN="TOP"><SPAN>Client Type</SPAN>
	    	<INPUT ID="Individual" NAME="ClientType_RB" TYPE=radio STYLE ="LEFT: 30px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="0" onclick="indRadioClicked()" >
		    <SPAN STYLE ="LEFT: 50px; WIDTH: 70px; POSITION: absolute; TOP: 21px; HEIGHT: 21px">
			 Individual
		   </SPAN>	
		   <INPUT ID="Business" NAME="ClientType_RB" TYPE=radio STYLE ="LEFT: 175px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="1" onclick="busRadioClicked()">
		    <SPAN STYLE ="LEFT: 195px; WIDTH: 80px; POSITION: absolute; TOP: 21px; HEIGHT: 21px">
			 Business
		   </SPAN>	
	</FIELDSET>
	<FIELDSET ALIGN=left STYLE="LEFT: 5px; WIDTH: 300px; POSITION: absolute; TOP: 65px; HEIGHT: 115px" ID="Nametype">
	    	<LEGEND VALIGN="TOP"><SPAN>Name</SPAN>
	   	    <SPAN STYLE ="LEFT: 10px; WIDTH: 60px; POSITION: absolute; TOP: 35px; HEIGHT: 21px">
			 Last*:
		   </SPAN>	
		   <INPUT ID="LastName_OTH_EF" NAME="LastName_OTH_EF" STYLE ="LEFT: 80px; WIDTH: 200px; POSITION: absolute; TOP: 30px; HEIGHT: 21px">
	       <SPAN STYLE ="LEFT: 10px; WIDTH: 60px; POSITION: absolute; TOP: 60px; HEIGHT: 21px">
			 First:
		   </SPAN>	
		   <INPUT ID="FirstName_OTH_EF" NAME="FirstName_OTH_EF" STYLE ="LEFT: 80px; WIDTH: 200px; POSITION: absolute; TOP: 55px; HEIGHT: 21px">
	       <SPAN STYLE ="LEFT: 10px; WIDTH: 60px; POSITION: absolute; TOP: 85px; HEIGHT: 21px">
			 Middle:
		   </SPAN>	
		   <INPUT ID="MiddleName_OTH_EF" NAME="MiddleName_OTH_EF" STYLE ="LEFT: 80px; WIDTH: 200px; POSITION: absolute; TOP: 80px; HEIGHT: 21px">
	
	</FIELDSET>
	<FIELDSET ALIGN=left STYLE="LEFT: 315px; WIDTH: 300px; POSITION: absolute; TOP: 65px; HEIGHT: 115px" ID="Birth">
	    	<LEGEND VALIGN="TOP"><SPAN>Birth Information</SPAN>
	   	    <SPAN STYLE ="LEFT: 40px; WIDTH: 50px; POSITION: absolute; TOP: 30px; HEIGHT: 21px">
			 Date:
		   </SPAN>	
		   <INPUT ID="BirthDate_OTH_EF" NAME="BirthDate_OTH_EF" STYLE ="LEFT: 100px; WIDTH: 80px; POSITION: absolute; TOP: 30px; HEIGHT: 21px" onchange="date_onChange(this)" onkeypress="date_onKeyPress(this)">
	       <SPAN STYLE ="LEFT: 40px; WIDTH: 60px; POSITION: absolute; TOP: 55px; HEIGHT: 21px">
			 State:
		   </SPAN>	
		   <SELECT ID="BirthState_OTH_DD" NAME="BirthState_OTH_DD" STYLE ="LEFT: 100px; WIDTH: 150px; POSITION: absolute; TOP: 55px; HEIGHT: 21px">
  			<OPTION value="" selected ></OPTION>
		</SELECT>
	</FIELDSET>
	<FIELDSET ALIGN=left STYLE="LEFT: 5px; WIDTH: 300px; POSITION: absolute; TOP: 184px; HEIGHT: 65px" ID="TaxInformation">
	    	<LEGEND VALIGN="TOP"><SPAN>Tax Information</SPAN>
	       <SPAN STYLE ="LEFT: 10px; WIDTH: 150px; POSITION: absolute; TOP: 15px; HEIGHT: 21px">
			 Social Security Number*:
	        </SPAN>
	        <INPUT ID="GovtID_OTH_EF" NAME="GovtID_OTH_EF" STYLE ="LEFT: 175px; WIDTH: 100px; POSITION: absolute; TOP: 15px; HEIGHT: 21px" onchange="ssn_onChange(this)" onkeypress="ssn_onKeyPress(this)">
	       <SPAN STYLE ="LEFT: 10px; WIDTH: 150px; POSITION: absolute; TOP: 40px; HEIGHT: 21px">
			 Tax Identification*:
	        </SPAN>
	        <INPUT ID="GovtIDTc_OTH_EF" NAME="GovtIDTc_OTH_EF" STYLE ="LEFT: 175px; WIDTH: 100px; POSITION: absolute; TOP: 40px; HEIGHT: 21px" onchange="ssn_onChange(this)" onkeypress="ssn_onKeyPress(this)">
	
	</FIELDSET>
	<FIELDSET ALIGN=left STYLE="LEFT: 315px; WIDTH: 300px; POSITION: absolute; TOP: 184px; HEIGHT: 65px" ID="Gender">
	    	<LEGEND VALIGN="TOP"><SPAN>Gender</SPAN>
	   	    <INPUT ID="Gender_Male" NAME="Gender_OTH_RB" TYPE=radio STYLE ="LEFT: 30px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="0" accesskey="M"><!-- SPR1592 -->
		    <SPAN STYLE ="LEFT: 50px; WIDTH: 50px; POSITION: absolute; TOP: 20px; HEIGHT: 21px">
			 <P><U>M</U>ale</P><!-- SPR1592 -->
		   </SPAN>	
		   <INPUT ID="GenderFemale" NAME="Gender_OTH_RB" TYPE=radio STYLE ="LEFT: 120px; WIDTH: 13px; POSITION: absolute; TOP: 20px" VALUE="1" accesskey="F"><!-- SPR1592 -->
		    <SPAN STYLE ="LEFT: 140px; WIDTH: 50px; POSITION: absolute; TOP: 20px; HEIGHT: 21px">
			 <P><U>F</U>emale</P><!-- SPR1592 -->
		   </SPAN>	
	 </FIELDSET>
	 <FIELDSET ALIGN=left STYLE="LEFT: 5px; WIDTH: 600px; POSITION: absolute; TOP: 253px; HEIGHT: 47px" ID="CorpName">
	       <LEGEND VALIGN="TOP"><SPAN>Corporation Name</SPAN> 
	        <SPAN STYLE ="LEFT: 10px; WIDTH: 75px; POSITION: absolute; TOP: 17px; HEIGHT: 21px">
			 Corporation*:
		   </SPAN>	
		   <INPUT ID="Corp_EF" NAME="Corp_EF" STYLE ="LEFT: 115px; WIDTH: 475px; POSITION: absolute; TOP: 16px; HEIGHT: 21px">
	 </FIELDSET>
     <BUTTON type=button Name="Search" STYLE ="LEFT: 5px; WIDTH: 100px; POSITION: absolute; TOP: 310px; HEIGHT: 25px"  value=Search onclick="search_Clicked()"  accesskey="R">Sea<U>r</U>ch</BUTTON><!-- SPR1592 -->
	 <BUTTON type=button Name="Clear" STYLE ="LEFT: 515px; WIDTH: 100px; POSITION: absolute; TOP: 310px; HEIGHT: 25px"  value=Clear onclick="clear_Clicked()">Clear</BUTTON>
	
	<FIELDSET ALIGN=left STYLE="LEFT: 0px; WIDTH: 645px; POSITION: absolute; TOP: 340px; HEIGHT: 400px" ID="Results">
	    	<LEGEND VALIGN="TOP"><SPAN>Client Search Results</SPAN>
	    	<DIV ID="Nba_ClientSearch_LP"  name="Nba_ClientSearch_LP" style="Z-INDEX: 100; LEFT: 10px;visibility:hidden; WIDTH: 590px; POSITION: absolute; TOP: 20px; HEIGHT: 250px" class=tvcontainer></DIV>
	    	<SPAN STYLE ="LEFT: 10px; WIDTH: 75px; POSITION: absolute; TOP: 20px; HEIGHT: 15px">
			 Client:
		   </SPAN>
	    	<DIV ID="Nba_ClientSearch_Client_LP"  name="Nba_ClientSearch_Client_LP" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 640px; POSITION: absolute; TOP: 40px; HEIGHT: 125px" class=tvcontainer></DIV>
	    	<SPAN STYLE ="LEFT: 10px; WIDTH: 75px; POSITION: absolute; TOP: 170px; HEIGHT: 15px">
			 Address:
		   </SPAN>
			<DIV ID="Nba_ClientSearch_Address_LP"  name="Nba_ClientSearch_Address_LP" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 640px; POSITION: absolute; TOP: 190px; HEIGHT:  75px" class=tvcontainer></DIV>
	    	<SPAN STYLE ="LEFT: 10px; WIDTH: 75px; POSITION: absolute; TOP: 270px; HEIGHT: 15px">
			 Phone:
		   </SPAN>
		   <SPAN STYLE ="LEFT: 325px; WIDTH: 75px; POSITION: absolute; TOP: 270px; HEIGHT: 15px">
			 EMail:
		   </SPAN>	
		   <DIV ID="Nba_ClientSearch_Phone_LP"  name="Nba_ClientSearch_Phone_LP" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 310px; POSITION: absolute; TOP: 290px; HEIGHT:  60px" class=tvcontainer></DIV>
	    	<DIV ID="Nba_ClientSearch_Email_LP"  name="Nba_ClientSearch_Email_LP" style="Z-INDEX: 100; LEFT: 315px; WIDTH: 310px; POSITION: absolute; TOP: 290px; HEIGHT:  60px" class=tvcontainer></DIV>
	    	
	    	<SPAN STYLE ="LEFT: 5px; WIDTH: 225px; POSITION: absolute; TOP:355px; HEIGHT: 21px">
	    	Select Role for person to be added :
	    	</SPAN>
	    	 <SELECT ID="Role_DD" NAME="Role_DD" STYLE ="LEFT: 230px; WIDTH: 150px; POSITION: absolute; TOP: 355px; HEIGHT: 40px" multiple>
  			  <OPTION value="" selected ></OPTION>
		    </SELECT>
	        <BUTTON type=button Name="Select" STYLE ="LEFT: 400px; WIDTH: 100px; POSITION: absolute; TOP: 355px; HEIGHT: 25px"  value=Select onclick="select_Clicked()" accesskey="T">Selec<U>t</U></BUTTON><!-- SPR1592 -->
	        <BUTTON type=button Name="SelectC" STYLE ="LEFT: 510px; WIDTH: 100px; POSITION: absolute; TOP: 355px; HEIGHT: 25px"  value=SelectCriteria onclick="selectCriteria_Clicked()" accesskey="I">Select Cr<U>i</U>teria</BUTTON><!-- SPR1592 -->
	</FIELDSET>
</DIV>

<SCRIPT>
       addSecureField("SEARCH");	 
		addSecureField("CLIENT");
		addSecureField("LNAME");
		addSecureField("FNAME");
		addSecureField("MNAME");
		addSecureField("BIRTHDT");
		addSecureField("BIRTHST");
		addSecureField("SSN");
		addSecureField("TAXID");
		addSecureField("CNAME");
		addSecureField("GENDER"); 
		addSecureField("INTCLNT"); 		
		
var search_Called;
var addKount, phoneKount, emailKount;
var itemPrinter = new ItemPrinter(); 

var clientTab = new Tab("Content4");
clientTab.headerName = "Client Search";
clientTab.headerWidth = 150;
clientTab.width = 650;
clientTab.clickHandler = "page4_onclick";
	
function page4_onclick(){	
} 	
//function to perform client search
function search_Clicked() {
  if (clientSearch_validate()){
  getField("ACTION").value = "SEARCH";
  
  if (getField("SearchType_RB")[0].checked) {  
      getField("SEARCH").value = "EXACT";
  }
  if (getField("SearchType_RB")[1].checked) {  
      getField("SEARCH").value = "PHONETIC";
  }
  if (getField("SearchType_RB")[2].checked) {  
      getField("SEARCH").value = "CONTAINS";
  }
  if (getField("SearchType_RB")[3].checked) {  
      getField("SEARCH").value = "STARTS WITH";
  }
  getField("CLIENT").value = "INDIVIDUAL";
  getField("LNAME").value = getField("LastName_OTH_EF").value;
  getField("FNAME").value = getField("FirstName_OTH_EF").value;
  getField("MNAME").value = getField("MiddleName_OTH_EF").value;
 
  getField("CLIENT").value = "BUSINESS";
  getField("CNAME").value = getField("Corp_EF").value;
   
  getField("BIRTHDT").value = getField("BirthDate_OTH_EF").value;
  getField("BIRTHST").value = getField("BirthState_OTH_DD").value;
  getField("SSN").value = getField("GovtID_OTH_EF").value;
  getField("TAXID").value = getField("GovtIDTc_OTH_EF").value;
  getField("GENDER").value = "";
   if (getField("Gender_OTH_RB")[0].checked) {  
      getField("GENDER").value = "1";
  }
  if (getField("Gender_OTH_RB")[1].checked) {  
      getField("GENDER").value = "2";
  }
    invokeServer("NbaClientSearchServlet"); 
    search_Called = "y";
    	  
  }
}

//function to clear fields on the client search tab
function clear_Clicked() {

  setRB("SearchType_RB","", false);  //SPR1592
  setRB("ClientType_RB","", false); //SPR1592
  getField("LastName_OTH_EF").value = "";
  getField("FirstName_OTH_EF").value = "";
  getField("MiddleName_OTH_EF").value = "";
  getField("BirthDate_OTH_EF").value = "";
  getField("BirthState_OTH_DD").value = "";
  getField("GovtID_OTH_EF").value = "";
  getField("GovtIDTc_OTH_EF").value = "";
  getField("Gender_OTH_RB")[0].checked = false; 
  getField("Gender_OTH_RB")[1].checked = false; 
  getField("Corp_EF").value = ""; 
 // SPR1592 code deleted
  setRB("Gender_OTH_RB","", false);
  
 
}

//function to validate client search parameters
function clientSearch_validate() {   
  if (getField("SearchType_RB")[0].checked) {
       if (getField("ClientType_RB")[0].checked) {
           if((getField("GovtID_OTH_EF").value == "") && (getField("LastName_OTH_EF").value == "")) {
               alert("Last Name or Social Security Number are missing or invalid. Please correct.");
               return false;
            }  
       } else {
          if ((getField("GovtIDTc_OTH_EF").value == "") && (getField("Corp_EF").value == "")) {
                alert("Corporation Name  or Tax Identification are missing or invalid. Please correct."); 
                 return false;
          }
       } 
  } else {
  //begin SPR1593
	  if (getField("ClientType_RB")[0].checked) { 
	     if (getField("LastName_OTH_EF").value == "" ){
	       alert("Last Name is missing or invalid. Please correct.");
	       return false;
	     }
	   } else {
	     if (getField("Corp_EF").value == "" ){
	       alert("Corporation Name is missing or invalid. Please correct.");
	       return false;
	     } 
	   }
   //end SPR1593
 }  
 return true;
}

//function for Individual radio button clicked
function indRadioClicked() {
  getField("GovtIDTc_OTH_EF").value = "";
  getField("Corp_EF").value = "";
  disableField(true, "GovtIDTc_OTH_EF");
  disableField(false, "GovtID_OTH_EF");
  disableField(true, "Corp_EF");
  disableField(false, "FirstName_OTH_EF");
  disableField(false, "MiddleName_OTH_EF");
  disableField(false, "LastName_OTH_EF");
  disableLabel(true, "Corporation*");
  disableLabel(true, "Tax Identification*");
  //getField("Gender_OTH_RB").checked = false;
  
}

//function for Bussiness radio button clicked
function busRadioClicked() {
  getField("GovtID_OTH_EF").value = "";
  getField("FirstName_OTH_EF").value = "";
  getField("MiddleName_OTH_EF").value = "";
  getField("LastName_OTH_EF").value = "";
  disableField(true, "GovtID_OTH_EF");
  disableField(true, "FirstName_OTH_EF");
  disableField(true, "MiddleName_OTH_EF");
  disableField(true, "LastName_OTH_EF");
  disableField(false, "GovtIDTc_OTH_EF");
  disableField(false, "Corp_EF");
  setRB("Gender_OTH_RB","", false);
} 
// handle the click event on the list pane
function handle_click(){
	
	var selectedNode = Nba_ClientSearch_LP.getActiveNode();
	//setComponentsFromNode(selectedNode);
}
// handle the click event on the list pane
function clientsearch_handle_click(){
    //debugger; 
    disableField(false, "Select");
	disableField(false, "SelectC");
	Nba_ClientSearch_Address_LP.clear();
    Nba_ClientSearch_Phone_LP.clear();
    Nba_ClientSearch_Email_LP.clear();
    addKount = 0;
    phoneKount = 0;
    emailKount = 0;
  	var selectedNode = Nba_ClientSearch_Client_LP.getActiveNode();
	Nba_ClientSearch_LP.setActiveNode(selectedNode.key, selectedNode.tag, false);
	var selectedRootNode = Nba_ClientSearch_LP.getActiveNode();
	var children = Nba_ClientSearch_LP.getChildren(false,selectedRootNode);
	//var children = Nba_ClientSearch_LP.getChildren(false,selectedNode);
	
	var childCount = 0;
	while(childCount < children.length) {
	      Nba_ClientSearch_LP.setActiveNode(children[childCount].key, children[childCount].tag, false);
          var addressChildren = Nba_ClientSearch_LP.getChildren(false,children[childCount]);
          var addressChildCount = 0;
          var childMethod;
         
          if (children[childCount].tag  == "AddressRoot") {
              childMethod = "Address";
          } else if(children[childCount].tag  == "PhoneRoot") {
              childMethod = "Phone";
          } else if(children[childCount].tag  == "EmailRoot") {
              childMethod = "Email";
          }
          while(addressChildCount < addressChildren.length) {
               var aNewAddressNode =  null;
               Nba_ClientSearch_LP.setActiveNode(addressChildren[addressChildCount].key,addressChildren[addressChildCount].tag, true);
               aNewAddressNode = itemPrinter.createNode("a"+childMethod+"StringForClientSearch");
               aNewAddressNode.tag = addressChildren[addressChildCount].tag;
               aNewAddressNode.key = addressChildren[addressChildCount].key;
               if (children[childCount].tag  == "AddressRoot") {
                //  Nba_ClientSearch_Address_LP.setActiveNode(children[childCount].key, children[childCount].tag, false);
                  Nba_ClientSearch_Address_LP.addRootNode(aNewAddressNode);
                  addKount++;
               } else if(children[childCount].tag  == "PhoneRoot") {
                  Nba_ClientSearch_Phone_LP.setActiveNode(children[childCount].key, children[childCount].tag, false);
                  Nba_ClientSearch_Phone_LP.addRootNode(aNewAddressNode);
                  phoneKount++;
               } else if(children[childCount].tag  == "EmailRoot") {
                  Nba_ClientSearch_Email_LP.setActiveNode(children[childCount].key, children[childCount].tag, false);
                  Nba_ClientSearch_Email_LP.addRootNode(aNewAddressNode);
                  emailKount++;
               }
               addressChildCount++;
           }
             childCount++;
	       }
}
function address_handle_click() {
 var  selectedNode = Nba_ClientSearch_Address_LP.getActiveNode();
 
}
function email_handle_click() {
var   selectedNode = Nba_ClientSearch_Email_LP.getActiveNode();
}
function phone_handle_click() {
 var  selectedNode = Nba_ClientSearch_Phone_LP.getActiveNode();
}
  </SCRIPT>