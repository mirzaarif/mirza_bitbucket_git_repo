<%-- CHANGE LOG --%>
<%-- Audit Number   Version   Change Description --%>
<%-- SPR3073           6      The add comments dialog under the upgraded (JSF) look & feel should not cover nbA --%>
<%-- NBA213            7      Unified user Interface --%>
<%-- FNB004         NB-1101   PHI --%>
<%-- NBA356         NB-1501   Comments Floating View --%>
<%-- SPRNBA-306     NB-1501   Commit Link Not Displayed on Comment Button Bar When Initially Add from History View --%>

<%@ page language="java" %>
<%-- SPRNBA-306 deleted --%>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
//begin SPRNBA-306
boolean refreshBF = true;
if (request.getQueryString() != null) {
	refreshBF = (request.getQueryString().indexOf("refreshBF=true") >= 0);
}
//end SPRNBA-306
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Close PopUp</title>  <%-- SPRNBA-306 --%>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="theme/accelerator.css">
<script type="text/javascript">
		//FNB004 code deleted
		top.window.close();
		//begin SPRNBA-306
		if ('<%= refreshBF %>' == 'true') {
			top.refreshParent();
		}
		//end SPRNBA-306
		//end FNB004
	</script>
</head>
<body>
</body>
</html>
