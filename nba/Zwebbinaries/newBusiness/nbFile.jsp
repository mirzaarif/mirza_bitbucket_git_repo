<%-- CHANGE LOG
     Audit Number   Version   Change Description
     NBA213            7      Unfied User Interface
     NBA251            8      nbA Case Manager and Companion Case Assignment
     SPR3519           8      Images and Image Viewer not Closed when Work Item Closed
     SPR3581           8      Application Entry and Application Update - Life or Annuity Tab Should be Disabled Based on Product
     SPR2926 		   8	  Red bar not displayed on tabs when there are items that require attention
     SPR3757  		   8	  Credit Card Payment Tab is disabled for Inforce Payments
     NBA254  		 NB-1101  nbA Automatic Closure and Refund of CWA
     SPR3826  		 NB-1101  General Code Clean Up
     FNB011  		 NB-1101  Work Tracking
     FNB013  		 NB-1101  DI Support for nbA
     FNB004			 NB-1101  PHI
     FNB016			 NB-1101  Configuration Changes
     NBA231			 NB-1101  Replacement Processing
     SPRNBA-576      NB-1301  Navigation Of The Coverages And Clients On The Final Disposition Overview Tab Is Incorrect
     NBA318    		 NB-1301  Quality Review Process
     NBA317          NB-1301  PCI Compliance For Credit Card Numbers Using Web Service 
     NBA324          NB-1301  nbAFull Personal History Interview
     SPRNBA-659      NB-1301  Issues in entering data on indexing view
     SPR3454         NB-1401   Not Prompted for Uncommitted Draft Changes When Navigating to Another Tab or Another Business Function
     SPR3103     	 NB-1401   Not Prompted for Uncommitted Changes on Invoke of Refresh or Leaving View
     NBA244			 NB-1401  Combine Contract Status and Contract Summary View
     SPRNBA-947 	 NB-1601  Null Pointer Exception May Occur in Inbox if User Selects Different Work
     SPRNBA-976      NB-1601  Null session variables result from overlapping transactions initiated on the To-Do List user interface
--%>
<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/FileLoader.tld" prefix="FileLoader"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>New Business File</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css"> 
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = 59;		
			var leftOffset = 5;
			var maxTabsPerRow =8;
			var fileLocationHRef = null;  
			var defaultindex = 1;
			var commentsIndex = 2;
			var commentReturnIndex = 0;
			var draftChanges = false;
			//SPRNBA-976 code deleted
			var prevSelectedText = null; //SPR3454
			
			//NBA251 New function
			function launchToDoList() {
				//begin SPRNBA-976
				if (top.todoPopup == null || top.todoPopup.closed) {
					top.todoPopup = launchPopup('toDoList', '<%=path%>/common/popup/popupFrameset.html?popup=<%=basePath%>/nbaToDoList/popup/nbaToDoList.faces', 600, 350);
				}
				top.todoPopup.focus();				
				//end SPRNBA-976
				return false;
			}
		
			function setTargetFrame() {
				document.forms['actionMenuForm'].target='controlFrame';
				return false;
			}
			function resetTargetFrame() {
				document.forms['actionMenuForm'].target='';
				return false;
			}

			function initialize(){
				try{
					top.mainContentFrame.contentRightFrame.nbaContextMenu.location.href = '<%=	path%>' + '/faces/desktops/menus/nbaStatusBar.jsp';
					//begin SPRNBA-976
					if (document.getElementById('actionMenuForm:closeToDoList').value == 'true') {
						top.closeToDoList();
					}
					//end SPRNBA-976
				} catch(err){
					top.reportException(err,"loadNbaContextBar");
				}
			}

			function initFileSize() {
				tabHeight = window.screen.availHeight - 250;
				document.getElementById('file').height = tabHeight;
			}

			function toggleActionMenu() {
				if(nbaMenu != null){
					if(nbaMenu.style.visibility=='visible'){
						nbaMenu.style.visibility='hidden';
					} else {
						var menu = document.getElementById('menuspan');
						nbaMenu.style.left =menu.offsetLeft;
						nbaMenu.style.top = menu.offsetHeight + 20;
						nbaMenu.style.visibility = 'visible';
					}
				}
				return false;
			}
			//SPR3519 new method
			function closeImages() {
				top.ImgViewer.closeImages(); 
			}
			
			//begin NBA317
			function setHiddenFieldValues(elementID, newValue){
				document.getElementById(elementID).value = newValue;
			}
			
			function getHiddenFieldValue(elementID) {
				return document.getElementById(elementID).value;
			}
			//end NBA317
			
			//begin SPR3454
			function ignoreDraftChangesForTab(newTabText){  //SPR3103
				return (newTabText.innerText == "UW Comments" || newTabText.innerText == "Clients" || newTabText.innerText == "Coverages");
			}
			
			function setPrevSelectedTabText(prevSelectedTextTemp,newTabTextTemp){ //SPR3103
				if(prevSelectedTextTemp != null){
					if( prevSelectedTextTemp.innerText == "Final Disposition" || prevSelectedTextTemp.innerText == "Clients" || prevSelectedTextTemp.innerText == "Coverages"){
						if(newTabTextTemp.innerText == "Final Disposition" || newTabTextTemp.innerText == "Clients" || newTabTextTemp.innerText == "Coverages")
								prevSelectedText = newTabTextTemp;
					}
				}
			}
			//end SPR3454
		//-->
	</script>
<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body class="desktopBody" onload="initialize();top.showWait('Loading Work Item');closeImages();"
	style="overflow-x: hidden; overflow-y: hidden; margin-left: 0px; margin-right: 0px;"> <%-- SPR3519 SPRNBA-947 --%>
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="INIT_NB_FROM_WORKFLOW" value="#{pc_WorkflowFile}" />
	<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr style="height:50px;" class="desktopBody">
				<td colspan="2"><iframe name="nbaContextMenu"
					style="height: 50px; width: 100%;"
					src="initializingContextMenu.html" frameborder="0" scrolling="no"></iframe>
				</td>
			</tr>
			<tr style="height:60px;" class="textMainTitleBar">
				<td ><FileLoader:Files location="newBusiness/file/" numTabsPerRow="8" /></td>
				<td align="right">
					<span id="todospan"
						onclick="launchToDoList();"
						style="height=100%;text-align=top; padding-top=5px;">
						<h:graphicImage id="todoImg" styleClass="menu" url="/images/toDoList.gif" rendered="#{pc_nbAAuth.auth.visibility['ToDoIcon'] && pc_nbAAuth.aggregateContractWork}"/>
					</span><%-- NBA251 --%>
					<span id="menuspan"
						onclick="toggleActionMenu();"
						style="height=100%;text-align=top;">
						<h:graphicImage id="actionMenuImg" styleClass="menu" url="/images/actionMenu.gif" />
					</span><%-- NBA251 --%>
				</td>
				<%-- Begin NBA244 --%>
				<FileLoader:DisableTab disableIndex="1_0"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractApproval'] && 
									(pc_nbAAuth.auth.enablement['ContractChangeBusiness'] || pc_nbAAuth.notContractChange) &&
									pc_nbAAuth.auth.enablement['ContractCopy'] &&
									pc_nbAAuth.auth.enablement['ContractInformation'] &&
									pc_nbAAuth.auth.enablement['Messages'] &&
									pc_nbAAuth.auth.enablement['PlanChange'] &&
									pc_nbAAuth.auth.enablement['ContractPrint'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_1"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractApproval'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_2"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractChangeBusiness'] ||
									pc_nbAAuth.notContractChange}" />
				<FileLoader:DisableTab disableIndex="1_3"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractCopy'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_4"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractInformation'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_5"
					disableValue="#{pc_nbAAuth.auth.enablement['Messages'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_6"
					disableValue="#{pc_nbAAuth.auth.enablement['PlanChange'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="1_7"
					disableValue="#{pc_nbAAuth.auth.enablement['ContractPrint'] ||
									pc_nbAAuth.contractMissing}" />
				<%-- End NBA244 --%>	
				<FileLoader:DisableTab disableIndex="2_0"
					disableValue="#{pc_nbAAuth.auth.enablement['Index']}" />
				<FileLoader:DisableTab disableIndex="3_0"
					disableValue="#{pc_nbAAuth.auth.enablement['ApplicationEntry'] ||
									(pc_nbAAuth.transaction && pc_nbAAuth.contractMissing)}" />
				<FileLoader:DisableTab disableIndex="3_1"
					disableValue="#{!pc_nbAAuth.life || pc_nbAAuth.di || pc_nbAAuth.PHITransaction}" /><%-- SPR3581 SPR3826 FNB013 FNB004--%>
				<FileLoader:DisableTab disableIndex="3_2"
					disableValue="#{!pc_nbAAuth.annuity || pc_nbAAuth.PHITransaction }" /><%-- SPR3581 SPR3826 FNB004 --%>
                <FileLoader:DisableTab disableIndex="3_3"
					disableValue="#{!pc_nbAAuth.di || pc_nbAAuth.PHITransaction}" /><%-- FNB013 FNB004 --%>
				<FileLoader:DisableTab disableIndex="3_4"
					disableValue="#{!pc_nbAAuth.PHITransaction || pc_nbAAuth.auth.enablement['PHI']}" /> <%-- FNB004 FNB013 NBA324--%>
				<FileLoader:DisableTab disableIndex="4_0"
					disableValue="#{pc_nbAAuth.auth.enablement['Billing'] &&
									pc_nbAAuth.auth.enablement['Comments'] &&
									pc_nbAAuth.auth.enablement['Correspondence'] &&
									pc_nbAAuth.auth.enablement['Requirements'] &&
									pc_nbAAuth.auth.enablement['Replacement'] &&
									pc_nbAAuth.auth.enablement['CoverageParty']}" />
				<FileLoader:DisableTab disableIndex="4_1"
					disableValue="#{pc_nbAAuth.auth.enablement['Billing'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="4_2"
					disableValue="#{pc_nbAAuth.auth.enablement['Comments']}" />
				<FileLoader:DisableTab disableIndex="4_3"
					disableValue="#{pc_nbAAuth.auth.enablement['Correspondence'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="4_4"
					disableValue="#{pc_nbAAuth.auth.enablement['Requirements'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="4_5"
					disableValue="#{pc_nbAAuth.auth.enablement['Replacement'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="4_6"
					disableValue="#{pc_nbAAuth.auth.enablement['CoverageParty'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="4_7"
					disableValue="#{pc_nbAAuth.auth.enablement['CompanionCase'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="5_0"
					disableValue="#{pc_nbAAuth.auth.enablement['CWA'] &&
									pc_nbAAuth.auth.enablement['CreditCard']}" /> <%-- SPR3757 --%>
				<FileLoader:DisableTab disableIndex="5_1"
					disableValue="#{pc_nbAAuth.auth.enablement['CWA'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:DisableTab disableIndex="5_2"
					disableValue="#{pc_nbAAuth.auth.enablement['CreditCard']}" /> <%-- SPR3757 --%>
				<FileLoader:DisableTab disableIndex="6_0"
					disableValue="#{pc_nbAAuth.auth.enablement['UnderwriterWorkbench'] ||
									pc_nbAAuth.contractMissing}" />
				<FileLoader:NotifyTab notifyIndex="6_1"
					notifyValue="#{pc_attntnreqbean.reqImpRequiresAttn}" /><%-- SPR2926 --%>
			</tr>
			<tr style="height:*; vertical-align: top;" class="textMainTitleBar">
				<td colspan="2"><iframe id="file" name="nbFile" src="" height="750px"
					width="100%" frameborder="0" scrolling="auto"
					onload="initFileSize();"></iframe></td>  <%-- SPRNBA-576 --%>
			</tr>
		</tbody>
	</table>
	<h:form id="actionMenuForm">
	<div id="nbaMenu" class="menuUtilities" onmouseout="toggleActionMenu();"  style="z-index: 100">
	<TABLE WIDTH="100%" CELLSPACING="0" CELLPADDING="0" >
		<tbody>
			<tr>
				<td class="menuItem"
					onmouseover="onSubMenuOver(this, document.all['actionMenuForm:reevalMenuItem']);"
					onmouseout="onSubMenuOut(this, document.all['actionMenuForm:reevalMenuItem']);">
					<h:commandLink value="#{property.evaluate}"
						action="#{pc_actionMenu.processEvaluate}" styleClass="menuItem"
						onmouseover="this.isClicked=false;" id="evalMenuItem"
						onmousedown="setTargetFrame();" rendered="#{pc_nbAAuth.auth.visibility['EvaluateMenu'] && !pc_nbAAuth.contractMissing}" onmouseup /> <%-- FNB016 NBA231 --%>
				</td>
			</tr>
			<%-- Begin NBA318 --%>
			<tr>
				<td class="menuItem"
					onmouseover="onSubMenuOver(this, document.all['actionMenuForm:reviewQualityMenuItem']);"
					onmouseout="onSubMenuOut(this, document.all['actionMenuForm:reviewQualityMenuItem']);">
					<h:commandLink value="#{property.reviewquality}"
						action="#{pc_actionMenu.processQualityReview}" styleClass="menuItem"
						onmouseover="this.isClicked=false;" id="reviewQualityMenuItem"
						onmousedown="setTargetFrame();" immediate="true" rendered="#{pc_nbAAuth.auth.visibility['ReviewQualityMenu'] && pc_nbAAuth.qualityReviewEnabled}"/>
				</td>
			</tr>
			<%-- End NBA318 --%>
			<tr>
				<td class="menuItem"
					onmouseover="onSubMenuOver(this, document.all['actionMenuForm:reopenMenuItem']);"
					onmouseout="onSubMenuOut(this, document.all['actionMenuForm:reopenMenuItem']);">
					<h:commandLink value="#{property.reopen}"
						action="#{pc_actionMenu.processReopen}" styleClass="menuItem"
						onmouseover="this.isClicked=false;" id="reopenMenuItem"
						onmousedown="setTargetFrame();" immediate="true" rendered="#{pc_nbAAuth.auth.visibility['ReOpenMenu'] && !pc_nbAAuth.contractMissing}"/> <%-- FNB016 NBA231 --%>
				</td>
			</tr>
			<%-- Begin NBA231 --%>
			<tr>
				<td class="menuItem"
					onmouseover="onSubMenuOver(this, document.all['actionMenuForm:reviewMenuItem']);"
					onmouseout="onSubMenuOut(this, document.all['actionMenuForm:reviewMenuItem']);">
					<h:commandLink value="Reg 60 Review"
						action="#{pc_actionMenu.processReg60Review}" styleClass="menuItem"
						onmouseover="this.isClicked=false;" id="reviewMenuItem"
						onmousedown="setTargetFrame();" immediate="true" rendered="#{pc_nbAAuth.auth.visibility['Reg60ReviewMenu'] &&  pc_nbAAuth.reg60 && !pc_nbAAuth.contractMissing}"/>   
				</td>
			</tr>
			<%-- End NBA231 --%>			
			<%-- Begin NBA254 --%>
			<tr>
				<td class="menuItem"
					onmouseover="onSubMenuOver(this, document.all['actionMenuForm:overrideMenuItem']);"
					onmouseout="onSubMenuOut(this, document.all['actionMenuForm:overrideMenuItem']);">
					<h:commandLink value="#{property.overrideautoclosure}"
						action="#{pc_actionMenu.processOverrideAutoClosure}" styleClass="menuItem"
						onmouseover="this.isClicked=false;" id="overrideMenuItem"
						onmousedown="setTargetFrame();" immediate="true" rendered="#{(pc_nbAAuth.standAlone || pc_nbAAuth.reg60) && !pc_nbAAuth.contractMissing}"/> <%-- NBA231 --%>
				</td>
			</tr>
			<%-- End NBA254 --%>
		</tbody>
	</TABLE>
	</div>
	<%-- begin NBA317 --%>
	<h:inputHidden id="cardNumber1Hidden" value=""></h:inputHidden>
	<h:inputHidden id="verifyCard1NumberHidden" value=""></h:inputHidden>
	<h:inputHidden id="cardNumber2Hidden" value=""></h:inputHidden>
	<%-- end NBA317 --%>
	<h:inputHidden id="lastactiveField" value=""></h:inputHidden>	<%-- SPRNBA-659 --%>
	<h:inputHidden id="lastFieldWithFocus" value=""></h:inputHidden>	<%-- SPRNBA-659 --%>
	<h:inputHidden id="closeToDoList" value="#{pc_nbAAuth.closeToDo}"></h:inputHidden>  <%-- SPRNBA-976 --%>
	</h:form>
</f:view>
</body>
</html>