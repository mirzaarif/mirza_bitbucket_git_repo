<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->
<!-- NBA215            8      wmA Integration -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="addUpdateIncreaseArea" styleClass="formDataEntryLine">
		<h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createTitle" value="#{property.contractChgAddIncreaesTitle}" rendered="#{pc_increase.createMode}" styleClass="shTextLarge" />
			<h:outputLabel id="updateRatingTitle" value="#{property.contractChgUpdateIncreaesTitle}" rendered="#{!pc_increase.createMode}"
				styleClass="shTextLarge" />
		</h:panelGroup>
		<!-- begin NBA215 -->
		<h:panelGroup id="increaseBaseGroup" styleClass="formDataEntryLine" rendered="#{pc_increase.renderIncreaseBase}">
			<h:outputText id="increaseBaseText" value="#{property.contractChgIncreaseBase}" styleClass="formLabel" style="width: 150px;" />			
			<h:selectBooleanCheckbox id="increaseBaseCB" value="#{pc_increase.currentObject.increaseBase}" 
				valueChangeListener="#{pc_increase.currentObject.changeIncreaseBaseCB}"	onclick="submit()" immediate="true" />			
		</h:panelGroup>
		<!-- end NBA215 -->
		<h:panelGroup id="coveragePGroup" styleClass="formDataEntryLine">
			<h:outputText id="coverageType" value="#{property.contractChCoverage}" styleClass="formLabel" style="width: 150px;" />
			<h:selectOneMenu id="coverageTypeDD" value="#{pc_increase.currentObject.productCodeCoverage}" rendered="#{!pc_increase.currentObject.increaseBase}"
				styleClass="formEntryText" style="width: 410px;">		<!-- NBA215 -->
				<f:selectItems id="coverageTypeList" value="#{pc_increase.coverageTypeList}" />
			</h:selectOneMenu>
			<h:outputText id="coverageTypeText" value="#{pc_increase.currentObject.productCodeCoverageText}" rendered="#{pc_increase.currentObject.increaseBase}"
				styleClass="formEntryText" style="width: 410px;" />	<!-- NBA215 -->
		</h:panelGroup>
		<h:panelGroup id="issuePGroup" styleClass="formDataEntryLine">
			<h:outputText id="issue" value="#{property.contractChIssue}" styleClass="formLabel" style="width: 150px;" />
			<h:inputText id="issueMontInpt" value="#{pc_increase.currentObject.issueMonthCoverage}" maxlength="2" styleClass="formEntryText" style="width: 90px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>

			<h:outputText id="issueMonth" value="#{property.contractChIssueMonth}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
			<h:inputText id="issueYearInpt" value="#{pc_increase.currentObject.issueYearCoverage}" styleClass="formEntryText" style="width: 90px;" maxlength="4">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputText id="issueYear" value="#{property.contractChIssueYear}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
		</h:panelGroup>
		<h:panelGroup id="rateClassPGroup" styleClass="formDataEntryLine">
			<h:outputText id="rateClass" value="#{property.contractChRateClass}" styleClass="formLabel" style="width: 150px;" />
			<h:selectOneMenu id="rateClassDD" value="#{pc_increase.currentObject.rateClass}" styleClass="formEntryTextFull" style="width: 90px;">
				<f:selectItems id="rateClassList" value="#{pc_increase.rateClassList}" />
			</h:selectOneMenu>
			<h:outputText id="units" value="#{property.contractChUnits}" styleClass="formLabel" style="width: 70px;" />
			<h:inputText id="unitsInpt" value="#{pc_increase.currentObject.unitsCoverage}" styleClass="formEntryText" style="width: 90px;" />
			<h:outputText id="signed" value="#{property.contractChSigned}" styleClass="formLabel" style="width: 70px;" />
			<h:inputText id="signedInpt" value="#{pc_increase.currentObject.signedDateCoverage}" styleClass="formEntryText" style="width: 90px;">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>
		<h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" action="#{pc_increase.actionCloseSubView}" />
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
				action="#{pc_increase.actionClear}" />
			<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_increase.actionAddNew}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_increase.actionAdd}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_increase.actionUpdate}"
				rendered="#{!pc_increase.createMode}" />
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
