<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->
<!-- SPRNBA-365        NB-1101   Insured's Name Field Populated from Impairment Search Should Be Expanded -->



<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
		<h:outputText value="#{property.appEntPage6Q3}" styleClass="formLabel" style="width: 345px;text-align: left;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup>
		<h:outputText value="" style="width: 100px;"></h:outputText>
		<h:commandButton id="cb1" action="#{pc_medicalInfo.impairmentSearch}" value="#{property.appEntImpSearch}" styleClass="ovAppEntryButton"
			style="width: 140px;" onclick="setTargetFrame();" disabled="#{!pc_medicalInfo.servicePresent}" />
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="width: 420px;padding-left: 2px;text-align: left;"> <!-- SPRNBA-365 -->
		<h:outputText value="#{property.appEntInsured}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText id="insuredNameText" value="#{pc_medicalInfo.insuredImpairmentInformation.insuredName}" styleClass="formEntryText"
			style="width: 230px;" disabled="#{!pc_medicalInfo.servicePresent}"></h:inputText> <!-- SPRNBA-365 -->
		<h:outputText value="" style="width: 10px;"></h:outputText>
		<h:selectBooleanCheckbox id="deletechkBoxInsured" value="#{pc_medicalInfo.insuredImpairmentInformation.deleteImpairment}"
			styleClass="ovFullCellSelectCheckBox" style="width: 15px;" disabled="#{pc_medicalInfo.updateMode || !pc_medicalInfo.servicePresent}" />
		<h:outputText value="#{property.appEntDelete}" styleClass="formLabel" style="width: 70px;text-align: center;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntImpairment}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:outputText value="#{pc_medicalInfo.insuredImpairmentInformation.impairmentName}" styleClass="formEntryText" style="width: 240px;"></h:outputText>
	</h:panelGroup>
	<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
		<h:outputText value="#{property.appEntDate}" styleClass="formLabel" style="width: 85px;"></h:outputText>
		<h:inputText value="#{pc_medicalInfo.insuredImpairmentInformation.impairmentDate}" styleClass="formEntryText" style="width: 130px;"
			disabled="#{!pc_medicalInfo.servicePresent}">
			<f:convertDateTime pattern="#{property.datePattern}" />
		</h:inputText>
	</h:panelGroup>
	<h:panelGroup id="jointOrOtherInsuredSection" rendered="#{pc_medicalInfo.jointOrOtherInsuredPresent || pc_jointOrOtherInsured.jointOrOtherInsuredPresent}">
		<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
			<h:outputText value="#{property.appEntInsured}" styleClass="formLabel" style="width: 85px;"></h:outputText>
			<h:inputText value="#{pc_medicalInfo.otherOrJointInsuredImpairmentInformation.insuredName}" styleClass="formEntryText" style="width: 150px;"
				disabled="#{!pc_medicalInfo.servicePresent}"></h:inputText>
			<h:outputText value="" style="width: 10px;"></h:outputText>
			<h:selectBooleanCheckbox id="deletechkBoxOtherOrJointInsured" value="#{pc_medicalInfo.otherOrJointInsuredImpairmentInformation.deleteImpairment}"
				styleClass="ovFullCellSelectCheckBox" style="width: 15px;" disabled="#{pc_medicalInfo.updateMode || !pc_medicalInfo.servicePresent}" />
			<h:outputText value="#{property.appEntDelete}" styleClass="formLabel" style="width: 70px;text-align: center;"></h:outputText>
		</h:panelGroup>
		<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
			<h:outputText value="#{property.appEntImpairment}" styleClass="formLabel" style="width: 85px;"></h:outputText>
			<h:outputText value="#{pc_medicalInfo.otherOrJointInsuredImpairmentInformation.impairmentName}" styleClass="formEntryText" style="width: 240px;"></h:outputText>
		</h:panelGroup>
		<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;text-align: left;">
			<h:outputText value="#{property.appEntDate}" styleClass="formLabel" style="width: 85px;"></h:outputText>
			<h:inputText value="#{pc_medicalInfo.otherOrJointInsuredImpairmentInformation.impairmentDate}" styleClass="formEntryText" style="width: 130px;"
				disabled="#{!pc_medicalInfo.servicePresent}">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
