<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="addUpdateIncreaseArea" styleClass="formDataEntryLine">
		<h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createTitle" value="#{property.contractChgAddAgentTitle}" rendered="#{pc_increase.createMode}" styleClass="shTextLarge" />
			<h:outputLabel id="updateRatingTitle" value="#{property.contractChgUpdateAgentTitle}" rendered="#{!pc_increase.createMode}" styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="producerPGroup" styleClass="formDataEntryLine">
			<h:outputText id="identification" value="#{property.contractChIdentification}" styleClass="formLabel" />
			<h:inputText id="identificationInpt" value="#{pc_increase.currentObject.companyProducerID}" styleClass="formEntryText" style="width: 120px;" />
			<h:outputText id="situation" value="#{property.contractChSituation}" styleClass="formLabel" rendered="#{pc_increase.currentObject.renderSituation}" style="width: 85px;" />
			<h:selectOneMenu id="situationTypeDD" value="#{pc_increase.currentObject.situationCode}" styleClass="formEntryText" rendered="#{pc_increase.currentObject.renderSituation}" style="width: 120px;">
				<f:selectItems id="situationTypeList" value="#{pc_increase.situationTypeList}" />
			</h:selectOneMenu>
			<h:outputText id="profile" value="#{property.contractChProfile}" styleClass="formLabel" rendered="#{!pc_increase.currentObject.renderSituation}" style="width: 85px;" />
			<h:inputText id="profileInpt" value="#{pc_increase.currentObject.profile}" styleClass="formEntryText" rendered="#{!pc_increase.currentObject.renderSituation}" style="width: 120px;" />
		</h:panelGroup>
		<h:panelGroup id="volumePGroup" styleClass="formDataEntryLine">
			<h:outputText id="volume" value="#{property.contractChVolume}" styleClass="formLabel" />
			<h:inputText id="volumeInpt" value="#{pc_increase.currentObject.volumeSharePct}" styleClass="formEntryText" style="width: 120px;" >
				<f:convertNumber type="percent"/>
			</h:inputText>			 
		</h:panelGroup>
		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>
		<h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" action="#{pc_increase.actionCloseSubView}" />
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
				action="#{pc_increase.actionClear}" />
			<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_increase.actionAddNew}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_increase.actionAdd}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_increase.actionUpdate}"
				rendered="#{!pc_increase.createMode}" />
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
