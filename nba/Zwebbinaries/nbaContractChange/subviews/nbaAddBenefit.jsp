<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->
<!-- NBA215            8      wmA Integration -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<h:panelGroup id="addUpdateIncreaseArea" styleClass="formDataEntryLine">
		<h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createTitle" value="#{property.contractChgAddBenefitTitle}" rendered="#{pc_increase.createMode}" styleClass="shTextLarge" />
			<h:outputLabel id="updateRatingTitle" value="#{property.contractChgUpdateBenefitTitle}" rendered="#{!pc_increase.createMode}"
				styleClass="shTextLarge" />
		</h:panelGroup>
		<h:panelGroup id="benefitPGroup" styleClass="formDataEntryLine">
			<h:outputText id="benefitType" value="#{property.contractChBenefit}" styleClass="formLabel" />
			<h:selectOneMenu id="benefitTypeDD" value="#{pc_increase.currentObject.productCodeCovOption}" styleClass="formEntryText" style="width: 150px;">	<!-- NBA215 -->
				<f:selectItems id="benefitTypeList" value="#{pc_increase.benefitTypeList}" />
			</h:selectOneMenu>
			<h:selectBooleanCheckbox value="#{pc_increase.currentObject.livesTypeCovOption}" style="margin-left: 30px;" />
			<h:outputLabel value="#{property.contractChJoint}" styleClass="formLabelRight" />
		</h:panelGroup>
		<h:panelGroup id="effectivePGroup" styleClass="formDataEntryLine">
			<h:outputText id="effective" value="#{property.contractChEffDate}" styleClass="formLabel" />
			<h:inputText id="effectiveMontInpt" value="#{pc_increase.currentObject.effMonthCovOption}" maxlength="2"  styleClass="formEntryText" style="width: 90px;" >	<!-- NBA215 -->
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputText id="effectiveMonth" value="#{property.contractChIssueMonth}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
			<h:inputText id="effectiveYearInpt" value="#{pc_increase.currentObject.effYearCovOption}" maxlength="4"  styleClass="formEntryText" style="width: 90px;" >	<!-- NBA215 -->
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputText id="effectiveYear" value="#{property.contractChIssueYear}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
		</h:panelGroup>
		<h:panelGroup id="ceasePGroup" styleClass="formDataEntryLine">
			<h:outputText id="cease" value="#{property.contractChCease}" styleClass="formLabel" />
			<h:inputText id="ceaseMontInpt" value="#{pc_increase.currentObject.termMonthCovOption}" maxlength="2" styleClass="formEntryText" style="width: 90px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>
			<h:outputText id="ceaseMonth" value="#{property.contractChIssueMonth}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
			<h:inputText id="ceaseYearInpt" value="#{pc_increase.currentObject.termYearCovOption}" maxlength="4" styleClass="formEntryText" style="width: 90px;">
				<f:convertNumber integerOnly="true" type="integer" />
			</h:inputText>	
			<h:outputText id="ceaseYear" value="#{property.contractChIssueYear}" styleClass="formLabel"
				style="width: 70px; text-align: left; padding-left: 10px;" />
			<h:outputText id="units1" value="#{property.contractChUnits}" styleClass="formLabel" style="width: 70px; margin-left: -20px;" />
			<h:inputText id="unitsInpt1" value="#{pc_increase.currentObject.unitsCovOption}" styleClass="formEntryText" style="width: 90px;" />
		</h:panelGroup>
		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>
		<h:panelGroup styleClass="formButtonBar">
			<h:commandButton id="btnCancel" value="#{property.buttonCancel}" styleClass="formButtonLeft" action="#{pc_increase.actionCloseSubView}" />
			<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft-1" onclick="resetTargetFrame();"
				action="#{pc_increase.actionClear}" />
			<h:commandButton id="btnAddNew" value="#{property.buttonAddNew}" styleClass="formButtonRight-1" action="#{pc_increase.actionAddNew}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnAdd" value="#{property.buttonAdd}" styleClass="formButtonRight" action="#{pc_increase.actionAdd}"
				rendered="#{pc_increase.createMode}" />
			<h:commandButton id="btnUpdate" value="#{property.buttonUpdate}" styleClass="formButtonRight" action="#{pc_increase.actionUpdate}"
				rendered="#{!pc_increase.createMode}" />
		</h:panelGroup>
	</h:panelGroup>
</jsp:root>
