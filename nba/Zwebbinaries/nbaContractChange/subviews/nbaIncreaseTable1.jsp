<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change Rewrite -->
<!-- NBA215			 8	  		wmA Integration  -->

<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">
	<f:loadBundle var="bundle" basename="com.csc.fs.accel.ui.config.ApplicationData" />
	<h:panelGroup styleClass="ovDivTableHeader" style="width: 100%;">
		<h:panelGrid columns="5" styleClass="ovTableHeader" columnClasses="ovColHdrText210,ovColHdrText50,ovColHdrText190,ovColHdrDate,ovColHdrDate"
			cellspacing="0">
			<h:outputLabel id="clientHdrCol1" value="#{property.uwCovClientCol1}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientHdrCol2" value="#{property.uwCovClientCol2}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientHdrCol3" value="#{property.uwCovClientCol3}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientHdrCol4" value="#{property.uwCovClientCol4}" styleClass="ovColSortedFalse" />
			<h:outputLabel id="clientHdrCol5" value="#{property.uwCovClientCol5}" styleClass="ovColSortedFalse" />
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup styleClass="ovDivTableData" style="height: 180px; width: 100%;">
		<h:dataTable id="client1Table" styleClass="ovTableData" cellspacing="0" rows="0" binding="#{pc_increase.increaseTable}"
			value="#{pc_increase.personIncreaseTableRowList}" var="nbaIncrease" rowClasses="#{pc_increase.rowStyles}"
			columnClasses="ovColText210,ovColText50,ovColText190,ovColDate,ovColDate">
			<h:column>
				<h:panelGroup styleClass="ovFullCellSelect" style="width: 100%;">
					<h:commandButton id="clientIcon1" image="images/hierarchies/T.gif" rendered="#{nbaIncrease.icon1Rendered}"
						styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" />
					<h:commandButton id="clientIcon2" image="images/hierarchies/L.gif" rendered="#{nbaIncrease.icon2Rendered}"
						styleClass="ovViewIconTrue" style="margin-left: 5px; margin-top: -6px;" />
					<h:commandLink id="clientCol1" value="#{nbaIncrease.col1}" style="vertical-align: top; color: #000000;"
						action="#{pc_increase.actionSelectIncreaseRow}" immediate="true" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:commandLink id="clientCol2" value="#{nbaIncrease.col2}" styleClass="ovFullCellSelect"
					action="#{pc_increase.actionSelectIncreaseRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientCol3" value="#{nbaIncrease.col3}" styleClass="ovFullCellSelect"
					action="#{pc_increase.actionSelectIncreaseRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientCol4" value="#{nbaIncrease.col4}" styleClass="ovFullCellSelect"
					action="#{pc_increase.actionSelectIncreaseRow}" immediate="true" />
			</h:column>
			<h:column>
				<h:commandLink id="clientCol5" value="#{nbaIncrease.col5}" styleClass="ovFullCellSelect"
					action="#{pc_increase.actionSelectIncreaseRow}" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<!-- Button bar -->
	<h:panelGroup styleClass="ovButtonBar" rendered="#{pc_increase.overView}">
		<h:commandButton id="btnCovClientDelete" value="#{property.buttonDelete}" styleClass="formButtonLeft"
			disabled="#{pc_increase.disableDelete}" action="#{pc_increase.actionDelete}"
			immediate="true" />
		<h:commandButton id="btnCovClientAmendEndorse" value="#{property.buttonView}" styleClass="formButtonRight-3" disabled="#{pc_increase.disableUpdate}"
			action="#{pc_increase.updateRow}" immediate="true" />
		<h:commandButton id="btnAddIncrease" value="#{property.buttonAddIncrease}" styleClass="formButtonRight-2" disabled="#{pc_increase.disableAddIncrease}"
			action="#{pc_increase.AddIncrease}" immediate="true" />
		<h:commandButton id="btnAddBenefit" value="#{property.buttonAddBenefit}" styleClass="formButtonRight-1" disabled="#{pc_increase.disableAddBenefit}"
			action="#{pc_increase.AddBenefit}" immediate="true" />	<!-- NBA215 -->
		<h:commandButton id="btnAddAgent" value="#{property.buttonAddAgent}" styleClass="formButtonRight" disabled="#{pc_increase.disableAddAgent}" action="#{pc_increase.AddAgent}"
			immediate="true" />
	</h:panelGroup>
</jsp:root>
