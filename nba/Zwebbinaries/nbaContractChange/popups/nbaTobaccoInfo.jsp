<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185			7	      Contract Change UI Rewrite -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
		<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<head>
			<base href="<%=basePath%>">
			<title><h:outputText value="#{property.appEntTobaccoInfo}" /></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">    
			<link  href="theme/accelerator.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
			<link href="theme/nbapopup.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="javascript/global/popupWindow.js"></script>
			<script language="JavaScript" type="text/javascript">
				var width=620;
				var height=200; 
				function setTargetFrame() {
					document.forms['form_UlTobaccoInfo'].target='controlFrame';
					return false;
				}
				function resetTargetFrame() {
					document.forms['form_UlTobaccoInfo'].target='';
					return false;
				}
			</script>
		</head>
		<body onload="popupInit();" style="overflow-x: hidden; overflow-y: scroll">
		<h:form id="form_UlTobaccoInfo" style="height:200px;">
			<h:panelGrid id="pinsTobaccoPGrid" columns="2" styleClass="formDataEntryLine" columnClasses="colOwnerInfo,colMoreCodes" cellpadding="0" cellspacing="0" style="width: 595px;" border="0">
				<h:column>
					<h:dataTable id="tobacco" cellspacing="0" cellpadding="0" rows="0" value="#{pc_medicalInfo.tobaccoInfoList}" var="tobacco" style="width: 530px;" border="0">
						<h:column>
							<h:panelGroup id="tobaccoInformationTitle" styleClass="formDataEntryLine">
								<h:outputText value="#{property.appEntType}" styleClass="entryLabel" style="width: 120px;"/>
								<h:selectOneMenu id="typeDD" value="#{tobacco.type}" styleClass="formEntryTextFull" style="width: 150px;">
									<f:selectItems value="#{pc_medicalInfo.typeList}" />
								</h:selectOneMenu>
								<h:outputText id="Qty" value="#{property.appEntQuantity}" styleClass="entryLabel" style="width: 70px;"/>
								<h:inputText id="QtyEF" value="#{tobacco.substanceAmt}" styleClass="entryFieldDate" style="width: 30px;"/>
 								<h:outputText value="#{property.appEntFrequency}" styleClass="entryLabel" style="width: 80px;"/>
								<h:selectOneMenu id="frequencyDD" value="#{tobacco.frequency}" styleClass="formEntryTextFull" style="width: 75px;">
									<f:selectItems value="#{pc_medicalInfo.freqList}" />
								</h:selectOneMenu>
							</h:panelGroup>
							<h:panelGroup styleClass="formDataEntryLine">
								<h:outputText id="yrs" value="#{property.appEntNoOfYrs}" styleClass="entryLabel" style="width: 120px;"/>
								<h:inputText id="yrsEF" value="#{tobacco.years}" styleClass="formEntryText" style="width: 30px;"/>
								<h:outputText id="date" value="#{property.appEntLastUsedDate}" styleClass="entryLabel" style="width: 110px;"/>
								<h:inputText id="dateEF" value="#{tobacco.lastDate}" styleClass="entryFieldDate" style="width: 90px;">
									<f:convertDateTime pattern="#{property.datePattern}"/>
								</h:inputText>
							</h:panelGroup>
						</h:column>
					</h:dataTable>
				</h:column>
				<h:column>
					<h:commandLink value="#{property.buttonAddAnother}" styleClass="formButtonInterface" action="#{pc_medicalInfo.addAnotherInfo}" style="width: 65px;"/>
				</h:column>
			</h:panelGrid>
			<h:panelGroup styleClass="buttonBar">
					<h:commandButton id="tobaccoCancel" value="#{property.buttonCancel}"  immediate="true" action="#{pc_medicalInfo.cancel}" styleClass="buttonLeft" onclick="setTargetFrame()"/>
					<h:commandButton id="tobaccoClear" value="#{property.buttonClear}" immediate="true"  styleClass="buttonLeft-1" action="#{pc_medicalInfo.clear}" onclick="resetTargetFrame()"/>
					<h:commandButton id="employerAdd" value="#{property.buttonAdd}" action="#{pc_medicalInfo.add}" styleClass="buttonRight" onclick="setTargetFrame()"/>
			</h:panelGroup>
		</h:form>
	</f:view>
</body>
</html>