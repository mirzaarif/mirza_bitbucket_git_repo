<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->
<!-- NBA213			   7	  Unified User Interface  -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
					
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaContractChange/nbaMedicalInfo.faces';

		function setTargetFrame() {		
			document.forms['form_contractChMedicalInfo'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_contractChMedicalInfo'].target='';
			return false;
		}

		function toggleCBGroup(selectedCB) {
			if (document.forms['form_contractChMedicalInfo']['form_contractChMedicalInfo:' + selectedCB].checked == true) {
				var temp;	
				if (selectedCB.indexOf('Yes') != -1) {
					temp = selectedCB.substring(0, selectedCB.indexOf('Yes'));
					document.forms['form_contractChMedicalInfo']['form_contractChMedicalInfo:' + temp + 'No'].value=false;
					document.forms['form_contractChMedicalInfo']['form_contractChMedicalInfo:' + temp + 'No'].checked=false;
				} else {
					temp = selectedCB.substring(0, selectedCB.indexOf('No'));
					document.forms['form_contractChMedicalInfo']['form_contractChMedicalInfo:' + temp + 'Yes'].value=false;
					document.forms['form_contractChMedicalInfo']['form_contractChMedicalInfo:' + temp + 'Yes'].checked=false;
				}
			}
		}
</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_MEDICAL_INFORMATION" value="#{pc_medicalInfo}" />
	<h:form id="form_contractChMedicalInfo">
		<DIV class="inputFormMat">
		<DIV class="inputForm" style="height: auto; width: 100%"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createCCTitle" value="#{property.contractChgMedicalTitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGrid id="GeneralInfoQuestions" columns="#{pc_medicalInfo.noOfColumns}" cellpadding="1" border="1" cellspacing="0" styleClass="formDataEntryLine"
			style="margin-top: 10px;margin-bottom: 10px;">
			<h:outputText id="questionCol" value="#{property.appEntQuestion}" styleClass="formLabel" style="width: 335px;font-weight: bold;text-align: center;" />
			<h:outputText id="insCol" value="#{property.appEntPrimaryIns}" styleClass="formLabel" style="width: 125px;font-weight: bold;text-align: center;" />
			<h:outputText id="otherInsCol" value="#{property.appEntOthIns}" rendered="#{pc_medicalInfo.othInsRenderer}" styleClass="formLabel"
				style="width: 125px;font-weight: bold;text-align: center;" />
			<h:outputText id="jointInsCol" value="#{property.appEntJntIns}" rendered="#{pc_medicalInfo.jntInsRenderer}" styleClass="formLabel"
				style="width: 125px;font-weight: bold;text-align: center;" />
			<h:panelGroup id="q1PG" styleClass="formDataEntryLine" style="padding-left: 2px;">
				<h:outputText id="q1" value="#{property.appEntPage5Q6}" styleClass="formLabel" style="width: 335px;text-align: left;" />
			</h:panelGroup>
			<h:panelGroup id="pinsQ1YesNoPG" style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q1CB1Yes" value="#{pc_medicalInfo.tobaccoIndPriYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;"
					onclick="toggleCBGroup('q1CB1Yes');" />
				<h:outputText id="pinsQ1Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
				<h:selectBooleanCheckbox id="q1CB1No" value="#{pc_medicalInfo.tobaccoIndPriNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px;"
					onclick="toggleCBGroup('q1CB1No');" />
				<h:outputText id="pinsQ1No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;" />
				<h:commandButton id="tobaccoInfo" image="images/link_icons/circle_i.gif" action="#{pc_medicalInfo.tobaccoInfoPriIns}" onclick="setTargetFrame();" />
			</h:panelGroup>
			<h:panelGroup id="otherInsQ1YesNoPG" rendered="#{pc_medicalInfo.othInsRenderer || pc_medicalInfo.jntInsRenderer}" style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q1CB2Yes" value="#{pc_medicalInfo.tobaccoIndOthYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q1CB2Yes');" />
				<h:outputText id="otherInsQ1Yes" value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;" />
				<h:selectBooleanCheckbox id="q1CB2No" value="#{pc_medicalInfo.tobaccoIndOthNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q1CB2No');" />
				<h:outputText id="otherInsQ1No" value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;" />
				<h:commandButton image="images/link_icons/circle_i.gif" action="#{pc_medicalInfo.tobaccoInfoOthIns}" onclick="setTargetFrame();" />
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="width:335px;" rendered="#{!pc_medicalInfo.updateMode}">
				<f:subview id="impairmentSection">
					<c:import url="/nbaContractChange/subviews/nbaImpairmentInfo.jsp" />
				</f:subview>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;" rendered="#{pc_medicalInfo.updateMode}">
				<h:outputText value="#{property.appEntPage6Q3}" styleClass="formLabel" style="width: 335px;text-align: left;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q2CB1Yes" value="#{pc_medicalInfo.q1PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q2CB1Yes');" />
				<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
				<h:selectBooleanCheckbox id="q2CB1No" value="#{pc_medicalInfo.q1PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q2CB1No');" />
				<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup rendered="#{pc_medicalInfo.othInsRenderer || pc_medicalInfo.jntInsRenderer}" styleClass="formDataEntryLine"
				style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q2CB2Yes" value="#{pc_medicalInfo.q1OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q2CB2Yes');" />
				<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
				<h:selectBooleanCheckbox id="q2CB2No" value="#{pc_medicalInfo.q1OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q2CB2No');" />
				<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
				<h:outputText value="#{property.appEntPage6Q5}" styleClass="formLabel" style="width: 335px;text-align: left;padding-bottom:4px;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q3CB1Yes" value="#{pc_medicalInfo.q2PriInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q3CB1Yes')" />
				<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
				<h:selectBooleanCheckbox id="q3CB1No" value="#{pc_medicalInfo.q2PriInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q3CB1No');" />
				<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup rendered="#{pc_medicalInfo.othInsRenderer || pc_medicalInfo.jntInsRenderer}" styleClass="formDataEntryLine"
				style="padding-left: 2px;">
				<h:selectBooleanCheckbox id="q3CB2Yes" value="#{pc_medicalInfo.q2OthInsYes}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q3CB2Yes');" />
				<h:outputText value="#{property.appEntYes}" styleClass="formLabel" style="width: 40px;text-align: center;"></h:outputText>
				<h:selectBooleanCheckbox id="q3CB2No" value="#{pc_medicalInfo.q2OthInsNo}" styleClass="ovFullCellSelectCheckBox" style="width: 15px"
					onclick="toggleCBGroup('q3CB2No');" />
				<h:outputText value="#{property.appEntNo}" styleClass="formLabel" style="width: 30px;text-align: right;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine">
				<h:panelGrid columns="1">
					<h:outputText value="#{property.appEntHeartDisorder}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntHeartDisease}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntAngina}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntStroke}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntHypertension}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntHBP}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntDiabetes}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
					<h:outputText value="#{property.appEntCancer}" styleClass="formLabel" style="width:280px;height:18px;text-align: left;padding-left: 40px;"></h:outputText>
				</h:panelGrid>
			</h:panelGroup>
			<h:panelGroup styleClass="formDataEntryLine" style="text-align: center;">
				<h:panelGrid columns="1">
					<h:selectBooleanCheckbox id="sb1" value="#{pc_medicalInfo.heartDisOrderPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb2" value="#{pc_medicalInfo.heartDiseaserPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb3" value="#{pc_medicalInfo.anginaPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb4" value="#{pc_medicalInfo.strokePriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb5" value="#{pc_medicalInfo.hypertensionPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb6" value="#{pc_medicalInfo.highBPPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb7" value="#{pc_medicalInfo.diabetesPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb8" value="#{pc_medicalInfo.cancerPriIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
				</h:panelGrid>
			</h:panelGroup>
			<h:panelGroup rendered="#{pc_medicalInfo.othInsRenderer || pc_medicalInfo.jntInsRenderer}" styleClass="formDataEntryLine"
				style="text-align: center;">
				<h:panelGrid columns="1">
					<h:selectBooleanCheckbox id="sb9" value="#{pc_medicalInfo.heartDisOrderOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb10" value="#{pc_medicalInfo.heartDiseaserOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb11" value="#{pc_medicalInfo.anginaOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb12" value="#{pc_medicalInfo.strokeOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb13" value="#{pc_medicalInfo.hypertensionOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb14" value="#{pc_medicalInfo.highBPOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb15" value="#{pc_medicalInfo.diabetesOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
					<h:selectBooleanCheckbox id="sb16" value="#{pc_medicalInfo.cancerOthIns}" styleClass="ovFullCellSelectCheckBox" style="width: 15px" />
				</h:panelGrid>
			</h:panelGroup>
		</h:panelGrid></DIV>
		</DIV>
		<f:subview id="nbaCommentBar" rendered="#{!pc_ccNavigation.openFromAction}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnBack" value="#{property.buttonBack}" disabled="#{pc_ccNavigation.disablePrevious}" styleClass="formButtonLeft" action="#{pc_medicalInfo.back}" />
			<h:commandButton id="btnSave" value="#{property.buttonSave}" action="#{pc_medicalInfo.save}" 
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				styleClass="formButtonRight-1" /> <!-- NBA213 -->
			<h:commandButton id="btnContinue" value="#{property.buttonContinue}" action="#{pc_medicalInfo.nextView}" styleClass="formButtonRight"
				rendered="#{!pc_ccNavigation.renderCommit}" />
			<h:commandButton id="btnCommit" value="#{property.buttonComplete}" action="#{pc_medicalInfo.commit}" styleClass="formButtonRight"
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				rendered="#{pc_ccNavigation.renderCommit}" onclick="setTargetFrame();"/> <!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>

</html>

