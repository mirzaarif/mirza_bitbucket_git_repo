<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185           7       Contract Change UI Rewrite -->

<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme()	+ "://"	+ request.getServerName() + ":"	+ request.getServerPort()+ path + "/";
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Contract Change File</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		<!--
			var context = '<%=path%>';
			var topOffset = -1;		
			var leftOffset = 5;
			var maxTabsPerRow = 8;

			function initFileSize() {
				winHeight = window.screen.availHeight;
				tabHeight = winHeight - 150;
				document.getElementById('file').height = tabHeight;
			}
		//-->
	</script>
	<script type="text/javascript" src="javascript/global/desktopComponent.js"></script>
</head>
<body class="desktopBody" onload="top.hideWait();" style="overflow-x: hidden; overflow-y: hidden">
	<f:view>
		<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr style="height:20px;" class="desktopBody">
					<td></td>
				</tr>
				<tr style="height:5px;" class="textMainTitleBar">
					<td></td>
				</tr>
				<tr style="height:*; vertical-align: top;" class="textMainTitleBar">
					<td><iframe id="file" name="file" src="faces/nbaContractChange/nbaContractChange.jsp?OPENTYPE=ACTION"
							 height="100%" width="100%" frameborder="0" scrolling="auto" onload="initFileSize();"></iframe></td>
				</tr>
			</tbody>
		</table>
	</f:view>
</body>
</html>
