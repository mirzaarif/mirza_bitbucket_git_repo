<!-- CHANGE LOG -->
<!-- Audit Number   Version   	Change Description -->
<!-- NBA185			 7        	Contract Change UI Rewrite -->
<!-- NBA213			 7	  		Unified User Interface  -->
<!-- NBA215			 8	  		wmA Integration  -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>
<script type="text/javascript" src="javascript/nbaimages.js"></script>
<script language="JavaScript" type="text/javascript">
				
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaContractChange/nbaIncrease.faces';	
		function setTargetFrame() {		
			document.forms['form_Increase'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_Increase'].target='';
			return false;
		}
	
	</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: hidden">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_INCREASE_INFORMATION" value="#{pc_increase}" />
	<h:form id="form_Increase">
		<DIV class="inputFormMat">
		<DIV class="inputForm" style="height: auto"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="IncreaseTitle" value="#{property.contractChgdIncreaesTitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGroup styleClass="sectionHeader">
			<h:selectOneMenu value="#{pc_increase.insuredID}" onchange="submit()" valueChangeListener="#{pc_increase.changeInsuredDD}"
				styleClass="shMenu" style="position: relative; left: 5px; width: 300px">	<!-- NBA215 -->
				<f:selectItems value="#{pc_increase.insuredClients}" />
			</h:selectOneMenu>
			<!-- NBA215 code deleted -->
			<h:outputText value="#{pc_increase.insuredGender}" styleClass="shText" style="position: absolute; left: 330px; width: 50px; text-align: center" />
			<h:outputText value="#{property.birthDate}" styleClass="shText" style="position: absolute; left: 422px" />
			<h:outputText value="#{pc_increase.insuredBirthDate}" styleClass="shText" style="position: absolute; left: 462px">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:outputText>
			<h:outputFormat value="{0} {1}" styleClass="shText" style="position: absolute; right: 15px">
				<f:param value="#{property.age}" />
				<f:param value="#{pc_increase.insuredAge}" />
			</h:outputFormat>
		</h:panelGroup> <h:panelGroup style="width: 100%">
			<f:subview id="increaseTable1">
				<c:import url="/nbaContractChange/subviews/nbaIncreaseTable1.jsp" />
			</f:subview>
			<f:subview id="addIncrease" rendered="#{pc_increase.renderIncrease}">
				<c:import url="/nbaContractChange/subviews/nbaAddIncrease.jsp" />
			</f:subview>
			<f:subview id="addBenefit" rendered="#{pc_increase.renderBenefit}">
				<c:import url="/nbaContractChange/subviews/nbaAddBenefit.jsp" />
			</f:subview>
			<f:subview id="addAgent" rendered="#{pc_increase.renderAgent}">
				<c:import url="/nbaContractChange/subviews/nbaAddAgent.jsp" />
			</f:subview>
		</h:panelGroup></DIV>
		</DIV>
		<f:subview id="nbaCommentBar" rendered="#{!pc_ccNavigation.openFromAction}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnBack" value="#{property.buttonBack}" styleClass="formButtonLeft" action="#{pc_increase.back}" />
			<h:commandButton id="btnSave" value="#{property.buttonSave}" 
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				action="#{pc_increase.save}" styleClass="formButtonRight-1" />	<!-- NBA213 -->
			<h:commandButton id="btnContinue" value="#{property.buttonContinue}" action="#{pc_increase.nextView}" styleClass="formButtonRight"
				rendered="#{!pc_ccNavigation.renderCommit}" />
			<h:commandButton id="btnCommit" value="#{property.buttonComplete}" action="#{pc_increase.commit}" styleClass="formButtonRight"
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				rendered="#{pc_ccNavigation.renderCommit}" onclick="setTargetFrame();" />	<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>

</html>
