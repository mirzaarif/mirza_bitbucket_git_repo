<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185           7       	Contract Change UI Rewrite -->
<!-- NBA213			  7	  		Unified User Interface  -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>

<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaContractChange/nbaReinstatement.faces';	
		function setTargetFrame() {		
			document.forms['form_reinstatement'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_reinstatement'].target='';
			return false;
		}
		
</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_REINSTATEMENT_INFORMATION" value="#{pc_reinstatement}" />
	<h:form id="form_reinstatement">
		<DIV class="inputFormMat" style="height: auto">
		<DIV class="inputForm" style="height: auto"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="reinstatementTitle" value="#{property.contractChReinstatementTitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGroup id="loanPGroup" styleClass="formDataEntryTopLine">
			<h:selectBooleanCheckbox id="loanPresentCheck" value="#{pc_reinstatement.loanPresent}" disabled="true" style="margin-left: 146px;" />
			<h:outputLabel id="loanPresentLbl" value="#{property.contractChChangeLoanPresent}" styleClass="formLabelRight" />
		</h:panelGroup> <h:panelGroup id="reinstatePGroup" styleClass="formDataEntryLine">
			<h:selectBooleanCheckbox id="reinstateLoanCheck" value="#{pc_reinstatement.reinstateLoan}" disabled="#{pc_reinstatement.disableReinstateLoan}"
				style="margin-left: 146px;" />
			<h:outputLabel id="reinstateLoanLbl" value="#{property.contractChReinstateLoan}" styleClass="formLabelRight" />
		</h:panelGroup> <h:panelGroup id="effDatePGroup" styleClass="formDataEntryLine">
			<h:outputText id="reinstatementDate" value="#{property.contractChReinstateDate}" rendered="#{pc_reinstatement.renderReinstatementDate}"
				styleClass="formLabel" style="width: 150px;" />
			<h:outputText id="asOfDate" value="#{property.contractChAsOfDate}" rendered="#{!pc_reinstatement.renderReinstatementDate}" styleClass="formLabel"
				style="width: 150px;" />
			<h:inputText id="effDateEF" value="#{pc_reinstatement.reinstateDate}" styleClass="formEntryText" style="width: 90px;">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup> <h:panelGroup id="paymentDateGroup" styleClass="formDataEntryLine" rendered="#{pc_reinstatement.renderNewDate}">
			<h:outputText id="paymentDate" value="#{property.contractChPaymentDate}" styleClass="formLabel" style="width: 150px;" />
			<h:inputText id="paymentDateEF" value="#{pc_reinstatement.paymentDueDate}" styleClass="formEntryText" style="width: 90px;">
				<f:convertDateTime pattern="#{property.datePattern}" />
			</h:inputText>
		</h:panelGroup></DIV>
		</DIV>
		<f:subview id="nbaCommentBar" rendered="#{!pc_ccNavigation.openFromAction}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnBack" value="#{property.buttonBack}" styleClass="formButtonLeft" action="#{pc_reinstatement.back}" />
			<h:commandButton id="btnSave" value="#{property.buttonSave}" action="#{pc_reinstatement.save}" 
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				styleClass="formButtonRight-1" />	<!-- NBA213 -->
			<h:commandButton id="btnContinue" value="#{property.buttonContinue}" action="#{pc_reinstatement.nextView}" styleClass="formButtonRight"
				rendered="#{!pc_ccNavigation.renderCommit}" />
			<h:commandButton id="btnCommit" value="#{property.buttonComplete}" action="#{pc_reinstatement.commit}" styleClass="formButtonRight"
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				rendered="#{pc_ccNavigation.renderCommit}" onclick="setTargetFrame();"/>	<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>
