<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185           7       Contract Change UI Rewrite -->

<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Close Dialog</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="css/accelerator.css">
<script type="text/javascript">
		window.opener = parent;
		parent.closeWindow();
		parent.hideWait();
		if(!parent.windowsOpen()){
			parent.refreshDT();
		}
		function setTargetFrame() {		
			document.forms['form_closeContractChange'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_closeContractChange'].target='';
			return false;
		}
		function closeContractChange() {
			document.getElementById('form_closeContractChange:closeDialog').click();
		}
	</script>
</head>
<body onload="closeContractChange();">
	CLOSE DIALOG
<f:view>
	<h:form id="form_closeContractChange">
		<h:commandButton id="closeDialog" style="display:none" action="#{pc_ccNavigation.closeCCDialog}" onclick="setTargetFrame();" />
	</h:form>
	<div id="Messages"><h:messages /></div>
</f:view>
<div id="MessageTitle">Information</div>
<script type="text/javascript">
		try{
		     //Begin SPR3356	
   		    var innerText=document.all["Messages"].innerHTML;
		    innerText=top.getInnerHTML(innerText);		    
		    document.all["Messages"].innerHTML= innerText; 
	        //End SPR3356 
			if(document.all["Messages"].innerHTML != null && document.all["Messages"].innerHTML != ""){
				parent.showWindow('<%=path%>','error.jsp', this);
			}
		} catch(err){
		}
	</script>
</body>
</html>
