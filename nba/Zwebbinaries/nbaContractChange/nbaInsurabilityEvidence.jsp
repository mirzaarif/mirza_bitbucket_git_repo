<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185            7      Contract Change UI Rewrite -->
<!-- NBA213			   7	  Unified User Interface  -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->

<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script language="JavaScript" type="text/javascript">
			
		var contextpath = '<%=path%>';	//FNB011
		
		var fileLocationHRef  = '<%=basePath%>' + 'nbaContractChange/nbaInsurabilityEvidence.faces';
		function setTargetFrame() {		
			document.forms['form_InsurabilityEvidence'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			document.forms['form_InsurabilityEvidence'].target='';
			return false;
		}
		
</script>
</head>
<body onload="filePageInit();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_INSURABILITY_EVIDENCE" value="#{pc_insurability}" />
	<h:form id="form_InsurabilityEvidence">
		<DIV class="inputFormMat" style="height: 170px;">
		<DIV class="inputForm" style="height: auto; width: 100%"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="InsurabilityTitle" value="#{property.contractChgInsurabilityTitle}" styleClass="shTextLarge" />
		</h:panelGroup> <h:panelGrid id="GeneralInfoQuestions" columns="#{pc_insurability.noOfColumns}" cellpadding="1" border="1" cellspacing="0" styleClass="formDataEntryLine"
			style="margin-top: 10px;margin-bottom: 10px;">
			<h:outputText id="questionCol" value="" styleClass="formLabel" style="width: 200x;font-weight: bold;text-align: center;" />
			<h:outputText id="insCol" value="#{property.contractChPrimaryIns}" styleClass="formLabel"
				style="width: 180px;font-weight: bold;text-align: center;" />
			<h:outputText id="otherInsCol" value="#{property.contractChOthIns}" rendered="#{pc_insurability.othInsRenderer}" styleClass="formLabel"
				style="width: 180px;font-weight: bold;text-align: center;" />
			<h:outputText id="jointInsCol" value="#{property.contractChJntIns}" rendered="#{pc_insurability.jntInsRenderer}" styleClass="formLabel"
				style="width: 180px;font-weight: bold;text-align: center;" />
			<h:panelGroup id="q6PG" styleClass="formDataEntryLine" style="height: 40px; padding-left: 2px;">
				<h:outputText id="q6" value="#{property.contractChOccupation}" styleClass="formLabel" style="width: 200px;text-align: center;" />
			</h:panelGroup>
			<h:panelGroup id="pinsQ6YesNoPG" style="padding-left: 2px;">
				<h:selectOneMenu id="insOccDD" value="#{pc_insurability.primaryInsOccupation}" styleClass="formEntryText" style="width: 180px;">
					<f:selectItems id="insOccDDList" value="#{pc_insurability.occupationList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="otherInsQ6YesNoPG" rendered="#{pc_insurability.othInsRenderer || pc_insurability.jntInsRenderer}" style="padding-left: 2px;">
				<h:selectOneMenu id="othOccDD" value="#{pc_insurability.otherInsOccupaton}" styleClass="formEntryText" style="width: 180px;">
					<f:selectItems id="othOccDDList" value="#{pc_insurability.occupationList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="Income" styleClass="formDataEntryLine" style="height: 40px; padding-left: 2px;">
				<h:outputText value="#{property.contractChAnnualInc}" styleClass="formLabel" style="width: 200px;text-align: center;"></h:outputText>
			</h:panelGroup>
			<h:panelGroup id="InsIncome" style="padding-left: 2px;">
				<h:inputText id="insIncomeInpt" value="#{pc_insurability.primaryInsSalary}" styleClass="formEntryText" style="width: 180px;">
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
				</h:inputText>
			</h:panelGroup>
			<h:panelGroup id="OthIncome" rendered="#{pc_insurability.othInsRenderer || pc_insurability.jntInsRenderer}" style="padding-left: 2px;">
				<h:inputText id="OthIncomeInpt" value="#{pc_insurability.othInsSalary}" styleClass="formEntryText" style="width: 180px;">
					<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" />
				</h:inputText>
			</h:panelGroup>
		</h:panelGrid></DIV>
		</DIV>
		<f:subview id="nbaCommentBar" rendered="#{!pc_ccNavigation.openFromAction}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnBack" value="#{property.buttonBack}"  disabled="#{pc_ccNavigation.disablePrevious}" styleClass="formButtonLeft" action="#{pc_insurability.back}" />
			<h:commandButton id="btnSave" value="#{property.buttonSave}" action="#{pc_insurability.save}" 
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				styleClass="formButtonRight-1" /> <!-- NBA213 -->
			<h:commandButton id="btnContinue" value="#{property.buttonContinue}" action="#{pc_insurability.nextView}" styleClass="formButtonRight"
				rendered="#{!pc_ccNavigation.renderCommit}" />
			<h:commandButton id="btnCommit" value="#{property.buttonComplete}" action="#{pc_insurability.commit}" styleClass="formButtonRight"
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				rendered="#{pc_ccNavigation.renderCommit}" onclick="setTargetFrame();"/>	<!-- NBA213 -->
		</h:panelGroup>
	</h:form>
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>
</html>

