
<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA185           7       Contract Change UI Rewrite -->
<!-- FNB011 				NB-1101	Work Tracking -->
<!-- SPR3746        NB-1101	  Unable to Change Reissue Sequence of Views By Removing All Rows For A Specific Change Type From NBA_CHANGETYPE_VIEWS  -->
<!-- SPR3715        NB-1301	  Modify Logic to Open Image Viewer in NbaBeanBase to Minimum Elemental Data  -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<%@ page language="java"%>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean"%>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help"%>
<%String path = request.getContextPath();
            String basePath = "";
            if (request.getServerPort() == 80) {
                basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
            } else {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/global/file.js"></script>
<script type="text/javascript" src="javascript/nbapopup.js"></script>
<script type="text/javascript" src="javascript/global/desktopManagement.js"></script>
<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
<script type="text/javascript" src="javascript/nbaimages.js"></script>
	<script type="text/javascript">
		<!--
								
			var contextpath = '<%=path%>';	//FNB011		
			//SPR3715  Code deleted	
		
			function viewImages() {
				//begin SPR3715
				var ImagePresent= document.getElementById('form_contractChange:images').value;
				if (ImagePresent == "true") {		
        			top.controlFrame.location.href = "../launchImage.faces";
        			document.getElementById('form_contractChange:images').value = "false";
    			}
    			//End SPR3715
			}
			
			function setTargetFrame() {		
				document.forms['form_contractChange'].target='controlFrame';
				return false;
			}
			function resetTargetFrame() {
				document.forms['form_contractChange'].target='';
				return false;
			}
			function enableButton() {					
				if(document.getElementById('form_contractChange:contractIpt').value!= "" && document.getElementById('form_contractChange:companyTypeDD').value!="-1") {
					document.getElementById('form_contractChange:btnContinue').disabled = false;				
				}else {
					document.getElementById('form_contractChange:btnContinue').disabled = true;				
				}
			}
			function enableButtonOnPlan() {	
				if(document.getElementById('form_contractChange:changeTypeDD1').value!= "-1" && 
				(document.getElementById('form_contractChange:planTypeDD') == null || document.getElementById('form_contractChange:planTypeDD').value!="-1")){
					document.getElementById('form_contractChange:btnContinue').disabled = false;				
				} else {
					document.getElementById('form_contractChange:btnContinue').disabled = true;				
				}
			}
			function submitForm() {
				document.forms['form_contractChange'].submit();
			}
			
		//-->
	</script>

</head>
<!-- NBA213 Common Tab Style -->
<body class="whiteBody" onload="filePageInit();viewImages();" style="overflow-x: hidden; overflow-y: scroll">
<f:view>
	<f:loadBundle basename="properties.nbaApplicationData" var="property" />
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACT_CHANGE" value="#{pc_ccNavigation}" />
	<PopulateBean:Load serviceName="RETRIEVE_CONTRACT_CHANGE" value="#{pc_contractChange}" />
	<h:form id="form_contractChange">
		<DIV class="inputFormMat" >
		<h:inputHidden id="images" value="#{pc_contractChange.imagesPresent}" /> <!--SPR3715  -->
		<DIV class="inputForm" style="height: 240px"><h:panelGroup styleClass="formTitleBar">
			<h:outputLabel id="createCCTitle" value="#{property.contractChTitle}" styleClass="shTextLarge" />
		</h:panelGroup><h:panelGroup id="blankGroup" styleClass="formDataEntryLine" style="height: 10px;" >
			<h:outputText id="blankText" value="" style="display:none"/>
		</h:panelGroup>
		 <h:panelGroup id="companyGroup" styleClass="formDataEntryLine" >
			<h:outputText id="company" value="#{property.contractChCompany}" styleClass="formLabel" />
			<h:selectOneMenu id="companyTypeDD" value="#{pc_contractChange.company}" onchange="enableButton()"
				rendered="#{!pc_contractChange.disableCmpContract}" styleClass="formEntryText"
				style="width: 350px;">
				<f:selectItems id="companyTypeList" value="#{pc_contractChange.companyList}" />
			</h:selectOneMenu>
			<h:outputText id="companyTypeDDText" value="#{pc_contractChange.companyText}" rendered="#{pc_contractChange.disableCmpContract}"
				styleClass="formEntryText" style="width: 350px;" />
		</h:panelGroup> <h:panelGroup id="contractGroup" styleClass="formDataEntryLine">
			<h:outputText id="contract" value="#{property.contractChContract}" styleClass="formLabel" />
			<h:inputText id="contractIpt" value="#{pc_contractChange.contractNo}" onkeyup="enableButton();"
				rendered="#{!pc_contractChange.disableCmpContract}" styleClass="formEntryText"
				style="width: 350px;"/>
			<h:outputText id="contractIptText" value="#{pc_contractChange.contractNo}" rendered="#{pc_contractChange.disableCmpContract}"
				styleClass="formEntryText" style="width: 350px;" />
		</h:panelGroup> <h:panelGroup id="planContractChgType" styleClass="formDataEntryLine" rendered="#{pc_contractChange.renderPlanChgType}">
			<h:panelGroup id="planGroup" styleClass="formDataEntryLine">
				<h:outputText id="plan" value="#{property.contractChPlan}" styleClass="formLabel" />
				<h:selectOneMenu id="planTypeDD" value="#{pc_contractChange.plan}" onchange="enableButtonOnPlan()" 
					rendered="#{!pc_contractChange.disablePlan}" styleClass="formEntryText" style="width: 350px;">
					<f:selectItems id="planTypeList" value="#{pc_contractChange.planList}" />
				</h:selectOneMenu>
				<h:outputText id="planTypeDDText" value="#{pc_contractChange.planText}" rendered="#{pc_contractChange.disablePlan}" styleClass="formEntryText"
					style="width: 350px;" />
			</h:panelGroup>
			<h:panelGroup id="applicationDateGroup" styleClass="formDataEntryLine">
				<h:outputText id="appDate" value="#{property.contractChApplDate}" styleClass="formLabel" />
				<h:inputText id="appDateIpt" value="#{pc_contractChange.appDate}" rendered="#{!pc_contractChange.disablePlan}" styleClass="formEntryText"
					style="width: 90px;">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:inputText>
				<h:outputText id="appDateIptText" value="#{pc_contractChange.appDate}" rendered="#{pc_contractChange.disablePlan}" styleClass="formEntryText"
					style="width: 90px;">
					<f:convertDateTime pattern="#{property.datePattern}" />
				</h:outputText>
			</h:panelGroup>
			<h:panelGroup id="changeTypePGroup" styleClass="formDataEntryLine">
				<h:outputText id="changeType" value="#{property.contractChChangeType}" styleClass="formLabel" />
				<h:selectOneMenu id="changeTypeDD1" 
								 value="#{pc_contractChange.contractChgType}" 
								 onchange="enableButtonOnPlan();resetTargetFrame();submit();"
								 valueChangeListener="#{pc_contractChange.changeTypeListener}"
								 styleClass="formEntryText" style="width: 350px;"> <!--SPR3746 -->
					<f:selectItems id="changeTypeList" value="#{pc_contractChange.contractChgTypeList}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<h:panelGroup id="changeFormGroup" styleClass="formDataEntryLine">
				<h:selectBooleanCheckbox value="#{pc_contractChange.noChngFrmReqd}" style="margin-left: 122px" />
				<h:outputLabel value="#{property.contractChChangeForm}" styleClass="formLabelRight" />
			</h:panelGroup>			
		</h:panelGroup>
		</DIV>
		</DIV>
		<f:subview id="nbaCommentBar" rendered="#{!pc_ccNavigation.openFromAction}">
			<c:import url="/common/subviews/NbaCommentBar.jsp" />
		</f:subview>
		<h:panelGroup styleClass="tabButtonBar">
			<h:commandButton id="btnBack" value="#{property.buttonBack}" styleClass="formButtonLeft" disabled="#{pc_contractChange.disableBack}"
				action="#{pc_contractChange.previousView}" />
			<h:commandButton id="btnContinue" value="#{property.buttonContinue}" disabled="#{pc_contractChange.disableContinue}"
				action="#{pc_contractChange.nextView}" styleClass="formButtonRight" onclick="submitForm();" 
				rendered="#{!pc_ccNavigation.finalView}"/> <!--SPR3746 -->
			<h:commandButton id="btnCommit" value="#{property.buttonComplete}" action="#{pc_contractChange.commit}" styleClass="formButtonRight"
				disabled="#{pc_ccNavigation.auth.enablement['Commit'] || 
							pc_ccNavigation.notLocked}" 
				rendered="#{pc_ccNavigation.finalView}" onclick="setTargetFrame();"/> <!-- SPR3746 -->
		</h:panelGroup>

		</h:form>
		<!--SPR3715 Code deleted  -->
	<div id="Messages" style="display:none"><h:messages /></div>
</f:view>
</body>

</html>
