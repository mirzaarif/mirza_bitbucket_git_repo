<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA153            6      Companion Case Rewrite -->
<!-- SPR3266           7      Table Pane Focus Should Not Reset to Top When History Event is Selected -->
<!-- NBA213 		   7	  Unified User Interface -->
<!-- NBA271            8      Implementation Of Business Entities in nbA -->
<!-- SPR3440           8      Search Push Button Should Be Disabled Until Minimum Criteria Entered Is Enabled on View Open for Search and Companion Case -->
<!-- SPR3811		NB-1301   Corrections to Companion Case View -->
<!-- SPRNBA-681		NB-1301   Companion Case Issues -->
<!-- SPRNBA-798     NB-1401   Change JSTL Specification Level -->
<!-- NBA340         NB-1501   Mask Government ID -->

<%@ page language="java" %>
<%@	taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- SPRNBA-798 -->
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="/WEB-INF/tld/PopulateBean.tld" prefix="PopulateBean" %>
<%@ taglib uri="/WEB-INF/tld/HelpTag.tld" prefix="Help" %>
<%
String path = request.getContextPath();
String basePath = "";
if (request.getServerPort() == 80) {
	basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
} else {
	basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path +"/";
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>nbA Companion Case Action</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link href="css/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/accelerator.css" rel="stylesheet" type="text/css" />
	<link href="theme/nbaStyle.css" rel="stylesheet" type="text/css" />
	<SCRIPT type="text/javascript" src="javascript/global/file.js"></SCRIPT>	
	<!-- NBA213 code deleted -->
	<SCRIPT type="text/javascript" src="javascript/formFunctions.js" ></SCRIPT>
	<script type="text/javascript" src="javascript/global/scroll.js"></script>  <!-- NBA213 -->
	<script type="text/javascript" src="javascript/global/jquery.js"></script><!--NBA340 -->
	<script type="text/javascript" src="include/govtid.js"></script><!-- NBA340 -->
	<script language="JavaScript" type="text/javascript">
	    var contextpath = '<%=path%>'; //NBA340
		function setTargetFrame() {
			//alert('Setting Target Frame');
			document.forms['form_companionCaseAction'].target='controlFrame';
			return false;
		}
		function resetTargetFrame() {
			//alert('Resetting Target Frame');
			document.forms['form_companionCaseAction'].target='';
			return false;
		}
		function viewImages() {
			var imagesValue = document.forms['form_companionCaseAction']['form_companionCaseAction:images'].value;
			if (imagesValue != null && imagesValue.length > 0) {
				top.closeAllDocuments();
				top.viewAllImages(imagesValue);
				document.forms['form_companionCaseAction']['form_companionCaseAction:images'].value = '';
			}
		}
		//SPR3266 New Method
		function saveTableScrollPosition() {
			saveScrollPosition('form_companionCaseAction:ccResults:resultData', 'form_companionCaseAction:ccResults:resultTableVScroll');
			saveScrollPosition('form_companionCaseAction:ccLinked:linkedData', 'form_companionCaseAction:ccLinked:linkedTableVScroll');
			return false;
		}
		//SPR3266 New Method
		function scrollTablePosition() {
			scrollToPosition('form_companionCaseAction:ccResults:resultData', 'form_companionCaseAction:ccResults:resultTableVScroll');
			scrollToPosition('form_companionCaseAction:ccLinked:linkedData', 'form_companionCaseAction:ccLinked:linkedTableVScroll');
			return false;
		}
		//NBA271 New Method
		function enableSearchButton() {
			toDisable = true; 
			if(document.getElementById('form_companionCaseAction:ccCriteria:businessAreaDD').value!="-1" ){ 
			//Begin SPR3440
			if(document.getElementById('form_companionCaseAction:ccCriteria:contractNumTxt').value!= "" || 
				document.getElementById('form_companionCaseAction:ccCriteria:govtIdInput').value!= "" ||  //NBA340
				document.getElementById('form_companionCaseAction:ccCriteria:lastNameTxt').value!= "" ) {
					toDisable = false;	
				} else if(document.getElementById('form_companionCaseAction:ccCriteria:workTypeDD').value!="-1" &&
				document.getElementById('form_companionCaseAction:ccCriteria:fromDateTxt').value!="" && 
				document.getElementById('form_companionCaseAction:ccCriteria:fromDateTxt').value!="MM/dd/yyyy") {
				  	toDisable = false;
				}
			//End SPR3440		
			}
			document.getElementById('form_companionCaseAction:btnSearch').disabled = toDisable;
		}
		
		//SPRNBA-681 New Method
		function setCompanionCaseView() {
		document.getElementById('form_companionCaseAction:viewOpenedFrom').value="companionCaseFromInteractiveContextMenu";
		}
		
        //NBA340 new Method
		$(document).ready(function() {			
	        $("input[name$='govtIdInput']").govtidValue();
		})		
	</script>
</head>
<body onload="filePageInit();viewImages();scrollTablePosition();enableSearchButton();setCompanionCaseView();" style="overflow-x: hidden; overflow-y: scroll">  <!-- SPR3266 NBA213 NBA271 SPRNBA-681--> 
	<f:view>
		<f:loadBundle basename="properties.nbaApplicationData" var="property"/>
		<PopulateBean:Load serviceName="RETRIEVE_COMPANION_CASES" value="#{pc_ccLinkedTable}" />
		<h:form id="form_companionCaseAction" onsubmit="saveTableScrollPosition();">  <!-- SPR3266 -->
			<div class="inputFormMat">
				<div class="inputForm" style="height: 350px">
					<h:panelGroup styleClass="formTitleBar">
						<h:outputLabel value="#{property.ccTitle}" styleClass="shTextLarge" />
					</h:panelGroup>
		
					<f:subview id="ccCriteria">
						<c:import url="/nbaCompanionCase/subviews/NbaCompanionCaseCriteria.jsp" />
					</f:subview>
		
					<hr class="formSeparator" />
		
					<h:panelGroup styleClass="formButtonBar">
						<h:commandButton id="btnClear" value="#{property.buttonClear}" styleClass="formButtonLeft"
										action="#{pc_ccCriteria.clear}" onclick="resetTargetFrame();" />
						<h:commandButton id="btnSearch" value="#{property.buttonSearch}" styleClass="formButtonRight"
										action="#{pc_ccCriteria.search}" onclick="resetTargetFrame();" accesskey="S" />
					</h:panelGroup>
				</div>
			</div>
			<h:outputLabel id="ccResultTableTitle" value="#{property.ccResultsTableTitle}" styleClass="sectionSubheader" style="margin-left: -10px;" />
			<f:subview id="ccResults">
				<c:import url="/nbaCompanionCase/subviews/NbaCompanionCaseResults.jsp" />
			</f:subview>
			<h:panelGroup id="ccLinkedTableTitle" styleClass="sectionSubheader" >
				<h:outputLabel value="#{property.ccLinkedTableTitle}" />
				<h:outputLabel value="#{property.ccLinkedType}" styleClass="formLabel" style="margin-left: 155px" />
				<h:selectOneMenu styleClass="formEntryText" value="#{pc_ccLinkedTable.companionType}" style="width: 220px; margin-top: -1px">
					<f:selectItems value="#{pc_ccLinkedTable.companionTypes}" />
				</h:selectOneMenu>
			</h:panelGroup>
			<f:subview id="ccLinked">
				<c:import url="/nbaCompanionCase/subviews/NbaCompanionCasesLinked.jsp" />
			</f:subview>
			<h:panelGroup styleClass="tabButtonBar" style="height: 35px">
				<h:commandButton id="btnRefresh" value="#{property.buttonRefresh}" styleClass="tabButtonLeft"
									action="#{pc_ccLinkedTable.refresh}" onclick="resetTargetFrame();"/> <!-- SPR3811 -->
				<h:commandButton id="btnCommit" value="#{property.buttonCommit}" styleClass="tabButtonRight" onclick="resetTargetFrame();"
									disabled="#{pc_ccLinkedTable.auth.enablement['Commit'] || 
												pc_ccLinkedTable.notLocked}"
									action="#{pc_ccLinkedTable.commitFromInteractiveContextMenu}" />  <!-- NBA213 SPR3811-->
			</h:panelGroup>
			<h:inputHidden id="images" value="#{pc_ccLinkedTable.images}" />
			<h:inputHidden id="viewOpenedFrom" value="#{pc_ccLinkedTable.viewOpenedFrom}" /> <!-- SPRNBA-681 -->
		</h:form>

		<!-- NBA213 code deleted -->
		<div id="Messages" style="display:none">
			<h:messages />
		</div>
	</f:view>
</body>
</html>
	