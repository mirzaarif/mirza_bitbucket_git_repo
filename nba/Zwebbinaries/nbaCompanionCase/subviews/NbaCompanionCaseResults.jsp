<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA153            6      Companion Case Rewrite -->
<!-- SPR3317           7      Allow Standard Keystrokes for Multi-Select Including Shift-Select -->
<!-- SPR3266           7      Table Pane Focus Should Not Reset to Top When History Event is Selected -->
<!-- NBA212            7      Content Services -->
<!-- NBA271            8      Implementation Of Business Entities in nbA -->
<!-- FNB011				NB-1101	Work Tracking -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGroup id="resultHeader" styleClass="ovDivTableHeader" >
		<h:panelGrid id="tableHeader" columns="6" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrIcon,ovColHdrText120,ovColHdrText120,ovColHdrText120,ovColHdrText105,ovColHdrText120"><!-- NBA271 -->
			<h:commandLink id="resultHdrCol1" value="#{property.ccResultCol1}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol1}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="resultHdrCol2" value="#{property.ccResultCol2}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol2}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="resultHdrCol3" value="#{property.ccResultCol3}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol3}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="resultHdrCol4" value="#{property.ccResultCol4}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol4}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="resultHdrCol5" value="#{property.ccResultCol5}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol5}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="resultHdrCol6" value="#{property.ccResultCol6}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccResultTable.sortedByCol6}" actionListener="#{pc_ccResultTable.sortColumn}" />  <!-- SPR3266 -->
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="resultData" styleClass="ovDivTableData" style="height: 101px">
		<h:dataTable id="resultTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_ccResultTable.dataTable}" value="#{pc_ccResultTable.results}" var="result"
					rowClasses="#{pc_ccResultTable.rowStyles}" 
					columnClasses="ovColIconTop,ovColText120,ovColText120,ovColText120,ovColText105,ovColText120" >
			<h:column>
				<h:commandLink id="resultCol1" styleClass="ovFullCellSelect" style="width: 100%; height: 100%;" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
								action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true" >  <!-- SPR3317, SPR3266, NBA212 -->
					<h:graphicImage url="images/comments/lock-closed-forlist.gif" title="#{result.lockedBy}"
									 styleClass="ovViewIcon#{result.locked}" style="border-width: 0px; margin-top: -3px;" />
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol2" title="#{result.status}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266, NBA212 -->
					<h:outputText id="cntrctNumTxt" value="#{result.contractNumber}" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol3" title="#{result.status}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266, NBA212 -->
					<h:inputTextarea id="resultName" readonly="true" value="#{result.name}" styleClass="ovMultiLine" style="width: 120px" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol4" title="#{result.status}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266, NBA212 -->
					<h:inputTextarea id="resultCompanionType" readonly="true" value="#{result.companionType}" styleClass="ovMultiLine" style="width: 120px" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol5" title="#{result.status}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%" action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266, NBA212 -->
					<h:outputText id="resultGovtId" value="#{result.govtID}" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="resultCol6" title="#{result.status}" onmouseover="resetTargetFrame();" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccResultTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266, NBA212 -->
					<h:inputTextarea id="resultWorkType" readonly="true" value="#{result.workType}" styleClass="ovMultiLine" style="width: 120px" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup id="buttnBar" styleClass="ovButtonBar"><!-- NBA271 -->
		<h:commandButton id="btnResultViewImages" value="#{property.buttonViewImages}" styleClass="ovButtonRight-1"
						onclick="setTargetFrame()" action="#{pc_ccResultTable.viewImages}" rendered="#{pc_ccResultTable.auth.visibility['Images']}" disabled="#{pc_ccResultTable.viewImagesDisabled}" /> <!-- NBA212 FNB011 -->
		<h:commandButton id="btnLink" value="#{property.buttonLink}" styleClass="ovButtonRight"
						action="#{pc_ccResultTable.link}" disabled="#{pc_ccResultTable.linkDisabled}" />
	</h:panelGroup>
	<h:panelGroup id="statusBarGrp" styleClass="ovStatusBar">
		<h:commandLink id="statusBarGrp1" value="#{property.previousAbsolute}" rendered="#{pc_ccResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.previousPageAbsolute}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:commandLink id="statusBarGrp2" value="#{property.previousPage}" rendered="#{pc_ccResultTable.showPrevious}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.previousPage}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:commandLink id="statusBarGrp3" value="#{property.previousPageSet}" rendered="#{pc_ccResultTable.showPreviousSet}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.previousPageSet}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:commandLink id="statusBarGrp4" value="#{pc_ccResultTable.page1Number}" rendered="#{pc_ccResultTable.showPage1}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page1}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt1" value="#{pc_ccResultTable.page1Number}" rendered="#{pc_ccResultTable.currentPage1}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp5" value="#{pc_ccResultTable.page2Number}" rendered="#{pc_ccResultTable.showPage2}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page2}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt2" value="#{pc_ccResultTable.page2Number}" rendered="#{pc_ccResultTable.currentPage2}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp6" value="#{pc_ccResultTable.page3Number}" rendered="#{pc_ccResultTable.showPage3}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page3}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt3" value="#{pc_ccResultTable.page3Number}" rendered="#{pc_ccResultTable.currentPage3}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp7" value="#{pc_ccResultTable.page4Number}" rendered="#{pc_ccResultTable.showPage4}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page4}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt4" value="#{pc_ccResultTable.page4Number}" rendered="#{pc_ccResultTable.currentPage4}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp8" value="#{pc_ccResultTable.page5Number}" rendered="#{pc_ccResultTable.showPage5}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page5}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt5" value="#{pc_ccResultTable.page5Number}" rendered="#{pc_ccResultTable.currentPage5}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->	
		<h:commandLink id="statusBarGrp9" value="#{pc_ccResultTable.page6Number}" rendered="#{pc_ccResultTable.showPage6}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page6}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt6" value="#{pc_ccResultTable.page6Number}" rendered="#{pc_ccResultTable.currentPage6}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->		
		<h:commandLink id="statusBarGrp10" value="#{pc_ccResultTable.page7Number}" rendered="#{pc_ccResultTable.showPage7}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page7}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt7" value="#{pc_ccResultTable.page7Number}" rendered="#{pc_ccResultTable.currentPage7}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp11" value="#{pc_ccResultTable.page8Number}" rendered="#{pc_ccResultTable.showPage8}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.page8}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:outputText id="resultPageNumTxt8" value="#{pc_ccResultTable.page8Number}" rendered="#{pc_ccResultTable.currentPage8}" styleClass="ovStatusBarTextBold" /><!-- NBA271 -->
		<h:commandLink id="statusBarGrp12" value="#{property.nextPageSet}" rendered="#{pc_ccResultTable.showNextSet}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.nextPageSet}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:commandLink id="statusBarGrp13" value="#{property.nextPage}" rendered="#{pc_ccResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.nextPage}" />  <!-- SPR3266 --><!-- NBA271 -->
		<h:commandLink id="statusBarGrp14" value="#{property.nextAbsolute}" rendered="#{pc_ccResultTable.showNext}" styleClass="ovStatusBarText"
						action="#{pc_ccResultTable.nextPageAbsolute}" />  <!-- SPR3266 --><!-- NBA271 -->
						
	</h:panelGroup>
	<h:inputHidden id="resultTableVScroll" value="#{pc_ccResultTable.VScrollPosition}" />  <!-- SPR3266 -->
