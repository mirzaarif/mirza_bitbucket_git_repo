<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA153            6      Companion Case Rewrite -->
<!-- SPR3317           7      Allow Standard Keystrokes for Multi-Select Including Shift-Select -->
<!-- SPR3266           7      Table Pane Focus Should Not Reset to Top When History Event is Selected -->
<!-- NBA212            7      Content Services -->
<!-- NBA271            8      Implementation Of Business Entities in nbA -->
<!-- FNB011				NB-1101	Work Tracking -->
<!-- SPR3811		NB-1301   Corrections to Companion Case View -->

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

	<h:panelGroup id="linkedHeader" styleClass="ovDivTableHeader">
		<h:panelGrid columns="6" styleClass="ovTableHeader" cellspacing="0"
				columnClasses="ovColHdrIcon,ovColHdrText120,ovColHdrText250,ovColHdrText50,ovColHdrText105,ovColHdrText60">
			<h:commandLink id="linkedHdrCol1" value="#{property.ccLinkedCol1}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol1}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="linkedHdrCol2" value="#{property.ccLinkedCol2}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol2}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="linkedHdrCol3" value="#{property.ccLinkedCol3}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol3}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="linkedHdrCol4" value="#{property.ccLinkedCol4}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol4}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="linkedHdrCol5" value="#{property.ccLinkedCol5}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol5}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
			<h:commandLink id="linkedHdrCol6" value="#{property.ccLinkedCol6}" style="width: 100%; height: 100%"
					styleClass="ovColSorted#{pc_ccLinkedTable.sortedByCol6}" actionListener="#{pc_ccLinkedTable.sortColumn}" />  <!-- SPR3266 -->
		</h:panelGrid>
	</h:panelGroup>
	<h:panelGroup id="linkedData" styleClass="ovDivTableData" style="height: 110px">
		<h:dataTable id="linkedTable" styleClass="ovTableData" cellspacing="0"
					binding="#{pc_ccLinkedTable.dataTable}" value="#{pc_ccLinkedTable.results}" var="result"
					rowClasses="#{pc_ccLinkedTable.rowStyles}"
					columnClasses="ovColIcon,ovColText120,ovColText250,ovColText50,ovColText105,ovColText60" >
			<h:column>
				<h:commandLink id="linkedCol1" title="#{result.status}" styleClass="ovFullCellSelect" style="width: 100%; height: 100%;"
								 onmousedown="saveTableScrollPosition();" action="#{pc_ccLinkedTable.selectForMultipleRows}" immediate="true" >  <!-- SPR3317, SPR3266 -->
					<h:graphicImage id="linkedCol1Attn1" url="images/needs_attention/reset.gif" rendered="#{result.overrideReset}"
									 styleClass="ovViewIconTrue" style="border-width: 0px" />	<!-- SPR3811 -->
					<h:graphicImage id="linkedCol1Attn2" url="images/needs_attention/yield.gif" rendered="#{result.overrideIcon}"
									 styleClass="ovViewIconTrue" style="border-width: 0px" />	<!-- SPR3811 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="linkedCol2" title="#{result.status}" onmousedown="saveTableScrollPosition();"
							styleClass="ovFullCellSelect" style="height: 100%; width:100%"
							action="#{pc_ccLinkedTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266 -->
					<h:outputText id="resultContractNum" value="#{result.contractNumber}" /><!-- NBA271 -->
					<h:outputText id="resultUnlinked" value=" #{property.ccUnlink}" rendered="#{result.unlinked}" /><!-- NBA271 -->
					<h:outputText id="resultLinked" value=" #{property.ccLink}" rendered="#{result.linked}" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="linkedCol3" title="#{result.status}" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccLinkedTable.selectForMultipleRows}" immediate="true">  <!-- SPR3317, SPR3266 -->
					<h:inputTextarea id="resultName"  value="#{result.name}" styleClass="ovMultiLine#{result.draftText}" style="width: 250px" /><!-- NBA271 -->
				</h:commandLink>
			</h:column>
			<h:column>
				<h:commandLink id="linkedCol4a" value=" " rendered="#{!pc_ccLinkedTable.checkSplitAllowed || !result.allowCheckSplit}" title="#{result.status}"
							styleClass="ovFullCellSelect" style="height: 100%" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccLinkedTable.selectForMultipleRows}" immediate="true" />  <!-- SPR3317, SPR3266 -->
				<h:selectBooleanCheckbox id="linkedCol4b" value="#{result.check}"
							rendered="#{pc_ccLinkedTable.checkSplitAllowed && result.allowCheckSplit}" disabled="#{pc_ccLinkedTable.committedCheckSplit}"
							styleClass="ovFullCellSelectCheckBox" valueChangeListener="#{result.splitCheck}" onclick="resetTargetFrame();submit();" />
			</h:column>
			<h:column>
				<h:inputText id="resultAmmountToIp" value="#{result.amountToApply}" rendered="#{pc_ccLinkedTable.splittingCheck}"
							styleClass="formEntryText" style="width: 100px; height: 19px; text-align: right;" ><!-- NBA271 -->
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
				</h:inputText>
				<h:commandLink id="linkedCol5" rendered="#{!pc_ccLinkedTable.splittingCheck}" title="#{result.status}"
							styleClass="ovFullCellSelect" style="width: 100%; height: 100%" onmousedown="saveTableScrollPosition();"
							action="#{pc_ccLinkedTable.selectForMultipleRows}" immediate="true" >  <!-- SPR3317, SPR3266 -->
					<h:outputText id="resultAmmountToOp" value="#{result.amountToApply}" style="width: 100px; height: 17px; text-align: right;"><!-- NBA271 -->
						<f:convertNumber type="currency" currencySymbol="#{property.currencySymbol}" maxFractionDigits="2"/>
					</h:outputText>
				</h:commandLink>
			</h:column>
			<h:column>
				<h:selectBooleanCheckbox id="linkedCol6" value="#{result.override}" title="#{result.overridden}"
							styleClass="ovFullCellSelectCheckBox" immediate="true" />
			</h:column>
		</h:dataTable>
	</h:panelGroup>
	<h:panelGroup id="btnBar" styleClass="ovButtonBar"><!-- NBA271 -->
		<h:commandButton id="btnDelete" value="#{property.buttonDelete}" styleClass="ovButtonLeft"
						action="#{pc_ccLinkedTable.delete}" immediate="true" onclick="setTargetFrame();" disabled="#{pc_ccLinkedTable.deleteDisabled}" />
		<h:commandButton id="btnLinkedViewImages" value="#{property.buttonViewImages}" styleClass="ovButtonRight-1"
						action="#{pc_ccLinkedTable.viewImages}" onclick="setTargetFrame();" rendered="#{pc_ccLinkedTable.auth.visibility['Images']}" disabled="#{pc_ccLinkedTable.viewImagesDisabled}" /> <!-- NBA212 FNB011 -->
		<h:commandButton id="btnUnlink" value="#{property.buttonUnlink}" styleClass="ovButtonRight"
						action="#{pc_ccLinkedTable.unlink}" onclick="resetTargetFrame();" disabled="#{pc_ccLinkedTable.unlinkDisabled}" />
	</h:panelGroup>
	<h:inputHidden id="linkedTableVScroll" value="#{pc_ccLinkedTable.VScrollPosition}" />  <!-- SPR3266 -->
	
