<!-- CHANGE LOG -->
<!-- Audit Number   Version   Change Description -->
<!-- NBA153            6      Companion Case Rewrite -->
<!-- NBA271            8      Implementation Of Business Entities in nbA -->
<!-- SPR3440           8      Search Push Button Should Be Disabled Until Minimum Criteria Entered Is Enabled on View Open for Search and Companion Case -->
<!-- FNB011             NB-1101 Work Tracking -->
<!-- NBA340         NB-1501   Mask Government ID -->

<script language="JavaScript" type="text/javascript">
	function noPercent(e)
	{
		var key;
		var keyChar;
		var percentCheck;

		if (window.event) {  // IE
			key = e.keyCode;
		} else if (e.which) { // Netscape/Firefox/Opera
			key = e.which;
		}
		keyChar = String.fromCharCode(key);
		percentCheck = /%/;
		return !percentCheck.test(keyChar);
	}
</script>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

		<h:panelGroup id="ccLinkedTable1" styleClass="formDataEntryTopLine"><!-- NBA271 -->
			<h:outputLabel id="businessAreaLbl" value="#{property.ccBusinessArea}" styleClass="formLabel" /><!-- NBA271 -->
			<h:selectOneMenu id="businessAreaDD" value="#{pc_ccCriteria.businessArea}" styleClass="formEntryText" style="width: 290px"
						valueChangeListener="#{pc_ccCriteria.changeBusinessArea}" onchange="submit()"><!-- NBA271 -->
				<f:selectItems id="businessAreaList" value="#{pc_ccCriteria.businessAreas}" /><!-- NBA271 -->
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="ccLinkedTable2" styleClass="formDataEntryLine" style="height: 25px;" ><!-- NBA271 -->
			<h:outputLabel id="queueLbl" value="#{property.ccQueue}" styleClass="formLabel" /><!-- NBA271 -->
			<h:selectOneMenu id="queueDD" value="#{pc_ccCriteria.queue}" styleClass="formEntryText" style="width: 290px"><!-- NBA271 -->
				<f:selectItems id="queueList" value="#{pc_ccCriteria.queues}" /><!-- NBA271 -->
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="ccLinkedTable3" styleClass="formDataEntryLine" style="height: 25px;" ><!-- NBA271 -->
			<h:outputLabel id="workTypeLbl" value="#{property.ccWorkType}" styleClass="formLabel" /><!-- NBA271 -->
			<h:selectOneMenu id="workTypeDD" value="#{pc_ccCriteria.workType}" styleClass="formEntryText" style="width: 290px"
						valueChangeListener="#{pc_ccCriteria.changeWorkType}" onchange="resetTargetFrame();submit()"><!-- NBA271 -->
				<f:selectItems id="workTypeList" value="#{pc_ccCriteria.workTypes}" /><!-- NBA271 -->
			</h:selectOneMenu>
		</h:panelGroup>
		<h:panelGroup id="ccLinkedTable4" styleClass="formDataEntryLine" style="height: 25px;" ><!-- NBA271 -->
			<h:outputLabel id="statusLbl" value="#{property.ccStatus}" styleClass="formLabel" /><!-- NBA271 -->
			<h:selectOneMenu id="statusDD" value="#{pc_ccCriteria.status}" styleClass="formEntryTextFull" ><!-- NBA271 -->
				<f:selectItems id="statusList" value="#{pc_ccCriteria.statuses}" /><!-- NBA271 -->
			</h:selectOneMenu>
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGroup id="ccLinkedTable5" styleClass="formDataEntryLine" style="height: 25px;" ><!-- NBA271 -->
			<h:outputLabel id="contractNumLbl" value="#{property.ccContractNumber}" styleClass="formLabel" /><!-- NBA271 -->
			<h:inputText id="contractNumTxt" value="#{pc_ccCriteria.contractNumber}" styleClass="formEntryText"
						style="width: 130px" maxlength="15" onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();"
						onmouseout="enableSearchButton();" /><!-- NBA271 SPR3440 FNB011-->
			<h:outputLabel id="govtIdLbl" value="#{property.ccGovtID}" styleClass="formLabel" style="width: 115px" /> <!-- NBA271 FNB011 -->
			<h:inputText id="govtIdInput" value="#{pc_ccCriteria.govtID}" styleClass="formEntryText" maxlength="9" style="width: 100px" 
				onkeyup="enableSearchButton();" onmouseout="enableSearchButton();"/><!-- NBA271 SPR3440 NBA340-->
		    <h:inputHidden id="govtIdKey" value="#{pc_ccCriteria.govtIdKey}"></h:inputHidden><!-- NBA340 -->
		</h:panelGroup>
		<h:panelGroup id="ccLinkedTable6" styleClass="formDataEntryLine" style="height: 25px;" ><!-- NBA271 -->
			<h:outputLabel id="lastNameLbl" value="#{property.ccLastName}" styleClass="formLabel" /><!-- NBA271 -->
			<h:inputText id="lastNameTxt" value="#{pc_ccCriteria.lastName}" styleClass="formEntryText" style="width: 130px"
							onkeypress="toUpperCase(event);" onkeyup="enableSearchButton();" onmouseout="enableSearchButton();" /><!-- NBA271 SPR3440 FNB011-->
			<h:outputLabel id="firstNameLbl" value="#{property.ccFirstName}" styleClass="formLabel" style="width: 115px" /> <!-- NBA271 FNB011 -->
			<h:inputText id="firstNameTxt" value="#{pc_ccCriteria.firstName}" styleClass="formEntryText" style="width: 100px"
							onkeypress="toUpperCase(event);" /><!-- NBA271 -->
			<h:outputLabel id="middleNameLbl" value="#{property.ccMiddleName}" styleClass="formLabel" style="width: 65px" /><!-- NBA271 FNB011 -->
			<h:inputText id="middleNameTxt" value="#{pc_ccCriteria.middleName}" styleClass="formEntryText" 
							onkeypress="toUpperCase(event);" style="width: 80px;" /><!-- NBA271 FNB011 -->
		</h:panelGroup>

		<f:verbatim>
			<hr class="formSeparator" />
		</f:verbatim>

		<h:panelGrid id="ccLinkedTable7" columns="2" cellspacing="0" cellpadding="0"><!-- NBA271 -->
			<h:column>
				<h:outputLabel id="fromDateLbl" value="#{property.ccFromDate}" styleClass="formLabel" /><!-- NBA271 -->
				<h:inputText id="fromDateTxt" value="#{pc_ccCriteria.fromDate}" styleClass="formEntryDate" style="width: 100px"
					onkeyup="enableSearchButton();" onmouseout="enableSearchButton();" ><!-- NBA271 SPR3440-->
					<f:convertDateTime pattern="#{property.datePattern}"/>
				</h:inputText>
				<h:outputLabel id="fromTimeLbl" value="#{property.ccFromTime}" styleClass="formLabel" style="width: 100px" /><!-- NBA271 -->
				<h:inputText id="fromTimeTxt" value="#{pc_ccCriteria.fromTime}" styleClass="formEntryDate" style="width: 80px" ><!-- NBA271 -->
					<f:convertDateTime pattern="#{property.shorttimePattern}"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="fromTimePrdRB" value="#{pc_ccCriteria.fromTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
								style="width: 100px; margin-right: 70px"><!-- NBA271 -->
					<f:selectItems id="fromTimePrdOpt" value="#{pc_ccCriteria.timePeriods}" /><!-- NBA271 -->
				</h:selectOneRadio>
			</h:column>
			<h:column>
				<h:outputLabel id="toDateLbl" value="#{property.ccToDate}" styleClass="formLabel" /><!-- NBA271 -->
				<h:inputText id="toDateTxt" value="#{pc_ccCriteria.toDate}" styleClass="formEntryDate" style="width: 100px" ><!-- NBA271 -->
					<f:convertDateTime pattern="#{property.datePattern}"/>
				</h:inputText>
				<h:outputLabel id="toTimeLbl" value="#{property.ccToTime}" styleClass="formLabel" style="width: 100px" /><!-- NBA271 -->
				<h:inputText id="toTimeTxt" value="#{pc_ccCriteria.toTime}" styleClass="formEntryDate" style="width: 80px"><!-- NBA271 -->
					<f:convertDateTime pattern="#{property.shorttimePattern}"/>
				</h:inputText>
			</h:column>
			<h:column>
				<h:selectOneRadio id="toTimePrdRB" value="#{pc_ccCriteria.toTimePeriod}" layout="lineDirection" enabledClass="formDisplayText"
								style="width: 100px; margin-right: 70px"><!-- NBA271 -->
					<f:selectItems id="toTimePrdOpt" value="#{pc_ccCriteria.timePeriods}" /><!-- NBA271 -->
				</h:selectOneRadio>
			</h:column>
		</h:panelGrid>
