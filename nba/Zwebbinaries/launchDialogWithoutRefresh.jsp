<%@ page language="java" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="javax.faces.context.FacesContext" %>
<%@ page import="javax.faces.application.FacesMessage" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%
String path = request.getContextPath();
String basePath = "";
if(request.getServerPort() == 80){
	basePath = request.getScheme()+"://"+request.getServerName() + path+"/";
} else {
	basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path+"/";
}
String dialogName = "";
dialogName = request.getQueryString();
int i = dialogName.indexOf("dialogView=");
if(i >= 0){
	dialogName = dialogName.substring(i + 11);
}

boolean closeWindow = (request.getQueryString().indexOf("close=true") >= 0);

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<title>Launch Dialog</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="css/accelerator.css">
	<script type="text/javascript">
		window.opener = parent;
		if ('<%= closeWindow%>' == 'true'){
			parent.closeWindow();
		}
		parent.showWindow('<%= path%>','<%= dialogName%>', parent);
	</script>
</head>
<body>
<f:view>
	<div id="Messages">	
		<h:messages />
	</div>
</f:view>
</body>
</html>
