package com.csc.fs.accel.reports;

/* 
 * *******************************************************************************<BR>
 * This program contains trade secrets and confidential information which<BR>
 * are proprietary to CSC Financial Services Group�.  The use,<BR>
 * reproduction, distribution or disclosure of this program, in whole or in<BR>
 * part, without the express written permission of CSC Financial Services<BR>
 * Group is prohibited.  This program is also an unpublished work protected<BR>
 * under the copyright laws of the United States of America and other<BR>
 * countries.  If this program becomes published, the following notice shall<BR>
 * apply:
 *     Property of Computer Sciences Corporation.<BR>
 *     Confidential. Not for publication.<BR>
 *     Copyright (c) 2002-2006 Computer Sciences Corporation. All Rights Reserved.<BR>
 * *******************************************************************************<BR>
 */
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.businessobjects.crystalreports.printer.bean.ReportPrinter;
import com.crystaldecisions.report.web.viewer.ReportExportControl;
import com.crystaldecisions.reports.sdk.ISubreportClientDocument;
import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.data.Fields;
import com.crystaldecisions.sdk.occa.report.data.IConnectionInfo;
import com.crystaldecisions.sdk.occa.report.data.ParameterField;
import com.crystaldecisions.sdk.occa.report.data.ParameterFieldDiscreteValue;
import com.crystaldecisions.sdk.occa.report.data.Table;
import com.crystaldecisions.sdk.occa.report.data.Tables;
import com.crystaldecisions.sdk.occa.report.data.Values;
import com.crystaldecisions.sdk.occa.report.document.PaperSize;
import com.crystaldecisions.sdk.occa.report.document.PaperSource;
import com.crystaldecisions.sdk.occa.report.exportoptions.ExportOptions;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.crystaldecisions.sdk.occa.report.lib.PropertyBag;
import com.crystaldecisions.sdk.occa.report.lib.PropertyBagHelper;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKExceptionBase;

/**
 * This class receives the http request for report processing.
 * <p>
 * <b>Modifications:</b><br>
 * <table border=0 cellspacing=5 cellpadding=5>
 * <thead>
 * <th align=left>Project</th><th align=left>Release</th><th align=left>Description</th>
 * </thead>
 * <tr><td>NBA198</td><td>Version 7</td><td>Wrapper for Crystal or Pure Java</td></tr>
 * <tr><td>NBA186</td><td>Version 8</td><td>nbA Underwriter Additional Approval and Referral Project</td></tr>
 * <tr><td>NBA228</td><td>Version NB-1101</td><td>Cash Management Enhancement</td></tr>
 * <tr><td>SPRNBA-1060</td><td>Version NB-1601</td><td>Address Possible Security Vulnerabilities Identified by Static Application Security Testing</td></tr>
 * </table>
 * <p>
 * @author CSC FSG Developer
 * @version 7.0.0
 * @since New Business Accelerator - Version 7
 */
public class CrystalReportProcessor extends HttpServlet implements Servlet {
    //These variables are used to retrieve the values passed in http request. The calling class in Resource adapter has similar variables to
    //post the variables in the query string
    public static final String FORMAT = "format";

    public static final String PRINTER = "printer";

    public static final String REPORT_OUTPUT_PATH = "file";

    public static final String REPORT = "rpt";

    public static final String CURRENT_USER = "UserID";

    //Variable to get the path for the Reports
    public static final String PATH_REPORT = "reportPath";

    //Variable to get the path for the log4j.properties file
    public static final String LOG4J_PATH = "log4j-init-file";

    //Variable to get the NbaCashiering DataSource
    public static final String JNDI_Name = "jndiName";

    //Variable used to get the SimpleDateFormat
    public static final String DATE_FORMAT_NOW = "yyyy_MM_dd_HH_mm_ss";

    //All input parameters that end with Param are passed as input to report. There is a similar variable in servlet code
    public static final String PARAM = "Param";

    //These variables are used to get a Filename
    public static final String DOT = ".";

    public static final String FORWARD_SLASH = "/";

    public static final String UNDERSCORE = "_";

    public static final String JDBC_JNDI = "JDBC (JNDI)";

    public static final String CRDB_DLL = "crdb_jdbc.dll";
    
    public static final String LOCATION_ID = "LocationIDParam"; //NBA228
    public static final String DRAFT = "DraftParam"; //NBA228

    //variable to set the report format
    public String requiredFormat = null;

    private static Logger logger;

    /**
     * Default Constructor
     *  
     */
    public CrystalReportProcessor() {
        super();
    }

    /**
     * Loading the log4j.properties on the servlet initialization
     */
    public void init() throws ServletException {
        try {
            //To Enable Logging for the Crystal Reports
            logger = Logger.getLogger(CrystalReportProcessor.class);
            //Loading log4j.properties file for Crystal Reports
            String log4jfile = getInitParameter(LOG4J_PATH);
            if (log4jfile != null) {
                String propfile = getServletContext().getRealPath(log4jfile);
                PropertyConfigurator.configure(propfile);
            }
        } catch (Throwable thr) {
            logger.error(thr.getMessage() + "Servlet Exception is thrown");
            // return error to the caller
            throw new ServletException(thr);
        }
    }

    /**
     * Process incoming HTTP GET requests.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException {

        doPost(req, res);
    }

    /**
     * Process incoming HTTP POST requests. It processes the requests for save and print of reports
     * 
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     * @exception javax.servlet.ServletException
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException {
        try {
            logger.info("Got the Request for Processing");
            String reportPath = getInitParameter(PATH_REPORT);
            if (logger.isDebugEnabled()) { //NBA186
                 logger.debug("reportPath=" + reportPath);
            }//NBA186
            // retrieve the name of the report to be processed
            String reportName = req.getParameter(REPORT);
            String locationId = req.getParameter(LOCATION_ID); //NBA228
            String draftParam = req.getParameter(DRAFT); //NBA228
            if (logger.isDebugEnabled()) { //NBA186
                logger.debug("reportName=" + reportName);
            } //NBA186
            // adding Report deployment location to the report
            String incomingRPT = reportPath + reportName;
            ReportClientDocument reportClientDoc = new ReportClientDocument();
            //open the report as report client document
            reportClientDoc.open(incomingRPT, 0);
            //modify the database properties of the report object before executing
            reportClientDoc = modifyDatabaseProperties(reportClientDoc);

            Map paramMap = req.getParameterMap();
            Set keySet = paramMap.keySet();
            Iterator paramItr = keySet.iterator();
            //Loop on input parameters to determing what values are required as input for report.
            String paramName = null;
            while (paramItr.hasNext()) {
                paramName = (String) paramItr.next();
                if (paramName.endsWith("SubReport")) {
                    ISubreportClientDocument subReport = reportClientDoc.getSubreportController().getSubreport(req.getParameter(paramName));
                    modifyDatabaseProperties(subReport);
                }

            }
            //getting the Timestamp
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
            String timeStamp = sdf.format(cal.getTime());

            //getting the User
            String userID = req.getParameter(CURRENT_USER);

            //getting the Report output Path
            String reportOutputPath = req.getParameter(REPORT_OUTPUT_PATH);

            //getting the format for the report
            requiredFormat = req.getParameter(FORMAT);

            //forming the name for the file to be saved
            StringBuffer fileName = new StringBuffer();
            fileName.append(reportOutputPath);
            //Begin NBA228
            if(locationId != null && !"".equals(locationId)) {
                fileName.append(FORWARD_SLASH);
                fileName.append(locationId);
            }
            if(draftParam != null && !"".equals(draftParam)) {
                fileName.append(FORWARD_SLASH);
                fileName.append(draftParam);
            }
            //End NBA228 
            fileName.append(FORWARD_SLASH);
            fileName.append(reportName.replaceAll("/","_"));//NBA186
            fileName.append(UNDERSCORE);
            fileName.append(userID);
            fileName.append(UNDERSCORE);
            fileName.append(timeStamp);
            fileName.append(DOT);
            fileName.append(requiredFormat);

            // if save requested
            if (fileName != null) {
                saveReport(reportClientDoc, fileName, req, res);
                logger.info("Report is Exported Successfully");
            }

            //If print requested
            String printerName = req.getParameter(PRINTER);
            if (logger.isDebugEnabled()) { //NBA186
                logger.debug("printerName=" + printerName);
            }//NBA186
            if (printerName != null) {
                printReport(reportClientDoc, printerName, reportName, req);
                logger.info("Report is Printed Successfully");
            }

            reportClientDoc.close();

        } catch (Throwable thr) {
            logger.error(thr.getMessage() + "Servlet Exception is thrown");
            // return error to the caller
            throw new ServletException(thr);
        }

    }

    /**
     * Default datasource is DSN.This method changes that to JNDI datasource
     * 
     * @param reportClientDoc
     * @return
     * @throws ReportSDKException
     */
    protected ReportClientDocument modifyDatabaseProperties(ReportClientDocument reportClientDoc) throws ReportSDKException {
        logger.info("Inside modifyDatabaseProperties method");
        Tables tables = reportClientDoc.getDatabaseController().getDatabase().getTables();
        int size = tables.size();
        Table table = null;
        for (int i = 0; i < size; i++) {
            table = extractDBProperties(tables, i);
            //Update old table in the report with the new table.
            reportClientDoc.getDatabaseController().setTableLocation(tables.getTable(i), table);
        }
        return reportClientDoc;
    }

    /**
     * Common method for Extracting DB properties for Reports and SubReports
     * 
     * @param tables
     * @param i
     * @return
     */
    protected Table extractDBProperties(Tables tables, int i) {
        //NBA186 code deleted
        Table table = null;
        IConnectionInfo connectionInfo = null;
        PropertyBag innerPropertyBagObj = null;
        table = (Table) tables.getTable(i);
        connectionInfo = table.getConnectionInfo();
        innerPropertyBagObj = connectionInfo.getAttributes();

        //Getting the datasource name
        String dbName = new String((String)innerPropertyBagObj.get(PropertyBagHelper.CONNINFO_SERVER_NAME)); //NBA186
        
        innerPropertyBagObj.clear();
        innerPropertyBagObj.put(PropertyBagHelper.CONNINFO_SERVER_NAME, dbName);

        //Creates DSN by default. Change that to JNDI datasource
        innerPropertyBagObj.put(PropertyBagHelper.CONNINFO_SERVER_TYPE, JDBC_JNDI);
        innerPropertyBagObj.put(PropertyBagHelper.CONNINFO_DATABASE_DLL, CRDB_DLL);
        connectionInfo.setAttributes(innerPropertyBagObj);
        table.setConnectionInfo(connectionInfo);
        return table;
    }

    /**
     * Default datasource is DSN.This method changes that to JNDI datasource
     * 
     * @param reportClientDoc
     * @return
     * @throws ReportSDKException
     */
    protected ISubreportClientDocument modifyDatabaseProperties(ISubreportClientDocument reportClientDoc) throws ReportSDKException {
        logger.info("Inside modifyDatabaseProperties method");
        Tables tables = reportClientDoc.getDatabaseController().getDatabase().getTables();
        int size = tables.size();
        Table table = null;
        for (int i = 0; i < size; i++) {
            table = extractDBProperties(tables, i);
            //Update old table in the report with the new table.
            reportClientDoc.getDatabaseController().setTableLocation(tables.getTable(i), table);
        }
        return reportClientDoc;
    }

    /**
     * Calls the Crystal api and sets the parameters required for generating the report
     * 
     * @param reportClientDoc
     * @param req
     * @return
     * @throws ReportSDKExceptionBase
     */
    protected ReportExportControl getReportExportOptions(ReportClientDocument reportClientDoc, HttpServletRequest req) throws ReportSDKExceptionBase {

        logger.info("Inside getReportExportOptions Method");
        ReportExportControl oReportExportControl = new ReportExportControl();
        //Pass the report source to the ReportExportControl and set the ExportOptions
        oReportExportControl.setReportSource(reportClientDoc.getReportSource());
        Map paramMap = req.getParameterMap();
        Set keySet = paramMap.keySet();
        Iterator paramItr = keySet.iterator();
        Fields oFields = null;
        //Loop on input parameters to determing what values are required as input for report.
        while (paramItr.hasNext()) {
            String paramName = (String) paramItr.next();
            if (!paramName.endsWith(PARAM)) {
                continue;
            }
            String reportParamName = paramName.substring(0, paramName.indexOf(PARAM));
            oFields = setDiscreteParameterValue("", oFields, reportParamName, req.getParameter(paramName));
        }
        //Pass Fields object to ReportExportControl
        oReportExportControl.setParameterFields(oFields);
        //Set the export options to export to the format of choice.
        ExportOptions oExportOptions = new ExportOptions();
        oReportExportControl.setExportOptions(setExportFormat(oExportOptions, requiredFormat));
        //ExportAsAttachment(true) prompts for open or save; false opens the report
        //  in the speficied format after exporting.
        oReportExportControl.setExportAsAttachment(false);
        return oReportExportControl;
    }

    /**
     * Sets all the save formats for the reports as supported by Crystal Report XI
     * 
     * @param exportOptions
     * @param requiredFormat
     * @return
     */
    protected ExportOptions setExportFormat(ExportOptions exportOptions, String requiredFormat) {
        //Getting all the formats for the Reports as supported by Crystal Report 11
        ReportExportFormat exportFormatObj = ReportExportFormat.from_string(requiredFormat);
        exportOptions.setExportFormatType(exportFormatObj);
        return exportOptions;
    }

    /**
     * Calls the Crystal api and sets the parameters required to save the report
     * 
     * @param reportClientDoc
     * @param fileName
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    protected void saveReport(ReportClientDocument reportClientDoc, StringBuffer fileName, HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        ByteArrayOutputStream byteArrayOutputStream = null;
        FileOutputStream fileOutputStream = null;
		InputStream istream = null; // SPRNBA-1060
        try {
            logger.info("Inside Save");
            // set the appropriate export options to the report
            ReportExportControl oReportExportControl = getReportExportOptions(reportClientDoc, req);
            HttpSession session = req.getSession(true);
            String sessionId = session.getId();
            this.getServletContext().setAttribute(sessionId, oReportExportControl);
            HttpURLConnection con = null;
            String url = req.getRequestURL() + "Helper?sessionId=" + sessionId;
            // make url object to reports Application
            URL reportsHelperApp = new URL(url);
            // prepare http urlconnection
            con = (HttpURLConnection) reportsHelperApp.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setUseCaches(false);
            con.setDefaultUseCaches(false);
            con.connect();
            // add url from parameters
            byte response[] = new byte[1024];
            //Getting the Input Stream from the Helper Servlet
			istream = con.getInputStream(); // SPRNBA-1060
            int c = -1;
            int counter = 0;
            while ((c = istream.read()) != -1) {
                response[counter] = (byte) c;
                counter++;
                if (response.length == counter) {
                    byte tempResponse[] = new byte[response.length + counter];
                    System.arraycopy(response, 0, tempResponse, 0, response.length);
                    response = tempResponse;
                }
            }

            File fileObj = new File(fileName.toString());
            if (!fileObj.exists()) {
                File destDir = new File(fileObj.getParent()); //NBA228
                destDir.mkdirs(); //NBA228
                fileObj.createNewFile();
            }
            fileOutputStream = new FileOutputStream(fileObj);
            byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(response, 0, response.length);
            byteArrayOutputStream.writeTo(fileOutputStream);
            byteArrayOutputStream.flush();

        } catch (Throwable thr) {
            logger.error(thr.getMessage() + "Servlet Exception is thrown");
            // return error to the caller
            throw new ServletException(thr);
		} finally {
			byteArrayOutputStream.close();
			fileOutputStream.close();
			if (istream != null) { // SPRNBA-1060
				istream.close(); // SPRNBA-1060
			} // SPRNBA-1060

		}
    }

    /**
     * Calls the Crystal api and sets the parameters required to print the report
     * 
     * @param reportClientDoc
     * @param printerName
     * @param reportName
     * @param req
     * @throws ReportSDKExceptionBase
     */
    protected void printReport(ReportClientDocument reportClientDoc, String printerName, String reportName, HttpServletRequest req)
            throws ReportSDKExceptionBase {
        logger.info("Inside Print");
        ReportPrinter printer = new ReportPrinter();
        printer.setReportSource(reportClientDoc.getReportSource());
        Map paramMap = req.getParameterMap();
        Set keySet = paramMap.keySet();
        Iterator paramItr = keySet.iterator();
        Fields oFields = null;
        //Loop on input parameters to determing what values are required as input for report.
        while (paramItr.hasNext()) {
            String paramName = (String) paramItr.next();
            if (!paramName.endsWith(PARAM)) {
                continue;
            }
            String reportParamName = paramName.substring(0, paramName.indexOf(PARAM));
            oFields = setDiscreteParameterValue("", oFields, reportParamName, req.getParameter(paramName));
        }
        //setting the printer parameters
        printer.setParameterFields(oFields);
        printer.setNCopies(1);
        printer.setJobTitle(reportName);
        printer.setPrinterName(printerName);
        printer.setPaperSource(PaperSource.auto);
        printer.setPaperSize(PaperSize.paperLetter);
        printer.setCollated(false);
        printer.print();
    }

    /**
     * Utility function to set values for the discrete parameters in the report. The report parameter value is set and added to the Fields collection,
     * which can then be passed to the viewer so that the user is not prompted for parameter values.
     * 
     * @param reportName
     * @param oFields
     * @param paramName
     * @param value
     * @return
     */
    protected Fields setDiscreteParameterValue(String reportName, Fields oFields, String paramName, Object value) {
        if (oFields == null) {
            oFields = new Fields();
        }
        //Create a Values object and a ParameterFieldDiscreteValue object for each
        //object for each parameter field you wish to set.
        //If a ranged value is being set, a ParameterFieldRangeValue object should
        //be used instead of the discrete value object.
        ParameterFieldDiscreteValue oParameterFieldDiscreteValue = new ParameterFieldDiscreteValue();
        oParameterFieldDiscreteValue.setValue(value);

        Values oValues = new Values();
        //Add the parameter field values to the Values collection object.
        oValues.add(oParameterFieldDiscreteValue);

        //Create a ParameterField object for each field that you wish to set.
        ParameterField oParameterField = new ParameterField();
        //You must set the report name.
        //Set the report name to an empty string if your report does not contain a
        //subreport; otherwise, the report name will be the name of the subreport
        oParameterField.setReportName(reportName);
        //Set the name of the parameter. This must match the name of the parameter as defined in the
        //report.
        oParameterField.setName(paramName);
        //Set the current Values collection for each parameter field.
        oParameterField.setCurrentValues(oValues);

        //Add parameter field to the Fields collection. This object is then passed to the
        //viewer as the collection of parameter fields values set.
        oFields.add(oParameterField);
        return oFields;
    }
}