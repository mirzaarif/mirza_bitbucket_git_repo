package com.csc.fs.accel.reports;

/* 
 * *******************************************************************************<BR>
 * This program contains trade secrets and confidential information which<BR>
 * are proprietary to CSC Financial Services Group�.  The use,<BR>
 * reproduction, distribution or disclosure of this program, in whole or in<BR>
 * part, without the express written permission of CSC Financial Services<BR>
 * Group is prohibited.  This program is also an unpublished work protected<BR>
 * under the copyright laws of the United States of America and other<BR>
 * countries.  If this program becomes published, the following notice shall<BR>
 * apply:
 *     Property of Computer Sciences Corporation.<BR>
 *     Confidential. Not for publication.<BR>
 *     Copyright (c) 2002-2006 Computer Sciences Corporation. All Rights Reserved.<BR>
 * *******************************************************************************<BR>
 */
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.crystaldecisions.report.web.viewer.ReportExportControl;

/**
 * This class receives the http request from CrystalReportProcessor Servlet 
 * for generating the response as a input stream.
 * <p>
 * <b>Modifications:</b><br>
 * <table border=0 cellspacing=5 cellpadding=5>
 * <thead>
 * <th align=left>Project</th><th align=left>Release</th><th align=left>Description</th>
 * </thead>
 * <tr><td>NBA198</td><td>Version 7</td><td>Wrapper for Crystal or Pure Java</td></tr>
 * </table>
 * <p>
 * @author CSC FSG Developer
 * @version 7.0.0
 * @since New Business Accelerator - Version 7
 */
public class CrystalReportProcessorExportHelper extends HttpServlet implements Servlet {

    private static Logger logger;

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        doPost(req, res);
    }

    /**
     * Process incoming HTTP POST requests.
     * 
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     * @exception javax.servlet.ServletException
     * @exception java.io.IOException
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            logger = Logger.getLogger(CrystalReportProcessor.class);
            logger.info("Got the Request For Processing inside Helper Servlet");
            ReportExportControl oReportExportControl = (ReportExportControl) this.getServletContext().getAttribute(req.getParameter("sessionId"));
            this.getServletContext().removeAttribute(req.getParameter("sessionId"));
            if (oReportExportControl != null) {
                oReportExportControl.processHttpRequest(req, res, getServletConfig().getServletContext(), null);
            }
        } catch (Throwable thr) {
            logger.error(thr.getMessage() + "Servlet Exception is thrown");
            // return error to the caller
            throw new ServletException(thr);
        }
    }

}